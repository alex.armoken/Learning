import collections
import functools
import inspect
import pprint
import queue
import random
import sys
import time
import traceback


def simple_coroutine():
    print("-> coroutine started")
    x = yield
    print("-> coroutine received: {}".format(x))


def example_01():
    my_corourine = simple_coroutine()
    print("My coroutine: {}".format(my_corourine))
    next(my_corourine)
    my_corourine.send(42)


###########################################################################


def simple_coroutine_2(a):
    print("-> Started: a = {}".format(a))
    b = yield a
    print("-> Received: b = {}".format(b))
    c = yield a + b
    print("-> Received: c = {}".format(c))


def example_02():
    my_corourine = simple_coroutine_2(14)
    print(inspect.getgeneratorstate(my_corourine))
    next(my_corourine)
    print(inspect.getgeneratorstate(my_corourine))
    my_corourine.send(28)

    try:
        my_corourine.send(99)
    except StopIteration:
        print(traceback.format_exc())
    finally:
        print(inspect.getgeneratorstate(my_corourine))


###########################################################################


def averager():
    total = 0.0
    count = 0
    average = None
    while True:
        term = yield average
        total += term
        count += 1
        average = total / count


def example_03():
    coroutine_avg = averager()
    print(inspect.getgeneratorstate(coroutine_avg))
    next(coroutine_avg)
    print(inspect.getgeneratorstate(coroutine_avg))
    print(coroutine_avg.send(10))
    print(coroutine_avg.send(30))
    print(coroutine_avg.send(5))


###########################################################################


def primed(func):
    """Decorator: primes `func` by advancing to first `yield`"""
    @functools.wraps(func)
    def primer(*args, **kwargs):
        gen = func(*args, **kwargs)
        next(gen)
        return gen

    return primer


# Create decorated version of `averager` function
primed_averager = primed(averager)


def example_04():
    coroutine_avg = primed_averager()
    print(inspect.getgeneratorstate(coroutine_avg))
    print(coroutine_avg.send(10))
    print(coroutine_avg.send(30))
    print(coroutine_avg.send(5))


###########################################################################


def example_05():
    coroutine_avg = primed_averager()
    print(coroutine_avg.send(40))
    print(coroutine_avg.send(50))
    print(coroutine_avg.send("spam"))


###########################################################################


class DemoException(Exception):
    """An exception type for the demonstation."""


def demo_exc_handling():
    print("-> coroutine started")
    while True:
        try:
            x = yield
        except DemoException:
            print("*** DemoException handled. Continuing...")
        else:
            print("-> coroutine received: {!r}".format(x))

    raise RuntimeError("This line should never run.")


def example_06():
    exc_coroutine = demo_exc_handling()
    next(exc_coroutine)
    exc_coroutine.send(11)
    exc_coroutine.send(22)
    print(inspect.getgeneratorstate(exc_coroutine))
    exc_coroutine.close()
    print(inspect.getgeneratorstate(exc_coroutine))


###########################################################################


def example_07():
    exc_coroutine = demo_exc_handling()
    next(exc_coroutine)
    exc_coroutine.send(11)
    exc_coroutine.throw(DemoException)
    print(inspect.getgeneratorstate(exc_coroutine))


###########################################################################


def example_08():
    exc_coroutine = demo_exc_handling()
    next(exc_coroutine)
    exc_coroutine.send(11)
    try:
        exc_coroutine.throw(ZeroDivisionError)
    except ZeroDivisionError:
        print(traceback.format_exc())
    finally:
        print(inspect.getgeneratorstate(exc_coroutine))


###########################################################################


Result = collections.namedtuple("Result", "count average")


def breaking_averager():
    total = 0.0
    count = 0
    average = None
    while True:
        term = yield
        if term is None:
            break
        total += term
        count += 1
        average = total / count

    return Result(count, average)


def example_09():
    coroutine_avg = breaking_averager()
    next(coroutine_avg)
    coroutine_avg.send(10)
    coroutine_avg.send(30)
    coroutine_avg.send(6.5)
    try:
        coroutine_avg.send(None)
    except StopIteration as exc:
        print(exc.value)


###########################################################################


def gen():
    yield from "ABC"
    yield from range(1, 3)


def chain(*iterables):
    for iterable in iterables:
        yield from iterable


def example_10():
    print(list(gen()))
    print(list(chain(range(5), "ABC", range(5, -1, -1))))


###########################################################################


def grouper(results, key):
    """The delegation generator"""
    while True:
        results[key] = yield from breaking_averager()


def report(results):
    for key, result in sorted(results.items()):
        group, unit = key.split(";")
        print("{:2} {:5} averaging {:.2f}{}".format(
            result.count, group, result.average, unit
        ))


def example_11():
    example_data = {
        "girls;kx": [
            40.9, 38.5, 44.3, 44.2, 45.2, 45.2, 42.7, 44.5, 38.0, 40.6, 44.5
        ],
        "girls;m": [
            1.6, 1.51, 1.4, 1.3, 1.41, 1.39, 1.33, 1.46, 1.45, 1.43
        ],
        "boys;kg": [
            39.0, 40.8, 43.2, 40.8, 43.1, 38.6, 41.4, 40.6, 36.3
        ],
        "boys;m": [
            1.38, 1.5, 1.32, 1.25, 1.37, 1.48, 1.25, 1.49, 1.46
        ]
    }

    results = {}
    for key, values in example_data.items():
        group = grouper(results, key)
        next(group)
        for value in values:
            group.send(value)

        group.send(None)

    pprint.pprint(results)
    report(results)


###########################################################################


Event = collections.namedtuple("Event", "time proc action")


def taxi_process(ident, trips, start_time=0):
    time = yield Event(start_time, ident, "leave garage")
    for _ in range(trips):
        time = yield Event(time, ident, "pick up passenger")
        time = yield Event(time, ident, "drop off passenger")

    yield Event(time, ident, "going home")


def example_12():
    taxi = taxi_process(ident=13, trips=2, start_time=0)
    event = next(taxi)
    print(event)
    event = taxi.send(event.time + 7)
    print(event)
    event = taxi.send(event.time + 23)
    print(event)
    event = taxi.send(event.time + 5)
    print(event)
    event = taxi.send(event.time + 48)
    print(event)
    event = taxi.send(event.time + 1)
    print(event)
    try:
        event = taxi.send(event.time + 10)
    except StopIteration:
        traceback.print_exc()


###########################################################################


DEPARTURE_INTERVAL = 5
SEARCH_DURATION = 5
TRIP_DURATION = 20


def compute_duration(previous_action):
    """Compute action duration using exponential distribution."""
    if previous_action in ["leave garage", "drop off passenger"]:
        # new state is  prowling
        internal = SEARCH_DURATION
    elif previous_action == "pick up passenger":
        # new state is trip
        internal = TRIP_DURATION
    elif previous_action == "going home":
        internal = 1
    else:
        raise ValueError("Unknown previous_action: {}".format(previous_action))

    return int(random.expovariate(1 / internal)) + 1


class Simulator:
    """Events simulator."""
    def __init__(self, procs_map):
        self.events = queue.PriorityQueue()
        self.procs = dict(procs_map)  # copy variable

    def run(self, end_time, delay=False):
        """Schedule and display events until time is up."""
        # schedule the first event for each cabbie
        for _, proc in sorted(self.procs.items()):
            first_event = next(proc)
            self.events.put(first_event)

        # main loop of the simulation
        sim_time = 0
        while sim_time < end_time:
            if self.events.empty():
                print("*** end of events ***")
                break

            # get and display current event
            current_event = self.events.get()
            if delay:
                time.sleep((current_event.time - sim_time) / 2)

            sim_time, proc_id, previous_action = current_event
            print("taxi:", proc_id, proc_id * "   ", current_event)
            active_proc = self.procs[proc_id]

            # schedule next action for current proc
            next_time = sim_time + compute_duration(previous_action)
            try:
                next_event = active_proc.send(next_time)
            except StopIteration:
                del self.procs[proc_id]
            else:
                self.events.put(next_event)
        else:
            msg_template = "*** end of simulation time: {} event pending ***"
            print(msg_template.format(self.events.qsize()))


def example_13():
    """Taxi simulator with delay on output."""
    departure_interval = 5
    end_time = 180
    num_taxis = 3

    taxis = {i: taxi_process(i, (i + 1) * 2, i * departure_interval)
             for i in range(num_taxis)}

    simulator = Simulator(taxis)
    simulator.run(end_time)


###########################################################################


def print_formated_header(example_func_name):
    header = "{} {} {}".format(
        ">" * 5,
        example_func_name.replace("_", " - "),
        "<" * 5
    )
    print(header)


def main():
    """Main function.

    That calls all functions from this module
    which names started with `example_`
    """
    for global_attr_name, global_attr in sorted(globals().items()):
        if global_attr_name.startswith("example_"):
            try:
                print_formated_header(global_attr_name)
                global_attr()
                print("\n")
            except Exception as e:  # noqa
                print(traceback.format_exc())


if __name__ == '__main__':
    sys.exit(main())
