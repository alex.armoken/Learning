import concurrent
import concurrent.futures
import sys
import time


def display(*args):
    print(time.strftime("[%H:%M:%S]"), end=" ")
    print(*args)


def loiter(n):
    msg = "{}loiter({}): doing nothing for {}s..."
    display(msg.format("\t" * n, n, n))
    time.sleep(n)
    msg = "{}loiter({}): done."
    display(msg.format("\t" * n, n))
    return n * 10


def main():
    count_of_ramblers = 10
    display("Script starting")
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=3)
    results = executor.map(loiter, range(count_of_ramblers))
    display("results:", results)
    display("Waiting for individual results:")
    for i, result in enumerate(results):
        display("result {}: {}".format(i, result))


if __name__ == '__main__':
    sys.exit(main())
