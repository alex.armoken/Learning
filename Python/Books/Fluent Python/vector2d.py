import sys
import math
import array


class Vector2D:
    """2D Vector that supports basic vector operations
    and fully integrated into Python object model.
    """
    typecode = "d"

    def __init__(self, x, y, ):
        self.__x = float(x)
        self.__y = float(y)

    def __iter__(self):
        return (i for i in (self.__x, self.__y))

    def __str__(self):
        return str(tuple(self))

    def __repr__(self):
        class_name = type(self).__name__
        return "{}({!r}, {!r})".format(class_name, *self)

    def __bytes__(self):
        return (bytes([ord(self.typecode)])
                + bytes(array.array(self.typecode, self)))

    def __eq__(self, other):
        if len(self) != len(len(other)):
            return False

        for a, b in zip(self, other):
            if a != b:
                return False

        return True

    def __abs__(self):
        return math.hypot(self.__x, self.__y)

    def __bool__(self):
        return bool(abs(self))

    def __hash__(self):
        return hash((self.__x, self.__y))

    @classmethod
    def frombytes(cls, octets):
        """Construct object from raw bytes."""
        typecode = chr(octets[0])
        data = memoryview(octets[1:]).cast(typecode)
        print(data)
        return cls(*data)

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    @property
    def angel(self):
        return math.atan2(self.__y, self.__x)

    def __format__(self, fmt_spec=""):
        if fmt_spec.endswith('p'):
            fmt_spec = fmt_spec[:-1]
            coords = (abs(self), self.angel)
            outer_fmt = '<{} {}>'
        else:
            coords = self
            outer_fmt = '({}, {})'

        components = (format(coord, fmt_spec) for coord in coords)
        return outer_fmt.format(*components)


def main():
    """Main function to show some operations with 2D vector."""
    vec = Vector2D(1, 2)
    print(str(vec))
    print(repr(vec))
    print(bytes(vec))
    print("{v}".format(v=vec))
    print("{v:p}".format(v=vec))
    print(hash(vec))
    return 0


if __name__ == "__main__":
    sys.exit(main())
