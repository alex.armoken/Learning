import sys
import math
import array
import numbers
import reprlib
import operator
import itertools
import functools


class Vector():
    """Simply n-dimentional vector implementation."""

    shortcut_names = "xyzt"

    __slots__ = (
        "_typecode",
        "_components",
    )

    def __init__(self, components, typecode="d"):
        self._typecode = typecode
        self._components = array.array(self._typecode, components)

    def __iter__(self):
        return iter(self._components)

    def __repr__(self):
        components = reprlib.repr(self._components)
        components = components[components.find("["): -1]
        return "{}({})".format(type(self).__name__, components)

    def __str__(self):
        return str(tuple(self))

    def __bytes__(self):
        return (bytes([ord(self._typecode)])
                + bytes(self._components))

    def __eq__(self, other):
        return tuple(self) == tuple(other)

    def __hash__(self):
        hashes = (hash(x) for x in self._components)
        return functools.reduce(operator.xor, hashes, 0)

    def __abs__(self):
        return math.sqrt(sum(x * x for x in self))

    def __bool__(self):
        return bool(abs(self))

    def __len__(self):
        return len(self._components)

    def __getitem__(self, index):
        cls = type(self)
        if isinstance(index, slice):
            return cls(self._components[index], self._typecode)
        elif isinstance(index, numbers.Integral):
            return self._components[index]
        else:
            msg = '{cls.__name__} indices must be integers'
            raise TypeError(msg.format(cls=cls))

    def __setattr__(self, name, value):
        cls = type(self)
        if len(name) == 1:
            if name in cls.shortcut_names:
                error = 'readonly attribute {attr_name!r}'
                raise AttributeError(error.format(attr_name=name))
            elif name.islower():
                error = "can't set attributes from 'a' to 'z' in {cls.__name__!r}"
                raise AttributeError(error.format(cls=cls))

        super().__setattr__(name, value)

    def __getattr__(self, name):
        cls = type(self)
        if len(name) == 1:
            pos = cls.shortcut_names.find(name)
            if 0 <= pos < len(self._components):
                return self._components[pos]

        msg = "{cls.__name__!r} object has no attribute {attr!r}"
        raise AttributeError(msg.format(cls=cls, attr=name))

    def angle(self, n):
        r = math.sqrt(sum(x * x for x in self[n:]))
        a = math.atan2(r, self[n - 1])
        if n == len(self) - 1 and self[-1] < 0:
            return math.pi * 2 - a

        return a

    def angles(self):
        return (self.angle(n) for n in range(1, len(self)))

    def __format__(self, fmt_spec=""):
        if fmt_spec.endswith('h'):  # hyperspherical coordinates
            fmt_spec = fmt_spec[:-1]
            coords = itertools.chain([abs(self)], self.angles())
            outer_fmt = "<{}>"
        else:
            coords = self
            outer_fmt = "({})"

        components = (format(c, fmt_spec) for c in coords)
        return outer_fmt.format(", ".join(components))

    @classmethod
    def frombytes(cls, octets):
        typecode = chr(octets[0])
        memory_view = memoryview(octets[1:]).cast(typecode)
        return cls(memory_view)


def main():
    vec1 = Vector([1, 2, 3, 4, 5])
    print("str testing: {}".format(str(vec1)))
    print("repr testing: {}".format(repr(vec1)))
    print("getitem testing: {}".format(vec1[3]))
    print("slicing testing: {}".format(vec1[2:5]))

    vec1.kek = 44
    print("setattr testing: {}".format(vec1.kek))
    print("getattr testing: {}".format(vec1.kek))
    print("MRO: {}".format(Vector.__mro__))

    try:
        print(vec1.a)
    except AttributeError as ex:
        print(ex)

    vec2 = Vector([2, 3, 3, 4])
    print("setattr testing: {}".format(vec2))


if __name__ == '__main__':
    sys.exit(main())
