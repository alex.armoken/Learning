import asyncio
import itertools
import sys


class AState:
    def __init__(self):
        self._cur_step = 0
        self._left_border = "["
        self._right_border = "]"


def print_cur_step(step_obj):
    for step_char in itertools.cycle("|\-\\"):
        print(step_char)

    for _ in range(100):
        sys.stdout.write("\b")


async def slow_function(duration_of_execution, state_obj):
    await asyncio.sleep(duration_of_execution)
    return 42


def main():
    loop =  asyncio.get_event_loop()
    sys.stdout.write("EEEEEP\b")


if __name__ == '__main__':
    sys.exit(main())
