import itertools
import sys
import threading
import time


class Signal:
    is_go = True


def spin(msg, signal):
    write, flush = sys.stdout.write, sys.stdout.flush
    for char in itertools.cycle("|/-\\"):
        status = "{} {}".format(char, msg)
        write(status)
        flush()
        write("\b" * len(status))
        time.sleep(0.1)
        if not signal.is_go:
            break

    write("{}{}".format(" " * len(status), "\b" * len(status)))


def slow_function():
    # pretend waiting a long time for I/O
    time.sleep(3)
    return 42


def supervisor():
    signal = Signal()
    spinner = threading.Thread(target=spin, args=("thinking", signal))
    print("Spinner object:", spinner)
    spinner.start()
    result = slow_function()
    signal.is_go = False
    spinner.join()
    return result


def main():
    result = supervisor()
    print("Answer:", result)


if __name__ == '__main__':
    sys.exit(main())
