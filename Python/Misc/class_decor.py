import sys
import functools


def inject_attributes(cls):
    class Wrapper:
        def __call__(self, **kwargs):
            class_copy = type(
                '{}WithInjectedAttributes'.format(cls.__name__),
                cls.__bases__,
                dict(cls.__dict__)
            )
            for attr_name, attr_value in kwargs.items():
                setattr(class_copy, attr_name, attr_value)

            class_copy.__doc__ = "!Attention! " \
                                 "Instance of this class another class " \
                                 " !Attention!\n{}".format(class_copy.__doc__)
            return class_copy

        def __getattribute__(self, attr_name):
            return getattr(cls, attr_name)

    return Wrapper()


@inject_attributes
class TestedClass:
    """Super test class."""

    def kek(self):
        """Whooo amazing. KEK. Eeeee."""
        return 50

    def top(self):
        return 42


def main():
    a = TestedClass(whooo="BITCH", kek="WHAT", cheburek="GOO")
    print(a.__doc__)
    print(a.whooo)
    print(a.kek)
    print(a.cheburek)
    print(a.top)


if __name__ == '__main__':
    sys.exit(main())
