#!/usr/bin/python3

import sys
import argparse

from modules import fib
from modules import text
from modules import sort
from modules import storage
from modules import validator

gustHelp = "Generate unsorted numbers: -gus [file] [count] \
[fromNum] [toNum]"
sortHelp = "Sort entered numbers: --sort [file]"
fibHelp = "Print all fib. numbers from first to n: --fib [n]"
storageHelp = "Run element storage: --storage"
textHelp = "Run text analyzer: --text [file]"
ngramHelp = "Ngram length and top count: --ngram [count] [length]"
validHelp = """Validate Email addres and URL and real number:
--valid [email:real:url] [text]"""


def main():
    parser = argparse.ArgumentParser(description="Labwork")

    parser.add_argument("--sort", "-s", action="store",
                        nargs=1, type=str,
                        help=sortHelp)
    parser.add_argument("--genunsort", "-gus", action="store",
                        nargs=4, type=str,
                        help=gustHelp)
    parser.add_argument("--fib", "-f", action="store",
                        nargs=1, type=int,
                        help=fibHelp)
    parser.add_argument("--storage", "-st", action="store_true",
                        help=storageHelp)
    parser.add_argument("--text", "-t", action="store",
                        nargs=1, type=str,
                        help=textHelp)
    parser.add_argument("--ngram", "-ng", action="store",
                        nargs=2, type=int,
                        help=ngramHelp)
    parser.add_argument("--valid", "-v", action="store",
                        nargs=2, type=str,
                        help=validHelp)

    args = parser.parse_args()

    if args.sort:
        sort.sort_all(sort.read_unsorted_file(args.sort[0].strip()))
    elif args.genunsort:
        sort.gen_unsorted_file(args.genunsort[0].strip(),
                               int(args.genunsort[1].strip()),
                               int(args.genunsort[2].strip()),
                               int(args.genunsort[3].strip()))
    elif args.storage:
        storage.element_storage()
    elif args.fib:
        fib.print_fib(args.fib[0])
    elif args.text:
        count = 10
        n = 4
        if args.ngram:
            count = args.ngram[0]
            n = args.ngram[1]

        text.analyze_text(args.text[0].strip(), count, n)
    elif args.valid:
        if args.valid[0] == "url":
            validator.get_url_parts(args.valid[1])
        elif args.valid[0] == "email":
            validator.validate_email(args.valid[1])
        elif args.valid[0] == "real":
            validator.validate_real_num(args.valid[1])
    else:
        print("Please, enter --help")


if __name__ == "__main__":
    sys.exit(main())
