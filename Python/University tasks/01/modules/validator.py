import re


def validate_real_num(testStr):
    regRealNum = re.compile("^-?[\d]+(\.[\d]*)?$")
    if re.match(regRealNum, testStr):
        print("Correct")
    else:
        print("Incorrect")


def validate_email(testStr):
    regEmail = re.compile("^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$")
    if re.match(regEmail, testStr):
        print("Correct")
    else:
        print("Incorrect")


def get_url_parts(testStr):
    urlReg = re.compile("^(?P<scheme>[a-z0-9]+)?://"
                        "(?P<path>[a-z0-9]+)?."
                        "(?P<fragment>([a-z0-9]*))?"
                        "(?P<query>(\?[a-z0-9]*))?", re.X)

    urlParts = ["scheme", "path", "query", "fragment"]
    if not re.match(urlReg, testStr):
        print("Incorrect")
    else:
        m = re.search(urlReg, testStr)
        for i in range(len(urlParts)):
            print(m.group(i))
