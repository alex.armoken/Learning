
import re


def element_storage():
    allElements = set()
    while True:
        print("""
        1) Enter add <key> [<key> ...] to add 1 and more elements
        2) Enter remove <key> to remove element
        3) Enter find <key> [<key> ...] to check availability
        4) Enter list to show all element
        5) Enter load to load file
        6) Enter save to save in file
        7) Enter grep to search in element by regular expression
        8) Enter quit to close program""")

        rawCommand = input()
        splitedCommand = rawCommand.split()
        if splitedCommand[0] == "add":
            for i in splitedCommand[1:]:
                allElements.add(i)

        elif splitedCommand[0] == "remove":
            for i in splitedCommand[1:]:
                allElements.remove(i)

        elif splitedCommand[0] == "find":
            for i in splitedCommand[1:]:
                if splitedCommand[i] in allElements:
                    print(splitedCommand[i])

        elif splitedCommand[0] == "list":
            for i in allElements:
                print(i)

        elif splitedCommand[0] == "load":
            with open(splitedCommand[1], "r") as inFile:
                fileElements = inFile.read()
                splitedElements = fileElements.split()
                allElements.clear()
                for i in splitedElements:
                    allElements.add(i)

        elif splitedCommand[0] == "save":
            with open(splitedCommand[1], "w+") as inFile:
                for i in allElements:
                    inFile.write(i + " ")

        elif splitedCommand[0] == "grep":
            rawReg = splitedCommand[1]
            compReg = re.compile(rawReg)
            for i in allElements:
                if compReg.match(i):
                    print(i)

        elif splitedCommand[0] == "quit":
            break
        else:
            print("Please try again")
