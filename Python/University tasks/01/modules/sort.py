
import random


def non_rec_quick_sort(arr):
    i = 0
    begEl = [0]
    endEl = [len(arr)]
    left = begEl[i]
    right = endEl[i] - 1
    while i >= 0:
        left = begEl[i]
        right = endEl[i] - 1
        if left < right:
            piv = arr[left]
            while left < right:
                while arr[right] >= piv and left < right:
                    right -= 1
                if left < right:
                    arr[left] = arr[right]
                    left += 1

                while arr[left] <= piv and left < right:
                    left += 1
                if left < right:
                    arr[right] = arr[left]
                    right -= 1

            arr[left] = piv
            begEl.insert(i + 1, left + 1)
            endEl.insert(i + 1, endEl[i])
            endEl[i] = left
            i += 1
            if endEl[i] - begEl[i] > endEl[i - 1] - begEl[i - 1]:
                begEl[i], begEl[i - 1] = begEl[i - 1], begEl[i]
                endEl[i], endEl[i - 1] = endEl[i - 1], endEl[i]
        else:
            i -= 1

    return arr


def merge_sort(arr):
    def merge(arr1, arr2):
        i = 0
        j = 0
        result = []
        while i < len(arr1) and j < len(arr2):
            if arr1[i] < arr2[j]:
                result.append(arr1[i])
                i += 1
            else:
                result.append(arr2[j])
                j += 1
                if i < len(arr1):
                    result += arr1[i:]
                elif j < len(arr2):
                    result += arr2[j:]

        return result

    if len(arr) > 1:
        arr1 = merge_sort(arr[:len(arr) // 2])
        arr2 = merge_sort(arr[len(arr) // 2:])
        return merge(arr1, arr2)
    return arr


def radix_sort(arr):
    def preradix_sort(arr):
        result = []
        maxLen = 0
        for i in arr:
            splitedNumber = []
            number = i
            while number != 0:
                digit = number % 10
                splitedNumber = [digit] + splitedNumber
                number = number // 10

            if maxLen < len(splitedNumber):
                maxLen = len(splitedNumber)

            result.append([i, splitedNumber])

            for i in range(len(result)):
                if len(result[i][1]) < maxLen:
                    for j in range(maxLen - len(result[i][1])):
                        result[i][1].insert(0, 0)

        return result

    correctArr = preradix_sort(arr)

    for i in range(len(correctArr[0][1]) - 1, -1, -1):
        c = [0 for j in range(10)]

        for j in correctArr:
            d = j[1][i]
            c[d] += 1

        count = 0
        for j in range(10):
            tmp = c[j]
            c[j] = count
            count += tmp

        sortedArr = [None for j in range(len(correctArr))]
        for j in correctArr:
            d = j[1][i]
            sortedArr[c[d]] = j
            c[d] += 1

        correctArr = sortedArr

    return correctArr


def gen_unsorted_file(inFilename, count, fromNum, toNum):
    random.seed()
    randomNumbers = []
    for i in range(count):
        randomNumbers.append(random.randint(fromNum, toNum))

    with open(inFilename, "w+") as inFile:
        for i in randomNumbers:
            inFile.write(str(i) + " ")


def read_unsorted_file(inFilename):
    with open(inFilename, "r") as inFile:
        inText = inFile.read()

    unsortedNumbersStr = inText.split()
    unsortedNumbers = []
    for i in unsortedNumbersStr:
        unsortedNumbers.append(int(i))

    return unsortedNumbers


def sort_all(arr):
    quickArr = non_rec_quick_sort(arr.copy())
    mergeArr = merge_sort(arr)
    radixArr = radix_sort(arr)

    for i in range(len(arr)):
        print(quickArr[i], "-", mergeArr[i], "-", radixArr[i][0])

    return 0
