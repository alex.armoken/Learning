import re
import operator


def splite_words(text):
    wordsText = text
    ignoreSymbols = ",.:;\"\'"
    regIgnore = "[" + re.escape("".join(ignoreSymbols)) + "]"
    onlyWords = re.sub(regIgnore, "", wordsText)
    onlyWords = onlyWords.lower()
    splitedWords = onlyWords.split()
    return splitedWords


def repetition_of_words(splitedWords):
    onlyWords = dict()
    for i in splitedWords:
        onlyWords[i] = splitedWords.count(i)

    onlyWords = sorted(onlyWords.items(), key=operator.itemgetter(1))
    return onlyWords


def splite_sentences(text):
    cleanText = text
    ignoreSymbols = ",:;\"\'"
    regIgnore = "[" + re.escape("".join(ignoreSymbols)) + "]"
    onlyWords = re.sub(regIgnore, "", cleanText)
    onlyWords = onlyWords.lower()
    splitedSenteces = onlyWords.split(".")
    splitedSenteces = [i for i in splitedSenteces if i]  # Remove empty strings
    return splitedSenteces


def median_of_words(totalWords, sentences):
    wordsPerSentence = [len(i.split()) for i in sentences]
    wordsPerSentence = sorted(wordsPerSentence)
    return wordsPerSentence[len(wordsPerSentence) // 2]


def ngram(splitedWords, count, n):
    ngramDict = dict()
    for i in splitedWords:
        for j in range(0, len(i) - n + 1):
            ngram = i[j: j + n]
            ngramDict[ngram] = 0
            for k in splitedWords:
                ngramDict[ngram] += k.count(ngram)

    ngramList = sorted(ngramDict.items(), key=operator.itemgetter(1))
    return reversed(ngramList[-count:])


def analyze_text(inFilename, count, n):
    inFile = open(inFilename, "r")
    inText = inFile.read()
    inFile.close()

    splitedWords = splite_words(inText)
    repetitionsOfWords = repetition_of_words(splitedWords)
    splitedSenteces = splite_sentences(inText)
    median = median_of_words(len(splitedWords), splitedSenteces)
    ngramDict = ngram(splitedWords, count, n)

    print("\nAverage number of words in sentences:")
    for i in repetitionsOfWords:
        print(i[0], i[1])

    print("\nTop-K N-grams:")
    for i in ngramDict:
        print(i[0], i[1])

    print("\nWords count:", len(splitedWords))
    print("Average number of words:", len(splitedWords) / len(splitedSenteces))
    print("Median:", median)
