def gen_fib(n):
    i = 1
    fib1 = 0
    fib2 = 1
    while i <= n:
        fib1, fib2 = fib2, fib1 + fib2
        i += 1
        yield fib1


def print_fib(n):
    for i in gen_fib(n):
        print(i)
    return 0
