import re
import math
from search.models import SearchURL, SearchWord


class SearchMixin(object):
    def get_idf(self, word):
        total_n = SearchURL.objects.count()
        db_word = SearchWord.objects.get(word=word)
        nqi = SearchURL.objects.get(word=db_word).count()
        return math.log((total_n - nqi + 0.5) / (nqi + 0.5))

    def get_fqi(self, word, url):
        return len(SearchURL.get(url=url, word=word).places)

    def get_doc_size(self, url):
        all_urls = SearchURL.select().where(url=url)
        doc_size = 0
        for url in all_urls:
            doc_size += len(url.places)

    def get_db25(self, word, url):
        return (self.get_idf(word) * self.get_fqi(word, url) * (2 + 1) /
                (self.get_fqi(word, url) +
                 (2 + 1) *
                 (1 - 0.75 + 0.75 * abs(self.get_doc_size(url)) / 3000)))

    def rank_results(self, result, phrase):
        result.sort(key=lambda url: len(url.places), reverse=True)
        return result

    def one_word_query(self, query_word):
        pattern = re.compile('[\W_]+')
        query_word = pattern.sub(' ', query_word)
        try:
            db_word = SearchWord.objects.get(word=query_word)
            return SearchURL.objects.filter(word=db_word)
        except SearchWord.DoesNotExist:
            return []

    def free_text_query(self, phrase):
        pattern = re.compile('[\W_]+')
        phrase = pattern.sub(' ', phrase).lower()

        result = dict()
        for word in phrase.split():
            for url in self.one_word_query(word):
                if url.url in result:
                    result[url.url].append(url)
                else:
                    result[url.url] = []
                    result[url.url].append(url)

        new_result = []
        for url, urls in result.items():
            rank = 0
            for u in urls:
                rank += self.get_db25(u.word, u.url)

            new_result.append((rank, urls[0]))

        new_result.sort(key=lambda url_pair: url_pair[0], reverse=True)
        return new_result

    # def free_text_query(self, phrase):
    #     pattern = re.compile('[\W_]+')
    #     phrase = pattern.sub(' ', phrase).lower()
    #     result = list()
    #     result_dict = dict()
    #     for word in phrase.split():
    #         query = self.one_word_query(word)
    #         result_dict[word] = query
    #         result += query

    #     url_set = set()
    #     clean_result = list()
    #     for url in result:
    #         if hash(url.url) not in url_set:
    #             url_set.add(hash(url.url))
    #             clean_result.append(url)

    #     return self.rank_results(clean_result, phrase)
