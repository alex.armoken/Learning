from django.contrib import admin
from search.models import SearchURL, SearchWord, UnindexedURL, HashPassedURL


class UnindexedURLAdmin(admin.ModelAdmin):
    list_display = ['url', 'text']


class HashPassedURLAdmin(admin.ModelAdmin):
    list_display = ['url_hash']


class SearchURLAdmin(admin.ModelAdmin):
    list_display = ['url', 'word']


class SearchWordAdmin(admin.ModelAdmin):
    list_display = ['word']


# Register your models here.
admin.site.register(SearchURL, SearchURLAdmin)
admin.site.register(SearchWord, SearchWordAdmin)
admin.site.register(UnindexedURL, UnindexedURLAdmin)
admin.site.register(HashPassedURL, HashPassedURLAdmin)
