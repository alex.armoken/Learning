from django.db import models
from django.contrib.postgres.fields import ArrayField


class UnindexedURL(models.Model):
    """
    Need for saving unindexed pages, after indexer stoping
    """

    url = models.CharField(max_length=2083)
    text = models.TextField()

    def __str__(self):
        return self.url


class HashPassedURL(models.Model):
    """
    Need for saving uncrawled urls, after crawler stoping
    """

    url_hash = models.IntegerField()

    def __str__(self):
        return self.url_hash


class SearchWord(models.Model):
    word = models.CharField(max_length=200)

    def __str__(self):
        return self.word


class SearchURL(models.Model):
    url = models.CharField(max_length=2083)
    places = ArrayField(models.PositiveIntegerField())
    word = models.ForeignKey(SearchWord, related_name="words")

    def __str__(self):
        return self.url
