from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from search.views import SearchResults, SearchPage

urlpatterns = [
    url(r'^$', SearchPage.as_view(), name='search_page'),
    url(r'^sres/', SearchResults.as_view(), name='search_results'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
