from django.shortcuts import render
from search.models import SearchURL
from django.http import HttpResponse, HttpResponseBadRequest
from django.views.generic import ListView, View
from search.forms import SearchForm
from search.search import SearchMixin


class SearchPage(View):
    form_class = SearchForm
    template_name = 'search/search.djhtml'

    def get(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        return render(request, self.template_name, {'form': form})


class SearchResults(ListView, SearchMixin):
    model = SearchURL
    context_object_name = 'results'
    template_name = 'search/searchresults.djhtml'
    paginate_by = 15

    def post(self, request, *args, **kwargs):
        form = SearchForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            return HttpResponse(request.POST.get('phrase', ' '))
        else:
            return HttpResponseBadRequest()

    def get(self, request, *args, **kwargs):
        if 'phrase' in request.GET:
            search_results = self.free_text_query(request.GET['phrase'])
            return render(request, self.template_name,
                          {'results': search_results})
