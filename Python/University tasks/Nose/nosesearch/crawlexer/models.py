import json
import pika
from django.db import models


class Message(models.Model):
    crawler_message = models.CharField(max_length=25)
    indexer_message = models.CharField(max_length=25)

    def save(self):
        connection = pika.BlockingConnection(pika.URLParameters(
            'amqp://noser:1488228kek@localhost:5672'))
        channel = connection.channel()
        print(self.crawler_message, self.indexer_message)
        if self.crawler_message:
            channel.queue_declare(queue='crawlermessages')
            channel.basic_publish(exchange='',
                                  routing_key='crawlermessages',
                                  body=json.dumps(self.crawler_message))

        if self.indexer_message:
            channel.queue_declare(queue='indexermessages')
            channel.basic_publish(exchange='',
                                  routing_key='indexermessages',
                                  body=json.dumps(self.indexer_message))

    class Meta:
        verbose_name = "messages to send to crawler and indexer"
        verbose_name_plural = "messages to send to crawler and indexer"
