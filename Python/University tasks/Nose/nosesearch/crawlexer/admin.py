from django.contrib import admin
from .models import Message


class MessageAdmin(admin.ModelAdmin):
    list_display = ['crawler_message', 'indexer_message']


admin.site.register(Message, MessageAdmin)
