from django.apps import AppConfig


class CrawlexerConfig(AppConfig):
    name = 'crawlexer'
