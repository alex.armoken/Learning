import json
import pika
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from search.models import SearchURL
from indexer.url_normalize import view_url_normalize


class NewIndexView(View):
    template_name = 'indexer/new_index.html'
    rabbitmq_url_params = pika.URLParameters(
        'amqp://noser:1488228kek@localhost:5672')

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)

    def post(self, request, *args, **kwargs):
        message = "Wrong request"

        if 'url' in request.POST:
            norm_url = view_url_normalize(request.POST['url'])
            if not SearchURL.objects.filter(url=norm_url).exists():
                message = 'Added to index'
                try:
                    connection = pika.BlockingConnection(
                        self.rabbitmq_url_params)
                    channel = connection.channel()
                    channel.queue_declare(queue='crawlertasks')
                    channel.basic_publish(exchange='',
                                          routing_key='crawlertasks',
                                          body=json.dumps(norm_url))
                except:
                    message = 'Something went wrong, please try it later'
            else:
                message = 'Already in index'

        return HttpResponse(json.dumps({'message': message}),
                            content_type='application/json')
