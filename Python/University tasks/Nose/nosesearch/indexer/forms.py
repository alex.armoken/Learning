from django import forms


class NewIndexForm(forms.Form):
    url = forms.CharField(max_length=1000)
