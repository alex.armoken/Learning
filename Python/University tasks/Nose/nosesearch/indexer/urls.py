from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static
from indexer.views import NewIndexView

urlpatterns = [
    url(r'^index', NewIndexView.as_view(), name='index')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
