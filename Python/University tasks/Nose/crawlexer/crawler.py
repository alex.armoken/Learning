#!/usr/bin/env python3

import os
import sys
import copy
import time
import json
import queue
import random
import logging

# urllib is a package that collects several modules for working with URLs
# The urllib.parse defines a standard interface to break URL
# strings up in components to combine the components back into a URL string,
# and to convert  a “relative URL” to an absolute URL given a “prev_url URL.”
from urllib import parse

# The urllib.request module defines functions and
# classes which help in opening URLs (mostly HTTP) in a complex world — basic
# and digest authentication, redirections, cookies and more.
from urllib import request

# Library for HTML parsing
from html5lib import treebuilders, HTMLParser

# Library for parallel processing
from multiprocessing import Process, Event, Queue

# Url normalization function
from url_normalize import normalize_url

# Robots Exclusion Protocol Parser for Python
import reppy

# Pika is a RabbitMQ client library for Python
import pika

from peewee import Model, IntegerField
from playhouse.postgres_ext import PostgresqlExtDatabase

# import django
sys.path.append(os.path.join(os.getcwd(), "../nosesearch/nosesearch"))
import settings


TIMEOUT = 30
MIN_SLEEP = 1  # Used for crawler to pause (They can block crawler)
MAX_SLEEP = 3
DEFAULT_AGENT_NAME = "Nose Search Agent/0.1 (My labwork)"
DEFAULT_HEADERS = {
    "Accept": "text/html, text/plain",
    "Accept-Charset": "windows-1251, koi8-r, UTF-8, iso-8859-1, US-ASCII",
    "Content-Language": "ru, en",
}

# Bad HTTP codes that can return HTTP HEAD method
BAD_CODES = {301, 303, 307, 404, 410, 500, 501, 502, 503, 504}
UNACCEPTABLE_PROTOCOLS = frozenset((
    'ed2k',
    'ftp',
    'irc',
    'mailto',
    'news',
    'gopher',
    'nntp',
    'telnet',
    'webcal',
    'xmpp',
    'callto',
    'feed',
    'urn',
    'aim',
    'rsync',
    'tag',
    'ssh',
    'sftp',
    'rtsp',
    'afs',
    'data',
))
UNACCEPTABLE_HTML_TAGS = frozenset((
    "script",
    "noscript",
    "style"
))
ACCEPTABLE_DATA_TYPES = frozenset((
    "asp",
    "aspx",
    "html",
    "htm",
    "php",
    ""
))

db = PostgresqlExtDatabase("nosenew", user="noser")
db.connect()


class HashPassedURL(Model):
    url_hash = IntegerField(unique=True)

    class Meta(object):
        database = db
        db_table = "search_hashpassedurl"


class RobotHandler(request.HTTPHandler):
    """
    Inherited HTTPHandler (A class to handle opening of HTTP URLs),
    that check robots.txt
    """

    def __init__(self, agent_name, robots, *args, **kwargs):
        request.HTTPHandler.__init__(self, *args, **kwargs)
        self.agent_name = agent_name
        self.robots = robots

    def https_open(self, request):
        """
        Send an HTTP request, which can be either GET or POST,
        depending on req.has_data()

        Args:
            request - instance of urllib2.Request
        """
        full_url = request.get_full_url()
        url_parts = parse.urlsplit(full_url)
        robo = None
        if url_parts.netloc in self.robots:
            robo = self.robots[url_parts.netloc]
        else:
            # Getting request url, for checking robots.txt
            host = parse.urlsplit(full_url)[1]
            rurl = parse.urlunparse(("http", host, "/robots.txt", "", ""))
            robo = reppy.cache.RobotsCache()
            robo.fetch(rurl, self.agent_name)
            self.robots[url_parts.netloc] = robo

        # Is url allow for crawler in robots.txt
        if robo.allowed(full_url, self.agent_name):
            # Return result of request
            return request.HTTPHandler.https_open(self, request)
        else:
            raise RuntimeError('Forbidden by robots.txt')


class Crawler(Process):
    """
    Simple crawler that inherited from process class.

    Args:
        alive_event (multiprocessing.Event): Event to alive crawler
        work_event (multiprocessing.Event): Event to signal crawler work
        email (str): Email for communication with sites owners
        queue (multiprocessing.Queue): For getting new urls for processing
        index_queue (multiprocessing.Queue): To giving task for indexer
            tuple(url [str], text [str])
        unavailable_urls (set): Urls that not working
        passed (set): Set for processed url
        robots (dict): Dict for caching robots.txt
        agent_name (Optional [str]): Agent name :]
        headers (Optional [dict]): Setting for http requests (handler need it)
        url_filter (Optional [function]): Function to filter url
    """

    def __init__(self, alive_event, work_event, email, queue, index_queue,
                 passed, robots, unavailable_urls,
                 agent_name=DEFAULT_AGENT_NAME, headers=DEFAULT_HEADERS,
                 url_filter=lambda x: True):
        Process.__init__(self)
        self.alive_event = alive_event
        self.work_event = work_event
        self.email = email
        self.queue = queue
        self.index_queue = index_queue
        self.passed = passed
        self.robots = robots
        self.unavailable_urls = unavailable_urls
        self.agent_name = agent_name
        self.url_filter = url_filter
        self.is_working = False

        self.handler = request.build_opener(RobotHandler(agent_name, robots))
        handler_headers = [(k, v) for k, v in copy.copy(headers).items()]
        handler_headers.append(("User-Agent", agent_name))
        handler_headers.append(("From", email))
        self.handler.addheaders = handler_headers

        self.html_parser = HTMLParser(tree=treebuilders.getTreeBuilder("dom"))
        self.connection = None

    def random_sleep(self, min_time=MIN_SLEEP, max_time=MAX_SLEEP):
        """
        Randomly sleep from min_time to max_time, you
        need it, because site can kick you for frequent requests

        Args:
            min_time (Optional [float]): min sleep time
            max_time (Optional [float]): max sleep time
        """

        time.sleep(random.uniform(min_time, max_time))

    def open(self, url, new_headers=None):
        """
        This method open page and download it

        Created a Request object, than added to it new_headers if they are.

        Args:
            url (str): url, simply url
            new_headers (Optional [dict]): some options for request

        Returns:
            Response on request (html page) or raise exception by handler
        """

        req = request.Request(url)
        if new_headers:
            for k, v in new_headers.items():
                req.add_header(k, v)

        return self.handler.open(req, None, TIMEOUT)

    def read_tag(self, element, prev_url):
        """
        This method get all urls from <a> tags and all user readable text,
        from DOM tree

        Args:
            element (DOM element): can get from DOM tree
                with getElementsByTagName
            prev_url (str): url of current page, need for transforming
                relative urls to absolute_
        """

        if element.nodeType == element.TEXT_NODE:
            return (None, element.data.strip())
        elif (element.nodeType == element.ELEMENT_NODE and
              element.tagName not in UNACCEPTABLE_HTML_TAGS):
            if element.tagName == "a":
                if element.hasAttribute("href"):
                    norm = normalize_url(element.getAttribute("href"),
                                         prev_url)
                    if norm:
                        return ([norm], None)
                    else:
                        return ([], None)

                else:
                    return (None, None)
            elif element.hasChildNodes():
                result_text = ""
                urls = []
                for child in element.childNodes:
                    links, text = self.read_tag(child, prev_url)
                    if text:
                        result_text += " " + text

                    if links:
                        for i in links:
                            urls.append(i)

                return (urls, result_text)

            else:
                return (None, None)

        else:
            return (None, None)

    def extract_data(self, response, prev_url):
        """
        This method simply build a DOM tree from response and extract all
        needed information (urls and user readable text)

        After parsing unlink from DOM tree to encourage early cleanup of
        the now-unneeded objects.

        Args:
            response (response object): response, simply response
            prev_url (str): url of current page, need for transforming
                relative urls to absolute_

        Returns:
            Tuple of list of urls(str) and text(str)
        """

        dom_tree = self.html_parser.parse(response)
        dom_body = dom_tree.getElementsByTagName("body")[0]
        urls, text = self.read_tag(dom_body, prev_url)
        dom_tree.unlink()
        return (urls, text)

    def explore(self, url_pair):
        """
        This method get all information from page.

        All urls on page with the help of link_iterator, and gets
        all human visible text from page. Also this method sleep before every
        new request, because sites can kick it for very important requests

        Everybody know that urls located at <a> tag in href attribute,
        therefore this method getp DOM tree of page in response,
        and return all plinks from this tags.

        Args:
            url_pair (tuple(current_url, previous_url)): current_url will
                processed now, previous url shown in new request as referer.
        """

        self.random_sleep()
        cur_url, prev_url = url_pair
        try:
            if prev_url:
                response = self.open(cur_url, {"Referer": prev_url})
            else:
                response = self.open(cur_url)

            urls, text = self.extract_data(response, cur_url)
            for url in urls:
                if hash(url) not in self.passed:
                    self.passed.add(hash(url))
                    self.queue.put((url, cur_url))

            return text

        except Exception:
            logging.warning("Failure to open: {0}".format(cur_url))
            self.unavailable_urls.add(cur_url)
            return None

    def run(self):
        logging.info("Crawler is starting")
        self.is_working = True

        logging.info("Trying to connect to rabbitmq server")
        connection = pika.BlockingConnection(pika.URLParameters(
            'amqp://noser:1488228kek@localhost:5672'))
        channel = connection.channel()
        channel.queue_declare(queue='indexertasks')
        channel.queue_declare(queue='indexermessages')
        logging.info("Connected to rabbitmq server")

        while self.alive_event.is_set():
            if self.work_event.is_set():
                try:
                    msg = self.queue.get_nowait()
                    text = self.explore(msg)
                    if text is not None:
                        print("\n\n", msg[0])
                        channel.basic_publish(exchange='',
                                              routing_key='indexertasks',
                                              body=json.dumps((msg[0], text)))
                except queue.Empty:
                    logging.debug("Queue is empty")
                    time.sleep(3)
            else:
                self.is_working = False

        connection.close()
        logging.info("Connection stopping")
        logging.info("Crawler is stoping")


class CrawlerFather(object):
    def __init__(self, count, email, passed=None):
        Process.__init__(self)
        self.is_alive = False
        if passed:
            self.passed = passed
        else:
            self.passed = set()  # Urls that where processed

        self.count = count  # Crawlers count
        self.email = email  # Email from  header for crawlers
        self.robots = dict()  # Robots.txt for urls that where processed
        self.alive_event = Event()  # Event to alive Crawlers
        self.work_event = Event()   # Event to signal Crawlers work
        self.queue = Queue()  # Next url to process
        self.index_queue = []  # With empty queue processes can't stop
        self.unavailable_urls = set()
        self.crawler_pool = []

    def start(self):
        if not self.alive_event.is_set():
            for task in self.index_queue:
                self.queue.put(task)

            for i in range(self.count):
                self.crawler_pool.append(Crawler(self.alive_event,
                                                 self.work_event, self.email,
                                                 self.queue,
                                                 self.index_queue,
                                                 self.passed, self.robots,
                                                 self.unavailable_urls))

            self.alive_event.set()
            self.work_event.set()
            self.is_alive = True
            for crawler in self.crawler_pool:
                crawler.start()

    def stop(self):
        self.work_event.clear()
        is_stop_working = False
        while not is_stop_working:
            for crawler in self.crawler_pool:
                is_stop_working = not (crawler.is_working and is_stop_working)

        while not self.queue.empty():
            self.index_queue.append(self.queue.get())

        passed_list = [hash for hash in self.passed]

        self.alive_event.clear()
        for crawler in self.crawler_pool:
            crawler.join()

        self.crawler_pool.clear()  # After every join processes must stoped
        logging.debug("All processes is stoped")
        return passed_list

    def add_url(self, url, referer=None):
        norm_url = normalize_url(url, referer)
        if norm_url:
            self.passed.add(hash(url))
            self.queue.put((url, referer))


def main():
    logging.basicConfig(level=logging.INFO)
    logging.info("Advanced Crawler for search")

    passed = set()
    for url_hash in HashPassedURL.select():
        passed.add(url_hash)
        url_hash.delete()

    crawl_pool = CrawlerFather(4, settings.CRAWLER_EMAIL, passed)
    crawl_pool.start()

    logging.info("Trying to connect to rabbitmq server")
    connection = pika.BlockingConnection(pika.URLParameters(
        'amqp://noser:1488228kek@localhost:5672'))

    channel = connection.channel()
    channel.queue_declare(queue='crawlertasks')
    channel.queue_declare(queue='crawlermessages')
    logging.info("Connected")

    while True:
        message_method, message_header, message_body = channel.basic_get(
            queue='crawlermessages')
        if message_method:
            message = json.loads(message_body.decode("utf-8"))
            channel.basic_ack(delivery_tag=message_method.delivery_tag)
            if message == 'STOP':
                break

        task_method, task_header, task_body = channel.basic_get(
            queue='crawlertasks')
        if task_method:
            logging.info("Get url for crawling")
            crawl_pool.add_url(json.loads(task_body.decode("utf-8")))
            channel.basic_ack(delivery_tag=task_method.delivery_tag)

    connection.close()
    passed = crawl_pool.stop()
    for url_hash in passed:
        HashPassedURL.create(url_hash=url_hash)

    logging.info("Exiting")
    db.close()


if __name__ == "__main__":
    sys.exit(main())
