#!/usr/bin/env python3

import re
import sys
import time
import json
import queue
import logging

# Library for parallel processing
from multiprocessing import Process, Event, Queue

# Pika is a RabbitMQ client library for Python
import pika

from peewee import Model, CharField, IntegerField, ForeignKeyField, TextField
from playhouse.postgres_ext import PostgresqlExtDatabase, ArrayField

db = PostgresqlExtDatabase("nosenew", user="noser")
db.connect()


class UnindexedURL(Model):
    url = CharField(unique=True)
    text = TextField()

    class Meta(object):
        database = db
        db_table = "search_unindexedurl"


class SearchWord(Model):
    word = CharField(unique=True)

    class Meta(object):
        database = db
        db_table = "search_searchword"


class SearchURL(Model):
    word = ForeignKeyField(SearchWord, related_name="words")
    url = CharField(unique=True)
    places = ArrayField(IntegerField)

    class Meta(object):
        database = db
        db_table = "search_searchurl"


class Indexer(Process):
    """
    Simple indexer that inherited from process class.

    Args:
    alive_event (multiprocessing.Event): Event to alive indexer
    work_event (multiprocessing.Event): Event to signal indexer work
    queue (multiprocessing.Queue): For getting new urls for processing
    """

    def __init__(self, queue, work_event, alive_event):
        Process.__init__(self)
        self.pattern = re.compile("[\W_]+")
        self.queue = queue
        self.work_event = work_event
        self.alive_event = alive_event
        self.is_working = False

    def one_word_query(self, word, index):
        word = self.words_pattern.sub(" ", word)
        if word in index:
            return [url for url in index[word].keys()]
        else:
            return []

    def free_text_query(self, text):
        text = self.pattern.sub(" ", text)
        result = []
        for word in text.split():
            result += self.one_word_query(word)

        return list(set(result))

    def phrase_query(self, phrase, index):
        phrase = self.pattern.sub(" ", phrase)
        list_of_lists, result = [], []
        for word in phrase.split():
            list_of_lists.append(self.one_word_query(word))

        setted = set(list_of_lists[0]).intersection(*list_of_lists)
        for url in setted:
            tmp = []
            for word in phrase.split():
                tmp.append(index[word][url])

            for i in range(len(tmp)):
                for index in range(len(tmp[i])):
                    tmp[i][index] -= i

            if set(tmp[0]).intersection(*tmp):
                result.append(url)

        return (phrase, result)

    def clean_text(self, text):
        text = self.pattern.sub(" ", text.lower())
        return text.split()

    def build_pre_index(self, words):
        text_index = dict()
        for index, word in enumerate(words):
            if word in text_index:
                text_index[word].append(index)
            else:
                text_index[word] = [index]

        return text_index

    def inverte_index(self, pre_index):
        index = {}
        for url in pre_index.keys():
            for word in pre_index[url].keys():
                if word in index:
                    if url in index[word]:
                        index[word][url].extend(pre_index[url][word])
                    else:
                        index[word][url] = pre_index[url][word]

                else:
                    index[word] = {url: pre_index[url][word]}

        return index

    def build_index(self, data_pair):
        pre_index = dict()
        url, text = data_pair
        pre_index[url] = self.build_pre_index(self.clean_text(text))

        return self.inverte_index(pre_index)

    def save_to_db(self, index):
        with db.atomic():
            for index_word, url_dict in index.items():
                db_word = SearchWord.get_or_create(word=index_word)[0]
                try:
                    for index_url, index_places in url_dict.items():
                        SearchURL.create(word=db_word,
                                         url=index_url,
                                         places=index_places)
                except:
                    print(index_url, index_word)

    def run(self):
        logging.debug("Indexer is starting")
        self.is_working = True

        while self.alive_event.is_set():
            if self.work_event.is_set():
                try:
                    msg = self.queue.get_nowait()
                    if msg:
                        self.save_to_db(self.build_index(msg))
                        logging.info(msg[0])

                except queue.Empty:
                    logging.debug("Queue is empty")
                    time.sleep(3)
            else:
                self.is_working = False

        logging.debug("Indexer is stoping")


class IndexerFather(object):
    def __init__(self, count):
        Process.__init__(self)
        self.is_alive = False
        self.count = count  # Indexers count
        self.alive_event = Event()  # Event to alive Indexers
        self.work_event = Event()   # Event to signal Indexers work
        self.queue = Queue()  # Next text to process
        self.index_queue = []  # List for saving tasks
        self.indexer_pool = []

    def start(self):
        if not self.alive_event.is_set():
            for task in self.index_queue:
                self.queue.put(task)

            for i in range(self.count):
                self.indexer_pool.append(Indexer(self.queue,
                                                 self.alive_event,
                                                 self.work_event))

            self.alive_event.set()
            self.work_event.set()
            self.is_alive = True
            for indexer in self.indexer_pool:
                indexer.start()

    def stop(self):
        self.work_event.clear()
        is_stop_working = False
        while not is_stop_working:
            for indexer in self.indexer_pool:
                is_stop_working = not (indexer.is_working and is_stop_working)

        while not self.queue.empty():
            self.index_queue.append(self.queue.get())

        self.alive_event.clear()
        for indexer in self.indexer_pool:
            indexer.join()

        self.indexer_pool.clear()  # After every join processes must stoped
        logging.debug("All processes is stoped")
        return self.index_queue

    def add_text(self, url, text):
        self.queue.put((url, text))


def main():
    print("Advanced indexer for search")
    logging.basicConfig(level=logging.INFO)

    unindexed = list()
    for data_pair in UnindexedURL.select():
        unindexed.add((data_pair.url, data_pair.text))
        data_pair.delete()

    indexer_pool = IndexerFather(4)
    indexer_pool.start()

    logging.info("Trying to connect to rabbitmq server")
    connection = pika.BlockingConnection(pika.URLParameters(
        'amqp://noser:1488228kek@localhost:5672'))

    channel = connection.channel()
    channel.queue_declare(queue='indexertasks')
    channel.queue_declare(queue='indexermessages')
    logging.info("Connected")

    while True:
        message_method, message_header, message_body = channel.basic_get(
            queue='indexermessages')
        if message_method:
            message = json.loads(message_body.decode("utf-8"))
            channel.basic_ack(delivery_tag=message_method.delivery_tag)
            if message == 'STOP':
                    break

        task_method, task_header, task_body = channel.basic_get(
            queue='indexertasks')
        if task_method:
            indexer_pool.add_text(*json.loads(task_body.decode("utf-8")))
            channel.basic_ack(delivery_tag=task_method.delivery_tag)

    connection.close()
    unindexed = indexer_pool.stop()
    for data_pair in unindexed:
        UnindexedURL.create(url=data_pair[0], text=data_pair[1])

    logging.info("Exiting")
    db.close()


if __name__ == "__main__":
    sys.exit(main())
