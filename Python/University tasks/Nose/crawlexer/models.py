#!/usr/bin/env python3

from peewee import Model, CharField, IntegerField, ForeignKeyField
from playhouse.postgres_ext import PostgresqlExtDatabase, ArrayField

db = PostgresqlExtDatabase("nosenew", user="noser")
db.connect()


class SearchWord(Model):
    word = CharField(unique=True)

    class Meta(object):
        database = db
        db_table = "search_searchword"


class SearchURL(Model):
    word = ForeignKeyField(SearchWord, related_name="words")
    url = CharField(unique=True)
    places = ArrayField(IntegerField)

    class Meta(object):
        database = db
        db_table = "search_searchurl"
