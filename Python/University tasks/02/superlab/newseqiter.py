#!/usr/bin/env python3

import sys


class FilterIter(object):
    def __init__(self, seq, filter_func):
        self.__seq__ = seq
        self.__filter_func__ = filter_func

    def __iter__(self):
        for element in self.__seq__:
            if self.__filter_func__(element):
                yield element

    def where(self, filter_func):
        return FilterIter(self, filter_func)


class LamdaFilter(object):
    def __init__(self, seq):
        self.__seq__ = seq
        self.__old_values__ = []

    def __iter__(self):
        for element in self.__old_values__:
            yield element

        for element in self.__seq__:
            self.__old_values__.append(element)
            yield element

    def where(self, filter_func):
        return FilterIter(self, filter_func)


def fib():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b


def main():
    kek = (i for i in range(10))
    aa = LamdaFilter(kek)
    for i, j in zip(range(10), aa.where(lambda x: x > 0).
                    where(lambda x: x < 5).where(lambda x: x > 3)):
        print(j)

    print("")
    for i, j in zip(range(10), aa.where(lambda i: i > 0)):
        print(j)

if __name__ == "__main__":
    sys.exit(main())
