#!/usr/bin/python3

import sys


class DefDict1(dict):
    def __init__(self, func):
        self.__def_factory__ = func
        self.__deep_dict__ = dict()

    def __setitem__(self, key, value):
        if value is None:
            deeperDict = DefDict1()
            self.__deep_dict__[key] = deeperDict
            return deeperDict
        else:
            self.__deep_dict__[key] = value

    def __getitem__(self, key):
        if key not in self.__deep_dict__:
            self.__deep_dict__[key] = DefDict1(self.__def_factory__)
            return self.__deep_dict__[key]
        else:
            return self.__deep_dict__[key]

    def __missing__(self, key):
        try:
            value = self.factory(self.type)
        except TypeError:
            value = self.factory()
            self[key] = value
        return value

    def __str__(self):
        return(str(self.__deep_dict__))

    def __len__(self):
        return len(self.__deep_dict__)


class DefDict2(dict):
    def __init__(self, defFunc=None):
        if not callable(defFunc):
            raise TypeError("This is not function!")
        self.factory = DefDict2
        self.type = defFunc

    def __missing__(self, key):
        try:
            value = self.factory(self.factory)
        except TypeError:
            value = self.factory()
            self[key] = value
        return value


def main():
    aaa = DefDict2(int)
    print(aaa[11][4])

if __name__ == "__main__":
    sys.exit(main())
