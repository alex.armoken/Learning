#!/usr/bin/python3

import sys


class Vector(object):
    class WrongOperand(TypeError):
        pass

    class LengthNotEqual(ValueError):
        pass

    def __init__(self, *args):
        if isinstance(args[0], list):
            self.coordinates = args[0]
        elif not isinstance(args[0], list):
            self.coordinates = [i for i in args if isinstance(i, (float, int))]

    def __getitem__(self, i):
        return self.coordinates[i]

    def __add__(self, vector):
        if isinstance(vector, Vector):
            if len(self) == len(vector):
                return Vector([i + j for i, j in zip(self, vector)])
            else:
                raise Vector.LengthNotEqual

        else:
            raise Vector.WrongOperand

    def __sub__(self, vector):
        if isinstance(vector, Vector):
            if len(self) == len(vector):
                return Vector([i - j for i, j in zip(self, vector)])
            else:
                raise Vector.LengthNotEqual

        else:
            raise Vector.WrongOperand

    def __mul__(self, operand):
        if isinstance(operand, (int, float)):
                return Vector([i * operand for i in self.coordinates])
        elif isinstance(operand, Vector):
            return sum([i * j for i, j in zip(self, operand)])
        else:
            raise Vector.WrongOperand

    def __eq__(self, operand):
        if isinstance(operand, Vector):
            return self.coordinates == operand.coordinates
        elif isinstance(operand, (int, float)) and len(self) == 1:
            return self.coordinates[0] == operand
        else:
            return False

    def __ne__(self, vector):
        return not self == vector

    def __len__(self):
        return len(self.coordinates)

    def __str__(self):
        return str(self.coordinates)

    def __repr__(self):
        return self.coordinates


def main():
    v = Vector(1, 23)
    u = Vector(2, 32)
    v = v + u
    v = v * 7
    print(v)

    print(Vector([1, 3, 5]))
    print(Vector(1, 3, 5) * 7)

if __name__ == "__main__":
    sys.exit(main())
