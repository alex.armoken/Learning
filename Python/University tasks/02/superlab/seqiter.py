#!/usr/bin/env python3

import sys


class LamdaFilter(object):
    class FilterIter(object):
        def __init__(self, seq, filter_func, old_values):
            self.__seq__ = seq
            self.__func__ = filter_func
            self.__old_values__ = old_values

        def __iter__(self):
            for i in self.__old_values__:
                yield i

            for i in self.__seq__:
                if self.__func__(i):
                    self.__old_values__.append(i)
                    yield i

    def __init__(self, seq):

        self.__old_values__ = []
        self.__seq__ = seq

    def where(self, filter_func):
        return self.FilterIter(self.__seq__, filter_func, self.__old_values__)


def fib():
    a, b = 0, 1
    while True:
        yield a
        a, b = b, a + b


def main():
    kek = (i for i in range(10))
    aa = LamdaFilter(kek)
    for i, j in zip(range(10), aa.where(lambda i: i > 0)):
        print(j)

    for i, j in zip(range(10), aa.where(lambda i: i > 0)):
        print(j)

if __name__ == "__main__":
    sys.exit(main())
