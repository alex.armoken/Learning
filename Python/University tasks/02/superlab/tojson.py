#!/usr/bin/python3

import sys


class Example(object):
    def __init__(self, a, b, c, d, e, f, g):
        self.aa = a
        self.bb = b
        self.cc = c
        self.dd = d
        self.ee = e
        self.ff = f
        self.gg = g

    def __str__(self):
        return str(self.aa) + " " + str(self.bb) + " " + str(self.cc) + \
            str(self.dd) + " " + str(self.ee) + " " + str(self.ff)


class InvalidJSONConversion(ValueError):
    def __init__(self, type):
        self.type = type

    def __str__(self):
        return "This type can't be serialised in JSON. Type: " + str(self.type)


def to_json(obj, is_attr=False, raise_unknown=False, json_compatible=True):
    json_string_el = ""

    if is_attr and json_compatible and not \
       isinstance(obj, (int, float, bool, str)):
        raise InvalidJSONConversion(type(obj))

    elif object is None:
        json_string_el += "null"
    elif isinstance(obj, str):
        json_string_el += ("\"" +
                           str(obj).replace("\\", "\\\\").
                           replace("\"", "\\\"").
                           replace("\n", "\\n").
                           replace("\b", "\\b").
                           replace("\r", "\\r").
                           replace("\t", "\\t") + "\"")
    elif isinstance(obj, bool):
        if obj:
            json_string_el += "true"
        else:
            json_string_el += "false"
    elif isinstance(obj, (int, float)):
        if is_attr:
            json_string_el += "\"" + str(obj) + "\""
        else:
            json_string_el += str(obj)
    elif isinstance(obj, (list, tuple)):
        json_string_el += "["
        i = 0
        for el in obj:
            json_string_el += to_json(el,
                                      False, raise_unknown, json_compatible)

            if i != len(obj) - 1:
                json_string_el += ", "

            i += 1

        json_string_el += "]"
    elif isinstance(obj, dict):
        json_string_el += "{"
        i = 0
        for attr, value in obj.items():
            json_string_el += to_json(attr,
                                      True, raise_unknown, json_compatible)
            json_string_el += ": "
            json_string_el += to_json(value,
                                      False, raise_unknown, json_compatible)

            if i != len(obj) - 1:
                json_string_el += ", "

            i += 1

        json_string_el += "}"
    elif not raise_unknown:
        json_string_el += "{"
        i = 0
        for attr, value in obj.__dict__.items():
            json_string_el += to_json(attr,
                                      True, raise_unknown, json_compatible)
            json_string_el += ": "
            json_string_el += to_json(value,
                                      False, raise_unknown, json_compatible)

            if i != len(obj.__dict__.items()) - 1:
                json_string_el += ", "

            i += 1

        json_string_el += "}"
    else:
        raise InvalidJSONConversion(type(obj))

    return json_string_el


def main():
    aa = Example(True, """adf""", ("qw", 13),
                 ["afsa", "sfaf"], {123: "afdaf", 11: "kekek"},
                 {(23, 4): "a\"sf"}, 2)

    try:
        print(to_json(aa, False, False, False))
    except InvalidJSONConversion as jsonError:
        print(jsonError, jsonError.args)


if __name__ == "__main__":
    sys.exit(main())
