#!/usr/bin/python3

import sys


class xrange(object):
    def __init__(self, *args):
        if len(args) == 1:
            self.fromNum = 0
            self.currNum = self.fromNum
            self.toNum = args[0]
            self.step = 1
        elif len(args) == 2:
            self.fromNum = args[0]
            self.currNum = self.fromNum
            self.toNum = args[1]
            self.step = 1
        elif len(args) == 3:
            self.fromNum = args[0]
            self.currNum = self.fromNum
            self.toNum = args[1]
            self.step = args[2]

        self.isReversed = False

    def __call__(self):
        outNum = self.currNum
        self.currNum += self.step
        return outNum

    def __iter__(self):
        self.isReversed = False
        while self.currNum != self.toNum:
            yield self.currNum
            self.currNum += self.step

    def __next__(self):
        return self.currNum + self.step

    def __getitem__(self, i):
        if i >= (self.toNum - self.fromNum) / self.step:
            raise IndexError
        else:
            out = self.fromNum
            for i in xrange(i):
                out += self.step

            return out

    def __reversed__(self):
        self.isReversed = True
        self.toNum -= self.step
        self.fromNum -= self.step
        (self.currNum, self.fromNum, self.toNum) = (self.toNum,
                                                    self.toNum,
                                                    self.fromNum)
        self.step = - self.step
        while self.currNum != self.toNum:
            yield self.currNum
            self.currNum += self.step


def main():
    for i in xrange(10):
        print(i)

    print("\n")
    for i in reversed(xrange(10)):
        print(i)

    print("\n")
    print(range(100500)[63])
    print(next(reversed(xrange(100500))))

if __name__ == "__main__":
    sys.exit(main())
