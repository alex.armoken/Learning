import unittest
from superlab.decor import cached


class TestDecor(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_cached(self):
        @cached
        def Kek(i):
            return i

        test_list = []
        for _ in range(10):
            test_list.append(Kek(1))

        self.assertEqual(test_list, [1 for _ in range(10)])


if __name__ == "__main__":
    unittest.main()
