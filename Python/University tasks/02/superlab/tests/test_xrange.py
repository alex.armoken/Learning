import unittest
from superlab.xrange import xrange


class TestXrange(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_next(self):
        self.assertEqual(next(reversed(xrange(100500))), 100499)

    def test_numbers(self):
        test_list = []
        for i in xrange(10):
            test_list.append(i)

        self.assertEqual(test_list, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    def test_reversed(self):
        test_list = []
        for i in reversed(xrange(10)):
            test_list.append(i)

        self.assertEqual(test_list, [9, 8, 7, 6, 5, 4, 3, 2, 1, 0])


if __name__ == "__main__":
    unittest.main()
