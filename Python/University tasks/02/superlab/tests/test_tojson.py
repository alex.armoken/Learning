import unittest
from superlab.tojson import to_json


class TestToJson(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_tojson_dict(self):
        test_json_string = to_json({"aa": 12, "bb": 33})
        is_true_json = False
        if test_json_string == "{\"aa\": 12, \"bb\": 33}" or \
           test_json_string == "{\"bb\": 33, \"aa\": 12}":
            is_true_json = True

        self.assertEqual(is_true_json, True)

    def test_tojson_string(self):
        test_string = "aaaslkj"
        test_json_string = to_json(test_string, False, False, False)
        self.assertEqual(test_json_string, "\"aaaslkj\"")

    def test_tojson_error(self):
        test_string = "aaaslkj"
        test_json_string = to_json(test_string, False, False, False)
        self.assertEqual(test_json_string, "{\"aaaslkj\"}")

if __name__ == "__main__":
    unittest.main()
