import unittest
from superlab.vector import Vector


class TestVector(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_add(self):
        v = Vector(1, 23)
        u = Vector(2, 32)
        self.assertEqual(v + u, Vector(3, 55))

    def test_mul(self):
        v = Vector(1, 23)
        v = v * 7
        self.assertEqual(v, Vector(7, 161))

    def test_vector_error(self):
        v = Vector(1, 23)
        u = Vector(2, 32)
        v = v + u
        v = v * 7
        self.assertEqual(v, 10)


if __name__ == "__main__":
    unittest.main()
