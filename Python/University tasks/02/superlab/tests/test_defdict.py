import unittest
from superlab.defdict import DefDict1


class TestDefDict(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_recursive(self):
        defdict = DefDict1(int)
        defdict[1][2] = 3
        self.assertEqual(defdict[1][2], 3)


if __name__ == "__main__":
    unittest.main()
