import unittest
from superlab.bigfilegen import gen_random_string


class TestBigFileGen(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_numeric(self):
        def represents_int(s):
            try:
                int(s)
                return True
            except ValueError:
                return False

        test_string = gen_random_string([4], 5, "\n", "\t",
                                        True, True)

        self.assertEqual(
            represents_int(test_string.split("\t")[0]), True)

    def test_word_length(self):
        test_string = gen_random_string([4], 5, "\n", "\t",
                                        False, True)
        splited_string = test_string.split("\t")
        self.assertEqual(len(splited_string[0]), 5)

    def test_words_count(self):
        test_string = gen_random_string([4], 5, "\n", "\t",
                                        True, True)
        splited_string = test_string.split("\t")
        self.assertEqual(len(splited_string), 4)

    def test_numeric_error(self):
        def represents_int(s):
            try:
                int(s)
                return True
            except ValueError:
                return False

        test_string = gen_random_string([4], 5, "\n", "\t",
                                        False, True)

        self.assertEqual(represents_int(test_string.split("\t")[0]), True)


if __name__ == "__main__":
    unittest.main()
