import unittest
from superlab.meta import Example


class TestMeta(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_meta1(self):
        b = Example(("/home/armoken/Development/Learning/"
                     "Python/02/superlab/attributes.attr"), "gggaa")

        bugaga = b.bugaga
        self.assertEqual(bugaga, "ыыыыыыы")

    def test_meta2(self):
        b = Example(("/home/armoken/Development/Learning/"
                     "Python/02/superlab/attributes.attr"), "gggaa")

        bugaga = b.kek
        self.assertEqual(bugaga, "lalala")

    def test_error_meta(self):
        b = Example(("/home/armoken/Development/Learning/"
                     "Python/02/superlab/attributes.attr"), "gggaa")

        bugaga = b.ultra
        self.assertEqual(bugaga, 1246)


if __name__ == "__main__":
    unittest.main()
