import unittest
from superlab.logger import Logger


class TestLogger(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_logger(self):
        kek = Logger()
        kek.something()
        log_string = str(kek)
        self.assertEqual(log_string, "Method name - something. \n\
        Args: (), {}. \n\
        Result - 2213123\n\n")

    def test_logger_format(self):
        kek = Logger()
        kek.something()
        kek.set_log_format(
            "Args: {1}, {2}. \n Result - {3} -- Method name - {0}. \n\n")
        log_string = str(kek)
        self.assertEqual(log_string, "Args: (), {}. \n\
        Result - 2213123 -- Method name - something. \n\n")


if __name__ == "__main__":
    unittest.main()
