import unittest
from superlab.seqiter import LamdaFilter


class TestSeqiter(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_infinite(self):
        kek = (i for i in range(10))

        aa = LamdaFilter(kek)
        test_list1 = []
        for i in aa.where(lambda i: i > 0):
            test_list1.append(i)

        test_list2 = []
        for i in aa.where(lambda i: i > 0):
            test_list2.append(i)

        self.assertEqual(test_list1, test_list2)

    def test_where(self):
        kek = (i for i in range(10))

        aa = LamdaFilter(kek)
        test_list = []
        for i in LamdaFilter(aa.where(lambda i: i > 0)).where(lambda i: i < 5):
            test_list.append(i)

        self.assertEqual(test_list, [1, 2, 3, 4])

    def test_sequiter_error(self):
        kek = (i for i in range(10))

        aa = LamdaFilter(kek)
        test_list = []
        for i in LamdaFilter(aa.where(lambda i: i > 0)).where(lambda i: i < 5):
            test_list.append(i)

        self.assertEqual(test_list, [0, 1, 2, 3, 4])


if __name__ == "__main__":
    unittest.main()
