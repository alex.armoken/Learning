#!/usr/bin/env python3

import sys


class SuperMeta(type):
    __attr_file__ = None
    __attrs__ = {}

    def __call__(cls, attr_file_path, *args, **kwargs):
        cls.__attr_file__ = attr_file_path

        obj = type.__call__(cls, *args, **kwargs)

        if attr_file_path is not None:
            with open(attr_file_path, "r") as attr_file:
                all_attrs = attr_file.read()

                attr_lines = all_attrs.split("\n")
                splited_attrs = [i.split("=") for i in attr_lines]

                for attr in splited_attrs[:-1]:
                    cls.__attrs__[attr[0]] = attr[1]

        for attr in cls.__attrs__.items():
            setattr(obj, attr[0], attr[1])

        return obj


class Example(object, metaclass=SuperMeta):
    def __init__(self, name):
        self.aa = name


def main():
    b = Example("attributes.attr", "gggaa")
    c = Example(None, "gggaa")

    print(b.bugaga, b.ultra, b.kek, b.aa)
    print(c.bugaga)


if __name__ == "__main__":
    sys.exit(main())
