#!/usr/bin/env python3

import re
import os
import sys
import random
import string
import argparse

outFileHelp = "Output file path: -o [filename]"
wordsCountHelp = """Set words count thar will be in every string:
-w 15 - this mean 15 words in every string,
-w 4 20 - this mean from 4 to 20 words in string"""
linesSplitterHelp = """Set symbol that will be used for splitting lines: -s
["symbol"]"""
fieldSplitterHelp = """Set symbol that will be used for splitting fields: -f
["symbol"]"""
fileSizeHelp = """Set file size:
-z [number] - set file size in lines count
-z [number]{K,M,G} - set file size in units"""
numericHelp = """Set numerical mode for generator"""
wordLengthHelp = """Set max word length: -l [number]"""
helpHelp = """Information about all flags"""
wordFixedLengthHelp = """Set fixed word length"""


def gen_random_string(wordsCount, wordLength, lineSplitter, fieldSplitter,
                      isNumeric, isFixed):
    newString = ""
    if isNumeric:
        selectedCount = random.choice(wordsCount)
        for i in range(selectedCount):
            if i != 0:
                newString += fieldSplitter

            newString += ''.join(random.choice(string.digits)
                                 for _ in range(random.randint(1, wordLength)))

        newString += lineSplitter
    else:
        selectedCount = random.choice(wordsCount)
        for i in range(selectedCount):
            if i != 0:
                newString += fieldSplitter

            if isFixed:
                newString += ''.join(random.choice(string.ascii_lowercase)
                                     for _ in range(wordLength))
            else:
                newString += ''.join(random.choice(string.ascii_lowercase)
                                     for _ in range(random.
                                                    randint(1, wordLength)))

        newString += lineSplitter

    return newString


def FileGenerator(outFile, wordsCount, lineSplitter, wordLength,
                  fieldSplitter, fileSize, isNumeric, isFixed):
    trueSize = 0
    needLines = True

    if fileSize[-1] == "K":
        trueSize = int(fileSize[:-1]) * 1024
        needLines = False
    elif fileSize[-1] == "M":
        trueSize = int(fileSize[:-1]) * 1024 ** 2
        needLines = False
    elif fileSize[-1] == "G":
        trueSize = int(fileSize[:-1]) * 1024 ** 3
        needLines = False
    else:
        trueSize = int(fileSize)

    with open(outFile, "wt+") as out:
        if needLines:
            for i in range(trueSize):
                out.write(gen_random_string(wordsCount, wordLength,
                                            lineSplitter,
                                            fieldSplitter, isNumeric, isFixed))
        else:
            while os.path.getsize(outFile) < trueSize:
                out.write(gen_random_string(wordsCount, wordLength,
                                            lineSplitter,
                                            fieldSplitter, isNumeric, isFixed))


def main():
    parser = argparse.ArgumentParser("Big File Generator")

    parser.add_argument("--out", "-o", action="store",
                        nargs=1, type=str,
                        help=outFileHelp)
    parser.add_argument("--wordscount", "-w", action="store",
                        nargs="+", type=int, required=True,
                        help=wordsCountHelp)
    parser.add_argument("--linessplitter", "-s", action="store",
                        nargs=1, type=str,
                        help=linesSplitterHelp)
    parser.add_argument("--fieldsplitter", "-f", action="store",
                        nargs=1, type=str,
                        help=fieldSplitterHelp)
    parser.add_argument("--filesize", "-z", action="store",
                        nargs=1, type=str, required=True,
                        help=fileSizeHelp)
    parser.add_argument("--numeric", "-n", action="store_true",
                        help=linesSplitterHelp)
    parser.add_argument("--wordlength", "-l", action="store",
                        nargs=1, type=int,
                        help=wordLengthHelp)
    parser.add_argument("--fixedwordlength", "-k", action="store_true",
                        help=wordFixedLengthHelp)

    args = parser.parse_args()

    outFile = "randomFile.txt"
    lineSplitter = "\n"
    fieldSplitter = "\t"
    fileSize = 100
    isNumeric = False
    isFixed = False
    wordLength = 20
    wordsCount = []

    if args.out:
        outFile = args.out[0].strip()

    if args.wordscount:
        wordsCount = args.wordscount

    if args.fixedwordlength:
        isFixed = True

    if args.linessplitter:
        lineSplitter = args.linessplitter[0].strip()

    if args.fieldsplitter:
        fieldSplitter = args.fieldsplitter[0].strip()

    fileSizeReg = re.compile("^[0-9]+[MKG]?$")
    if fileSizeReg.match(args.filesize[0].strip()):
        fileSize = args.filesize[0].strip()
    else:
        print(parser.print_help())

    if args.numeric:
        isNumeric = True

    if args.wordlength:
        print(args.wordlength)
        wordLength = args.wordlength[0]

    FileGenerator(outFile, wordsCount, lineSplitter, wordLength,
                  fieldSplitter, fileSize, isNumeric, isFixed)

if __name__ == "__main__":
    sys.exit(main())
