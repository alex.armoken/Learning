#!/usr/bin/python3

import sys


def from_json(json_string, json_compatible=True):
    def skip_spaces(string, cur_pos):
        while True:
            if string[cur_pos] == " ":
                cur_pos += 1
            else:
                return cur_pos

    def read_string(string, cur_pos):
        new_string = ""
        is_start = False
        while True:
            if string[cur_pos] == "\"" and not is_start:
                is_start = True
                cur_pos += 1
            elif string[cur_pos] == "\\" and (string[cur_pos + 1] == "\"" or
                                              string[cur_pos + 1] == "\b" or
                                              string[cur_pos + 1] == "\n" or
                                              string[cur_pos + 1] == "\r" or
                                              string[cur_pos + 1] == "\t"):
                new_string += string[cur_pos] + string[cur_pos + 1]
                cur_pos += 2
            elif string[cur_pos] == "\"" and is_start:
                cur_pos += 1
                break
            else:
                new_string += string[cur_pos]
                cur_pos += 1

        return (new_string, cur_pos)

    def read_bool(string, cur_pos):
        new_bool = ""
        while True:
            if string[cur_pos] == "," or \
               string[cur_pos] == "]" or \
               string[cur_pos] == "}":
                if new_bool == "true":
                    return (True, cur_pos)
                else:
                    return (False, cur_pos)
            else:
                new_bool += string[cur_pos]
                cur_pos += 1

        return (new_bool, cur_pos)

    def read_number(string, cur_pos):
        new_number = ""
        while True:
            if string[cur_pos] == "," or \
               string[cur_pos] == "]" or \
               string[cur_pos] == "}":
                return (float(new_number), cur_pos)
            else:
                new_number += string[cur_pos]
                cur_pos += 1

    def read_array(string, cur_pos):
        new_array = list()
        cur_pos += 1  # Need to skip "["

        while True:
            # Reading value
            value = None
            if string[cur_pos] == "\"":
                value, cur_pos = read_string(string, cur_pos)
            elif string[cur_pos] == "{":
                value, cur_pos = read_dict(string, cur_pos)
            elif string[cur_pos] == "[":
                value, cur_pos = read_array(string, cur_pos)
            elif string[cur_pos] == "t" or string[cur_pos] == "f":
                value, cur_pos = read_bool(string, cur_pos)
            else:
                value, cur_pos = read_number(string, cur_pos)

            new_array.append(value)

            if string[cur_pos] == "]":
                return (new_array, cur_pos)
            else:
                cur_pos = skip_spaces(string, cur_pos)  # Need to skip ", "
                cur_pos += 1
                cur_pos = skip_spaces(string, cur_pos)

    def read_dict(string, cur_pos):
        new_dict = dict()
        cur_pos += 1  # Need to skip "{"

        while True:
            key, cur_pos = read_string(string, cur_pos)  # Reading key

            # # cur_pos += skip_spaces(string, cur_pos)  # Need to skip ": "
            # cur_pos += 2
            cur_pos = skip_spaces(string, cur_pos)  # Need to skip ": "
            cur_pos += 1
            cur_pos = skip_spaces(string, cur_pos)

            # Reading value of key
            value = None
            if string[cur_pos] == "\"":
                value, cur_pos = read_string(string, cur_pos)
            elif string[cur_pos] == "{":
                value, cur_pos = read_dict(string, cur_pos)
            elif string[cur_pos] == "[":
                value, cur_pos = read_array(string, cur_pos)
                cur_pos += 1
            elif string[cur_pos] == "t" or string[cur_pos] == "f":
                value, cur_pos = read_bool(string, cur_pos)
            else:
                value, cur_pos = read_number(string, cur_pos)

            new_dict[key] = value

            if string[cur_pos] == "}":
                return (new_dict, cur_pos)
            else:
                cur_pos = skip_spaces(string, cur_pos)  # Need to skip ", "
                cur_pos += 1
                cur_pos = skip_spaces(string, cur_pos)

    value = None
    if json_string[0] == "{":
        value = read_dict(json_string, 0)[0]
    elif json_string[0] == "[":
        value = read_array(json_string, 0)[0]
    elif json_string[0] == "\"":
        value = read_string(json_string, 0)[0]
    elif json_string[0] == "t" or json_string[0] == "f":
        value = read_bool(json_string, 0)[0]
    else:
        value = read_number(json_string, 0)[0]

    return value


def main():
    # simple_string = "\"asf\\\"saf\""
    bbb = """[true, false, 100500, [1337, 228, "[1, 3]"]]"""

    # print(aaa)
    obj = from_json(bbb)
    print(obj)


if __name__ == "__main__":
    sys.exit(main())
