#!/usr/bin/env python3

import sys


def cached(func):
    argsAndRes = {}

    def tupleIt(args=None, kwargs=None):
        if isinstance(args, (int, float, tuple, str)):
            return args

        allArgs = None
        if args:
            allArgs = tuple(tupleIt(i) for i in sorted(args))

        allKwargs = None
        if kwargs:
            allKwargs = tuple(tupleIt(i) for i in sorted(kwargs.items()))

        if args and kwargs:
                return zip(args, kwargs)

        elif args:
            return allArgs
        elif kwargs:
            return allKwargs

    def wrapper(*args, **kwargs):
        nonlocal argsAndRes
        tup = tupleIt(args, kwargs)
        if tup in argsAndRes:
            return argsAndRes[tup]
        else:
            resOfFunc = func(*args, **kwargs)
            argsAndRes[tup] = resOfFunc
            return resOfFunc

    return wrapper


@cached
def Kek(i):
    return i


def main():
    for i in range(10):
        print(i, Kek(1))

    for i in range(10):
        print(i, Kek(2))


if __name__ == "__main__":
    sys.exit(main())
