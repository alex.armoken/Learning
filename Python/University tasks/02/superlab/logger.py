#!/usr/bin/env python3

import sys
import types


class Logger(object):
    def __init__(self):
        self.__loglist__ = []
        self.__format_string__ = "Method name - {0}. \n\
        Args: {1}, {2}. \n\
        Result - {3}\n\n"

    def something(self):
        return 2213123

    def set_log_format(self, format_string):
        self.__format_string__ = format_string

    def __getattribute__(self, name):
        if isinstance(object.__getattribute__(self, name), types.MethodType):
            def log_func(*args, **kwargs):
                result = object.__getattribute__(self, name)(*args, **kwargs)
                self.__loglist__.append((name, args, kwargs, result))
                return result

            return log_func

        else:
            return object.__getattribute__(self, name)

    def __str__(self):
        logLine = ""
        for info in self.__loglist__:
            logLine += self.__format_string__. format(info[0], info[1],
                                                      info[2], info[3])

        return logLine


def main():
    kek = Logger()
    kek.something()
    print(kek)
    print("\n")
    kek.set_log_format(
        "Args: {1}, {2}. \n Result - {3} -- Method name - {0}. \n\n")
    print(kek)

if __name__ == "__main__":
    sys.exit(main())
