#!/usr/bin/env python3

import sys
import tempfile
import argparse


file_help = """Input file path: -f [path]"""
out_help = """Output file path: -o [path]"""
check_help = """Program will check input data and will stop
program on first sort error"""
lines_splitter_help = """Set symbol that will be used for splitting lines: -l
["symbol"]"""
field_splitter_help = """Set symbol that will be used for splitting fields: -t
["symbol"]"""
numeric_help = """Set numerical mode for generator"""
revert_help = """Set revert sorting mode"""
words_count_help = """Set revert sorting mode"""


def str_compare(first_string,
                second_string,
                field_splitter,
                is_numeric, sorted_fields_nums=None):
    """
    This function is used to compare 2 strings

    It return 1 if 1st string is large then 2nd
    It return 0 if 1st equal to 2nd string
    It return -1 if 2nd string is large then 1st"""

    if first_string is None and second_string is None:
        return 0
    elif first_string is None:
        return -1
    elif second_string is None:
        return 1

    first_splitting = first_string.split(field_splitter)
    second_splitting = second_string.split(field_splitter)

    if is_numeric:
        first_splitting = [int(i) for i in first_splitting]
        second_splitting = [int(i) for i in second_splitting]

    if isinstance(sorted_fields_nums, tuple):
        for i in sorted_fields_nums:
            if first_splitting[i] < second_splitting[i]:
                return -1
            elif first_splitting[i] > second_splitting[i]:
                return 1
            else:
                return 0

    else:
        max_len = max(len(first_splitting), len(second_splitting))
        for i in range(max_len):
            if first_splitting[i] < second_splitting[i]:
                return -1
            elif first_splitting[i] > second_splitting[i]:
                return 1

            if i == max_len - 1:
                if len(first_splitting) > len(second_splitting):
                    return 1
                elif len(first_splitting) < len(second_splitting):
                    return -1
                else:
                    return 0


def revert_str_compare(first_string, second_string, field_splitter,
                       is_numeric, sorted_fields_nums=None):
    return -str_compare(first_string, second_string,
                        field_splitter, is_numeric, sorted_fields_nums)


def heap_sort(arr, cmp_func, field_splitter, is_numeric, sorted_fields_nums):
    def down_heap(arr, sifting_num, n):
        selected_el = arr[sifting_num]
        while 2 * sifting_num + 1 < n:
            child = 2 * sifting_num + 1
            if child + 1 < n and cmp_func(arr[child + 1],
                                          arr[child],
                                          field_splitter,
                                          is_numeric,
                                          sorted_fields_nums) == 1:
                child += 1
            if cmp_func(selected_el, arr[child],
                        field_splitter, is_numeric, sorted_fields_nums) >= 0:
                break
            arr[sifting_num] = arr[child]
            sifting_num = child

        arr[sifting_num] = selected_el

    size = len(arr)
    for i in range(size // 2 - 1, -1, -1):
        down_heap(arr, i, size)

    for i in range(size - 1, 0, -1):
        temp = arr[i]
        arr[i] = arr[0]
        arr[0] = temp
        down_heap(arr, 0, i)


def read_source_line(out_source_file, lines_splitter):
    """This function simply read one line from source file"""
    buf_string = ""
    while True:
        symbol = out_source_file.read(1)

        if not symbol:
            return None
        elif symbol != lines_splitter:
            buf_string += symbol
        else:
            buf_string += symbol
            return buf_string


def write_out_line(line, in_file):
    """This function simply write line to file"""
    for i in line:
        in_file.write(i)


def polyphase_sort(orig_file_path, file_out, lines_splitter,
                   field_splitter, is_numeric, buf_size,
                   num_of_files, cmp_func, sorted_fields_nums):

    ideal_ser_count = [1 for i in range(num_of_files)]
    empty_ser_count = [1 for i in range(num_of_files)]

    ideal_ser_count[num_of_files - 1] = 0
    empty_ser_count[num_of_files - 1] = 0

    last_elements = [None for _ in range(num_of_files)]
    curr_line = None
    next_line = None
    tmp_files = [tempfile.TemporaryFile() for _ in range(num_of_files)]
    curr_out_file_num = 0
    curr_level_num = 1

    def read_line(out_file):
        """This function simply read one line from file"""
        buf_string = ""
        while True:
            symbol = out_file.read(1)
            symbol = str(symbol, encoding='UTF-8')

            if not symbol:
                return None
            elif symbol != lines_splitter:
                buf_string += symbol
            else:
                buf_string += symbol
                return buf_string

    def write_line(line, in_file):
        """This function simply write line to file"""
        for i in line:
            in_file.write(bytes(i, "UTF-8"))

    def seriesed_file(path_to_original_file, max_buf_size):
        """This function split original file (path_to_original_file)
        on short sorted series that are not
        more greater than the max_buf_size"""
        buf_array = []
        ser_file = tempfile.TemporaryFile()
        with open(path_to_original_file) as origFile:
            while True:
                buf_line = read_source_line(origFile, lines_splitter)
                if not buf_line:
                    heap_sort(buf_array, cmp_func,
                              field_splitter, is_numeric,
                              sorted_fields_nums)

                    for line in buf_array:
                        write_line(line, ser_file)
                    break
                else:
                    buf_line_size = sys.getsizeof(buf_line)
                    buf_array_size = sys.getsizeof(buf_array)

                    if buf_line_size + buf_array_size > max_buf_size:
                        heap_sort(buf_array, cmp_func,
                                  field_splitter, is_numeric,
                                  sorted_fields_nums)

                        for line in buf_array:
                            write_line(line, ser_file)

                        buf_array = []
                        buf_array.append(buf_line)
                    else:
                        buf_array.append(buf_line)

        ser_file.seek(0)
        return ser_file

    def copy_ser(out_file, toFile, fileNum):
        nonlocal curr_line
        nonlocal next_line

        while True:
            curr_line = next_line
            next_line = read_line(out_file)

            if curr_line is not None:
                if next_line is not None:
                    if cmp_func(curr_line,
                                next_line, field_splitter,
                                is_numeric, sorted_fields_nums) <= 0:
                        write_line(curr_line, toFile)
                        last_elements[fileNum] = curr_line
                    else:
                        write_line(curr_line, toFile)
                        last_elements[fileNum] = curr_line
                        break

                else:
                    write_line(curr_line, toFile)
                    last_elements[fileNum] = curr_line
                    break

    def selecte_file():

        """This function calculate values a_1 ... a_n-1 (ideal_ser_count) and
        number of current output file (curr_out_file_num)
        after every level increase"""

        nonlocal curr_out_file_num
        nonlocal curr_level_num

        if empty_ser_count[curr_out_file_num] < \
           empty_ser_count[curr_out_file_num + 1]:
            curr_out_file_num += 1
        else:
            if empty_ser_count[curr_out_file_num] == 0:
                curr_level_num += 1
                selectedSeriesCount = ideal_ser_count[0]
                for i in range(num_of_files - 1):
                    empty_ser_count[i] = (selectedSeriesCount +
                                          ideal_ser_count[i + 1] -
                                          ideal_ser_count[i])

                    ideal_ser_count[i] = (selectedSeriesCount +
                                          ideal_ser_count[i + 1])

            curr_out_file_num = 0

        empty_ser_count[curr_out_file_num] -= 1

    def split_file(temp_ser_file):
        nonlocal curr_out_file_num
        while True:
            selecte_file()
            copy_ser(temp_ser_file, tmp_files[curr_out_file_num],
                     curr_out_file_num)
            if next_line is None or curr_out_file_num == num_of_files - 1:
                break

        while next_line is not None:
            selecte_file()
            if cmp_func(last_elements[curr_out_file_num],
                        next_line, field_splitter,
                        is_numeric, sorted_fields_nums) <= 0:

                copy_ser(temp_ser_file,
                         tmp_files[curr_out_file_num],
                         curr_out_file_num)

                if next_line is None:
                    empty_ser_count[selecte_file] += 1
                else:
                    copy_ser(temp_ser_file,
                             tmp_files[curr_out_file_num],
                             curr_out_file_num)

            else:
                copy_ser(temp_ser_file,
                         tmp_files[curr_out_file_num],
                         curr_out_file_num)

    def merge():
        def merge_one_series(index_map):
            local_index_map = []
            for i in index_map:
                if empty_ser_count[i] == 0:
                    local_index_map.append(i)

            while len(local_index_map) != 1:
                # Get minimal line
                min_line_num = local_index_map[0]
                min_line = last_elements[min_line_num]
                for i in local_index_map[:-1]:
                    peeked_line = last_elements[i]
                    if cmp_func(min_line, peeked_line,
                                field_splitter, is_numeric,
                                sorted_fields_nums) == 1:
                        min_line_num = i
                        min_line = peeked_line

                # Copy minimal line to output file
                selected_file = tmp_files[min_line_num]
                last_elements[min_line_num] = read_line(selected_file)

                write_line(min_line, tmp_files[local_index_map[-1]])

                if last_elements[min_line_num] is None:
                    local_index_map.remove(min_line_num)
                elif cmp_func(
                        min_line,
                        last_elements[min_line_num],
                        field_splitter,
                        is_numeric,
                        sorted_fields_nums) == 1:
                    local_index_map.remove(min_line_num)

        def toogle_file(index_map):
            # Get number of next out file
            next_out_file_num = 0
            for i in range(len(index_map)):
                if ideal_ser_count[index_map[i]] < \
                   ideal_ser_count[index_map[next_out_file_num]]:
                    next_out_file_num = i

            # Generate new index map with only one empty file
            is_was_zero = False
            old_index_map = index_map
            index_map = []
            for i in old_index_map:
                if ideal_ser_count[i] == 0 and not is_was_zero:
                    is_was_zero = True
                    index_map.append(i)
                elif ideal_ser_count[i] != 0:
                    index_map.append(i)

            min_id_count = ideal_ser_count[index_map[0]]
            min_id_count_num = index_map[0]
            for i in range(len(index_map)):
                if min_id_count > ideal_ser_count[index_map[i]] and \
                   ideal_ser_count[index_map[i]] != 0:
                    min_id_count_num = i

            # Set empty file last
            tmp = index_map[-1]
            index_map[-1] = index_map[next_out_file_num]
            index_map[next_out_file_num] = tmp

            tmp = index_map[-2]
            index_map[-2] = index_map[min_id_count_num]
            index_map[min_id_count_num] = tmp

            tmp_files[index_map[-1]].seek(0)
            tmp_files[index_map[-1]].truncate()

            return index_map

        def true_ser_count(file):
            prevl = read_line(file)
            nextl = read_line(file)
            true_count = 0
            while nextl is not None:
                if cmp_func(prevl, nextl, field_splitter,
                            is_numeric, sorted_fields_nums) == 1:
                    true_count += 1

                prevl = nextl
                nextl = read_line(file)

            return true_count

        nonlocal curr_level_num
        nonlocal last_elements

        # Rewind files and rewrite output file
        # Last number is always out, prelast is next out
        for i in tmp_files:
            i.seek(0)

        tmp_files[-1].truncate()

        last_elements = [read_line(tmp_files[i])
                         for i in range(num_of_files)]

        # Last number is always out, prelast is next out
        index_map = [i for i in range(num_of_files)]

        for lel in range(curr_level_num):
            print(ideal_ser_count)
            # Merge empty series
            count_files_with_empty_ser = 0
            is_merge_only_empty_ser = False
            while is_merge_only_empty_ser:
                count_files_with_empty_ser = 0
                for i in empty_ser_count:
                    if i > 0:
                        count_files_with_empty_ser += 1

                if count_files_with_empty_ser != num_of_files - 1:
                    is_merge_only_empty_ser = False
                else:
                    empty_ser_count[num_of_files - 1] += 1
                    ideal_ser_count[num_of_files - 1] += 1

                    for i in range(num_of_files - 1):
                        empty_ser_count[i] -= 1
                        ideal_ser_count[i] -= 1

            # Merge true series
            if ideal_ser_count[index_map[-2]] != 0:
                for i in range(ideal_ser_count[index_map[-2]]):
                    merge_one_series(index_map)

                    # Decrement count of ideal and empty series
                    ideal_ser_count[index_map[-1]] += 1
                    for i in range(len(index_map) - 1):
                        ideal_ser_count[index_map[i]] -= 1
                        if empty_ser_count[index_map[i]] > 0:
                            empty_ser_count[index_map[i]] -= 1

            if lel != curr_level_num - 1:
                # Check ser count in output file
                tmp_files[index_map[-1]].seek(0)
                ind = index_map[-1]
                empty_ser_count[ind] = (ideal_ser_count[ind] -
                                        true_ser_count(tmp_files[ind]) - 1)

                # Need to read line or on next iteration it will be None
                tmp_files[index_map[-1]].seek(0)
                out_num = index_map[-1]
                last_elements[out_num] = read_line(tmp_files[out_num])

                index_map = toogle_file(index_map)

    split_file(seriesed_file(orig_file_path, buf_size))
    print("kakak")
    merge()
    print("kakak")

    out_file_num = 0
    for i in range(len(ideal_ser_count)):
        if ideal_ser_count[i] == 1:
            out_file_num = i
            break

    if not file_out:
        tmp_files[out_file_num].seek(0)
        ready_line = read_line(tmp_files[out_file_num])
        while ready_line is not None:
            sys.stdout.write(ready_line)
            ready_line = read_line(tmp_files[out_file_num])
    else:
        with open(file_out, "w+") as outF:
            tmp_files[out_file_num].seek(0)
            ready_line = read_line(tmp_files[out_file_num])
            while ready_line is not None:
                write_out_line(ready_line, outF)
                ready_line = read_line(tmp_files[out_file_num])


def check_file(file_path, lines_splitter, field_splitter,
               is_numeric, sorted_fields_nums, cmp_func):
    with open(file_path, "r") as in_file:
        prev_line = read_source_line(in_file, lines_splitter)
        next_line = read_source_line(in_file, lines_splitter)
        breaked_line_number = 1
        while next_line is not None:
            cmpRes = cmp_func(prev_line, next_line,
                              field_splitter, is_numeric, sorted_fields_nums)
            breaked_line_number += 1
            if cmpRes == -1 or cmpRes == 0:
                prev_line = next_line
                next_line = read_source_line(in_file, lines_splitter)
            else:
                print("Unsorted file!!!")
                print(breaked_line_number)
                print(prev_line, next_line)
                break


def main():
    parser = argparse.ArgumentParser("Big File Sorter")

    parser.add_argument("--file", "-f", action="store",
                        nargs=1, type=str,
                        help=file_help)
    parser.add_argument("--out", "-o", action="store",
                        nargs=1, type=str,
                        help=out_help)
    parser.add_argument("--check", "-c", action="store_true",
                        help=check_help)
    parser.add_argument("--revert", "-r", action="store_true",
                        help=revert_help)
    parser.add_argument("--linessplitter", "-l", action="store",
                        nargs=1, type=str,
                        help=lines_splitter_help)
    parser.add_argument("--fieldsplitter", "-t", action="store",
                        nargs=1, type=str,
                        help=field_splitter_help)
    parser.add_argument("--keys", "-k", action="store",
                        nargs="+", type=int, required=False,
                        help=words_count_help)
    parser.add_argument("--numeric", "-n", action="store_true",
                        help=lines_splitter_help)

    args = parser.parse_args()

    is_numeric = False
    lines_splitter = "\n"
    field_splitter = "\t"
    file_path = None
    file_out = None
    keys = None
    cmp_func = str_compare

    if args.numeric:
        is_numeric = True

    isCheck = False
    if args.check:
        isCheck = True

    if args.keys:
        keys = tuple(int(args.keys[i]) for i in range(len(args.keys)))
        print(keys)

    if args.revert:
        cmp_func = revert_str_compare

    if args.linessplitter:
        lines_splitter = args.linessplitter[0].strip()

    if args.fieldsplitter:
        field_splitter = args.fieldsplitter[0].strip()

    if args.file:
        file_path = args.file[0].strip()
        if args.out:
            file_out = args.out[0].strip()
        else:
            file_out = "output.txt"
    else:
        file_out = False
        with open("tempFile.zzz", "w+") as stdBuf:
            while True:
                line = sys.stdin.readline()
                if line:
                    write_out_line(line, stdBuf)
                else:
                    break

        file_path = "tempFile.zzz"

    if not isCheck:
        polyphase_sort(file_path, file_out, lines_splitter,
                       field_splitter, is_numeric, 4000, 5, cmp_func, keys)
    else:
        check_file(file_path, lines_splitter, field_splitter,
                   is_numeric, None, cmp_func)


if __name__ == "__main__":
    sys.exit(main())
