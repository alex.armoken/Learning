#!/usr/bin/env python3

import sys


class Singleton(type):
    instance = None

    def __call__(cls, *args, **kwargs):
        if not cls.instance:
            cls.instance = super(Singleton, cls).__call__(*args, **kwargs)
        return cls.instance


class SimpleClass(object, metaclass=Singleton):
    def __init__(self, value):
        self.source = value


def main():
    aaa = SimpleClass(88005553575)
    bbb = SimpleClass(265)
    print(aaa.source, bbb.source)


if __name__ == "__main__":
    sys.exit(main())
