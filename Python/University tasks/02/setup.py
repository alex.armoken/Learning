import unittest
from setuptools import setup, find_packages
from os.path import join, dirname


def my_test_suite():
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover("superlab/tests", pattern="test_*.py")
    return test_suite


setup(
    name="superlab",
    version="0.95",
    include_package_data=True,
    test_suite="setup.my_test_suite",
    packages=find_packages(),
    long_description=open(join(dirname(__file__), "README.txt")).read(),
    entry_points={
        "console_scripts":
        ["superlab_bigfilegen = superlab.bigfilegen:main",
         "superlab_decor = superlab.decor:main",
         "superlab_dict = superlab.defdict:main",
         "superlab_fromjson = superlab.fromjson:main",
         "superlab_logger = superlab.logger:main",
         "superlab_meta = superlab.meta:main",
         "superlab_modelcreator = superlab.modelcreator:main",
         "superlab_polysort = superlab.polysort:main",
         "superlab_seqiter = superlab.seqiter:main",
         "superlab_singleton = superlab.singleton:main",
         "superlab_tojson = superlab.tojson:main",
         "superlab_vector = superlab.vector:main",
         "superlab_xrange = superlab.xrange:main"]
    }
)
