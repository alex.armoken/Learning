#include <algorithm>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <queue>
#include <vector>

using namespace std;

class Graph
{
    const int EDGE_LENGTH = 6;

    public:
        Graph(int n)
        {
            _adjacencyMatrix.resize(n);
            for (auto& row: _adjacencyMatrix)
            {
                row.resize(n, 0);
            }
        }

        void add_edge(int u, int v)
        {
            _adjacencyMatrix[u][v] = EDGE_LENGTH;
            _adjacencyMatrix[v][u] = EDGE_LENGTH;
        }

        vector<int> shortest_reach(int start)
        {
            std::vector<bool> visitedNodes(_adjacencyMatrix.size(), false);
            std::vector<int> lengths(_adjacencyMatrix.size(), -1);
            std::queue<int> queueOfNodes;

            queueOfNodes.push(start);
            lengths[start] = 0;
            visitedNodes[start] = true;

            while(not queueOfNodes.empty())
            {
                const int curNode = queueOfNodes.front();
                queueOfNodes.pop();

                const auto& adjacentNodes = _adjacencyMatrix[curNode];
                for (auto nodeNumber = 0; nodeNumber < adjacentNodes.size(); ++nodeNumber)
                {
                    if (not visitedNodes[nodeNumber] and adjacentNodes[nodeNumber])
                    {
                        visitedNodes[nodeNumber] = true;

                        const auto newPathLength = adjacentNodes[nodeNumber] + lengths[curNode];
                        lengths[nodeNumber] = newPathLength;

                        queueOfNodes.push(nodeNumber);
                    }
                }
            }

            return lengths;
        }

private:
    std::vector<std::vector<int>> _adjacencyMatrix;
};

int main() {
    int queries;
    cin >> queries;

    for (int t = 0; t < queries; t++) {

		int n, m;
        cin >> n;
        // Create a graph of size n where each edge weight is 6:
        Graph graph(n);
        cin >> m;
        // read and set edges
        for (int i = 0; i < m; i++) {
            int u, v;
            cin >> u >> v;
            u--, v--;
            // add each edge to the graph
            graph.add_edge(u, v);
        }
		int startId;
        cin >> startId;
        startId--;
        // Find shortest reach from node s
        vector<int> distances = graph.shortest_reach(startId);

        for (int i = 0; i < distances.size(); i++) {
            if (i != startId) {
                cout << distances[i] << " ";
            }
        }
        cout << endl;
    }

    return 0;
}
