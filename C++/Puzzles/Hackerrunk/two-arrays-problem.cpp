#include <vector>
#include <iostream>
#include <algorithm>
#include <numeric>


std::string twoArrays(int k, std::vector<int> A, std::vector<int> B)
{
    std::sort(A.begin(), A.end());
    std::sort(B.begin(), B.end(), std::greater<int>());

    for (auto i = 0; i < A.size(); ++i)
    {
        if (A[i] + B[i] < k)
        {
            return "NO";
        }
    }
    return "YES";
}


int main()
{
    int q;
    std::cin >> q;

    for (int a0 = 0; a0 < q; a0++)
    {
        int n;
        int k;
        std::cin >> n >> k;

        std::vector<int> A(n);
        for (int A_i = 0; A_i < n; A_i++)
        {
            std::cin >> A[A_i];
        }

        std::vector<int> B(n);
        for(int B_i = 0; B_i < n; B_i++)
        {
            std::cin >> B[B_i];
        }

        std::string result = twoArrays(k, A, B);
        std::cout << result << std::endl;
    }
    return 0;
}
