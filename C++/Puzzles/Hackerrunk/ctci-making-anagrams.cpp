#include <bits/stdc++.h>

using namespace std;



int main()
{
    string a;
    getline(cin, a);
    string b;
    getline(cin, b);

    std::map<char, int> a_map;
    for (auto ch: a) {
        auto it = a_map.find(ch);
        if (it == a_map.end()) {
            a_map.insert(std::make_pair(ch, 1));
        } else {
            it->second++;
        }
    }

    std::map<char, int> b_map;
    for (auto ch: b) {
        auto it = b_map.find(ch);
        if (it == b_map.end()) {
            b_map.insert(std::make_pair(ch, 1));
        } else {
            it->second++;
        }
    }

    auto result = 0;
    for (auto& charNumberPair: a_map) {
        auto charCountInB = 0;
        auto itToCharInB = b_map.find(charNumberPair.first);
        if (itToCharInB != b_map.end()) {
            charCountInB = itToCharInB->second;
        }

        result += std::abs(charCountInB - charNumberPair.second);
    }

    for (auto& charNumberPair: b_map) {
        auto itToCharInA = a_map.find(charNumberPair.first);
        if (itToCharInA == a_map.end()) {
            result += charNumberPair.second;
        }
    }

    std::cout << result << std::endl;

    return 0;
}
