#include <algorithm>
#include <vector>
#include <unordered_set>
#include <iostream>

std::vector<int> missingNumbers(std::vector<int> arr, std::vector<int> brr)
{
    std::unordered_set<int> counted;
    std::vector<int> result;
    for (auto el: brr)
    {
        if (counted.find(el) == counted.end())
        {
            auto aCount = std::count(arr.cbegin(), arr.cend(), el);
            auto bCount = std::count(brr.cbegin(), brr.cend(), el);
            if (bCount - aCount)
            {
                result.push_back(el);
            }
            counted.insert(el);
        }
    }

    std::sort(result.begin(), result.end());
    return result;
}
