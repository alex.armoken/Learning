#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>


bool isPrime(unsigned int number)
{
    if (number == 1) {
        return false;
    }

    for (unsigned int i = 2; i * i <= number; ++i) {
        if (number % i == 0) {
            return false;
        }
    }

    return true;
}


int main()
{
    unsigned int p;
    std::cin >> p;

    for (unsigned int i = 0; i < p; ++i) {
        unsigned int number;
        std::cin >> number;

        std::cout << (isPrime(number) ? "Prime" : "Not prime") << std::endl;
    }

    return 0;
}
