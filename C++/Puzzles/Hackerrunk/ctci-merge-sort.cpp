#include <bits/stdc++.h>

using namespace std;


std::pair<std::vector<int>, int> mergeWithInversionCount(
    std::vector<int> a,
    std::vector<int> b
)
{
    std::vector<int> resultArr;
    int resultInversionsCount = 0;

    auto aIt = a.begin();
    auto bIt = b.begin();
    while (aIt != a.end() && bIt != b.end()) {
        if (*aIt < *bIt) {
            resultArr.push_back(*aIt++);
        } else {
            resultInversionsCount++;
            resultArr.push_back(*bIt++);
        }
    }

    for (; aIt != a.end(); ++aIt) {
        resultArr.push_back(*aIt);
    }

    for (; bIt != b.end(); ++bIt) {
        resultArr.push_back(*bIt);
    }

    return std::make_pair(std::move(resultArr), resultInversionsCount);
}


// Complete the countInversions function below.
std::pair<std::vector<int>, int> sortAndCountInversions(std::vector<int> arr)
{
    if (arr.size() > 1) {
        const int middle = arr.size() / 2;
        std::vector<int> aArray(arr.begin(), arr.begin() + middle);
        std::vector<int> bArray(arr.begin() + middle + 1, arr.end());

        return mergeWithInversionCount(std::move(aArray), std::move(bArray));
    }

    return std::make_pair(std::move(arr), 0);
}


int countInversions(std::vector<int> arr)
{
    auto resultPair = sortAndCountInversions(std::move(arr));
    for (auto num: resultPair.first) {
        std::cout << num << " ";
    }
    std::cout << ":";

    return resultPair.second;
}


vector<string> split_string(string input_string)
{
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}


int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int t;
    cin >> t;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int t_itr = 0; t_itr < t; t_itr++) {
        int n;
        cin >> n;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        string arr_temp_temp;
        getline(cin, arr_temp_temp);

        vector<string> arr_temp = split_string(arr_temp_temp);

        vector<int> arr(n);

        for (int i = 0; i < n; i++) {
            int arr_item = stoi(arr_temp[i]);

            arr[i] = arr_item;
        }

        long result = countInversions(arr);

        fout << result << "\n";
    }

    fout.close();

    return 0;
}
