struct Node {
    int data;
    Node* left;
    Node* right;
};

bool isSubNodesGreater(Node* root, int value)
{
    const bool isLeftValid = root->left != nullptr
        ? value < root->left->data && isSubNodesGreater(root->left, value)
        : true;
    const bool isRightValid = root->right != nullptr
        ? value < root->right->data && isSubNodesGreater(root->right, value)
        : true;
    return isLeftValid && isRightValid;
}

bool isSubNodesLower(Node* root, int value)
{
    const bool isLeftValid = root->left != nullptr
        ? value > root->left->data && isSubNodesLower(root->left, value)
        : true;
    const bool isRightValid = root->right != nullptr
        ? value > root->right->data && isSubNodesLower(root->right, value)
        : true;
    return isLeftValid && isRightValid;
}

bool checkBST(Node* root)
{
    const bool isLeftValid = root->left != nullptr
        ? root->data > root->left->data
            && checkBST(root->left)
            && isSubNodesLower(root->left, root->data)
        : true;
    const bool isRightValid = root->right != nullptr
        ? root->data < root->right->data
            && checkBST(root->right)
            && isSubNodesGreater(root->right, root->data)
        : true;

    return isLeftValid && isRightValid;
}
