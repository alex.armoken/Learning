#include <iostream>


int fibonacci(const int n)
{
    if (n == 1) {
        return 1;
    } else if (n == 0) {
        return 0;
    }

    return fibonacci(n - 1) + fibonacci(n - 2);
}


int main()
{
    int n;
    std::cin >> n;
    std::cout << fibonacci(n);
    return 0;
}
