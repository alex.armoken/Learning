#include <bits/stdc++.h>
#include <unordered_set>

using namespace std;


long long int calcNumberOfWaysToMakeChange(
    const int numberOfDollars,
    const std::vector<int>& coins
)
{
    std::vector<std::pair<long long int, std::unordered_set<int>>> countOfWaysToMakeChange(numberOfDollars + 1);

    for (auto amountOfDollars = 1; amountOfDollars <= numberOfDollars; ++amountOfDollars) {
        countOfWaysToMakeChange[amountOfDollars].first = 0;

        const auto itToCoin = std::find(coins.cbegin(), coins.cend(), amountOfDollars);
        if (itToCoin != coins.cend()) {
            countOfWaysToMakeChange[amountOfDollars].first++;
        }

        for (auto coin: coins) {
            if (amountOfDollars - coin > 0) {
                if (countOfWaysToMakeChange[amountOfDollars].second.find(amountOfDollars - coin) == countOfWaysToMakeChange[amountOfDollars].second.end()
                    && countOfWaysToMakeChange[amountOfDollars].second.find(coin) == countOfWaysToMakeChange[amountOfDollars].second.end()) {
                    countOfWaysToMakeChange[amountOfDollars].first += countOfWaysToMakeChange[amountOfDollars - coin].first;
                    countOfWaysToMakeChange[amountOfDollars].second.insert(amountOfDollars - coin);
                    countOfWaysToMakeChange[amountOfDollars].second.insert(coin);
                }
            }
        }
    }

    return countOfWaysToMakeChange[numberOfDollars].first;
}


vector<string> split_string(string);
int main()
{
    string nm_temp;
    getline(cin, nm_temp);

    vector<string> nm = split_string(nm_temp);

    int n = stoi(nm[0]);

    int m = stoi(nm[1]);

    string coins_temp_temp;
    getline(cin, coins_temp_temp);

    vector<string> coins_temp = split_string(coins_temp_temp);

    vector<int> coins(m);

    for (int i = 0; i < m; i++) {
        int coins_item = stoi(coins_temp[i]);

        coins[i] = coins_item;
    }

    std::cout << calcNumberOfWaysToMakeChange(n, coins) << std::endl;

    return 0;
}


vector<string> split_string(string input_string) {
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}
