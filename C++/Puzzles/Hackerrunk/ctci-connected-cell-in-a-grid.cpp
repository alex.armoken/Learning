#include <bits/stdc++.h>

using namespace std;

struct Pos
{
    Pos(int x, int y)
        : x(x), y(y)
    {
    }

    int x;
    int y;
};


uint32_t findRegionSize(const Pos curPos, std::vector<std::vector<int>>& grid)
{
    uint32_t size = 0;

    if (curPos.x >= 0 && curPos.x < grid.size()
        && curPos.y >= 0 && curPos.y < grid.front().size()
        && grid[curPos.x][curPos.y])
    {
        size += 1;
        grid[curPos.x][curPos.y] = 0;

        for (auto i = -1; i <= 1; ++i)
        {
            for (auto j = -1; j <= 1; ++j)
            {
                size += findRegionSize(Pos(curPos.x + i, curPos.y + j), grid);
            }
        }
    }

    return size;
}


uint32_t findSizeOfMaxRegion(std::vector<std::vector<int>>& grid)
{
    uint32_t maxRegionSize =  0;

    for (auto rowIt = grid.begin(); rowIt != grid.end(); ++rowIt) {
        for (auto columnIt = rowIt->begin(); columnIt != rowIt->end(); ++columnIt) {
            if (*columnIt) {
                const Pos elPos( rowIt - grid.begin(), columnIt - rowIt->begin());
                const auto curRegionSize = findRegionSize(elPos, grid);

                maxRegionSize = (curRegionSize > maxRegionSize) ? curRegionSize : maxRegionSize;
            }
        }
    }

    return maxRegionSize;
}


int main()
{
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    int m;
    cin >> m;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    vector<vector<int>> grid(n);
    for (int i = 0; i < n; i++) {
        grid[i].resize(m);

        for (int j = 0; j < m; j++) {
            cin >> grid[i][j];
        }

        cin.ignore(numeric_limits<streamsize>::max(), '\n');

    }
    std::cout << findSizeOfMaxRegion(grid);

    return 0;
}
