struct Node
{
    int data;
    Node* next;
};


int findLength(Node* node)
{
    int length = 0;
    do
    {
        length++;
        node = node->next;
    } while(node->next != nullptr);

    return length;
}


int FindMergeNode(Node *headA, Node *headB)
{
    auto lengthA = findLength(headA);
    auto lengthB = findLength(headB);

    if (lengthA > lengthB)
    {
        for (auto i = 0; i < (lengthA - lengthB); ++i)
        {
            headA = headA->next;
        }
    }
    else if (lengthA < lengthB)
    {
        for (auto i = 0; i < (lengthB - lengthA); ++i)
        {
            headB = headB->next;
        }
    }

    while(true)
    {
        if (headA != headB)
        {
            headA = headA->next;
            headB = headB->next;
        }
        else
        {
            return headA->data;
        }
    }
}
