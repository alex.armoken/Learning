#include <bits/stdc++.h>
#include <unordered_map>

using namespace std;


class TrieNode final
{
public:
    void addNode(
        std::string::iterator itToChar,
        std::string::iterator endIt
    );

    unsigned int getValuesCount() const;

    unsigned int getCountOfWordsStartedWith(
        std::string::iterator itToChar,
        std::string::iterator endIt
    ) const;

private:
    std::map<char, TrieNode> _children;

    unsigned int _countOfStoredValues = 0;
};


void TrieNode::addNode(
    std::string::iterator itToChar,
    std::string::iterator endIt
)
{
    if (itToChar != endIt)
    {
        auto itToChildNode = _children.find(*itToChar);
        if (itToChildNode == _children.end()) {
            auto itToCharNodePair = _children.insert(
                std::make_pair(
                     *itToChar,
                     TrieNode()
                )
            );
            itToChildNode = itToCharNodePair.first;
        }

        itToChildNode->second.addNode(itToChar + 1, endIt);
    }
    _countOfStoredValues++;
}


unsigned int TrieNode::getValuesCount() const
{
    unsigned int countOfValues = 0;
    for (const auto& child: _children) {
        countOfValues += child.second.getValuesCount();
    }

    return countOfValues + _countOfStoredValues;
}


unsigned int TrieNode::getCountOfWordsStartedWith(
    std::string::iterator itToChar,
    std::string::iterator endIt
) const
{
    if (itToChar != endIt) {
        auto itToChildNode = _children.find(*itToChar);
        if (itToChildNode != _children.end()) {
            return itToChildNode->second.getCountOfWordsStartedWith(
                itToChar + 1,
                endIt
            );
        } else {
            return 0;
        }
    }

    return _countOfStoredValues;
}


vector<string> split_string(string input_string)
{
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}


int main()
{
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    TrieNode trie;
    for (int n_itr = 0; n_itr < n; n_itr++) {
        string opContact_temp;
        getline(cin, opContact_temp);

        vector<string> opContact = split_string(opContact_temp);

        string op = opContact[0];
        string contact = opContact[1];

        if (op == "add") {
            trie.addNode(contact.begin(), contact.end());
        } else {
            const auto count = trie.getCountOfWordsStartedWith(
                contact.begin(),
                contact.end()
            );
            std::cout << count << std::endl;
        }

    }

    return 0;
}
