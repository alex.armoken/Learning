 #include <bits/stdc++.h>


int main()
{
    int n;
    std::cin >> n;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::vector<int> minHeap;
    std::vector<int> maxHeap;
    float median = NAN;

    for (int i = 0; i < n; i++) {
        int a_item;
        std::cin >> a_item;
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

        if (a_item < median) {
            maxHeap.push_back(a_item);
            std::push_heap(maxHeap.begin(), maxHeap.end(), std::less<int>());
        } else {
            minHeap.push_back(a_item);
            std::push_heap(minHeap.begin(), minHeap.end(), std::greater<int>());
        }

        {
            const auto heapsSizeDelta = std::abs(static_cast<int>(minHeap.size() - maxHeap.size()));
            if (heapsSizeDelta > 1) {
                if (minHeap.size() > maxHeap.size()) {
                    for (auto _ = 0; _ < heapsSizeDelta - 1; ++_) {
                        maxHeap.push_back(minHeap.front());
                        std::push_heap(maxHeap.begin(), maxHeap.end(), std::less<int>());

                        std::pop_heap(minHeap.begin(), minHeap.end(), std::greater<int>());
                        minHeap.pop_back();
                    }
                } else {
                    for (auto _ = 0; _ < heapsSizeDelta - 1; ++_) {
                        minHeap.push_back(maxHeap.front());
                        std::push_heap(minHeap.begin(), minHeap.end(), std::greater<int>());

                        std::pop_heap(maxHeap.begin(), maxHeap.end(), std::less<int>());
                        maxHeap.pop_back();
                    }
                }
            }
        }

        if (minHeap.size() > maxHeap.size()) {
            median = minHeap.front();
        } else if (maxHeap.size() > minHeap.size()) {
            median = maxHeap.front();
        } else {
            median = (minHeap.front() + maxHeap.front()) / 2.0;
        }

        std::cout << std::setprecision(1) << std::fixed << median << std::endl;
    }

    return 0;
}
