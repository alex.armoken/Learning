#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

int countNumberOfWaysOfClimbing(int restOfSteps, int maxStepSize)
{
    static std::map<std::pair<int, int>, int> inputOutputMap;

    auto it = inputOutputMap.find(std::make_pair(restOfSteps, maxStepSize));
    if (it != inputOutputMap.end()) {
        return it->second;
    }

    if (restOfSteps == 0) {
        return 1;
    }

    int numberOfWaysOfClimbing = 0;
    for (auto i = 1; i <= maxStepSize; ++i) {
        const auto newRestOfSteps = restOfSteps - i;
        const auto maxPossibleStepSize = std::min(newRestOfSteps, maxStepSize);

        numberOfWaysOfClimbing += countNumberOfWaysOfClimbing(
            newRestOfSteps,
            maxPossibleStepSize
        );
    }

    inputOutputMap.insert(
        std::make_pair(
            std::make_pair(restOfSteps, maxStepSize),
            numberOfWaysOfClimbing
        )
    );

    return numberOfWaysOfClimbing;
}


int main()
{
    int s;
    cin >> s;
    for(int a0 = 0; a0 < s; a0++) {
        int n;
        cin >> n;
        std::cout << countNumberOfWaysOfClimbing(n, 3) << std::endl;
    }
    return 0;
}
