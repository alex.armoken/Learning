#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

using namespace std;

int lonely_integer(const vector<int>& a)
{
    std::bitset<2 * 100 + 2> isNonUnique;
    for (const auto number: a) {
        if (!isNonUnique[number * 2]) {
            isNonUnique.set(number * 2);
        } else {
            isNonUnique.set(number * 2 + 1);
        }
    }

    for (auto i = 0; i <= 100; ++i) {
        if (isNonUnique[i * 2] ^ isNonUnique[i * 2 + 1]) {
            return i;
        }
    }

    return 0;
}

int main()
{
    int n;
    cin >> n;
    vector<int> a(n);
    for(int a_i = 0;a_i < n;a_i++){
       cin >> a[a_i];
    }
    cout << lonely_integer(a) << endl;
    return 0;
}
