#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <stack>
#include <queue>
#include <cstdint>


class MyQueue
{
public:
    void push(uint64_t x)
    {
        stack_newest_on_top.push(x);
    }

    void moveValues()
    {
        uint64_t size = stack_newest_on_top.size();
        for (auto i = 0; i < size; ++i)
        {
            uint64_t value = stack_newest_on_top.top();
            stack_newest_on_top.pop();

            stack_oldest_on_top.push(value);
        }
    }

    void pop()
    {
        if (stack_oldest_on_top.empty())
        {
            moveValues();
        }

        stack_oldest_on_top.pop();
    }

    uint64_t front()
    {
        if (stack_oldest_on_top.empty())
        {
            moveValues();
        }

        return stack_oldest_on_top.top();
    }

private:
    std::stack<uint64_t> stack_newest_on_top;
    std::stack<uint64_t> stack_oldest_on_top;
};

int main()
{
    MyQueue q1;
    uint64_t q, type, x;
    std::cin >> q;

    for(uint64_t i = 0; i < q; i++)
    {
        std::cin >> type;
        if(type == 1)
        {
            std::cin >> x;
            q1.push(x);
        }
        else if(type == 2)
        {
            q1.pop();
        }
        else
        {
            std::cout << q1.front() << std::endl;
        }
    }
    return 0;
}
