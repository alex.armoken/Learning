#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>

std::set<unsigned int> eratosthenesSieveLinear(const unsigned int maxNumber)
{
    std::vector<bool> isPrime(maxNumber + 1, true);
    isPrime[0] = isPrime[1] = false;

    for (unsigned int i = 2; i < maxNumber; i += 2) {
        isPrime[i] = false;
    }
    isPrime[2] = true;

    for (unsigned int i = 2; i * i <= maxNumber; ++i) {
        if (isPrime[i]) {
            for (unsigned int j = i * i; j <= maxNumber; j += i) {
                isPrime[j] = false;
            }
        }
    }

    std::set<unsigned int> result;
    for (unsigned int i = 0; i < maxNumber + 1; ++i) {
        if (isPrime[i]) {
            result.insert(i);
        }
    }

    return result;
}

std::vector<bool> analyzeNumbersOnPrime(const std::vector<unsigned int>& numbers)
{
    const unsigned int maxNumber = *std::max_element(numbers.cbegin(), numbers.cend());

    std::vector<bool> isNumbersPrime;
    auto setOfPrimeNumbers = eratosthenesSieveLinear(maxNumber);
    for (auto number: numbers) {
        if (setOfPrimeNumbers.find(number) != setOfPrimeNumbers.end()) {
            isNumbersPrime.push_back(true);
        } else {
            isNumbersPrime.push_back(false);
        }
    }

    return isNumbersPrime;
}


int main()
{
    int p;
    std::cin >> p;

    std::vector<unsigned int> input;
    for (int a0 = 0; a0 < p; a0++) {
        unsigned int n;
        std::cin >> n;
        input.push_back(n);
    }

    auto isNumbersPrime = analyzeNumbersOnPrime(input);
    for (auto isPrime: isNumbersPrime) {
        std::cout << (isPrime ? "Prime" : "Not prime") << std::endl;
    }

    return 0;
}
