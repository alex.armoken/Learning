struct Node
{
    int data;
    Node *next;
    Node *prev;
};

#include <algorithm>
Node* Reverse(Node* head)
{
    Node* curNode = head;
    while (curNode != nullptr)
    {
        std::swap(curNode->next, curNode->prev);

        if (curNode->prev == nullptr)
        {
            break;
        }
        curNode = curNode->prev;
    }

    return curNode;
}
