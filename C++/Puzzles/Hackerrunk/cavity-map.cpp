#include <bits/stdc++.h>

using namespace std;

bool isDipper(const std::vector<std::string>& grid, int i, int j)
{
    if (j == 0
        || j == grid.front().size() - 1
        || i == 0
        || i == grid.size() - 1)
    {
        return false;
    }

    const auto curDepth = grid[i][j];

    const auto topDepth = grid[i - 1][j];
    const auto bottomDepth = grid[i + 1][j];
    const auto leftDepth = grid[i][j - 1];
    const auto rightDepth = grid[i][j + 1];

    return curDepth > topDepth
        && curDepth > bottomDepth
        && curDepth > leftDepth
        && curDepth > rightDepth;
}

// Complete the cavityMap function below.
vector<string> cavityMap(vector<string> grid) {
    std::vector<std::string> result = grid;

    for (auto i = 0; i < grid.size(); ++i)
    {
        for (auto j = 0; j < grid[i].size(); ++j)
        {
            if (isDipper(grid, i, j))
            {
                result[i][j] = 'X';
            }
        }
    }

    return result;
}

int main()
{
    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    vector<string> grid(n);

    for (int i = 0; i < n; i++) {
        string grid_item;
        getline(cin, grid_item);

        grid[i] = grid_item;
    }

    vector<string> result = cavityMap(grid);

    cout << endl;
    for (int i = 0; i < result.size(); i++) {
        cout << result[i];

        if (i != result.size() - 1) {
            cout << "\n";
        }
    }
    cout << "\n";

    return 0;
}
