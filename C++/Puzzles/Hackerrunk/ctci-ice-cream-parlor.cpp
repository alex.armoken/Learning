#include <bits/stdc++.h>

using namespace std;

vector<string> split_string(string);


std::pair<bool, int> binarySearch(const std::vector<std::pair<int, int>>& arr, int left, int right, int value, int excludedIndex)
{
    if (left > right) {
        return std::make_pair(false, 0);
    }

    const int mid = left + (right - left) / 2;
    if (arr[mid].second == value && arr[mid].first != excludedIndex) {
        return std::make_pair(true, mid);
    } else if (value < arr[mid].second) {
        return binarySearch(arr, left, mid - 1, value, excludedIndex);
    } else {
        return binarySearch(arr, mid + 1, right, value, excludedIndex);
    }
}


// Complete the solve function below.
void solve(vector<int> arr, int money) {
    std::vector<std::pair<int, int>> newArr(arr.size());
    for (auto i = 0; i < arr.size(); ++i) {
        newArr[i].first = i;
        newArr[i].second = arr[i];
    }
    std::sort(newArr.begin(), newArr.end(), [](std::pair<int, int> a, std::pair<int, int> b) { return a < b; });

    int firstFlavorId = 0;
    int secondFlavorId = 0;
    for (auto i = 0; i < arr.size(); ++i) {
        auto searchResult= binarySearch(newArr, 0, newArr.size(), money - arr[i], i);
        if (searchResult.first) {
            firstFlavorId = i;
            secondFlavorId = searchResult.second;
            break;
        }
    }

    if (firstFlavorId > secondFlavorId) {
        std::swap(firstFlavorId, secondFlavorId);
    }

    std::cout << firstFlavorId << " " << secondFlavorId << std::endl;
}

int main()
{
    int t;
    cin >> t;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    for (int t_itr = 0; t_itr < t; t_itr++) {
        int money;
        cin >> money;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        int n;
        cin >> n;
        cin.ignore(numeric_limits<streamsize>::max(), '\n');

        string arr_temp_temp;
        getline(cin, arr_temp_temp);

        vector<string> arr_temp = split_string(arr_temp_temp);

        vector<int> arr(n);

        for (int i = 0; i < n; i++) {
            int arr_item = stoi(arr_temp[i]);

            arr[i] = arr_item;
        }

        solve(arr, money);
    }

    return 0;
}

vector<string> split_string(string input_string) {
    string::iterator new_end = unique(input_string.begin(), input_string.end(), [] (const char &x, const char &y) {
        return x == y and x == ' ';
    });

    input_string.erase(new_end, input_string.end());

    while (input_string[input_string.length() - 1] == ' ') {
        input_string.pop_back();
    }

    vector<string> splits;
    char delimiter = ' ';

    size_t i = 0;
    size_t pos = input_string.find(delimiter);

    while (pos != string::npos) {
        splits.push_back(input_string.substr(i, pos - i));

        i = pos + 1;
        pos = input_string.find(delimiter, i);
    }

    splits.push_back(input_string.substr(i, min(pos, input_string.length()) - i + 1));

    return splits;
}
