#pragma once

#include <string>
#include <vector>

namespace leaderboard
{
    class Player final
    {
    public:
        Player(
            std::string id,
            std::string name,
            std::string urlToPhoto
        ) : id(id), name(name), urlToPhoto(urlToPhoto)
        {
        };

        ~Player() = default;

        void setId(std::string id)
        {
            this->id = id;
        }

        std::string getId() const
            {
                return this->id;
            }

        void setName(std::string name)
            {
                this->name = name;
            }

        std::string getName() const
            {
                return this->name;
            }


        void setUrlToPhoto(std::string urlToPhoto)
            {
                this->urlToPhoto = urlToPhoto;
            }

        std::string getUrlToPhoto() const
            {
                return this->urlToPhoto;
            }

    private:
        std::string id;
        std::string name;
        std::string urlToPhoto;
    };

    class Entry final
    {
    public:
        Entry(
            uint32_t posIndex,
            uint32_t score,
            Player player
            ) : posIndex(posIndex), score(score), player(player)
            {

            }

        ~Entry() = default;


        void setPosIndex(uint32_t posIndex)
            {
                this->posIndex = posIndex;
            }

        uint32_t getPosIndex() const
            {
                return this->posIndex;
            }

        void setScore(uint32_t score)
            {
                this->score = score;
            }

        uint32_t getScore() const
            {
                return this->score;
            }

        void setPlayer(Player player)
            {
                this->player = player;
            }

        Player getPlayer() const
            {
                return this->player;
            }

    private:
        uint32_t posIndex;
        uint32_t score;

        Player player;
    };

    void printPlayerConst(const Player& player);
    void printPlayer(Player& player);
    void printEntryConst(const Entry& entry);
    void printEntry(Entry& entry);
    void printEntriesVectorConst(const std::vector<Entry>& entries);
    void printEntriesVector(std::vector<Entry>& entries);
}
