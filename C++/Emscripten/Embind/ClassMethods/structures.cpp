#include "structures.hpp"

#include <iostream>

#include <emscripten/bind.h>


namespace leaderboard
{
    void printPlayerConst(const Player& player)
    {
        std::cout << "Start printing const player"
                  << player.getId()
                  << player.getName()
                  << player.getUrlToPhoto()
                  << std::endl;
    }


    void printPlayer(Player& player)
    {
        Player ownPlayer = std::move(player);
        std::cout << "Start printing player"
                  << ownPlayer.getId()
                  << ownPlayer.getName()
                  << ownPlayer.getUrlToPhoto()
                  << std::endl;
    }


    void printEntryConst(const Entry& entry)
    {
        std::cout << "Start printing const entry"
                  << entry.getScore()
                  << entry.getPosIndex()
                  << std::endl;
    }


    void printEntry(Entry& entry)
    {
        Entry ownEntry = std::move(entry);
        std::cout << "Start printing entry"
                  << ownEntry.getScore()
                  << ownEntry.getPosIndex()
                  << std::endl;
    }

    void printEntriesVectorConst(const std::vector<Entry>& entries)
    {
        std::cout << "Start printing const entries vector" << std::endl;
        for(const auto entry: entries)
        {
            std::cout << entry.getScore() << entry.getPosIndex() << std::endl;
        }
        std::cout << "Stop printing const entries vector" << std::endl;
    }


    void printEntriesVector(std::vector<Entry>& entries)
    {
        const std::vector<Entry>& ownEntries = std::move(entries);

        std::cout << "Start printing entries vector" << std::endl;
        for(const auto entry: ownEntries)
        {
            std::cout << entry.getScore() << entry.getPosIndex() << std::endl;
        }
        std::cout << "Stop printing entries vector" << std::endl;
    }
}


EMSCRIPTEN_BINDINGS(my_module)
{
    emscripten::class_<leaderboard::Player>("LeaderboardPlayer")
        .constructor<std::string, std::string, std::string>()
        .property("id", &leaderboard::Player::getId, &leaderboard::Player::setId)
        .property("name", &leaderboard::Player::getName, &leaderboard::Player::setName)
        .property("urlToPhoto", &leaderboard::Player::getUrlToPhoto, &leaderboard::Player::setUrlToPhoto)
        ;

    emscripten::class_<leaderboard::Entry>("LeaderboardEntry")
        .constructor<uint32_t, uint32_t, leaderboard::Player>()
        .property("posIndex", &leaderboard::Entry::getPosIndex, &leaderboard::Entry::setPosIndex)
        .property("score", &leaderboard::Entry::getScore, &leaderboard::Entry::setScore)
        .property("player", &leaderboard::Entry::getPlayer, &leaderboard::Entry::setPlayer)
        ;

    emscripten::register_vector<leaderboard::Entry>("LeaderboardEntriesVector");

    emscripten::function("printPlayer", leaderboard::printPlayer);
    emscripten::function("printPlayerConst", leaderboard::printPlayerConst);

    emscripten::function("printEntry", leaderboard::printEntry);
    emscripten::function("printEntryConst", leaderboard::printEntryConst);

    emscripten::function("printEntriesVector", leaderboard::printEntriesVector);
    emscripten::function("printEntriesVectorConst", leaderboard::printEntriesVectorConst);
}
