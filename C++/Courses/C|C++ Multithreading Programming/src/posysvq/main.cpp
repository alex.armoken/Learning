#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>

#define MT_POSIX

#if defined(MT_POSIX)
#include <mqueue.h>
#else
#include <sys/msg.h>
#include <sys/ipc.h>
#include <sys/types.h>
#endif

// #define MT_TEST

#if defined(MT_POSIX)
std::pair<std::size_t, bool> getQueueMsgSize(const mqd_t queueID)
{
  mq_attr attr;
  if (mq_getattr(queueID, &attr) == -1)
  {
    std::cerr << "Can't get POSIX queue size!" << std::endl;
    return std::make_pair(0, false);
  }

  return std::make_pair(attr.mq_msgsize, true);
}

int posix(const std::string& outPath)
{
  const auto queueID = mq_open("/test.mq", O_RDONLY | O_CREAT, S_IRUSR | S_IRUSR, nullptr);
  if (queueID == -1)
  {
    std::cerr << "Can't get or create POSIX queue!" << std::endl;
    return 1;
  }

  const auto maybeMsgSize = getQueueMsgSize(queueID);
  if (!maybeMsgSize.second)
  {
    return 2;
  }

  while (true)
  {
    std::vector<char> buf(maybeMsgSize.first, '\0');
    unsigned int priority = 0;
    const auto resultMsgSize = mq_receive(queueID, buf.data(), maybeMsgSize.first, &priority);
    if (resultMsgSize == -1)
    {
      std::cerr << "Can't receive message from POSIX queue!" << std::endl;
      return 3;
    }

    std::fstream outputFile(outPath, std::ios::out | std::ios::trunc);
    outputFile.write(buf.data(), resultMsgSize);
  }

  if (mq_unlink("./test.mq") == -1)
  {
    std::cerr << "Can't delete POSIX queue!" << std::endl;
    return 4;
  }

  return 0;
}
#else
struct message
{
  long mtype;
  char mtext[80];
};

int sysv(const std::string& outPath)
{
  const key_t queueKey = ftok("/tmp/msg.temp", 1);
  const auto MSG_PERMISSION = 0666;
  const auto queueID = msgget(queueKey, IPC_CREAT | MSG_PERMISSION);
  if (queueID == -1)
  {
    std::cerr << "Can't get or create SysV queue!" << std::endl;
    return 1;
  }

  while (true)
  {
    message msg;
    const auto resultMsgSize = msgrcv(queueID, &msg, sizeof(message), 0, 0);
    if (resultMsgSize == -1)
    {
      std::cerr << "Error occured during reading from SysV queue!" << std::endl;
      return 2;
    }
    std::cout << "Message recevied: " << msg.mtext << std::endl;

    std::fstream outputFile(outPath, std::ios::out | std::ios::trunc);
    outputFile.write(msg.mtext, resultMsgSize);
  }

  return 0;
}
#endif

int main(const int, char**)
{
  const std::string outPath =
#if defined(MT_TEST)
    "./message.txt"
#else
    "/home/box/message.txt"
#endif
    ;
#if defined(MT_POSIX)
  return posix(outPath);
#else
  return sysv(outPath);
#endif
}
