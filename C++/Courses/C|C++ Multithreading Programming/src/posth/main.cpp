#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <chrono>
#include <unistd.h>

#include <pthread.h>

// #define MT_TEST

void* threadWorkFunc(void* const)
{
  while (true)
  {
    usleep(100); // Microseconds
  }

  return nullptr;
}


int main(const int, char**)
{
  const std::string pathToOutPidFile =
#if defined(MT_TEST)
    "./main.pid"
#else
    "/home/box/main.pid"
#endif
    ;

  pthread_t threadID = 0;
  if (pthread_create(&threadID, nullptr, threadWorkFunc, nullptr) != 0)
  {
    std::cerr << "Can't create POSIX thread!" << std::endl;
    return 1;
  }

  {
    std::fstream outPidStream(pathToOutPidFile, std::ios::out | std::ios::trunc);
    outPidStream << std::to_string(getpid()) << std::endl;
  }

  if (pthread_join(threadID, nullptr) != 0)
  {
    std::cerr << "Can't successfully join POSIX thread!" << std::endl;
    return 2;
  }

  return 0;
}
