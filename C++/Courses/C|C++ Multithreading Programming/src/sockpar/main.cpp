#include <iostream>
#include <unistd.h>
#include <thread>
#include <cassert>
#include <chrono>
#include <climits>
#include <fstream>
#include <sstream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>

#define MT_TEST

int main(const int, char**)
{
  int sockfd[2];
  if (socketpair(AF_UNIX, SOCK_STREAM, 0, sockfd) != 0)
  {
    std::cerr << "Can't create socket pair!" << std::endl;
    return 1;
  }

  const int maybePid = fork();
  if (maybePid < 0)
  {
    std::cerr << "Can't make fork!" << std::endl;
    return 2;
  }
  else if (maybePid == 0)
  {
    close(sockfd[0]);
  }
  else
  {
    close(sockfd[1]);
  }

  while (true)
  {
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
  }

  return 0;
}
