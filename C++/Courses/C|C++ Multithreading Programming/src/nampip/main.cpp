#include <iostream>
#include <unistd.h>
#include <vector>
#include <cassert>
#include <climits>
#include <fstream>
#include <sstream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#define MT_TEST

int main(const int, char**)
{
  const std::string inPath =
#if defined(MT_TEST)
    "./in.fifo"
#else
    "/home/box/in.fifo"
#endif
    ;

  const std::string outPath =
#if defined(MT_TEST)
    "./out.fifo"
#else
    "/home/box/out.fifo"
#endif
    ;

  if (access(inPath.c_str(), F_OK) == 0)
  {
    unlink(inPath.c_str());
  }
  if (mkfifo(inPath.c_str(), 0666) != 0)
  {
    std::cerr << "Error occured during creating of input named pipe!" << std::endl;
    return 1;
  }

  if (mkfifo(outPath.c_str(), 0666) != 0)
  {
    std::cerr << "Error occured during creating of output named pipe!" << std::endl;
    return 1;
  }
  if (access(outPath.c_str(), F_OK) == 0)
  {
    unlink(outPath.c_str());
  }

  const auto readSize = 256;
  std::vector<char> buf(readSize + 1, 0);

  const auto in = open(inPath.c_str(), O_RDONLY);
  const auto out = open(outPath.c_str(), O_WRONLY);

  while (true)
  {
    const auto bytesRead = read(in, buf.data(), readSize);
    if (bytesRead < 0)
    {
      std::cerr << "Error during reading from named pipe!" << std::endl;
      return 2;
    }
    else if (bytesRead == 0)
    {
      std::cout << "Empty input named pipe!" << std::endl;
      return 0;
    }
    else
    {
      const auto bytesWritten = write(out, buf.data(), bytesRead);
      if (bytesWritten != -1)
      {
        std::cerr << "Error during writing to named pipe!" << std::endl;
        return 3;
      }
    }
  }

  close(in);
  close(out);

  return 0;
}
