#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <regex>
#include <sstream>
#include <type_traits>
#include <exception>
#include <stdint.h>
#include <unordered_map>

static inline void ltrim(std::string &s)
{
  s.erase(s.begin(),
          std::find_if(s.begin(), s.end(),
                       [](unsigned char ch)
                       {
                         return !std::isspace(ch);
                       }));
}

static inline void rtrim(std::string &s)
{
  s.erase(std::find_if(s.rbegin(), s.rend(),
                       [](unsigned char ch) {
                         return !std::isspace(ch);
                       }).base(),
          s.end());
}

static inline void trim(std::string &s)
{
  ltrim(s);
  rtrim(s);
}

static inline std::string ltrim_copy(std::string s)
{
  ltrim(s);
  return s;
}

static inline std::string rtrim_copy(std::string s)
{
  rtrim(s);
  return s;
}

static inline std::string trim_copy(std::string s)
{
  trim(s);
  return s;
}

std::vector<std::string> split(const std::string& text, const char sep)
{
  std::vector<std::string> tokens;
  std::size_t start = 0;
  std::size_t end = 0;
  while ((end = text.find(sep, start)) != std::string::npos)
  {
    tokens.push_back(text.substr(start, end - start));
    start = end + 1;
  }
  tokens.push_back(text.substr(start));

  return tokens;
}

std::vector<std::string> parse(const char* const args)
{
  auto splited_tokens = split(args, ',');
  for (auto& token: splited_tokens)
  {
    trim(token);
  }

  return splited_tokens;
}

template<typename T, typename ...Ts>
std::map<T, std::string> make_map(const char* const text, Ts... args)
{
  std::vector<T> keys{args...};
  std::vector<std::string> vals = parse(text);

  auto k = keys.cbegin();
  auto v = vals.cbegin();
  std::map<T, std::string> r;
  for (; k != keys.cend(); k++, v++)
  {
    r.emplace(*k, *v);
  }

  return r;
}

#define ENUM(name, ...)                                                                         \
namespace name##_details                                                                        \
{                                                                                               \
enum name                                                                                       \
{                                                                                               \
  __VA_ARGS__                                                                                   \
};                                                                                              \
}                                                                                               \
using name = name##_details::name;                                                              \
                                                                                                \
static std::string to_string(const name v)                                                      \
{                                                                                               \
  using namespace name##_details;                                                               \
  static std::map<name, std::string> m = make_map<name>(#__VA_ARGS__, __VA_ARGS__);             \
                                                                                                \
  return m.at(v);                                                                               \
}

enum class NumberSign
{
  Positive,
  Negative
};

using Power = int32_t;
using Multiplier = int32_t;
using Monomial = std::pair<Power, Multiplier>;
using Polynomial = std::vector<Monomial>;

class MonomialBuilder final
{
public:
  using BuildHandler = std::function<void(Monomial&&)>;
  MonomialBuilder(const BuildHandler buildHandler)
  {
    buildHandler_m = buildHandler;
  }

  ~MonomialBuilder() = default;

  MonomialBuilder(const MonomialBuilder&) = delete;
  MonomialBuilder(MonomialBuilder&&) = delete;

  bool withPower(const Power power)
  {
    if (powerFound_m)
    {
      return false;
    }

    power_m = power;
    powerFound_m = true;

    return true;
  }

  bool withVariable()
  {
    if (variableFound_m)
    {
      return false;
    }

    power_m = 1;
    variableFound_m = true;

    return true;
  }

  bool withPowerNumberSign(const NumberSign numberSign)
  {
    if (powerNumberSignFound_m)
    {
      return false;
    }

    if (numberSign == NumberSign::Negative)
    {
      isNegativePower_m = true;
    }
    else
    {
      isNegativePower_m = false;
    }
    powerNumberSignFound_m = true;

    return true;
  }

  bool withMultiplier(const Multiplier multiplier)
  {
    if (multiplierFound_m)
    {
      return false;
    }

    multiplier_m = multiplier;
    multiplierFound_m = true;

    return true;
  }

  bool withMultiplierSign(const NumberSign numberSign)
  {
    if (multiplierSignFound_m)
    {
      return false;
    }

    if (numberSign == NumberSign::Negative)
    {
      isNegativeMultiplier_m = true;
    }
    else
    {
      isNegativeMultiplier_m = false;
    }
    multiplierSignFound_m = true;

    return true;
  }

  void build()
  {
    buildHandler_m(
      std::make_pair(
        isNegativeMultiplier_m ? -1 * multiplier_m : multiplier_m,
        isNegativePower_m ? -1 * power_m : power_m
      )
    );
    reset();
  }

  bool somethingPassed() const
  {
    return powerFound_m
      || powerNumberSignFound_m
      || multiplierFound_m
      || multiplierSignFound_m
      || variableFound_m;
  }

private:
  Power power_m = 0;
  bool powerFound_m = false;

  bool isNegativePower_m = false;
  bool powerNumberSignFound_m = false;

  Multiplier multiplier_m = 1;
  bool multiplierFound_m = false;

  bool isNegativeMultiplier_m = false;
  bool multiplierSignFound_m = false;

  bool variableFound_m = false;

  BuildHandler buildHandler_m;

  void reset()
  {
    power_m = 0;
    powerFound_m = false;

    isNegativePower_m = false;
    powerNumberSignFound_m = false;

    multiplier_m = 1;
    multiplierFound_m = false;

    isNegativeMultiplier_m = false;
    multiplierSignFound_m = false;

    variableFound_m = false;
  }
};

ENUM(ParseState,
     // Values
     Start,
     MultiplierSign,
     MultiplierNumber,
     MultiplicationSign,
     Variable,
     PowerSign,
     PowerNumberSign,
     PowerNumber,
     Error);

std::vector<std::string> tokenize(const std::string& polynomial)
{
  std::vector<std::string> result;
  const std::regex tokensRegexp(R"(([0-9]+)|(\+)|(\-)|(\^)|(\*)|(x))", std::regex::ECMAScript);

  std::for_each(
    std::regex_iterator<std::string::const_iterator>(polynomial.cbegin(), polynomial.cend(), tokensRegexp),
    std::regex_iterator<std::string::const_iterator>(),
    [&result](const std::match_results<std::string::const_iterator>& it)
    {
      if (!it.empty())
      {
        result.emplace_back(it.str());
      }
      else
      {
        throw std::runtime_error("Invalid token!" + it.str());
      }
    }
  );

  return result;
}

using Token = std::string;
using StateSwitchFunc = std::function<ParseState(const Token&, MonomialBuilder&)>;

ParseState fromStartState(const Token& token, MonomialBuilder& builder)
{
  if (token == "-")
  {
    builder.withMultiplierSign(NumberSign::Negative);
    return ParseState::MultiplierSign;
  }
  else if (token == "+")
  {
    builder.withMultiplierSign(NumberSign::Positive);
    return ParseState::MultiplierSign;
  }
  else if (token == "x")
  {
    builder.withVariable();
    return ParseState::Variable;
  }

  try
  {
    const auto number = std::stoull(token);
    builder.withMultiplier(number);

    return ParseState::MultiplierNumber;
  }
  catch (const std::invalid_argument&)
  {
    return ParseState::Error;
  }
}

ParseState fromMultiplierSignState(const Token& token, MonomialBuilder& builder)
{
  if (token == "x")
  {
    builder.withVariable();
    return ParseState::Variable;
  }

  try
  {
    const auto number = std::stoull(token);
    builder.withMultiplier(number);

    return ParseState::MultiplierNumber;
  }
  catch (const std::invalid_argument&)
  {
    return ParseState::Error;
  }
}

ParseState fromMultiplierState(const Token& token, MonomialBuilder& builder)
{
  if (token == "*")
  {
    return ParseState::MultiplicationSign;
  }
  else if (token == "-" || token == "+")
  {
    builder.build();

    if (token == "-")
    {
      builder.withMultiplierSign(NumberSign::Negative);
    }
    else if (token == "+")
    {
      builder.withMultiplierSign(NumberSign::Positive);
    }

    return ParseState::Start;
  }

  return ParseState::Error;
}

ParseState fromMultiplicationSign(const Token& token, MonomialBuilder& builder)
{
  if (token == "x")
  {
    builder.withVariable();
    return ParseState::Variable;
  }

  return ParseState::Error;
}

ParseState fromVariableState(const Token& token, MonomialBuilder& builder)
{
  if (token == "^")
  {
    return ParseState::PowerSign;
  }
  else if (token == "-" || token == "+")
  {
    builder.build();

    if (token == "-")
    {
      builder.withMultiplierSign(NumberSign::Negative);
    }
    else if (token == "+")
    {
      builder.withMultiplierSign(NumberSign::Positive);
    }

    return ParseState::Start;
  }

  return ParseState::Error;
}

ParseState fromPowerSignState(const Token& token, MonomialBuilder& builder)
{
  if (token == "-")
  {
    builder.withPowerNumberSign(NumberSign::Negative);
    return ParseState::PowerNumberSign;
  }

  try
  {
    const auto number = std::stoull(token);
    builder.withPower(number);

    return ParseState::PowerNumber;
  }
  catch (const std::invalid_argument&)
  {
    return ParseState::Error;
  }
}

ParseState fromPowerNumberState(const Token& token, MonomialBuilder& builder)
{
  builder.build();

  if (token == "-")
  {
    builder.withMultiplierSign(NumberSign::Negative);

    return ParseState::MultiplierSign;
  }
  else if (token == "+")
  {
    builder.withMultiplierSign(NumberSign::Positive);

    return ParseState::MultiplierSign;
  }

  return ParseState::Error;
}

ParseState fromPowerNumberSignState(const Token& token, MonomialBuilder& builder)
{
  try
  {
    const auto number = std::stoull(token);
    builder.withPower(number);

    return ParseState::PowerNumber;
  }
  catch (const std::invalid_argument&)
  {
    return ParseState::Error;
  }
}

static const std::map<ParseState, StateSwitchFunc> StateSwitchFuncMap = {
  {ParseState::Start             , fromStartState},
  {ParseState::MultiplierSign    , fromMultiplierSignState},
  {ParseState::MultiplierNumber  , fromMultiplierState},
  {ParseState::MultiplicationSign, fromMultiplicationSign},
  {ParseState::Variable          , fromVariableState},
  {ParseState::PowerSign         , fromPowerSignState},
  {ParseState::PowerNumberSign   , fromPowerNumberSignState},
  {ParseState::PowerNumber       , fromPowerNumberState}
};

Polynomial parse(const std::string& polynomialStr)
{
  const auto tokens = tokenize(polynomialStr);

  Polynomial result;
  MonomialBuilder monomialBuilder(
    [&result](Monomial&& monomial)
    {
      result.emplace_back(std::move(monomial));
    }
  );

  ParseState curParseState = ParseState::Start;
  for (const auto& token: tokens)
  {
    std::cerr << "TOKEN: " << token << std::endl;
    const ParseState prevParseState = curParseState;
    curParseState = StateSwitchFuncMap.at(curParseState)(token, monomialBuilder);

    if (curParseState == ParseState::Error)
    {
      throw std::runtime_error(
        "Can't switch from " + to_string(prevParseState) + ". "
        + "Can't process token '" + token + "'."
      );
    }
  }
  if (monomialBuilder.somethingPassed()
      && (curParseState == ParseState::MultiplicationSign
          || curParseState == ParseState::PowerSign))
  {
    throw std::runtime_error("Can't process monomial. It is not fully constructed.");
  }
  else if (monomialBuilder.somethingPassed())
  {
    monomialBuilder.build();
  }

  return result;
}

std::string polynomialToStr(const Polynomial& polynomial)
{
  std::stringstream result;
  for (std::size_t i = 0; i < polynomial.size(); ++i)
  {
    const auto multiplier = polynomial[i].first;
    const auto power = polynomial[i].second;

    if (std::isless(multiplier, 0))
    {
      if (!(std::isgreaterequal(multiplier, 0) && std::islessequal(multiplier, 0)))
      {
        result << multiplier;
      }

      if (std::isgreaterequal(power, 1))
      {
        if (!(std::isgreaterequal(multiplier, 0) && std::islessequal(multiplier, 0)))
        {
          result << "*";
        }

        result << "x";
        if (std::isgreaterequal(power, 2))
        {
          result << "^" << power;
        }
      }
    }
    else
    {
      if (i != 0)
      {
        result << '+';
      }

      if (!(std::isgreaterequal(multiplier, 0) && std::islessequal(multiplier, 0)))
      {
        result << multiplier;
      }

      if (std::isgreaterequal(power, 1))
      {
        if (!(std::isgreaterequal(multiplier, 0) && std::islessequal(multiplier, 0)))
        {
          result << "*";
        }

        result << "x";
        if (std::isgreaterequal(power, 2))
        {
          result << "^" << power;
        }
      }
    }
  }
  return result.str();
}

bool containsMonomialOfPower(const Polynomial& polynomial, const Power power)
{
  for (std::size_t i = 0; i < polynomial.size(); ++i)
  {
    if (std::islessequal(power, polynomial[i].second)
        && std::isgreaterequal(power, polynomial[i].second))
    {
      return true;
    }
  }

  return false;
}

Polynomial reduction(const Polynomial& polynomial)
{
  Polynomial result;
  for (std::size_t i = 0; i < polynomial.size(); ++i)
  {
    const auto power = polynomial[i].second;
    if (containsMonomialOfPower(result, power))
    {
      continue;
    }

    auto resultMultiplier = polynomial[i].first;
    for (std::size_t j = 0; j < polynomial.size(); ++j)
    {
      if (i != j
          && std::islessequal(power, polynomial[j].second)
          && std::isgreaterequal(power, polynomial[j].second))
      {
        resultMultiplier += polynomial[j].first;
      }
    }

    result.emplace_back(resultMultiplier, power);
  }

  return result;
}

std::string derivative(const std::string& polynomialStr)
{
  const auto polynomial = parse(polynomialStr);

  Polynomial derivedPolynomial;
  for (const auto& powerMultiplierPair: polynomial)
  {
    const auto power = powerMultiplierPair.second;
    if (power == 0)
    {
      continue;
    }
    else
    {
      const auto multiplier = powerMultiplierPair.first;
      derivedPolynomial.emplace_back(multiplier * power, power - 1);
    }
  }

  derivedPolynomial = reduction(derivedPolynomial);
  std::sort(derivedPolynomial.begin(), derivedPolynomial.end(),
            [](const Monomial& monomialA, const Monomial& monomialB)
            {
              return monomialA.second > monomialB.second;
            });

  return polynomialToStr(derivedPolynomial);
}


int main(const int /* argc */, const char* const * const /* argv */)
{
  std::string inputString = "   10*x-  9as  *  x";

  std::cout << derivative(inputString) << std::endl;

  return 0;
}
