#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>

#define MT_POSIX

#if defined(MT_POSIX)
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/fcntl.h>
#else
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/types.h>
#endif

#if defined(MT_POSIX)
int posix(const std::string& shmemKey)
{
  if (shm_unlink(shmemKey.c_str()) != -1)
  {
    std::cerr << "Already exists POSIX shared memory removed!" << std::endl;
  }

  const auto shmemDescriptor = shm_open(shmemKey.c_str(), O_CREAT | O_RDWR, 0);
  if (shmemDescriptor == -1)
  {
    std::cerr << "Can't create POSIX shared memory!" << std::endl;
    return 1;
  }

  struct stat st;
  if (fstat(shmemDescriptor, &st) == -1)
  {
    std::cerr << "Can't get info about POSIX shared memory!" << std::endl;
    return 3;
  }
  std::cout << "Posix shared memory size in bytes: " << st.st_size << std::endl;

  const auto memorySize = 1024 * 1024; // 1 megabyte
  if (ftruncate(shmemDescriptor, memorySize) == -1)
  {
    std::cerr << "Can't resize POSIX shared memory!" << std::endl;
    return 4;
  }

  void* shmemPtr = mmap(nullptr, memorySize,
                        PROT_READ | PROT_WRITE,
                        MAP_SHARED,
                        shmemDescriptor,
                        0);
  if (shmemPtr == MAP_FAILED)
  {
    std::cerr << "Can't mmap POSIX shared memory!" << std::endl;
    return 5;
  }

  for (std::size_t i = 0; i < memorySize; ++i)
  {
    reinterpret_cast<char*>(shmemPtr)[i] = 13;
  }

  return 0;
}
#else
int sysv(const std::string& shmemFile)
{
  const auto semKey = ftok(shmemFile.c_str(), 1);
  const auto SEM_PERMISSION = 0666;
  const auto memorySize = 1024 * 1024 * 3; // 3 megabytes
  const auto shmemID = shmget(semKey, memorySize, IPC_CREAT | SEM_PERMISSION);
  if (shmemID == -1)
  {
    std::cerr << "Can't get or create SysV shared memory!" << std::endl;
    return 1;
  }

  void* shmemPtr = shmat(shmemID, nullptr, 0);
  if (shmemPtr == reinterpret_cast<void*>(-1))
  {
    std::cerr << "Can't connect SysV shared memory!" << std::endl;
    return 2;
  }

  const auto initMemSize = 1024 * 1024; // 1 megabyte
  for (std::size_t i = 0; i < initMemSize; ++i)
  {
    reinterpret_cast<char*>(shmemPtr)[i] = 42;
  }

  return 0;
}
#endif

int main(const int, char**)
{
  const std::string shmemKey =
#if defined(MT_POSIX)
    "/test.shm"
#else
    "/tmp/mem.temp"
#endif
    ;
#if defined(MT_POSIX)
  return posix(shmemKey);
#else
  return sysv(shmemKey);
#endif
}
