#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>

#define MT_POSIX

#if defined(MT_POSIX)
#include <semaphore.h>
#include <sys/fcntl.h>
#else
#include <sys/sem.h>
#include <sys/ipc.h>
#include <sys/types.h>
#endif

#if defined(MT_POSIX)
int posix(const std::string& semKey)
{
  const auto initValue = 66;
  auto* semaphore = sem_open(semKey.c_str(), O_CREAT, O_RDWR, initValue);
  if (semaphore == SEM_FAILED)
  {
    std::cerr << "Can't create POSIX semaphore!" << std::endl;
    return 1;
  }

  return 0;
}
#else
union semun
{
  int val;
  struct semid_ds* buf;
  unsigned short* array;
};

int sysv(const std::string& semFile)
{
  const auto semKey = ftok(semFile.c_str(), 1);
  const auto SEM_PERMISSION = 0666;
  const auto semCount = 16;
  const auto semID = semget(semKey, semCount, IPC_CREAT | SEM_PERMISSION);
  if (semID == -1)
  {
    std::cerr << "Can't get or create SysV semaphores!" << std::endl;
    return 1;
  }

  union semun arg;
  arg.array = new unsigned short[semCount];
  for (auto i = 0; i < semCount; ++i)
  {
    arg.array[i] = i;
  }

  if (semctl(semID, 0, SETALL, arg) == -1)
  {
    std::cerr << "Can't setup SysV semaphores!" << std::endl;
    delete[] arg.array;

    return 2;
  }
  delete[] arg.array;

  return 0;
}
#endif

int main(const int, char**)
{
  const std::string semKey =
#if defined(MT_POSIX)
    "/test.sem"
#else
    "/tmp/sem.temp"
#endif
    ;
#if defined(MT_POSIX)
  return posix(semKey);
#else
  return sysv(semKey);
#endif
}
