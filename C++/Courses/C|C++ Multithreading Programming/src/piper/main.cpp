#include <iostream>
#include <unistd.h>
#include <vector>
#include <cassert>
#include <climits>
#include <fstream>
#include <sstream>
#include <thread>
#include <chrono>
#include <sys/wait.h>


class NamedPipe
{
public:
  NamedPipe(const NamedPipe&) = default;
  NamedPipe(NamedPipe&&) = default;

  NamedPipe& operator=(const NamedPipe&) = default;
  NamedPipe& operator=(NamedPipe&&) = default;

  ~NamedPipe() = default;

  static NamedPipe create()
  {
    int fd[2];
    const auto pipeStatus = pipe(fd);
    assert(pipeStatus == 0);

    NamedPipe result;
    result.inputDescriptor = fd[1];
    result.outputDescriptor = fd[0];

    return result;
  }

  int input() const { return inputDescriptor; }
  int output() const { return outputDescriptor; }

  void closeInput()
  {
    assert(inputDescriptor != -1);
    close(inputDescriptor);
    inputDescriptor = -1;
  }

  void closeOutput()
  {
    assert(outputDescriptor != -1);
    close(outputDescriptor);
    outputDescriptor = -1;
  }

private:
  int inputDescriptor = -1;
  int outputDescriptor = -1;

  NamedPipe() = default;
};

bool isEmptySubString(const std::string& str,
                      const std::size_t start,
                      const std::size_t end)
{
  for (auto i = start; i < end; ++i)
  {
    if (!std::isspace(str[i]))
    {
      return false;
    }
  }

  return true;
}

std::size_t skipSpaces(const std::string& str,
                       const std::size_t searchStart)
{
  if (searchStart >= str.size())
  {
    return str.npos;
  }

  auto curPos = searchStart;
  while (true)
  {
    if (std::isspace(str[curPos]))
    {
      if (curPos == str.size() - 1)
      {
        return str.npos;
      }

      curPos++;
    }
    else
    {
      return curPos;
    }
  }
}

std::size_t skipNotSpaces(const std::string& str,
                          const std::size_t searchStart,
                          const char stopChar)
{
  if (searchStart >= str.size())
  {
    return str.npos;
  }

  auto curPos = searchStart;
  while (true)
  {
    if (str[curPos] == stopChar)
    {
      return curPos;
    }
    else if (!std::isspace(str[curPos]))
    {
      if (curPos == str.size() - 1)
      {
        return str.npos;
      }

      curPos++;
    }
    else
    {
      return curPos;
    }
  }
}

std::vector<std::string> getSubcommandParts(const std::string& command,
                                            const std::size_t subCommandStart,
                                            const std::size_t subCommandEnd)
{
  std::vector<std::string> result;

  bool isEnd = false;
  auto partStart = skipSpaces(command, subCommandStart);
  do
  {
    auto partEnd = skipNotSpaces(command, partStart, '|');
    if (partEnd == subCommandEnd)
    {
      isEnd = true;
    }
    else if (partEnd == command.npos)
    {
      partEnd = command.size();
      isEnd = true;
    }
    else if (partEnd > subCommandEnd)
    {
      partEnd = command.rfind('|', partEnd);
      isEnd = true;
    }

    if (!isEmptySubString(command, partStart, partEnd))
    {
      result.emplace_back(command, partStart, partEnd - partStart);
    }
    partStart = partEnd + 1;
  } while (!isEnd);

  return result;
}

std::vector<std::vector<std::string>> splitCommand(const std::string& command)
{
  std::vector<std::vector<std::string>> result;

  bool isEnd = false;
  std::size_t subcommandStart = skipSpaces(command, 0);
  do
  {
    std::size_t subcommandEnd = command.find('|', subcommandStart);
    if (subcommandEnd == command.npos)
    {
      subcommandEnd = command.size();
      isEnd = true;
    }

    auto subcommand = getSubcommandParts(command, subcommandStart, subcommandEnd);
    if (!subcommand.empty())
    {
      result.emplace_back(subcommand);
    }
    subcommandStart = subcommandEnd + 1;
  } while (!isEnd);

  return result;
}

void saveSTDINToFile()
{
  const auto readSize = PIPE_BUF;
  std::vector<char> buf(readSize + 1, 0);
  std::ofstream outFile("./outfile.txt", std::ios::trunc);

  while (true)
  {
    const auto bytesRead = read(STDIN_FILENO, buf.data(), readSize);
    if (bytesRead < 0)
    {
      break;
    }
    else if (bytesRead == 0)
    {
      break;
    }
    else
    {
      outFile.write(buf.data(), bytesRead);
    }
  }
}


void executeSubCommand(const std::vector<std::string>& subcommand)
{
  std::vector<char*> cmdPartsPointers;
  for (auto& part: subcommand)
  {
    cmdPartsPointers.emplace_back(const_cast<char*>(part.c_str()));
  }
  cmdPartsPointers.emplace_back(nullptr);

  assert(execvp(cmdPartsPointers[0], cmdPartsPointers.data()) != -1);
}

void executeCommand(const std::vector<std::vector<std::string>>& subcommands,
                    const std::size_t subcommandIdx)
{
  auto pipe = NamedPipe::create();

  const auto maybePid = fork();
  if (maybePid == 0) // Child process
  {
    pipe.closeInput();
    assert(dup2(pipe.output(), STDIN_FILENO) != -1);
    pipe.closeOutput();

    if (subcommandIdx == subcommands.size() - 1)
    {
      saveSTDINToFile();
    }
    else
    {
      executeCommand(subcommands, subcommandIdx + 1);
    }
  }
  else if (maybePid > 0) // Original process
  {
    // Replace STDOUT by pipe's in
    pipe.closeOutput();
    assert(dup2(pipe.input(), STDOUT_FILENO) != -1);
    pipe.closeInput();

    executeSubCommand(subcommands[subcommandIdx]);
  }
  else
  {
    assert(false);
  }
}

int main(const int, char**)
{
  std::istreambuf_iterator<char> begin(std::cin), end;
  std::string input(begin, end);

  const auto subcommands = splitCommand(input);
  if (!subcommands.empty())
  {
    const auto maybePid = fork();
    if (maybePid == 0) // Child process
    {
      executeCommand(subcommands, 0);
    }
    else if (maybePid > 0)
    {
      // Wait for all childs
      int status = 0;
      waitpid(-1, &status, 0);
    }
    else
    {
      assert(false);
    }
  }

  return 0;
}
