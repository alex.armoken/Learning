#include <iostream>
#include <map>
#include <vector>
#include <cstring>
#include <assert.h>

class SmallAllocator
{
private:
  using BlockSize = std::size_t;
  using BlockOffset = std::size_t;

  struct AllocatedMemoryBlockInfo
  {
    BlockOffset offset = 0;
    BlockSize size = 0;

    AllocatedMemoryBlockInfo(const BlockOffset offset,
                             const BlockSize size)
      : offset(offset)
      , size(size)
    {}
  };

  struct FreeMemoryBlockInfo
  {
    BlockOffset offset = 0;
    char* pointer = nullptr;

    FreeMemoryBlockInfo(const BlockOffset offset,
                        char* const pointer)
      : offset(offset)
      , pointer(pointer)
    {}
  };

public:
  SmallAllocator()
    : memorySize_m(10485576)
    , memory_m(new char[memorySize_m]())
  {}

  ~SmallAllocator()
  {
    delete[] memory_m;
    memory_m = nullptr;

    memorySize_m = 0;
  }

  void* Alloc(const unsigned int size)
  {
    const auto itToFreeBlock = freeBlocks_m.find(size);
    if (itToFreeBlock == freeBlocks_m.end())
    {
      // Create new block
      return allocateBlockByMovingTheMemoryEdge(size);
    }

    // Reuse existing block
    auto* const result = static_cast<void*>(itToFreeBlock->second.pointer);
    allocatedBlocks_m.emplace(
      std::make_pair(
        result,
        AllocatedMemoryBlockInfo(itToFreeBlock->second.offset, size)
      )
    );

    return result;
  }

  void* ReAlloc(void* const pointer, const unsigned int size)
  {
    auto itToAllocatedBlock = allocatedBlocks_m.find(pointer);
    if (itToAllocatedBlock == allocatedBlocks_m.end())
    {
      std::cerr << "Trying to realloc unknown memory block!" << std::endl;
      return nullptr;
    }

    if (size == itToAllocatedBlock->second.size)
    {
      return pointer;
    }

    const auto itToFreeBlock = freeBlocks_m.find(size);
    if (itToFreeBlock == freeBlocks_m.end())
    {
      // Create new block
      auto* result = allocateBlockByMovingTheMemoryEdge(size);
      std::memcpy(result, pointer, itToAllocatedBlock->second.size);

      freeBlocks_m.emplace(
        std::make_pair(
          itToAllocatedBlock->second.size,
          FreeMemoryBlockInfo(itToAllocatedBlock->second.offset, static_cast<char*>(pointer))
        )
      );
      allocatedBlocks_m.erase(itToAllocatedBlock);

      return result;
    }

    // Reuse existing block
    auto* const result = static_cast<void*>(itToFreeBlock->second.pointer);
    std::memcpy(result, pointer, itToAllocatedBlock->second.size);
    allocatedBlocks_m.emplace(
      std::make_pair(
        result,
        AllocatedMemoryBlockInfo(itToFreeBlock->second.offset, size)
      )
    );

    allocatedBlocks_m.erase(itToAllocatedBlock);
    freeBlocks_m.erase(itToFreeBlock);

    return result;
  }

  void Free(void* pointer)
  {
    auto it = allocatedBlocks_m.find(pointer);
    if (it == allocatedBlocks_m.end())
    {
      std::cerr << "Trying to free unknown memory block!" << std::endl;
      return;
    }

    freeBlocks_m.emplace(
      std::make_pair(
        it->second.size,
        FreeMemoryBlockInfo(it->second.offset, static_cast<char*>(pointer))
      )
    );
    allocatedBlocks_m.erase(it->first);
  }

private:
  std::size_t memorySize_m = 0;
  char* memory_m = nullptr;
  std::size_t edgeOffset_m = 0;

  std::map<void*, AllocatedMemoryBlockInfo> allocatedBlocks_m;
  std::multimap<BlockSize, FreeMemoryBlockInfo> freeBlocks_m;

  void* allocateBlockByMovingTheMemoryEdge(unsigned int size)
  {
    if (edgeOffset_m + size >= memorySize_m)
    {
      throw std::bad_alloc();
    }

    allocatedBlocks_m.emplace(
      std::make_pair(
        memory_m + edgeOffset_m,
        AllocatedMemoryBlockInfo(edgeOffset_m, size)
      )
    );

    auto* const result = static_cast<void*>(memory_m + edgeOffset_m);
    edgeOffset_m += size;

    return result;
  }
};


void test1()
{
  {
    SmallAllocator A1;
    auto* A1_P1 = static_cast<int*>(A1.Alloc(sizeof(int)));
    A1_P1 = static_cast<int*>(A1.ReAlloc(A1_P1, 2 * sizeof(int)));
    A1.Free(A1_P1);
  }

  {
    SmallAllocator A2;
    auto* A2_P1 = static_cast<int*>(A2.Alloc(10 * sizeof(int)));
    for (unsigned int i = 0; i < 10; i++)
    {
      A2_P1[i] = i;
    }
    for (unsigned int i = 0; i < 10; i++)
    {
      if (A2_P1[i] != static_cast<int>(i))
      {
        std::cout << "ERROR 1" << std::endl;
      }
    }

    auto* A2_P2 = static_cast<int*>(A2.Alloc(10 * sizeof(int)));
    for (unsigned int i = 0; i < 10; i++)
    {
      A2_P2[i] = -1;
    }
    for (unsigned int i = 0; i < 10; i++)
    {
      if (A2_P1[i] != static_cast<int>(i))
      {
        std::cout << "ERROR 2" << std::endl;
      }
    }
    for (unsigned int i = 0; i < 10; i++)
    {
      if (A2_P2[i] != -1)
      {
        std::cout << "ERROR 3" << std::endl;
      }
    }

    A2_P1 = static_cast<int*>(A2.ReAlloc(A2_P1, 20 * sizeof(int)));
    for (unsigned int i = 10; i < 20; i++)
    {
      A2_P1[i] = i;
    }
    for (unsigned int i = 0; i < 20; i++)
    {
      if (A2_P1[i] != static_cast<int>(i))
      {
        std::cout << "ERROR 4" << std::endl;
      }
    }
    for (unsigned int i = 0; i < 10; i++)
    {
      if (A2_P2[i] != -1)
      {
        std::cout << "ERROR 5" << std::endl;
      }
    }

    A2_P1 = static_cast<int*>(A2.ReAlloc(A2_P1, 5 * sizeof(int)));
    for (unsigned int i = 0; i < 5; i++)
    {
      if (A2_P1[i] != static_cast<int>(i))
      {
        std::cout << "ERROR 6" << std::endl;
      }
    }
    for (unsigned int i = 0; i < 10; i++)
    {
      if (A2_P2[i] != -1)
      {
        std::cout << "ERROR 7" << std::endl;
      }
    }
    A2.Free(A2_P1);
    A2.Free(A2_P2);
  }
}


template<typename T>
class FastAllocator: public std::allocator<T>
{
public:

};

void test2()
{

}

int main(const int /* argc */, const char* const * const /* argv */)
{
  test1();
  test2();

  return 0;
}
