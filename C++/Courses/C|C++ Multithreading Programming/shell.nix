{ pkgs ? import <nixpkgs>{} }:
with import <nixpkgs>{};
let
  debugBoost = enableDebugging pkgs.boost;
in pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    debugBoost
    clang-tools
    lldb_12
    cmake
    gdb
    fish
    mono # For cpptools (debug)
  ];

  inputsFrom = with pkgs; [
    debugBoost
  ];

  runScript = "fish";
}
