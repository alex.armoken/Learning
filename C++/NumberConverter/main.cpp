#include <iostream>
#include <string>

long long int string_to_long_long_int(std::string &number)
{
    bool is_negative = false;
    long long int result = 0;

    auto iterator = number.begin();
    if (*number.begin() == '-') {
        is_negative = true;
        iterator += 1;
    }
    for (iterator = number.begin(); iterator <= number.end(); iterator++) {
        char digit = *iterator - '0';
        if (!(0 < digit and digit < 9) ) {
            break;
        }
        result = result * 10 + digit;
    }
    if (is_negative) {
        result *= -1;
    }
    return result;
}

int main(int argc, char *argv[])
{
    std::string string_number;
    std::cin >> string_number;
    long long int number = string_to_long_long_int(string_number);
    std::cout << "Real number: " << number << std::endl;
    return 0;
}
