#include <iostream>
#include <string.h>
#include "string_funcs.hpp"

#if !(defined (VERYLONG))
#define VERYLONG

using namespace std;

const int COUNT_OF_DIGIT_POSITIONS = 1000;

class superint
{
  private:
    char num_as_str[COUNT_OF_DIGIT_POSITIONS];
    int num_len;
    superint mult_digit(const int num) const;
    superint mult_10(const superint num) const;

  public:
    char *tmp2;

    superint() : num_len(0) {
        num_as_str[0] = '\0';
    }
    superint(const char num_as_str[COUNT_OF_DIGIT_POSITIONS]) {
        strcpy(this->num_as_str, num_as_str);
        num_len = strlen(num_as_str);
    }
    superint(const unsigned long num) {
        long_to_str(num, num_as_str);
        strrev(num_as_str);
        num_len = strlen(num_as_str);
    }
    virtual ~superint() {};

    char* to_string() const;

    superint operator+(const superint n);
    superint operator-(const superint n);
    superint operator*(const superint n);
    superint operator/(const superint n);

    friend istream& operator>>(istream &s, superint &n);
    friend ostream& operator<<(ostream &s, superint &n);
};

#endif
