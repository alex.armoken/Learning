#include "verylong.hpp"

char* superint::to_string() const {
    char *tmp = new char[COUNT_OF_DIGIT_POSITIONS];
    strcpy(tmp, num_as_str);
    strrev(tmp);
    return tmp;
}

superint superint::operator+(const superint n) {
    char tmp[COUNT_OF_DIGIT_POSITIONS];
    int j;
    int max_num_len = (num_len > n.num_len) ? num_len : n.num_len;
    int carry = 0;

    for (j = 0; j < max_num_len; j++) {
        int d1 = (j > num_len - 1) ? 0 : num_as_str[j] - '0';
        int d2 = (j > n.num_len - 1) ? 0 : n.num_as_str[j] - '0';
        int digit_sum = d1 + d2 + carry;

        if (digit_sum >= 10) {
            digit_sum -= 10;
            carry = 1;
        } else {
            carry = 0;
        }
        tmp[j] = digit_sum + '0';
    }

    if (carry == 1) {
        tmp[j++] = '1';
    }
    tmp[j] = '0';
    return superint(tmp);
}

superint superint::operator-(const superint num) {

}

superint superint::mult_10(const superint num) const {
    char tmp[COUNT_OF_DIGIT_POSITIONS];
    for (int i = num.num_len - 1; i >= 0; i--) {
        tmp[i + 1] = num.num_as_str[i];
    }
    tmp[0] = '0';
    tmp[num.num_len + 1] = '\0';
    return superint(tmp);
}

superint superint::mult_digit(const int) const {
    char tmp[COUNT_OF_DIGIT_POSITIONS];
    int j, carry = 0;
}

superint superint::operator*(const superint) {

}

superint superint::operator/(const superint) {

}

istream& operator>>(istream &s, superint &i) {
    s >> i.num_as_str;
    i.num_len = strlen(i.num_as_str);
    strrev(i.num_as_str);
    return s;
}

ostream& operator<<(ostream &s, superint &num) {
    char *tmp = num.to_string();
    s << tmp;
    delete[] tmp;
    return s;
}

int main(int argc, char *argv[]) {
    superint kek(1234);
    superint top(5555);
    kek = kek + top;
    cout << kek << endl;
    return 0;
}
