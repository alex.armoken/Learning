#include "string_funcs.hpp"

char* strrev(char *str) {
    if (str || *str) {
        char *p1 = str;
        char *p2 = str + strlen(str) - 1;
        for (; p2 > p1; ++p1, --p2) {
            *p1 ^= *p2;
            *p2 ^= *p1;
            *p1 ^= *p2;
        }
    }

    return str;
}

char* long_to_str(long num, char *str) {
    unsigned int pos = 0;

    bool is_neg = (num < 0) ? true : false;
    num = labs(num);

    do {
        str[pos++] =  '0' + num % 10;
        num /= 10;
    } while (num != 0);

    if (is_neg) {
        str[pos++] = '-';
    }

    return strrev(str);
}
