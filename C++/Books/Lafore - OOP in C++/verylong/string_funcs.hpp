#include <string.h>
#include <stdlib.h>
#include <math.h>

#if !(defined (STRING_FUNCS))
#define STRING_FUNCS

char* strrev(char *str);
char* long_to_str(long num, char *str);

#endif
