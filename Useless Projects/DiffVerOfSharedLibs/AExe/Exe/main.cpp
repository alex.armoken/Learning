#include <iostream>

#include <dlfcn.h>

#include "dynlib.hpp"

int main(const int argc, char** const argv)
{
    if (argc != 2)
    {
        std::cerr << "!!! Path to dynamic library must be passed as command line argument !!!" << std::endl;
        return 1;
    }

    std::cout << "Executable started!" << std::endl
              << getMessage() << std::endl;

    auto* modlibHandler = dlmopen(LM_ID_NEWLM, argv[1], RTLD_NOW | RTLD_LOCAL);
    if (modlibHandler == nullptr)
    {
        std::cout << "!!! ModLib NOT loaded !!!" << dlerror() << std::endl;
        return 2;
    }
    std::cout << "ModLib loaded!" << std::endl;

    auto* getMsgFunc = reinterpret_cast<const char*(*)()>(dlsym(modlibHandler, "getMessageFromModLib"));
    if (getMsgFunc == nullptr)
    {
        std::cerr << "!!! Symbol not found !!!" << std::endl;

        dlclose(modlibHandler);
        modlibHandler = nullptr;

        return 3;
    }

    std::cout << "Symbol retrieved!" << std::endl;
    std::cout << getMsgFunc() << std::endl;

    dlclose(modlibHandler);
    modlibHandler = nullptr;

    return 0;
}
