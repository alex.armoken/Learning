#include <string>
#include "dynlib.hpp"

const char* getMessage()
{
    static std::string msg = "This is message retrieved from shared library for DYNAMICALLY LOADED LIBRARY!";
    return msg.c_str();
}
