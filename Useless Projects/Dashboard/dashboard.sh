#!/usr/bin/env bash

cleanup() {
	tmux kill-session -t user-dashboard
}

trap cleanup EXIT

tmux new-session -d -s user-dashboard
tmux set status off
tmux set pane-border-style "fg=black"
tmux set pane-active-border-style "fg=black"

tmux split-window -d -t 0 -v
tmux split-window -d -t 0 -h

tmux resize-pane -t 0 -x 76
tmux resize-pane -t 0 -y 40
tmux send-keys -t 0 "bash" enter
tmux send-keys -t 0 "export PS1=''; history -c; clear; echo; gcal -H '\e[34m:\e[0m:\e[32m:\e[0m' 2022 | tail +5 | awk '{print \"\t    \" \$0}'" enter

tmux resize-pane -t 1 -x 135
tmux send-keys -t 1 "bash" enter
tmux send-keys -t 1 "export PS1=''; history -c; clear; echo; curl --silent 'wttr.in/Minsk' | head -n -3 | awk '{print \"     \" \$0}'" enter

tmux send-keys -t 2 "bash" enter
tmux send-keys -t 2 "export PS1=''; history -c; clear; ipython" enter
tmux select-pane -t 2

tmux attach -t user-dashboard
