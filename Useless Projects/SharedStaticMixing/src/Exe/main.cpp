#include <iostream>

#include <dlfcn.h>

#include "ashalib.hpp"
#include "bshalib.hpp"

int main(const int argc, char** const argv)
{
    std::cout << getMessageA() << std::endl;
    std::cout << getMessageB() << std::endl;

    return 0;
}
