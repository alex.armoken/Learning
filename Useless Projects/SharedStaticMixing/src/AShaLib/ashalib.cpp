#include <string>
#include <optional>
#include "ashalib.hpp"
#include "statlib.hpp"

const char* getMessageA()
{
    static std::optional<std::string> msg;
    if (!msg)
    {
        msg = getMessageStat();
        *msg += " And also from DYNAMIC LIBRARY!";
    }

    return msg->c_str();
}
