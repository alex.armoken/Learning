import sys
import multiprocessing.connection


def main() -> int:
    address = ('localhost', 6000)
    connection = multiprocessing.connection.Client(address,
                                                   authkey='secret password')

    connection.send('close')

    connection.close()

    return 0


if __name__ == "__main__":
    sys.exit(main())
