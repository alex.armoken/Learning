import sys
import time
import multiprocessing.connection


def main() -> int:
    address = ('localhost', 6000)
    listener = multiprocessing.connection.Listener(address,
                                                   authkey='secret password')
    connection = listener.accept()
    print('Connection accepted from', listener.last_accepted)

    while True:
        message = connection.recv()
        if message == 'close':
            connection.close()
            break

        time.sleep(1)

    listener.close()

    return 0


if __name__ == "__main__":
    sys.exit(main())
