{ pkgs ? import <nixpkgs>{} }:
with import <nixpkgs>{};
let
  pyglossary-package = python-packages:
    with python-packages;
    buildPythonPackage rec {
      pname   = "pyglossary";
      version = "4.6.1";

      src = fetchPypi {
        inherit pname version;
        sha256 = "b7dbb4904dfbd1ba5af4b443b32d2c5cc45fd37256fa44a89185cce3d330d443";
      };

      propagatedBuildInputs = [
        PyICU            # Python extension wrapping the ICU C++ API.
        beautifulsoup4   # HTML and XML parser.
        html5lib         # HTML parser based on WHAT-WG HTML5 specification.
        lxml             # Pythonic binding for the libxml2 and libxslt libraries.
        marisa-trie      # Static memory-efficient Trie-like structures for Python based on marisa-trie C++ library.
        pycairo          # Python 3 bindings for cairo.
        pygobject3       # Python bindings for Glib.
        python-lzo       # Python bindings for the LZO data compression library.
        pyyaml           # The next generation YAML parser and emitter for Python.
        setuptools       # Utilities to facilitate the installation of Python packages.
        tkinter          # A high-level dynamically-typed programming language.
      ];
      nativeBuildInputs     = [
        pkgs.coreutils
      ];

      doCheck = false;
      USER = builtins.getEnv "USER";

      meta = with lib; {
        homepage        = "https://github.com/ilius/pyglossary";
        description     = "A tool for converting dictionary files aka glossaries.";
        license         = licenses.gpl3;
        longDescription = ''
        A tool for converting dictionary files aka glossaries.
        Mainly to help use our offline glossaries in any Open
        Source dictionary we like on any modern operating
        system / device.
        '';
        platforms       = platforms.linux;
      };
    };

  orgparse-package = python-packages:
    with python-packages;
    buildPythonPackage rec {
      pname   = "orgparse";
      version = "0.3.2";

      src = fetchPypi {
        inherit pname version;
        sha256 = "451050e79acb7a51c65dc99b9095eae4d50bd598541354f9e763cb4cbdf59a55";
      };

      nativeBuildInputs = [
        setuptools-scm
      ];

      doCheck = false;

      meta = with lib; {
        homepage        = "https://github.com/karlicoss/orgparse";
        description     = "Python module for reading Emacs org-mode files.";
        license         = licenses.bsd2;
        platforms       = platforms.linux;
      };
    };

  inorganic-package = python-packages:
    with python-packages;
    buildPythonPackage rec {
      pname   = "inorganic";
      version = "1.0";

      preBuild = ''
      cat > setup.py << EOF
      from setuptools import setup

      setup(
        name="${pname}",
        version="${version}",
        author="karlicoss",
        description="Convert python structures into org-mode.",
        packages=["inorganic"],
        package_dir={"inorganic": "src"}
      )
      EOF
      '';
      postPatch = ''
      cat > src/__init__.py << EOF
      from .inorganic import OrgNode
      EOF
      '';

      src = fetchFromGitHub {
        owner  = "karlicoss";
        repo   = pname;
        rev    = "783539bfc6cc2f4956e90940f9d9f76a73fbc9c7";
        sha256 = "sha256-XE2KkxCN+wHBMat8E++B5A9dCVJl8y8mQfDwo0cn7gg=";
      };

      doCheck = false;

      meta = with lib; {
        homepage    = "https://github.com/karlicoss/inorganic";
        description = "Convert python structures into org-mode.";
        license     = licenses.mit;
        platforms   = platforms.linux;
      };
    };

  python-with-packages = ((pkgs.python310Full.withPackages(ps: [
        ps.beautifulsoup4 # HTML and XML parser.
        ps.dbus-next      # A zero-dependency DBus library for Python with asyncio support.
        ps.pillow         # The friendly PIL fork (Python Imaging Library).
        ps.pygobject3     # Python bindings for Glib.
        ps.pytest         # Framework for writing tests.
        ps.rpyc           # Remote Python Call (RPyC), a transparent and symmetric RPC library.
        ps.watchdog       # Python API and shell utilities to monitor file system events.

        (inorganic-package  ps)
        (orgparse-package   ps)
        (pyglossary-package ps)
      ])).overrideAttrs (args: { ignoreCollisions = true; doCheck = false; }));
in pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    gtk3
    libappindicator-gtk3
    gobject-introspection

    python-with-packages
    sdcv                                 # Console version of StarDict.

    nodePackages.pyright                 # Type checker for the Python language.
    vscode-extensions.ms-pyright.pyright # VS Code static type checking for Python.

    fish
  ];
  shellHook = ''
    PYTHONPATH=${python-with-packages}/${python-with-packages.sitePackages}
    # maybe set more env-vars
  '';

  runScript = "fish";
}
