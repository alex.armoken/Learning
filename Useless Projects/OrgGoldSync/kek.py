import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository.GdkPixbuf import Pixbuf

icons = ["edit-cut", "edit-paste", "edit-copy", "view-refresh"]


class IconViewWindow(Gtk.Window):
    def __init__(self):
        super().__init__()
        self.set_default_size(200, 200)

        liststore = Gtk.ListStore(Pixbuf, str)
        iconview = Gtk.IconView.new()
        iconview.set_model(liststore)
        iconview.set_pixbuf_column(0)
        iconview.set_text_column(1)

        icon_theme = Gtk.IconTheme.get_default()
        # icon_theme.set_custom_theme('Qogir-dark')

        for icon in icons:
            pixbuf = icon_theme.load_icon(icon, 64, 0)
            liststore.append([pixbuf, "Label"])

        self.add(iconview)


win = IconViewWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
