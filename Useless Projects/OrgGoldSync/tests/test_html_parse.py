import pytest

from ..org_gold_sync.stardict_source import (
    extract_from_flat_heirarchy,
    extract_from_two_level_heirarchy_with_transcription,
    extract_from_three_level_heirarchy_with_transcription,
    extract_translations_from_sdcv_query
)

kek = "\n<div style=\"margin-left:1em\"><font color=\"green\">сущ.</font></div>\n<div style=\"margin-left:1em\">1) <font color=\"green\">обр.</font> школа; учебное заведение</div>\n<div style=\"margin-left:2em\"><span class=\"sec\"><span class=\"ex\"><font color=\"steelblue\">private school — частная школа</font></span></span></div>\n<div style=\"margin-left:2em\"><span class=\"sec\"><b>See:</b></span></div>\n<div style=\"margin-left:2em\"><span class=\"sec\"><a href=\"elementary school\">elementary school</a>, <a href=\"grammar school\">grammar school</a>, <a href=\"high school\">high school</a>, <a href=\"secondary school\">secondary school</a></span></div>\n<div style=\"margin-left:1em\">2) <font color=\"green\">обр.</font> факультет университета (<i>дающий право на получение ученой степени</i>)</div>\n<div style=\"margin-left:2em\"><span class=\"sec\"><b>See:</b></span></div>\n<div style=\"margin-left:2em\"><span class=\"sec\"><a href=\"John F. Kennedy School of Government\">John F. Kennedy School of Government</a></span></div>\n<div style=\"margin-left:1em\">3) <font color=\"green\">обр.</font> учение, образование</div>\n<div style=\"margin-left:2em\"><span class=\"sec\"><b>Syn:</b></span></div>\n<div style=\"margin-left:2em\"><span class=\"sec\">education</span></div>\n<div style=\"margin-left:2em\"><span class=\"sec\"><b>See:</b></span></div>\n<div style=\"margin-left:2em\"><span class=\"sec\"><a href=\"high school\">high school</a>, <a href=\"school board\">school board</a>, <a href=\"school bond\">school bond</a>, <a href=\"school district\">school district</a>, <a href=\"secondary school\">secondary school</a></span></div>"
