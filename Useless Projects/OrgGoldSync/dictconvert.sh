#!/usr/bin/env bash

set -euf -o pipefail
# Disable checking absence of some functionality in POSIX shell
# shellcheck disable=SC2039

function convert_dictionaries_in_dir()
{
	local path_to_src_dir="$1"
	local path_to_output_dir="$2"

	if [[ "${path_to_src_dir}" == "${path_to_output_dir}" ]]; then
		printf ">>>>> Input and output directories are identical!\n"
		exit 1
	fi

	if ! [ -d "${path_to_src_dir}" ]; then
		printf ">>>>> Directory %s not exists!\n" "${path_to_src_dir}"
		exit 2
	fi

	if ! [ -d "${path_to_output_dir}" ]; then
		printf ">>>>> Directory %s not exists!\n" "${path_to_output_dir}"
		exit 3
	fi

	local depths_of_src_dir=$(
		find "${path_to_src_dir}" -type f -print0                                                    \
			| while IFC="" read -r -d "" file_path; do
				relative_file_path=$(realpath --relative-to "${path_to_src_dir}" "${file_path}")
				file_path_depth=$(echo "${relative_file_path}" | grep --only-matching / | wc --lines)
				echo "${file_path_depth}"
			done                                                                                     \
			| sort --unique                                                                          \
			| sed --expression 's/^[[:space:]]*//'
	)
	local depths_count_of_src_dir=$(echo "${depths_of_src_dir}" | wc --lines)

	if [[ ${depths_count_of_src_dir} != 1 ]]; then
		printf ">>>>> This script supports only source directories with identical depths\n"
		printf ">>>>> But in directory %s there are %d variants of depths\n" "${path_to_src_dir}" "${depths_count_of_src_dir}"
		exit 4
	fi
	if (( depths_of_src_dir > 1 )); then
		printf ">>>>> This script supports only source directories with two-level hierarchies\n"
		printf ">>>>> But directory %s have %d-level hierarchy\n" "${path_to_src_dir}" "${depths_of_src_dir}"
		exit 5
	fi

	find "${path_to_src_dir}" -type f -name "*.dsl" -print0 \
		| while IFC="" read -r -d "" file_path; do
			local relative_file_path=$(realpath --relative-to "${path_to_src_dir}" "${file_path}")
			local output_file_path="${path_to_output_dir}/${relative_file_path%.dsl}.ifo"
			printf "=!=!=!= From '%s' to '%s'\n" "${file_path}" "${output_file_path}"

			mkdir --parents "$(dirname '${output_file_path}')"

			pyglossary                        \
				"${file_path}"                \
				"${output_file_path}"         \
				--read-format ABBYYLingvoDSL  \
				--write-format Stardict
		done

	find "${path_to_src_dir}" -type f -name "*.dsl.dz" -print0 \
		| while IFC="" read -r -d "" file_path; do
			local relative_file_path=$(realpath --relative-to "${path_to_src_dir}" "${file_path}")
			local output_file_path="${path_to_output_dir}/${relative_file_path%.dsl.dz}.ifo"
			printf "=!=!=!= From '%s' to '%s'\n" "${file_path}" "${output_file_path}"

			mkdir --parents "$(dirname '${output_file_path}')"

			pyglossary                       \
				"${file_path}"               \
				"${output_file_path}"        \
				--read-format ABBYYLingvoDSL \
				--write-format Stardict
		done
}


declare -a dicts_dirs=(
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/Uk-Uk"
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/Uk-Ru"
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/Ru-Uk"
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/En-Uk"
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/Sound"
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/Uk-En"
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/Ru-Ru"
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/En-Ru"
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/Abbrev"
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/En-En"
	"/home/armoken/Installers/Windows/Office/GoldenDict_Dicts/Ru-En"
)
path_to_output_dir="/home/armoken/Downloads/GoldDictsInStarDictFormat"
for path in "${dicts_dirs[@]}"; do
	dict_dir_name=$(basename "${path}")

	output_dir_path="${path_to_output_dir}/${dict_dir_name}"
	mkdir --parents "${output_dir_path}"

	convert_dictionaries_in_dir "${path}" "${output_dir_path}"
done
