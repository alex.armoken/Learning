import dataclasses
from abc import abstractmethod, ABC
from typing import Dict, List, Optional

@dataclasses.dataclass(frozen=True)
class LanguagePair:
    """Class that should be used as dict's key."""
    source: Optional[str]
    target: Optional[str]


@dataclasses.dataclass
class WordInfo:
    """Collected information about word loaded from
       Goldendict's history file."""
    word: str
    translations: Dict[LanguagePair, List[str]]
    card_type: Optional[str] = None
    edited_by_user: bool = False
    user_edited_translation: str = ""

    def get_language_pairs_of_translations(self) -> List[LanguagePair]:
        return [pair for pair in self.translations.keys()]

    def get_languages_of_translations(self) -> List[str]:
        languages = set()
        for pair in self.get_language_pairs_of_translations():
            languages.add(pair.source)
            languages.add(pair.target)

        return list(languages)


class SubscriptionManager:
    def __init__(
        self,
        subscribe_func,
        unsubscribe_func,
        obj
    ) -> None:
        self._subscribe_func = subscribe_func
        self._unsubscribe_func = unsubscribe_func
        self._obj = obj

    def __enter__(self):
        self._subscribe_func(self._obj)

    def __exit__(self):
        self._unsubscribe_func(self._obj)


class IOrgDependent(ABC):
    @abstractmethod
    def on_org_reload(self):
        pass


class IOrgProvidingService(ABC):
    @property
    @abstractmethod
    def words(self) -> List[str]:
        return []

    @abstractmethod
    def subscribe_on_changing(self, obj: IOrgDependent):
        pass

    @abstractmethod
    def unsubscribe_from_changing(self, obj: IOrgDependent):
        pass

    @abstractmethod
    def save_translation_variants(self, words: List[WordInfo]):
        pass

    def with_obj(self, obj: IOrgDependent) -> SubscriptionManager:
        return SubscriptionManager(
            self.subscribe_on_changing,
            self.unsubscribe_from_changing,
            obj
        )


class IHistoryDependent(ABC):
    @abstractmethod
    def on_history_reload(self):
        pass


class IHistoryProvidingService(ABC):
    @property
    @abstractmethod
    def words(self) -> List[str]:
        return []

    @abstractmethod
    def subscribe_on_changing(self, obj: IHistoryDependent):
        pass

    @abstractmethod
    def unsubscribe_from_changing(self, obj: IHistoryDependent):
        pass

    def with_obj(self, obj: IHistoryDependent) -> SubscriptionManager:
        return SubscriptionManager(
            self.subscribe_on_changing,
            self.unsubscribe_from_changing,
            obj
        )


class IDictionariesDependent(ABC):
    @abstractmethod
    def on_dicts_changing(self):
        pass


class IDictProvidingService(ABC):
    @abstractmethod
    def get_word_translations(self, words: str) -> Dict[LanguagePair, List[str]]:
        pass

    @abstractmethod
    def subscribe_on_changing(self, obj: IDictionariesDependent):
        pass

    @abstractmethod
    def unsubscribe_from_changing(self, obj: IDictionariesDependent):
        pass

    def with_obj(self, obj: IDictionariesDependent) -> SubscriptionManager:
        return SubscriptionManager(
            self.subscribe_on_changing,
            self.unsubscribe_from_changing,
            obj
        )


class ITranslationsDependent(ABC):
    @abstractmethod
    def on_translations_changing(self):
        pass


class ITranslationsProvidingService(ABC):
    @abstractmethod
    def get_words_translations(
        self,
        words: List[str]
    ) -> Dict[str, Dict[LanguagePair, List[str]]]:
        pass

    @abstractmethod
    def subscribe_on_changing(self, obj: ITranslationsDependent):
        pass

    @abstractmethod
    def unsubscribe_from_changing(self, obj: ITranslationsDependent):
        pass

    def with_obj(self, obj: ITranslationsDependent) -> SubscriptionManager:
        return SubscriptionManager(
            self.subscribe_on_changing,
            self.unsubscribe_from_changing,
            obj
        )
