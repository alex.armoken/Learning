#!/usr/bin/env python3
import argparse
import logging
import os
import sys
import time
import signal
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from typing import Optional

from watchdog.observers import Observer
from dbus_next.glib.message_bus import MessageBus

import rpyc
from rpyc.utils.helpers import classpartial

from notifications import NotificationService
from utils import OrgGoldSyncException
from gui import (idle_add,
                 gui_main,
                 quit_gui_main,
                 OrgGoldSyncProcessingWindowService,
                 TrayDbusService)
from test_mocks import (TestTranslationsProvidingService,
                        TestHistoryService,
                        TestOrgProvidingService)
from model import OrgGoldSyncModel
from org import OrgSynchronizer
from goldendict import GoldHistoryService
from stardict_source import StarDictDictsSynchronizer
from dict_service import TranslationsProvidingRPCClient, TranslationsProvidingRPCService


logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(message)s')


LOG_CONTEXT_BORDER = "=" * 7;


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description=("Script for adding new words"
                     " from Goldendict history to Org")
    )
    parser.add_argument(
        "--path-to-goldendict-dir",
        default=str(Path.home() / ".goldendict"),
        action="store", nargs=1, type=str
    )
    parser.add_argument(
        "--path-to-config",
        default=str(Path.home()
                    / os.environ["XDG_CONFIG_HOME"]
                    / "org_gold_sync.json"),
        action="store", nargs=1, type=str
    )
    parser.add_argument(
        "--path-to-org",
        action="store", nargs=1, type=str
    )
    parser.add_argument("--sync-manually",
                        action="store_true")

    parser.add_argument(
        "--port",
        action="store", nargs=1, required=True, type=int
    )
    parser.add_argument("--test", action="store_true")
    parser.add_argument("--tray-service", action="store_true")
    parser.add_argument("--gui-service", action="store_true")
    parser.add_argument("--dict-service", action="store_true")

    return parser.parse_args()


def test_args_logic(args: argparse.Namespace):
    if args.gui_service and args.dict_service:
        raise OrgGoldSyncException(
            "Dictionary providing service and GUI providing service"
            " can't be used in the same process in the same time."
        )

    if not args.gui_service and not args.dict_service:
        raise OrgGoldSyncException(
            "One of service types (dictionary providing service, GUI providing service)"
            " should be used."
        )

    if args.sync_manually and args.dict_service:
        raise OrgGoldSyncException(
            "Flag --sync-manually can't be used with --dict-service."
        )


class OrgGoldSyncApplication:
    def __init__(
        self,
        test: bool,
        port: int,
        path_to_org: Optional[Path],
        path_to_goldendict_dir: Optional[Path],
        bus: MessageBus,
        notify_service: NotificationService
    ) -> None:
        self._test = test
        self._in_context = False

        if test:
            self._org_service = TestOrgProvidingService()
        else:
            assert path_to_org is not None
            self._path_to_org = path_to_org
            self._org_service = OrgSynchronizer(self._path_to_org)

        if test:
            self._history_service = TestHistoryService()
        else:
            assert path_to_goldendict_dir is not None
            self._path_to_goldendict_history = path_to_goldendict_dir / "history"
            self._history_service = GoldHistoryService(
                self._path_to_goldendict_history,
                notify_service
            )

        if test:
            self._translation_service = TestTranslationsProvidingService()
        else:
            self._translation_service = TranslationsProvidingRPCClient()
            rpyc.connect(host="localhost",
                         port=port,
                         service=self._translation_service) # type: ignore

        self._model = OrgGoldSyncModel(
            self._org_service,
            self._translation_service,
            self._history_service
        )
        self._executor = ThreadPoolExecutor(max_workers=7)

        self._notify_service = notify_service
        self._window_service = OrgGoldSyncProcessingWindowService(
            self._model,
            self._notify_service
        )
        self._tray_service = TrayDbusService(bus, self._model, self._window_service)

    def _signal_handler(self, *_):
        idle_add(quit_gui_main)

    def __enter__(self):
        assert not self._in_context
        self._in_context = True

        signal.signal(signal.SIGINT, self._signal_handler)
        logging.debug("SIGINT handler installed")

        self._org_service.subscribe_on_changing(self._model)
        self._history_service.subscribe_on_changing(self._model)
        self._translation_service.subscribe_on_changing(self._model)
        self._model.subscribe_on_changing(self._tray_service)
        logging.debug("Ready to start application services")

    def __exit__(self, *_):
        assert self._in_context
        self._in_context = False

        self._model.unsubscribe_from_changing(self._tray_service)
        self._translation_service.unsubscribe_from_changing(self._model)
        self._history_service.unsubscribe_from_changing(self._model)
        self._org_service.unsubscribe_from_changing(self._model)

        signal.signal(signal.SIGINT, signal.SIG_DFL)
        logging.debug("SIGINT handler reseted")
        logging.debug("Not ready to start application services")

    def run_once(self):
        assert self._in_context

        logging.info("%s Synchronization job manually started %s",
                     LOG_CONTEXT_BORDER,
                     LOG_CONTEXT_BORDER)

        self._tray_service.initialize()
        logging.debug("Window and tray initialized")

        gui_thread_future = self._executor.submit(gui_main)
        gui_thread_future.result()

        logging.info(
            "%s Manually started synchronization job successfully executed %s",
            LOG_CONTEXT_BORDER,
            LOG_CONTEXT_BORDER
        )

    def run_as_service(self):
        assert not self._test
        assert self._in_context

        gui_thread_future = self._executor.submit(gui_main)

        idle_add(self._tray_service.initialize)
        logging.debug("Window and tray initialized")

        observer = Observer()
        observer.schedule(self._org_service, self._path_to_org)
        observer.schedule(self._history_service, self._path_to_goldendict_history)

        observer.start()
        logging.info("%s OrgGold Watchdog started %s",
                     LOG_CONTEXT_BORDER,
                     LOG_CONTEXT_BORDER)
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            logging.info(
                "%s Keyboard interruption received, service stopping started %s",
                LOG_CONTEXT_BORDER,
                LOG_CONTEXT_BORDER
            )
        finally:
            observer.stop()
            observer.join()
            gui_thread_future.result()
            logging.info("%s OrgGold Synchronizer stopped %s",
                         LOG_CONTEXT_BORDER,
                         LOG_CONTEXT_BORDER)


def run_gui_service(args,
                    bus: MessageBus,
                    notify_service: NotificationService) -> int:
    if args.path_to_org is None:
        maybe_path_to_org = None
    else:
        maybe_path_to_org = Path(args.path_to_org)

    if args.path_to_goldendict_dir is None:
        maybe_path_to_goldendict_dir = None
    else:
        maybe_path_to_goldendict_dir = Path(args.path_to_goldendict_dir)

    app = OrgGoldSyncApplication(
        args.test,
        args.port,
        maybe_path_to_org,
        maybe_path_to_goldendict_dir,
        bus,
        notify_service
    )

    with app:
        if args.sync_manually:
            app.run_once()
        else:
            app.run_as_service()

    return 0


def run_dict_service(
    args,
    notify_service: NotificationService
) -> int:
    executor = ThreadPoolExecutor(max_workers=7)

    dict_providing_service = StarDictDictsSynchronizer(
        args,
        executor,
        notify_service
    )

    server = rpyc.ThreadedServer(
        classpartial(
            TranslationsProvidingRPCService,
            dict_providing_service,
            notify_service
        ),
        port=args.port[0]
    )
    try:
        executor.submit(server.start)
        logging.info("%s Dictionary providing RPC service started %s",
                     LOG_CONTEXT_BORDER,
                     LOG_CONTEXT_BORDER)

        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        logging.info(
            "%s Keyboard interruption received, service stopping started %s",
            LOG_CONTEXT_BORDER,
            LOG_CONTEXT_BORDER
        )
    finally:
        server.close()
        executor.shutdown(wait=True)

        logging.info(
            "%s Dictionary providing RPC service, watchdog, and all threads stopped %s",
            LOG_CONTEXT_BORDER,
            LOG_CONTEXT_BORDER
        )

    return 0


def main() -> int:
    args = parse_args()
    test_args_logic(args)
    logging.info("Arguments parsed")

    session_bus = MessageBus().connect_sync()

    notify_service = NotificationService(session_bus)
    logging.info("D-Bus Notify interface retrieved")

    if args.gui_service:
        return run_gui_service(args, session_bus, notify_service)
    elif args.dict_service:
        return run_dict_service(args, notify_service)

    return 1


if __name__ == "__main__":
    sys.exit(main())
