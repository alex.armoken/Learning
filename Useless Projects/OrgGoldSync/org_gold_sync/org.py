import threading
from pathlib import Path
from typing import List, Optional, Tuple

import inorganic
import orgparse
from orgparse.node import OrgNode
from watchdog.events import FileSystemEventHandler

from interfaces import (IOrgDependent,
                        IOrgProvidingService)
from model import WordInfo, LanguagePair
from utils import OrgGoldSyncException


class OrgSynchronizer(IOrgProvidingService, FileSystemEventHandler):
    """Class that handle org file contents changing."""
    def __init__(
        self,
        path_to_org_file: Path
    ) -> None:
        self._lock = threading.RLock()

        self._path_to_org_file = path_to_org_file

        self._src_org_root: Optional[OrgNode] = None
        self._words: List[str] = []

        self._src_org_root, self._words = self._read_alread_added_words_from_org()

        self._org_dependents: List[IOrgDependent] = []

        super().__init__()

    @property
    def words(self) -> List[str]:
        with self._lock:
            return self._words

    def subscribe_on_changing(self, obj: IOrgDependent):
        with self._lock:
            self._org_dependents.append(obj)

    def unsubscribe_from_changing(self, obj: IOrgDependent):
        with self._lock:
            maybe_idx = next((
                i
                for i, dep in enumerate(self._org_dependents)
                if dep is obj
            ), None)
            if maybe_idx is None:
                raise OrgGoldSyncException(
                    "Invaid attemp to unsubscribe from org changing "
                )

            self._org_dependents.pop(maybe_idx)

    def on_modified(self, _):
        with self._lock:
            for dependent in self._org_dependents:
                dependent.on_org_reload()

    def _read_alread_added_words_from_org(self) -> Tuple[OrgNode, List[str]]:
        """Languages ignored."""
        org_root = orgparse.load(self._path_to_org_file)

        words = []
        for top_child in org_root.children:
            for word_child in top_child.children:
                words.append(word_child.heading)

        return (org_root, words)

    def _convert_orgparse_node_to_inorganic(
        self,
        org_node: OrgNode,
        new_children: List[inorganic.OrgNode] = []
    ) -> inorganic.OrgNode:
        result = inorganic.OrgNode(
            heading=org_node.heading,
            todo=org_node.todo,
            tags=list(org_node.tags),
            scheduled=org_node.scheduled.start,
            properties={key: str(value)
                        for key, value in org_node.properties.items()},
            body=org_node.body,
            children=[self._convert_orgparse_node_to_inorganic(child)
                      for child in org_node.children] + new_children
        )

        return result

    def _add_translations_to_org(
        self,
        words: List[WordInfo],
        org_node: OrgNode
    ) -> inorganic.OrgNode:
        new_children = []
        for word in words:
            assert word.card_type is not None
            card_node = inorganic.OrgNode(
                heading=word.word,
                todo=None,
                tags=(),
                scheduled=None,
                properties={"ANKI_NOTE_TYPE": word.card_type},
                body=word.user_edited_translation
            )
            new_children.append(card_node)

        return self._convert_orgparse_node_to_inorganic(org_node, new_children)

    def _add_new_translations_to_org(
        self,
        org_root: OrgNode,
        words: List[WordInfo]
    ) -> inorganic.OrgNode:
        children = []
        for child in org_root.children:
            if not child.heading:
                inorganic_child = self._convert_orgparse_node_to_inorganic(child)
                children.append(inorganic_child)
                continue

            langs = [lang.strip() for lang in child.heading.lower().split("to")]
            if len(langs) != 2:
                inorganic_child = self._convert_orgparse_node_to_inorganic(child)
                children.append(inorganic_child)
                continue

            lang_pair = LanguagePair(langs[0], langs[1])
            words_for_cur_heading = [word for word in words]

            if words_for_cur_heading:
                inorganic_child = self._add_translations_to_org(
                    words_for_cur_heading,
                    child
                )
                children.append(inorganic_child)
            else:
                inorganic_child = self._convert_orgparse_node_to_inorganic(child)
                children.append(inorganic_child)

        new_org_root = inorganic.OrgNode(
            heading=lambda: "",
            properties={key: str(value)
                        for key, value in org_root.properties.items()},
            body=org_root.body,
            children=children
        )

        return new_org_root

    def save_translation_variants(self, words: List[WordInfo]):
        with self._lock:
            word_edited_by_user = [word for word in words
                                   if word.edited_by_user]

            assert self._src_org_root is not None
            new_org_root = self._add_new_translations_to_org(
                self._src_org_root,
                word_edited_by_user
            )

            new_org_file_content = new_org_root.render(level=0)
            with self._path_to_org_file.open("w") as f:
                f.write(new_org_file_content)
