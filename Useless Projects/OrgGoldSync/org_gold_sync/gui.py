import os
import logging
import threading
from typing import Callable, Tuple

import dbus_next
import dbus_next.service
from dbus_next.constants import PropertyAccess
from dbus_next.glib.message_bus import MessageBus
from PIL import Image

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk # type: ignore

from interfaces import (ITranslationsDependent)
from model import LanguagePair, WordInfo, OrgGoldSyncModel
from notifications import NotificationService


class OrgGoldSyncProcessingWindowPresenter(ITranslationsDependent):
    """Class that acts as presenter in MVP pattern."""
    def __init__(
        self,
        view_builder: Gtk.Builder,
        model: OrgGoldSyncModel,
        on_show_callback: Callable,
        on_destroy_callback: Callable
    ) -> None:
        self._model = model

        self._init_words_treeview(view_builder)
        self._init_current_word_entry(view_builder)
        self._init_card_selector(view_builder)
        self._init_current_word_translations_list(view_builder)
        self._init_result_translation_editbox(view_builder)
        self._init_seleted_word_box(view_builder)

        self._on_show_callback = on_show_callback
        self._on_destroy_callback = on_destroy_callback

    def _init_words_treeview(self, view_builder: Gtk.Builder):
        self._words_treeview = view_builder.get_object("OrgGoldSync_WordsTreeView")
        self._words_list_store = view_builder.get_object("OrgGoldSync_WordsListStore")

        for i, column_title in enumerate(
            ["Source language", "Target language", "Word"]
        ):
            renderer = Gtk.CellRendererText()
            column = Gtk.TreeViewColumn(column_title, renderer, text=i)
            self._words_treeview.append_column(column)

        self._word_selection = self._words_treeview.get_selection()
        self._word_selection.connect("changed", self.onWordRowChanged)

        words = self._model.get_words_to_process()
        for word in words:
            self._words_list_store.append([
                "" if word.get_language_pairs_of_translations()[0].source is None else word.get_language_pairs_of_translations()[0].source,
                "" if word.get_language_pairs_of_translations()[0].target is None else word.get_language_pairs_of_translations()[0].target,
                word.word
            ])

    def _init_current_word_entry(self, view_builder: Gtk.Builder):
        self._cur_word_entry = view_builder.get_object("OrgGoldSync_CurrentWordEntry")

    def _init_card_selector(self, view_builder: Gtk.Builder):
        card_types_store = Gtk.ListStore(str)
        card_types = [
            "Basic",
            "Basic (and reversed card)"
        ]
        for card_type in card_types:
            card_types_store.append([card_type])

        self._card_type_combobox = view_builder.get_object(
            "OrgGoldSync_CardTypeComboBox"
        )
        self._card_type_combobox.set_model(card_types_store)
        self._card_type_combobox.set_entry_text_column(0)

        renderer_text = Gtk.CellRendererText()
        self._card_type_combobox.pack_start(renderer_text, True)
        self._card_type_combobox.add_attribute(renderer_text, "text", 0)

    def _init_current_word_translations_list(self, view_builder: Gtk.Builder):
        self._translations_treeview = view_builder.get_object(
            "OrgGoldSync_TranslationsTreeView"
        )
        self._translations_list_store = view_builder.get_object(
            "OrgGoldSync_TranslationsListStore"
        )

        translation_renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Translations", translation_renderer, text=0)
        self._translations_treeview.append_column(column)

        self._translation_selection = self._translations_treeview.get_selection()
        self._translation_selection.connect("changed", self.onTranslationRowChanged)
        self._translation_selection.set_mode(Gtk.SelectionMode.MULTIPLE)

    def _init_result_translation_editbox(self, view_builder: Gtk.Builder):
        self._result_translation_editbox = view_builder.get_object(
            "OrgGoldSync_ResultTranslationEditBox"
        )
        self._result_translation_textbuf = self._result_translation_editbox.get_buffer()

    def _init_seleted_word_box(self, view_builder: Gtk.Builder):
        self._selected_word_box = view_builder.get_object("OrgGoldSync_SelectedWordBox")
        self._selected_word_box.set_sensitive(False)

    def on_translations_changing(self):
        model, treeiter = self._word_selection.get_selected()
        assert treeiter is not None
        cur_word = model[treeiter][2]

        words = self._model.get_words_to_process()
        self._words_list_store.clear()
        for word in words:
            self._words_list_store.append([
                "" if word.get_language_pairs_of_translations()[0].source is None else word.get_language_pairs_of_translations()[0].source,
                "" if word.get_language_pairs_of_translations()[0].target is None else word.get_language_pairs_of_translations()[0].target,
                word.word
            ])

        cur_word_id = next((
            i
            for i, row in enumerate(self._words_list_store)
            if row[0] == cur_word
        ), 0)
        self._words_list_store.set_active(cur_word_id)

    def onWordRowChanged(self, selection):
        model, treeiter = selection.get_selected()
        if treeiter is not None:
            self._selected_word_box.set_sensitive(True)
            self._card_type_combobox.set_active(0)

            cur_word = model[treeiter][2]
            self._cur_word_entry.set_text(cur_word)

            lang_pair = LanguagePair(
                model[treeiter][0],
                model[treeiter][1]
            )

            cur_word_info = next(
                info
                for info in self._model.get_words_to_process()
                if info.word == cur_word
            )

            self._translations_list_store.clear()
            for translation in cur_word_info.translations[lang_pair]:
                self._translations_list_store.append([translation])

            card_type_id = next((
                i
                for i, row in enumerate(self._card_type_combobox.get_model())
                if row[0] == cur_word_info.card_type
            ), 0)
            self._card_type_combobox.set_active(card_type_id)
        else:
            self._selected_word_box.set_sensitive(False)
            self._card_type_combobox.set_active(-1)
            self._cur_word_entry.set_text("")
            self._translations_list_store.clear()
            self._result_translation_textbuf.set_text("")

    def _get_info_for_current_word(self) -> WordInfo:
        model, treeiter = self._word_selection.get_selected()
        assert treeiter is not None

        cur_word = model[treeiter][2]
        self._cur_word_entry.set_text(cur_word)

        cur_word_info = next(
            info
            for info in self._model.get_words_to_process()
            if info.word == cur_word
        )

        return cur_word_info

    def onTranslationRowChanged(self, selection):
        model, treeiters = selection.get_selected_rows()

        result_text = ""
        for i, treeiter in enumerate(treeiters):
            if i != 0:
                result_text += "\n"

            result_text += model[treeiter][0]

        self._result_translation_textbuf.set_text(result_text)

    def onCardTypeChanged(self, *_):
        word_info = self._get_info_for_current_word()
        combo_iter = self._card_type_combobox.get_active_iter()
        if combo_iter is not None:
            model = self._card_type_combobox.get_model()
            card_type = model[combo_iter][0]
            word_info.card_type = card_type

    def onSyncButtonPressed(self, *_):
        self._model.save_translation_variants()

    def onShow(self, *_):
        self._on_show_callback()

    def onDestroy(self, *_):
        self._on_destroy_callback()


class OrgGoldSyncProcessingWindowService:
    """Class that opens window if required and reopens until errors will
       be no fixed, or window closed without them."""
    def __init__(
        self,
        model: OrgGoldSyncModel,
        notify_service: NotificationService,
    ):
        self._model = model
        self._notify_service = notify_service

        self._lock = threading.Lock()
        self._cancel_request_was = False
        self._is_window_visible = False
        self._path_to_window_markup = os.path.join(
            os.path.dirname(__file__),
            "OrgGoldSync.glade"
        )

    def _on_show(self):
        with self._lock:
            self._is_window_visible = True
            logging.info("Window showed")

    def _on_destroy(self):
        with self._lock:
            self._is_window_visible = False
            self._cancel_request_was = False
            self._model.unsubscribe_from_changing(self._presenter)
            logging.info("Window destroyed")

    def toggle(self):
        with self._lock:
            if self._is_window_visible:
                logging.debug("Attempt to hide window")

                if self._cancel_request_was:
                    return
                else:
                    self._cancel_request_was = True

                GLib.idle_add(self._window.destroy)
            else:
                logging.debug("Attempt to show window")

                builder = Gtk.Builder()
                builder.add_from_file(self._path_to_window_markup)

                logging.debug("Window markup loaded from file")

                self._window = builder.get_object("OrgGoldSync_Window")
                self._presenter = OrgGoldSyncProcessingWindowPresenter(
                    builder,
                    self._model,
                    self._on_show,
                    self._on_destroy
                )
                builder.connect_signals(self._presenter)
                logging.debug("Window initialized and ready to use")

                self._model.subscribe_on_changing(self._presenter)
                GLib.idle_add(self._window.show_all)


class TrayIconPixmap:
    def __init__(self, path_to_icon: str, icon_desc) -> None:
        self._width, self._height, self._pixmap = self._load_icon(path_to_icon)
        self._desc = icon_desc

    def _load_icon(self, path: str) -> Tuple[int, int, bytes]:
        """Load icon and convert it to and ARGB byte array."""
        icon = Image.open(path).convert('RGBA')

        arr = bytearray()
        for i in range(icon.width):
            for j in range(icon.height):
                r, g, b, a = icon.getpixel((i, j))
                arr.append(a)
                arr.append(r)
                arr.append(g)
                arr.append(b)

        return (icon.width, icon.height, bytes(arr))

    @property
    def width(self) -> int:
        return self._width

    @property
    def height(self) -> int:
        return self._height

    @property
    def description(self) -> str:
        return self._desc

    @property
    def pixmap(self) -> bytes:
        return self._pixmap


class TrayDbusService(dbus_next.service.ServiceInterface,
                      ITranslationsDependent):
    """Interface implemented after reading of dbus-status-notifier-item.xml
    from protocol directory of Waybar repo."""

    def __init__(self,
                 bus: MessageBus,
                 model: OrgGoldSyncModel,
                 window_job: OrgGoldSyncProcessingWindowService) -> None:
        super().__init__("org.kde.StatusNotifierItem")

        self._bus = bus
        self._model = model
        self._window_job = window_job

        self._path_to_active_state_icon = os.path.join(
            os.path.dirname(__file__),
            "ActiveIcon.png"
        )
        self._active_icon = TrayIconPixmap(self._path_to_active_state_icon,
                                           "There are new unprocessed words")

        self._path_to_inactive_state_icon = os.path.join(
            os.path.dirname(__file__),
            "InactiveIcon.png"
        )
        self._inactive_icon = TrayIconPixmap(self._path_to_inactive_state_icon,
                                             "No new words to process")

        self._cur_icon = self._active_icon

    def _register_on_bus(self, bus: MessageBus):
        bus.export("/StatusNotifierItem", self)
        bus_name = "org.freedesktop.StatusNotifierItem-{}-OrgGoldSyncGUI".format(
            os.getpid()
        )
        bus.request_name_sync(bus_name)

        def register_tray():
            logging.info("Tray registration started")

            def on_notifier_host_unregistered():
                logging.info("Status notifier watcher unregistered")
                GLib.idle_add(register_tray)

            watcher_introspection = bus.introspect_sync(
                "org.kde.StatusNotifierWatcher",
                "/StatusNotifierWatcher"
            )
            watcher_obj = bus.get_proxy_object(
                "org.kde.StatusNotifierWatcher",
                "/StatusNotifierWatcher",
                watcher_introspection
            )
            watcher_interface = watcher_obj.get_interface(
                "org.kde.StatusNotifierWatcher"
            )
            getattr(watcher_interface, "call_register_status_notifier_item_sync")(
                bus_name
            )

            getattr(watcher_interface, "on_status_notifier_host_registered")(
                on_notifier_host_unregistered
            )

            logging.info("Tray registered on bus %s", bus_name)

        register_tray()

    def initialize(self):
        self._register_on_bus(self._bus)

    def _switch_to_inactive_state(self):
        self._cur_icon = self._inactive_icon
        self.NewIcon()
        self.NewTitle()

    def _switch_to_active_state(self):
        self._cur_icon = self._active_icon
        self.NewIcon()
        self.NewTitle()

    def on_translations_changing(self):
        if len(self._model.get_words_to_process()) > 0:
            GLib.idle_add(self._switch_to_active_state)
        else:
            GLib.idle_add(self._switch_to_inactive_state)

    @dbus_next.service.method()
    def Activate(self, x: "i", y: "i"):  # type: ignore
        logging.debug("Click on tray icon received")
        GLib.idle_add(self._window_job.toggle)

    @dbus_next.service.dbus_property(access=PropertyAccess.READ)
    def Category(self) -> "s":  # type: ignore
        return "ApplicationStatus"

    @dbus_next.service.dbus_property(access=PropertyAccess.READ)
    def Id(self) -> "s":  # type: ignore
        return "OrgGoldSync"

    @dbus_next.service.dbus_property(access=PropertyAccess.READ)
    def Title(self) -> "s":  # type: ignore
        return self._cur_icon.description

    @dbus_next.service.dbus_property(access=PropertyAccess.READ)
    def Status(self) -> "s":  # type: ignore
        return "Active"

    @dbus_next.service.dbus_property(access=PropertyAccess.READ)
    def IconPixmap(self) -> "a(iiay)":  # type: ignore
        return [[self._cur_icon.width,
                 self._cur_icon.height,
                 self._cur_icon.pixmap]]

    @dbus_next.service.dbus_property(access=PropertyAccess.READ)
    def ItemIsMenu(self) -> "b":  # type: ignore
        return False

    @dbus_next.service.signal()
    def NewTitle(self):
        logging.debug("Tray title changed")

    @dbus_next.service.signal()
    def NewIcon(self):
        logging.debug("Tray icon changed")


def idle_add(func: Callable):
    GLib.idle_add(func)


def gui_main():
    Gtk.main()


def quit_gui_main():
    Gtk.main_quit()
