import re
import logging
import threading
from pathlib import Path
from typing import List

from watchdog.events import FileSystemEventHandler, FileModifiedEvent

from interfaces import IHistoryDependent, IHistoryProvidingService
from notifications import NotificationService
from utils import OrgGoldSyncException


class GoldHistoryService(IHistoryProvidingService, FileSystemEventHandler):
    """Class that handle history file changing."""
    def __init__(
        self,
        path_to_history_file: Path,
        notify_service: NotificationService
    ):
        self._lock = threading.RLock()
        self._notify_service = notify_service

        self._words = self._read_words_from_history_file(path_to_history_file)
        self._history_dependents: List[IHistoryDependent] = []

        super().__init__()

    @property
    def words(self) -> List[str]:
        return self._words

    def subscribe_on_changing(self, obj: IHistoryDependent):
        with self._lock:
            self._history_dependents.append(obj)

    def unsubscribe_from_changing(self, obj: IHistoryDependent):
        with self._lock:
            maybe_idx = next((
                i
                for i, dep in enumerate(self._history_dependents)
                if dep is obj
            ), None)
            if maybe_idx is None:
                raise OrgGoldSyncException(
                    "Invaid attemp to unsubscribe from translations changing"
                )

            self._history_dependents.pop(maybe_idx)

    def _get_word_from_line(self, history_line) -> str:
        match_obj = re.match(r"(?P<number>[0-9]+)\s+(?P<word>.+)",
                             history_line)
        if match_obj is None:
            logging.error("Can't parse history line '%s'", history_line)
            raise OrgGoldSyncException(
                f"Can't parse history line '{history_line}'"
            )

        return match_obj["word"].strip()

    def _read_words_from_history_file(self, path: Path) -> List[str]:
        with open(path, "r", encoding="utf-8") as hist:
            history_content = hist.read()
            trimmed_lines = [line.strip()
                             for line in history_content.split("\n")]

        words = []
        for line in trimmed_lines:
            if line:
                word = self._get_word_from_line(line)
                words.append(word)

        logging.info("History file successfully read")
        return words

    def on_modified(self, event: FileModifiedEvent):
        logging.info("Goldendict's history file changed")
        with self._lock:
            self._words = self._read_words_from_history_file(event.src_path)

        for obj in self._history_dependents:
            obj.on_history_reload()
