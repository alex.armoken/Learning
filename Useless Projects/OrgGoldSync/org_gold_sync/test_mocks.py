from typing import List, Dict

from interfaces import (ITranslationsDependent,
                        ITranslationsProvidingService,
                        IHistoryDependent,
                        IHistoryProvidingService,
                        IOrgDependent,
                        IOrgProvidingService)
from model import LanguagePair, WordInfo


class TestTranslationsProvidingService(ITranslationsProvidingService):
    def __init__(self) -> None:
        self._dependents: List[ITranslationsDependent] = []

    def get_words_translations(
        self,
        _: List[str]
    ) -> Dict[str, Dict[LanguagePair, List[str]]]:
        return {
            "arrange": {
                LanguagePair("English", "Russian"): ["размещать"]
            },
            "mangle": {
                LanguagePair("English", "Russian"): ["рубить"],
                LanguagePair("English", "English"): [
                    "To ruin or spoil through ineptitude or ignorance"
                ]
            },
            "arrange": {
                LanguagePair("English", "Russian"): [
                    "To arrange a group of things or people",
                    "To carefully or secretly arrange something"
                ]
            },
            "ломать": {
                LanguagePair("Russian", "English"): ["break"],
                LanguagePair("Russian", "Russian"): [
                    "Сгибая",
                    "Перегибая или сильно ударяя",
                    "Разделять что-либо на части"
                ]
            },
            "возникать": {
                LanguagePair("Russian", "English"): [
                    "emerge",
                    "arise"
                ],
                LanguagePair("Russian", "Russian"): [
                    "Появляться",
                    "Начинаться",
                    "Завязываться"
                ]
            },
            "исправность": {
                LanguagePair("Russian", "English"): [
                    "operability",
                    "regularity",
                    "repair"
                ],
                LanguagePair("Russian", "Russian"): [
                    "Бесперебойность",
                    "Безотказность"
                ]
            },
            "people": {
                LanguagePair(None, None): []
            },
            "ascend": {
                LanguagePair(None, None): []
            }
        }

    def subscribe_on_changing(self, _: ITranslationsDependent):
        pass

    def unsubscribe_from_changing(self, _: ITranslationsDependent):
        pass


class TestHistoryService(IHistoryProvidingService):
    def __init__(self) -> None:
        super().__init__()

    @property
    def words(self) -> List[str]:
        return [
            "arrange",
            "mangle",
            "возникать",
            "people",
            "ascend",
            "ломать",
            "исправность"
        ]

    def subscribe_on_changing(self, _: IHistoryDependent):
        pass

    def unsubscribe_from_changing(self, _: IHistoryDependent):
        pass


class TestOrgProvidingService(IOrgProvidingService):
    def __init__(self) -> None:
        pass

    @property
    def words(self) -> List[str]:
        return [
            "ломать",
            "mangle"
        ]

    def subscribe_on_changing(self, _: IOrgDependent):
        pass

    def unsubscribe_from_changing(self, _: IOrgDependent):
        pass

    def save_translation_variants(self, _: List[WordInfo]):
        pass
