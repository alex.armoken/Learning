import logging
import threading
from typing import List, Dict, Optional
from collections import defaultdict

import rpyc
import rpyc.core.protocol

from interfaces import (ITranslationsDependent,
                        ITranslationsProvidingService,
                        IDictionariesDependent,
                        IDictProvidingService)
from model import LanguagePair
from utils import OrgGoldSyncException
from notifications import NotificationType, NotificationService


@rpyc.service
class TranslationsProvidingRPCService(rpyc.Service,
                                      IDictionariesDependent):
    def __init__(
        self,
        dict_providing_service: IDictProvidingService,
        notify_service: NotificationService
    ) -> None:
        self._lock = threading.Lock()

        self._dict_providing_service = dict_providing_service
        dict_providing_service.subscribe_on_changing(self)

        self._notify_service = notify_service

        self._client_service: Optional[TranslationsProvidingRPCClient] = None

        super().__init__()

    def on_dicts_changing(self):
        with self._lock:
            assert self._client_service is not None
            self._client_service.on_dictionaries_reloaded()

            logging.info("Dictionaries re-loaded")

    def on_connect(self, connection: rpyc.core.protocol.Connection):
        self._client_service = connection.root

    @rpyc.exposed
    def get_words_translations(
        self,
        words: List[str]
    ) -> Dict[str, Dict[LanguagePair, List[str]]]:
        with self._lock:
            result = defaultdict(dict)
            for word in words:
                lang_translations_map = self._dict_providing_service.get_word_translations(word)

                result[word] = lang_translations_map

            return result


@rpyc.service
class TranslationsProvidingRPCClient(rpyc.Service, ITranslationsProvidingService):
    """RPC client for dictionary providing service that works in
       separated process."""
    def __init__(self) -> None:
        self._server_service: Optional[TranslationsProvidingRPCService] = None
        self._translations_dependents: List[ITranslationsDependent] = []

        super().__init__()

    def on_connect(self, connection: rpyc.core.protocol.Connection):
        self._server_service = connection.root

    @rpyc.exposed
    def on_dictionaries_reloaded(self):
        for dependent in self._translations_dependents:
            dependent.on_translations_changing()
    def get_words_translations(
        self,
        words: List[str]
    ) -> Dict[str, Dict[LanguagePair, List[str]]]:
        assert self._server_service is not None
        return self._server_service.get_words_translations(words)

    def subscribe_on_changing(self, obj: ITranslationsDependent):
        self._translations_dependents.append(obj)

    def unsubscribe_from_changing(self, obj: ITranslationsDependent):
        maybe_idx = next((
            i
            for i, dep in enumerate(self._translations_dependents)
            if dep is obj
        ), None)
        if maybe_idx is None:
            raise OrgGoldSyncException(
                "Invaid attemp to unsubscribe from translations changing"
            )

        self._translations_dependents.pop(maybe_idx)
