import dataclasses
import threading
from typing import Optional, List, Dict, Sequence

from interfaces import (SubscriptionManager,
                        IHistoryDependent,
                        IHistoryProvidingService,
                        ITranslationsDependent,
                        ITranslationsProvidingService,
                        IOrgDependent,
                        IOrgProvidingService)
from utils import OrgGoldSyncException


@dataclasses.dataclass(frozen=True)
class LanguagePair:
    """Class that should be used as dict's key."""
    source: Optional[str]
    target: Optional[str]


@dataclasses.dataclass
class WordInfo:
    """Collected information about word loaded from
       Goldendict's history file."""
    word: str
    translations: Dict[LanguagePair, List[str]]
    card_type: Optional[str] = None
    edited_by_user: bool = False
    user_edited_translation: str = ""

    def get_language_pairs_of_translations(self) -> List[LanguagePair]:
        return [pair for pair in self.translations.keys()]

    def get_languages_of_translations(self) -> List[str]:
        languages = set()
        for pair in self.get_language_pairs_of_translations():
            languages.add(pair.source)
            languages.add(pair.target)

        return list(languages)


class OrgGoldSyncModel(IHistoryDependent, ITranslationsDependent, IOrgDependent):
    """Class that contains all data in process that interacts with user."""
    def __init__(
        self,
        org_service: IOrgProvidingService,
        dict_service: ITranslationsProvidingService,
        history_service: IHistoryProvidingService
    ) -> None:
        self._lock = threading.RLock()

        self._translations_dependents: List[ITranslationsDependent] = []

        self._org_service = org_service
        self._org_service.subscribe_on_changing(self)

        self._dict_service = dict_service

        self._history_service = history_service
        self._history_service.subscribe_on_changing(self)

        self._words_to_process: Optional[List[WordInfo]] = None

    def _get_words_to_process(self) -> List[str]:
        history_words = self._history_service.words
        org_words = self._org_service.words

        words_to_process = list(set(history_words) - set(org_words))

        return words_to_process

    def _get_info_for_words(self, words: List[str]) -> List[WordInfo]:
        all_translations = self._dict_service.get_words_translations(words)

        result = []
        for word, translations_map in all_translations.items():
            result.append(
                WordInfo(
                    word,
                    translations_map
                )
            )

        return result

    def _load_words(self):
        words_to_process = self._get_words_to_process()
        if self._words_to_process is None:
            self._words_to_process = self._get_info_for_words(words_to_process)
        else:
            already_loaded_words = [info.word for info in self._words_to_process]
            words_to_add = list(set(words_to_process) - set(already_loaded_words))

            words_info_to_add = self._get_info_for_words(words_to_add)
            self._words_to_process.extend(words_info_to_add)

    def get_words_to_process(self) -> Sequence[WordInfo]:
        with self._lock:
            if self._words_to_process is None:
                self._load_words()

            assert self._words_to_process is not None
            return self._words_to_process

    def save_translation_variants(self):
        with self._lock:
            assert self._words_to_process is not None
            self._org_service.save_translation_variants(self._words_to_process)

    def _update_subscribers(self):
        for dep in self._translations_dependents:
            dep.on_translations_changing()

    def on_history_reload(self):
        with self._lock:
            self._load_words()
            self._update_subscribers()

    def on_org_reload(self):
        with self._lock:
            self._load_words()
            self._update_subscribers()

    def on_translations_changing(self):
        with self._lock:
            assert self._words_to_process is not None
            words = [info.word for info in self._words_to_process]
            all_translations = self._dict_service.get_words_translations(words)

            # Replace translations by newer
            for word, translations_map in all_translations.items():
                word_info = next(
                    info
                    for info in self._words_to_process
                    if info.word == word
                )

                word_info.translations = translations_map

            self._update_subscribers()

    def subscribe_on_changing(self, obj: ITranslationsDependent):
        with self._lock:
            self._translations_dependents.append(obj)

    def unsubscribe_from_changing(self, obj: ITranslationsDependent):
        with self._lock:
            maybe_idx = next((
                i
                for i, dep in enumerate(self._translations_dependents)
                if dep is obj
            ), None)
            if maybe_idx is None:
                raise OrgGoldSyncException(
                    "Invaid attemp to unsubscribe from translations changing"
                )

            self._translations_dependents.pop(maybe_idx)

    def with_obj(self, obj: ITranslationsDependent) -> SubscriptionManager:
        return SubscriptionManager(
            self.subscribe_on_changing,
            self.unsubscribe_from_changing,
            obj
        )
