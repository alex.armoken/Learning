import re
import json
import threading
import subprocess
import logging
from collections import defaultdict
from pathlib import Path
from concurrent.futures import ThreadPoolExecutor
from typing import List, Dict, Optional

import bs4
from watchdog.events import FileSystemEventHandler

from model import LanguagePair
from interfaces import IDictionariesDependent, IDictProvidingService
from notifications import NotificationService
from utils import OrgGoldSyncException


def extract_from_flat_heirarchy(text_parts: List[str]) -> Optional[List[str]]:
    result = []
    for part in text_parts:
        match_obj = re.match(r"[\d\w]+\)\s+(?P<translation>[\.\s\w\,;\-]+)",
                             part)
        if match_obj is None:
            continue

        result.append(match_obj["translation"].strip())

    return result


def extract_from_two_level_heirarchy_with_transcription(
    text_parts: List[str]
) -> Optional[List[str]]:
    result = []
    for part in text_parts:
        match_obj = re.match(r"[\d\w]+\)\s+(?P<translation>[\.\s\w\,;\-]+)",
                             part)
        if match_obj is None:
            continue

        result.append(match_obj["translation"].strip())

    return result


def extract_from_three_level_heirarchy_with_transcription(
    text_parts: List[str]
) -> Optional[List[str]]:
    result = []
    for part in text_parts:
        match_obj = re.match(r"[\d\w]+\)\s+(?P<translation>[\.\s\w\,;\-]+)",
                             part)
        if match_obj is None:
            continue

        result.append(match_obj["translation"].strip())

    return result


def extract_translations_from_sdcv_query(definitions: str) -> List[str]:
    soup = bs4.BeautifulSoup(definitions)
    text_parts = soup.findAll(text=True)

    methods = [
        extract_from_flat_heirarchy,
        extract_from_two_level_heirarchy_with_transcription,
        extract_from_three_level_heirarchy_with_transcription
    ]
    for method in methods:
        maybe_result = method(text_parts)
        if maybe_result is not None:
            return maybe_result

    return []


class StarDictDictsSynchronizer(FileSystemEventHandler, IDictProvidingService):
    """Class that handle Goldendict's config changing and serves as
       dictionaries providing service."""
    def __init__(
        self,
        path_to_org_gold_sync_cfg: Path,
        executor: ThreadPoolExecutor,
        notify_service: NotificationService,
    ):
        self._path_to_org_gold_sync_cfg = path_to_org_gold_sync_cfg
        self._executor = executor
        self._notify_service = notify_service

        self._lock = threading.RLock()
        self._dicts_dependents: List[IDictionariesDependent] = []

        super().__init__()

    def _get_path_to_dicts_dir(self) -> Path:
        with self._path_to_org_gold_sync_cfg.open("r") as f:
            cfg = json.load(f)

        return cfg["PathToDirectoryWithStarDictDicts"]

    def _run_sdcv_to_query_translations(self, word: str) -> Dict:
        path_to_dicts_dir = self._get_path_to_dicts_dir()
        sdcv_process = subprocess.Popen(
            args=[
                "sdcv",
                "--non-interactive",
                "--json",
                "--data-dir",
                path_to_dicts_dir,
                word
            ],
            stdout=subprocess.PIPE
        )
        stdout, _ = sdcv_process.communicate()

        return json.loads(stdout)

    def _get_lang_pair_from_dict_name(self, dict_name: str) -> LanguagePair:
        match_obj = re.match(r".*\s+\((?P<from>\w+)\-(?P<to>\w+)\)",
                             dict_name)
        if match_obj is None:
            logging.error("Can't parse dictionary name '%s'", dict_name)
            raise OrgGoldSyncException(
                f"Can't parse dictionary name '{dict_name}'"
            )

        return LanguagePair(
            match_obj["from"].strip(),
            match_obj["to"].strip()
        )

    def _extract_from_comma_and_colo_separated_string(
        self,
        text_parts: List[str]
    ) -> Optional[List[str]]:
        result = []
        for part in text_parts:
            match_obj = re.match(r"[\d\w]+\)\s+(?P<translation>[\.\s\w\,;\-]+)",
                                 part)
            if match_obj is None:
                continue

            result.append(match_obj["translation"].strip())

        return result

    def get_word_translations(
        self,
        word: str
    ) -> Dict[LanguagePair, List[str]]:
        with self._lock:
            sdcv_result = self._run_sdcv_to_query_translations(word)

            result = defaultdict(list)
            for translation_group in sdcv_result:
                dict_name = translation_group["dict"]
                lang_pair = self._get_lang_pair_from_dict_name(dict_name)

                definitions = translation_group["definition"]
                translations = extract_translations_from_sdcv_query(
                    definitions
                )

                result[lang_pair].extend(translations)

            return result

    def on_modified(self, _):
        for config_dependent in self._dicts_dependents:
            config_dependent.on_dicts_changing()

    def subscribe_on_changing(self, obj: IDictionariesDependent):
        with self._lock:
            self._dicts_dependents.append(obj)

    def unsubscribe_from_changing(self, obj: IDictionariesDependent):
        with self._lock:
            maybe_idx = next((
                i
                for i, dep in enumerate(self._dicts_dependents)
                if dep is obj
            ), None)
            if maybe_idx is None:
                raise OrgGoldSyncException(
                    "Invaid attemp to unsubscribe from dictionaries changing"
                )

            self._dicts_dependents.pop(maybe_idx)
