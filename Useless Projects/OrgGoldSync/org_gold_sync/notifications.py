import logging
from enum import Enum

from dbus_next.signature import Variant
from dbus_next.glib.message_bus import MessageBus

from utils import OrgGoldSyncException


class NotificationType(Enum):
    Info = 0
    Warn = 1
    Error = 2


class NotificationService:
    def __init__(self, bus: MessageBus) -> None:
        introspection = bus.introspect_sync("org.freedesktop.Notifications",
                                            "/org/freedesktop/Notifications")
        obj = bus.get_proxy_object("org.freedesktop.Notifications",
                                   "/org/freedesktop/Notifications",
                                   introspection)

        self._notify_interface = obj.get_interface("org.freedesktop.Notifications")

    def _get_icon_name(self, notification_type: NotificationType) -> str:
        if notification_type == NotificationType.Error:
            return "dialog-error"
        elif notification_type == NotificationType.Info:
            return "dialog-info"
        elif notification_type == NotificationType.Warn:
            return "dialog-warning"
        else:
            raise OrgGoldSyncException("Unsupported notification type!")

    def send_notification(self,
                          msg: str,
                          notification_type: NotificationType):
        icon_name = self._get_icon_name(notification_type)
        getattr(self._notify_interface, "call_notify_sync")(
            "anki-gold-sync", 0, icon_name,
            "Can't add cards to Org!", msg,
            [], {"urgency": Variant("i", notification_type.value)}, 3000
        )

        logging.warning("User notification sent")
