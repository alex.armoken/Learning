import sys
import argparse


def init_argparser():
    parser = argparse.ArgumentParser(description="PyArbo")
    parser.add_argument("--input", "-f", action="store", nargs=1, type=str)
    parser.add_argument("--out", "-o", action="store", nargs=1, type=str)
    return parser


def main():
    pass
# parser = init_argparser()
# args = parser.parse_args()
# if args.file:
# data = file(args.file).readlines()


if __name__ == "__main__":
    sys.exit(main())
