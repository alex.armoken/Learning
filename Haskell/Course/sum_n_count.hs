sum'n'count :: Integer -> (Integer, Integer)
sum'n'count x | x == 0 = (0, 1)
              | otherwise = helper 0 0 x
  where
    helper cur_sum cur_count 0 = (cur_sum, cur_count)
    helper cur_sum cur_count x_rest = helper (cur_sum + (abs (rem x_rest 10))) (cur_count + 1) (quot x_rest 10)
