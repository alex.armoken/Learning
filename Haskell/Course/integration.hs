integration :: (Double -> Double) -> Double -> Double -> Double
integration f a b = h * (middle_value + partial_sum) where
  parts_count = 1000
  h = (b - a) / parts_count
  middle_value = (f a + f b) / 2
  partial_sum = part_sum_helper f (a + h) h 0 (parts_count - 2) where
    part_sum_helper func start_value step sum 0 = func start_value + sum
    part_sum_helper func start_value step sum parts_count = part_sum_helper func (start_value + step) step (sum + func start_value) (parts_count - 1)
