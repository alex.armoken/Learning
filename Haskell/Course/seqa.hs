seqA :: Integer -> Integer
seqA n | n == 0 = 1
       | n == 1 = 2
       | n == 2 = 3
       | otherwise = let
                       helper cur_index a1 a2 a3 | cur_index == n = a3
                                                 | otherwise = helper (cur_index + 1) a2 a3 (a3 + a2 - (2 * a1))
                     in helper 2 1 2 3
