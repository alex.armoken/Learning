fibonacci :: Integer -> Integer
fibonacci n | n > 0 = fib_helper 1 n 0 1
            | n < 0 = neg_fib_helper (-1) n 0 1
            | otherwise = n

fib_helper :: Integer -> Integer -> Integer -> Integer -> Integer
fib_helper cur_index target_index a b | cur_index == target_index = b
                                      | otherwise = fib_helper (cur_index + 1) target_index b (a + b)

neg_fib_helper :: Integer -> Integer -> Integer -> Integer -> Integer
neg_fib_helper cur_index target_index a b | cur_index == target_index = b
                                          | otherwise = neg_fib_helper (cur_index - 1) target_index b (a - b)
