fibonacci :: Integer -> Integer
fibonacci n | n == -1 = 1
            | n < 0 = ((-1) ^ (abs n - 1)) * fibonacci (abs n)
            | n == 0 = 0
            | n == 1 = 1
            | otherwise = fibonacci (n - 1) + fibonacci (n - 2)

-- Not my
nm_fibonacci :: Integer -> Integer
nm_fibonacci n | n > 1     = nm_fibonacci (n - 1) + nm_fibonacci (n - 2)
               | n < 0     = nm_fibonacci (n + 2) - nm_fibonacci (n + 1)
               | otherwise = n
