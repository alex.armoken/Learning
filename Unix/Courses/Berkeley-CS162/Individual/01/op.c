#include "stdio.h"
#include "stdint.h"
#include "unistd.h"
#include "stdbool.h"

enum state_t {
    START_STATE,
    SPACE_STATE,
    TEXT_STATE,
    NEW_LINE_STATE,
    EOF_STATE
} typedef state;

typedef struct result_t {
    uint64_t bytes_count;
    uint64_t words_count;
    uint64_t lines_count;
} result;

typedef void (*transition_callback)(result *r);


void start_to_text(result *r) {
    r->bytes_count++;
}

void start_to_eof(result *r) {
    printf("Nothin to read!");
}

void start_to_space(result *r) {
    r->bytes_count++;
}

void start_to_new_line(result *r) {
    r->bytes_count++;
}


void text_to_space(result *r) {
    r->bytes_count++;
    r->words_count++;
}

void text_to_new_line(result *r) {
    r->bytes_count++;
    r->words_count++;
}

void text_to_text(result *r) {
    r->bytes_count++;
}

void text_to_eof(result *r) {
    r->words_count++;
    r->lines_count++;
    printf("Reading is done.");
}


void new_line_to_new_line(result *r) {
    r->bytes_count++;
    r->lines_count++;
}

void new_line_to_text(result *r) {
    r->bytes_count++;
    r->lines_count++;
}

void new_line_to_space(result *r) {
    r->bytes_count++;
    r->lines_count++;
}

void new_line_to_eof(result *r) {
    r->lines_count++;
    printf("Reading is done.");
}


void space_to_new_line(result *r) {
    r->bytes_count++;
}

void space_to_text(result *r) {
    r->bytes_count++;
}

void space_to_space(result *r) {
    r->bytes_count++;
}

void space_to_eof(result *r) {
    r->lines_count++;
}


transition_callback transition_table[5][5] = {
    [START_STATE][EOF_STATE] = start_to_eof,
    [START_STATE][TEXT_STATE] = start_to_text,
    [START_STATE][SPACE_STATE] = start_to_space,
    [START_STATE][NEW_LINE_STATE] = start_to_new_line,

    [TEXT_STATE][EOF_STATE] = text_to_eof,
    [TEXT_STATE][TEXT_STATE] = text_to_text,
    [TEXT_STATE][SPACE_STATE] = text_to_space,
    [TEXT_STATE][NEW_LINE_STATE] = text_to_new_line,

    [SPACE_STATE][EOF_STATE] = space_to_eof,
    [SPACE_STATE][TEXT_STATE] = space_to_text,
    [SPACE_STATE][SPACE_STATE] = space_to_space,
    [SPACE_STATE][NEW_LINE_STATE] = space_to_new_line,

    [NEW_LINE_STATE][EOF_STATE] = new_line_to_eof,
    [NEW_LINE_STATE][TEXT_STATE] = new_line_to_text,
    [NEW_LINE_STATE][SPACE_STATE] = new_line_to_space,
    [NEW_LINE_STATE][NEW_LINE_STATE] = new_line_to_new_line
};


state get_state(char ch) {
    switch (ch) {
        case ' ': {
            return SPACE_STATE;
        }
        case '\n': {
            return NEW_LINE_STATE;
        }
        default:
            return TEXT_STATE;
    }
}

int main() {
    char ch;
    result r = {0, 0, 0};
    state cur_state = START_STATE;
    while (read(STDIN_FILENO, &ch, 1) > 0) {
        state new_state = get_state(ch);
        transition_table[cur_state][new_state](&r);
        cur_state = new_state;
    }
    transition_table[cur_state][EOF_STATE](&r);

    printf("%7ld %7ld %7ld\n", r.lines_count, r.words_count, r.bytes_count);
    return 0;
}
