#!/bin/bash

pattern="(^|:)$2(:|$)"
total_size=0
files_count=0

print_folder_recurse() {
    for i in "$1"/*;do
        if [[ -d "$i" ]]; then
            print_folder_recurse "$i"
        elif [[ "$i" =~ $pattern ]]; then
			total_size=`expr $total_size + $(du -b "$i" | awk '{print $1}')`
			files_count=$(($files_count + 1))
            echo $i
        fi
    done
}

short_size() {
	if [[ $1_size < 10000 ]]; then
		result="$1 B"
	else
		result=`bc -l <<< "$1 / 1024"`
		result=`printf "%.3f Kb" $result`
	fi
	echo $result
}

# try get path from param
path=""
if [ -d "$1" ]; then
    path=$1;
else
    path="/tmp"
fi

print_folder_recurse $path
middle_size=$(short_size $(($total_size / $files_count)))
total_size=$(short_size $total_size)
echo Total size: $total_size, files count: $files_count, middle file size: $middle_size
