#!/bin/bash

data=`cat - | sed -e "/^$/D"`
data=`echo "$data" | sed -e "${1}q"`

echo "$data"
