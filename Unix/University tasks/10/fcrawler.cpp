#include <mutex>
#include <queue>
#include <regex>
#include <tuple>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>

#include <getopt.h>
#include <curl/curl.h>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/lockfree/queue.hpp>
#include <Poco/Net/FTPClientSession.h>

#include "ftpparse.hpp"

using namespace boost::asio;

struct global_args_t {
    char *url;
    char *path;
} global_args;

struct FtpFile {
    const char *filename;
    FILE *stream;
};

static struct option OPTS[] = {
    {"url", required_argument, NULL, 'u'},
    {"path", required_argument, NULL, 'p'},
    {0, 0, 0, 0}
};

volatile sig_atomic_t is_alive = 1;

typedef boost::system::error_code error_code;
typedef boost::shared_ptr<ip::tcp::socket> tcp_socket_ptr;

const std::string USERNAME("anonymous");
const std::string PASSWORD("topkek");
const Poco::UInt16 FTP_PORT = 21;

void sigint_handler(int sig)
{
    std::cout << "SIGINT handled!\n" << std::endl;
    is_alive = 0;
}

int parse_arguments(int argc, char *argv[])
{
    int c;
    int opt_idx;
    const char *args = "u:p:";
    global_args.url = NULL;
    global_args.path = NULL;

    while ((c = getopt_long(argc, argv, args, OPTS, &opt_idx)) != -1) {
        switch (c) {
        case 'p':
            global_args.path = optarg;
            break;
        case 'u':
            global_args.url = optarg;
            break;
        default:
            return -1;
        }
    }
    return 0;
}

bool check_args_logic()
{
    if (global_args.url == NULL) {
        std::cout << "Set url" << std::endl;
        return false;
    } else if (global_args.path == NULL) {
        std::cout << "Set save path" << std::endl;
        return false;
    }
    return true;
}

bool download_file(Poco::Net::FTPClientSession &session,
                   const std::string &ftp_path,
                   std::string dir_path)
{
    try {
        std::istream &stream = session.beginDownload(ftp_path);

        char buf[1024];
        std::streamsize n;
        stream.read(buf, 1024);
        std::ofstream out(dir_path + ftp_path);
        while (stream || (n = stream.gcount()) != 0) {
            out.write(buf, n);
            if (n) {
                stream.read(buf, 1024);
            }
        }

        session.endDownload();
    } catch (...) {
        return false;
    }
    return true;
}

bool is_valid_directory(const std::string &path)
{
    const std::string symbolic_link = " -> ";
    if (path.find(symbolic_link) != std::string::npos) {
        return false;
    }
    return true;
}

void process_directory_listing(std::vector<std::string*> &tokens,
                               const std::string &ftp_path,
                               std::mutex &m,
                               std::queue<std::string*> &q)
{
    for (auto *it: tokens) {
        struct ftpparse fp;
        ftpparse(&fp, (char*)it->c_str(), it->length() + 1);
        if (is_valid_directory(std::string(fp.name))) {
            std::string *name = new std::string(ftp_path + "/" + fp.name);
            name->erase(std::remove(name->begin(), name->end(), '\r'), name->end());
            std::cout << *name << "\n";
            m.lock();
            q.push(name);
            m.unlock();
        }
    }
}

bool list_directory(Poco::Net::FTPClientSession &session,
                    const std::string &ftp_path,
                    const std::string &file_path,
                    std::mutex &m, std::queue<std::string*> &q)
{
    try {
        std::istream &stream = session.beginList(ftp_path, true);

        std::vector<std::string*> tokens;
        std::string *line = new std::string();
        while(std::getline(stream , *line, '\n')) {
            tokens.push_back(line);
            line = new std::string();
        }

        session.endList();
        process_directory_listing(tokens, ftp_path, m, q);
    } catch (...) {
        return false;
    }
    return true;
}

bool process_file(Poco::Net::FTPClientSession &session,
                  const std::string &save_path,
                  std::mutex &q_mtx,
                  std::queue<std::string*> &q)
{
    bool result = false;
    q_mtx.lock();
    std::string *ftp_path = q.front();
    q.pop();
    q_mtx.unlock();
    std::cout << *ftp_path << std::endl;

    if (download_file(session, *ftp_path, save_path)
        || list_directory(session, *ftp_path, save_path, q_mtx, q)) {
        result = true;
    }
    return result;
}

int main_program()
{
    curl_global_init(CURL_GLOBAL_DEFAULT);
    const std::string HOST(global_args.url);
    Poco::Net::FTPClientSession session(HOST, FTP_PORT, USERNAME, PASSWORD);

    std::mutex q_mtx;
    std::mutex ftp_mtx;
    std::queue<std::string*> q;
    q.push(new std::string("/"));

    boost::filesystem::path dir(global_args.path);
    if (!boost::filesystem::exists(dir)) {
        boost::filesystem::create_directory(dir);
    }

    process_file(session, global_args.path, q_mtx, q);

    return 0;
}

int main(int argc, char *argv[])
{
    if (parse_arguments(argc, argv) == -1) {
        return 1;
    } else if (!check_args_logic()) {
        return 2;
    }
    return main_program();
}
