#!/bin/sh

read
echo $REPLY
words=($REPLY)
words_count=${#words[@]}

if [[ $words_count > 3 ]]; then
	echo "Too much data!"
	exit 1
elif [[ $words_count < 3 ]]; then
	echo "Too few data!"
	exit 1
fi

for ((i=0; i < $words_count; i++)); do
	if ! [[ ${words[$i]} =~ ^-?[0-9]+$ ]];
	then
		echo ${words[$i]} " - Not a number!"
		exit 1
	fi
done

a=${words[0]}
b=${words[1]}
c=${words[2]}

D=$(($b * $b - 4 * $a * $c))
echo $D

if (($D < 0)); then
	echo "No real decision!"
elif (($D == 0)); then
	echo "Only on desicion!"
	echo $((-$b / (2 * $a *$c)))
else
	echo "Two desicions!"
	DSqrt=`echo "sqrt($D)" | bc -l`
	echo "x1 = "`echo "-$b - $DSqrt / (2 * $a *$c)" | bc -l`
	echo "x2 = "`echo "-$b + $DSqrt / (2 * $a *$c)" | bc -l`
fi
