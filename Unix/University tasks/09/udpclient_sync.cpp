#include <vector>
#include <iostream>

#include <getopt.h>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

using namespace boost::asio;

struct global_args_t {
    bool is_run_tests;
    char *server_port;
    char *server_address;
    char *message;
} global_args;

static struct option OPTS[] = {
    {"server_port", required_argument, NULL, 'p'},
    {"server_address", required_argument, NULL, 'a'},
    {"message", required_argument, NULL, 'm'},
    {"run_tests", no_argument, NULL, 't'},
    {0, 0, 0, 0}
};

typedef boost::system::error_code error_code;
typedef boost::shared_ptr<ip::tcp::socket> socket_ptr;

int parse_arguments(int argc, char *argv[])
{
    int c;
    int opt_idx;
    const char *args = "ta:p:m:";
    global_args.server_port = NULL;
    global_args.is_run_tests = false;
    global_args.server_address = NULL;

    while ((c = getopt_long(argc, argv, args, OPTS, &opt_idx)) != -1) {
        switch (c) {
        case 'a':
            global_args.server_address = optarg;
            break;
        case 'm':
            global_args.message = optarg;
            break;
        case 'p':
            if (atoi(optarg) != 0) {
                global_args.server_port = optarg;
            }
            break;
        case 't':
            global_args.is_run_tests = true;
            break;
        default:
            return -1;
        }
    }
    return 0;
}

bool check_args_logic()
{
    if (global_args.server_port == NULL) {
        std::cout << "Set server port" << std::endl;
        return false;
    } else if (global_args.server_address == NULL) {
        std::cout << "Set server address" << std::endl;
        return false;
    } else if (global_args.message == NULL) {
        std::cout << "Write message" << std::endl;
        return false;
    }
    return true;
}

void sync_echo_msg(std::string msg, io_service &ios,
                   ip::udp::resolver::query query)
{
    std::vector<uint8_t> buf;
    ip::udp::endpoint sender_ep;
    socket_base::bytes_readable command(true);

    try {
        ip::udp::socket sock(ios, ip::udp::endpoint(ip::udp::v4(), 0));
        ip::udp::resolver resolver(ios);
        ip::udp::resolver::iterator iter = resolver.resolve(query);
        sock.send_to(buffer(msg, msg.length()), *iter);

        sock.receive_from(null_buffers(), sender_ep);
        sock.io_control(command);
        buf.resize(command.get());
        sock.receive_from(buffer(buf, buf.size()), sender_ep);
        std::string echo_msg(buf.begin(), buf.end());

        if (buf.size() != msg.length() || msg != echo_msg) {
            std::cout << "Echo not equal to original msg!: "
                      << echo_msg << std::endl;
        } else {
            std::cout << "Original msg: " << msg << std::endl
                      << "Echoed msg: " << echo_msg << std::endl << std::endl;
        }
    } catch (boost::system::system_error const& error) {
        std::cout << error.what() << std::endl;
    }
}

int main_program()
{
    io_service ios;
    ip::udp::resolver::query query(ip::udp::v4(),
                                   global_args.server_address,
                                   global_args.server_port);
    sync_echo_msg(global_args.message, ios, query);
    return 0;
}

int main(int argc, char *argv[])
{
    if (parse_arguments(argc, argv) == -1) {
        return 1;
    } else if (!check_args_logic()) {
        return 2;
    }
    return main_program();
}
