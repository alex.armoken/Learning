#include "time.h"
#include "stdio.h"
#include "stdlib.h"
#include "stddef.h"
#include "limits.h"
#include "string.h"
#include "unistd.h"

#include "poll.h"
#include "signal.h"
#include "sys/uio.h"
#include "sys/wait.h"

#include "../suplib/aio.h"
#include "../suplib/aproc.h"
#include "../suplib/astring.h"

volatile sig_atomic_t is_alive = 1;

void sigint_handler(int sig)
{
    printf("SIGINT handled!\n");
    is_alive = 0;
}

int poll_read_cycle(struct pollfd* d_for_r, int count)
{
    astring str = {NULL, 0, 0};
    while(is_alive) {
        int ready_count = poll(d_for_r, count, -1);
        if (ready_count == -1) {
            return 1;
        } else if (ready_count > 0) {
            for (int i = 0; i < count; i++) {
                if (d_for_r[i].revents & POLLIN) {
                    READ_STATE st = readline(d_for_r[i].fd, &str);
                    if (st == IO_ERROR || st == NO_MEMORY) {
                        free_astring(&str);
                        return -1;
                    }
                    printf("From %s\n", str.data);
                    str.len = 0;
                }
            }
            printf("----------------------------\n");
        }
    }
    free_astring(&str);
    return 0;
}

static struct pollfd* create_array_for_polling(proc_dp** processes, int count)
{
    struct pollfd* d_for_r = (struct pollfd*)
        malloc(sizeof(struct pollfd) * count);
    if (d_for_r != NULL) {
        for (int i = 0; i < count; i++) {
            d_for_r[i].fd = processes[i]->pipes.readd;
            d_for_r[i].events = POLLIN;
        }
    }
    return d_for_r;
}

int read_data_from_descriptors(proc_dp** processes, int count_of_processes)
{
    int result;
    struct pollfd* d_for_r = NULL;
    char* hostname = (char*)malloc(HOST_NAME_MAX * sizeof(char));
    if (hostname == NULL) {
        printf("Can't alloc memory for hostname!\n");
        goto hostname_memory_alloc_failed;
    } else if (gethostname(hostname, HOST_NAME_MAX) == 1) {
        printf("Can't get hostname!\n");
        goto gethostname_failed;
    }

    d_for_r = create_array_for_polling(processes, count_of_processes);
    if (d_for_r == NULL) {
        goto poll_memory_alloc_failed;
    }

    printf("If you want to stop program, please press Ctrl-C\n");
    result = poll_read_cycle(d_for_r, count_of_processes);
    if (result == -1) {
        goto io_error;
    } else if (result == 1) {
        goto poll_failed;
    }
    printf("Parent exit!\n");
    return 0;

 io_error:
 poll_failed:
    free(d_for_r);
 poll_memory_alloc_failed:
    free(hostname);
 gethostname_failed:
 hostname_memory_alloc_failed:
    printf("Parent exit!\n");
    return -1;
}

// Str must be at lease 21 bytes long.
inline size_t time_to_str(char* str, uint32_t len)
{
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    if (strftime(str, len, "%d.%m.%Y %H:%M:%S\n", t) == 0) {
        return 0;
    }
    return strlen(str);
}

int write_to_descriptor(dup_pipe dp, int proc_num)
{
    char num_txt[10];
    sprintf(num_txt, "%d - ", proc_num);

    const uint32_t time_cap = 21;
    char time_str[time_cap];

    struct iovec line_parts[2] = {{num_txt, strlen(num_txt)}, {time_str, 0}};

    astring pmsg = {NULL, 0, 0}; // Parent message string
    struct pollfd parentd = {dp.readd, POLLIN};

    while (is_alive) {
        sleep(1);
        READ_STATE state;
        if (poll(&parentd, 1, 0)) {
            if ((state = readline(dp.readd, &pmsg)) == IO_ERROR) {
                printf("Child process: IO error!\n");
                is_alive = 0;
                continue;
            } else if (state == NO_MEMORY) {
                printf("Child process: No memory error!\n");
                is_alive = 0;
                continue;
            } else if (strcmp(pmsg.data, "STOP_CHILD") == 0){
                is_alive = 0;
                continue;
            }
        }

        if ((line_parts[1].iov_len = time_to_str(time_str, time_cap)) != 0) {
            ssize_t len = line_parts[0].iov_len + line_parts[1].iov_len;
            if (writev(dp.writed, line_parts, 2) != len) {
                printf("Child process: Pipe writev error!\n");
                is_alive = 0;
            }
        }
    }

    free_astring(&pmsg);
}

void send_kill_msg_to_processes(proc_dp** processes, int count_of_processes)
{
    const char* STOP_MSG = "STOP_CHILD";
    for (int i = 0; i < count_of_processes; i++) {
        write(processes[i]->pipes.writed, STOP_MSG, strlen(STOP_MSG));
        close(processes[i]->pipes.readd);
        close(processes[i]->pipes.writed);
    }
}

int main(int argc, char *argv[])
{
    int piped[2];
    int count_of_processes = 0;
    if (argc != 2) {
        printf("Wrong argument count!\n");
        return 1;
    } else if ((count_of_processes = atoi(argv[1])) == 0) {
        printf("Can't parse first argument!\n");
        return 2;
    }
    signal(SIGINT, sigint_handler);

    int8_t is_child = 0;
    proc_dp** processes = create_processes_dp(&count_of_processes, &is_child);
    if (processes == NULL) {
        printf("Fork failed!\n");
        return 3;
    } else if (is_child == 1) {
        signal(SIGINT, SIG_IGN);
        printf("Child start writing!\n");
        write_to_descriptor(processes[0]->pipes, count_of_processes);
    } else if (is_child == 0) {
        read_data_from_descriptors(processes, count_of_processes);
        send_kill_msg_to_processes(processes, count_of_processes);
        for (size_t i = 0; i < count_of_processes; i++) {
            if (waitpid(processes[i]->pid, NULL, 0) != processes[i]->pid) {
                printf("Parent proc: waitpid error!\n");
            }
            printf("Child proc - %d ended!\n", processes[i]->pid);
        }
    }

    for (int i = 0; i < count_of_processes; i++) {
        free(processes[i]);
    }
    free(processes);

    return 0;
}
