#!/bin/sh

print_subfile()
{
	echo -n "|"
	for ((i=0; i < $1; i++)); do echo -ne "    |"; done
	echo " -- "$2
}

print_subfile 0 `readlink -f $1`
for i in $(ls $1); do
	if test -d "$1/$i"; then
		print_subfile 1 `readlink -f $1/$i`
	fi
done

