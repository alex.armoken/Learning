#!/bin/sh

proc_list=$(ps -o pid= -au $1)
echo "User:" $1 > $2
echo "$proc_list" >> $2
echo "Proc count:" `wc -l <<< "$proc_list"` >> $2
