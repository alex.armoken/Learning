#include "Info.h"

#define STATFILE "/stat"
#define STATUSFILE "/status"
#define CMDLINEFILE "/cmdline"

//Reading the status file
int ReadStatFile(char reading_dir[], Process *process)
{
	char *filename = concat(reading_dir, STATFILE);
	FILE *file = fopen(filename, "r");
	if (file == NULL)
		{
			free(filename);
			return 2;
		}

	char buf[MAX_READ];

	//Check the size of text. If it equals to zero, exit from program.
	int size = fread(buf, sizeof(char), MAX_READ, file);
	if (!size || (size == 0))
		{
			fclose(file);
			free(filename);
			return 3;
		}

	//I am added two here to remove bracket and one space after this
	char *except_proc_name = strchr(buf, ')') + 2;
	if (!except_proc_name)
		{
			fclose(file);
			free(filename);
			return 3;
		}

	sscanf(except_proc_name,
		   "%c %u %u %u %u "
		   "%*d %*u "
		   "%*u %*u %*u %*u "
		   "%u %u %u %u "
		   "%d %d %*d "
		   "%*d %*u %*u %u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u "
		   "%*d %d",
		   &process->state, &process->ppid, &process->pgrp, &process->session, &process->tty_nr,
		   &process->utime, &process->stime, &process->cutime, &process->cstime,
		   &process->priority, &process->nice,
		   &process->startime,
		   &process->processor);

	process->total_time = process->utime + process->stime + process->cutime + process->cstime;
	fclose(file);
	return 0;
}

//Reading the status file
int ReadStatusFile(char reading_dir[], Process *process)
{
	char *filename = concat(reading_dir, STATUSFILE);
	FILE *file = fopen(filename, "r");
	if (file == NULL)
		{
			free(filename);
			return 2;
		}

	char buf[MAX_READ];

	//Check the size of text. If it equals to zero, exit from program.
	int size = fread(buf, sizeof(char), MAX_READ, file);
	if (!size)
		{
			fclose(file);
			free(filename);
			return 3;
		}

	//I am added two here to remove bracket and one space after this
	char *except_proc_name = strchr(buf, '\n') + 1;
	if (!except_proc_name)
		{
			fclose(file);
			free(filename);
			return 3;
		}

	sscanf(except_proc_name,
		   "State:	%c	%*s\n"
		   "Tgid:	%u\n"
		   "Ngid:	%u\n"
		   "Pid:	%u\n"
		   "PPid:	%u\n"
		   "TracerPid:	%*d\n"
		   "Uid:	%u	%*u	%*u	%*u"
		   "Gid:	%u	%*u	%*u	%*u\n",
		   &process->state, &process->tgid,
		   &process->ngid, &process->pid, &process->ppid,
		   &process->uidd, &process->gidd);

	fclose(file);
	free(filename);
	return 0;
}

//Reading of the complete command line for the process
int ReadCmdLineFile(char reading_dir[], Process *process)
{
	char *filename = concat(reading_dir, CMDLINEFILE);
	FILE *file = fopen(filename, "r");
	if (file == NULL)
		{
			return 2;
		}

	//Check the size of text. If it equals to zero, exit from program.
	char buf[4096+1]; // max cmdline length on Linux
	int count = fread(buf, 1, sizeof(buf) - 1, file);
	//Removing the \0
	if (count > 0) {
		for (int i = 0; i < count; i++)
			if (buf[i] == '\0' || buf[i] == '\n') {
				buf[i] = ' ';
			}
	}
	buf[count] = '\0';
	strcpy(process->cmdline, buf);

	fclose(file);
	free(filename);
	return 0;
}

//Getting proc owner by uid
int GetProcOwner(Process *process)
{
	//printf("%d\n", process->pid);
	struct passwd *user = getpwuid(process->uidd);
	if (user != NULL)
		{
			strcpy(process->user, user->pw_name);
		}
	else
		{
			return 4;
		}
	return 0;
}
