#include "Info.h"

#define PROCDIR "/proc/"
#define SUBPROCDIR "/task"

//Reading the /proc dir
int ReadDir(char work_dir_name[], Process **processes_list, unsigned int *last_el, unsigned int *array_size)
{
	DIR *main_work_dir = opendir(work_dir_name);

	if (main_work_dir == NULL) {
		return 2;
	}

	char *end;
	unsigned long int check_strtoul;
	struct dirent *entry = readdir(main_work_dir);

	while (entry != NULL) {

		//Check does the dir name convert right to exlude everything without pid numbers
		check_strtoul = strtoul(entry->d_name, &end, 10);

		if (check_strtoul != 0)  {

			//Getting subdir path for subprocesses
			char *proc_dir_name = concat(PROCDIR, entry->d_name);
			char *proc_task_dir_name = concat(proc_dir_name, SUBPROCDIR);
			DIR *proc_task_dir = opendir(proc_task_dir_name);
			struct dirent *subproc_dir = readdir(proc_task_dir);

			//Reading from subdir
			if (subproc_dir != NULL) {
				while (subproc_dir != NULL) {
					check_strtoul = strtoul(subproc_dir->d_name, &end, 10);
					if (check_strtoul != 0) {
						char *pre_reading_dir = concat(proc_task_dir_name, "/");
						char *reading_dir = concat(pre_reading_dir, subproc_dir->d_name);
						free(pre_reading_dir);

						IncreaseArraySize(processes_list, array_size, last_el);

						ReadStatusFile(reading_dir, &(*processes_list)[*last_el]);
						ReadStatFile(reading_dir, &(*processes_list)[*last_el]);
						ReadCmdLineFile(reading_dir, &(*processes_list)[*last_el]);
						GetProcOwner(&(*processes_list)[*last_el]);

						GetProcTotalTime(&(*processes_list)[*last_el]);
						GetCpuTotalTime(&(*processes_list)[*last_el]);
						ReadStatmFile(reading_dir, &(*processes_list)[*last_el]);

						strcpy((*processes_list)[*last_el].filename, reading_dir);

						free(reading_dir);
						*last_el = *last_el + 1;
					}
					subproc_dir = readdir(proc_task_dir);
				}
			}
			free(proc_dir_name);
			free(proc_task_dir_name);
		}
		entry = readdir(main_work_dir);
	}
	return 0;
}

int GetInformation(Process **processes_list, unsigned int *last_proc_num, unsigned int *array_size)
{
	if (ReadDir(PROCDIR, processes_list, last_proc_num, array_size) == 1) {
		printf("Mem Error\n");
	}
	GetProcessCpuLoad(processes_list, array_size);
	qsort(*processes_list, *array_size, sizeof(Process), PidCompare);
	return 0;
}
