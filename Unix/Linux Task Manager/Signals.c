#include "Info.h"

#define SIG

void send_signal (int sig_number)
{
	GtkTreeIter iter;

    gboolean reader = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(mainWindowObjects.processesList), &iter);
    while ( reader )
		{
			gboolean selected;
			gtk_tree_model_get(GTK_TREE_MODEL(mainWindowObjects.processesList), &iter,
							   SELECTED, &selected, -1);
			if ( selected == TRUE)
				{
					int pid;
					gtk_tree_model_get (GTK_TREE_MODEL(mainWindowObjects.processesList), &iter,
										PID, &pid,
										-1);

					kill(pid, sig_number);

				}
			reader = gtk_tree_model_iter_next(GTK_TREE_MODEL(mainWindowObjects.processesList), &iter);
		}
}

void on_sigstop_clicked (GtkButton *sigterm, gpointer data)
{
	send_signal(SIGSTOP);
}

void on_sigterm_clicked (GtkButton *sigterm, gpointer data)
{
	send_signal(SIGTERM);
}

void on_sighup_clicked (GtkButton *sighup, gpointer data)
{
	send_signal(SIGHUP);
}

void on_sigint_clicked (GtkButton *sigint, gpointer data)
{
	send_signal(SIGINT);
}

void on_sigabrt_clicked (GtkButton *sigabrt, gpointer data)
{
	send_signal(SIGABRT);
}

void on_sigkill_clicked (GtkButton *sigkill, gpointer data)
{
	send_signal(SIGKILL);
}

void on_sigsegv_clicked (GtkButton *sigsegv, gpointer data)
{
	send_signal(SIGSEGV);
}

void on_sigtstp_clicked (GtkButton *sigtstp, gpointer data)
{
	send_signal(SIGTSTP);
}

void on_sigcont_clicked (GtkButton *sigtstp, gpointer data)
{
	send_signal(SIGCONT);
}

void on_sigfpe_clicked (GtkButton *sigtstp, gpointer data)
{
	send_signal(SIGFPE);
}

void on_sigill_clicked (GtkButton *sigtstp, gpointer data)
{
	send_signal(SIGILL);
}
