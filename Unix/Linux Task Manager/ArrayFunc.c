#include "Info.h"

char* concat(char *s1, char *s2)
{
	char *result = malloc(strlen(s1)+strlen(s2)+1);//+1 for the zero-terminator
	strcpy(result, s1);
	strcat(result, s2);
	return result;
}

//I am using a single function to make code more obvious and to add more check
int IncreaseArraySize(Process **processes_list, unsigned int *array_size, unsigned int *last_el)
{
	Process* new_processes_list;

    if (*last_el == *array_size) {
        *array_size = *array_size + 3;
        new_processes_list = (Process*)malloc(*array_size * sizeof(Process));
        if (new_processes_list == NULL) {
            *array_size = *array_size - 3;
            return 1;
        }
        else {
            for (int i = 0; i < *last_el; i++) {
				new_processes_list[i] = (*processes_list)[i];
			}
			free(*processes_list);
			*processes_list = new_processes_list;
			return 0;
		}
	}
	else {
		return 0;
	}
}

//Remove processes what user dont need to see
int CleanProcessesList(Process **processes_list, unsigned int *array_size, unsigned int *last_el)
{
	int true_count = 0;//Count of processes what user need to watch

	for (int i = 0; i < *last_el; i++) {
		if 	((*processes_list)[i].pgrp != 0 && (*processes_list)[i].pid != 0)
			{
				++true_count;
			}
	}

	Process *new_processes_list = (Process*)malloc(true_count * sizeof(Process));
	if (new_processes_list == NULL) {
		return 1;
	}
	else {
		for (int i = 0, j = 0; i < *last_el; i++) {
			if((*processes_list)[i].pgrp != 0 && (*processes_list)[i].pid != 0)
				{
					new_processes_list[j] = (*processes_list)[i];
					++j;
				}
		}

		*array_size = true_count;
		*last_el = true_count;

		free(*processes_list);
		*processes_list = new_processes_list;
		return 0;
	}
}
