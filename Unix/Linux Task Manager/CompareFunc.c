#include "Info.h"

int PidCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	return (a_r->pid - b_r->pid);
}

int UserCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	return strcmp(a_r->user, b_r->user);
}

int PriorityCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	return (a_r->priority - b_r->priority);
}

int NiceCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	return (a_r->nice - b_r->nice);
}
int VirtCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	return (a_r->virt - b_r->virt);
}

int ResCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	return (a_r->res - b_r->res);
}

int ShrCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	return (a_r->shr - b_r->shr);
}

int StateCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	if (a_r->state > b_r->state)
		{
			return 1;
		}
	else if  (a_r->state < b_r->state)
		{
			return -1;
		}
	else
		{
			return 0;
		}
}

int CpuloadCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	return (a_r->cpu_load - b_r->cpu_load);
}

int MemloadCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	return (a_r->mem_load - b_r->mem_load);
}

int CmdCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;

	return strcmp(a_r->cmdline, b_r->cmdline);
}

int TotalTimeCompare(const void *a, const void *b)
{
	Process const* a_r = (Process const*)a;
	Process const*  b_r = (Process const*)b;
	unsigned int sec_a = a_r->proc_hour * 3600 + a_r->proc_min * 60 + a_r->proc_sec;
	unsigned int sec_b = b_r->proc_hour * 3600 + b_r->proc_min * 60 + b_r->proc_sec;

	return (sec_a - sec_b);
}
