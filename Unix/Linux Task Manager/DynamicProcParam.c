#include "Info.h"

#define PROCDIR "/proc/"
#define STATMFILE "/statm"
#define DEVCPUDIVECES "/dev/cpu"
#define PROCSTATFILE "/proc/stat"
#define PROCUPTIMEFILE "/proc/uptime"

int GetGoodMemValue(unsigned int *value, char *measure)
{
	if (*value > 104857600)
		{
			*value = *value / 1024 / 1024;
			strcpy(measure, "mB");
		}
	else
		{
			*value = *value / 1024;
			strcpy(measure, "kB");
		}
	return 0;
}

int GetProcessMemLoad(Process *process)
{
	unsigned long totalRAM = totalPhysMem();

	process->mem_load = 0.1 * (process->res / (totalRAM * 1024));

	return 0;
}

//Reading the status file
int ReadStatmFile(char reading_dir[], Process *process)
{
	char *filename = concat(reading_dir, STATMFILE);
	FILE *file = fopen(filename, "r");
	if (file == NULL)
		{
			free(filename);
			return 2;
		}

	char buf[MAX_READ];

	//Check the size of text. If it equals to zero, exit from program.
	int size = fread(buf, sizeof(char), MAX_READ, file);
	if (!size)
		{
			fclose(file);
			free(filename);
			return 3;
		}

	sscanf(buf, "%u %u %u %*u %*u %*u %*u", &process->virt, &process->res,  &process->shr);

	unsigned int page = sysconf(_SC_PAGESIZE);
	process->virt = process->virt * page;
	process->res = process->res * page;
	process->shr = process->shr * page;

	GetProcessMemLoad(process);

	GetGoodMemValue(&process->virt, process->virt_str);
	GetGoodMemValue(&process->res, process->res_str);
	GetGoodMemValue(&process->shr, process->shr_str);


	fclose(file);
	free(filename);
	return 0;
}

int GetCpuTotalTime(Process *process)
{
	unsigned int lastTotalUser, lastTotalUserLow, lastTotalSys, lastTotalIdle;

	FILE* file = fopen(PROCSTATFILE, "r");
	if (file == NULL) {
		return 2;
	}

	//	char buf[MAX_READ];
	unsigned int proc_num = 0;
	int count = fscanf(file,
					   "cpu %u %*d %u %u %u %*u %*u %*u %*u %*u\n",
					   &lastTotalUser, &lastTotalUserLow,
					   &lastTotalSys, &lastTotalIdle);

	for (int i = 0; i <= process->processor; i++) {
		count = fscanf(file,
					   "cpu%u %u %*d %u %u %u %*u %*u %*u %*u %*u\n",
					   &proc_num, &lastTotalUser, &lastTotalUserLow,
					   &lastTotalSys, &lastTotalIdle);
	}

	memset(&count, 0, sizeof(int));
	process->total_cpu_time = 0;
	process->total_cpu_time = process->total_cpu_time + lastTotalUser + lastTotalUserLow + lastTotalSys + lastTotalIdle;
	fclose(file);
	return 0;
}

int GetProcessCpuLoad(Process **processes_list_before, unsigned int *array_size_before)
{
	unsigned int array_size_after = 3;
	unsigned int last_proc_num_after = 0;
	Process *processes_list_after = (Process*)malloc(array_size_after * sizeof(Process));

	sleep(3);
	if (ReadDir(PROCDIR, &processes_list_after, &last_proc_num_after, &array_size_after) == 1) {
		printf("Mem Error\n");
	}

	unsigned int in_two_count = (array_size_after < *array_size_before) ? array_size_after: *array_size_before;
	for (int i = 0; i < in_two_count; i++)
		{
			if ((*processes_list_before)[i].pid == processes_list_after[i].pid &&
				strcmp((*processes_list_before)[i].cmdline, processes_list_after[i].cmdline ) == 0)
				{
					float user_util = 0;
					float sys_util = 0;
					unsigned int residual = processes_list_after[i].total_cpu_time - (*processes_list_before)[i].total_cpu_time;

					if (residual != 0)
						{
							user_util= 100.0 * (processes_list_after[i].utime - (*processes_list_before)[i].utime) / residual;
							sys_util= 100.0 * (processes_list_after[i].stime - (*processes_list_before)[i].stime) / residual;
						}
					processes_list_after[i].cpu_load = user_util + sys_util;
				}
		}

	free(*processes_list_before);
	*processes_list_before = processes_list_after;
	return 0;
}

int GetProcTotalTime(Process *process)
{
	double hertz = sysconf(_SC_CLK_TCK);
	double jiffytime = 1.0 / hertz;

	unsigned int total_time = process->utime + process->stime + process->cstime + process->cutime;

	double realTime = total_time * jiffytime;
	unsigned long long iRealTime = (unsigned long long) realTime;

	process->proc_hour = iRealTime / 3600;
	process->proc_min = (iRealTime / 60) % 60;
	process->proc_sec = iRealTime % 60;
	int hundredths = (realTime - iRealTime) * 100;

	if (process->proc_hour == 0)
		{
			process->proc_hour = process->proc_min;
			process->proc_min = process->proc_sec;
			process->proc_sec = hundredths;
		}
	return 0;
}
