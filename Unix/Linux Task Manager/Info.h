#include <grp.h>
#include <pwd.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <dirent.h>
#include <unistd.h>
#include <signal.h>
#include <pthread.h>
#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <glib-2.0/glib.h>
#include <glib-2.0/glib/gmacros.h>

#define MAX_USER 32
#define MAX_NAME 2048
#define MAX_READ 2048
#define MAX_STRING 4096
#define MAX_FILE_NAME 256

typedef struct {
	int nice;
	int priority;
	double cpu_load;
	double mem_load;
	long long int exit_signal;
	unsigned int uidd;
	unsigned int gidd;
	unsigned int proc_sec;
	unsigned int proc_min;
	unsigned int proc_hour;
	unsigned int processor;

	//Mem types
	unsigned int res;
	char res_str[2];
	unsigned int shr;
	char shr_str[2];
	unsigned int virt;
	char virt_str[2];

	unsigned int pid;
	unsigned int ppid;
	unsigned int ngid;

	unsigned int pgrp;
	unsigned int tgid;
	unsigned int utime;

	unsigned int stime;
	unsigned int cstime;
	unsigned int cutime;;

	unsigned int tty_nr;
	unsigned int session;
	unsigned int startime;
	unsigned int total_time;
	unsigned long long int total_cpu_time;

	char user[MAX_USER];
	char state;
	char filename[MAX_NAME];
	char cmdline[MAX_STRING];
} Process;


enum
	{
		SELECTED = 0,
		PID,
		USER,

		PRI,
		NICE,
		VIRT,

		VIRT_S,
		RES,
		RES_S,

		SHR,
		SHR_S,
		S,

		CPU,
		MEM,
		TIME,
		CMD
	};

struct MainWindowObjects
{
    GtkWindow      *topWindow;
    GtkTreeView    *procList;
    GtkListStore   *processesList;

	GtkLabel      *get_cpu;
	GtkLabel      *get_ram;
	GtkLabel      *get_swap;
	GtkLabel      *get_proc_count;

	GtkButton      *sighup;
	GtkButton      *sigint;
	GtkButton      *sigabrt;
	GtkButton      *sigkill;
	GtkButton      *sigsegv;
	GtkButton      *sigterm;
	GtkButton      *sigcount;
	GtkButton      *sigstop;
	GtkButton      *sigtstp;
} mainWindowObjects;

#ifndef RAM
int getUsedMem();
unsigned long totalPhysMem();
unsigned long totalFreeMem();
unsigned long totalSwapMem();
unsigned long totalFreeSwap();
unsigned long physSwapUsed();
#endif

#ifndef CPU
void cpu_init();
double physCpuUsage();
#endif

#ifndef STATMFILE
int GetTimeProc(Process *process);
int GetCpuTotalTime(Process *process);
int GetProcTotalTime(Process *process);
int ReadStatmFile(char reading_dir[], Process *process);
int GetProcessCpuLoad(Process **processes_list_before, unsigned int *array_size_before);
#endif

#ifndef STATFILE
int GetProcOwner(Process *process);
int ReadStatFile(char reading_dir[], Process *process);
int ReadStatusFile(char reading_dir[], Process *process);
int ReadCmdLineFile(char reading_dir[], Process *process);
#endif

#ifndef ARRAYFUNC
char* concat(char *s1, char *s2);
int IncreaseArraySize(Process **processes_list, unsigned int *array_size, unsigned int *last_el);
int CleanProcessesList(Process **processes_list, unsigned int *array_size, unsigned int *last_el);
#endif

#ifndef COMPARE
int PidCompare(const void *a, const void *b);
int ResCompare(const void *a, const void *b);
int ShrCompare(const void *a, const void *b);
int CmdCompare(const void *a, const void *b);
int UserCompare(const void *a, const void *b);
int NiceCompare(const void *a, const void *b);
int VirtCompare(const void *a, const void *b);
int StateCompare(const void *a, const void *b);
int CpuloadCompare(const void *a, const void *b);
int MemloadCompare(const void *a, const void *b);
int PriorityCompare(const void *a, const void *b);
int TotalTimeCompare(const void *a, const void *b);
#endif

#ifndef SUBPROCDIR
int GetInformation(Process **processes_list, unsigned int *last_proc_num, unsigned int *array_size);
int ReadDir(char work_dir_name[], Process **processes_list, unsigned int *last_el, unsigned int *array_size);
#endif

#ifndef SIG
void on_topWindow_destroy(GtkWidget *window, gpointer data);
void on_topWindow_show(GtkEntry *entry, gpointer user_data);

void on_sighup_clicked (GtkButton *sighup, gpointer data);
void on_sigint_clicked (GtkButton *sigint, gpointer data);
void on_sigill_clicked (GtkButton *sigill, gpointer data);

void on_sigabrt_clicked (GtkButton *sigabrt, gpointer data);
void on_sigfpe_clicked (GtkButton *sigfpe, gpointer data);
void on_sigkill_clicked (GtkButton *sigkill, gpointer data);

void on_sigsegv_clicked (GtkButton *sigsegv, gpointer data);
void on_sigterm_clicked (GtkButton *sigterm, gpointer data);
void on_sigtstp_clicked (GtkButton *sigstp, gpointer data);

void on_sigcont_clicked (GtkButton *sigcount, gpointer data);
void on_sigstop_clicked (GtkButton *sigstop, gpointer data);
#endif
