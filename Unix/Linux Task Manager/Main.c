#include "Info.h"

#define UI_FILE "UI.glade"

int proc_scanning = 0;
Process *processes_array;
pthread_t info;
pthread_t plist;
unsigned int last_proc_num;
unsigned int array_size;

void giving_information(void)
{
	while (proc_scanning == 1)
		{
			sleep(1);
			char cpu_load_str[8];
			double cpu_load = physCpuUsage();
			snprintf(cpu_load_str, 7, "%4.1f",  cpu_load);
			gtk_label_set_text(mainWindowObjects.get_cpu, cpu_load_str);

			gchar ram_load_str[5];
			float ram_load = getUsedMem();
			snprintf(ram_load_str, 5, "%4.f",  ram_load);
			gtk_label_set_text(mainWindowObjects.get_ram, ram_load_str);

			gchar swap_load_str[5];
			float swap_load = physSwapUsed();
			snprintf(swap_load_str, 5, "%4.f",  swap_load);

			gtk_label_set_text(mainWindowObjects.get_swap, swap_load_str);

			gchar proc_count_str[5];
			snprintf(proc_count_str, 5, "%u", last_proc_num);
			gtk_label_set_text(mainWindowObjects.get_proc_count, proc_count_str);
		}
}

void on_topWindow_show(GtkEntry *entry, gpointer user_data)
{
	GtkTreeIter iter;
	Process *processes_list = (Process*)malloc(array_size * sizeof(Process));

	proc_scanning = 1;
	pthread_create(&info, NULL, (void *(*)(void *))giving_information, NULL);

	GetInformation(&processes_list, &last_proc_num, &array_size);
	CleanProcessesList(&processes_list, &array_size, &last_proc_num);

	for (int i = 0 ; i < last_proc_num; i++) {
		/* char temp[2]; */
		/* temp[0] = processes_array[i].state; */
		/* temp[1] = '\0'; */

		/* printf("%c\n", processes_array[i].state); */
		gtk_list_store_append(mainWindowObjects.processesList, &iter);
		gtk_list_store_set(mainWindowObjects.processesList, &iter,
						   SELECTED, FALSE,
						   PID, (unsigned int)processes_list[i].pid,
						   USER, processes_list[i].user,

						   PRI, (int)processes_list[i].priority,
						   NICE, (int)processes_list[i].nice,
						   VIRT, (int)processes_list[i].virt,

						   VIRT_S, processes_list[i].virt_str,
						   RES, (int)processes_list[i].res,
						   RES_S, processes_list[i].res_str,

						   SHR, (int)processes_list[i].shr,
						   SHR_S, processes_list[i].shr_str,
						   //  S, temp,

						   CPU,(double) processes_list[i].cpu_load,
						   MEM,(double) processes_list[i].mem_load,
						   TIME,(unsigned int)processes_list[i].total_cpu_time,
						   CMD, processes_list[i].cmdline,
						   -1 );
	}
	/* This is working */
	/* for (int j = 0; j < last_proc_num; j++) { */
	/* 	printf("%c\n", processes_list[j].state); */
	/* } */
	free(processes_list);
}

void on_cellrenderKill_toggled(GtkCellRendererToggle *render, gchar *path, gpointer data)
{
    GtkTreeIter iter;
    GtkTreeModel *model;
    gboolean selected;

    model = gtk_tree_view_get_model ( mainWindowObjects.procList );
    if (gtk_tree_model_get_iter_from_string(model, &iter, path))
		{
			gtk_tree_model_get( model, &iter, SELECTED, &selected, -1 );

			if (selected == TRUE)
				{
					selected = FALSE;
				}
		    else if (selected == FALSE)
				{
					selected = TRUE;
				}

			gtk_list_store_set( GTK_LIST_STORE (model), &iter, SELECTED, selected, -1 );
		}
}

void on_topWindow_destroy(GtkWidget *window, gpointer data)
{
	proc_scanning = 0;
	sleep(1);
	gtk_main_quit();
}

int main(int argc, char** argv)
{
	GtkBuilder *builder;
	GError *error = NULL;


	gtk_init( &argc, &argv );

	builder = gtk_builder_new();

	if( ! gtk_builder_add_from_file( builder, UI_FILE, &error ) )
		{
			g_warning( "%s\n", error->message );
			g_free( error );
			return( 1 );
		}

	mainWindowObjects.topWindow = GTK_WINDOW(gtk_builder_get_object(builder, "topWindow"));
	mainWindowObjects.procList = GTK_TREE_VIEW( gtk_builder_get_object( builder, "procList") );
	mainWindowObjects.processesList = GTK_LIST_STORE( gtk_builder_get_object(builder, "processesList") );

	mainWindowObjects.get_cpu = GTK_LABEL( gtk_builder_get_object( builder, "get_cpu") );
	mainWindowObjects.get_ram = GTK_LABEL( gtk_builder_get_object( builder, "get_ram") );
	mainWindowObjects.get_swap = GTK_LABEL( gtk_builder_get_object( builder, "get_swap") );
	mainWindowObjects.get_proc_count = GTK_LABEL( gtk_builder_get_object( builder, "get_proc_count") );

	mainWindowObjects.sighup = GTK_BUTTON( gtk_builder_get_object( builder, "sighup") );
	mainWindowObjects.sigint = GTK_BUTTON( gtk_builder_get_object( builder, "sigint") );
	mainWindowObjects.sigabrt = GTK_BUTTON( gtk_builder_get_object( builder, "sigabrt") );
	mainWindowObjects.sigkill = GTK_BUTTON( gtk_builder_get_object( builder, "sigkill") );
	mainWindowObjects.sigsegv = GTK_BUTTON( gtk_builder_get_object( builder, "sigsegv") );
	mainWindowObjects.sigterm = GTK_BUTTON( gtk_builder_get_object( builder, "sigterm") );
	mainWindowObjects.sigcount = GTK_BUTTON( gtk_builder_get_object( builder, "sigcount") );
	mainWindowObjects.sigstop = GTK_BUTTON( gtk_builder_get_object( builder, "sigstop") );
	mainWindowObjects.sigtstp = GTK_BUTTON( gtk_builder_get_object( builder, "sigtstp") );

	GtkTreeSelection *selection;
	selection = gtk_tree_view_get_selection( GTK_TREE_VIEW(mainWindowObjects.procList) );
	gtk_tree_selection_set_mode( selection, GTK_SELECTION_MULTIPLE );


	gtk_builder_connect_signals (builder, &mainWindowObjects);

	g_object_unref( G_OBJECT( builder ) );
	gtk_widget_show_all ( GTK_WIDGET (mainWindowObjects.topWindow) );
	gtk_main ();

	return 0;
}


/* void information_at_begin(void) */
/* { */
/* 	GtkTreeIter iter; */
/* 	Process *processes_array = (Process*)malloc(array_size * sizeof(Process)); */

/* 	GetInformation(&processes_array, &last_proc_num, &array_size); */
/* 	CleanProcessesList(&processes_array, &array_size, &last_proc_num); */

/* 	for (int i = 0 ; i < last_proc_num; i++) { */
/* 		char temp[2]; */
/* 		temp[0] = processes_array[i].state; */
/* 		temp[1] = '\0'; */
/* 		gtk_list_store_append (mainWindowObjects.processesList, &iter); */
/* 		gtk_list_store_set(mainWindowObjects.processesList, &iter, */
/* 						   SELECTED, FALSE, */
/* 						   PID, (unsigned int)processes_array[i].pid, */
/* 						   USER, processes_array[i].user, */
/* 						   PRI, (int)processes_array[i].priority, */
/* 						   NICE, (int)processes_array[i].nice, */
/* 						   VIRT, (int)processes_array[i].virt, */
/* 						   VIRT_S, processes_array[i].virt_str, */
/* 						   RES, (int)processes_array[i].res, */
/* 						   RES_S, processes_array[i].res_str, */
/* 						   SHR, (int)processes_array[i].shr, */
/* 						   SHR_S, processes_array[i].shr_str, */
/* 						   S, temp, */
/* 						   CPU, (double) processes_array[i].cpu_load, */
/* 						   MEM, (double) processes_array[i].mem_load, */
/* 						   TIME, (unsigned int)processes_array[i].total_cpu_time, */
/* 						   CMD, processes_array[i].cmdline, */
/* 						   -1 ); */
/* 	} */
/* 	free(processes_array); */
/* } */
