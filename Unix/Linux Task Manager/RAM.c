#include "Info.h"

struct sysinfo memInfo;

unsigned long totalPhysMem()
{
	sysinfo (&memInfo);
    return (memInfo.totalram * memInfo.mem_unit / 1024 / 1024);
}

unsigned long totalFreeMem()
{
    sysinfo (&memInfo);
    return (memInfo.freeram * memInfo.mem_unit / 1024 / 1024);
}

unsigned long totalSwapMem()
{
    sysinfo (&memInfo);
    return (memInfo.totalswap * memInfo.mem_unit / 1024 / 1024);
}

unsigned long totalFreeSwap()
{
    sysinfo (&memInfo);
    return (memInfo.freeswap * memInfo.mem_unit / 1024 / 1024);
}

unsigned long physSwapUsed()
{
    sysinfo (&memInfo);
    return totalSwapMem() - totalFreeSwap();
}


int getUsedMem()
{
    FILE* file;
    char lose[200];
    unsigned long physAvailableMem = 0;

    file = fopen("/proc/meminfo", "r");
    char *end;
	end = fgets(lose, 200, file);
    end = fgets(lose, 200, file);

    int count = fscanf(file, "MemAvailable:    %lu kB", &physAvailableMem);
	memset(&count, 0, sizeof(int));
	end += 1;
    fclose(file);

    unsigned long physMemUsed = totalPhysMem() - physAvailableMem / 1024;

    return physMemUsed;
}
