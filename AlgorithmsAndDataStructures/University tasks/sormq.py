#!/usr/bin/python3

import sys
import math


def build(arr):
    table = [[] for _ in range(int(math.log2(len(arr))) + 1)]
    table[0] = arr
    for k in range(1, int(math.log2(len(arr))) + 1):
        for i in range(len(arr) - 2 ** k + 1):
            table[k].append(min(table[k - 1][i],
                                table[k - 1][i + 2 ** (k - 1)]))

    return table


def get_min(l, r, table):
    k = int(math.log2(r - l + 1))
    return min(table[k][l], table[k][r - 2 ** k + 1])


def main():
    arr = [3, 8, 6, 4, 2, 5, 9, 0, 7, 1]
    table = build(arr)
    print(table)
    print(get_min(1, 6, table))

if __name__ == "__main__":
    sys.exit(main())
