#include <ctime>
#include <random>
#include <limits>

#include <tuple>
#include <vector>
#include <iostream>

using namespace std;

typedef struct max_sum_t
{
    uint32_t start;
    uint32_t end;
    int64_t sum;
    max_sum_t(uint32_t start, uint32_t end, uint64_t sum) :
            start(start), end(end), sum(sum) {};
} max_sum;

max_sum find_max_crossing_subarray_left(vector<int32_t> arr,
                                        uint32_t start, uint32_t end) {
    int64_t left_sum = INT32_MIN;
    uint32_t max_left = 0;
    int64_t cur_sum = 0;
    for (int32_t i = end; i > start; i--) {
        cur_sum += arr[i];
        if (cur_sum > left_sum) {
            left_sum = cur_sum;
            max_left = i;
        }
    }
    return max_sum(max_left, end, left_sum);
}

max_sum find_max_crossing_subarray_right(vector<int32_t> arr,
                                         uint32_t start, uint32_t end) {
    int64_t cur_sum = 0;
    uint32_t max_right = 0;
    int64_t right_sum = INT64_MIN;
    for (int32_t i = start; i <= end; i++) {
        cur_sum += arr[i];
        if (cur_sum > right_sum) {
            right_sum = cur_sum;
            max_right = i;
        }
    }
    return max_sum(start, max_right, right_sum);
}

max_sum find_max_crossing_subarray(
    vector<int32_t> arr, uint32_t start, uint32_t mid, uint32_t end) {
    auto left = find_max_crossing_subarray_left(arr, start, mid);
    auto right = find_max_crossing_subarray_right(arr, mid + 1, end);
    return max_sum(left.start, right.end, left.sum + right.sum);
}

max_sum find_max_subarray(
    vector<int32_t> arr, uint32_t start, uint32_t end) {
    if (start == end) {
        return max_sum(start, end, arr[start]);
    }

    uint32_t mid = (start + end) / 2;
    auto left = find_max_subarray(arr, start, mid);
    auto crossing = find_max_crossing_subarray(arr, start, mid, end);
    auto right = find_max_subarray(arr, mid + 1, end);

    if (left.sum >= right.sum && left.sum >= crossing.sum)
        return left;
    else if (right.sum >= left.sum && right.sum >= crossing.sum)
        return right;

    return crossing;
}

max_sum find_max_subarray_n(vector<int32_t> arr,
                            uint32_t start, uint32_t end) {
    int64_t result_sum = arr[0];
    int32_t resust_start = 0;
    int32_t result_end = 0;
    int32_t cur_sum = 0;
    int32_t min_sum = 0;
    int32_t min_sum_end = -1;

    for (uint32_t cur_end = start; cur_end < end; cur_end++) {
        cur_sum += arr[cur_end];

        int maybe_result_sum = cur_sum - min_sum;
        if (maybe_result_sum > result_sum) {
            result_sum = maybe_result_sum;
            resust_start = min_sum_end + 1;
            result_end = cur_end;
        }

        if (maybe_result_sum < min_sum) {
            min_sum = maybe_result_sum;
            min_sum_end = cur_end;
        }
    }

    return max_sum(resust_start, result_end, result_sum);
}

void print_vectors(vector<vector<int32_t>> arr_of_arrays) {
    for (uint32_t i = 0; i < arr_of_arrays.front().size(); i++) {
        cout << " " << i << ") ";
        for (uint32_t j = 0; j < arr_of_arrays.size(); j++) {
            cout << " " << arr_of_arrays[j][i];
        }
        cout << endl;
    }
}
int main() {
    vector<int32_t> arr;
    // = {13, -3, -25, 20, -3, -16, -23, 18,
    //    20, -7, 12, -5, -22, 15, -4, 7};
    srand(time(0));
    for (uint32_t i = 0; i < 10; i++) {
        arr.push_back(rand() % 100 - 50);
    }

    print_vectors(vector<vector<int32_t>>{arr});

    cout << "==================" << "\n";
    auto result = find_max_subarray(arr, 0, arr.size() - 1);
    cout << " " << result.start << " " << result.end
         << " " << result.sum << endl;

    result = find_max_subarray_n(arr, 0, arr.size() - 1);
    cout << " " << result.start << " " << result.end
         << " " << result.sum << endl;
    return 0;
}
