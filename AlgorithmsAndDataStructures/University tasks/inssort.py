#!/usr/bin/python3

import sys


def ins_sort(arr):
    for i in range(len(arr)):
        j = i - 1
        while j >= 0 and arr[j] > arr[j + 1]:
            arr[j], arr[j + 1] = arr[j + 1], arr[j]
            j -= 1

    return arr


def main():
    arr = [1, 2, 5, 2, 4, 5, 100, 0, 1]
    print(ins_sort(arr))


if __name__ == "__main__":
    sys.exit(main())
