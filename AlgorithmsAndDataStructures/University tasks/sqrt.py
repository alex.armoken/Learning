#!/usr/bin/python3

import sys
import math


def build(arr):
    step = int(math.sqrt(len(arr)))
    sqrt_arr = []
    for i in range(step, len(arr), step):
        sqrt_arr.append(min(arr[i - step: i]))

    return sqrt_arr


def get_min(l, r, arr, sqrt_arr):
    step = int(math.sqrt(len(arr)))
    min_arr = []

    i = l
    while i <= r:
        if i % step == 0 and i + step - 1 <= r:
            min_arr.append(sqrt_arr[i // step])
            i += step
        else:
            min_arr.append(arr[i])
            i += 1

    return min(min_arr)


def main():
    arr = [3, 8, 6, 4, 2, 5, 9, 0, 7, 1]
    table = build(arr)
    print(table)
    print(get_min(1, 7, arr, table))

if __name__ == "__main__":
    sys.exit(main())
