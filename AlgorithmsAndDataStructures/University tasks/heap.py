#!/usr/bin/python3

import sys


class Heap(object):
    def __init__(self, seq=None):
        self.arr = []
        self.cur_size = 0
        if seq:
            for i in seq:
                self.arr.append(i)
                self.cur_size += 1

    def sift_up(self, i):
        while self.arr[i] < self.arr[(i - 1) // 2]:
            tmp = self.arr[i]
            self.arr[i] = self.arr[(i - 1) // 2]
            self.arr[(i - 1) // 2] = tmp

            i = (i - 1) // 2

    def sift_down(self, i):
        while i * 2 + 1 <= self.cur_size:
            if self.arr[i] > self.arr[2 * i + 1]:
                tmp = self.arr[i]
                self.arr[i] = self.arr[2 * i + 1]
                self.arr[2 * i + 1] = tmp

                i = 2 * i + 1
            elif i * 2 + 2 <= self.cur_size:
                if self.arr[i] > self.arr[2 * i + 2]:
                    tmp = self.arr[i]
                    self.arr[i] = self.arr[2 * i + 2]
                    self.arr[2 * i + 2] = tmp

                    i = 2 * i + 2

    def add(self, el):
        self.arr.append(el)
        self.cur_size += 1
        self.sift_up(self.cur_size)

    def del_min(self, el):
        ret_value = self.arr[0]
        self.arr[0] = self.arr[self.cur_size - 1]
        self.cur_size -= 1
        self.arr.pop()
        return ret_value


def main():
    pass


if __name__ == "__main__":
    sys.exit(main())
