import sys
import random

def insertion_sort(arr, is_on_increase_in=True):
    predicate = [lambda a, b: a < b, lambda a, b: a > b][is_on_increase_in]
    for j in range(1, len(arr)):
        key = arr[j]
        i = j - 1

        while i >= 0 and predicate(arr[i], key):
            arr[i + 1] = arr[i]
            i -= 1

        arr[i + 1] = key

    return arr


def check_is_array_sorted(arr, is_sort_on_increase_in=True):
    predicate = [lambda a, b: a < b, lambda a, b: a > b][is_sort_on_increase_in]
    for i in range(len(arr) - 1):
        if predicate(arr[i], arr[i + 1]):
            print("Array unserted, error in {0} {1} place".format(i, i + 1))
            break

    else:
        print("All is good")


def main():
    arr = [i for i in range(100)]
    random.shuffle(arr)
    print(arr, "\n")

    insertion_sort(arr)
    check_is_array_sorted(arr)
    print(arr)


if __name__ == "__main__":
    sys.exit(main())
