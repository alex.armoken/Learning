#!/usr/bin/env python3

import sys
from random import randint


class Treap(object):
    def __init__(self, key, pri, left=None, right=None):
        self.key = key
        self.pri = pri
        self.left = left
        self.right = right

    def merge(self, left, right):
        if left is None:
            return right

        if right is None:
            return left

        if left.pri > right.pri:
            newRight = self.merge(left.right, right)
            return Treap(left.key, left.pri, left.left, newRight)
        else:
            newLeft = self.merge(left, right.left)
            return Treap(right.key, right.pri, newLeft, right.right)

    def split(self, key):
        left = None
        right = None

        if self.key <= key:
            if self.right is None:
                right = None
            else:
                left, right = self.right.split(key)

            left = Treap(self.key, self.pri, self.left, None)

        else:
            if self.left is None:
                left = None
            else:
                left, right = self.left.split(key)

            right = Treap(self.key, self.pri, None, self.right)

        return (left, right)

    def add(self, key):
        left, right = self.split(key)
        middle = Treap(key, randint(0, 1000000))
        return self.merge(self.merge(left, middle), right)

    def remove(self, key):
        left, right = self.split(key - 1)
        middle, right = right.split(key)
        return self.merge(left, right)

    def __str__(self):
        return str(self.key) + " " + str(self.pri) + "\n"


def main():
    a = Treap(1, 3)
    a = a.add(35)

    print(a, a.left, a.right)
    print()

    a = a.add(44)
    print(a, a.left, a.right)

    a = a.remove(44)
    print(a, a.left, a.right)


if __name__ == "__main__":
    sys.exit(main())
