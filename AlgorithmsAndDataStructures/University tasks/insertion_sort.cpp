#include <ctime>
#include <random>
#include <stdint.h>

#include <tuple>
#include <vector>
#include <cstdlib>
#include <cstdarg>
#include <iostream>
#include <algorithm>
#include <functional>

using namespace std;


template <typename T, typename Compare = std::less<>>
void insertion_sort(vector<T> &arr) {
    for (uint32_t j = 1; j < arr.size(); j++) {
        uint32_t key = arr[j];
        int32_t i = j - 1;
        while (i >= 0 && arr[i] > key) {
            arr[i + 1] = arr[i];
            i--;
        }
        arr[i + 1] = key;
    }
}

void print_vectors(vector<vector<uint32_t>> arr_of_arrays) {
    for (uint32_t i = arr_of_arrays.front().size() - 1; i != 0; i--) {
        for (uint32_t j = 0; j < arr_of_arrays.size(); j++) {
            cout << arr_of_arrays[j][i] << " ";
        }
        cout << endl;
    }
}

int main() {
    vector<uint32_t> arr1;
    vector<uint32_t> arr2;

    srand(time(0));
    for (uint32_t i = 0; i < 100; i++) {
        arr1.push_back(random() % 1000);
    }
    arr2 = arr1;

    insertion_sort(arr1);
    print_vectors(vector<vector<uint32_t>>{arr1, arr2});

    return 0;
}
