#!/usr/bin/python3

import sys
import random

def selection_sort(arr):
    for i in range(len(arr) - 1):
        min_pos = i
        for j in range(i, len(arr)):
            if arr[j] < arr[min_pos]:
                min_pos = j

        arr[i], arr[min_pos] = arr[min_pos], arr[i]

    return arr


def main():
    arr = [i for i in range(100)]
    random.shuffle(arr)
    print(arr)
    print(selection_sort(arr))


if __name__ == "__main__":
    sys.exit(main())
