#!/usr/bin/python3

import sys


def mersort(arr):
    def merge(arr1, arr2):
        i = 0
        j = 0
        result = []
        while i < len(arr1) and j < len(arr2):
            if arr1[i] < arr2[j]:
                result.append(arr1[i])
                i += 1
            else:
                result.append(arr2[j])
                j += 1

        if i < len(arr1):
            result += arr1[i:]
        elif j < len(arr2):
            result += arr2[j:]

        return result

    if len(arr) > 1:
        arr1 = mersort(arr[:len(arr) // 2])
        arr2 = mersort(arr[len(arr) // 2:])
        return merge(arr1, arr2)
    else:
        return arr


def main():
    arr = [1, 2, 5, 2, 4, 5, 100, 0, 1]
    print(mersort(arr))


if __name__ == "__main__":
    sys.exit(main())
