#!/usr/bin/python3

import sys


def qsort(arr, start_l, start_r):
    l = start_l
    r = start_r
    mid = arr[(l + r) // 2]
    while l <= r:
        while arr[l] < mid:
            l += 1

        while arr[r] > mid:
            r -= 1

        if l <= r:
            arr[l], arr[r] = arr[r], arr[l]
            l += 1
            r -= 1

    if start_l < r:
        qsort(arr, start_l, r)

    if l < start_r:
        qsort(arr, l, start_r)


def main():
    arr = [1, 2, 5, 2, 4, 5, 100, 0, 1]
    qsort(arr, 0, len(arr) - 1)
    print(arr)


if __name__ == "__main__":
    sys.exit(main())
