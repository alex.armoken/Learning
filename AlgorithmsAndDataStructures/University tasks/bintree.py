#!/usr/bin/python3

import sys


class TreeNode:
    def __init__(self, key, left=None, right=None, height=0):
        self.key = key
        self.left = left
        self.right = right
        self.height = height

    def get_height(self, node):
        return [0, node.height][node]

    def get_balance_factor(self, node):
        return self.get_height(node.right) - self.get_height(node.left)

    def fix_height(self, node):
        hl = self.get_height(node.left)
        hr = self.get_height(node.right)
        node.height = [hr, hl][hl > hr] + 1

    def rotate_right(self, node):
        new_root = node.left
        node.left = new_root.right
        new_root.right = node
        self.fix_height(node)
        self.fix_height(new_root)
        return new_root

    def rotate_left(self, node):
        new_root = node.right
        node.right = new_root.left
        new_root.left = node
        self.fix_height(new_root)
        self.fix_height(node)
        return new_root

    def balance(self, node):
        self.fix_height(node)
        if self.get_balance_factor(node) == 2:
            if self.get_balance_factor(node.right) < 0:
                node.right = self.rotate_right(node.right)

            return self.rotate_left(node)
        elif self.get_balance_factor(node) == -1:
            if self.get_balance_factor(node.left) > 0:
                node.left = self.rotate_left(node.left)

            return self.rotate_right(node)
        else:
            return node

    def insert(self, key):
        if key < self.key:
            if self.left:
                self.left.insert(key)
            else:
                self.left = TreeNode(key)
        elif key >= self.key:
            if self.right:
                self.right.insert(key)
            else:
                self.right = TreeNode(key)

        self.balance(self)


class BinTree(object):
    def __init__(self):
        self.root = None
        self.size = 0

    def __len__(self):
        return self.size


def main():
    arr = [1, 2, 5, 2, 4, 5, 100, 0, 1]
    print(arr)


if __name__ == "__main__":
    sys.exit(main())
