int gcd(int a, int b)
{
    int tmp = a;
    a = (a > b) ? a : b;
    b = (a > b) ? b : tmp;

    while (b != 0) {
        int r = a % b;
        a = b;
        b = r;
    }
    return a;
}

int gcd(int a, int b, int& x, int& y)
{
    int x1 = 1, x2 = 0;
    int y1 = 0, y2 = 1;
    while (b > 0) {
        int q = a / b;
        int r = a % b;
        a = b;
        b = r;

        int x_tmp = x1 - q * x2;
        x1 = x2;
        x2 = x_tmp;

        int y_tmp = y1 - q * y2;
        y1 = y2;
        y2 = y_tmp;
    }
    x = x1;
    y = y1;
    return a;
}

int multiplicative_inverse(int a, int module)
{
    int x, y;
    gcd(module, a, x, y);
    return (y < 0) ? y + module : y;
}

