#include <ctime>
#include <random>
#include <stdint.h>

#include <tuple>
#include <vector>
#include <cstdlib>
#include <cstdarg>
#include <iostream>
#include <algorithm>

using namespace std;


void selection_sort(vector<uint32_t> &arr) {
    for (uint32_t i = 0; i < arr.size(); i++) {
        uint32_t min = arr[i];
        uint32_t min_pos = i;
        for (uint32_t j = i; j < arr.size(); j++) {
            if (min > arr[j]) {
                min_pos = j;
                min = arr[j];
            }
        }
        arr[min_pos] = arr[i];
        arr[i] = min;
    }
    cout << endl;
}

void print_vectors(vector<vector<uint32_t>> arr_of_arrays) {
    for (uint32_t i = arr_of_arrays.front().size() - 1; i != 0; i--) {
        for (uint32_t j = 0; j < arr_of_arrays.size(); j++) {
            cout << arr_of_arrays[j][i] << " ";
        }
        cout << endl;
    }
}

int main() {
    vector<uint32_t> arr1;
    vector<uint32_t> arr2;

    srand(time(0));
    for (uint32_t i = 0; i < 100; i++) {
        arr1.push_back(random() % 1000);
    }
    arr2 = arr1;

    selection_sort(arr1);
    print_vectors(vector<vector<uint32_t>>{arr1, arr2});

    return 0;
}
