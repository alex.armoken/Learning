#!/usr/bin/python3

import sys


def main():
    N = int(input())
    M = int(input())
    graph = [[] for _ in range(N)]
    queue = [0 for i in range(N)]
    for i in range(M):
        A, B, C = map(int, input().split())
        graph[A].append[(B, C)]
        graph[B].append[(A, C)]

    from_node = int(input())
    to_node = int(input())



if __name__ == "__main__":
    sys.exit(main())
