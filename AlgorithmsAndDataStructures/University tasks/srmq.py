#!/usr/bin/python3

import sys


def build(A, k):
    size = len(A)
    B = [0 for i in range(size)]
    C = [0 for i in range(size)]
    k -= 1

    for i in range(size):
        if i % k != 0:
            B[i] = min(A[i], B[i - 1])
        else:
            B[i] = A[i]

    for i in range(size - 2, -1, -1):
        if (i + 1) % k:
            C[i] = min(A[i], C[i + 1])
        else:
            C[i] = A[i]

    return (B, C)


def get_max(l, k, B, C):
    return min(C[l], B[l + k - 1])


def main():
    arr = [1, 2, 4, 10, 14, 55, 1, 0, 12, 51, 88, 14]
    B, C = build(arr, 4)

if __name__ == "__main__":
    sys.exit(main())
