#include <ctime>
#include <random>
#include <stdint.h>

#include <tuple>
#include <vector>
#include <cstdlib>
#include <cstdarg>
#include <iostream>
#include <algorithm>
#include <functional>

using namespace std;

void print_vectors(vector<vector<uint32_t>> arr_of_arrays) {
    for (uint32_t i = arr_of_arrays.front().size() - 1; i != 0; i--) {
        for (uint32_t j = 0; j < arr_of_arrays.size(); j++) {
            cout << " " << arr_of_arrays[j][i];
        }
        cout << endl;
    }
}

void merge(vector<uint32_t> &arr,
           uint32_t start, uint32_t mid,
           uint32_t end, std::function<bool(uint32_t, uint32_t)> cmp) {
    vector<uint32_t> tmp1, tmp2;
    for (auto i = start; i <= mid; i++) {
        tmp1.push_back(arr[i]);
    }
    for (auto i = mid + 1; i <= end; i++) {
        tmp2.push_back(arr[i]);
    }

    uint32_t i1 = 0;
    uint32_t i2 = 0;

    for (uint32_t i = start; i <= end; i++) {
        if (i2 == tmp2.size()
            || (i1 < tmp1.size() && cmp(tmp1[i1], tmp2[i2]))) {
            arr[i] = tmp1[i1];
            i1 += 1;
        } else if (i2 < tmp2.size()) {
            arr[i] = tmp2[i2];
            i2 += 1;
        }
    }
}

void merge2(vector<uint32_t> &arr,
            uint32_t start, uint32_t mid,
            uint32_t end, std::function<bool(uint32_t, uint32_t)> cmp) {
    // This merge realization use only one temporary array
    vector<uint32_t> tmp;
    for (auto i = start; i <= mid; i++) {
        tmp.push_back(arr[i]);
    }

    uint32_t i1 = 0;
    uint32_t i2 = mid + 1;

    for (uint32_t i = start; i <= end; i++) {
        if ((i2 == end + 1)
            || (i1 < tmp.size() && cmp(tmp[i1], arr[i2]))) {
            arr[i] = tmp[i1];
            i1 += 1;
        } else if (i2 != end + 1) {
            arr[i] = arr[i2];
            i2 += 1;
        }
    }
}


void merge_sort(vector<uint32_t> &arr, uint32_t start,
                uint32_t end, std::function<bool(uint32_t, uint32_t)> cmp) {
    // end argument must be equal (array size - 1) on first start
    if (end - start > 0) {
        uint32_t mid = start + (end - start) / 2;
        merge_sort(arr, start, mid, cmp);
        merge_sort(arr, mid + 1, end, cmp);
        merge(arr, start, mid, end, cmp);
    }
}

int main() {
    vector<uint32_t> arr1;
    vector<uint32_t> arr2;

    srand(time(0));
    for (uint32_t i = 0; i < 15; i++) {
        arr1.push_back(rand() % 100);
    }
    arr2 = arr1;

    merge_sort(arr1, 0, arr1.size() - 1, [](uint32_t a, uint32_t b) {
            return a > b;
        });
    print_vectors(vector<vector<uint32_t>>{arr1, arr2});

    return 0;
}
