//
// Trapping Rain Water
//
// Given n non-negative integers representing an elevation map where the
// width of each bar is 1, compute how much water it can trap after
// raining.
//
// Example:
//   Input: height = [0,1,0,2,1,0,1,3,2,1,2,1]
//   Output: 6
//   Explanation: The above elevation map (black section) is represented by
//                array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of
//                rain water (blue section) are being trapped.
//
// https://leetcode.com/problems/trapping-rain-water/
//
#include <iostream>
#include <vector>

class StraightSolution {
private:
  unsigned int fillPit(
    const unsigned int pitStartIdx,
    const unsigned int pitEndIdx,
    std::vector<int>& heights
  ) const
  {
    unsigned int volume = 0;
    const auto pitMaxFilledHeight = std::min(
      heights[pitStartIdx],
      heights[pitEndIdx]
    );
    for (unsigned int i = pitStartIdx + 1; i <= pitEndIdx - 1; ++i)
    {
      if (heights[i] >= pitMaxFilledHeight)
      {
        continue;
      }

      volume += pitMaxFilledHeight - heights[i];
      heights[i] = pitMaxFilledHeight;
    }

    return volume;
  }

public:
  int trap(std::vector<int>& heights) const
  {
    int prevResultVolume = -1;
    int resultVolume = 0;

    while (resultVolume != prevResultVolume)
    {
      prevResultVolume = resultVolume;

      // From left to right
      int maxLeftHeightIdx = -1;
      int maxRightHeightIdx = -1;
      for (unsigned int i = 0; i < heights.size() - 1; ++i)
      {
        const auto currentHeight = heights[i];
        const auto nextHeight = heights[i + 1];

        if (currentHeight > nextHeight) // Pit started
        {
          if (maxLeftHeightIdx == -1)
          {
            maxLeftHeightIdx = i;
          }
          else
          {
            if (maxRightHeightIdx == -1)
            {
              continue;
            }
            else // New pit started
            {
              // Calculate volume of previous
              resultVolume += fillPit(maxLeftHeightIdx, maxRightHeightIdx, heights);

              maxLeftHeightIdx = i;
              maxRightHeightIdx = -1;
            }
          }
        }
        else if (currentHeight < nextHeight) // Pit ended
        {
          maxRightHeightIdx = i + 1;
        }
        else
        {
          continue;
        }
      }
      if (maxLeftHeightIdx != -1 && maxRightHeightIdx != -1)
      {
        resultVolume += fillPit(maxLeftHeightIdx, maxRightHeightIdx, heights);
      }
    }

    return resultVolume;
  }
};

void printHeights(const std::vector<int>& heights, const std::string& text)
{
  std::cout << text << " {";
  for (unsigned int i = 0; i < heights.size(); ++i)
  {
    std::cout << heights[i];
    if (i != heights.size() - 1)
    {
      std::cout << ", ";
    }
  }
  std::cout << std::endl;
}

int main(const int argc, char **const argv)
{
  const auto sol = StraightSolution();

  {
    std::vector<int> heights = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
    printHeights(heights, "Unmodified heights:");
    std::cout << "Volume 1 (6 expected): "
              << sol.trap(heights) << std::endl;
    printHeights(heights, "MODIFIED heights:");
  }
  {
    std::vector<int> heights = {4, 2, 0, 3, 2, 5};
    printHeights(heights, "Unmodified heights:");
    std::cout << "Volume 2 (9 expected): "
              << sol.trap(heights) << std::endl;
    printHeights(heights, "MODIFIED heights:");
  }
  {
    std::vector<int> heights = {5, 4, 1, 2};
    printHeights(heights, "Unmodified heights:");
    std::cout << "Volume 3 (1 expected): "
              << sol.trap(heights) << std::endl;
    printHeights(heights, "MODIFIED heights:");
  }
  {
    std::vector<int> heights = {6, 4, 2, 0, 3, 2, 0, 3, 1, 4, 5, 3, 2,
                                7, 5, 3, 0, 1, 2, 1, 3, 4, 6, 8, 1, 3};
    printHeights(heights, "Unmodified heights:");
    std::cout << "Volume 3 (1 expected): "
              << sol.trap(heights) << std::endl;
    printHeights(heights, "MODIFIED heights:");
  }

  return 0;
}
