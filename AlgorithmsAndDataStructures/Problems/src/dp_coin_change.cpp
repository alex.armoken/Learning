//
// DP: Coin Change
//
// Complete the 'ways' function below.
//
// The function accepts following parameters:
//  1. INTEGER symm
//  2. INTEGER_ARRAY coins
//
// The function is expected to return how many ways you can make change.
//
// https://www.hackerrank.com/challenges/ctci-coin-change/problem
//

#include <bits/stdc++.h>

#define PRB_LOCAL

int ways_impl(const int requiredSum,
              const std::vector<int>& coins,
              const int currentSum,
              const int maxCoin)
{
  int result = 0;
  for (const auto coin: coins)
  {
    if (currentSum == 0)
    {
      if (coin < requiredSum)
      {
        result += ways_impl(requiredSum, coins, coin, coin);
      }
      else if (coin == requiredSum)
      {
        result++;
      }
    }
    else
    {
      if (coin <= maxCoin)
      {
        const auto newSum = currentSum + coin;
        if (newSum < requiredSum)
        {
          result += ways_impl(requiredSum, coins,
                              newSum,
                              std::min(maxCoin, coin));
        }
        else if (newSum == requiredSum)
        {
          result++;
        }
      }
    }
  }

  return result;
}

int ways(const int sum, std::vector<int>& coins)
{
  if (sum == 0 || coins.size() == 0)
  {
    return 0;
  }

  return ways_impl(sum, coins, 0, 0);
}

int ways_memoized_impl(const int sum,
                       const int i,
                       std::vector<int>& coins,
                       std::vector<std::vector<int>>& coinSumWaysMap)
{
  if (i >= coins.size() || sum < 0)
  {
    return 0;
  }

  if (sum == 0)
  {
    return 1;
  }

  if (coinSumWaysMap[i][sum] != -1)
  {
    return coinSumWaysMap[i][sum];
  }

  return coinSumWaysMap[i][sum] = ways_memoized_impl(sum - coins[i], i, coins, coinSumWaysMap)
                           + ways_memoized_impl(sum, i + 1, coins, coinSumWaysMap);
}

int ways_memoized(const int sum, std::vector<int>& coins)
{
  if (sum == 0 || coins.size() == 0)
  {
    return 0;
  }

  std::sort(coins.begin(), coins.end(), std::greater<int>());

  std::vector<std::vector<int>> coinSumWaysMap(coins.size(),
                                               std::vector<int>(sum + 1, -1));

  return ways_memoized_impl(sum, 0, coins, coinSumWaysMap);
}

int ways_tabulated(int sum, std::vector<int>& coins)
{
  if (sum == 0)
  {
    return 1;
  }

  if (sum <= 0)
  {
    return 0;
  }

  if (coins.size() == 0)
  {
    return 0;
  }

  std::vector<std::vector<int>> coinSumWaysMap(coins.size() + 1,
                                               std::vector<int>(sum + 1, 0));

  for (int i = 0; i < coins.size() + 1;i++)
  {
    coinSumWaysMap[i][0] = 1;
  }

  for (int i = coins.size() - 1; i >= 0; i--)
  {
    for (int j = 1; j < sum + 1; j++)
    {
      if (coins[i] <= j)
      {
        coinSumWaysMap[i][j] = coinSumWaysMap[i][j - coins[i]] + coinSumWaysMap[i + 1][j];
      }
      else
      {
        coinSumWaysMap[i][j] = coinSumWaysMap[i + 1][j];
      }
    }
  }

  return coinSumWaysMap[0][sum];
}

std::string ltrim(const std::string &str)
{
  std::string s(str);

  s.erase(
    s.begin(),
    std::find_if(s.begin(), s.end(),
                 std::not1(std::ptr_fun<int, int>(isspace)))
  );

  return s;
}

std::string rtrim(const std::string &str) {
  std::string s(str);

  s.erase(
    std::find_if(s.rbegin(), s.rend(),
                 std::not1(std::ptr_fun<int, int>(isspace))).base(),
    s.end()
  );

  return s;
}

std::vector<std::string> split(const std::string &str)
{
  std::vector<std::string> tokens;

  std::string::size_type start = 0;
  std::string::size_type end = 0;

  while ((end = str.find(" ", start)) != std::string::npos)
  {
    tokens.push_back(str.substr(start, end - start));

    start = end + 1;
  }

  tokens.push_back(str.substr(start));

  return tokens;
}

void printCoins(const std::vector<int>& coins,
                const std::string& text)
{
  std::cout << text << " {";
  for (unsigned int j = 0; j < coins.size(); ++j)
  {
    std::cout << std::setw(3) << coins[j];
    if (j != coins.size() - 1) {
      std::cout << ", ";
    }
  }
  std::cout << std::setw(3) << "}" << std::endl;
}

int main()
{
#if defined(PRB_LOCAL)
  {
    std::vector<int> coins = {1, 2, 3};
    std::cout << std::string(21, '=') << std::endl;
    const auto waysCount = ways_tabulated(4, coins);
    std::cout << "Test 1 - 4 (4 expected): " << waysCount << std::endl;
    printCoins(coins, "Coins:");
  }

  {
    std::vector<int> coins = {2, 3, 5, 6};
    std::cout << std::string(21, '=') << std::endl;
    const auto waysCount = ways_tabulated(10, coins);
    std::cout << "Test 2 - 10 (5 expected): " << waysCount << std::endl;
    printCoins(coins, "Coins:");
  }

  {
    std::vector<int> coins = {3, 25, 34, 38, 26, 42, 16, 10, 15, 50, 39, 44, 36, 29, 22, 43, 20, 27, 9, 30, 47, 13, 40, 33};
    std::cout << std::string(21, '=') << std::endl;
    const auto waysCount = ways_tabulated(222, coins);
    std::cout << "Test 3 - 222 (5621927 expected): " << waysCount << std::endl;
    printCoins(coins, "Coins:");
  }

  {
    std::vector<int> coins = {1, 2, 5};
    std::cout << std::string(21, '=') << std::endl;
    const auto waysCount = ways_tabulated(11, coins);
    std::cout << "Test 4 - 11 (11 expected): " << waysCount << std::endl;
    printCoins(coins, "Coins:");
  }

  return 0;

#else
  std::ofstream fout(getenv("OUTPUT_PATH"));

  std::string first_multiple_input_temp;
  getline(std::cin, first_multiple_input_temp);

  std::vector<std::string> first_multiple_input = split(rtrim(first_multiple_input_temp));

  int n = stoi(first_multiple_input[0]);

  int m = stoi(first_multiple_input[1]);

  std::string coins_temp_temp;
  getline(std::cin, coins_temp_temp);

  std::vector<std::string> coins_temp = split(rtrim(coins_temp_temp));

  std::vector<int> coins(m);

  for (int i = 0; i < m; i++)
  {
    int coins_item = stoi(coins_temp[i]);

    coins[i] = coins_item;
  }

  int res = ways(n, coins);

  fout << res << "\n";

  fout.close();

  return 0;
#endif
}
