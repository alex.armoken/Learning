//
// Trapping Rain Water II
//
// Given an m x n integer matrix heightMap representing the height of each
// unit cell in a 2D elevation map, return the volume of water it can trap
// after raining.
//
// Example 1:
//   Input: heightMap = [[1,4,3,1,3,2],[3,2,1,3,2,4],[2,3,3,2,3,1]]
//   Output: 4
//   Explanation: After the rain, water is trapped between the blocks.
//                We have two small ponds 1 and 3 units trapped.
//                The total volume of water trapped is 4.
// Example 2:
//   Input: heightMap = [[3,3,3,3,3],[3,2,2,2,3],[3,2,1,2,3],[3,2,2,2,3],[3,3,3,3,3]]
//   Output: 10
//
// Constraints:
//   m == heightMap.length
//   n == heightMap[i].length
//   1 <= m, n <= 200
//   0 <= heightMap[i][j] <= 2 * 104
//
// https://leetcode.com/problems/trapping-rain-water-ii/
//
#include <iostream>
#include <vector>
#include <numeric>
#include <queue>
#include <iomanip>

// #define PRB_TRACE

void printVisitMap(const std::vector<std::vector<bool>>& visitMap,
                   const std::string& text)
{
  std::cout << text << " {" << std::endl;
  for (unsigned int i = 0; i < visitMap.size(); ++i)
  {
    std::cout << std::string(4, ' ') << "{";
    for (unsigned int j = 0; j < visitMap[i].size(); ++j)
    {
      if (visitMap[i][j])
      {
        std::cout << "X";
      }
      else
      {
        std::cout << "O";
      }

      if (j != visitMap[i].size() - 1)
      {
        std::cout << " ";
      }
    }

    std::cout << "}";
    if (i != visitMap.size() - 1)
    {
      std::cout << ", " << std::endl;
    }
  }
  std::cout << std::endl << "}" << std::endl;
}

void printHeights(const std::vector<std::vector<int>>& heightMap,
                  const std::string& text)
{
  std::cout << text << " {" << std::endl;
  for (unsigned int i = 0; i < heightMap.size(); ++i)
  {
    std::cout << std::string(4, ' ') << "{";
    for (unsigned int j = 0; j < heightMap[i].size(); ++j)
    {
      std::cout << std::setw(2) << heightMap[i][j];
      if (j != heightMap[i].size() - 1)
      {
        std::cout << ", ";
      }
    }

    std::cout << "}";
    if (i != heightMap.size() - 1)
    {
      std::cout << ", " << std::endl;
    }
  }
  std::cout << std::endl << "}" << std::endl;
}

std::vector<std::vector<bool>> createVisitMap(
  const std::vector<std::vector<int>>& heightMap
)
{
  return std::vector<std::vector<bool>>(
    heightMap.size(),
    std::vector<bool>(heightMap.front().size(), false)
  );
}

class StraightSolution {
private:
  void resetVisitMap(std::vector<std::vector<bool>>& visitMap) const
  {
    for (auto& visitLine: visitMap)
    {
      std::fill(visitLine.begin(), visitLine.end(), false);
    }
  }

  int findMaxBorderHeight(
    const unsigned int prevI,
    const unsigned int prevJ,
    const unsigned int curI,
    const unsigned int curJ,
    const std::vector<std::vector<int>>& heightMap,
    std::vector<std::vector<bool>>& visitMap,
    std::vector<std::vector<bool>>& pitMap,
    int curMinBorderHeight
  ) const
  {
#if defined(PRB_TRACE)
    std::cerr << "Move from: i=" << std::setw(2) << prevI << ",j=" << std::setw(2) << prevJ
              << " to i=" << std::setw(2) << curI << ",j=" << std::setw(2) << curJ << std::endl;
#endif

    if (visitMap[curI][curJ])
    {
      return curMinBorderHeight;
    }
    visitMap[curI][curJ] = true;

    // If border
    if (heightMap[prevI][prevJ] < heightMap[curI][curJ])
    {
      return curMinBorderHeight == -1
        ? heightMap[curI][curJ]
        : std::min(curMinBorderHeight, heightMap[curI][curJ]);
    }

    // If break
    if (heightMap[prevI][prevJ] > heightMap[curI][curJ])
    {
      return 0;
    }

    // If on the edge
    if (curI == heightMap.size() - 1
             || curI == 0
             || curJ == heightMap[curI].size() - 1
             || curJ == 0)
    {
      return 0;
    }

    pitMap[curI][curJ] = true;

    // Up
    if (curI != 0)
    {
      curMinBorderHeight = findMaxBorderHeight(curI, curJ, curI - 1, curJ, heightMap, visitMap, pitMap, curMinBorderHeight);
    }

    // Down
    if (curI != heightMap.size() - 1 && curMinBorderHeight != 0)
    {
      curMinBorderHeight = findMaxBorderHeight(curI, curJ, curI + 1, curJ, heightMap, visitMap, pitMap, curMinBorderHeight);
    }

    // Left
    if (curJ != 0 && curMinBorderHeight != 0)
    {
      curMinBorderHeight = findMaxBorderHeight(curI, curJ, curI, curJ - 1, heightMap, visitMap, pitMap, curMinBorderHeight);
    }

    // Right
    if (curJ != heightMap[curI].size() - 1 && curMinBorderHeight != 0)
    {
      curMinBorderHeight = findMaxBorderHeight(curI, curJ, curI, curJ + 1, heightMap, visitMap, pitMap, curMinBorderHeight);
    }

    return curMinBorderHeight;
  }

  unsigned int fillPit(
    const unsigned int prevI,
    const unsigned int prevJ,
    const unsigned int curI,
    const unsigned int curJ,
    const int maxHeight,
    std::vector<std::vector<bool>>& visitMap,
    std::vector<std::vector<int>>& heightMap
  ) const
  {
    unsigned int volume = 0;

    if (visitMap[curI][curJ])
    {
      return volume;
    }
    visitMap[curI][curJ] = true;

    // If border
    if (heightMap[prevI][prevJ] != heightMap[curI][curJ])
    {
      return 0;
    }

    // Up
    if (curI != 0)
    {
      volume += fillPit(curI, curJ, curI - 1, curJ, maxHeight, visitMap, heightMap);
    }

    // Down
    if (curI != heightMap.size() - 1)
    {
      volume += fillPit(curI, curJ, curI + 1, curJ, maxHeight, visitMap, heightMap);
    }

    // Left
    if (curJ != 0)
    {
      volume += fillPit(curI, curJ, curI, curJ - 1 , maxHeight, visitMap, heightMap);
    }

    // Right
    if (curJ != heightMap[curI].size() - 1)
    {
      volume += fillPit(curI, curJ, curI, curJ + 1, maxHeight, visitMap, heightMap);
    }

    volume += std::abs(maxHeight - heightMap[curI][curJ]);
    heightMap[curI][curJ] = maxHeight;

    return volume;
  }

  unsigned int fillPit(
    const int fillHeight,
    std::vector<std::vector<bool>>& pitMap,
    std::vector<std::vector<int>>& heightMap
  ) const
  {
    unsigned int volume = 0;
    for (unsigned int i = 0; i < heightMap.size(); ++i)
    {
      for (unsigned int j = 0; j < heightMap[i].size(); ++j)
      {
        if (pitMap[i][j])
        {
          volume += fillHeight - heightMap[i][j];
          heightMap[i][j] = fillHeight;
        }
      }
    }

    return volume;
  }

  int trapRainWaterImpl(std::vector<std::vector<int>>& heightMap) const
  {
    unsigned int volume = 0;
    auto visitMap = createVisitMap(heightMap);
    auto pitMap = createVisitMap(heightMap);

    for (unsigned int i = 0; i < heightMap.size(); ++i)
    {
      for (unsigned int j = 0; j < heightMap[i].size(); ++j)
      {
        const int fillHeight = findMaxBorderHeight(i, j, i, j, heightMap, visitMap, pitMap, -1);
        if (fillHeight <= 0)
        {
#if defined(PRB_TRACE)
          std::cerr << std::string(5, '-') << std::endl;
#endif

          resetVisitMap(visitMap);
          resetVisitMap(pitMap);
          continue;
        }

#if defined(PRB_TRACE)
        std::cerr << "Fill height: " << fillHeight << "  i: " << i << " - j: " << j << std::endl;
        printVisitMap(visitMap, "VISIT MAP");
#endif

        // volume += fillPit(i, j, i, j, maxHeight, visitMap, heightMap);
        volume += fillPit(fillHeight, pitMap, heightMap);

#if defined(PRB_TRACE)
        std::cerr << "Intermediate volume: " << volume << std::endl;
        printHeights(heightMap, "MODIFIED HEIGHTS");
        printVisitMap(pitMap, "PIT MAP");
        std::cerr << std::string(14, '-') << std::endl;
#endif

        resetVisitMap(visitMap);
        resetVisitMap(pitMap);
      }
    }

    return volume;
  }

public:
  int trapRainWater(std::vector<std::vector<int>>& heightMap) const
  {
    unsigned int volume = 0;

    while (true)
    {
      const auto intermediateVolume = trapRainWaterImpl(heightMap);
      if (intermediateVolume == 0)
      {
        break;
      }

      volume += intermediateVolume;
    }

    return volume;
  }
};

class PriorityBFSSolution {
private:
  class Cell
  {
  public:
    Cell() = delete;

    Cell(
      const int x,
      const int y,
      const int height
    ) : x(x), y(y), height(height)
    {}

    Cell(const Cell&) = default;
    Cell& operator=(const Cell&) = default;

    ~Cell() = default;

    bool operator>(const Cell& other) const
    {
      return height > other.height;
    }

    int x = 0;
    int y = 0;
    int height = 0;
  };

  class Step
  {
  public:
    Step() = delete;
    Step(const int x, const int y)
      : x(x), y(y)
    {}
    ~Step() = default;

    const int x;
    const int y;
  };

  using MinHeap = std::priority_queue<Cell, std::vector<Cell>, std::greater<Cell>>;

  void initHeap(
    MinHeap& minHeap,
    std::vector<std::vector<bool>>& visitMap,
    const std::vector<std::vector<int>>& heightMap
  ) const
  {
    const int rowsCount = heightMap.size();
    const int colsCount = heightMap.front().size();

    for (int i = 0; i < rowsCount; ++i)
    {
      for (int j = 0; j < colsCount; ++j)
      {
        if (i == 0
            or j == 0
            or i == rowsCount - 1
            or j == colsCount - 1)
        {
          minHeap.emplace(i, j, heightMap[i][j]);
          visitMap[i][j] = true;
        }
      }
    }
  }

public:
  int trapRainWater(const std::vector<std::vector<int>>& heightMap) const
  {
    const int rowsCount = heightMap.size();
    const int colsCount = heightMap.front().size();
    if (rowsCount <= 2 or colsCount <= 2)
    {
      return 0;
    }

    MinHeap minHeap;
    auto visitMap = createVisitMap(heightMap);
    initHeap(minHeap, visitMap, heightMap);

    unsigned int resultVolume = 0;
    while (!minHeap.empty())
    {
      const Cell curCell = minHeap.top();
      minHeap.pop();

      static const std::vector<Step> steps = {
        {-1, 0},
        {1,  0},
        {0, -1},
        {0,  1}
      };
      for (const auto& step: steps)
      {
        const int nextX = curCell.x + step.x;
        const int nextY = curCell.y + step.y;

        if (nextX >= 0
            and nextY >= 0
            and nextX < rowsCount
            and nextY < colsCount
            and !visitMap[nextX][nextY])
        {
          resultVolume += std::max(0, curCell.height - heightMap[nextX][nextY]);

          minHeap.emplace(
            nextX, nextY,
            std::max(heightMap[nextX][nextY], curCell.height)
          );

          visitMap[nextX][nextY] = true;
        }
      }
    }

    return resultVolume;
  }
};

int main(const int /*argc*/, char **const /*argv*/)
{
  const auto sol = PriorityBFSSolution();

  {
    std::vector<std::vector<int>> heightMap = {
        {1, 4, 3, 1, 3, 2},
        {3, 2, 1, 3, 2, 4},
        {2, 3, 3, 2, 3, 1}
    };
    std::cerr << std::string(21, '=') << std::endl;
    printHeights(heightMap, "Unmodified heights 1:");
    const auto volume = sol.trapRainWater(heightMap);
    std::cout << "Volume 1 (4 expected): " << volume << std::endl;
  }

  {
    std::vector<std::vector<int>> heightMap = {
      {3, 3, 3, 3, 3},
      {3, 2, 2, 2, 3},
      {3, 2, 1, 2, 3},
      {3, 2, 2, 2, 3},
      {3, 3, 3, 3, 3}
    };
    std::cerr << std::string(21, '=') << std::endl;
    printHeights(heightMap, "Unmodified heights 2:");
    const auto volume = sol.trapRainWater(heightMap);
    std::cout << "Volume 2 (10 expected): " << volume << std::endl;
  }

  {
    std::vector<std::vector<int>> heightMap = {
      {5, 8, 7, 7},
      {5, 2, 1, 5},
      {7, 1, 7, 1},
      {8, 9, 6, 9},
      {9, 8, 9, 9}
    };
    std::cerr << std::string(21, '=') << std::endl;
    printHeights(heightMap, "Unmodified heights 3:");
    const auto volume = sol.trapRainWater(heightMap);
    std::cout << "Volume 3 (12 expected): " << volume << std::endl;
  }

  {
    std::vector<std::vector<int>> heightMap = {
      {9, 9, 9, 9, 9},
      {9, 2, 1, 2, 9},
      {9, 2, 8, 2, 9},
      {9, 2, 3, 2, 9},
      {9, 9, 9, 9, 9}
    };

    std::cerr << std::string(21, '=') << std::endl;
    printHeights(heightMap, "Unmodified heights 4:");
    const auto volume = sol.trapRainWater(heightMap);
    std::cout << "Volume 4 (57 expected): " << volume << std::endl;
  }

  {
    std::vector<std::vector<int>> heightMap = {
        {14, 17, 18, 16, 14, 16},
        {17, 3, 10, 2, 3, 8},
        {11, 10, 4, 7, 1, 7},
        {13, 7, 2, 9, 8, 10},
        {13, 1, 3, 4, 8, 6},
        {20, 3, 3, 9, 10, 8}
    };

    std::cerr << std::string(21, '=') << std::endl;
    printHeights(heightMap, "Unmodified heights 5:");
    const auto volume = sol.trapRainWater(heightMap);
    std::cout << "Volume 5 (25 expected): " << volume << std::endl;
  }

  {
    std::vector<std::vector<int>> heightMap = {
      {14, 17, 18},
      {17, 3, 10},
      {11, 10, 4},
    };

    std::cerr << std::string(21, '=') << std::endl;
    printHeights(heightMap, "Unmodified heights 6:");
    const auto volume = sol.trapRainWater(heightMap);
    std::cout << "Volume 6 (7 expected): " << volume << std::endl;
  }

  return 0;
}
