BUILD_DIR      := ./build
CONFIGURATION  ?= Debug
OUTPUT_DIR     := ./output
EXECUTABLE_DIR ?= $(BUILD_DIR)/$(CONFIGURATION)

PROBLEM_FILES   := $(shell find ./ -maxdepth 1 -type f -name "*.cpp" -printf "%f\n")
PROBLEM_TARGETS := $(foreach problem_file,$(PROBLEM_FILES),$(basename $(problem_file)))


.PHONY: all
all: configure build


.PHONY: $(foreach problem_target,$(PROBLEM_TARGETS),all_$(problem_target))
$(foreach problem_target,$(PROBLEM_TARGETS),all_$(problem_target)): all_% : configure build_%


.PHONY: configure
configure:
	@echo '{{{Start configuring}}}'
	@mkdir --parents $(BUILD_DIR) \
		&& cmake ./CMakeLists.txt \
				 -B$(BUILD_DIR) \
				 -DCMAKE_BUILD_TYPE=$(CONFIGURATION) \
				 -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ../
	@cp $(BUILD_DIR)/compile_commands.json ./

.PHONY: configure_debug
configure_debug:
	@CONFIGURATION=Debug $(MAKE) --silent configure

.PHONY: configure_release
configure_release:
	@CONFIGURATION=Release $(MAKE) --silent configure


.PHONY: $(foreach problem_target,$(PROBLEM_TARGETS),build_$(problem_target))
$(foreach problem_target,$(PROBLEM_TARGETS),build_$(problem_target)):
	@echo "<<<Start building $(subst build_,,$@)>>>"
	@cmake \
		--build ${BUILD_DIR} \
		--target "$(subst build_,,$@)" \
		--parallel \
		-- --silent # build-tool options

.PHONY: build
build:
	@echo '{{{Start building all}}}'
	@cmake \
		--build ${BUILD_DIR} \
		--config $(CONFIGURATION) \
		--parallel \
		-- --silent # build-tool options

.PHONY: clean
clean:
	@rm --recursive --force $(BUILD_DIR)
	@rm --recursive --force ${OUTPUT_DIR}
	@rm --force ./compile_commands.json


# Workaround from:
#  https://stackoverflow.com/questions/4219255/how-do-you-get-the-list-of-targets-in-a-makefile
.PHONY: list_targets
list_targets:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null \
		| awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}'\
		| sort \
		| egrep -v -e '^[^[:alnum:]]' -e '^$@$$'
