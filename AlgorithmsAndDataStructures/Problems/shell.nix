{ pkgs ? import <nixpkgs>{} }:
with import <nixpkgs>{};
let
in pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    ccls
    clang-tools
    lldb_12
    cmake
    gdb
    gcc
    fish
    mono # For cpptools (debug)
  ];

  inputsFrom = with pkgs; [
  ];

  runScript = "fish";
}
