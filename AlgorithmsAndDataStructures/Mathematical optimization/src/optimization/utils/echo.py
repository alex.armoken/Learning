def build_matrix_with_msg_string(msg, matrix):
    """Build msg string with indented matrix.

    Args:
        msg (str)
        matrix (numpy.matrix)

    Returns:
        str
    """
    str_matrix = []
    matrix_lines = str(matrix).split("\n")
    for i, line in enumerate(matrix_lines):
        if i == 0:
            formated_line = "{}\n".format(line)
        elif i == len(matrix_lines) - 1:
            formated_line = "{}{}".format(len(msg) * " ", line)
        else:
            formated_line = "{}{}\n".format(len(msg) * " ", line)

        str_matrix.append(formated_line)

    return "{}{}".format(msg, "".join(str_matrix))
