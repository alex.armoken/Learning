import sys
import math
import logging
import operator

import numpy

logger = logging.getLogger(__name__)


class UnlimitedSetOfPlans(Exception):
    pass


class IncompatibleRestrictions(Exception):
    pass


def calculate_potential_vector(base_product_profits,
                               inverse_base_matrix):
    """Calculate potential vector.

    Args:
        base_product_profits (numpy.matrix): one row
        inverse_base_matrix (numpy.matrix)

    Returns:
        numpy.matrix: one row
    """
    return base_product_profits * inverse_base_matrix


def calculate_vector_of_estimates(potential_vector,
                                  resource_cost_of_products,
                                  product_profits,
                                  nonbase_indexes):
    """Calculate potential vector.

    Args:
        potential_vector (numpy.matrix): one row
        resource_cost_of_products (numpy.matrix)
        product_profits (numpy.matrix): one row
        nonbase_indexes (list)

    Returns:
        list of tuples (nonbase index, estimate)
    """
    vector_of_estimates = []
    for nonbase_index in nonbase_indexes:
        estimate = (potential_vector
                    * resource_cost_of_products[:, nonbase_index]
                    - product_profits[0, nonbase_index])[0, 0]
        vector_of_estimates.append((nonbase_index, estimate))

    return vector_of_estimates


def calculate_vector_z(inverse_base_matrix,
                       resource_cost_of_products,
                       min_estimate_index):
    """Calculate vector z.

    Args:
        inverse_base_matrix (numpy.matrix)
        resource_cost_of_products (numpy.matrix)
        min_estimate_index (int)

    Returns:
        numpy.matrix: one row
    """
    result = inverse_base_matrix * resource_cost_of_products[:, min_estimate_index]
    return result.T


def calculate_vector_theta(base_plan,
                           base_indexes,
                           vector_z):
    """Calculate vector theta.

    Args:
        base_plan (numpy.matrix): one row
        base_indexes (list)
        vector_z (numpy.matrix): one row

    Returns:
        numpy.matrix: one row
    """
    theta = []
    for i, base_index in enumerate(base_indexes):
        if vector_z[0, i] <= 0:
            theta_el = math.inf
        else:
            theta_el = base_plan[0, base_index] / vector_z[0, i]

        theta.append(theta_el)

    return numpy.matrix(theta, dtype=numpy.float)


def move_to_a_new_base_plan(base_plan,
                            base_indexes,
                            new_index,
                            s,
                            min_theta,
                            vector_z):
    """Build new base plan from current.

    Args:
        base_plan (numpy.array): one row
        base_indexes (list): set of base indexes
        new_index (int): new base index
        s (int): index of old base index in base indexes
        min_theta (float): minimal value of vector theta
        vector_z (numpy.matrix): one row

    Returns:
        (numpy.matrix, list): tuple of new base plan and base indexes
    """
    new_base_plan = numpy.matrix(base_plan, dtype=numpy.float)
    for i, z_el in enumerate(vector_z.A1):
        base_index = base_indexes[i]
        new_base_plan[0, base_index] -= min_theta * z_el

    new_base_plan[0, new_index] = min_theta

    old_index = base_indexes[s]
    new_base_plan[0, old_index] = 0  # Fix to prevent floating point problems

    new_base_indexes = list(base_indexes)
    new_base_indexes[s] = new_index
    return (new_base_plan, new_base_indexes)


def calculate_new_inverse_base_matrix(vector_z,
                                      inverse_base_matrix,
                                      s):
    """Calculate inverse base matrix for new base plan.

    Args:
        vector_z (numpy.matrix): one row
        inverse_base_matrix (numpy.matrix): inverse base matrix
        s (int): index of old base index in base indexes

    Returns:
        numpy.matrix
    """
    z_s = vector_z[0, s]
    vector_z[0, s] = -1
    m_matrix = numpy.matrix(numpy.eye(vector_z.shape[1]))
    m_matrix[:, s] = -vector_z.T / z_s
    return m_matrix * inverse_base_matrix


def iteration(resource_cost_of_products,
              product_profits,
              inverse_base_matrix,
              base_plan,
              base_indexes):
    """One iteration of simplex algorithm.

    Args:
        resource_cost_of_products (numpy.matrix)
        product_profits (numpy.matrix): one row
        inverse_base_matrix (numpy.matrix)
        base_plan (numpy.matrix): one row
        base_indexes (list)

    Raises:
        UnlimitedSetOfPlans: if objective function unlimited on set of plans
        StopIteration: if current base plan optimal

    Returns:
        (numpy.matrix, numpy.matrix, list): tuple of new inverse base matrix,
            new base plan and base indexes
    """
    nonbase_indexes = list(set(range(base_plan.shape[1])) - set(base_indexes))
    base_product_profits = product_profits[0, base_indexes]

    potential_vector = calculate_potential_vector(base_product_profits,
                                                  inverse_base_matrix)
    vector_of_estimates = calculate_vector_of_estimates(
        potential_vector,
        resource_cost_of_products,
        product_profits,
        nonbase_indexes
    )

    # Bland's rule, to prevent cycling (1st rule)
    min_estimate_index, min_estimate = None, None
    for nonbase_index, estimate in vector_of_estimates:
        if estimate < 0:
            min_estimate_index = nonbase_index
            min_estimate = estimate
            break

    if min_estimate is None:
        raise StopIteration

    vector_z = calculate_vector_z(inverse_base_matrix,
                                  resource_cost_of_products,
                                  min_estimate_index)

    count_of_negative_elements = sum([1 if el <= 0 else 0
                                      for el in vector_z.A1])
    if count_of_negative_elements == vector_z.shape[1]:
        raise UnlimitedSetOfPlans

    # Bland's rule, to prevent cycling (2nd rule)
    vector_theta = calculate_vector_theta(base_plan, base_indexes, vector_z)
    s, min_theta = min(enumerate(vector_theta.A1), key=operator.itemgetter(1))

    base_plan_and_indexes = move_to_a_new_base_plan(
        base_plan,
        base_indexes,
        min_estimate_index,
        s,
        min_theta,
        vector_z
    )
    new_inverse_base_matrix = calculate_new_inverse_base_matrix(
        vector_z,
        inverse_base_matrix,
        s
    )
    return (new_inverse_base_matrix, *base_plan_and_indexes)


def algorithm(resource_cost_of_products,
              product_profits,
              base_plan,
              base_indexes):
    """Simplex algorithm.

    Args:
        resource_cost_of_products (list of lists)
        product_profits (list)
        base_plan (list): start base plan
        base_indexes (list): start base indexes

    Raises:
        UnlimitedSetOfPlans: if objective function unlimited on set of plans
        StopIteration: if current base plan optimal

    Returns:
        tuple of (list, list): tuple of optimal base plan and base indexes
    """
    resource_cost_of_products = numpy.matrix(resource_cost_of_products,
                                             dtype=numpy.float)
    product_profits = numpy.matrix(product_profits, dtype=numpy.float)
    base_plan = numpy.matrix(base_plan, dtype=numpy.float)

    base_costs_matrix = resource_cost_of_products[:, base_indexes]
    inverse_base_matrix = base_costs_matrix.I
    args = (inverse_base_matrix, base_plan, base_indexes)

    while True:
        try:
            args = iteration(resource_cost_of_products,
                             product_profits,
                             *args)
        except StopIteration:
            result = (list(args[1].A1), args[2])
            break
        except UnlimitedSetOfPlans:
            result = (None, None)
            break

    return result


def first_phase(resource_coust_for_the_product,
                resource_limits,
                product_profits):
    """First phase of simplex method. To get start base plan.

    Args:
        resource_coust_for_the_product (numpy.matrix): matrix of products coasts in resources
        resource_limits (numpy.matrix): resource limits (one column)
        product_profits (numpy.matrix): product profits (one column)

    Returns:
        tuple of (list, list): tuple of start base plan and base indexes
    """
    return (None, None)


def two_phase_simplex_method(resource_coust_for_the_product,
                             resource_limits,
                             product_profits):
    """Two phase simplex method.

    Args:
        resource_coust_for_the_product (numpy.matrix): matrix of products coasts in resources
        resource_limits (numpy.matrix): resource limits (one column)
        product_profits (numpy.matrix): product profits (one column)

    Returns:
        tuple of (list, list): tuple of optimal base plan and base indexes
    """
    base_plan, base_indexes = first_phase(
        resource_coust_for_the_product,
        resource_limits,
        product_profits
    )
    if base_plan is not None:
        base_plan, base_indexes = algorithm(
            resource_coust_for_the_product,
            product_profits,
            base_plan,
            base_indexes
        )
        return (base_plan, base_indexes)

    return (None, None)


def calc_base_chi_vec(resource_limits,
                      inverse_base_matrix):
    """Calculate base pseudo plan vector.

    Args:
        resource_limits (numpy.matrix): one row
        inverse_base_matrix (numpy.matrix)

    Returns:
        numpy.matrix: one row
    """
    return (inverse_base_matrix * resource_limits.T).T


def calc_delta_vec(dual_base_plan,
                   resource_cost_of_products,
                   product_profits):
    """Calculate coplan vector.

    Args:
        dual_base_plan (numpy.matrix): one row
        resource_cost_of_products (numpy.matrix)
        product_profits (numpy.matrix): one row

    Returns:
        numpy.matrix: one row
    """
    return dual_base_plan * resource_cost_of_products - product_profits


def calc_vec_mu(resource_cost_of_products,
                inverse_base_matrix,
                k):
    """Calculate vector mu.

    Args:
        resource_cost_of_products (numpy.matrix): one row
        inverse_base_matrix (numpy.matrix)
        k (int):

    Returns:
        numpy.matrix: one row
    """
    e_k = numpy.matrix(numpy.zeros(resource_cost_of_products.shape[0]))
    e_k[0, k] = 1
    return e_k * inverse_base_matrix * resource_cost_of_products


def calc_sigma_vec(vec_delta,
                   vec_mu,
                   nonbase_indexes):
    """Calculate vector sigma.

    Args:
        vec_delta (numpy.matrix): one row
        vec_mu (dict of {nonbase idx: vector mu el})
        nonbase_indexes (list)

    Returns:
        list
    """
    sigma_vec = []
    for idx in nonbase_indexes:
        if vec_mu[0, idx] < 0:
            sigma_vec.append(-vec_delta[0, idx] / vec_mu[0, idx])
        else:
            sigma_vec.append(math.inf)

    return sigma_vec


def calc_new_dual_base_plan(dual_base_plan,
                            k,
                            inverse_base_matrix,
                            min_sigma,
                            resource_type_count):
    """Build new dual base plan from current.

    Args:
        dual_base_plan (numpy.matrix): one row
        k (int): index of base index
        inverse_base_matrix (numpy.matrix)
        min_sigma (float)
        resource_type_count (int)

    Returns:
        numpy.matrix: one row
    """
    e_k = numpy.matrix(numpy.zeros(resource_type_count))
    e_k[0, k] = 1
    new_dual_base_plan = dual_base_plan + min_sigma * e_k * inverse_base_matrix
    return new_dual_base_plan


def calc_new_vec_delta(vec_delta,
                       min_sigma,
                       vec_mu):
    """Calculate new vec delta and new base indexes.

    Args:
        vec_delta (numpy.matrix): coplan vector (one row)
        min_sigma (float)
        vec_mu (numpy.matrix)

    Returns:
        numpy.matrix: one row
    """
    return vec_delta + min_sigma * vec_mu


def calc_new_base_indexes(base_indexes,
                          k,
                          new_index):
    """Calculate new base indexes.

    Args:
        base_indexes (list)
        k (int): index of old index in base indexes
        new_index (int)

    Returns:
        list
    """
    new_base_indexes = list(base_indexes)
    new_base_indexes[k] = new_index
    return new_base_indexes


def calc_new_inverse_base_matrix(vec_mu,
                                 new_base_index,
                                 k,
                                 base_indexes,
                                 inverse_base_matrix,
                                 resource_cost_of_products):
    """Calculate inverse base matrix for new vec delta.

    Args:
        vec_mu (dict of {nonbase index: vec mu el})
        new_base_index (int)
        k (int): index of old index in base indexes
        base_indexes (list)
        inverse_base_matrix (numpy.matrix)
        resource_cost_of_products (numpy.matrix)

    Returns:
        numpy.matrix
    """
    e_k = numpy.matrix(numpy.zeros(resource_cost_of_products.shape[0]))
    e_k[0, k] = 1
    alpha = vec_mu[0, new_base_index]
    old_base_index = base_indexes[k]

    return inverse_base_matrix - (
        inverse_base_matrix * (
            resource_cost_of_products[:, new_base_index] -
            resource_cost_of_products[:, old_base_index]
        ) * e_k * inverse_base_matrix
    ) / alpha


def dual_iteration(resource_cost_of_products,
                   product_profits,
                   resource_limits,
                   inverse_base_matrix,
                   vec_delta,
                   dual_base_plan,
                   base_indexes):
    """One iteration of dual simplex algorithm.

    Args:
        resource_cost_of_products (numpy.matrix)
        product_profits (numpy.matrix): one row
        resource_limits (numpy.matrix): one row
        inverse_base_matrix (numpy.matrix)
        vec_delta (numpy.matrix): one row
        dual_base_plan (numpy.matrix): one row
        base_indexes (list): base indexes

    Raises:
        UnlimitedSetOfPlans: if objective function unlimited on set of plans
        StopIteration: if current base plan optimal

    Returns:
        tuple of (numpy.matrix, numpy.matrix, numpy.matrix, list): tuple of
            new inverse base matrix,
            new vector delta (one row),
            new dual base plan (one row),
            base indexes
    """
    nonbase_indexes = list(set(range(vec_delta.shape[1])) - set(base_indexes))
    base_vec_chi = calc_base_chi_vec(resource_limits,
                                     inverse_base_matrix)
    neg_chi_els = []
    for chi_index, chi_el in enumerate(base_vec_chi.A1):
        if chi_el < 0:
            neg_chi_els.append((base_indexes[chi_index], chi_index))

    if not neg_chi_els:
        raise StopIteration

    # Bland's rule, to prevent cyclin (1st rule)
    k = min(neg_chi_els)[1]
    vec_mu = calc_vec_mu(resource_cost_of_products, inverse_base_matrix, k)

    vec_sigma = calc_sigma_vec(vec_delta, vec_mu, nonbase_indexes)
    # Bland's rule, to prevent cyclin (2nd rule)
    new_base_index, min_sigma = min(zip(nonbase_indexes, vec_sigma),
                                    key=operator.itemgetter(1))
    if math.isinf(min_sigma):
        raise IncompatibleRestrictions

    resource_type_count = resource_cost_of_products.shape[0]
    new_dual_base_plan = calc_new_dual_base_plan(
        dual_base_plan,
        k,
        inverse_base_matrix,
        min_sigma,
        resource_type_count
    )
    new_vec_delta = calc_new_vec_delta(vec_delta, min_sigma, vec_mu)
    new_base_indexes = calc_new_base_indexes(base_indexes, k, new_base_index)
    inverse_base_matrix = calc_new_inverse_base_matrix(
        vec_mu,
        new_base_index,
        k,
        base_indexes,
        inverse_base_matrix,
        resource_cost_of_products
    )
    return (inverse_base_matrix, new_vec_delta,
            new_dual_base_plan, new_base_indexes)


def dual_algorithm(resource_cost_of_products,
                   product_profits,
                   resource_limits,
                   base_dual_plan,
                   base_indexes):
    """Dual simplex algorithm.

    Args:
        resource_cost_of_products (list of lists)
        product_profits (list)
        resource_limits (list)
        base_dual_plan (list): start base plan
        base_indexes (list): start base indexes

    Raises:
        UnlimitedSetOfPlans: if objective function unlimited on set of plans
        StopIteration: if current base plan optimal

    Returns:
        list: optimal base plan
    """
    resource_cost_of_products = numpy.matrix(resource_cost_of_products,
                                             dtype=numpy.float)
    product_profits = numpy.matrix(product_profits, dtype=numpy.float)
    resource_limits = numpy.matrix(resource_limits, dtype=numpy.float)

    inverse_base_matrix = resource_cost_of_products[:, base_indexes].I
    dual_base_plan = numpy.matrix(base_dual_plan, dtype=numpy.float)
    vec_delta = calc_delta_vec(base_dual_plan, resource_cost_of_products,
                               product_profits)
    args = (inverse_base_matrix, vec_delta, dual_base_plan, base_indexes)

    while True:
        try:
            args = dual_iteration(resource_cost_of_products,
                                  product_profits,
                                  resource_limits,
                                  *args)
        except StopIteration:
            break
        except IncompatibleRestrictions:
            logger.info("Attention: Incompatible restrictions!")
            return (None, None)

    return (list(args[2].A1), args[3])


def simplex_algo_using_example():
    # resource_coust_for_the_product = [
    #     [0, -1, 1, -7.5, 0, 0, 0, 2],
    #     [0, 2, 1, 0, -1, 3, -1.5, 0],
    #     [1, -1, 1, -1, 0, 3, 1, 1]
    # ]
    # resource_limits = [6, 1.5, 10]
    # product_profits = [-6, -9, -5, 2, -6, 0, 1, 3]
    # base_plan = [4, 0, 6, 0, 4.5, 0, 0, 0]
    # base_indexes = [0, 2, 4]

    resource_coust_for_the_product = [
        [0, 1, 4, 1, 0, -8, 1, 5],
        [0, -1, 0, -1, 0, 0, 0, 0],
        [0, 2, -1, 0, -1, 3, -1, 0],
        [1, 1, 1, 1, 0, 3, 1, 1]
    ]
    resource_limits = [36, -11, 10, 20]
    product_profits = [-5, 2, 3, -4, -6, 0, 1, -5]
    base_plan = [4, 5, 0, 6, 0, 0, 0, 5]
    base_indexes = [0, 1, 3, 7]

    base_plan, base_indexes = algorithm(
        resource_coust_for_the_product,
        product_profits,
        base_plan,
        base_indexes
    )

    if base_plan is not None:
        print("Base plan: {}".format(base_plan))
        print("Base indexes: {}".format(base_indexes))

        max_profit = 0
        for i, plan_el in enumerate(base_plan):
            max_profit += plan_el * product_profits[i]

        print("Max profit: {}".format(max_profit))
    else:
        print("No solution!")


def dual_simplex_algo_using_example():
    # resource_coust_for_the_product = [
    #     [-2, -1, 1, -7, 0, 0, 0, 2],
    #     [4, 2, 1, 0, 1, 5, -1, -5],
    #     [1, 1, 0, -1, 0, 3, -1, 1],
    # ]
    # resource_limits = [-2, 4, 3]
    # product_profits = [2, 2, 1, -10, 1, 4, -2, -3]
    # dual_base_plan = [1, 1, 1]
    # base_indexes = [1, 4, 6]

    resource_coust_of_products = [
        [-2, -1, 1, -7, 1, 0, 0, 2],
        [-4, 2, 1, 0, 5, 1, -1, -5],
        [1, 1, 0, -1, 0, 3, -1, 1],
    ]
    resource_limits = [-2, 4, -2]
    product_profits = [-12, 2, 2, -6, 10, -1, -9, -8]
    dual_base_plan = [1, 2, -1]
    base_indexes = [1, 3, 5]

    # resource_coust_for_the_product = [
    #     [-2, -1, 1, -7, 1, 0, 0, 2],
    #     [-4, 2, 1, 0, 5, 1, -1, -5],
    #     [1, 1, 0, 1, 4, 3, 1, 1],
    # ]
    # resource_limits = [-2, 8, -2]
    # product_profits = [12, -2, -6, 20, -18, -5, -7, -20]
    # dual_base_plan = [-3, -2, -1]
    # base_indexes = [1, 3, 5]

    dual_base_plan, base_indexes = dual_algorithm(
        resource_coust_of_products,
        product_profits,
        resource_limits,
        dual_base_plan,
        base_indexes
    )

    if dual_base_plan is not None:
        print("Dual base plan:", dual_base_plan)
        print("Base indexes:", base_indexes)

        base_matrix = numpy.matrix(resource_coust_of_products)[:, base_indexes]
        base_plan = base_matrix.I * numpy.matrix(resource_limits).T
        print("Base plan:", list(base_plan.A1))

        print("Max profit:", (numpy.matrix(resource_limits, dtype=numpy.float)
                              * numpy.matrix(dual_base_plan).T)[0, 0])
    else:
        print("No solution!")


def main():
    logger.setLevel(logging.INFO)

    print("{} Simplex algo {}".format("<" * 45, ">" * 45))
    simplex_algo_using_example()
    print("{} Dual simplex algo {}".format("<" * 45, ">" * 45))
    dual_simplex_algo_using_example()


if __name__ == "__main__":
    sys.exit(main())
