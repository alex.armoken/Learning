import sys
import math
import logging
import operator

import numpy

logger = logging.getLogger(__name__)


def calc_vec_c_from_support_plan(vec_c,
                                 matrix_d,
                                 plan):
    """Calculate vector c from support plan.

    Args:
        vec_c (numpy.matrix): one row
        matrix_d (numpy.matrix): positive-semidefinite matrix
        plan (numpy.matrix): one row

    Returns:
        numpy.matrix: one row
    """
    return (matrix_d * plan.T + vec_c.T).T


def calc_vec_of_potentials(vec_c_from_plan,
                           support_of_limitations,
                           support_inverse_matrix_a):
    """Calculate vector of potentials.

    Args:
        vec_c_from_plan (numpy.matrix): one row
        support_of_limitations (list)
        support_inverse_matrix_a (numpy.matrix)

    Returns:
        numpy.matrix: one row
    """
    return -vec_c_from_plan[:, support_of_limitations] \
        * support_inverse_matrix_a


def calc_vec_of_estimates(vec_of_potentials,
                          matrix_a,
                          vec_c_from_plan):
    """Calculate vector of potentials.

    Args:
        vec_of_potentials (numpy.matrix): one row
        matrix_a (numpy.matrix)
        vec_c_from_plan (numpy.matrix): one row

    Returns:
        numpy.matrix: one row
    """
    return vec_of_potentials * matrix_a + vec_c_from_plan


def construct_matrix_h(matrix_a,
                       matrix_d,
                       extended_support):
    """Construct matrix H.

    Args:
        matrix_a (numpy.matrix)
        matrix_d (numpy.matrix)
        extended_support (list)

    Returns:
        numpy.matrix
    """
    extended_support_cardinality = len(extended_support)
    es_matrix_a = matrix_a[:, extended_support]  # Extended support matrix
    matrix_a_rows_count, matrix_a_columns_count = es_matrix_a.shape

    h_size = matrix_a_rows_count + extended_support_cardinality
    matrix_h = numpy.matrix(numpy.zeros((h_size, h_size)), dtype=numpy.float)

    support_matrix_d = matrix_d[extended_support, :][:, extended_support]

    for i in range(extended_support_cardinality):
        for j in range(extended_support_cardinality):
            matrix_h[i, j] = support_matrix_d[i, j]

    for i in range(matrix_a_columns_count):
        for j in range(matrix_a_columns_count):
            matrix_h[i + extended_support_cardinality, j] = es_matrix_a[i, j]

    es_matrix_a_t = es_matrix_a.T
    matrix_a_rows_count, matrix_a_columns_count = (matrix_a_columns_count,
                                                   matrix_a_rows_count)
    for i in range(matrix_a_rows_count):
        for j in range(matrix_a_columns_count):
            matrix_h[i, j + extended_support_cardinality] = es_matrix_a_t[i, j]

    return matrix_h


def construct_vec_bb(matrix_a,
                     matrix_d,
                     extended_support,
                     min_neg_estimate_index):
    """Construct vector bb.

    Args:
        matrix_a (numpy.matrix)
        matrix_d (numpy.matrix)
        extended_support (list)
        min_neg_estimate_index (int)

    Returns:
        numpy.matrix: one row
    """
    matrix_a_rows_count = matrix_a.shape[0]
    extended_support_cardinality = len(extended_support)
    result = numpy.zeros(matrix_a_rows_count + extended_support_cardinality)

    left_columns_with_numbers = zip(
        range(extended_support_cardinality),
        matrix_d[extended_support, min_neg_estimate_index].A1
    )
    for i, element in left_columns_with_numbers:
        result[i] = element

    right_columns_with_numbers = zip(
        range(extended_support_cardinality,
              extended_support_cardinality + matrix_a_rows_count),
        matrix_a[:, min_neg_estimate_index].A1
    )
    for i, element in right_columns_with_numbers:
        result[i] = element

    return numpy.matrix(result)


def calc_vec_of_directions(matrix_a,
                           matrix_d,
                           extended_support,
                           min_neg_estimate_index):
    """Calculate vector of directions.

    Args:
        matrix_a (numpy.matrix)
        matrix_d (numpy.matrix)
        extended_support (list)
        min_neg_estimate_index (int)

    Returns:
        numpy.matrix: one row
    """
    directions = numpy.zeros(matrix_a.shape[1])
    directions[min_neg_estimate_index] = 1

    matrix_h = construct_matrix_h(matrix_a, matrix_d, extended_support)
    vec_bb = construct_vec_bb(matrix_a, matrix_d, extended_support,
                              min_neg_estimate_index)
    vec_l_delta_y = (-matrix_h.I * vec_bb.T).A1
    for i, index in enumerate(extended_support):
        directions[index] = vec_l_delta_y[i]

    return numpy.matrix(directions)


def calc_steps(plan,
               matrix_d,
               vec_of_estimates,
               extended_support,
               vec_of_directions,
               min_neg_estimate_index):
    """Calculate steps (vector theta).

    Args:
        plan (numpy.matrix): one row
        matrix_d (numpy.matrix)
        vec_of_estimates (numpy.matrix): one row
        extended_support (list)
        vec_of_directions (numpy.matrix): one row
        min_neg_estimate_index (float)

    Returns:
        list of tuples (extended support index, step)
    """
    result = []
    for index in extended_support:
        if vec_of_directions[0, index] >= 0:
            step = math.inf
        else:
            step = -plan[0, index] / vec_of_directions[0, index]

        result.append((index, step))

    delta = (vec_of_directions * matrix_d * vec_of_directions.T)[0, 0]
    if delta == 0:
        step_delta = math.inf
    else:
        step_delta = abs(vec_of_estimates[0, min_neg_estimate_index]) / delta

    result.append((min_neg_estimate_index, step_delta))

    return result


def iteration(plan,
              vec_c,
              matrix_a,
              matrix_d,
              support_vec_plan,
              extended_support,
              support_of_limitations,
              support_inverse_matrix_a):
    vec_c_from_support_plan = calc_vec_c_from_support_plan(vec_c, matrix_d,
                                                           support_vec_plan)
    vec_of_potentials = calc_vec_of_potentials(vec_c_from_support_plan,
                                               support_of_limitations,
                                               support_inverse_matrix_a)
    vec_of_estimates = calc_vec_of_estimates(vec_of_potentials, matrix_a,
                                             vec_c_from_support_plan)

    nonbase_indexes = list(set(range(matrix_a.shape[1]))
                           - set(support_of_limitations))
    # Bland's rule, to prevent cycling (1st rule)
    min_neg_estimate_index, min_estimate = None, None
    for nonbase_index in nonbase_indexes:
        estimate = vec_of_estimates[0, nonbase_index]
        if estimate < 0:
            min_neg_estimate_index = nonbase_index
            min_estimate = estimate
            break

    if min_neg_estimate_index is None:
        raise StopIteration

    vec_of_directions = calc_directions(matrix_a, matrix_d, extended_support,
                                        min_neg_estimate_index)
    steps = calc_steps(plan, matrix_d, vec_of_estimates, extended_support,
                       vec_of_directions, min_neg_estimate_index)
    min_step_extended_support_index, min_step = min(steps, key=operator.itemgetter(1))

def solve_problem():
    pass


def main():
    logger.setLevel(logging.INFO)


if __name__ == "__main__":
    sys.exit(main())
