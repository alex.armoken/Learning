import setuptools

package = "optimization_algos"
version = "0.1"

setuptools.setup(
    name=package,
    license="MIT",
    version=version,
    description="Some of optimization algorithms from BSUIR course",
    author="Alexander Vaschilko",
    author_email="alexander.vaschilko@gmail.com",
    url="https://github.com/Armoken/Learning/Optimization"
)
