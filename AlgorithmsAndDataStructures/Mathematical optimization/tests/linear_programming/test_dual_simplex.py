import os
import sys
import math

import numpy
import pytest

sys.path.append(
    os.path.abspath(
        os.path.join(
            os.path.dirname(__file__), '../../src'
        )
    )
)  # noqa
import optimization.simplex as simplex

RESOURCE_COST_OF_PRODUCTS_1 = [
    [-2, -1, 1, -7, 0, 0, 0, 2],
    [4, 2, 1, 0, 1, 5, -1, -5],
    [1, 1, 0, -1, 0, 3, -1, 1],
]

COUNT_OF_RESOURCE_TYPES = len(RESOURCE_COST_OF_PRODUCTS_1)
INVERSE_BASE_MATRIX_1_1 = numpy.matrix([
    [-1, 0, 0],
    [1, 1, -1],
    [-1, 0, -1]
], dtype=numpy.float)
INVERSE_BASE_MATRIX_1_2 = numpy.matrix([
    [-1.5, -0.5, 0.5],
    [-0.25, -0.25, 0.25],
    [-1.75, -0.75, -0.25]
], dtype=numpy.float)
PRODUCT_PROFITS_1 = [2, 2, 1, -10, 1, 4, -2, -3]
RESOURCE_LIMITS_1 = [-2, 4, 3]
PRODUCTS_COUNT_1 = len(PRODUCT_PROFITS_1)

DUAL_BASE_PLAN_1_1 = [1, 1, 1]
DUAL_BASE_PLAN_1_2 = [1.25, 1.25, 0.75]
DUAL_BASE_PLAN_1_3 = [0.6667, 1, 0.6667]

BASE_INDEXES_1_1 = [1, 4, 6]
BASE_INDEXES_1_2 = [1, 7, 6]
BASE_INDEXES_1_3 = [1, 7, 4]
NONBASE_INDEXES_1_1 = list(set(range(PRODUCTS_COUNT_1)) - set(BASE_INDEXES_1_1))
NONBASE_INDEXES_1_2 = list(set(range(PRODUCTS_COUNT_1)) - set(BASE_INDEXES_1_2))

VEC_DELTA_1_1 = numpy.matrix([1, 0, 1, 2, 0, 4, 0, 1],
                             dtype=numpy.float)
VEC_DELTA_1_2 = numpy.matrix([1.25, 0, 1.5, 0.5, 0.25, 4.5, 0, 0],
                             dtype=numpy.float)
BASE_CHI_VECTOR_1 = numpy.matrix([2, -1, -1],
                                 dtype=numpy.float)
VEC_MU_1 = numpy.matrix([1, 0, 2, -6, 1, 2, 0, -4], dtype=numpy.float)
SIGMA_VEC_1 = [math.inf, math.inf, 0.33333333333333331, math.inf, 0.25]


@pytest.fixture(scope="function", params=[
    (
        numpy.matrix(RESOURCE_LIMITS_1, dtype=numpy.float),
        INVERSE_BASE_MATRIX_1_1,
        BASE_CHI_VECTOR_1,  # Desired delta vector
    )
])
def base_chi_vec_calc_params(request):
    return request.param


def test_base_chi_vec_cal(base_chi_vec_calc_params):
    *arguments, desired_base_chi_vec = base_chi_vec_calc_params
    base_chi_vec = simplex.calc_base_chi_vec(*arguments)
    assert(numpy.allclose(base_chi_vec, desired_base_chi_vec, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        numpy.matrix(DUAL_BASE_PLAN_1_1, dtype=numpy.float),
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1, dtype=numpy.float),
        numpy.matrix(PRODUCT_PROFITS_1, dtype=numpy.float),
        VEC_DELTA_1_1,  # Desired result
    )
])
def delta_vec_calc_params(request):
    return request.param


def test_delta_vec_cal(delta_vec_calc_params):
    *arguments, desired_delta_vec = delta_vec_calc_params
    delta_vec = simplex.calc_delta_vec(*arguments)
    assert(numpy.allclose(delta_vec, desired_delta_vec, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1, dtype=numpy.float),
        INVERSE_BASE_MATRIX_1_1,
        1,         # K
        VEC_MU_1,  # Desired result
    ),
])
def vec_mu_calc_params(request):
    return request.param


def test_vec_mu_cal(vec_mu_calc_params):
    *arguments, desired_vec_mu = vec_mu_calc_params
    vec_mu = simplex.calc_vec_mu(*arguments)
    assert(numpy.allclose(vec_mu, desired_vec_mu, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        VEC_DELTA_1_1,
        VEC_MU_1,
        NONBASE_INDEXES_1_1,
        SIGMA_VEC_1,  # Desired result
    ),
])
def sigma_vec_calc_params(request):
    return request.param


def test_sigma_vec_cal(sigma_vec_calc_params):
    *arguments, desired_sigma_vec = sigma_vec_calc_params
    sigma_vec = simplex.calc_sigma_vec(*arguments)
    assert(sigma_vec == desired_sigma_vec)


@pytest.fixture(scope="function", params=[
    (
        numpy.matrix(DUAL_BASE_PLAN_1_1, dtype=numpy.float),
        1,                                                    # K
        INVERSE_BASE_MATRIX_1_1,
        0.25,                                                 # Min sigma
        COUNT_OF_RESOURCE_TYPES,
        numpy.matrix(DUAL_BASE_PLAN_1_2, dtype=numpy.float),  # Desired result
    ),
])
def calc_new_dual_base_plan_params(request):
    return request.param


def test_calc_new_dual_base_plan(calc_new_dual_base_plan_params):
    *arguments, desired_result = calc_new_dual_base_plan_params
    result = simplex.calc_new_dual_base_plan(*arguments)
    assert(numpy.allclose(result, desired_result, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        VEC_DELTA_1_1,
        0.25,           # Min sigma
        VEC_MU_1,
        VEC_DELTA_1_2,  # Desired result
    ),
])
def calc_new_vec_delta_params(request):
    return request.param


def test_calc_new_vec_delta(calc_new_vec_delta_params):
    *arguments, desired_vec_delta = calc_new_vec_delta_params
    vec_delta = simplex.calc_new_vec_delta(*arguments)
    assert(numpy.allclose(vec_delta, desired_vec_delta, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        BASE_INDEXES_1_1,
        1,                 # K
        7,                 # New index
        BASE_INDEXES_1_2,  # Desired result
    ),
])
def calc_new_base_indexes_params(request):
    return request.param


def test_calc_new_base_indexes(calc_new_base_indexes_params):
    *arguments, desired_base_indexes = calc_new_base_indexes_params
    base_indexes = simplex.calc_new_base_indexes(*arguments)
    assert(desired_base_indexes == base_indexes)


@pytest.fixture(scope="function", params=[
    (
        VEC_MU_1,
        7,                        # New base index
        1,                        # K
        BASE_INDEXES_1_1,
        INVERSE_BASE_MATRIX_1_1,
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1, dtype=numpy.float),
        INVERSE_BASE_MATRIX_1_2,  # Desired result
    ),
])
def new_inverse_base_matrix_calc_params(request):
    return request.param


def test_new_inverse_base_matrix_calc(new_inverse_base_matrix_calc_params):
    *arguments, desired_result = new_inverse_base_matrix_calc_params
    result = simplex.calc_new_inverse_base_matrix(*arguments)
    assert(numpy.allclose(result.A1, desired_result.A1))


@pytest.fixture(scope="function", params=[
    (
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1, dtype=numpy.float),
        numpy.matrix(PRODUCT_PROFITS_1, dtype=numpy.float),
        numpy.matrix(RESOURCE_LIMITS_1, dtype=numpy.float),
        INVERSE_BASE_MATRIX_1_1,
        VEC_DELTA_1_1,
        numpy.matrix(DUAL_BASE_PLAN_1_1, dtype=numpy.float),
        BASE_INDEXES_1_1,
        # Desired result
        (INVERSE_BASE_MATRIX_1_2,
         VEC_DELTA_1_2,
         numpy.matrix(DUAL_BASE_PLAN_1_2, dtype=numpy.float),
         BASE_INDEXES_1_2),
    ),
])
def dual_simplex_iteration_params(request):
    return request.param


def test_dual_simplex_iteration(dual_simplex_iteration_params):
    *arguments, desired_result = dual_simplex_iteration_params
    result = simplex.dual_iteration(*arguments)
    assert(numpy.allclose(result[0].A1, desired_result[0].A1))
    assert(numpy.allclose(result[1], desired_result[1]))
    assert(numpy.allclose(result[2], desired_result[2]))
    assert(result[3] == desired_result[3])


@pytest.fixture(scope="function", params=[
    (
        RESOURCE_COST_OF_PRODUCTS_1,
        PRODUCT_PROFITS_1,
        RESOURCE_LIMITS_1,
        DUAL_BASE_PLAN_1_1,
        BASE_INDEXES_1_1,
        # Desired new vec delta and new base indexes
        (DUAL_BASE_PLAN_1_3, BASE_INDEXES_1_3),
    ),
])
def dual_simplex_algorithm_params(request):
    return request.param


def test_dual_simplex_algorithm(dual_simplex_algorithm_params):
    *arguments, desired_result = dual_simplex_algorithm_params
    result = simplex.dual_algorithm(*arguments)
    assert(numpy.allclose(result[0], desired_result[0], atol=1e-04))
    assert(result[1] == desired_result[1])
