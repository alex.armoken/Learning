import os
import sys
import math

import numpy
import pytest

sys.path.append(
    os.path.abspath(
        os.path.join(
            os.path.dirname(__file__), '../../src'
        )
    )
) # noqa
import optimization.simplex as simplex

RESOURCE_COST_OF_PRODUCTS_1 = [
    [0, 1, 4, 1, 0, -3, 5, 0],
    [1, -1, 0, 1, 0, 0, 1, 0],
    [0, 7, -1, 0, -1, 3, 8, 0],
    [1, 1, 1, 1, 0, 3, -3, 1],
]

PRODUCT_PROFITS_1 = [-5, -2, 3, -4, -6, 0, -1, -5]
COUNT_OF_PRODUCTS_1 = len(PRODUCT_PROFITS_1)
ALL_INDEXES_1 = list(range(COUNT_OF_PRODUCTS_1))

BASE_INDEXES_1_1 = [0, 3, 4, 7]
BASE_INDEXES_1_2 = [0, 2, 4, 7]
BASE_INDEXES_1_3 = [0, 2, 4, 5]

NONBASE_INDEXES_1_1 = list(set(ALL_INDEXES_1) - set(BASE_INDEXES_1_1))
NONBASE_INDEXES_1_2 = list(set(ALL_INDEXES_1) - set(BASE_INDEXES_1_2))
NONBASE_INDEXES_1_3 = list(set(ALL_INDEXES_1) - set(BASE_INDEXES_1_3))

BASE_MATRIX_1_1 = numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1)[:, BASE_INDEXES_1_1]
BASE_MATRIX_1_2 = numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1)[:, BASE_INDEXES_1_2]
BASE_MATRIX_1_3 = numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1)[:, BASE_INDEXES_1_3]

INVERSE_BASE_MATRIX_1_1 = numpy.linalg.inv(BASE_MATRIX_1_1)
INVERSE_BASE_MATRIX_1_2 = numpy.linalg.inv(BASE_MATRIX_1_2)
INVERSE_BASE_MATRIX_1_3 = numpy.linalg.inv(BASE_MATRIX_1_3)

POTENTIAL_VECTOR_1_1 = numpy.matrix([1, 0, 6, -5],
                                    dtype=numpy.float)
POTENTIAL_VECTOR_1_2 = numpy.matrix([3.5, 0, 6, -5],
                                    dtype=numpy.float)

VECTOR_OF_ESTIMATES_1_1 = [(1, 40), (2, -10), (5, 0), (6, 69)]
VECTOR_OF_ESTIMATES_1_2 = [(1, 42.5), (3, 2.5), (5, -7.5), (6, 81.5)]

VECTOR_Z_1_1 = numpy.matrix([-4, 4, 1, 1], dtype=numpy.float)
VECTOR_Z_1_2 = numpy.matrix([0, -0.75, -2.25, 3.75], dtype=numpy.float)

BASE_PLAN_1_1 = [4, 0, 0, 6, 2, 0, 0, 5]
BASE_PLAN_1_2 = [10, 0, 1.5, 0, 0.5, 0, 0, 3.5]
BASE_PLAN_1_3 = [10, 0, 2.199975, 0, 2.599925, 0.9333, 0, 0]

VECTOR_THETA_1_1 = numpy.matrix([math.inf, 1.5, 2, 5], dtype=numpy.float)
VECTOR_THETA_1_2 = numpy.matrix([math.inf, math.inf, math.inf, 0.93333333333],
                                dtype=numpy.float)


RESOURCE_COST_OF_PRODUCTS_2 = [
    [0, 1, 4, 1, 0, -3, 1, 0],
    [1, -1, 0, 1, 0, 0, 0, 0],
    [0, 7, -1, 0, -1, 3, -1, 0],
    [1, 1, 1, 1, 0, 3, -1, 1]
]

PRODUCT_PROFITS_2 = [-5, -2, 3, -4, -6, 0, 1, -5]

COUNT_OF_PRODUCTS_2 = len(PRODUCT_PROFITS_2)
ALL_INDEXES_2 = list(range(COUNT_OF_PRODUCTS_2))

BASE_INDEXES_2_1 = [0, 2, 4, 7]
BASE_INDEXES_2_2 = [0, 2, 4, 5]

NONBASE_INDEXES_2_1 = list(set(ALL_INDEXES_2) - set(BASE_INDEXES_2_1))
NONBASE_INDEXES_2_2 = list(set(ALL_INDEXES_2) - set(BASE_INDEXES_2_2))

BASE_MATRIX_2_1 = numpy.matrix(RESOURCE_COST_OF_PRODUCTS_2)[:, BASE_INDEXES_2_1]
BASE_MATRIX_2_2 = numpy.matrix(RESOURCE_COST_OF_PRODUCTS_2)[:, BASE_INDEXES_2_2]

INVERSE_BASE_MATRIX_2_1 = numpy.linalg.inv(BASE_MATRIX_2_1)
INVERSE_BASE_MATRIX_2_2 = numpy.linalg.inv(BASE_MATRIX_2_2)

POTENTIAL_VECTOR_2_1 = numpy.matrix([3.5, 0, 6, -5], dtype=numpy.float)

VECTOR_OF_ESTIMATES_2_1 = [(1, 42.5), (3, 2.5), (5, -7.5), (6, 1.5)]

VECTOR_Z_2_1 = numpy.matrix([0, -0.75, -2.25, 3.75], dtype=numpy.float)

BASE_PLAN_2_1 = [10, 0, 1.5, 0, 0.5, 0, 0, 3.5]
BASE_PLAN_2_2 = [10, 0, 2.199975, 0, 2.599925, 0.9333, 0, 0]

VECTOR_THETA_2_1 = numpy.matrix([math.inf, math.inf, math.inf, 0.933333333333],
                                dtype=numpy.float)


@pytest.fixture(scope="function", params=[
    (
        numpy.matrix(PRODUCT_PROFITS_1)[0, BASE_INDEXES_1_1],
        INVERSE_BASE_MATRIX_1_1,
        POTENTIAL_VECTOR_1_1,  # Desired result
    ),
    (
        numpy.matrix(PRODUCT_PROFITS_1)[0, BASE_INDEXES_1_2],
        INVERSE_BASE_MATRIX_1_2,
        POTENTIAL_VECTOR_1_2
    ),
    (
        numpy.matrix(PRODUCT_PROFITS_2)[0, BASE_INDEXES_2_1],
        INVERSE_BASE_MATRIX_2_1,
        POTENTIAL_VECTOR_2_1
    ),
])
def vector_potential_calculation_params(request):
    return request.param


def test_potential_vector_calculation(vector_potential_calculation_params):
    *args, desired_potential_vector = vector_potential_calculation_params
    potential_vector = simplex.calculate_potential_vector(*args)
    assert((potential_vector == desired_potential_vector).all())


@pytest.fixture(scope="function", params=[
    (
        POTENTIAL_VECTOR_1_1,
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1, dtype=numpy.float),
        numpy.matrix(PRODUCT_PROFITS_1, dtype=numpy.float),
        NONBASE_INDEXES_1_1,
        VECTOR_OF_ESTIMATES_1_1,  # Desired result
    ),
    (
        POTENTIAL_VECTOR_1_2,
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1, dtype=numpy.float),
        numpy.matrix(PRODUCT_PROFITS_1, dtype=float),
        NONBASE_INDEXES_1_2,
        VECTOR_OF_ESTIMATES_1_2,
    ),
    (
        POTENTIAL_VECTOR_2_1,
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_2, dtype=numpy.float),
        numpy.matrix(PRODUCT_PROFITS_2, dtype=float),
        NONBASE_INDEXES_2_1,
        VECTOR_OF_ESTIMATES_2_1,
    ),
])
def vector_of_estimates_calc_params(request):
    return request.param


def test_vector_vector_of_estimates_calc(vector_of_estimates_calc_params):
    *args, desired_vector_of_estimates = vector_of_estimates_calc_params
    vector_of_estimates = simplex.calculate_vector_of_estimates(*args)
    assert(vector_of_estimates == desired_vector_of_estimates)


@pytest.fixture(scope="function", params=[
    (
        INVERSE_BASE_MATRIX_1_1,
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1, dtype=numpy.float),
        2,              # Min estimate index
        VECTOR_Z_1_1    # Desired result
    ),
    (
        INVERSE_BASE_MATRIX_1_2,
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1, dtype=numpy.float),
        5,
        VECTOR_Z_1_2
    ),
    (
        INVERSE_BASE_MATRIX_2_1,
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_2, dtype=numpy.float),
        5,
        VECTOR_Z_2_1
    ),
])
def vector_z_calculation_params(request):
    return request.param


def test_vector_z_calculation(vector_z_calculation_params):
    *args, desired_vector_z = vector_z_calculation_params
    vector_z = simplex.calculate_vector_z(*args)
    assert(numpy.allclose(vector_z.A1, desired_vector_z.A1))


@pytest.fixture(scope="function", params=[
    (
        numpy.matrix(BASE_PLAN_1_1, dtype=numpy.float),
        BASE_INDEXES_1_1,
        VECTOR_Z_1_1,
        VECTOR_THETA_1_1  # Desired result
    ),
    (
        numpy.matrix(BASE_PLAN_1_2, dtype=numpy.float),
        BASE_INDEXES_1_2,
        VECTOR_Z_1_2,
        VECTOR_THETA_1_2
    ),
    (
        numpy.matrix(BASE_PLAN_2_1, dtype=numpy.float),
        BASE_INDEXES_2_1,
        VECTOR_Z_2_1,
        VECTOR_THETA_2_1
    ),
])
def vector_theta_calculation_params(request):
    return request.param


def test_vector_theta_calculation(vector_theta_calculation_params):
    *args, desired_vector_theta = vector_theta_calculation_params
    vector_theta = simplex.calculate_vector_theta(*args)
    assert(numpy.allclose(vector_theta.A1, desired_vector_theta.A1))


@pytest.fixture(scope="function", params=[
    (
        numpy.matrix(BASE_PLAN_1_1, dtype=numpy.float),
        BASE_INDEXES_1_1,
        2,                            # New plan index
        1,                            # S
        1.5,                          # Min theta el
        VECTOR_Z_1_1,
        numpy.matrix(BASE_PLAN_1_2),  # Desired result
        BASE_INDEXES_1_2              # Desired result
    ),
    (
        numpy.matrix(BASE_PLAN_1_2, dtype=numpy.float),
        BASE_INDEXES_1_2,
        5,
        3,
        0.9333,
        VECTOR_Z_1_2,
        numpy.matrix(BASE_PLAN_1_3),
        BASE_INDEXES_1_3
    ),
    (
        numpy.matrix(BASE_PLAN_2_1, dtype=numpy.float),
        BASE_INDEXES_2_1,
        5,
        3,
        0.9333,
        VECTOR_Z_2_1,
        numpy.matrix(BASE_PLAN_2_2),
        BASE_INDEXES_2_2
    )
])
def moving_to_a_new_plan_params(request):
    return request.param


def test_moving_to_a_new_base_plan(moving_to_a_new_plan_params):
    *args, desired_plan, desired_indexes = moving_to_a_new_plan_params
    new_plan, new_indexes = simplex.move_to_a_new_base_plan(*args)
    assert(numpy.allclose(new_plan, desired_plan, atol=1e-05))
    assert(new_indexes == desired_indexes)


@pytest.fixture(scope="function", params=[
    (
        VECTOR_Z_1_1,
        INVERSE_BASE_MATRIX_1_1,
        1,                       # S
        INVERSE_BASE_MATRIX_1_2  # Desired inverse base matrix
    ),
    (
        VECTOR_Z_1_2,
        INVERSE_BASE_MATRIX_1_2,
        3,
        INVERSE_BASE_MATRIX_1_3
    ),
    (
        VECTOR_Z_2_1,
        INVERSE_BASE_MATRIX_2_1,
        3,
        INVERSE_BASE_MATRIX_2_2
    ),
])
def new_inverse_base_matrix_calculcation_params(request):
    return request.param


def test_new_inverse_base_matrix_calculcation(
        new_inverse_base_matrix_calculcation_params):
    *args, desired_result = new_inverse_base_matrix_calculcation_params
    inverse_base_matrix = simplex.calculate_new_inverse_base_matrix(*args)
    assert(numpy.allclose(inverse_base_matrix, desired_result, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1, dtype=numpy.float),
        numpy.matrix(PRODUCT_PROFITS_1, dtype=numpy.float),
        INVERSE_BASE_MATRIX_1_1,
        numpy.matrix(BASE_PLAN_1_1, dtype=numpy.float),
        BASE_INDEXES_1_1,
        numpy.matrix(BASE_PLAN_1_2, dtype=numpy.float),  # Desired results
        BASE_INDEXES_1_2,
        INVERSE_BASE_MATRIX_1_2
    ),
    (
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_1, dtype=numpy.float),
        numpy.matrix(PRODUCT_PROFITS_1, dtype=numpy.float),
        INVERSE_BASE_MATRIX_1_2,
        numpy.matrix(BASE_PLAN_1_2, dtype=numpy.float),
        BASE_INDEXES_1_2,
        numpy.matrix(BASE_PLAN_1_3, dtype=numpy.float),
        BASE_INDEXES_1_3,
        INVERSE_BASE_MATRIX_1_3
    ),
    (
        numpy.matrix(RESOURCE_COST_OF_PRODUCTS_2, dtype=numpy.float),
        numpy.matrix(PRODUCT_PROFITS_2, dtype=numpy.float),
        INVERSE_BASE_MATRIX_2_1,
        numpy.matrix(BASE_PLAN_2_1, dtype=numpy.float),
        BASE_INDEXES_2_1,
        numpy.matrix(BASE_PLAN_2_2, dtype=numpy.float),
        BASE_INDEXES_2_2,
        INVERSE_BASE_MATRIX_2_2
    ),
])
def iteration_params(request):
    return request.param


def test_iteration(iteration_params):
    args = iteration_params[0: -3]
    desired_plan = iteration_params[-3]
    desired_indexes = iteration_params[-2]
    desired_inverse_base_matrix = iteration_params[-1]
    inverse_base_matrix, new_plan, new_indexes = simplex.iteration(*args)

    assert(numpy.allclose(new_plan,
                          desired_plan,
                          atol=1e-03))
    assert(numpy.allclose(inverse_base_matrix,
                          desired_inverse_base_matrix,
                          atol=1e-03))
    assert(new_indexes == desired_indexes)


@pytest.fixture(scope="function", params=[
    (
        RESOURCE_COST_OF_PRODUCTS_1,
        PRODUCT_PROFITS_1,
        BASE_PLAN_1_1,
        BASE_INDEXES_1_1,
        BASE_PLAN_1_3,
        BASE_INDEXES_1_3,
    ),
    (
        RESOURCE_COST_OF_PRODUCTS_2,
        PRODUCT_PROFITS_2,
        BASE_PLAN_2_1,
        BASE_INDEXES_2_1,
        None,
        None
    ),
])
def algorithm_params(request):
    return request.param


def test_algorithm(algorithm_params):
    *args, desired_base_plan, desired_base_indexes = algorithm_params
    new_base_plan, new_base_indexes = simplex.algorithm(*args)

    if (desired_base_plan is None):
        assert(new_base_plan is None)
        assert(new_base_indexes is None)
    else:
        assert(numpy.allclose(new_base_plan, desired_base_plan,
                              atol=1e-03))
        assert(new_base_indexes == desired_base_indexes)
