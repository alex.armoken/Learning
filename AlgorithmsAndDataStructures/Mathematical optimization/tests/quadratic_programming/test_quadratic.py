import os
import sys
import math

import numpy
import pytest

sys.path.append(
    os.path.abspath(
        os.path.join(
            os.path.dirname(__file__), '../../src'
        )
    )
) # noqa
import optimization.quadratic_programming as quadratic_programming

MATRIX_D_1 = numpy.matrix([
    [6, 11, -1, 0, 6, -7, -3, -2],
    [11, 41, -1, 0, 7, -24, 0, -3],
    [-1, -1, 1, 0, -3, -4, 2, -1],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [6, 7, -3, 0, 11, 6, -7, 1],
    [-7, -24, -4, 0, 6, 42, -7, 10],
    [-3, 0, 2, 0, -7, -7, 5, -1],
    [-2, -3, -1, 0, 1, 10, -1, 3]
], dtype=numpy.float)
MATRIX_A_1 = numpy.matrix([
    [1, 2, 0, 1, 0, 4, -1, -3],
    [1, 3, 0, 0, 1, -1, -1, 2],
    [1, 4, 1, 0, 0, 2, -2, 0],
], dtype=numpy.float)
VEC_B_1 = numpy.matrix([4, 5, 6], dtype=numpy.float)
VEC_D_1 = numpy.matrix([7, 3, 3], dtype=numpy.float)
VEC_C_1 = numpy.matrix([-10, -31, 7, 0, -21, -16, 11, -7], dtype=numpy.float)

MATRIX_H_1_1 = numpy.matrix([
    [1, 0, -3, 0, 0, 1],
    [0, 0, 0, 1, 0, 0],
    [-3, 0, 11, 0, 1, 0],
    [0, 1, 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0],
    [1, 0, 0, 0, 0, 0]
], dtype=numpy.float)

PLAN_1_1 = numpy.matrix([0, 0, 6, 4, 5, 0, 0, 0], dtype=numpy.float)
SUPPORT_OF_LIMITATIONS_1_1 = [2, 3, 4]
EXTENDED_SUPPORT_1_1 = [2, 3, 4]

SUPPORT_INVERSE_MATRIX_A_1_1 = MATRIX_A_1[:, SUPPORT_OF_LIMITATIONS_1_1].I

VEC_C_FROM_SUPPORT_PLAN_1_1 = numpy.matrix([14, -2, -2, 0, 16, -10, -12, -8],
                                           dtype=numpy.float)

VEC_OF_POTENTIALS_1_1 = numpy.matrix([0, -16, 2], dtype=numpy.float)

VEC_OF_ESTIMATES_1_1 = numpy.matrix([0, -42, 0, 0, 0, 10, 0, -40],
                                    dtype=numpy.float)

VEC_BB_1_1 = numpy.matrix([-1, 0, 7, 2, 3, 4], dtype=numpy.float)
VEC_OF_DIRECTIONS_1_1 = numpy.matrix([0, 1, -4, -2, -3, 0, 0, 0],
                                     dtype=numpy.float)

VEC_OF_STEPS_1_1 = [(2, 1.5), (3, 2), (4, 1.6666666666666667), (1, 0.84)]


@pytest.fixture(scope="function", params=[
    (
        VEC_C_1,
        MATRIX_D_1,
        PLAN_1_1,
        VEC_C_FROM_SUPPORT_PLAN_1_1,
    ),
])
def vec_c_from_support_plan_calc_params(request):
    return request.param


def test_vec_c_from_support_plan_calc(vec_c_from_support_plan_calc_params):
    *args, desired_result = vec_c_from_support_plan_calc_params
    result = quadratic_programming.calc_vec_c_from_support_plan(*args)
    assert(numpy.allclose(desired_result.A1, result.A1, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        VEC_C_FROM_SUPPORT_PLAN_1_1,
        SUPPORT_OF_LIMITATIONS_1_1,
        SUPPORT_INVERSE_MATRIX_A_1_1,
        VEC_OF_POTENTIALS_1_1,
    ),
])
def vec_of_potentials_calc_params(request):
    return request.param


def test_vec_of_potentials_calc(vec_of_potentials_calc_params):
    *args, desired_result = vec_of_potentials_calc_params
    result = quadratic_programming.calc_vec_of_potentials(*args)
    assert(numpy.allclose(desired_result.A1, result.A1, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        VEC_OF_POTENTIALS_1_1,
        MATRIX_A_1,
        VEC_C_FROM_SUPPORT_PLAN_1_1,
        VEC_OF_ESTIMATES_1_1,
    ),
])
def vec_of_estimates_calc_params(request):
    return request.param


def test_vec_of_estimates_calc(vec_of_estimates_calc_params):
    *args, desired_result = vec_of_estimates_calc_params
    result = quadratic_programming.calc_vec_of_estimates(*args)
    assert(numpy.allclose(desired_result.A1, result.A1, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        MATRIX_A_1,
        MATRIX_D_1,
        EXTENDED_SUPPORT_1_1,
        MATRIX_H_1_1,
    ),
])
def matrix_h_construction_params(request):
    return request.param


def test_matrix_h_construction(matrix_h_construction_params):
    *args, desired_result = matrix_h_construction_params
    result = quadratic_programming.construct_matrix_h(*args)
    assert(numpy.allclose(desired_result.A1, result.A1, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        MATRIX_A_1,
        MATRIX_D_1,
        EXTENDED_SUPPORT_1_1,
        1,
        VEC_BB_1_1,
    ),
])
def vec_bb_construction_params(request):
    return request.param


def test_vec_bb_construction(vec_bb_construction_params):
    *args, desired_result = vec_bb_construction_params
    result = quadratic_programming.construct_vec_bb(*args)
    assert(numpy.allclose(desired_result.A1, result.A1, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        MATRIX_A_1,
        MATRIX_D_1,
        EXTENDED_SUPPORT_1_1,
        1,
        VEC_OF_DIRECTIONS_1_1,
    ),
])
def vec_of_directions_calc_params(request):
    return request.param


def test_vec_of_directions_calc(vec_of_directions_calc_params):
    *args, desired_result = vec_of_directions_calc_params
    result = quadratic_programming.calc_vec_of_directions(*args)
    assert(numpy.allclose(desired_result.A1, result.A1, atol=1e-05))


@pytest.fixture(scope="function", params=[
    (
        PLAN_1_1,
        MATRIX_D_1,
        VEC_OF_ESTIMATES_1_1,
        EXTENDED_SUPPORT_1_1,
        VEC_OF_DIRECTIONS_1_1,
        1,
        VEC_OF_STEPS_1_1,
    ),
])
def steps_calc_params(request):
    return request.param


def test_steps_calc(steps_calc_params):
    *args, desired_result = steps_calc_params
    result = quadratic_programming.calc_steps(*args)
    assert(numpy.allclose(desired_result, result, atol=1e-05))
