#include <iostream>
#include <vector>

#if !defined (A_HEAP)
#define A_HEAP

namespace alib
{
    namespace data_structures
    {
        template<typename Value, typename Comparator>
        class Heap
        {
        public:
            Heap() = default;
            Heap(std::vector<Value>&& data);
            Heap(const std::vector<Value>& data);

            ~Heap() = default;

            void merge(const Heap& heap);
            static Heap merge(Heap&& a, Heap&& b);
            static Heap merge(const Heap& a, const Heap& b);

            void add(Value&& val);

            void deleteTopElement();

            const Value& getTopElement() const;

            std::size_t getSize() const;

        private:
            std::vector<Value> _data;

            void _siftUp(int index);
            void _siftDown(int index);
        };


        template<typename Value, typename Comparator>
        Heap<Value, Comparator>::Heap(std::vector<Value>&& data)
        {
        }


        template<typename Value, typename Comparator>
        Heap<Value, Comparator>::Heap(const std::vector<Value>& data)
        {

        }


        template<typename Value, typename Comparator>
        void Heap<Value, Comparator>::merge(const Heap& heap)
        {

        }


        template<typename Value, typename Comparator>
        Heap<Value, Comparator> Heap<Value, Comparator>::merge(Heap&& a, Heap&& b)
        {

        }


        template<typename Value, typename Comparator>
        Heap<Value, Comparator> Heap<Value, Comparator>::merge(const Heap& a, const Heap& b)
        {

        }


        template<typename Value, typename Comparator>
        void Heap<Value, Comparator>::add(Value&& val)
        {

        }


        template<typename Value, typename Comparator>
        void Heap<Value, Comparator>::deleteTopElement()
        {

        }


        template<typename Value, typename Comparator>
        const Value& Heap<Value, Comparator>::getTopElement() const
        {

        }


        template<typename Value, typename Comparator>
        std::size_t Heap<Value, Comparator>::getSize() const
        {

        }


        template<typename Value, typename Comparator>
        void  Heap<Value, Comparator>::_siftUp(int index)
        {
        }


        template<typename Value, typename Comparator>
        void Heap<Value, Comparator>::_siftDown(int index)
        {

        }
    }
}

#endif
