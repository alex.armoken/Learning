# FindRapidCSV.cmake
#
# Finds the rapidjson library
#
# This will define the following variables
#
#    RapidCSV_FOUND
#    RapidCSV_HEADER
#
# and the following imported targets
#
#     RapidCSV::RapidCSV
#
# Author: Alexander Vaschilka - alex.armoken@gmail.com

FIND_PACKAGE(PkgConfig)

FIND_PATH(RapidCSV_HEADER
    NAMES rapidcsv.h
    PATHS "/usr/include/"
)
IF(NOT RapidCSV_HEADER-NOTFOUND)
    SET(RapidCSV_FOUND "TRUE")
ENDIF()

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(RapidCSV
    REQUIRED_VARS RapidCSV_HEADER
)

MARK_AS_ADVANCED(RapidCSV_HEADER RapidCSV_FOUND)

IF(RapidCSV_FOUND AND NOT TARGET RapidCSV::RapidCSV)
    ADD_LIBRARY(RapidCSV::RapidCSV INTERFACE IMPORTED)
    SET_TARGET_PROPERTIES(RapidCSV::RapidCSV PROPERTIES
        INTERFACE_SOURCES "${RapidCSV_HEADER}"
    )
ENDIF()
