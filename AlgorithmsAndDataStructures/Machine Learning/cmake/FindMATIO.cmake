# FindMATIO
#
# Try to find MATIO library
#
# Once done this will define:
#
#  MATIO_FOUND - True if MATIO found.
#  MATIO_LIBRARIES - MATIO libraries.
#  MATIO_INCLUDE_DIRS - where to find matio.h, etc..
#  MATIO_VERSION_STRING - version number as a string (e.g.: "1.3.4")
#
# Provides the following imported target:
# Matio::matio
#

IF(NOT COMMAND feature_summary)
    INCLUDE(FeatureSummary)
ENDIF()

# Look for the header file.
FIND_PATH(MATIO_INCLUDE_DIR
    NAMES matio.h
    HINTS
      ${MATIO_ROOT}/include
      $ENV{MATIO_ROOT}/include
    DOC "The MATIO include directory"
)

# Look for the library.
FIND_LIBRARY(MATIO_LIBRARY
    NAMES matio
    HINTS
      ${MATIO_ROOT}/lib
      $ENV{MATIO_ROOT}/lib
      ${MATIO_ROOT}/lib64
      $ENV{MATIO_ROOT}/lib64
    DOC "The MATIO library"
)

IF(MATIO_INCLUDE_DIR)
    # ---------------------------------------------------
    #  Extract version information from MATIO
    # ---------------------------------------------------

    # If the file is missing, set all values to 0
    SET(MATIO_MAJOR_VERSION 0)
    SET(MATIO_MINOR_VERSION 0)
    SET(MATIO_RELEASE_LEVEL 0)

    # new versions of MATIO have `matio_pubconf.h`
    IF(EXISTS ${MATIO_INCLUDE_DIR}/matio_pubconf.h)
        SET(MATIO_CONFIG_FILE "matio_pubconf.h")
    ELSE()
        SET(MATIO_CONFIG_FILE "matioConfig.h")
    ENDIF()

    IF(MATIO_CONFIG_FILE)
        # Read and parse MATIO config header file for version number
        FILE(STRINGS
            "${MATIO_INCLUDE_DIR}/${MATIO_CONFIG_FILE}"
            _matio_HEADER_CONTENTS
            REGEX "#define MATIO_((MAJOR|MINOR)_VERSION)|(RELEASE_LEVEL) "
        )
        FOREACH(line ${_matio_HEADER_CONTENTS})
            IF(line MATCHES "#define ([A-Z_]+) ([0-9]+)")
                SET("${CMAKE_MATCH_1}" "${CMAKE_MATCH_2}")
            ENDIF()
        ENDFOREACH()

        UNSET(_matio_HEADER_CONTENTS)
    ENDIF()

    SET(MATIO_VERSION_STRING
        "${MATIO_MAJOR_VERSION}.${MATIO_MINOR_VERSION}.${MATIO_RELEASE_LEVEL}"
    )
ENDIF()

MARK_AS_ADVANCED(MATIO_INCLUDE_DIR MATIO_LIBRARY)

# handle the QUIETLY and REQUIRED arguments and set MATIO_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(MATIO REQUIRED_VARS
                                  MATIO_LIBRARY
                                  MATIO_INCLUDE_DIR
                                  VERSION_VAR
                                  MATIO_VERSION_STRING)

IF(MATIO_FOUND)
  SET(MATIO_LIBRARIES ${MATIO_LIBRARY})
  SET(MATIO_INCLUDE_DIRS ${MATIO_INCLUDE_DIR})
ELSE()
  SET(MATIO_LIBRARIES)
  SET(MATIO_INCLUDE_DIRS)
ENDIF()

IF(MATIO_FOUND AND MATIO_VERSION_STRING)
    SET_PACKAGE_PROPERTIES(MATIO PROPERTIES
        DESCRIPTION
        "MATLAB MAT File I/O Library (found: v${MATIO_VERSION_STRING})"
    )
ELSE()
    SET_PACKAGE_PROPERTIES(MATIO PROPERTIES
        DESCRIPTION
        "MATLAB MAT File I/O Library"
    )
ENDIF()

SET_PACKAGE_PROPERTIES(MATIO PROPERTIES
    URL "https://github.com/tbeu/matio"
)

IF(MATIO_FOUND AND NOT TARGET Matio::matio)
    ADD_LIBRARY(Matio::matio SHARED IMPORTED)
    SET_TARGET_PROPERTIES(Matio::matio PROPERTIES
        IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
        IMPORTED_LOCATION "${MATIO_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${MATIO_INCLUDE_DIR}"
        INTERFACE_LINK_LIBRARIES "${MATIO_LIBRARY}"
    )
ENDIF()
