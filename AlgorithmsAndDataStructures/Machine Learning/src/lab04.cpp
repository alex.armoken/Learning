#include <algorithm>
#include <array>
#include <cmath>
#include <filesystem>
#include <functional>
#include <functional>
#include <iostream>
#include <locale>
#include <numeric>
#include <optional>
#include <string>
#include <variant>

#include <boost/program_options.hpp>
#include <rapidcsv.h>
#include <tl/expected.hpp>

#include <TASImage.h>
#include <TApplication.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TF12.h>
#include <TF2.h>
#include <TGaxis.h>
#include <TGraph2D.h>
#include <TGraph2DErrors.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TROOT.h>
#include <TRandom.h>
#include <TStyle.h>

#include "ml/utils/cmd_args_helpers.hpp"
#include "ml/utils/color_helpers.hpp"
#include "ml/utils/enum_helpers.hpp"
#include "ml/utils/io_helpers.hpp"
#include "ml/utils/plot_helpers.hpp"
#include "ml/utils/polynom_elements_degrees.hpp"
#include "ml/utils/time_meter.hpp"

#include <ml/opdesc/grad_descent.hpp>
#include "ml/nn/neural_network.hpp"


using namespace ml::utils;
namespace boost_po = boost::program_options;

namespace lab4
{
    using namespace ROOT;
    using namespace ml::opdesc;
    using namespace ml::nn;
    using namespace ml::utils;
    using namespace ml::utils::activation_funcs;

    const auto INPUT_DIR_OPT  = "input_data_dir";
    const auto OUTPUT_DIR_OPT = "output_dir";
    const auto THETA_OPT  = "theta";
    const auto ALPHA_OPT  = "alpha";

    enum class ProgramReturnCode
    {
        FIRST                          = 0,
        ALL_IS_GOOD                    = 0,
        ERROR_DURING_ARGS_PARSING      = 1,
        ERROR_DURING_READING_FROM_FILE = 2,
        ERROR_DURING_OUTPUT_SAVING     = 3,
        LAST                           = 3
    };

    enum class ArgsLogicError
    {
        FIRST                   = 0,
        NO_INPUT_OR_OUTPUT_DIRS = 0,
        LAST                    = 0
    };
    const std::map<ArgsLogicError, std::string> ArgsLogicErrExplanationMap = {
        {
            ArgsLogicError::NO_INPUT_OR_OUTPUT_DIRS,
            "Use --input_data_dir and --output_dir options."
        }
    };

    static const std::map<ProgramReturnCode, std::string>
    ReturnValExplanationMap = {
        { ProgramReturnCode::ALL_IS_GOOD, "All is good" },
    };

    boost_po::options_description get_options_description()
    {
        boost_po::options_description opts_desc;

        opts_desc.add_options()
            (
                INPUT_DIR_OPT,
                boost_po::value<std::string>(),
                "Path to directory with input data"
            )
            (
                OUTPUT_DIR_OPT,
                boost_po::value<std::string>(),
                "Path to directory with output data"
            )
            (
                THETA_OPT,
                boost_po::value<std::vector<double>>()->multitoken(),
                "Theta coefficients"
            )
            (
                ALPHA_OPT,
                boost_po::value<double>(),
                "Alpha coefficient"
            );

        return opts_desc;
    }

    std::optional<ArgsLogicError> check_args_logic(
        const boost_po::variables_map& var_map
    )
    {
        const auto is_input_dir_setuped = var_map.count(INPUT_DIR_OPT) != 0;
        const auto is_output_dir_setuped = var_map.count(OUTPUT_DIR_OPT) != 0;

        if (is_input_dir_setuped && is_output_dir_setuped)
        {
            return std::nullopt;
        }

        return ArgsLogicError::NO_INPUT_OR_OUTPUT_DIRS;
    }

    void SetupRootSettings()
    {
        // Remove useless notifications
        // GetROOT()->ProcessLine("gErrorIgnoreLevel = kFatal;");

        gStyle->SetPalette(55);

        std::cout << std::fixed << std::setprecision(10);
    }

    const epsilon<double> DEFAULT_EPSILONE(1e-8);
    const auto SURF_POINTS_COUNT = 100000;

    template<typename T, std::size_t NInputVars>
    std::optional<FuncIOVariants<T, NInputVars>> load_images(
        const std::filesystem::path& path
    )
    {
        const auto maybe_reader = MatFileReader::load_file(path);
        if (not maybe_reader) {
            return std::nullopt;
        }
        auto maybe_variants
            = maybe_reader->read_variants<T, NInputVars>("X", "y");
        if (not maybe_variants) {
            return std::nullopt;
        }

        return FuncIOVariants<T, NInputVars>(std::move(*maybe_variants));
    }

    template<typename T, std::size_t NInputVars>
    std::optional<FuncIOVariants<T, NInputVars>> load_weights(
        const std::filesystem::path& path
    )
    {
        const auto maybe_reader = MatFileReader::load_file(path);
        if (not maybe_reader) {
            return std::nullopt;
        }
        auto maybe_variants
            = maybe_reader->read_variants<T, NInputVars>("X", "y");
        if (not maybe_variants) {
            return std::nullopt;
        }

        return FuncIOVariants<T, NInputVars>(std::move(*maybe_variants));
    }

    template<typename TO, typename FROM>
    std::unique_ptr<TO> static_unique_pointer_cast (std::unique_ptr<FROM>&& old)
    {
        return std::unique_ptr<TO>{static_cast<TO*>(old.release())};
    }

    template<typename T, std::size_t... NNConfiguration>
    std::tuple<uint8_t, double> get_digit_mark_from_image(
        const TVectorT<T>& image,
        const NeuralNetwork<T, NNConfiguration...>& nn
    )
    {
        TVectorT<double> posibilities = nn.forward_propagation(image);

        const auto max_el_iter = std::max_element(
            posibilities.GetMatrixArray(),
            posibilities.GetMatrixArray() + posibilities.GetNoElements()
        );
        const auto digit_mark = std::distance(
            posibilities.GetMatrixArray(),
            max_el_iter
        ) + 1;

        return { digit_mark, *max_el_iter };
    }

    template<typename T, std::size_t... NNConfiguration>
    double get_precision_of_digit_predition(
        const std::vector<TVectorT<T>>& images,
        const y_vec<T>& y,
        const NeuralNetwork<T, NNConfiguration...>& nn
    )
    {
        const auto images_count = images.size();
        std::vector<uint8_t> predicted_result;
        predicted_result.reserve(images_count);

        for (std::size_t i = 0; i < images_count; ++i) {
            const auto info = get_digit_mark_from_image(images[i], nn);
            const auto predicted_digit_mark = std::get<0>(info);
            predicted_result.push_back(predicted_digit_mark);
        }

        return calc_avg<double, bool>(
            compare_els<uint8_t>(
                transform_vec<double, uint8_t>(
                    y,
                    [] (const double el) { return static_cast<uint8_t>(el); }
                ),
                predicted_result
            )
        );
    }

    void labwork_1st_part(
        const std::filesystem::path& input_dir,
        const std::filesystem::path& out_dir
    )
    {
        print_text({"1. Загрузите данные ex4data1.mat из файла."}, true);
        const auto maybe_data_reader = MatFileReader::load_file(
            input_dir / "ex4data1.mat"
        );
        assert(maybe_data_reader);

        const auto maybe_images
            = maybe_data_reader->read_array_of_vectors<double>("X");
        const auto maybe_x
            = maybe_data_reader->read_matrix<double>("X", true);
        const auto maybe_x_vectors
            = maybe_data_reader->read_array_of_vectors<double>("X", true);
        const auto maybe_y = maybe_data_reader->read_vector<double>("y");

        const auto& images = *maybe_images;
        const auto& x_vectors = *maybe_x_vectors;
        const auto& x = *maybe_x;
        const auto& y = *maybe_y;
        std::cout << "Images loaded!" << "\n";


        print_text({
           "5. Перекодируйте исходные метки классов по схеме one-hot."
        }, true);
        const auto one_hot_encoded_y = one_hot_encode(y, 10);
        const auto one_hot_encoded_y_m = one_hot_encode_m(y, 10);
        std::cout << "Y vector successfully one-hot encoded!" << "\n";


        ///// Neural network configuration /////
        constexpr std::size_t FirstLayerThetas = 401;
        constexpr std::size_t FirstLayerNeurons = 25;

        constexpr std::size_t SecondLayerThetas = 26;
        constexpr std::size_t SecondLayerNeurons = 10;

        {
            print_text({
                "2. Загрузите веса нейронной сети из файла ex4weights.mat,",
                "   который содержит две матрицы Θ(1) (25, 401) и",
                "   Θ(2) (10, 26). Какова структура полученной нейронной",
                "   сети?"
            }, true);
            const auto maybe_weights_reader = MatFileReader::load_file(
                input_dir / "ex4weights.mat"
            );
            assert(maybe_weights_reader);

            const auto maybe_neural_network = ml::nn::NeuralNetwork<
                double,
                FirstLayerThetas,
                FirstLayerNeurons,
                SecondLayerThetas,
                SecondLayerNeurons
            >::create(*maybe_weights_reader,
                      {"Theta1", "Theta2"},
                      static_unique_pointer_cast<Activator<double>>(
                          std::make_unique<SigmoidActivator<double>>()
                      ));
            assert(maybe_neural_network);
            auto& neural_network = *maybe_neural_network;
            std::cout << "Neural network weights loaded!" << "\n";
            std::cout << "NOutputUnits: "
                      << maybe_neural_network->OUTPUT_UNITS_COUNT << "\n";


            print_text({
               "3. Реализуйте функцию прямого распространения с сигмоидом в",
               "   качестве функции активации.",
               "4. Вычислите процент правильных классификаций на обучающей",
               "   выборке. Сравните полученный результат с логистической",
               "   регрессией."
            }, true);
            std::cout << "Precision of prediction: "
                      << get_precision_of_digit_predition(images, y,
                                                          neural_network)
                      << std::endl;


            print_text({
               "6. Реализуйте функцию стоимости для данной нейронной сети.",
               "7. Добавьте L2-регуляризацию в функцию стоимости."
            }, true);
            const auto loss = neural_network.calc_loss(x_vectors,
                                                       one_hot_encoded_y);
            std::cout << "LOSS: " << loss << "\n";

            const auto loss_l2 = neural_network.calc_loss_l2(
                x_vectors,
                one_hot_encoded_y,
                lambda<double>(5)
            );
            std::cout << "LOSS L2: " << loss_l2 << "\n";
        }

        print_text({
           "8. Реализуйте функцию вычисления производной для функции",
           "   активации.",
           "9. Инициализируйте веса небольшими случайными числами."
        }, true);
        const double weights_range = std::sqrt(6)
            / std::sqrt((FirstLayerThetas - 1) + SecondLayerNeurons);
        const uint32_t weights_seed = 228;

        const auto neural_network_2 = ml::nn::NeuralNetwork<
            double,
            FirstLayerThetas,
            FirstLayerNeurons,
            SecondLayerThetas,
            SecondLayerNeurons
        >::create(
            -weights_range, weights_range, weights_seed,
            static_unique_pointer_cast<Activator<double>>(
                std::make_unique<SigmoidActivator<double>>()
            )
        );


        print_text({
           "10. Реализуйте алгоритм обратного распространения ошибки",
           "    для данной конфигурации сети."
        }, true);
        const auto grads = neural_network_2.calc_grad(x, one_hot_encoded_y_m);
        // for (const auto grad: grads) {
        //     print_matrix(grad, "GRAD: ");
        // }

        print_text({
           "11. Для того, чтобы удостоверится в правильности",
           "    вычисленных значений градиентов используйте метод",
           "    проверки градиента с параметром ε = 10-4."
        }, true);


        print_text({
           "12. Добавьте L2-регуляризацию в процесс вычисления градиентов."
        }, true);


        print_text({"13. Проверьте полученные значения градиента."}, true);


        print_text({
           "14. Обучите нейронную сеть с использованием градиентного",
           "    спуска или других более эффективных методов оптимизации."
        }, true);


        print_text({
           "15. Вычислите процент правильных классификаций на обучающей",
           "    выборке."
        }, true);


        print_text({"16. Визуализируйте скрытый слой обученной сети."}, true);


        print_text({
           "17. Подберите параметр регуляризации. Как меняются изображения",
           "    на скрытом слое в зависимости от данного параметра?"
        }, true);
    }
}

int main(int argc, char* argv[])
{
    lab4::SetupRootSettings();

    const auto expected_opts_var_map = parse_arguments<lab4::ArgsLogicError>(
        argc, argv,
        lab4::check_args_logic,
        lab4::get_options_description,
        lab4::ArgsLogicErrExplanationMap
    );
    if (not expected_opts_var_map) {
        print_arguments_error(expected_opts_var_map.error());
        return to_underlying(
            lab4::ProgramReturnCode::ERROR_DURING_ARGS_PARSING
        );
    }

    const auto& opts_var_map = expected_opts_var_map.value();
    std::filesystem::path input_dir = opts_var_map[lab4::INPUT_DIR_OPT]
        .as<std::string>();
    std::filesystem::path output_dir = opts_var_map[lab4::OUTPUT_DIR_OPT]
        .as<std::string>();

    lab4::labwork_1st_part(input_dir, output_dir);

    return 0;
}
