#include <array>
#include <filesystem>
#include <functional>
#include <iostream>
#include <locale>
#include <string>
#include <variant>
#include <functional>
#include <numeric>
#include <algorithm>
#include <optional>
#include <cmath>

#include <boost/program_options.hpp>
#include <rapidcsv.h>
#include <tl/expected.hpp>

#include <TApplication.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TF12.h>
#include <TF2.h>
#include <TGaxis.h>
#include <TGraph2D.h>
#include <TGraph2DErrors.h>
#include <TGraphErrors.h>
#include <TASImage.h>
#include <TLegend.h>
#include <TROOT.h>
#include <TRandom.h>
#include <TStyle.h>

#include "ml/opdesc/logreg.hpp"
#include "ml/utils/cmd_args_helpers.hpp"
#include "ml/utils/enum_helpers.hpp"
#include "ml/utils/io_helpers.hpp"
#include "ml/utils/plot_helpers.hpp"
#include "ml/utils/polynom_elements_degrees.hpp"
#include "ml/utils/color_helpers.hpp"
#include "ml/utils/time_meter.hpp"


using namespace ml::utils;
namespace boost_po = boost::program_options;

namespace lab2
{
    using namespace ROOT;
    using namespace ml::opdesc;
    using namespace ml::opdesc::logreg;

    const auto INPUT_DIR_OPT  = "input_data_dir";
    const auto OUTPUT_DIR_OPT = "output_dir";
    const auto THETA_OPT  = "theta";
    const auto ALPHA_OPT  = "alpha";

    enum class ProgramReturnCode
    {
        FIRST                          = 0,
        ALL_IS_GOOD                    = 0,
        ERROR_DURING_ARGS_PARSING      = 1,
        ERROR_DURING_READING_FROM_FILE = 2,
        ERROR_DURING_OUTPUT_SAVING     = 3,
        LAST                           = 3
    };

    enum class ArgsLogicError
    {
        FIRST                   = 0,
        NO_INPUT_OR_OUTPUT_DIRS = 0,
        LAST                    = 0
    };
    const std::map<ArgsLogicError, std::string> ArgsLogicErrExplanationMap = {
        {
            ArgsLogicError::NO_INPUT_OR_OUTPUT_DIRS,
            "Use --input_data_dir and --output_dir options."
        }
    };

    static const std::map<ProgramReturnCode, std::string>
    ReturnValExplanationMap = {
        { ProgramReturnCode::ALL_IS_GOOD, "All is good" },
    };

    boost_po::options_description get_options_description()
    {
        boost_po::options_description opts_desc;

        opts_desc.add_options()
            (
                INPUT_DIR_OPT,
                boost_po::value<std::string>(),
                "Path to directory with input data"
            )
            (
                OUTPUT_DIR_OPT,
                boost_po::value<std::string>(),
                "Path to directory with output data"
            )
            (
                THETA_OPT,
                boost_po::value<std::vector<double>>()->multitoken(),
                "Theta coefficients"
            )
            (
                ALPHA_OPT,
                boost_po::value<double>(),
                "Alpha coefficient"
            );

        return opts_desc;
    }

    std::optional<ArgsLogicError> check_args_logic(
        const boost_po::variables_map& var_map
    )
    {
        const auto is_input_dir_setuped = var_map.count(INPUT_DIR_OPT) != 0;
        const auto is_output_dir_setuped = var_map.count(OUTPUT_DIR_OPT) != 0;

        if (is_input_dir_setuped && is_output_dir_setuped)
        {
            return std::nullopt;
        }

        return ArgsLogicError::NO_INPUT_OR_OUTPUT_DIRS;
    }

    void SetupRootSettings()
    {
        // Remove useless notifications
        GetROOT()->ProcessLine("gErrorIgnoreLevel = kFatal;");

        gStyle->SetPalette(55);

        std::cout << std::fixed << std::setprecision(10);
    }

    const epsilon<double> DEFAULT_EPSILONE(1e-8);
    const auto SURF_POINTS_COUNT = 100000;

    template<typename T, std::size_t NInputVars>
    std::optional<FuncIOVariants<T, NInputVars>> load_data(
        std::filesystem::path csv
    )
    {
        try {
            auto maybe_reader = CSVFileReader::load_file_without_labels(csv);
            if (not maybe_reader) {
                std::cout << "Can't continue. File <" << csv
                          << "> not exists" << std::endl;
                return std::nullopt;
            }

            auto maybe_variants = maybe_reader->get_variants<T, NInputVars>();
            if (not maybe_variants) {
                std::cout
                    << "Can't read function input/output variants from <"
                    << csv << "> file." << std::endl;
                return std::nullopt;
            }

            std::cout << "File <" << csv
                      << "> successfully loaded!" << std::endl;
            return FuncIOVariants<T, NInputVars>(std::move(*maybe_variants));
        } catch (const std::ios_base::failure& ex) {
            return std::nullopt;
        }
    }

    template<std::size_t NInputVars>
    void print_data_plot__2nd_ex(
        const FuncIOVariants<double, NInputVars>& dots,
        const std::filesystem::path& out_file
    )
    {
        TCanvas canvas("canvas1", "", 700, 700);
        canvas.SetGrid(1, 1);

        TGraph graph_passed;
        graph_passed.SetMarkerColor(EColor::kBlue);
        graph_passed.SetMarkerStyle(500);

        TGraph graph_not_passed;
        graph_not_passed.SetMarkerColor(EColor::kRed);
        graph_not_passed.SetMarkerStyle(500);

        for (std::size_t i = 0; i < dots.get_variants_count(); ++i) {
            const auto& point = dots[i];
            auto& graph = point.out() == 1
                ? graph_passed
                : graph_not_passed;

            graph.SetPoint(static_cast<int>(i), point.in(0), point.in(1));
        }

        graph_passed.Draw("AP*");
        graph_not_passed.Draw("SAME P*");
        save_plot(canvas, "1.1 plot", out_file);
    }

    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    void print_data_plot_with_divide_line_plot__6th_ex(
        const FuncIOVariants<double, NInputVars>& dots,
        const MinimumDesc<double, NThetas>& theta_descent,
        const std::filesystem::path& out_file
    )
    {
        TCanvas canvas("canvas1", "", 700, 700);
        canvas.SetGrid(1, 1);

        TGraph graph_passed;
        graph_passed.SetMarkerColor(EColor::kBlue);
        graph_passed.SetMarkerStyle(500);

        TGraph graph_not_passed;
        graph_not_passed.SetMarkerColor(EColor::kRed);
        graph_not_passed.SetMarkerStyle(500);

        for (std::size_t i = 0; i < dots.get_variants_count(); ++i) {
            const auto& point = dots[i];
            auto& graph = point.out() == 1
                ? graph_passed
                : graph_not_passed;

            graph.SetPoint(static_cast<int>(i), point[0], point[1]);
        }

        // Method from: https://datascience.stackexchange.com/a/49583
        TF1 func("h_0(x)", "([0] + x * [1]) * (-1 / [2])", -30, 150);
        func.SetMarkerColor(kGreen);
        func.SetLineColor(kGreen);
        const auto final_theta = theta_descent.get_vec_minimum();
        func.SetParameter(0, final_theta[0]);
        func.SetParameter(1, final_theta[1]);
        func.SetParameter(2, final_theta[2]);

        graph_passed.Draw("SAME AP*");
        graph_not_passed.Draw("SAME P*");
        func.Draw("SAME");
        save_plot(canvas, "1.2 plot", out_file);
    }

    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<double, NThetas>> logreg__3rd_ex(
        const FuncIOVariants<double, NInputVars>& func_io
    )
    {
        const auto start_thetas = make_vec<double>({0, 0, 0});
        const auto x = func_io.get_x_matrix();
        const auto y = func_io.get_y_vector();
        const alpha<double> alpha(0.02);
        const beta<double> beta(0.8);

        auto maybe_minimum
            = logreg::calc_grad_descent_momentum<double, NInputVars>(
                start_thetas, x, y, alpha, beta, DEFAULT_EPSILONE
              );
        if (maybe_minimum) {
            maybe_minimum->print_info("1.1");
        }

        return maybe_minimum;
    }

    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<double, NThetas>> nma__4th_ex(
        const FuncIOVariants<double, NInputVars>& func_io
    )
    {
        const start_simplex<double, NThetas> start_simplex(0);
        const auto x = func_io.get_x_matrix();
        const auto y = func_io.get_y_vector();
        const alpha<double> alpha(0.0001);

        auto maybe_minimum = logreg::call_nma_minimizer<NInputVars>(
            start_simplex, x, y, alpha, DEFAULT_EPSILONE
        );
        if (maybe_minimum) {
            maybe_minimum->print_info("1.1");
        }

        return maybe_minimum;
    }

    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<double, NThetas>> bfgs__4_ex(
        const FuncIOVariants<double, NInputVars>& func_io
    )
    {
        const start_simplex<double, NThetas> start_simplex(0);
        const auto x = func_io.get_x_matrix();
        const auto y = func_io.get_y_vector();
        const alpha<double> alpha(0.00001);

        auto maybe_minimum = logreg::call_bfgs_minimizer<NInputVars>(
            start_simplex, x, y, alpha, DEFAULT_EPSILONE
        );
        if (maybe_minimum) {
            maybe_minimum->print_info("1.1");
        }

        return maybe_minimum;
    }

    bool will_the_student_be_enrolled(const x_vec<double>& marks,
                                      const theta_vec<double>& w)
    {
        return std::isgreaterequal(logreg::calc_h(w, marks), 0.5);
    }

    void will_the_student_be_enrolled(const Array<double, 2>& marks,
                                      const theta_vec<double>& w,
                                      const std::string& student_name)
    {
        std::cout << student_name << " passed the tests: ";
        print_els(marks);
        std::cout << ". ";
        if (will_the_student_be_enrolled(make_x_vec(marks), w)) {
            std::cout << student_name << " will be entrolled";
        } else {
            std::cout << student_name << " will NOT be entrolled";
        }

        std::cout << std::endl;
    }

    std::vector<bool> transform_y_to_bool(const TVectorT<double>& y)
    {
        return transform_vec<double, bool>(
            y,
            [](double a) { return std::isgreaterequal(a, 0.5); }
        );
    }

    std::vector<bool> will_the_students_be_enrolled(
        const x_matrix<double>& x,
        const theta_vec<double>& w
    )
    {
        return transform_y_to_bool(logreg::calc_h2(w, x));
    }

    double get_precision_of_prediction_for_students(
        const x_matrix<double>& x,
        const y_vec<double>& y,
        const theta_vec<double>& w
    )
    {
        return calc_avg<double, bool>(
            compare_els<bool>(
                transform_y_to_bool(y),
                will_the_students_be_enrolled(x, w)
            )
        );
    }

    void labwork_1st_part(
        const std::filesystem::path& input_dir,
        const std::filesystem::path& out_dir
    )
    {
        // 1. Загрузите данные ex2data1.txt из текстового файла.
        const auto path_to_file = input_dir / "ex2data1.txt";
        constexpr std::size_t NInputVars = 2;
        const auto maybe_fio = load_data<double, NInputVars>(path_to_file);
        if (not maybe_fio) {
            return;
        }
        const auto& fio = maybe_fio.value();

        // 2. Постройте график, где по осям откладываются оценки по
        //    предметам, а точки обозначаются двумя разными маркерами в
        //    зависимости от того, поступил ли данный студент в
        //    университет или нет.
        print_data_plot__2nd_ex(fio, (out_dir / "02-1-01_plot.pdf"));

        // 3. Реализуйте функции потерь J(θ) и градиентного спуска для
        //    логистической регрессии с использованием векторизации.
        // 4. Реализуйте другие методы (как минимум 2) оптимизации для
        //    реализованной функции стоимости (например, Метод Нелдера —
        //    Мида, Алгоритм Бройдена — Флетчера — Гольдфарба — Шанно,
        //    генетические методы и т.п.). Разрешается использовать
        //    библиотечные реализации методов оптимизации (например,
        //    из библиотеки scipy).
        const auto maybe_min_logreg = logreg__3rd_ex<NInputVars>(fio);
        const auto maybe_min_nma = nma__4th_ex<NInputVars>(fio);
        const auto maybe_min_bfgs = bfgs__4_ex<NInputVars>(fio);

        if (maybe_min_logreg) {
            const auto& minimum = maybe_min_logreg->get_vec_minimum();

            // 5. Реализуйте функцию предсказания вероятности
            // поступления студента в зависимости от значений оценок
            // по экзаменам.
            will_the_student_be_enrolled({74, 89}, minimum, "Vasya");
            will_the_student_be_enrolled({50, 50}, minimum, "Boris");
            const auto precision = get_precision_of_prediction_for_students(
                    fio.get_x_matrix(),
                    fio.get_y_vector(),
                    minimum
            );
            std::cout << "Prediction precision: " << precision << std::endl;

            // 6. Постройте разделяющую прямую, полученную в
            // результате обучения модели. Совместите прямую с
            // графиком из пункта 2.
            print_data_plot_with_divide_line_plot__6th_ex(
                fio, *maybe_min_bfgs, (out_dir / "02-1-02_plot.pdf")
            );
        }
    }


    template<std::size_t NInputVars>
    void print_data_plot__8_ex(
        const FuncIOVariants<double, NInputVars>& dots,
        const std::filesystem::path& out_file
    )
    {
        TCanvas canvas("canvas1", "", 700, 700);
        canvas.SetGrid(1, 1);

        TGraph graph_passed;
        graph_passed.SetMarkerColor(EColor::kBlue);
        graph_passed.SetMarkerStyle(3);

        TGraph graph_not_passed;
        graph_not_passed.SetMarkerColor(EColor::kRed);
        graph_not_passed.SetMarkerStyle(3);

        for (std::size_t i = 0; i < dots.get_variants_count(); ++i) {
            const auto& point = dots[i];
            auto& graph = point.out() == 1
                ? graph_passed
                : graph_not_passed;

            graph.SetPoint(static_cast<int>(i), point.in(0), point.in(1));
        }

        graph_passed.Draw("AP*");
        graph_not_passed.Draw("SAME P");
        save_plot(canvas, "2.1 plot", out_file);
    }

    template<std::size_t NInputVars,
             std::size_t NThetas = NInputVars + 1>
    void print_data_plot_with_devide_line__13_14_ex(
        const FuncIOVariants<double, NInputVars>& dots,
        const MinimumDesc<double, NThetas>& minimum,
        const polynom_degrees<NInputVars> els_degrees,
        const lambda<double> lambda,
        const std::string name_part,
        const std::filesystem::path& out_file
    )
    {
        TCanvas canvas("canvas1", "", 700, 700);
        canvas.SetGrid(1, 1);

        TGraph graph_passed;
        graph_passed.SetMarkerColor(EColor::kBlue);
        graph_passed.SetMarkerStyle(3);

        TGraph graph_not_passed;
        graph_not_passed.SetMarkerColor(EColor::kRed);
        graph_not_passed.SetMarkerStyle(3);

        for (std::size_t i = 0; i < dots.get_variants_count(); ++i) {
            const auto& point = dots[i];
            auto& graph = point.out() == 1
                ? graph_passed
                : graph_not_passed;

            graph.SetPoint(static_cast<int>(i), point.in(0), point.in(1));
        }

        TF2 func(
            "h",
            [&] (const double* const val, const double* const par) {
                double result = 0;
                for (std::size_t i = 0; i < els_degrees.size(); ++i) {
                    double el_val = 1;
                    for (std::size_t j = 0; j < NInputVars; ++j) {
                        el_val *= std::pow(
                            val[j],
                            static_cast<double>(els_degrees[i][j])
                        );
                    }

                    result += minimum[i + 1] * el_val;
                }
                result += minimum[0];

                return logreg::sigmoid(result);
            },
            -5, 5, -5, 5, 2);
        func.SetLineColor(kGreen);

        graph_passed.Draw("AP");
        graph_not_passed.Draw("SAME P");
        func.Draw("SAME");

        TLegend legend(0.5, 0.8, 0.9, 0.9);
        const auto header = "Data with divide line: " + name_part;
        legend.SetHeader(header.c_str());

        const auto lambda_entry = "Lambda = " + std::to_string(*lambda);
        auto* const learning_sample_entry = legend.AddEntry(
            "lambda", lambda_entry.c_str(), "l"
        );

        legend.Draw();
        save_plot(canvas, name_part, out_file);
    }

    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<double, NThetas>> logreg_l2__10_ex(
        const FuncIOVariants<double, NInputVars>& fio,
        const lambda<double> lambda,
        const bool need_info = true
    )
    {
        const auto start_thetas = make_zero_vec<double>(NThetas);
        const auto x = fio.get_x_matrix();
        const auto y = fio.get_y_vector();
        const alpha<double> alpha(0.01);

        auto maybe_minimum
            = logreg::calc_grad_descent_l2_momentum<double, NInputVars>(
                start_thetas, x, y, alpha, beta<double>(0.9),  lambda,
                epsilon<double>(1e-5)
              );
        if (maybe_minimum and need_info) {
            maybe_minimum->print_info("2.1");
        }

        return maybe_minimum;
    }

    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars,
             std::size_t NDims = NThetas + 1>
    std::optional<MinimumDesc<double, NThetas>> nma_l2__11_ex(
        const FuncIOVariants<double, NInputVars>& fio,
        const lambda<double> lambda
    )
    {
        const start_simplex<double, NDims> start_simplex(0);
        const auto x = fio.get_x_matrix();
        const auto y = fio.get_y_vector();
        const non_zero_value<double> alpha(1e-5);

        auto maybe_minimum = logreg::call_nma_minimizer_l2<NInputVars>(
            start_simplex, x, y, alpha, lambda, epsilon<double>(1e-4)
        );
        if (maybe_minimum) {
            maybe_minimum->print_info("2.1");
        }

        return maybe_minimum;
    }

    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars,
             std::size_t NDims = NThetas + 1>
    std::optional<MinimumDesc<double, NThetas>> bfgs_l2__11_ex(
        const FuncIOVariants<double, NInputVars>& fio,
        const lambda<double> lambda
    )
    {
        const start_simplex<double, NThetas> start_simplex(0);
        const auto x = fio.get_x_matrix();
        const auto y = fio.get_y_vector();
        const alpha<double> alpha(1e-8);

        auto maybe_minimum = logreg::call_bfgs_minimizer_l2<NInputVars>(
            start_simplex, x, y, alpha, lambda
        );
        if (maybe_minimum) {
            maybe_minimum->print_info("2.1");
        }

        return maybe_minimum;
    }


    template<typename T, std::size_t NElements>
    T get_probability_of_passing_check(const Array<T, NElements> params,
                                       const polynom_degrees<NElements> degrees,
                                       const theta_vec<T> weight)
    {
        return logreg::calc_h(weight, make_x_vec(params, degrees));
    }

    template<typename T, std::size_t NElements>
    void print_probability_of_passing_check(
        const Array<T, NElements> params,
        const polynom_degrees<NElements> degrees,
        const theta_vec<T> weight,
        const std::string& detail_name
    )
    {
        std::cout << "Probability that detail "
                  << detail_name
                  << " with params ( ";
        print_els(params);
        std::cout << ") will pass check: "
                  << get_probability_of_passing_check(params, degrees, weight)
                  << std::endl;
    }

    void labwork_2nd_part(
        const std::filesystem::path& input_dir,
        const std::filesystem::path& out_dir
    )
    {
        constexpr auto NInputVars = 2;
        constexpr auto PolynomDegree = 6;
        constexpr auto NPolynomEls
            = polynom_combinations(NInputVars, PolynomDegree);
        constexpr auto NNewInputVars = NPolynomEls - 1;
        const auto polynom_degrees = make_polynom_degrees_vec<
                                         NInputVars,
                                         PolynomDegree,
                                         NPolynomEls>();

        // 7. Загрузите данные ex2data2.txt из текстового файла.
        const auto path_to_file = input_dir / "ex2data2.txt";
        const auto maybe_fio = load_data<double, NInputVars>(path_to_file);
        if (not maybe_fio) {
            return;
        }
        const auto& func_io = maybe_fio.value();

        // 8. Постройте график, где по осям откладываются результаты
        //    тестов, а точки обозначаются двумя разными маркерами в
        //    зависимости от того, прошло ли изделие контроль или нет.
        print_data_plot__8_ex(func_io, (out_dir / "02-2-01_plot.pdf"));

        // 9. Постройте все возможные комбинации признаков x1 (результат
        //    первого теста) и x2 (результат второго теста), в которых степень
        //    полинома не превышает 6, т.е. 1, x1, x2, x12, x1x2, x22, …,
        //    x1x25, x26 (всего 28 комбинаций).
        const auto& ext_fio = func_io.create_feature_map<NNewInputVars>(
            polynom_degrees
        );
        std::cout << "Input successfully extended!" << std::endl;

        // 10. Реализуйте L2-регуляризацию для логистической регрессии и
        //     обучите ее на расширенном наборе признаков методом
        //     градиентного спуска.
        // 11. Реализуйте другие методы оптимизации.
        const lambda<double> default_lambda(0.05);
        const auto maybe_minimum_logreg = logreg_l2__10_ex(ext_fio,
                                                           default_lambda);
        const auto maybe_minimum_nma = nma_l2__11_ex(ext_fio,
                                                     default_lambda);
        const auto maybe_minimum_bfgs = bfgs_l2__11_ex(ext_fio,
                                                       default_lambda);

        if (maybe_minimum_logreg) {
            // 12. Реализуйте функцию предсказания вероятности прохождения
            //     контроля изделием в зависимости от результатов тестов.
            print_probability_of_passing_check<double, 2>(
                {0.051267,0.69956},
                polynom_degrees,
                maybe_minimum_logreg->get_vec_minimum(),
                "TopKek"
            );

            // 13. Постройте разделяющую кривую, полученную в результате
            //     обучения модели. Совместите прямую с графиком из пункта 7.
            print_data_plot_with_devide_line__13_14_ex(
                func_io, *maybe_minimum_logreg,
                polynom_degrees, default_lambda,
                "Logreg 13 ex", (out_dir / "02-2-02_plot.pdf")
            );
        }

        // 14. Попробуйте различные значения параметра регуляризации λ. Как
        //     выбор данного значения влияет на вид разделяющей кривой?
        //     Ответ дайте в виде графиков
        for (const auto lambda: {0.0001, 0.1, 0.5, 1.0, 100.0}) {
            const auto maybe_minimum_logreg = logreg_l2__10_ex(
                ext_fio, ml::utils::lambda<double>(lambda), false
            );
            assert(maybe_minimum_logreg);
            const auto path_to_plot = (
                out_dir / ("02-2-03-" + std::to_string(lambda) + "_plot.pdf")
            );

            print_data_plot_with_devide_line__13_14_ex(
                func_io, *maybe_minimum_logreg, polynom_degrees,
                ml::utils::lambda<double>(lambda), "Logreg 13 ex",
                path_to_plot
            );
        }
    }

    std::optional<FuncIOVariants<double, 400>> get_3rd_data(
        const std::filesystem::path& path,
        const std::size_t x_vars_count
    )
    {
        const auto maybe_file_reader = MatFileReader::load_file(path);
        if (not maybe_file_reader) {
            return std::nullopt;
        }

        auto maybe_variants = maybe_file_reader->read_variants<double, 400>(
            "X", "y"
        );
        if (not maybe_variants) {
            return std::nullopt;
        }

        // Fix Y vector numbers
        for (std::size_t i = 0; i < maybe_variants->size(); ++i) {
            if ((*maybe_variants)[i].out() == 10) {
                (*maybe_variants)[i].out() = 0;
            }
        }

        return FuncIOVariants<double, 400>(std::move(*maybe_variants));
    }

    std::optional<TMatrixTRow_const<double>> get_digit_image(
        const uint8_t digit,
        const x_matrix<double>& images,
        const y_vec<double>& corresponding_digits,
        const std::size_t skip_n_images = 0
    )
    {
        std::size_t skipped_images = 0;
        for (Int_t img_idx = 0; img_idx < images.GetNrows(); ++img_idx) {
            if (corresponding_digits[img_idx] == static_cast<double>(digit)) {
                if (skipped_images != skip_n_images) {
                    skipped_images++;
                    continue;
                }

                return TMatrixTRow_const<double>(images, img_idx);
            }
        }

        return std::nullopt;
    }

    void set_pixels(TASImage& image,
                    const TMatrixTRow_const<double>& image_row,
                    const uint8_t width,
                    const uint8_t height)
    {
        for (uint8_t row_idx = 0; row_idx < height; ++row_idx) {
            for (uint8_t col_idx = 0; col_idx < width; ++col_idx) {
                const auto alpha_channel = image_row[
                    static_cast<Int_t>(col_idx * height + row_idx + 1)
                ];
                // const ColorRGBA rgba(0, 0, 0, alpha_channel);
                // const ColorRGB rgb = rgba.rgb(ColorRGB::get_white());
                auto rgb = get_color_from_ycbcr(alpha_channel, 0, 0);
                const auto rgb_hex_repr = rgb.get_hex();

                image.PutPixel(
                    static_cast<Int_t>(col_idx),
                    static_cast<Int_t>(row_idx),
                    rgb_hex_repr.c_str()
                );
            }
        }
    }

    // Print all digits
    void print_5th_plot(
        const x_matrix<double>& image_matrix,
        const y_vec<double>& corresponding_digits,
        const std::filesystem::path& out_file
    )
    {
        constexpr uint8_t img_width = 20;
        constexpr uint8_t img_height = img_width;
        constexpr uint8_t digits_hor = 5;
        constexpr uint8_t digits_vert = 2;
        const auto canvas_width = static_cast<Int_t>(img_width * digits_hor);
        const auto canvas_height = static_cast<Int_t>(img_height * digits_vert);
        const double img_scale = 20;

        TCanvas canvas("canvas1", "", 400 * 5, 400 * 2); // canvas_width, canvas_height
        canvas.Divide(digits_hor, digits_vert);

        std::vector<TASImage> images;
        images.reserve(10);

        for (uint8_t digit = 0; digit <= 9; ++digit) {
            const auto subpad_id = static_cast<Int_t>(digit + 1);
            canvas.cd(subpad_id);

            const auto maybe_digit_image_vec = get_digit_image(
                digit,
                image_matrix,
                corresponding_digits
            );
            if (not maybe_digit_image_vec) {
                std::cout << "Image for " << std::to_string(digit)
                          << " digit not found!" << std::endl;
                continue;
            }

            images.emplace_back(img_width, img_height);
            set_pixels(images.back(),
                       *maybe_digit_image_vec,
                       img_width, img_height);
            images.back().Scale(
                static_cast<UInt_t>(img_width * img_scale),
                static_cast<UInt_t>(img_height * img_scale)
            );
            images.back().Draw("x");
        }

        save_plot(canvas, "3.1 plot", out_file);
    }

    y_vec<double> get_y_vec_for_digit(
        const uint8_t digit,
        const y_vec<double>& y
    )
    {
        y_vec<double> result(y.GetNrows());
        for (Int_t i = 0; i < y.GetNrows(); ++i) {
            if (static_cast<uint8_t>(y[i]) == digit) {
                result[i] = 1;
            } else {
                result[i] = 0;
            }
        }

        return result;
    }

    template<typename T, std::size_t NXVars, std::size_t NThetas = NXVars>
    std::optional<theta_vec<T>> get_weigth_for_digit(
        const uint8_t digit,
        const x_matrix<T>& x,
        const y_vec<T>& non_specialized_y
    )
    {
        const auto y = get_y_vec_for_digit(digit, non_specialized_y);
        std::cout << "Start grad descent for "
                  << std::to_string(digit) << " digit." << "\n";
        const auto maybe_minimum
            = logreg::calc_grad_descent_l2_momentum<T, NXVars>(
                make_zero_vec<T>(NThetas),
                x, y, alpha<T>(0.3), beta<T>(0.888), lambda<T>(0.5),
                epsilon<T>(1e-7)
             );

        if (not maybe_minimum) {
            std::cout << "Minimum for " << std::to_string(digit)
                      << " digit NOT found!" << std::endl;
        }

        return maybe_minimum->get_vec_minimum();
    }

    template<typename T, std::size_t NXVars>
    std::optional<std::vector<theta_vec<T>>> get_weights_for_digits(
        const x_matrix<T>& x,
        const y_vec<T>& non_specialized_y
    )
    {
        std::vector<theta_vec<T>> result;
        result.reserve(10);

        for (uint8_t digit = 0; digit <= 9; ++digit) {
            auto maybe_weight = get_weigth_for_digit<T, NXVars>(
                digit, x, non_specialized_y
            );
            if (not maybe_weight) {
                return std::nullopt;
            }

            result.emplace_back(std::move(*maybe_weight));
        }

        return result;
    }

    template<typename T>
    std::tuple<uint8_t, double> get_digit_from_image(
        const x_vec<T>& image,
        const std::vector<theta_vec<T>>& weights
    )
    {
        std::vector<double> posibilities;
        for (uint8_t digit = 0; digit <= 9; ++digit) {
            posibilities.push_back(
                logreg::calc_h(weights[digit], image)
            );
        }

        const auto max_el_iter = std::max_element(posibilities.begin(),
                                                  posibilities.end());

        return {
            static_cast<uint8_t>(
                std::distance(posibilities.begin(), max_el_iter)
            ),
            *max_el_iter
        };
    }

    template<typename T>
    double get_precision_of_digit_predition(
        const x_matrix<T>& images,
        const y_vec<T>& non_specialized_y,
        const std::vector<theta_vec<T>>& weights
    )
    {
        const auto images_count
            = static_cast<std::size_t>(non_specialized_y.GetNrows());
        std::vector<uint8_t> predicted_result;
        predicted_result.reserve(images_count);
        for (std::size_t i = 0; i < images_count; ++i) {
            const auto predicted_info = get_digit_from_image(
                get_row(images, static_cast<Int_t>(i)),
                weights
            );
            predicted_result.push_back(std::get<0>(predicted_info));
        }

        return calc_avg<double, bool>(
            compare_els<uint8_t>(
                transform_vec<double, uint8_t>(
                    non_specialized_y,
                    [] (const double el) { return static_cast<uint8_t>(el); }
                ),
                predicted_result
            )
        );
    }

    void labwork_3rd_part(
        const std::filesystem::path& input_dir,
        const std::filesystem::path& out_dir
    )
    {
        constexpr std::size_t N_INPUT_VARS = 400;
        constexpr std::size_t N_XVARS = N_INPUT_VARS + 1;
        constexpr std::size_t N_THETAS = N_XVARS;

        // 15. Загрузите данные ex2data3.mat из файла.
        const auto path_to_mat = input_dir / "ex2data3.mat";
        const auto maybe_data = get_3rd_data(path_to_mat, N_XVARS);
        if (not maybe_data) {
            return;
        }

        // 16. Визуализируйте несколько случайных изображений из набора
        //     данных. Визуализация должна содержать каждую цифру как
        //     минимум один раз.
        const auto& x = maybe_data->get_x_matrix();
        const auto& y = maybe_data->get_y_vector();
        print_5th_plot(x, y, (out_dir / "02-3-01_plot.jpeg"));

        // 17. Реализуйте бинарный классификатор с помощью логистической
        //     регрессии с использованием векторизации (функции потерь и
        //     градиентного спуска).
        // 18. Добавьте L2-регуляризацию к модели.
        const auto maybe_w = get_weights_for_digits<double, N_XVARS>(x, y);
        if (not maybe_w) {
            return;
        }

        // 19. Реализуйте многоклассовую классификацию по методу “один
        //     против всех”.
        // 20. Реализуйте функцию предсказания класса по изображению с
        //     использованием обученных классификаторов.
        const Int_t digit_image_idx = 2500;
        auto info = get_digit_from_image<double>(
            get_row(x, digit_image_idx),
            *maybe_w
        );

        std::cout << "Digit " << std::to_string(y[digit_image_idx])
                  << " classified as "
                  << std::to_string(std::get<0>(info))
                  << " digit with a probability of "
                  << std::get<1>(info)
                  << " persents."
                  << std::endl;

        // 21. Процент правильных классификаций на обучающей выборке должен
        //     составлять около 95%.
        std::cout << "Precision of prediction: "
                  << get_precision_of_digit_predition(x, y, *maybe_w)
                  << std::endl;
    }
}

int main(int argc, char* argv[])
{
    lab2::SetupRootSettings();

    const auto expected_opts_var_map = parse_arguments<lab2::ArgsLogicError>(
        argc, argv,
        lab2::check_args_logic,
        lab2::get_options_description,
        lab2::ArgsLogicErrExplanationMap
    );
    if (not expected_opts_var_map) {
        print_arguments_error(expected_opts_var_map.error());
        return to_underlying(
            lab2::ProgramReturnCode::ERROR_DURING_ARGS_PARSING
        );
    }

    const auto& opts_var_map = expected_opts_var_map.value();
    std::filesystem::path input_dir = opts_var_map[lab2::INPUT_DIR_OPT]
        .as<std::string>();
    std::filesystem::path output_dir = opts_var_map[lab2::OUTPUT_DIR_OPT]
        .as<std::string>();

    // lab2::labwork_1st_part(input_dir, output_dir);
    // print_marking_line();
    lab2::labwork_2nd_part(input_dir, output_dir);
    print_marking_line();
    lab2::labwork_3rd_part(input_dir, output_dir);

    return 0;
}
