#include <algorithm>
#include <array>
#include <cmath>
#include <filesystem>
#include <functional>
#include <iostream>
#include <locale>
#include <numeric>
#include <optional>
#include <string>
#include <variant>

#include <boost/program_options.hpp>
#include <rapidcsv.h>
#include <tl/expected.hpp>

#include <TVectorT.h>
#include <TASImage.h>
#include <TApplication.h>
#include <TAxis.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TF12.h>
#include <TF2.h>
#include <TGaxis.h>
#include <TGraph2D.h>
#include <TGraph2DErrors.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TROOT.h>
#include <TRandom.h>
#include <TStyle.h>

#include "ml/utils/cmd_args_helpers.hpp"
#include "ml/utils/color_helpers.hpp"
#include "ml/utils/enum_helpers.hpp"
#include "ml/utils/io_helpers.hpp"
#include "ml/utils/plot_helpers.hpp"
#include "ml/utils/polynom_elements_degrees.hpp"
#include "ml/utils/time_meter.hpp"

#include "ml/opdesc/grad_descent.hpp"
#include "ml/opdesc/linreg.hpp"
#include "ml/opdesc/plot_helpers.hpp"

using namespace ml::utils;
namespace boost_po = boost::program_options;

namespace lab3
{
    using namespace std::placeholders;

    using namespace ROOT;

    using namespace ml::opdesc;
    using namespace ml::opdesc::plot_helpers;


    const auto INPUT_DIR_OPT  = "input_data_dir";
    const auto OUTPUT_DIR_OPT = "output_dir";
    const auto THETA_OPT  = "theta";
    const auto ALPHA_OPT  = "alpha";

    enum class ProgramReturnCode
    {
        FIRST                          = 0,
        ALL_IS_GOOD                    = 0,
        ERROR_DURING_ARGS_PARSING      = 1,
        ERROR_DURING_READING_FROM_FILE = 2,
        ERROR_DURING_OUTPUT_SAVING     = 3,
        LAST                           = 3
    };

    enum class ArgsLogicError
    {
        FIRST                   = 0,
        NO_INPUT_OR_OUTPUT_DIRS = 0,
        LAST                    = 0
    };
    const std::map<ArgsLogicError, std::string> ArgsLogicErrExplanationMap = {
        {
            ArgsLogicError::NO_INPUT_OR_OUTPUT_DIRS,
            "Use --input_data_dir and --output_dir options."
        }
    };

    static const std::map<ProgramReturnCode, std::string>
    ReturnValExplanationMap = {
        { ProgramReturnCode::ALL_IS_GOOD, "All is good" },
    };

    boost_po::options_description get_options_description()
    {
        boost_po::options_description opts_desc;

        opts_desc.add_options()
            (
                INPUT_DIR_OPT,
                boost_po::value<std::string>(),
                "Path to directory with input data"
            )
            (
                OUTPUT_DIR_OPT,
                boost_po::value<std::string>(),
                "Path to directory with output data"
            )
            (
                THETA_OPT,
                boost_po::value<std::vector<double>>()->multitoken(),
                "Theta coefficients"
            )
            (
                ALPHA_OPT,
                boost_po::value<double>(),
                "Alpha coefficient"
            );

        return opts_desc;
    }

    std::optional<ArgsLogicError> check_args_logic(
        const boost_po::variables_map& var_map
    )
    {
        const auto is_input_dir_setuped = var_map.count(INPUT_DIR_OPT) != 0;
        const auto is_output_dir_setuped = var_map.count(OUTPUT_DIR_OPT) != 0;

        if (is_input_dir_setuped && is_output_dir_setuped)
        {
            return std::nullopt;
        }

        return ArgsLogicError::NO_INPUT_OR_OUTPUT_DIRS;
    }

    void SetupRootSettings()
    {
        // Remove useless notifications
        GetROOT()->ProcessLine("gErrorIgnoreLevel = kFatal;");

        gStyle->SetPalette(55);

        std::cout << std::fixed << std::setprecision(10);
    }

    const epsilon<double> DEFAULT_EPSILONE(1e-7);
    const auto SURF_POINTS_COUNT = 100000;

    template<typename T, std::size_t NInputVars>
    std::optional<FuncIOVariants<T, NInputVars>> load_data(
        const std::filesystem::path& path,
        const std::size_t x_vars_count,
        const std::string x_name,
        const std::string y_name
    )
    {
        const auto maybe_reader = MatFileReader::load_file(path);
        if (not maybe_reader) {
            return std::nullopt;
        }
        auto maybe_variants
            = maybe_reader->read_variants<T, NInputVars>(x_name, y_name);
        if (not maybe_variants) {
            return std::nullopt;
        }

        return FuncIOVariants<T, NInputVars>(std::move(*maybe_variants));
    }


    void labwork(
        const std::filesystem::path& input_dir,
        const std::filesystem::path& out_dir
    )
    {
        constexpr std::size_t NInputVars = 1;
        constexpr std::size_t NXVars = NInputVars + 1;
        constexpr std::size_t NThetas = NXVars;

        constexpr std::size_t PolynomDegree = 8;
        constexpr auto NPolynomEls = polynom_combinations(NInputVars, PolynomDegree);
        constexpr auto NNewInputVars = NPolynomEls - 1;
        const auto polynom_degrees
            = make_polynom_degrees_arr<NInputVars, PolynomDegree, NPolynomEls>();
        constexpr std::size_t NNewXVars = NNewInputVars + 1;
        constexpr std::size_t NNewThetas = NNewXVars;


        print_text({
           "1. Загрузите данные ex3data1.mat из файла.",
           "7. Реализуйте функцию добавления p - 1 новых признаков в",
           "   обучающую выборку (X2, X3, X4, …, Xp).",
           "8. Поскольку в данной задаче будет использован полином",
           "   высокой степени, то необходимо перед обучением",
           "   произвести нормализацию признаков."
        }, true);
        ///// Learning sample /////
        const auto path_to_file = input_dir / "ex3data1.mat";
        const auto maybe_fio = load_data<double, NInputVars>(path_to_file,
                                                             1, "X", "y");
        assert(maybe_fio);
        const auto& fio = *maybe_fio;
        const auto x = fio.get_x_matrix();
        const auto y = fio.get_y_vector();

        const auto norm_fio = normalize_data(fio);
        const auto& norm_x = norm_fio.get_x_matrix();
        const auto& norm_y = norm_fio.get_y_vector();

        const auto& ext_fio = fio.create_feature_map<NNewInputVars>(
            polynom_degrees
        );
        const auto ext_avg = calc_params_avg_values(ext_fio);
        const auto ext_rms = calc_params_rms_deviation(ext_fio, ext_avg);

        const auto norm_ext_fio = normalize_data(ext_fio, ext_avg, ext_rms);
        const auto& norm_ext_x = norm_ext_fio.get_x_matrix();
        const auto& norm_ext_y = norm_ext_fio.get_y_vector();
        std::cout << "Learning sample ready!"<< std::endl;
        print_marking_line();

        //////////////////// Validation sample ////////////////////
        const auto maybe_validation_fio = load_data<double, NInputVars>(
            path_to_file,
            1, "Xval", "yval"
        );
        assert(maybe_validation_fio);
        const auto& val_fio = *maybe_validation_fio;
        const auto& x_val = val_fio.get_x_matrix();
        const auto& y_val = val_fio.get_y_vector();
        const auto ext_val_fio = val_fio.create_feature_map<NNewInputVars>(
            polynom_degrees
        );
        const auto norm_ext_val_fio = normalize_data(ext_val_fio,
                                                     ext_avg, ext_rms);
        std::cout << "Validation sample ready!"<< std::endl;
        print_marking_line();

        //////////////////// Test sample ////////////////////
        const auto maybe_test_fio = load_data<double, NInputVars>(
            path_to_file, 1, "Xtest", "ytest"
        );
        assert(maybe_test_fio);
        const auto& test_fio = *maybe_test_fio;
        const auto ext_test_fio
            = test_fio.create_feature_map<NNewInputVars>(polynom_degrees);
        const auto& norm_ext_test_fio = normalize_data(ext_test_fio,
                                                       ext_avg, ext_rms);
        const auto& norm_ext_x_test = norm_ext_test_fio.get_x_matrix();
        const auto& norm_ext_y_test = norm_ext_test_fio.get_y_vector();
        std::cout << "Test sample ready!"<< std::endl;

        {
            print_text({
                "2. Постройте график, где по осям откладываются X и y из",
                "   обучающей выборOки."
            }, true);
            print_data_plot<double>({{"Learning", fio},
                                     {"Validation", val_fio},
                                     {"Test", test_fio}},
                                    "Samples plot",
                                    (out_dir / "03-1-01-all-samples.pdf"));
        }

        {
            print_text({
               "3. Реализуйте функцию стоимости потерь для линейной",
               "   регрессии с L2-регуляризацией.",
               "4. Реализуйте функцию градиентного спуска для линейной",
               "   регрессии с L2-регуляризацией."
            }, true);
            /// Can't use gradient descent with lambda equal to 0 because ///
            /// of lambda type implementation, it use                     ///
            /// type_safe::constraints::non_default constraint            ///
            const auto maybe_min
                = linreg::calc_grad_descent<double, NInputVars>(
                    make_zero_vec<double>(NThetas),
                    norm_x, norm_y,
                    alpha<double>(0.0001), epsilon<double>(1e-6)
                  );
            assert(maybe_min);
            const auto& min = *maybe_min;
            min.print_info("Learning sample with LAMBDA = 0");
            const auto& vec_min = min.get_vec_minimum();

            print_text({
               "5. Постройте модель линейной регрессии с коэффициентом",
               "   регуляризации 0 и постройте график полученной функции",
               "   совместно с графиком из пункта 2. Почему регуляризация в",
               "   данном случае не сработает?"
            }, true);
            const auto functor
                = make_linreg_hyph_functor<double, NInputVars>(vec_min);
            print_data_plot<double>(
                {{"Learning sample", norm_fio}},
                {{"Linear regression hypothesis function", functor}},
                "Plot of model of learning sample with model with LAMBDA = 0",
                (out_dir / "03-1-02-learning-sample-and-model.pdf")
            );

            print_text({
               "6. Постройте график процесса обучения (learning curves) для",
               "   обучающей и валидационной выборки. По оси абсцисс",
               "   откладывается число элементов из обучающей выборки, а по",
               "   оси ординат - ошибка (значение функции потерь) для",
               "   обучающей выборки (первая кривая) и валидационной",
               "   выборки (вторая кривая). Какой вывод можно сделать по",
               "   построенному графику?"
            }, true);
            print_learning_curve_plot<double, NInputVars>(
                "Learning curve plot",
                (out_dir / "03-1-02-learning-validation-learning-curves.pdf"),
                {{"Learning sample", fio},
                 {"Validation sample", val_fio}},
                std::bind(linreg::calc_loss<double>, vec_min, _1, _2)
            );
        }

        {
            print_text({
                "9. Обучите модель с коэффициентом регуляризации 0 и p = 8."
            }, true);
            const auto maybe_min
                = linreg::calc_grad_descent<double, NNewInputVars>(
                    make_zero_vec<double>(NNewThetas),
                    norm_ext_x, y,
                    alpha<double>(0.1), epsilon<double>(1e-6)
                  );
            assert(maybe_min);
            const auto& min_vec = maybe_min->get_vec_minimum();
            maybe_min->print_info(
                "Normalized extended learning sample, lambda = 0, p = 8"
            );

            print_text({
                "10. Постройте график модели, совмещенный с обучающей",
                "    выборкой, а также график процесса обучения. Какой вывод",
                "    можно сделать в данном случае?"
            }, true);
            const auto functor
                = make_linreg_hyph_functor<double, NInputVars, NNewInputVars>(
                    min_vec,
                    polynom_degrees,
                    ext_avg, ext_rms
                 );
            print_data_plot<double>(
                {{"Learning sample", fio}},
                {{"Linear regression hypothesis function", functor}},
                "Plot of model for learning sample with LAMBDA = 0, P = "
                    + std::to_string(PolynomDegree),
                (out_dir / "03-1-03-learning-sample-model.pdf")
            );

            plot_helpers::print_learning_curve_plot<double, NNewInputVars>(
                "Learning curve plot with LAMBDA = 0, P = "
                    + std::to_string(PolynomDegree),
                (out_dir / "03-1-03-learning-sample-curve-model.pdf"),
                {{"Normalized extended learning sample", norm_ext_fio},
                 {"Normalized extended validation sample", norm_ext_val_fio}},
                std::bind(linreg::calc_loss<double>, std::cref(min_vec), _1, _2)
            );
        }

        {
            print_text({
                    "11. Постройте графики из пункта 10 для моделей с",
                    "    коэффициентами регуляризации 1 и 100. Какие выводы",
                    "    можно сделать?"
            }, true);
            for (const auto lambda_it: {1.0, 100.0}) {
                const auto maybe_minimum
                    = linreg::calc_grad_descent_l2<double, NNewInputVars>(
                        make_zero_vec<double>(NNewThetas), norm_ext_x,
                        norm_ext_y, alpha<double>(0.001),
                        lambda<double>(lambda_it), DEFAULT_EPSILONE
                      );
                assert(maybe_minimum);
                const auto& vec_min = maybe_minimum->get_vec_minimum();
                maybe_minimum->print_info(
                    "Normalized extended learning sample, lambda = "
                        + std::to_string(lambda_it)
                );

                const auto functor
                    = make_linreg_hyph_functor(
                        vec_min, polynom_degrees, ext_avg, ext_rms
                      );
                const auto data_plot_name
                    = "Plot of model for learning sample, lambda = "
                          + std::to_string(lambda_it);
                const auto path_to_data_plot = out_dir
                    / ("03-1-04-learning-sample-model-lambda-"
                       + std::to_string(lambda_it)
                       + ".pdf");
                print_data_plot<double>(
                    {{"Learning sample", fio},
                     {"Validation sample", val_fio}},
                    {{"Linear regression hypothesis function", functor}},
                    data_plot_name,
                    path_to_data_plot
                );

                const auto learning_curve_plot_name
                    = "Learning curve plot, lambda - "
                          + std::to_string(lambda_it);
                const auto path_to_learning_curve_plot = out_dir
                    / ("03-1-04-learning-sample-curve-lambda-"
                       + std::to_string(lambda_it)
                       + ".pdf");
                plot_helpers::print_learning_curve_plot<double, NNewInputVars>(
                    learning_curve_plot_name,
                    path_to_learning_curve_plot,
                    {{"Normalized extended learning sample", norm_ext_fio},
                     {"Normalized extended validation sample",
                      norm_ext_val_fio}},
                    std::bind(
                        linreg::calc_loss_l2<double>,
                        std::cref(vec_min), _1, _2,
                        ml::utils::lambda<double>(lambda_it)
                    )
                );
                print_marking_line();
            }
        }

        {
            print_text({
                "12. С помощью валидационной выборки подберите коэффиент",
                "    регуляризации, который позволяет достичь наименьшей",
                "    ошибки. Процесс подбора отразите с помощью графика",
                "    (графиков)."
            }, true);
            for (const auto lambda_it : {0.01, 0.1, 1.0, 4.0, 5.0, 10.0}) {
                const auto maybe_minimum
                    = linreg::calc_grad_descent_l2<double, NNewInputVars>(
                        make_zero_vec<double>(NNewThetas), norm_ext_x,
                        norm_ext_y, alpha<double>(0.001),
                        lambda<double>(lambda_it), DEFAULT_EPSILONE
                      );
                assert(maybe_minimum);
                const auto& vec_min = maybe_minimum->get_vec_minimum();
                maybe_minimum->print_info(
                    "Normalized extended learning sample, lambda = "
                        + std::to_string(lambda_it)
                );

                const auto functor
                    = make_linreg_hyph_functor(
                        vec_min, polynom_degrees, ext_avg, ext_rms
                      );
                const auto data_plot_name
                    = "Plot of model for validation sample, lambda = "
                          + std::to_string(lambda_it);
                const auto path_to_data_plot = out_dir
                    / ("03-1-05-validation-sample-model-lambda-"
                       + std::to_string(lambda_it)
                       + ".pdf");
                print_data_plot<double>(
                    {{"Validation sample", val_fio}},
                    {{"Linear regression hypothesis function", functor}},
                    data_plot_name,
                    path_to_data_plot
                );
                print_marking_line();

                std::cout
                    << "LOSS: "
                    << linreg::calc_loss_l2(vec_min,
                                            norm_ext_x_test,
                                            norm_ext_y_test,
                                            lambda<double>(lambda_it))
                    << std::endl;
            }
        }

        {
            print_text(
                {"13. Вычислите ошибку (потерю) на контрольной выборке"},
                true
            );

            const auto maybe_min =
                linreg::calc_grad_descent_l2<double, NNewInputVars>(
                    make_zero_vec<double>(NNewThetas),
                    norm_ext_x, norm_ext_y, alpha<double>(0.01),
                    ml::utils::lambda<double>(1), DEFAULT_EPSILONE
                );
            const auto& vec_min = maybe_min->get_vec_minimum();

            std::cout
                << "LOSS: "
                << linreg::calc_loss_l2(vec_min, norm_ext_x_test,
                                        norm_ext_y_test, lambda<double>(10))
                << std::endl;
        }
    }
}

int main(int argc, char* argv[])
{
    lab3::SetupRootSettings();

    const auto expected_opts_var_map = parse_arguments<lab3::ArgsLogicError>(
        argc, argv,
        lab3::check_args_logic,
        lab3::get_options_description,
        lab3::ArgsLogicErrExplanationMap
    );
    if (not expected_opts_var_map) {
        print_arguments_error(expected_opts_var_map.error());
        return to_underlying(
            lab3::ProgramReturnCode::ERROR_DURING_ARGS_PARSING
        );
    }

    const auto& opts_var_map = expected_opts_var_map.value();
    std::filesystem::path input_dir = opts_var_map[lab3::INPUT_DIR_OPT]
        .as<std::string>();
    std::filesystem::path output_dir = opts_var_map[lab3::OUTPUT_DIR_OPT]
        .as<std::string>();

    lab3::labwork(input_dir, output_dir);

    return 0;
}
