#include <algorithm>
#include <array>
#include <filesystem>
#include <functional>
#include <functional>
#include <iostream>
#include <locale>
#include <numeric>
#include <optional>
#include <string>
#include <variant>

#include <boost/program_options.hpp>
#include <rapidcsv.h>
#include <tl/expected.hpp>

#include <TAxis.h>
#include <TCanvas.h>
#include <TF1.h>
#include <TF2.h>
#include <TGaxis.h>
#include <TGraph2D.h>
#include <TGraph2DErrors.h>
#include <TGraphErrors.h>
#include <TLegend.h>
#include <TROOT.h>
#include <TRandom.h>
#include <TStyle.h>

#include <ml/utils/cmd_args_helpers.hpp>
#include <ml/utils/enum_helpers.hpp>
#include <ml/utils/io_helpers.hpp>
#include <ml/utils/plot_helpers.hpp>
#include <ml/utils/print_helpers.hpp>
#include <ml/utils/time_meter.hpp>

#include <ml/opdesc/grad_descent.hpp>
#include <ml/opdesc/linreg.hpp>

using namespace ml::utils;
namespace boost_po = boost::program_options;

namespace lab1
{
    using namespace ROOT;
    using namespace ml::opdesc;
    using namespace ml::opdesc::linreg;

    const auto INPUT_DIR_OPT  = "input_data_dir";
    const auto OUTPUT_DIR_OPT = "output_dir";
    const auto THETA_OPT  = "theta";
    const auto ALPHA_OPT  = "alpha";

    enum class ProgramReturnCode
    {
        FIRST                          = 0,
        ALL_IS_GOOD                    = 0,
        ERROR_DURING_ARGS_PARSING      = 1,
        ERROR_DURING_READING_FROM_FILE = 2,
        ERROR_DURING_OUTPUT_SAVING     = 3,
        LAST                           = 3
    };

    enum class ArgsLogicError
    {
        FIRST                   = 0,
        NO_INPUT_OR_OUTPUT_DIRS = 0,
        LAST                    = 0
    };
    const std::map<ArgsLogicError, std::string> ArgsLogicErrExplanationMap = {
        {
            ArgsLogicError::NO_INPUT_OR_OUTPUT_DIRS,
            "Use --input_data_dir and --output_dir options."
        }
    };

    static const std::map<ProgramReturnCode, std::string>
    ReturnValExplanationMap = {
        { ProgramReturnCode::ALL_IS_GOOD, "All is good" },
    };

    boost_po::options_description get_options_description()
    {
        boost_po::options_description opts_desc;

        opts_desc.add_options()
            (
                INPUT_DIR_OPT,
                boost_po::value<std::string>(),
                "Path to directory with input data"
            )
            (
                OUTPUT_DIR_OPT,
                boost_po::value<std::string>(),
                "Path to directory with output data"
            )
            (
                THETA_OPT,
                boost_po::value<std::vector<double>>()->multitoken(),
                "Theta coefficients"
            )
            (
                ALPHA_OPT,
                boost_po::value<double>(),
                "Alpha coefficient"
            );

        return opts_desc;
    }

    std::optional<ArgsLogicError> check_args_logic(
        const boost_po::variables_map& var_map
    )
    {
        const auto is_input_dir_setuped = var_map.count(INPUT_DIR_OPT) != 0;
        const auto is_output_dir_setuped = var_map.count(OUTPUT_DIR_OPT) != 0;

        if (is_input_dir_setuped && is_output_dir_setuped)
        {
            return std::nullopt;
        }

        return ArgsLogicError::NO_INPUT_OR_OUTPUT_DIRS;
    }

    void SetupRootSettings()
    {
        // Remove useless notifications
        GetROOT()->ProcessLine("gErrorIgnoreLevel = 1001;");

        gStyle->SetPalette(55);

        std::cout << std::fixed << std::setprecision(3);
    }

    const auto SURF_POINTS_COUNT = 100000;

    template<typename T, std::size_t NInputVars>
    std::optional<FuncIOVariants<T, NInputVars>> load_data(
        std::filesystem::path csv
    )
    {
        try {
            auto maybe_reader = CSVFileReader::load_file_without_labels(csv);
            if (not maybe_reader) {
                std::cout << "Can't continue. File <" << csv
                          << "> not exists" << std::endl << std::endl;
                return std::nullopt;
            }

            auto maybe_variants = maybe_reader->get_variants<T, NInputVars>();
            if (not maybe_variants) {
                std::cout
                    << "Can't read function input/output variants from <"
                    << csv << "> file." << std::endl << std::endl;
                return std::nullopt;
            }

            std::cout << "File <" << csv
                      << "> successfully loaded!" << std::endl << std::endl;
            return FuncIOVariants<T, NInputVars>(std::move(*maybe_variants));
        } catch (const std::ios_base::failure& ex) {
            return std::nullopt;
        }
    }

    void print_1st_plot(
        const FuncIOVariants<double, 1>& dots,
        const std::filesystem::path& out_dir
    )
    {
        TCanvas canvas("canvas1", "", 700, 700);
        canvas.SetGrid(1, 1);

        TGraph graph;
        graph.SetMarkerColor(kBlue);
        graph.SetMarkerStyle(500);
        set_points_with_view_fix<double, TGraph, 2>(graph, dots);

        graph.Draw("SAME AP*");
        canvas.Draw();
        canvas.Print(out_dir.c_str());
        std::cout << "First plot saved" << std::endl;
    }

    void print_2nd_plot(
        const FuncIOVariants<double, 1>& data,
        const MinimumDesc<double, 2>& theta_descent,
        const std::filesystem::path& out_dir
    )
    {
        TCanvas canvas("canvas1", "", 700, 700);
        canvas.SetGrid(1, 1);

        TGraph graph;
        graph.SetMarkerColor(kBlue);
        graph.SetMarkerStyle(500);
        set_points_with_view_fix<double, TGraph, 2>(graph, data);

        TF1 func("h_0(x)", "[0] + x * [1]", -10, 35);
        const auto final_theta = theta_descent.get_vec_minimum();
        func.SetParameter(0, final_theta[0]);
        func.SetParameter(1, final_theta[1]);

        graph.Draw("AP*");
        func.Draw("SAME");
        canvas.Draw();
        canvas.Print(out_dir.c_str());
        std::cout << "Second plot saved" << std::endl;
    }

    void print_3rd_plot(
        const FuncIOVariants<double, 1>& data,
        const MinimumDesc<double, 2>& theta_descent,
        const std::filesystem::path& out_dir
    )
    {
        TCanvas canvas("canvas1", "", 700, 700);
        canvas.SetGrid(1, 1);
        canvas.SetTheta(45.0);
        canvas.SetPhi(5.0);

        TGraph2D surf_graph;
        set_random_points<double, TGraph2D, 3>(
            surf_graph,
            SURF_POINTS_COUNT,
            [&data](theta_arr<double, 2> thetas) {
                return calc_loss(thetas, data);
            }
        );
        surf_graph.Draw("SURF1");

        TGraph2D path_graph;
        path_graph.SetMarkerColor(kRed);
        path_graph.SetMarkerStyle(500);

        const auto minimum = theta_descent.get_arr_minimum();
        const auto J = linreg::calc_loss(minimum, data);
        path_graph.SetPoint(0, minimum[0], minimum[1], J);
        path_graph.Draw("SAME P0");

        canvas.Draw();
        canvas.Print(out_dir.c_str());
        std::cout << "Third plot saved" << std::endl;
    }

    void print_4th_plot(
        const FuncIOVariants<double, 1>& data,
        const MinimumDesc<double, 2>& theta_descent,
        const std::filesystem::path& out_dir
    )
    {
        TCanvas canvas("canvas1", "", 700, 700);
        canvas.SetGrid(1, 1);
        canvas.SetTheta(45.0);
        canvas.SetPhi(5.0);

        TGraph2D surf_graph;
        set_random_points<double, TGraph2D, 3>(
            surf_graph,
            SURF_POINTS_COUNT,
            [&data](theta_arr<double, 2> thetas) {
                return calc_loss(thetas, data);
            }
        );
        surf_graph.Draw("CONT1");

        TGraph points2d;
        points2d.SetMarkerColor(kBlue);
        points2d.SetMarkerStyle(500);
        const auto& final_theta = theta_descent.get_vec_minimum();
        points2d.SetPoint(0, final_theta[0], final_theta[1]);
        points2d.Draw("SAME *");

        canvas.Draw();
        canvas.Print(out_dir.c_str());
        std::cout << "Fourth plot saved" << std::endl;
    }

    void first_labwork_part(
        const std::filesystem::path& input_dir,
        const std::filesystem::path& out_dir
    )
    {
        const auto path_to_file = input_dir / "ex1data1.txt";
        const auto maybe_func_io = load_data<double, 1>(path_to_file);
        if (not maybe_func_io) {
            return;
        }
        const auto& func_io = maybe_func_io.value();


        const auto maybe_thetas_descent = calc_grad_descent<double, 1>(
            theta_arr<double, 2>({0, 0}),
            func_io,
            alpha<double>(0.001)
        );
        if (not maybe_thetas_descent.has_value()) {
            return;
        }
        const auto& thetas_descent = maybe_thetas_descent.value();
        thetas_descent.print_info("1.1");


        const auto maybe_thetas_descent_vec = calc_grad_descent<double, 2>(
            make_vec<double>({0, 0}),
            func_io.get_x_matrix(),
            func_io.get_y_vector(),
            non_zero_value<double>(0.001)
        );
        if (not maybe_thetas_descent_vec.has_value()) {
            return;
        }
        const auto& thetas_descent_vec = maybe_thetas_descent_vec.value();
        thetas_descent_vec.print_info("1.1");

        const auto thetas_descent_analytic
            = calc_grad_descent_analytically<double, 1>(
                func_io.get_x_matrix(),
                func_io.get_y_vector()
              );
        thetas_descent_analytic.print_info("1.1");

        print_1st_plot(func_io, (out_dir / "01-1-01_plot.pdf"));
        print_2nd_plot(func_io, thetas_descent,
                       (out_dir / "01-1-02_plot.pdf"));
        print_3rd_plot(func_io, thetas_descent,
                       (out_dir / "01-1-03_plot.pdf"));
        print_4th_plot(func_io, thetas_descent,
                       (out_dir / "01-1-04_plot.pdf"));
    }


    void second_labwork_part(
        const std::filesystem::path& input_dir,
        const std::filesystem::path& output_dir
    )
    {
        const auto path_to_file = input_dir / "ex1data2.txt";
        const auto maybe_func_io = load_data<double, 2>(path_to_file);
        if (not maybe_func_io) {
            return;
        }
        const auto& func_io = maybe_func_io.value();


        const auto maybe_first_descent = calc_grad_descent<double, 2>(
            theta_arr<double, 3>({89590, 120, -8700}),
            func_io,
            alpha<double>(0.0000001)
        );
        if (not maybe_first_descent.has_value()) {
            return;
        }
        maybe_first_descent->print_info("2.1");


        auto norm_func_io = normalize_data(func_io);
        std::cout << "Data normalized" << std::endl;


        const auto maybe_2nd_descent = calc_grad_descent<double, 2>(
            theta_arr<double, 3>({0, 0, 0}),
            norm_func_io,
            non_zero_value<double>(0.001)
        );
        if (not maybe_2nd_descent.has_value()) {
            return;
        }
        maybe_2nd_descent->print_info("2.2 for normalized data");


        const auto maybe_vec_2nd_descent = calc_grad_descent<double, 2>(
            make_zero_vec<double>(3),
            norm_func_io.get_x_matrix(),
            norm_func_io.get_y_vector(),
            non_zero_value<double>(0.001)
        );
        if (not maybe_vec_2nd_descent.has_value()) {
            return;
        }
        maybe_vec_2nd_descent->print_info("2.2 for normalized data");


        const auto third_descent = calc_grad_descent_analytically<double, 2>(
            func_io.get_x_matrix(),
            func_io.get_y_vector()
        );
        third_descent.print_info("2.3");


        const auto fourth_descent = calc_grad_descent_analytically<double, 2>(
            norm_func_io.get_x_matrix(),
            norm_func_io.get_y_vector()
        );
        fourth_descent.print_info("2.4 for normalized data");
    }
}

int main(int argc, char* argv[])
{
    lab1::SetupRootSettings();

    const auto expected_opts_var_map = parse_arguments<lab1::ArgsLogicError>(
        argc, argv,
        lab1::check_args_logic,
        lab1::get_options_description,
        lab1::ArgsLogicErrExplanationMap
    );
    if (not expected_opts_var_map) {
        print_arguments_error(expected_opts_var_map.error());
        return to_underlying(
            lab1::ProgramReturnCode::ERROR_DURING_ARGS_PARSING
        );
    }

    char kek[] = "asasdsad";


    const auto& opts_var_map = expected_opts_var_map.value();
    std::filesystem::path input_dir = opts_var_map[lab1::INPUT_DIR_OPT]
        .as<std::string>();
    std::filesystem::path output_dir = opts_var_map[lab1::OUTPUT_DIR_OPT]
        .as<std::string>();

    lab1::first_labwork_part(input_dir, output_dir);
    print_marking_line();
    lab1::second_labwork_part(input_dir, output_dir);

    return 0;
}
