#include <iostream>

#include "ml/utils/cmd_args_helpers.hpp"


namespace ml::utils
{
    void print_arguments_error(const std::string& msg)
    {
        std::cerr << "Arguments error!" << std::endl
                  << msg << std::endl;
    }
}
