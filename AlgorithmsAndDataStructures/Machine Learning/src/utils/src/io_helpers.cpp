#include <ml/utils/io_helpers.hpp>

#include <functional>
#include <tuple>
#include <utility>
#include <filesystem>
#include <memory>

#include <matio.h>

namespace ml::utils
{
    std::optional<CSVFileReader> CSVFileReader::_load_csv_file(
        const std::filesystem::path& path_to_file,
        rapidcsv::LabelParams label_params
    )
    {
        if (not std::filesystem::exists(path_to_file)) {
            return std::nullopt;
        }

        return CSVFileReader(
            rapidcsv::Document(path_to_file, std::move(label_params))
        );
    }

    std::optional<CSVFileReader> CSVFileReader::load_file(
        const std::filesystem::path& path_to_file,
        const std::size_t col_name_idx,
        const std::size_t row_name_idx
    )
    {
        return _load_csv_file(
            path_to_file,
            rapidcsv::LabelParams(
                static_cast<int>(col_name_idx),
                static_cast<int>(row_name_idx)
            )
        );
    }

    std::optional<CSVFileReader> CSVFileReader::load_file_without_labels(
        const std::filesystem::path& path_to_file
    )
    {
        return _load_csv_file(path_to_file, rapidcsv::LabelParams(-1, -1));
    }

    std::optional<MatFileReader> MatFileReader::load_file(
        const std::filesystem::path& path_to_mat
    )
    {
        mat_t* const mat = Mat_Open(path_to_mat.c_str(), MAT_ACC_RDONLY);
        if (mat == nullptr) {
            return std::nullopt;
        }

        return std::make_optional(MatFileReader(mat));
    }

    std::vector<std::string> MatFileReader::get_variables() const
    {
        std::vector<std::string> result;

        matvar_t* matvar = nullptr;
        while ((matvar = Mat_VarReadNextInfo(this->_file)) != NULL) {
            result.emplace_back(matvar->name);
            Mat_VarFree(matvar);
            matvar = NULL;
        }

        return result;
    }
}
