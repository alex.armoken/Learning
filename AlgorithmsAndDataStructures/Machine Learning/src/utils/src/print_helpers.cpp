#include <ml/utils/print_helpers.hpp>

#include <sys/ioctl.h>
#include <unistd.h>

namespace ml::utils
{
    void print_marking_line(const std::string marker)
    {
        struct winsize size;
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);

        for (std::size_t _ = 0; _ < size.ws_col; ++_) {
            std::cout << marker;
        }
        std::cout << std::endl;
    }

    void print_text(const std::initializer_list<std::string> lines,
                    const bool print_border)
    {
        const auto marker = "■";
        if (print_border) {
            print_marking_line(marker);
        }

        for (const auto& line: lines) {
            std::cout << line << std::endl;
        }

        if (print_border) {
            print_marking_line(marker);
        }
    }
}  // namespace ml::utils
