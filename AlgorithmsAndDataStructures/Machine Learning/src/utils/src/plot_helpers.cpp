#include <ml/utils/plot_helpers.hpp>

#include <functional>
#include <regex>
#include <tuple>
#include <utility>

#include <TCanvas.h>
#include <TGraph.h>


namespace ml::utils
{
    const std::vector<Color_t> COLORS = {
        EColor::kBlue,
        EColor::kOrange,
        EColor::kGreen,
        EColor::kViolet,
        EColor::kYellow,
        EColor::kMagenta,
        EColor::kPink,
        EColor::kSpring,
        EColor::kTeal,
        EColor::kCyan,
        EColor::kAzure,
        EColor::kGray,
        EColor::kGray + 1,
        EColor::kGray + 3
    };

    std::array<TAxis*, 2> get_axes(const TGraph& graph)
    {
        return { graph.GetXaxis(), graph.GetYaxis() };
    }

    std::array<TAxis*, 3> get_axes(const TGraph2D& graph)
    {
        return { graph.GetXaxis(), graph.GetYaxis(), graph.GetZaxis() };
    }

    void save_plot(TCanvas& canvas,
                   const std::string& plot_name,
                   const std::filesystem::path& out_file)
    {
        canvas.Draw();
        canvas.Print(out_file.c_str());
        const auto absolue_path = std::filesystem::current_path()
            / out_file.lexically_normal();
        std::cout << plot_name
                  << " saved to " << std::endl << "\t"
                  << "file://" << std::regex_replace(absolue_path.string(),
                                                     std::regex(" "), "%20")
                  << std::endl;
    }

    void set_default_function_conf(TF1& func, const std::size_t color_idx)
    {
        assertm(COLORS.size() > color_idx, "Can't get new colors");

        const auto color = COLORS[color_idx];
        func.SetMarkerColor(color);
    }
}
