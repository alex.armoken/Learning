#if !(defined(ML_EXCEPTIONS_H))
#define ML_EXCEPTIONS_H

#include <stdexcept>
#include <array>
#include <cmath>
#include <cassert>

#include <TVectorT.h>

// Use (void) to silent unused warnings.
#define assertm(exp, msg) assert(((void)msg, exp))

namespace ml::utils
{
    class NotImplementedException : public std::logic_error
    {
    public:
        NotImplementedException()
            : std::logic_error("Function not yet implemented.")
        {
        }
    };

    class NanException : public std::logic_error
    {
    public:
        NanException()
            : std::logic_error("During calculations, a NaN happend!")
        {
        }
    };


    template<typename T, std::size_t Size>
    bool is_contain_nan(const Array<T, Size>& arr)
    {
        for (std::size_t i = 0; i < Size; ++i) {
            if (std::isnan(arr[i])) {
                return true;
            }
        }

        return false;
    }

    template<typename T>
    bool is_contain_nan(const TVectorT<T>& vec)
    {
        const int rows_count = vec.GetNrows();
        for (int i = 0; i < rows_count; ++i) {
            if (std::isnan(vec[i])) {
                return true;
            }
        }

        return false;
    }

    template<typename T>
    bool is_contain_nan(const TMatrixT<T>& m)
    {
        const auto rows_count = static_cast<std::size_t>(m.GetNrows());
        const auto cols_count = static_cast<std::size_t>(m.GetNcols());

        for (std::size_t i = 0; i < rows_count; ++i) {
            for (std::size_t j = 0; j < cols_count; ++j) {
                if (std::isnan(m(i, j))) {
                    return true;
                }
            }
        }

        return false;
    }
}
#endif // ML_EXCEPTIONS_H
