#if !(defined(ML_HELPER_TYPES_H))
#define ML_HELPER_TYPES_H
#include <cassert>
#include <numeric>

#include <TVectorT.h>
#include <TMatrixT.h>
#include <type_safe/integer.hpp>
#include <type_safe/strong_typedef.hpp>
#include <type_safe/constrained_type.hpp>

namespace ml::utils
{
    namespace ts = type_safe;


    template<typename T>
    using non_zero_value = ts::constrained_type<T, ts::constraints::non_default>;

    using nfirst = ts::constrained_type<std::size_t, ts::constraints::non_default>;
    using ndimensions = ts::constrained_type<uint8_t, ts::constraints::non_default>;


    template<typename T = double>
    using alpha = non_zero_value<T>;

    template<typename T = double>
    using lambda = non_zero_value<T>;

    template<typename T = double>
    using beta = non_zero_value<T>;

    template<typename T = double>
    using epsilon = non_zero_value<T>;

    template<typename T = double>
    using x_matrix = TMatrixT<T>;

    template<typename T = double>
    using x_vec = TVectorT<T>;

    template<typename T = double>
    using y_matrix = TMatrixT<T>;

    template<typename T = double>
    using y_vec = TVectorT<T>;
}
#endif // ML_HELPER_TYPES_H
