#if !(defined(ML_TEMPLATE_HELPERS_H))
#define ML_TEMPLATE_HELPERS_H

#include <type_traits>

namespace ml::utils
{
    template<typename T>
    constexpr bool is_even(const T number)
    {
        static_assert(std::is_arithmetic<T>::value,
                      "Not arithmetic type");

        return number % 2 == 0;
    }

    template<typename T>
    constexpr bool is_odd(const T number)
    {
        return not is_even(number);
    }
}  // namespace ml::utils
#endif // ML_TEMPLATE_HELPERS_H
