#if !(defined(ML_PLOT_HELPERS_H))
#define ML_PLOT_HELPERS_H

#include <algorithm>
#include <array>
#include <functional>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <TF1.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TLegend.h>
#include <TROOT.h>
#include <TRandom.h>

#include <ml/utils/io_helpers.hpp>

namespace ml::utils
{
    using namespace std::literals::string_literals;

    enum class CanvasState
    {
        Clean = 0,
        SomethingIsAlreadyDrawn
    };

    enum class AxisState
    {
        Needed = 0,
        NotNeeded
    };

    extern const std::vector<Color_t> COLORS;

    template<typename T>
    class RealTimeLossPlotBuilder
    {
    public:
        using Func = std::function<T(const TVectorT<T>&)>;

        RealTimeLossPlotBuilder(Func update_state_func)
            : _update_state_func(update_state_func)
        {
            _canvas.Show();
            _canvas.cd(0);
            _canvas.SetCanvasSize(1500, 1500);
            _canvas.SetWindowSize(500, 500);
        }

        void update(const TVectorT<T>& vec)
        {
            const auto value = _update_state_func(vec);
            _graph.SetPoint(_dots_count, _dots_count, value);
            _graph.Draw();
            _canvas.Draw();

            _dots_count++;
        }

    private:
        TCanvas _canvas;
        TGraph _graph;
        Func _update_state_func;
        std::size_t _dots_count = 0;
    };

    using vec_of_funcs = std::vector<std::function<void()>>;

    template<typename T, class G, std::size_t... I,
             // Not for public using
             std::size_t NInputVars = sizeof...(I) - 1>
    void set_points_impl(G& graph,
                         const FuncIOVariants<T, NInputVars>& points,
                         const std::size_t point_idx_offset,
                         std::index_sequence<I...>)
    {
        for (std::size_t i = 0; i < points.get_variants_count(); ++i) {
            const auto& point = points[i];
            graph.SetPoint(static_cast<Int_t>(point_idx_offset + i),
                           point.in(I)..., point.out());
        }
    }

    template<typename T, class G, std::size_t NInputVars,
             typename Indices = std::make_index_sequence<NInputVars>>
    void set_points(G& graph,
                    const FuncIOVariants<T, NInputVars>& points,
                    const std::size_t point_idx_offset = 0)
    {
        set_points_impl(graph, points, point_idx_offset, Indices{});
    }


    std::array<TAxis*, 2> get_axes(const TGraph& graph);
    std::array<TAxis*, 3> get_axes(const TGraph2D& graph);


    template <typename T, class G, std::size_t NInputVars>
    void fix_view(G& graph,
                  const FuncIOVariants<T, NInputVars>& points,
                  const T offset = 10)
    {
        auto axes = get_axes(graph);
        for (std::size_t i = 0; i < axes.size(); ++i) {
            auto* axis = axes[i];

            const auto min_val = points.get_minimal(i);
            const auto max_val = points.get_maximal(i);
            axis->SetLimits(min_val - offset, max_val + offset);

            axis->UnZoom();
        }
    }

    template <typename T, class G, std::size_t NInputVars>
    void set_points_with_view_fix(G& graph,
                                  const FuncIOVariants<T, NInputVars>& points)
    {
        set_points(graph, points);
        fix_view(graph, points);
    }


    template<typename T, typename G, std::size_t NInputVars, std::size_t... I>
    void set_random_points_impl(
        G& graph, const int points_count,
        const std::function<T(Array<T, NInputVars>)>& func,
        std::index_sequence<I...>
    )
    {
        TRandom rnd;
        constexpr T P = 20.0;
        constexpr auto DimCount = sizeof...(I) + 1;
        for (int point_idx = 0; point_idx < points_count; ++point_idx) {
            Array<T, NInputVars> thetas = { (2 * P * (rnd.Rndm(I)) - P)..., };
            const auto y = func(thetas);

            graph.SetPoint(point_idx, thetas[I]..., y);
        }
    }

    template<typename T, typename G, std::size_t NInputVars,
             typename Indices = std::make_index_sequence<NInputVars>>
    void set_random_points(
        G& graph, const std::size_t points_count,
        const std::function<double(Array<T, NInputVars>)>& func
    )
    {
        set_random_points_impl(graph, static_cast<int>(points_count),
                               func, Indices{});
    }

    void save_plot(TCanvas& canvas,
                   const std::string& plot_name,
                   const std::filesystem::path& out_file);


    template <typename T, std::size_t NInputVars>
    struct NamedData
    {
        const std::string name;
        const FuncIOVariants<T, NInputVars>& data;
    };

    template<typename T, std::size_t NInputVars>
    T get_minimal(const std::vector<NamedData<T, NInputVars>>& dots,
                  const std::size_t input_output_var_idx)
    {
        T result = dots.front().data.get_minimal(input_output_var_idx);
        for (std::size_t i = 1; i < dots.size(); ++i) {
            result = std::min(
                result,
                dots[i].data.get_minimal(input_output_var_idx)
            );
        }

        return result;
    }

    template<typename T, std::size_t NInputVars>
    T get_maximal(const std::vector<NamedData<T, NInputVars>>& dots,
                  const std::size_t input_output_var_idx)
    {
        T result = dots.front().data.get_maximal(input_output_var_idx);
        for (std::size_t i = 1; i < dots.size(); ++i) {
            result = std::max(
                result,
                dots[i].data.get_minimal(input_output_var_idx)
            );
        }

        return result;
    }


    template <typename T>
    struct FuncData
    {
        const std::string name;
        const std::function<T(T*, T*)> func;
    };


    template<typename T>
    void draw_objects(
        std::vector<T>& objects,
        const std::string options,
        const CanvasState canvas_state = CanvasState::Clean,
        const AxisState axis_state = AxisState::Needed
    )
    {
        const auto same_opt = options.empty() ? "SAME" : "SAME ";
        for (std::size_t i = 0; i < objects.size(); ++i) {
            if (i == 0) {
                const auto obj_options =
                    (canvas_state == CanvasState::SomethingIsAlreadyDrawn
                     ? same_opt
                     : ""s)
                    + (axis_state == AxisState::Needed ? "A"s : ""s)
                    + options;
                objects[i].Draw(obj_options.c_str());
            } else {
                const auto obj_options = same_opt + options;
                objects[i].Draw(obj_options.c_str());
            }
        }
    }


    template<typename T>
    void set_default_graph_conf(T& graph, const std::size_t color_idx = 0)
    {
        assertm(COLORS.size() > color_idx, "Can't get new colors");

        const auto color = COLORS[color_idx];
        graph.SetMarkerColor(color);
        graph.SetLineColor(color);
        graph.SetMarkerStyle(3);
    }

    void set_default_function_conf(TF1& func, const std::size_t color_idx = 0);


    template<typename T, std::size_t NInputVars>
    std::vector<TGraph> draw_graphs(
        const std::vector<NamedData<T, NInputVars>>& dots,
        const CanvasState canvas_state = CanvasState::Clean,
        const AxisState axis_state = AxisState::NotNeeded,
        const std::size_t points_offset = 0,
        const std::size_t obj_offset = 0
    )
    {
        std::vector<TGraph> graphs;
        std::size_t point_start_idx = points_offset;
        for (std::size_t i = 0; i < dots.size(); ++i) {
            graphs.emplace_back();
            auto& graph = graphs.back();
            set_default_graph_conf(graph, obj_offset + i);
            fix_view(graph, dots[i].data);

            set_points(graph, dots[i].data);
            point_start_idx += dots[i].data.get_variants_count();
        }
        draw_objects(graphs, "P", canvas_state, axis_state);

        return graphs;
    }

    template<typename T>
    std::vector<TF1> draw_functions(
        const T min_val, const T max_val,
        const std::vector<FuncData<T>>& funcs_data,
        const CanvasState canvas_state = CanvasState::Clean,
        const AxisState axis_state = AxisState::NotNeeded,
        const std::size_t color_offset = 0,
        const std::size_t obj_offset = 0
    )
    {
        std::vector<TF1> functions;
        for (std::size_t i = 0; i < funcs_data.size(); ++i) {
            const auto func_name = "func_" + std::to_string(obj_offset + i);
            functions.emplace_back(func_name.c_str(),
                                   funcs_data[i].func,
                                   min_val, max_val, 0, 1);

            set_default_function_conf(functions.back(), color_offset + i);
        }
        draw_objects(functions, "", canvas_state, axis_state);

        return functions;
    }

    template<typename T, std::size_t NInputVars>
    void add_legend_entries(TLegend& legend,
                            const std::vector<TGraph>& graphs,
                            const std::vector<NamedData<T, NInputVars>>& dots,
                            const std::size_t obj_offset = 0)
    {
        assert(graphs.size() == dots.size());

        for (std::size_t i = 0; i < dots.size(); ++i) {
            const auto& graph = graphs[i];

            auto* const learning_sample_entry = legend.AddEntry(
                ("data_" + std::to_string(i + obj_offset)).c_str(),
                ("Dots from " + dots[i].name).c_str(),
                "l"
            );
            learning_sample_entry->SetTextColor(graph.GetMarkerColor());
        }
    }

    template<typename T>
    void add_legend_entries(TLegend& legend,
                            const std::vector<TF1>& functions,
                            const std::vector<FuncData<T>>& funcs_data,
                            const std::size_t obj_offset = 0)
    {
        assert(functions.size() == funcs_data.size());

        for (std::size_t i = 0; i < functions.size(); ++i) {
            const auto& function = functions[i];

            const auto func_name = "func_" + std::to_string(i + obj_offset);
            auto* const learning_sample_entry = legend.AddEntry(
                ("legend_" + func_name).c_str(),
                (funcs_data[i].name + " function").c_str(),
                "l"
            );
            learning_sample_entry->SetTextColor(function.GetLineColor());
        }
    }


    template<typename T>
    void print_data_plot(const std::vector<NamedData<T, 1>>& dots,
                         const std::string& plot_name,
                         const std::filesystem::path& out_file)
    {
        TCanvas canvas("Canvas", "", 700, 700);
        canvas.SetGrid(1, 1);

        const std::vector<TGraph> graphs = draw_graphs(dots,
                                                       CanvasState::Clean,
                                                       AxisState::Needed);

        TLegend legend(0.5, 0.8, 0.9, 0.9);
        legend.SetHeader(plot_name.c_str());
        add_legend_entries(legend, graphs, dots);
        legend.Draw();

        save_plot(canvas, plot_name, out_file);
    }

    template<typename T>
    void print_data_plot(const std::vector<NamedData<T, 1>>& dots,
                         const std::vector<FuncData<T>>& funcs_data,
                         const std::string& plot_name,
                         const std::filesystem::path& out_file)
    {
        TCanvas canvas("Canvas", "", 700, 700);
        canvas.SetGrid(1, 1);

        const std::vector<TGraph> graphs = draw_graphs(
            dots,
            CanvasState::Clean,
            AxisState::Needed
        );

        const std::vector<TF1> functions = draw_functions(
            get_minimal(dots, 0) - 0.5, get_maximal(dots, 0) + 0.5,
            funcs_data,
            CanvasState::SomethingIsAlreadyDrawn,
            AxisState::NotNeeded
        );

        TLegend legend(0.5, 0.8, 0.9, 0.9);
        legend.SetHeader(plot_name.c_str());
        add_legend_entries(legend, graphs, dots);
        add_legend_entries(legend, functions, funcs_data);
        legend.Draw();

        save_plot(canvas, plot_name, out_file);
    }
}
#endif // ML_PLOT_HELPERS_H
