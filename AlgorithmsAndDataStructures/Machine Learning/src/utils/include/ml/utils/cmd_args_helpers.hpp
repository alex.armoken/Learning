#if !(defined(ML_CMD_ARGS_HELPERS_H))
#define ML_CMD_ARGS_HELPERS_H

#include <functional>
#include <optional>
#include <type_traits>

#include <tl/expected.hpp>
#include <boost/program_options.hpp>

namespace ml::utils
{
    namespace boost_po = boost::program_options;

    template <typename T>
    using ArgsLogicCheckerFunc = std::function<std::optional<T>(
        const boost_po::variables_map&
    )>;

    using OptDescProviderFunc = std::function<boost_po::options_description()>;

    template <typename T>
    tl::expected<boost_po::variables_map, std::string> parse_arguments(
        const int argc, char * argv[],
        const ArgsLogicCheckerFunc<T>& logic_checker,
        const OptDescProviderFunc& desc_provider,
        const std::map<T, std::string>& args_error_meaning_map
    )
    {
        static_assert(std::is_enum<T>::value, "Only enum");

        auto opts_desc = desc_provider();
        boost_po::parse_command_line(argc, argv, opts_desc);

        boost_po::variables_map var_map;
        try {
            auto parsed_opts = boost_po::command_line_parser(argc, argv)
                .options(opts_desc)
                .run();
            boost_po::store(parsed_opts, var_map);
        } catch (const boost_po::error &err) {
            return tl::make_unexpected(err.what());
        }

        const auto maybe_error = logic_checker(var_map);
        if (maybe_error) {
            return tl::make_unexpected(
                args_error_meaning_map.at(*maybe_error)
            );
        }

        return var_map;
    }

    void print_arguments_error(const std::string& msg);
} // namespace ml::utils
#endif // ML_CMD_ARGS_HELPERS_H
