#if !(defined(ML_NUM_HELPERS_H))
#define ML_NUM_HELPERS_H

#include <sstream>
#include <iomanip>
#include <cmath>

namespace ml::utils
{
    template <typename T>
    std::string int_to_hex(const T i,
                           const bool use_prefix = true,
                           const std::size_t width = sizeof(T) * 2)
    {
        std::stringstream stream;
        if (use_prefix) {
            stream << "0x";
        }
        stream << std::setfill('0')
               << std::setw(static_cast<int>(width))
               << std::hex
               << i;

        return stream.str();
    }

    template<typename T, typename U>
    T round_and_cast(const U value)
    {
        static_assert(std::is_floating_point<U>::value);

        return static_cast<T>(std::round(value));
    }
}  // namespace ml::utils
#endif // ML_NUM_HELPERS_H
