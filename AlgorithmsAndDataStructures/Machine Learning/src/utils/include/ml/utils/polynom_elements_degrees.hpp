#if !(defined(ML_POLYNOM_ELEMENTS_DEGREES_HELPERS_H))
#define ML_POLYNOM_ELEMENTS_DEGREES_HELPERS_H
#include <vector>
#include <cassert>
#include <numeric>
#include <type_traits>

#include <boost/iterator/iterator_facade.hpp>

#include "./array.hpp"
#include "./exceptions.hpp"

namespace ml::utils
{
    template<std::size_t VarsCount>
    using monomial_degrees = Array<std::size_t, VarsCount>;

    template<std::size_t VarsCount>
    using polynom_degrees = std::vector<monomial_degrees<VarsCount>>;


    constexpr std::size_t factorial(std::size_t n) noexcept
    {
        return n > 0 ? n * factorial(n - 1) : 1;
    }

    constexpr std::size_t combinations(std::size_t n, std::size_t k) noexcept
    {
        return factorial(n) / (factorial(k) * factorial(n - k));
    }

    constexpr std::size_t combinations_with_repetitions(
        std::size_t n, std::size_t k
    ) noexcept
    {
        return factorial(n) / (factorial(k) * factorial(n - k));
    }

    constexpr std::size_t polynom_combinations(
        std::size_t vars_count,
        std::size_t degree
    ) noexcept
    {
        return combinations(vars_count + degree, degree);
    }

    template<typename T, std::size_t NElements>
    void make_combinations(
        T* const result,
        const T* const input,
        const polynom_degrees<NElements>& degrees
    )
    {
        for (std::size_t i = 0; i < degrees.size(); ++i) {
            T combination(1);
            for (std::size_t el_idx = 0; el_idx < NElements; ++el_idx) {
                combination *= std::pow(input[el_idx], degrees[i][el_idx]);
            }

            result[i] = combination;
        }
    }

    template<typename T, std::size_t NElements>
    std::vector<T> make_combinations_arr(
        const Array<T, NElements>& els,
        const polynom_degrees<NElements>& degrees
    )
    {
        std::vector<T> result(degrees.size());
        make_combinations(result.data(), els.data(), degrees);

        return result;
    }

    template<typename T, std::size_t NEls>
    TVectorT<T> make_combinations_vec(const Array<T, NEls>& els,
                                      const polynom_degrees<NEls>& degrees)
    {
        TVectorT<T> result(static_cast<Int_t>(degrees.size()));
        make_combinations(result.GetMatrixArray(), els.data(), degrees);

        return result;
    }

    template<typename T, std::size_t NEls>
    TVectorT<T> make_combinations(const TVectorT<T>& vec,
                                  const polynom_degrees<NEls>& degrees)
    {
        TVectorT<T> result(static_cast<Int_t>(degrees.size()));
        make_combinations(result.GetMatrixArray(), vec.GetMatrixArray(),
                          degrees);

        return result;
    }


    template<std::size_t NVars>
    class PolynomElementsDegreesConstIter
        : public boost::iterator_facade<
                     PolynomElementsDegreesConstIter<NVars>,
                     const monomial_degrees<NVars>,
                     boost::forward_traversal_tag>
    {
    public:
        PolynomElementsDegreesConstIter(
            const std::size_t max_degree,
            const std::size_t combinations_count
        ): _cur_idx(0)
         , _degrees(0)
         , _max_degree(max_degree)
         , _combinations_count(combinations_count)
        {
        }

        PolynomElementsDegreesConstIter(
            const std::size_t cur_idx,
            const std::size_t max_degree,
            const std::size_t combinations_count
        ): _cur_idx(cur_idx)
         , _degrees(0)
         , _max_degree(max_degree)
         , _combinations_count(combinations_count)
        {
            if (cur_idx != _combinations_count) {
                for (std::size_t i = 0; i <= cur_idx; ++i) {
                    increment();
                }
            }
        }

        PolynomElementsDegreesConstIter(
            const PolynomElementsDegreesConstIter& it
        ): _cur_idx(it._cur_idx)
         , _degrees(it._degrees)
         , _max_degree(it._max_degree)
         , _combinations_count(it._combinations_count)
        {
        }

    private:
        friend class boost::iterator_core_access;

        template<std::size_t>
        friend class PolynomElementsDegreesConstIter;

        bool is_valid_combination() const
        {
            const auto degrees_sum = _degrees.get_sum();

            return degrees_sum <= _max_degree;
        }

        void next_combination()
        {
            for (int32_t i = static_cast<int32_t>(NVars - 1); i >= 0; --i) {
                if (_degrees[static_cast<std::size_t>(i)] == _max_degree) {
                    continue;
                } else {
                    _degrees[static_cast<std::size_t>(i)] += 1;
                    for (int32_t j = i + 1;
                         j < static_cast<int32_t>(NVars); ++j) {
                        _degrees[static_cast<std::size_t>(j)] = 0;
                    }
                    break;
                }
            }
        }

        void increment()
        {
            _cur_idx++;
            if (_cur_idx >= _combinations_count) {
                return;
            }

            do {
                this->next_combination();
            } while (not this->is_valid_combination());
        }

        bool equal(const PolynomElementsDegreesConstIter& it) const
        {
            return this->_cur_idx == it._cur_idx;
        }

        const monomial_degrees<NVars>& dereference() const
        {
            assertm(this->_cur_idx < _combinations_count,
                    "All combinations were returned!");

            return this->_degrees;
        }

        std::size_t _cur_idx = 0;
        monomial_degrees<NVars> _degrees;

        const std::size_t _max_degree;
        const std::size_t _combinations_count;
    };

    template<std::size_t NVars>
    class PolynomElsDegreesGenerator
    {
    public:
        using ConstIter = PolynomElementsDegreesConstIter<NVars>;

        PolynomElsDegreesGenerator(
            const std::size_t max_degree,
            const std::size_t combinations_count
        ): _max_degree(max_degree), _combinations_count(combinations_count)
        {
        }

        ConstIter begin() const {
            return {0, _max_degree, _combinations_count};
        }

        ConstIter end() const {
            return {_combinations_count, _max_degree, _combinations_count};
        }

    private:
        const std::size_t _max_degree;
        const std::size_t _combinations_count;
    };

    template<std::size_t NVars,
             std::size_t Degree,
             std::size_t CombinationsCount
                 = polynom_combinations(NVars, Degree)>
    polynom_degrees<NVars> make_polynom_degrees_arr()
    {
        const PolynomElsDegreesGenerator<NVars> generator(
            Degree,
            CombinationsCount
        );

        return polynom_degrees<NVars>(generator.begin(), generator.end());
    }
}
#endif // ML_POLYNOM_ELEMENTS_DEGREES_HELPERS_H
