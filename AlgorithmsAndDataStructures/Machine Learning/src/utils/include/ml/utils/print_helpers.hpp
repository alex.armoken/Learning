#if !(defined(ML_PRINT_HELPERS_H))
#define ML_PRINT_HELPERS_H

#include <array>
#include <iostream>
#include <vector>
#include <iomanip>

#include <TMatrixT.h>

#include "./array.hpp"
#include "./helper_types.hpp"

using namespace std::string_literals;

namespace ml::utils
{
    void print_marking_line(const std::string marker = "—");

    template<typename T>
    void print_els(const std::vector<T>& vec, const bool make_indent = false)
    {
        if (make_indent) {
            std::cout << "\t";
        }

        std::cout << "[";
        for (std::size_t i = 0; i < vec.size(); ++i) {
            std::cout << vec[i];
            if (i != vec.size() - 1) {
                std::cout << ", ";
            }
        }
        std::cout << "]";
    }

    template<typename T>
    void print_els2(
        const std::vector<T>& vec,
        const std::string& intro
    )
    {
        std::cout << intro;
        print_els(vec, true);
        std::cout << std::endl << std::endl << std::flush;
    }

    template<typename T, std::size_t Size>
    void print_els(const Array<T, Size>& arr, const bool make_indent = false)
    {
        if (make_indent) {
            std::cout << "\t";
        }

        std::cout << "[";
        for (std::size_t i = 0; i < Size; ++i) {
            std::cout << arr[i];
            if (i != Size - 1) {
                std::cout << ", ";
            }
        }
        std::cout << "]";
    }

    template<typename T, std::size_t Size>
    void print_els2(const Array<T, Size>& vec, const std::string& intro)
    {
        std::cout << intro << std::endl;
        print_els(vec, true);
        std::cout << std::endl << std::flush;
    }


    template<typename T>
    void print_matrix(const TMatrixT<T>& matrix)
    {
        const auto cols_count = static_cast<std::size_t>(matrix.GetNcols());
        const auto rows_count = static_cast<std::size_t>(matrix.GetNrows());

        std::cout << "[";
        for (std::size_t row_idx = 0; row_idx < rows_count; ++row_idx) {
            std::cout << "\t[";

            for (std::size_t col_idx = 0; col_idx < cols_count; ++col_idx) {
                std::cout << matrix(row_idx, col_idx);
                if (col_idx != cols_count - 1) {
                    std::cout << ", ";
                }
            }

            std::cout << "]";
            if (row_idx != rows_count - 1) {
                std::cout << ", ";
            }
            std::cout << std::endl;
        }
        std::cout << "]";
    }

    template <typename T>
    void print_matrix(const TMatrixT<T>& matrix, const std::string& intro)
    {
        std::cout << intro << std::endl;
        print_matrix(matrix);
        std::cout << std::endl << std::flush;
    }


    template <typename T>
    void print_vec(const TVectorT<T>& vector)
    {
        std::cout << "\t[";
        const int rows_count = vector.GetNrows();
        for (int i = 0; i < rows_count; ++i) {
            std::cout << std::to_string(vector[i]);
            if (i != rows_count - 1) {
                std::cout << ", ";
            }
        }
        std::cout << "]";
    }

    template <typename T>
    void print_vec(const TVectorT<T>& vector, const std::string& intro)
    {
        std::cout << intro << std::endl;
        print_vec(vector);
        std::cout << std::endl << std::flush;
    }

    void print_text(const std::initializer_list<std::string> lines,
                    const bool print_border = false);
}
#endif // ML_PRINT_HELPERS_H
