#if !(defined(ML_TIME_METER_H))
#define ML_TIME_METER_H
#include <cassert>
#include <chrono>

namespace ml::utils
{
    class TimeMeter
    {
    public:
        class TimerGuard
        {
        public:
            friend TimeMeter;

            TimerGuard() = delete;
            TimerGuard(const TimerGuard& timer_guard)
                : _timer(timer_guard._timer)
            {
            }

            virtual ~TimerGuard()
            {
                _timer.stop();
            }

        private:
            TimerGuard(TimeMeter& timer)
                : _timer(timer)
            {
                timer.start();
            }

            TimeMeter& _timer;
        };

        TimeMeter() {}
        virtual ~TimeMeter() = default;

        TimerGuard get_guard()
        {
            return TimerGuard(*this);
        }

        void start()
        {
            assert(not this->_is_running);
            this->_start_time = this->_get_time();
            this->_is_running = true;
        }

        void stop()
        {
            assert(this->_is_running);
            this->_stop_time = this->_get_time();
            this->_is_running = false;
            this->_is_initialized = true;
        }

        template<typename T = std::chrono::nanoseconds>
        T get_duration() const
        {
            assert(this->_is_initialized);
            return std::chrono::duration_cast<T>(
                this->_stop_time - this->_start_time
            );
        }

        template<typename T = uint64_t,
                 typename U = std::chrono::nanoseconds>
        T get_duration_length() const
        {
            return this->get_duration<U>().count();
        }


    private:
        std::chrono::high_resolution_clock::time_point _get_time() const
        {
            return std::chrono::high_resolution_clock::now();
        }

        bool _is_initialized = false;
        bool _is_running = false;
        std::chrono::high_resolution_clock::time_point _start_time;
        std::chrono::high_resolution_clock::time_point _stop_time;
    };
} // namespace ml::utils
#endif // ML_TIME_METER_H
