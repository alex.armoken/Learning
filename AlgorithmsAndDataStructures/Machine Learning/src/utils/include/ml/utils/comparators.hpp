#if !(defined(ML_UTILS_COMPARATORS_H))
#define ML_UTILS_COMPARATORS_H
#include <cmath>
#include <functional>

#include "./helper_types.hpp"

namespace ml::utils
{
    template<typename T>
    struct EpsilonComparator
    {
    public:
        EpsilonComparator(non_zero_value<T> epsilon)
            : _epsilon(epsilon)
        {
        }

        bool operator()(T a, T b) const
        {
            return std::islessequal(std::abs(a - b), *_epsilon * std::abs(a));
        }

        operator std::function<bool(T, T)>() { return *this; }

    private:
        const non_zero_value<T> _epsilon = 0;
    };
} // namespace ml::utils
#endif // ML_UTILS_COMPARATORS_H
