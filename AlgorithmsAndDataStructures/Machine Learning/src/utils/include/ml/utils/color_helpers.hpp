#if !(defined(ML_COLOR_HELPERS_H))
#define ML_COLOR_HELPERS_H
#include <cmath>
#include <sstream>
#include <stdint.h>
#include <string>
#include <algorithm>

#include "./num_helpers.hpp"

namespace ml::utils
{
    class ColorRGB
    {
    public:
        static const uint8_t MAX_CHANNEL_VALUE = 255;

        ColorRGB(uint8_t r, uint8_t g, uint8_t b)
            : _r(r), _g(g), _b(b)
        {
        }

        uint8_t r() const { return _r; }
        uint8_t g() const { return _g; }
        uint8_t b() const { return _b; }

        std::string get_hex() const
        {
             const int32_t rgb = ((_r & 0xff) << 16)
                                  + ((_g & 0xff) << 8)
                                  + (_b & 0xff);

             return "#" + int_to_hex(rgb, false, 6);
        }

        static ColorRGB get_white()
        {
            return ColorRGB(
                MAX_CHANNEL_VALUE,
                MAX_CHANNEL_VALUE,
                MAX_CHANNEL_VALUE
            );
        }

        static ColorRGB get_black()
        {
            return ColorRGB(0, 0, 0);
        }

    private:
        uint8_t _r = 0;
        uint8_t _g = 0;
        uint8_t _b = 0;
    };

    class ColorRGBA
    {
    public:
        static const uint8_t MAX_CHANNEL_VALUE = 255;

        ColorRGBA(uint8_t r, uint8_t g, uint8_t b, double alpha)
            : _r(r), _g(g), _b(b), _alpha(alpha)
        {
        }

        uint8_t r() const { return _r; }
        uint8_t g() const { return _g; }
        uint8_t b() const { return _b; }
        double alpha() const { return _alpha; }

        std::string get_hex() const
        {
            const auto alpha_channel = round_and_cast<int32_t>(
                _alpha * MAX_CHANNEL_VALUE
            );
            const int32_t rgb = ((_r & 0xff) << 24)
                                 + ((_g & 0xff) << 16)
                                 + ((_b & 0xff) << 8)
                                 + (alpha_channel & 0xff);

             return "#" + int_to_hex(rgb, false, 6);
        }

        ColorRGB rgb(ColorRGB bg) const
        {
            return ColorRGB(
                round_and_cast<uint8_t>((1 - _alpha) * bg.r() + _alpha * _r),
                round_and_cast<uint8_t>((1 - _alpha) * bg.g() + _alpha * _g),
                round_and_cast<uint8_t>((1 - _alpha) * bg.b() + _alpha * _b)
            );
        }

        static ColorRGBA get_white()
        {
            return ColorRGBA(
                MAX_CHANNEL_VALUE,
                MAX_CHANNEL_VALUE,
                MAX_CHANNEL_VALUE,
                1.0
            );
        }

        static ColorRGBA get_black()
        {
            return ColorRGBA(0, 0, 0, 1.0);
        }


    private:
        uint8_t _r = 0;
        uint8_t _g = 0;
        uint8_t _b = 0;
        double _alpha = 0;
    };

    ColorRGB get_color_from_ycbcr(double y, double cb, double cr)
    {
        double r = std::max(
            0.0,
            std::min(1.0, (double)(y + 0.0000 * cb + 1.4022 * cr))
        );
        double g = std::max(
            0.0,
            std::min(1.0, (double)(y - 0.3456 * cb - 0.7145 * cr))
        );
        double b = std::max(
            0.0,
            std::min(1.0, (double)(y + 1.7710 * cb + 0.0000 * cr))
        );

        return ColorRGB(
            (uint8_t)(r * 255),
            (uint8_t)(g * 255),
            (uint8_t)(b * 255)
        );
    }
}  // namespace ml::utils
#endif // ML_COLOR_HELPERS_H
