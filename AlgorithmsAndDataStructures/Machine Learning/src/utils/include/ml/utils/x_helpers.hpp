#if !(defined(ML_X_HELPER_H))
#define ML_X_HELPER_H
#include "./helper_types.hpp"
#include "./polynom_elements_degrees.hpp"

namespace ml::utils
{
    template<typename T>
    x_vec<T> make_x_vec(const std::initializer_list<T> list)
    {
        x_vec<T> result(static_cast<Int_t>(list.size() + 1));

        result[0] = 1;
        std::copy(list.begin(), list.end(), result.GetMatrixArray() + 1);

        return result;
    }

    template<typename T, std::size_t NEls>
    x_vec<T> make_x_vec(const T* const els)
    {
        x_vec<T> result(static_cast<Int_t>(NEls + 1));

        result[0] = 1;
        std::copy(els, els + NEls, result.GetMatrixArray() + 1);

        return result;
    }

    template<typename T, std::size_t NEls>
    x_vec<T> make_x_vec(const T* const els,
                        const polynom_degrees<NEls>& degrees)
    {
        x_vec<T> result(static_cast<Int_t>(degrees.size() + 1));

        result[0] = 1;
        make_combinations(result.GetMatrixArray() + 1, els, degrees);

        return result;
    }

    template<typename T, std::size_t NEls>
    x_vec<T> make_x_vec(const Array<T, NEls>& els)
    {
        x_vec<T> result(static_cast<Int_t>(els.size() + 1));

        result[0] = 1;
        std::copy(els.data(), els.data() + NEls, result.GetMatrixArray() + 1);

        return result;
    }

    template<typename T>
    x_vec<T> make_x_vec(const TVectorT<T>& vec)
    {
        x_vec<T> result(static_cast<Int_t>(vec.GetNrows() + 1));

        result[0] = 1;
        std::copy(
            vec.GetMatrixArray(),
            vec.GetMatrixArray() + static_cast<std::size_t>(vec.GetNrows()),
            result.GetMatrixArray() + 1
        );

        return result;
    }

    template<typename T>
    x_matrix<T> make_x_matrix(const TMatrixT<T>& matrix)
    {
        x_matrix<T> result(matrix);
        result.ResizeTo(0, matrix.GetNrows() - 1, -1, matrix.GetNcols() - 1);
        result.Shift(0, 1);

        for (Int_t i = 0; i < matrix.GetNrows(); ++i) {
            result(i, 0) = 1;
        }

        return result;
    }


    template<typename T, std::size_t NEls>
    x_vec<T> make_x_vec(const Array<T, NEls>& els,
                        const polynom_degrees<NEls>& degrees)
    {
        x_vec<T> result(static_cast<Int_t>(degrees.size() + 1));

        result[0] = 1;
        make_combinations(result.GetMatrixArray() + 1, els.data(), degrees);

        return result;
    }

    template<typename T, std::size_t NEls>
    x_vec<T> make_x_vec(const TVectorT<T>& vec,
                        const polynom_degrees<NEls>& degrees)
    {
        x_vec<T> result(static_cast<Int_t>(degrees.size() + 1));

        result[0] = 1;
        make_combinations(result.GetMatrixArray() + 1,
                          vec.GetMatrixArray(), degrees);

        return result;
    }
} // ml::utils
#endif // ML_X_HELPER_H
