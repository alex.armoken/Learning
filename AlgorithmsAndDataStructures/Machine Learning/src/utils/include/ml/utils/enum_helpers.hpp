#include <type_traits>

#if !(defined(ML_ENUM_HELPERS_H))
#define ML_ENUM_HELPERS_H
namespace ml::utils
{
    template <typename E>
    constexpr auto to_underlying(E e) noexcept
    {
        return static_cast<std::underlying_type_t<E>>(e);
    }
}
#endif // ML_ENUM_HELPERS_H
