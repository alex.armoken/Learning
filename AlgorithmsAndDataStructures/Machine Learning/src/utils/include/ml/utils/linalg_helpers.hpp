#if !(defined(ML_LINALG_HELPERS_H))
#define ML_LINALG_HELPERS_H

#include <array>
#include <iostream>
#include <vector>
#include <iomanip>
#include <stdint.h>
#include <map>
#include <cassert>

#include <TMatrixT.h>

#include "./helper_types.hpp"

using namespace std::string_literals;

namespace ml::utils
{
    template<typename T>
    class LogAction final: public TElementActionT<T>
    {
    public:
        virtual ~LogAction() = default;

        void Operation(T& element) const override
        {
            element = std::log(element);
        };
    };


    template<typename T>
    TMatrixT<T> transpose(const TMatrixT<T>& m)
    {
        TMatrixT<T> result(TMatrixT<T>::kTransposed, m);

        return result;
    }

    template<typename T>
    TMatrixT<T> transpose(const TVectorT<T>& v)
    {
        const auto rows_count = static_cast<std::size_t>(v.GetNrows());
        TMatrixT<T> result(static_cast<int>(rows_count), 1);
        for (std::size_t i = 0; i < rows_count; ++i) {
            result(i, 0) = v(i);
        }

        return result;
    }

    template<typename T>
    TMatrixT<T> to_matrix(const TVectorT<T>& v)
    {
        const auto rows_count = static_cast<std::size_t>(v.GetNrows());
        TMatrixT<T> result(1, static_cast<int>(rows_count));
        for (std::size_t col_idx = 0; col_idx < rows_count; ++col_idx) {
            result(0, col_idx) = v(col_idx);
        }

        return result;
    }

    template<typename T>
    TVectorT<T> to_vector(const TMatrixT<T>& m)
    {
        if ((m.GetNrows() == 1 and m.GetNcols() == 1)
              or (m.GetNrows() == 1 and m.GetNcols() > 1)) {
            return TVectorT<T>(TMatrixTRow_const<T>(m, 0));
        } else if (m.GetNrows() > 1 and m.GetNcols() == 1) {
            return TVectorT<T>(TMatrixTColumn_const<T>(m, 0));
        }

        assert("Not single column or single row matrix!");
        std::abort();
    }

    template<typename T>
    TVectorT<T> make_vec(std::initializer_list<T> list)
    {
        TVectorT<T> result(static_cast<int>(list.size()));
        for (std::size_t i = 0; i < list.size(); ++i) {
            result[static_cast<int>(i)] = *(list.begin() + i);
        }

        return result;
    }

    template<typename T, std::size_t Size>
    TVectorT<T> make_vec(const Array<T, Size>&  arr)
    {
        TVectorT<T> result(static_cast<Int_t>(Size));
        for (std::size_t i = 0; i < Size; ++i) {
            result[static_cast<Int_t>(i)] = arr[i];
        }

        return result;
    }

    template<typename T, std::size_t ASize>
    TVectorT<T> make_vec(const Array<T, ASize>&  arr, const nfirst count)
    {
        assert(*count <= ASize);

        TVectorT<T> result(static_cast<Int_t>(*count));
        for (std::size_t i = 0; i < *count; ++i) {
            result[static_cast<Int_t>(i)] = arr[i];
        }

        return result;
    }

    template<typename T>
    TVectorT<T> make_zero_vec(const Int_t size)
    {
        TVectorT<T> result(size);
        for (Int_t i = 0; i < size; ++i) {
            result[i] = 0;
        }

        return result;
    }

    template<typename T>
    TVectorT<T> make_zero_vec(const std::size_t size)
    {
        return make_zero_vec<T>(static_cast<Int_t>(size));
    }

    template<typename T>
    TMatrixT<T> make_zero_matrix(
        const std::size_t rows_count,
        const std::size_t cols_count
    )
    {
        TMatrixT<T> result(static_cast<Int_t>(rows_count),
                           static_cast<Int_t>(cols_count));
        std::fill(result.GetMatrixArray(),
                  result.GetMatrixArray() + result.GetNoElements(),
                  0);

        return result;
    }


    template<typename T, typename BinaryPredicate>
    bool is_equal(
        const TMatrixT<T>& a, const TMatrixT<T>& b,
        BinaryPredicate comparator
    )
    {
        const auto rows_count = static_cast<std::size_t>(a.GetNrows());
        const auto cols_count = static_cast<std::size_t>(a.GetNcols());
        if (a.GetNrows() != b.GetNrows() or a.GetNcols() != b.GetNcols()) {
            return false;
        }

        for (std::size_t i = 0; i < rows_count; ++i) {
            for (std::size_t j = 0; j < cols_count; ++j) {
                if (!bool(comparator(a(i, j), b(i, j)))) {
                    return false;
                }
            }
        }

        return true;
    }

    template<typename T, typename Func, typename BinaryPredicate>
    bool is_equal(
        const T& a, const T& b,
        const Func func,
        const BinaryPredicate comparator
    )
    {
        return comparator(func(a), func(b));
    }


    template<typename T>
    bool is_equal(const TMatrixT<T>& a, const TMatrixT<T>& b)
    {
        return is_equal(a, b, [](T a_el, T b_el) { return a_el == b_el; });
    }


    template<typename T, typename BinaryPredicate>
    bool is_equal(const TVectorT<T>& a, const TVectorT<T>& b,
                  BinaryPredicate comparator)
    {
        const int rows_count = a.GetNrows();
        if (a.GetNrows() != b.GetNrows()) {
            return false;
        }

        for (int i = 0; i < rows_count; ++i) {
            if (!bool(comparator(a[i], b[i]))) {
                return false;
            }
        }

        return true;
    }

    template<typename T>
    std::vector<bool> compare_els(
        const std::vector<T>& a,
        const std::vector<T>& b,
        std::function<bool(T, T)> comparator = nullptr
    )
    {
        assert(a.size() == b.size());
        std::vector<bool> result;
        result.resize(a.size());

        for (std::size_t i = 0; i < a.size(); ++i) {
            result[i] = comparator == nullptr
                ? a[i] == b[i]
                : comparator(a[i], b[i]);
        }

        return result;
    }

    template<typename T, typename U>
    T calc_avg(const std::vector<U>& vec)
    {
        T sum = std::accumulate(
            vec.begin(), vec.end(), static_cast<T>(0),
            [](T a, U b) { return a + static_cast<T>(b); }
        );

        return sum / static_cast<T>(vec.size());
    }

    template<typename T, typename U>
    std::vector<U> transform_vec(const TVectorT<T>& vec,
                                 std::function<U(T)> transformer)
    {
        const auto rows_count = vec.GetNrows();
        std::vector<U> result(static_cast<std::size_t>(rows_count));
        for (Int_t i = 0; i < rows_count; ++i) {
            result[static_cast<std::size_t>(i)] = transformer(vec[i]);
        }

        return result;
    }

    template<typename T>
    bool is_equal(const TVectorT<T>& a, const TVectorT<T>& b)
    {
        return is_equal(a, b, [](T a_el, T b_el) { return a_el == b_el; });
    }

    template<typename T>
    TVectorT<T> get_row(const TMatrixT<T>& m, const int32_t row_idx)
    {
        return TVectorT<T>(TMatrixTRow_const<T>(m, row_idx));
    }

    template<typename T>
    TVectorT<T> get_col(const TMatrixT<T>& m, const int32_t col_idx)
    {
        return TVectorT<T>(TMatrixTColumn_const<T>(m, col_idx));
    }

    template<typename T>
    TVectorT<T> zeroing_1st_el(const TVectorT<T> vec)
    {
        TVectorT<T> result(vec);
        result(0) = 0;

        return result;
    }

    template<typename T>
    bool is_valid_proportions(const TMatrixT<T>& m,
                              const std::size_t rows_count,
                              const std::size_t cols_count)
    {
        return m.GetNrows() == static_cast<Int_t>(rows_count)
            and m.GetNcols() == static_cast<Int_t>(cols_count);
    }
}  // namespace ml::utils
#endif // ML_LINALG_HELPERS_H
