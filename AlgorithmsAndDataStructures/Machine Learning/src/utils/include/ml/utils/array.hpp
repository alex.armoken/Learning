#if !(defined(ML_UTILS_ARRAY_H))
#define ML_UTILS_ARRAY_H

#include <array>
#include <numeric>

namespace ml::utils
{
    template<typename T, std::size_t NEls>
    class Array
    {
    public:
        Array() = default;
        Array(Array&&) = default;
        Array(const Array&) = default;
        Array(std::initializer_list<T> init_list)
        {
            assert(NEls == init_list.size());
            for (std::size_t i = 0; i < NEls; ++i) {
                _data[i] = *(init_list.begin() + i);
            }
        }
        Array(const T init_value) { _data.fill(init_value); }
        ~Array() = default;

        std::size_t size() const { return NEls; }

        T* data() { return _data.data(); }
        const T* data() const { return _data.data(); }

        T get_sum() const
        {
            return std::accumulate(_data.begin(), _data.end(), T(0));
        }

        Array& operator=(const Array& rhs)
        {
            // Check for self-assignment!
            if (this == &rhs) {
                return *this;
            }
            _data = rhs._data;

            return *this;
        }

        Array& operator=(std::initializer_list<T> init_list)
        {
            _data = init_list;

            return *this;
        }


        T& operator[](std::size_t index)
        {
            return _data[index];
        }

        const T& operator[](std::size_t index) const
        {
            return _data[index];
        }


        Array operator+(const T& val)
        {
            Array result = *this;
            for (std::size_t i = 0; i < NEls; ++i) {
                result[i] += val;
            }
            return result;
        }

        Array operator-(const T& val)
        {
            Array result = *this;
            for (std::size_t i = 0; i < NEls; ++i) {
                result[i] -= val;
            }
            return result;
        }

        Array operator*(const T& val)
        {
            Array result = *this;
            for (std::size_t i = 0; i < NEls; ++i) {
                result[i] *= val;
            }
            return result;
        }

        Array operator/(const T& val)
        {
            Array result = *this;
            for (std::size_t i = 0; i < NEls; ++i) {
                result[i] /= val;
            }
            return result;
        }


        Array operator+(const Array& other)
        {
            Array result = *this;
            for (std::size_t i = 0; i < NEls; ++i) {
                result[i] += other[i];
            }
            return result;
        }

        Array operator-(const Array& other)
        {
            Array result = *this;
            for (std::size_t i = 0; i < NEls; ++i) {
                result[i] -= other[i];
            }
            return result;
        }

        Array operator*(const Array& other)
        {
            Array result = *this;
            for (std::size_t i = 0; i < NEls; ++i) {
                result[i] *= other[i];
            }
            return result;
        }

        Array operator/(const Array& other)
        {
            Array result = *this;
            for (std::size_t i = 0; i < NEls; ++i) {
                result[i] /= other[i];
            }

            return result;
        }

    private:
        std::array<T, NEls> _data;
    };
} // namespace ml::utils::
#endif // ML_UTILS_ARRAY_H
