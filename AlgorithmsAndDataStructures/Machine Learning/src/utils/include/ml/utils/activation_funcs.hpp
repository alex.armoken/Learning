#if !(defined(ML_ACTIVATION_FUNCS_H))
#define ML_ACTIVATION_FUNCS_H

#include <cmath>

#include <TMatrixT.h>

namespace ml::utils::activation_funcs
{
    template<typename T>
    class Activator
    {
    public:
        virtual ~Activator() = default;

        virtual T activate(const T el) const = 0;
        virtual TVectorT<T> activate(const TVectorT<T>& vec) const = 0;
        virtual TMatrixT<T> activate(const TMatrixT<T>& vec) const = 0;

        virtual T activate_derivative(const T el) const = 0;
        virtual TVectorT<T> activate_derivative(
            const TVectorT<T>& vec
        ) const = 0;
        virtual TMatrixT<T> activate_derivative(
            const TMatrixT<T>& vec
        ) const = 0;

        T operator()(const T el) const
        {
            return activate(el);
        }

        TVectorT<T> operator()(const TVectorT<T>& vec) const
        {
            return activate(vec);
        }

        TVectorT<T> operator()(const TMatrixT<T>& matrix) const
        {
            return activate(matrix);
        }
    };


    template<typename T>
    T sigmoid(const T x)
    {
        return 1 / (1 + std::exp(-x));
    }

    template<typename T>
    T sigmoid_derivative(const T x)
    {
        return sigmoid(x) / (1 - sigmoid(x));
    }

    template<typename T>
    class SigmoidActivator final: public Activator<T>
    {
    private:
        class Action final: public TElementActionT<T>
        {
        public:
            virtual ~Action() = default;

            void Operation(T& element) const override
            {
                element = sigmoid(element);
            };
        };

        class DerivativeAction final: public TElementActionT<T>
        {
        public:
            virtual ~DerivativeAction() = default;

            void Operation(T& element) const override
            {
                element = sigmoid_derivative(element);
            };
        };

    public:
        virtual ~SigmoidActivator() = default;

        T activate(const T el) const override
        {
            return sigmoid(el);
        }

        TVectorT<T> activate(const TVectorT<T>& vec) const override
        {
            TVectorT<T> result(vec);
            result.Apply(Action());

            return result;
        }

        TMatrixT<T> activate(const TMatrixT<T>& matrix) const override
        {
            TMatrixT<T> result(matrix);
            result.Apply(Action());

            return result;
        }

        T activate_derivative(const T el) const
        {
            return sigmoid_derivative(el);
        }

        TVectorT<T> activate_derivative(const TVectorT<T>& vec) const
        {
            TVectorT<T> result(vec);
            result.Apply(DerivativeAction());

            return result;
        }

        TMatrixT<T> activate_derivative(const TMatrixT<T>& matrix) const
        {
            TMatrixT<T> result(matrix);
            result.Apply(DerivativeAction());

            return result;
        }
    };


    template<typename T>
    T tanh(const T x)
    {
        return std::tanh(x);
    }

    template<typename T>
    T tanh_derivative(const T x)
    {
        return 1 / std::pow(std::cosh(x), 2);
    }

    template<typename T>
    class TanhActivator final: public Activator<T>
    {
    private:
        class Action final: public TElementActionT<T>
        {
        public:
            virtual ~Action() = default;

            void Operation(T& element) const override
            {
                element = tanh(element);
            };
        };

        class DerivativeAction final: public TElementActionT<T>
        {
        public:
            virtual ~DerivativeAction() = default;

            void Operation(T& element) const override
            {
                element = tanh_derivative(element);
            };
        };

    public:
        virtual ~TanhActivator() = default;

        T activate(const T el) const override
        {
            return tanh(el);
        }

        TVectorT<T> activate(const TVectorT<T>& vec) const override
        {
            TVectorT<T> result(vec);
            result.Apply(Action());

            return result;
        }

        TMatrixT<T> activate(const TMatrixT<T>& matrix) const override
        {
            TMatrixT<T> result(matrix);
            result.Apply(Action());

            return result;
        }


        T activate_derivative(const T el) const
        {
            return tanh_derivative(el);
        }

        TVectorT<T> activate_derivative(const TVectorT<T>& vec) const
        {
            TVectorT<T> result(vec);
            result.Apply(DerivativeAction());

            return result;
        }

        TMatrixT<T> activate_derivative(const TMatrixT<T>& matrix) const
        {
            TMatrixT<T> result(matrix);
            result.Apply(DerivativeAction());

            return result;
        }
    };


    // template<typename T>
    // T ReLU(const T x)
    // {
    //     return std::max(0, x);
    // }

    // template<typename T>
    // class ReLUActivator final: public Activator<T>
    // {
    // private:
    //     class Action final: public TElementActionT<T>
    //     {
    //     public:
    //         virtual ~Action() = default;

    //         void Operation(T& element) const override
    //         {
    //             element = ReLU(element);
    //         };
    //     };

    // public:
    //     virtual ~ReLUActivator() = default;

    //     T activate(const T el) const override
    //     {
    //         return ReLU(el);
    //     }

    //     TVectorT<T> activate(const TVectorT<T>& vec) const override
    //     {
    //         TVectorT<T> result(vec);
    //         result.Apply(Action());

    //         return result;
    //     }
    // };


    // template<typename T>
    // T leaky_ReLU(const T x)
    // {
    //     return std::max(0.1 * x, x);
    // }

    // template<typename T>
    // class LeakyReLUActivator final: public Activator<T>
    // {
    // private:
    //     class Action final: public TElementActionT<T>
    //     {
    //     public:
    //         virtual ~Action() = default;

    //         void Operation(T& element) const override
    //         {
    //             element = leaky_ReLU(element);
    //         };
    //     };

    // public:
    //     virtual ~LeakyReLUActivator() = default;

    //     T activate(const T el) const override
    //     {
    //         return leaky_ReLU(el);
    //     }

    //     TVectorT<T> activate(const TVectorT<T>& vec) const override
    //     {
    //         TVectorT<T> result(vec);
    //         result.Apply(Action());

    //         return result;
    //     }
    // };


    // template<typename T>
    // T maxout(const T x, const T w_1, const T w_2, const T b_1, const T b_2)
    // {
    //     return std::max(w_1 * x + b_1, w_2 * x + b_2);
    // }

    // template<typename T>
    // class MaxoutActivator final: public Activator<T>
    // {
    // private:
    //     class Action final: public TElementActionT<T>
    //     {
    //     public:
    //         Action(const T w_1, const T w_2, const T b_1, const T b_2)
    //             : _w_1(w_1), _w_2(w_2), _b_1(b_1), _b_2(b_2)
    //         {
    //         }
    //         Action(const Action&) = default;
    //         virtual ~Action() = default;

    //         Action(Action&&) = delete;

    //         void Operation(T& element) const override
    //         {
    //             element = maxout(element, _w_1, _w_1, _b_1, _b_2);
    //         };

    //     private:
    //         const T _w_1 = 0;
    //         const T _w_2 = 0;
    //         const T _b_1 = 0;
    //         const T _b_2 = 0;
    //     };

    // public:
    //     MaxoutActivator(const T w_1, const T w_2, const T b_1, const T b_2)
    //         : _w_1(w_1), _w_2(w_2), _b_1(b_1), _b_2(b_2)
    //     {
    //     }
    //     MaxoutActivator(const MaxoutActivator&) = default;
    //     virtual ~MaxoutActivator() = default;

    //     MaxoutActivator(MaxoutActivator&&) = delete;

    //     T activate(const T el) const override
    //     {
    //         return maxout(el, _w_1, _w_1, _b_1, _b_2);
    //     }

    //     TVectorT<T> activate(const TVectorT<T>& vec) const override
    //     {
    //         TVectorT<T> result(vec);
    //         result.Apply(Action(_w_1, _w_1, _b_1, _b_2));

    //         return result;
    //     }

    // private:
    //     const T _w_1 = 0;
    //     const T _w_2 = 0;
    //     const T _b_1 = 0;
    //     const T _b_2 = 0;
    // };


    // template<typename T>
    // T ELU(const T x, const T alpha)
    // {
    //     if (std::isgreaterequal(x, 0)) {
    //         return x;
    //     } else {
    //         return alpha * (std::exp(x) - 1);
    //     }
    // }

    // template<typename T>
    // class ELUActivator final: public Activator<T>
    // {
    // private:
    //     class Action final: public TElementActionT<T>
    //     {
    //     public:
    //         Action(const T w_1, const T alpha)
    //             : _alpha(alpha)
    //         {
    //         }
    //         Action(const Action&) = default;
    //         virtual ~Action() = default;

    //         Action(Action&&) = delete;

    //         void Operation(T& element) const override
    //         {
    //             element = ELU(element, _alpha);
    //         };

    //     private:
    //         const T _alpha = 0;
    //     };

    // public:
    //     ELUActivator(const T alpha)
    //         : _alpha(alpha)
    //     {
    //     }
    //     ELUActivator(const ELUActivator&) = default;
    //     virtual ~ELUActivator() = default;

    //     ELUActivator(ELUActivator&&) = delete;

    //     T activate(const T el) const override
    //     {
    //         return ELU(el, _alpha);
    //     }

    //     TVectorT<T> activate(const TVectorT<T>& vec) const override
    //     {
    //         TVectorT<T> result(vec);
    //         result.Apply(Action(_alpha));

    //         return result;
    //     }

    // private:
    //     const T _alpha = 0;
    // };
}
#endif // ML_ACTIVATION_FUNCS_H
