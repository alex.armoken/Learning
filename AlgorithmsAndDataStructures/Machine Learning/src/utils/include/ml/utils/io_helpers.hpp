#if !(defined(ML_IO_HELPERS_H))
#define ML_IO_HELPERS_H
#include <cassert>
#include <vector>
#include <functional>
#include <filesystem>

#include <TMatrixT.h>
#include <TVectorT.h>
#include <rapidcsv.h>
#include <matio.h>

#include "ml/utils/helper_types.hpp"
#include "ml/utils/polynom_elements_degrees.hpp"

namespace ml::utils
{
    template<typename T, std::size_t InputParamsCount>
    using input_params = Array<T, InputParamsCount>;

    template<typename T, std::size_t InputParamsCount>
    class Variant
    {
    public:
        static const std::size_t INPUT_PARAMS_COUNT = InputParamsCount;
        static const std::size_t VALUES_COUNT = INPUT_PARAMS_COUNT + 1;

        Variant(): _input(0), _output(0)
        {
        }

        Variant(const input_params<T, InputParamsCount>& input, T output)
        {
            this->_input = input;
            this->_output = output;
        }

        Variant(const Array<T, VALUES_COUNT>& input_output)
        {
            for (std::size_t i = 0; i < InputParamsCount; ++i) {
                this->_input[i] = input_output[i];
            }

            this->_output = input_output[VALUES_COUNT - 1];
        }
        ~Variant() = default;


        T& in(const std::size_t idx)
        {
            return _input[idx];
        }

        T& out()
        {
            return _output;
        }

        T in(const std::size_t idx) const
        {
            return _input[idx];
        }

        const input_params<T, InputParamsCount>& get_input() const
        {
            return _input;
        }

        T out() const
        {
            return _output;
        }

        T& get(const std::size_t idx)
        {
            assert(idx < VALUES_COUNT);

            if (idx < INPUT_PARAMS_COUNT) {
                return this->_input[idx];
            } else if (idx == INPUT_PARAMS_COUNT) {
                return this->_output;
            } else {
                std::terminate();
            }
        }

        T& operator[](const std::size_t idx)
        {
            return get(idx);
        }

        T get(const std::size_t idx) const
        {
            return (const_cast<Variant*>(this))->get(idx);
        }

        T operator[](const std::size_t idx) const
        {
            return get(idx);
        }

    private:
        input_params<T, InputParamsCount> _input;
        T _output;
    };


    template<typename T, std::size_t NInputVars>
    class FuncIOVariants
    {
    public:
        static const std::size_t INPUT_PARAMS_COUNT = NInputVars;

        // Input and output values
        static const std::size_t VALUES_COUNT = INPUT_PARAMS_COUNT + 1;

        // Count of characteristics with additional characteristic
        static const std::size_t XVARS_COUNT = INPUT_PARAMS_COUNT + 1;

        FuncIOVariants() = default;
        FuncIOVariants(FuncIOVariants<T, NInputVars>&&) = default;
        FuncIOVariants(const FuncIOVariants<T, NInputVars>&) = default;

        FuncIOVariants(std::vector<Variant<T, NInputVars>>&& variants)
            : _variants(std::move(variants))
        {
        }

        virtual ~FuncIOVariants() = default;

        void emplace_back(Variant<T, NInputVars>&& variant)
        {
            this->_variants.emplace_back(std::move(variant));
        }

        void push_back(const Variant<T, NInputVars>& variant)
        {
            this->_variants.push_back(variant);
        }

        void push_back(const input_params<T, NInputVars>& in, T out)
        {
            this->_variants.emplace_back(in, out);
        }

        std::size_t get_variants_count() const
        {
            return this->_variants.size();
        }

        std::size_t size() const
        {
            return this->get_variants_count();
        }


        const Variant<T, NInputVars>& operator[](std::size_t idx) const
        {
            return this->_variants[idx];
        }

        const Variant<T, NInputVars>& get(const std::size_t idx) const
        {
            return this->_variants[idx];
        }

        T get_minimal(const std::size_t var_idx) const
        {
            return std::min_element(
                this->_variants.cbegin(),
                this->_variants.cend(),
                [=](const Variant<T, NInputVars>& a,
                    const Variant<T, NInputVars>& b) {
                    return a[var_idx] < b[var_idx];
                }
            )->get(var_idx);
        }

        T get_maximal(const std::size_t var_idx) const
        {
            return std::max_element(
                this->_variants.cbegin(),
                this->_variants.cend(),
                [=](const Variant<T, NInputVars>& a,
                    const Variant<T, NInputVars>& b) {
                    return a[var_idx] < b[var_idx];
                }
            )->get(var_idx);
        }

        const x_matrix<T>& get_x_matrix() const
        {
            if (not _maybe_y) {
                const_cast<FuncIOVariants*>(this)->_maybe_x
                    = _create_x_matrix();
            }

            return *_maybe_x;
        }

        const y_vec<T>& get_y_vector() const
        {
            if (not _maybe_y) {
                const_cast<FuncIOVariants*>(this)->_maybe_y
                    = _create_y_vector();
            }

            return *_maybe_y;
        }

        template<std::size_t NNewInputVars>
        FuncIOVariants<T, NNewInputVars> create_feature_map(
            const polynom_degrees<NInputVars>& els_degrees
        ) const
        {
            FuncIOVariants<T, NNewInputVars> result;
            for (const auto& variant: _variants) {
                result.emplace_back(
                    _create_new_variant<NNewInputVars>(
                        variant,
                        els_degrees
                    )
                );
            }

            return result;
        }

    private:
        std::vector<Variant<T, NInputVars>> _variants;
        std::optional<x_matrix<T>> _maybe_x;
        std::optional<y_vec<T>> _maybe_y;

        T _calc_value_of_new_characteristic(
            const Variant<T, NInputVars>& variant,
            const monomial_degrees<NInputVars>& degrees
        ) const
        {
            T characteristics_val = 1;
            for (std::size_t i = 0; i < NInputVars; ++i) {
                characteristics_val *= std::pow(variant.in(i), degrees[i]);
            }

            return characteristics_val;
        }

        template<std::size_t NNewInputVars>
        Variant<T, NNewInputVars> _create_new_variant(
            const Variant<T, NInputVars> variant,
            const polynom_degrees<NInputVars>& els_degrees
        ) const
        {
            Variant<T, NNewInputVars> new_variant;
            for (std::size_t i = 0; i < els_degrees.size(); ++i) {
                new_variant.in(i) = _calc_value_of_new_characteristic(
                    variant,
                    els_degrees[i]
                );
            }
            new_variant.out() = variant.out();

            return new_variant;
        }

        x_matrix<T> _create_x_matrix() const
        {
            TMatrixT<T> result(static_cast<Int_t>(this->size()),
                               static_cast<Int_t>(XVARS_COUNT));

            for (std::size_t var_idx = 0; var_idx < this->size(); ++var_idx) {
                result(static_cast<Int_t>(var_idx), 0) = T(1);

                for (std::size_t in_idx = 0;
                     in_idx < INPUT_PARAMS_COUNT; ++in_idx) {
                    const auto in = this->get(var_idx).in(in_idx);
                    result(static_cast<Int_t>(var_idx),
                           static_cast<Int_t>(in_idx + 1)) = in;
                }
            }

            return result;
        }

        y_vec<T> _create_y_vector() const
        {
            TVectorT<T> result(static_cast<Int_t>(this->size()));
            for (std::size_t var_idx = 0; var_idx < this->size(); ++var_idx) {
                result[static_cast<Int_t>(var_idx)]
                    = this->get(var_idx).out();
            }

            return result;
        }
    };


    class CSVFileReader final
    {
    private:
        CSVFileReader(rapidcsv::Document&& doc)
            : _doc(std::move(doc))
        {
        }

        static std::optional<CSVFileReader> _load_csv_file(
            const std::filesystem::path& path_to_file,
            rapidcsv::LabelParams label_params
        );

    public:
        CSVFileReader() = delete;

        CSVFileReader(CSVFileReader&&) = default;
        CSVFileReader(const CSVFileReader&) = default;

        static std::optional<CSVFileReader> load_file_without_labels(
            const std::filesystem::path& path_to_file
        );

        static std::optional<CSVFileReader> load_file(
            const std::filesystem::path& path_to_file,
            const std::size_t col_name_idx,
            const std::size_t row_name_idx
        );

        template<typename T, std::size_t NInputVars>
        std::optional<std::vector<Variant<T, NInputVars>>> get_variants() const
        {
            const auto ncols = _doc.GetColumnCount();
            if ((ncols - 1) != NInputVars) {
                return std::nullopt;
            }
            const auto nrows = _doc.GetRowCount();

            std::vector<Variant<T, NInputVars>> result;
            result.reserve(nrows);

            for (std::size_t row_idx = 0; row_idx < nrows; ++row_idx) {
                result.emplace_back(
                    _get_variant<T, NInputVars>(row_idx, ncols)
                );
            }

            return result;
        }

    private:
        rapidcsv::Document _doc;

        template<typename T, std::size_t NInputVars>
        Variant<T, NInputVars> _get_variant(const std::size_t row_idx,
                                            const std::size_t ncols) const
        {
            Variant<T, NInputVars> result;
            for (std::size_t col_idx = 0; col_idx < ncols - 1; ++col_idx) {
                result.in(col_idx) = _doc.GetCell<T>(col_idx, row_idx);
            }
            result.out() = _doc.GetCell<T>(ncols - 1, row_idx);

            return result;
        }
    };

    class MatFileReader final
    {
    private:
        using matvar_ptr = std::unique_ptr<matvar_t,
                                           std::function<void(matvar_t*)>>;

        MatFileReader(mat_t* mat_file)
            : _file(mat_file)
        {
        }

    public:
        MatFileReader() = delete;
        MatFileReader(const MatFileReader& m) = delete;

        MatFileReader(MatFileReader&& m)
        {
            _file = m._file;
            m._file = nullptr;
        }

        ~MatFileReader()
        {
            if (_file != nullptr) {
                Mat_Close(_file);
                _file = nullptr;
            }
        }

        static std::optional<MatFileReader> load_file(
            const std::filesystem::path& path_to_mat
        );

        template<typename T>
        std::optional<TMatrixT<T>> read_matrix(
            const std::string& var_name,
            const bool is_x_matrix
        ) const
        {
            static_assert(std::is_arithmetic<T>::value,
                          "Not arithmetic type");

            const matvar_ptr var = _read_var(var_name);
            if (not _is_valid_data_var(var, ndimensions(2))) {
                return std::nullopt;
            }

            const auto nrows = var->dims[0];
            const auto ncols = var->dims[1];
            const auto offset = static_cast<std::size_t>(is_x_matrix);
            TMatrixT<T> result(
                static_cast<Int_t>(nrows),
                static_cast<Int_t>(ncols + offset)
            );

            for (std::size_t i = 0; i < nrows; ++i) {
                if (is_x_matrix) {
                    result(static_cast<Int_t>(i), 0) = 1;
                }

                for (std::size_t j = 0; j < ncols; ++j) {
                    result(
                        static_cast<Int_t>(i),
                        static_cast<Int_t>(j + offset)
                    ) = _read_el<T>(var, i, j, nrows, ncols);
                }
            }

            return std::make_optional(std::move(result));
        }

        template<typename T>
        std::optional<std::vector<TVectorT<T>>> read_array_of_vectors(
            const std::string& var_name,
            const bool is_x_vectors = false
        ) const
        {
            static_assert(std::is_arithmetic<T>::value,
                          "Not arithmetic type");

            const matvar_ptr var = _read_var(var_name);
            if (not _is_valid_data_var(var, ndimensions(2))) {
                return std::nullopt;
            }

            const std::size_t nrows = var->dims[0];
            const std::size_t ncols = var->dims[1];
            const auto offset = static_cast<std::size_t>(is_x_vectors);
            std::vector<TVectorT<T>> result;
            result.reserve(nrows);

            for (std::size_t i = 0; i < nrows; ++i) {
                TVectorT<T> vec(static_cast<Int_t>(ncols + offset));
                if (is_x_vectors) {
                    vec[0] = 1;
                }

                for (std::size_t j = 0; j < ncols; ++j) {
                    vec(static_cast<Int_t>(j + offset))
                        = _read_el<T>(var, i, j, nrows, ncols);
                }
                result.emplace_back(std::move(vec));
            }

            return std::make_optional(std::move(result));
        }

        template<typename T>
        std::optional<TVectorT<T>> read_vector(
            const std::string& var_name
        ) const
        {
            static_assert(std::is_arithmetic<T>::value,
                          "Not arithmetic type");

            const matvar_ptr var = _read_var(var_name);
            if (not _is_valid_data_var(var, ndimensions(2))) {
                return std::nullopt;
            }

            const auto nrows = var->dims[0];
            TVectorT<T> result(static_cast<Int_t>(nrows));
            for (std::size_t i = 0; i < nrows; ++i) {
                result(static_cast<Int_t>(i)) = _read_el<T>(var, i);
            }

            return result;
        }

        template<typename T, std::size_t NInputVars>
        std::optional<std::vector<Variant<T, NInputVars>>> read_variants(
            const std::string& name_of_input_var,
            const std::string& name_of_output_var
        ) const
        {
            const matvar_ptr in_var = _read_var(name_of_input_var);
            if (not _is_valid_data_var(in_var, ndimensions(2))) {
                return std::nullopt;
            }

            const matvar_ptr out_var = _read_var(name_of_output_var);
            if (not _is_valid_data_var(out_var, ndimensions(1))) {
                return std::nullopt;
            }

            const auto nrows = in_var->dims[0];
            const auto ncols = in_var->dims[1];
            if (out_var->dims[0] != nrows or NInputVars != ncols) {
                return std::nullopt;
            }

            std::vector<Variant<T, NInputVars>> result;
            result.reserve(nrows);

            for (std::size_t i = 0; i < nrows; ++i) {
                result.emplace_back(
                    _read_row<T, NInputVars>(in_var, i, nrows),
                    _read_el<T>(out_var, i)
                );
            }

            return result;
        }

        std::vector<std::string> get_variables() const;

    private:
        mat_t* _file = nullptr;

        matvar_ptr _read_var(const std::string& var_name,
                            const bool info_only = false) const
        {
            matvar_t* result = info_only
                ? Mat_VarReadInfo(_file, var_name.c_str())
                : Mat_VarRead(_file, var_name.c_str());

            return matvar_ptr(result, Mat_VarFree);
        }

        bool _is_valid_data_var(const matvar_ptr& var,
                                const ndimensions dims) const
        {
            if (not var or var->data == nullptr) {
                return false;
            } else if (var->rank == static_cast<int>(*dims)) {
                return true;
            } else if (var->rank == static_cast<int>(*dims + 1)) {
                bool is_one_dimension_flat = false;
                for (int i = 0; i < var->rank; ++i) {
                    if (var->dims[i] == 1) {
                        is_one_dimension_flat = true;
                        break;
                    }
                }

                return is_one_dimension_flat;
            }

            return false;
        }

        template<typename T>
        T _read_el(const matvar_ptr& var, const std::size_t idx) const
        {
            return reinterpret_cast<T*>(var->data)[idx];
        }

        template<typename T>
        T _read_el(const matvar_ptr& var,
                  const std::size_t row_idx, const std::size_t col_idx,
                  const std::size_t nrows, const std::size_t ncols
        ) const
        {
            const std::size_t offset = nrows * col_idx + row_idx;

            return _read_el<T>(var, offset);
        }

        template<typename T, std::size_t NCols>
        Array<T, NCols> _read_row(const matvar_ptr& var,
                                  const std::size_t row_idx,
                                  const std::size_t nrows) const
        {

            Array<T, NCols> result;
            for (std::size_t col_idx = 0; col_idx < NCols; ++col_idx) {
                result[col_idx]
                    = _read_el<T>(var, row_idx, col_idx, nrows, NCols);
            }

            return result;
        }
    };
} // namespace ml::utils
#endif // ML_IO_HELPERS_H
