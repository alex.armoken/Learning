#if !(defined(ML_OPDESC_LINREG_H))
#define ML_OPDESC_LINREG_H

#include <array>
#include <cstddef>
#include <chrono>
#include <vector>
#include <numeric>

#include <TMatrixT.h>
#include <TVectorT.h>


#include "ml/utils/comparators.hpp"
#include "ml/utils/io_helpers.hpp"
#include "ml/utils/print_helpers.hpp"
#include "ml/utils/exceptions.hpp"
#include "ml/utils/linalg_helpers.hpp"
#include "ml/utils/time_meter.hpp"
#include "ml/utils/helper_types.hpp"

#include "./grad_descent.hpp"

namespace ml::opdesc::linreg
{
    using namespace utils;

    template<typename T>
    T calc_h(const theta_vec<T>& thetas, const x_vec<T>& x)
    {
        return Dot(thetas, x);
    }

    template<typename T>
    TVectorT<T> calc_h2(const theta_vec<T>& thetas, const x_matrix<T>& x)
    {
        return x * thetas;
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    T calc_loss(
        const theta_arr<T, NThetas>& thetas,
        const FuncIOVariants<T, NInputVars>& data
    )
    {
        const auto rows_count = data.get_variants_count();

        T sum = 0;
        for (std::size_t row_idx = 0; row_idx < rows_count; ++row_idx) {
            T hypothesis_func_value = 0;
            for (std::size_t x_idx = 0; x_idx < NXVars + 1; ++x_idx) {
                const auto x_val = x_idx == 0
                    ? 1
                    : data[row_idx].in(x_idx - 1);
                hypothesis_func_value += thetas[x_idx] * x_val;
            }

            const auto y_val = data[row_idx].out();
            sum += std::pow(hypothesis_func_value - y_val, 2);
        }

        return sum / (2 * static_cast<T>(rows_count));
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    T calc_loss_l2(
        const theta_arr<T, NThetas>& thetas,
        const FuncIOVariants<T, NInputVars>& data,
        const lambda<T> lambda
    )
    {
        const auto loss = calc_loss(thetas, data);

        T reg = 0;
        for (std::size_t i = 0; i < NThetas; ++i) {
            reg += std::pow(thetas[i], 2);
        }
        reg *= *lambda / (2 * static_cast<T>(data.get_variants_count()));

        return loss + reg;
    }

    template <typename T>
    T calc_loss(const theta_vec<T>& thetas,
                const x_matrix<T>& x,
                const y_vec<T>& y)
    {
        assert(thetas.GetNoElements() == x.GetNcols());
        const auto rows_count = x.GetNrows();

        auto h = calc_h2(thetas, x);
        auto h_y = h - y;
        auto sqr = h_y.Sqr();

        return sqr.Sum() / (2 * rows_count);
    }

    template <typename T>
    T calc_loss_l2(const theta_vec<T>& thetas,
                   const x_matrix<T>& x,
                   const y_vec<T>& y,
                   const lambda<T> lambda)
    {
        const auto rows_count = x.GetNrows();

        T sum = 0;
        for (Int_t row_idx = 0; row_idx < rows_count; ++row_idx) {
            const auto x_row = get_row(x, row_idx);
            sum += std::pow(
                calc_h(thetas, x_row) - y[row_idx],
                2
            );
        }

        const auto reg = *lambda * (Dot(thetas, thetas)
                                    - std::pow(thetas[0], 2));

        return (sum + reg) / (2 * rows_count);
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    T calc_loss_deriv(
        const std::size_t theta_deriv_var_idx,
        const theta_arr<T, NThetas>& thetas,
        const FuncIOVariants<T, NInputVars>& data
    )
    {
        const auto rows_count = data.get_variants_count();

        T sum = 0;
        for (std::size_t row_idx = 0; row_idx < rows_count; ++row_idx) {
            T hypothesis_func_value = 0;
            for (std::size_t x_idx = 0; x_idx < NXVars + 1; ++x_idx) {
                const auto x_val = x_idx == 0 ? 1 : data[row_idx].in(x_idx - 1);
                hypothesis_func_value += thetas[x_idx] * x_val;
            }

            const auto x_val = theta_deriv_var_idx == 0
                ? 1
                : data[row_idx].in(theta_deriv_var_idx - 1);
            const auto y_val = data[row_idx].out();
            sum += (hypothesis_func_value - y_val) * x_val;
        }

        return sum / static_cast<T>(rows_count);
    }


    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    grad_arr<T, NThetas> calc_grad(const theta_arr<T, NThetas>& thetas,
                                   const FuncIOVariants<T, NInputVars>& data)
    {
        grad_arr<T, NThetas> result_grad;
        for (std::size_t i = 0; i < NThetas; ++i) {
            result_grad[i] = calc_loss_deriv<T, NInputVars>(i, thetas, data);
        }

        return result_grad;
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    grad_arr<T, NThetas> calc_grad_l2(const theta_arr<T, NThetas>& thetas,
                                      const FuncIOVariants<T, NInputVars>& data,
                                      const lambda<T> lambda)
    {
        grad_arr<T, NThetas> result_grad;
        const T rows_count = data.get_variants_count();

        for (std::size_t i = 0; i < NThetas; ++i) {
            const auto reg = i == 0
                ? 0
                : *lambda * thetas[i] / static_cast<T>(rows_count);

            result_grad[i] = calc_loss_deriv(i, thetas, data) + reg;
        }

        return result_grad;
    }

    template<typename T>
    TVectorT<T> calc_grad(const theta_vec<T>& thetas,
                          const x_matrix<T>& x,
                          const x_matrix<T>& x_T,
                          const y_vec<T>& y)
    {
        assert(thetas.GetNoElements() == x.GetNcols());
        assert(x_T.GetNcols() == y.GetNoElements());

        const auto m = static_cast<T>(x.GetNrows());

        return (1 / m) * (x_T * (calc_h2(thetas, x) - y));
    }

    template<typename T>
    TVectorT<T> calc_grad_l2(const theta_vec<T>& thetas,
                             const x_matrix<T>& x,
                             const x_matrix<T>& x_T,
                             const y_vec<T>& y,
                             const lambda<T> lambda)
    {
        assert(thetas.GetNoElements() == x.GetNcols());
        assert(x_T.GetNcols() == y.GetNoElements());

        const auto steps = calc_grad(thetas, x, x_T, y);

        const auto m = static_cast<T>(x.GetNrows());
        const auto reg = (1 / m) * *lambda * zeroing_1st_el(thetas);

        return steps + reg;
    }


    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent(
        theta_arr<T, NThetas> thetas,
        const FuncIOVariants<T, NInputVars>& data,
        const alpha<T> alpha,
        const epsilon<T> epsilon = opdesc::epsilon<T>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerArr<T, NThetas> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        return opdesc::calc_grad_descent<T, NInputVars, NXVars, NThetas>(
            "Linear regression",
            thetas,
            [alpha, &data] (const theta_arr<T, NThetas>& t) {
                return calc_grad<T, NInputVars, NXVars, NThetas>(t, data);
            },
            [alpha, &data] (const theta_arr<T, NThetas>& t) {
                return calc_loss<T, NInputVars, NXVars, NThetas>(t, data);
            },
            alpha, epsilon,
            keep_intermediate_thetas, theta_handler, print_loss_values
        );
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent_l2(
        theta_arr<T, NThetas> thetas,
        const FuncIOVariants<T, NInputVars>& data,
        const alpha<T> alpha,
        const lambda<T> lambda,
        const epsilon<T> epsilon = opdesc::epsilon<T>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerArr<T, NThetas> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        return opdesc::calc_grad_descent<T, NInputVars, NXVars, NThetas>(
            "Linear regression with L2 regularization",
            thetas,
            [lambda, &data] (const theta_arr<T, NThetas>& t) {
                return calc_grad_l2<T, NInputVars>(t, data, lambda);
            },
            [lambda, &data] (const theta_arr<T, NThetas>& t) {
                return calc_loss_l2<T, NInputVars>(t, data, lambda);
            },
            alpha, epsilon,
            keep_intermediate_thetas, theta_handler, print_loss_values
        );
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent(
        theta_vec<T> thetas,
        const x_matrix<T>& x,
        const y_vec<T>& y,
        const alpha<T> alpha,
        const epsilon<T> epsilon = opdesc::epsilon<T>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerVec<T> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        assert(thetas.GetNoElements() == x.GetNcols());
        assert(static_cast<std::size_t>(thetas.GetNoElements()) == NThetas);
        assert(static_cast<std::size_t>(x.GetNcols()) == NXVars);

        const auto x_T = transpose(x);
        assert(x_T.GetNcols() == y.GetNoElements());

        return opdesc::calc_grad_descent<T, NInputVars, NXVars, NThetas>(
            "Vectorized Linear regression",
            thetas,
            [alpha, &x, &x_T, &y] (const theta_vec<T>& t) {
                return calc_grad<T>(t, x, x_T, y);
            },
            [alpha, &x, &x_T, &y] (const theta_vec<T>& t) {
                return calc_loss<T>(t, x, y);
            },
            alpha, epsilon,
            keep_intermediate_thetas, theta_handler, print_loss_values
        );
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent_l2(
        theta_vec<T> thetas,
        const x_matrix<T>& x,
        const y_vec<T>& y,
        const alpha<T> alpha,
        const lambda<T> lambda,
        const epsilon<T> epsilon = opdesc::epsilon<T>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerVec<T> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        assert(thetas.GetNoElements() == x.GetNcols());
        assert(static_cast<std::size_t>(thetas.GetNoElements()) == NThetas);
        assert(static_cast<std::size_t>(x.GetNcols()) == NXVars);

        const auto x_T = transpose(x);
        assert(x_T.GetNcols() == y.GetNoElements());

        return opdesc::calc_grad_descent<T, NInputVars, NXVars, NThetas>(
            "Vectorized Linear regression with L2 regularization",
            thetas,
            [alpha, lambda, &x, &x_T, &y] (const theta_vec<T>& t) {
                return calc_grad_l2(t, x, x_T, y, lambda);
            },
            [alpha, lambda, &x, &x_T, &y] (const theta_vec<T>& t) {
                return calc_loss_l2(t, x, y, lambda);
            },
            alpha, epsilon,
            keep_intermediate_thetas, theta_handler, print_loss_values
        );
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    MinimumDesc<T, NThetas> calc_grad_descent_analytically(
        const x_matrix<T>& x,
        const y_vec<T>& y
    )
    {
        assert(x.GetNrows() == y.GetNoElements());

        theta_vec<T> result(NThetas);
        TimeMeter timer;
        {
            const auto guard = timer.get_guard();
            const auto x_T = transpose(x);
            result = (x_T * x).Invert() * x_T * y;
        }

        return {
            "Analytical linear regression",
            std::move(result), 1, timer
        };
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    MinimumDesc<T, NThetas> calc_grad_descent_analytically_l2(
        const x_matrix<T>& x,
        const y_vec<T>& y,
        const lambda<T> lambda
    )
    {
        assert(x.GetNrows() == y.GetNoElements());

        theta_vec<T> result(NThetas);
        TimeMeter timer;
        {
            auto reg = TMatrixT<T>::UnitMatrix();
            reg(0, 0) = 0;
            reg *= *lambda;

            const auto guard = timer.get_guard();
            const auto x_T = transpose(x);
            result = (x_T * x + reg).Invert() * x_T * y;
        }

        return {
            "Analytical linear regression with L2 regularization",
            std::move(result), 1, timer
        };
    }
} // namespace ml::opdesc::linreg
#endif // ML_OPDESC_LINREG_H
