#if !(defined(ML_GRAD_DESCENT_PLOT_HELPERS_H))
#define ML_GRAD_DESCENT_PLOT_HELPERS_H

#include <TAxis.h>
#include <TCanvas.h>
#include <TLegend.h>

#include "ml/utils/io_helpers.hpp"
#include "ml/utils/plot_helpers.hpp"

#include "./minimum_desc.hpp"
#include "./grad_descent.hpp"
#include "./logreg.hpp"
#include "./linreg.hpp"


namespace ml::opdesc::plot_helpers
{
    template <typename T, std::size_t NInputVars,
              std::size_t NXVars = NInputVars + 1,
              std::size_t NThetas = NXVars>
    struct LossFuncData: utils::NamedData<T, NInputVars>
    {
        const MinimumDesc<T, NThetas>& minimum;
    };

    template <typename T, std::size_t NInputVars>
    void set_loss_plot_points(
        TGraph& graph,
        const LossFuncData<T, NInputVars>& data,
        const std::function<T(const theta_vec<T>&,
                              const x_matrix<T>&,
                              const y_vec<T>&)> loss_func,
        const std::size_t start_point_idx = 0
    )
    {
        const auto maybe_steps = data.minimum.get_vec_steps();
        if (not maybe_steps) {
            return;
        }
        auto& steps = maybe_steps->get();

        for (std::size_t i = 0; i < steps.size(); ++i) {
            const auto loss = loss_func(steps[i],
                                        data.data.get_x_matrix(),
                                        data.data.get_y_vector());
            graph.SetPoint(static_cast<Int_t>(start_point_idx + i),
                           static_cast<double>(i),
                           loss);
        }
    }

    template <typename T, std::size_t NInputVars>
    void print_loss_function_plot(
        const std::string plot_name,
        const std::filesystem::path& out_file,
        const std::vector<LossFuncData<T, NInputVars>> data,
        const std::function<T(const theta_vec<T>&,
                              const x_matrix<T>&,
                              const y_vec<T>&)> loss_func
    )
    {
        assertm(COLORS.size() >= data.size(), "Can't get new colors");

        TCanvas canvas("Canvas", "", 700, 700);
        canvas.SetGrid(1, 1);

        TLegend legend(0.5, 0.8, 0.9, 0.9);
        legend.SetHeader(plot_name.c_str());

        std::vector<TGraph> graphs;
        graphs.resize(data.size());

        std::size_t point_start_idx = 0;
        for (std::size_t i = 0; i < data.size(); ++i) {
            auto& graph = graphs[i];
            set_default_graph_conf(graph, i);

            set_loss_plot_points(graph, data[i], loss_func, point_start_idx);
            point_start_idx += data[i].minimum.get_steps_count();

            auto* const learning_sample_entry = legend.AddEntry(
                ("curve_" + std::to_string(i)).c_str(),
                ("Learning curve for " + data[i].name + " sample").c_str(),
                "l"
            );
            learning_sample_entry->SetTextColor(graph.GetMarkerColor());
        }

        draw_objects(graphs, "P");
        legend.Draw();
        save_plot(canvas, plot_name, out_file);
    }

    template<typename T, std::size_t NInputVars>
    std::function<T(const T*, const T*)> make_hyph_functor(
        const theta_vec<T>& thetas,
        const std::function<T(const theta_vec<T>&, const x_vec<T>&)> hyph
    )
    {
        return [=] (const double* args, const double*) {
            return hyph(thetas, make_x_vec<T, NInputVars>(args));
        };
    }

    template<typename T, std::size_t NInputVars>
    std::function<T(const T*, const T*)> make_hyph_functor(
        const theta_vec<T>& thetas,
        const polynom_degrees<NInputVars> degrees,
        const std::function<T(const theta_vec<T>&, const x_vec<T>&)> hyph
    )
    {
        return [=] (const double* args, const double*) {
            auto x_vec = make_x_vec<T, NInputVars>(args, degrees);
            const auto val = hyph(thetas, x_vec);
            assert(not std::isnan(val));

            return val;
        };
    }

    template<typename T, std::size_t NInputVars, std::size_t NNewInputVars>
    std::function<T(const T*, const T*)> make_hyph_functor(
        const theta_vec<T>& thetas,
        const Array<T, NNewInputVars>& in_params_avg,
        const Array<T, NNewInputVars>& in_params_rms,
        const polynom_degrees<NInputVars>& degrees,
        const std::function<T(const theta_vec<T>&, const x_vec<T>&)> hyph
    )
    {
        return [=] (const double* args, const double*) {
            auto x_vec = make_x_vec<T, NInputVars>(args, degrees);
            normalize_vec(x_vec, in_params_avg, in_params_rms);

            const auto val = hyph(thetas, x_vec);
            assert(not std::isnan(val));

            return val;
        };
    }

    template<typename T, std::size_t NInputVars>
    std::function<T(const T*, const T*)> make_logreg_hyph_functor(
        const theta_vec<T>& thetas
    )
    {
        return make_hyph_functor<T, NInputVars>(thetas, logreg::calc_h<T>);
    }


    template<typename T, std::size_t NInputVars>
    std::function<T(const T*, const T*)> make_linreg_hyph_functor(
        const theta_vec<T>& thetas
    )
    {
        return make_hyph_functor<T, NInputVars>(thetas, linreg::calc_h<T>);
    }

    template<typename T, std::size_t NInputVars, std::size_t NNewInputVars>
    std::function<T(const T*, const T*)> make_linreg_hyph_functor(
        const theta_vec<T>& thetas,
        const polynom_degrees<NInputVars>& degrees,
        const Array<T, NNewInputVars>& in_params_avg,
        const Array<T, NNewInputVars>& in_params_rms
    )
    {
        return make_hyph_functor<T, NInputVars>(
            thetas,
            in_params_avg,
            in_params_rms,
            degrees,
            linreg::calc_h<T>
        );
    }


    template <typename T, std::size_t NInputVars>
    std::size_t get_biggest_size(
        const std::vector<utils::NamedData<T, NInputVars>> data
    )
    {
        return std::max_element(
            data.begin(), data.end(),
            [](const NamedData<T, NInputVars>& a,
               const NamedData<T, NInputVars>& b) {
                return a.data.size() < b.data.size();
            }
        )->data.size();
    }

    template<typename T, std::size_t NInputVars>
    void print_learning_curve_plot(
        const std::string plot_name,
        const std::filesystem::path& out_file,
        const std::vector<utils::NamedData<T, NInputVars>> data,
        const std::function<T(const x_matrix<T>&, const y_vec<T>&)> loss_func
    )
    {
        TCanvas canvas("Canvas", "", 700, 700);
        canvas.SetGrid(1, 1);

        std::vector<TGraph> graphs(data.size());
        const auto biggest_data_size = get_biggest_size(data);

        T max_loss = 0;
        std::size_t points_offset = 0;
        for (std::size_t i = 0; i < data.size(); ++i) {
            auto& graph = graphs[i];
            set_default_graph_conf(graph, i);

            const auto& x_i = data[i].data.get_x_matrix();
            const auto& y_i = data[i].data.get_y_vector();

            for (std::size_t j = 0; j < data[i].data.get_variants_count(); ++j) {
                const auto sub_x_i = x_i.GetSub(0, static_cast<Int_t>(j),
                                                0, x_i.GetNcols() - 1);
                const auto sub_y_i = y_i.GetSub(0, static_cast<Int_t>(j));

                const auto loss = loss_func(sub_x_i, sub_y_i);
                assert(not std::isnan(loss));
                max_loss = std::max(max_loss, loss);

                graph.SetPoint(static_cast<Int_t>(points_offset + j),
                               static_cast<T>(j),
                               loss);
            }

            points_offset += data[i].data.get_variants_count();
        }

        for (std::size_t i = 0; i < data.size(); ++i) {
            auto& graph = graphs[i];
            graph.GetXaxis()->SetLimits(
                0.0,
                static_cast<Double_t>(biggest_data_size)
            );
            graph.GetXaxis()->UnZoom();

            graph.GetYaxis()->SetLimits(
                -20.0,
                static_cast<Double_t>(max_loss)
            );
            graph.GetYaxis()->UnZoom();
        }
        draw_objects(graphs, "CP");

        TLegend legend(0.5, 0.8, 0.9, 0.9);
        legend.SetHeader(plot_name.c_str());
        add_legend_entries(legend, graphs, data);
        legend.Draw();

        save_plot(canvas, plot_name, out_file);
    }
}  // namespace ml::opdesc::plot_helpers
#endif // ML_GRAD_DESCENT_PLOT_HELPERS_H
