#if !(defined(ML_OPDESC_H))
#define ML_OPDESC_H

#include <TMatrixT.h>
#include <TVectorT.h>

#include "ml/utils/comparators.hpp"
#include "ml/utils/io_helpers.hpp"
#include "ml/utils/print_helpers.hpp"
#include "ml/utils/linalg_helpers.hpp"
#include "ml/utils/time_meter.hpp"
#include "ml/utils/helper_types.hpp"

#include "./minimum_desc.hpp"

namespace ml::opdesc
{
    template<typename T, std::size_t Size>
    using start_simplex = Array<T, Size>;


    template<typename T, std::size_t NThetas>
    using grad_arr = Array<T, NThetas>;

    template<typename T, std::size_t NThetas>
    using GradFuncArr = std::function<
        grad_arr<T, NThetas>(const theta_arr<T, NThetas>&)
    >;

    template<typename T, std::size_t NThetas>
    using LossFuncArr = std::function<T(const theta_arr<T, NThetas>&)>;

    template<typename T, std::size_t NThetas>
    using TempThetaHandlerArr = std::function<void(const theta_arr<T, NThetas>&)>;


    template<typename T>
    using grad_vec = TVectorT<T>;

    template<typename T>
    using GradFuncVec = std::function<grad_vec<T>(const theta_vec<T>&)>;

    template<typename T>
    using LossFuncVec = std::function<T(const theta_vec<T>&)>;

    template<typename T>
    using TempThetaHandlerVec = std::function<void(const theta_vec<T>&)>;


    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent(
        const std::string& algo_name,
        theta_arr<T, NThetas> thetas,
        const GradFuncArr<T, NThetas> grad_func,
        const LossFuncArr<T, NThetas> loss_func,
        const utils::alpha<T> alpha,
        const utils::epsilon<double> epsilon = utils::epsilon<double>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerArr<T, NThetas> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        uint64_t steps_count = 1;
        theta_arr_descent<T, NThetas> steps;
        if (keep_intermediate_thetas) {
            steps.push_back(thetas);
        }

        TimeMeter timer;
        {
            auto time_guard = timer.get_guard();

            while (true) {
                const auto temp_thetas = thetas - grad_func(thetas) * *alpha;
                if (theta_handler) {
                    theta_handler(temp_thetas);
                }

                steps_count++;
                if (keep_intermediate_thetas) {
                    steps.push_back(temp_thetas);
                }

                if (print_loss_values) {
                    std::cout << "LOSS: " << loss_func(temp_thetas)
                              << std::endl;
                }

                if (is_equal(temp_thetas, thetas, loss_func,
                             EpsilonComparator(epsilon))) {
                    thetas = std::move(temp_thetas);
                    break;
                }

                if (is_contain_nan(temp_thetas)) {
                    return std::nullopt;;
                }

                thetas = std::move(temp_thetas);
            }
        }

        if (keep_intermediate_thetas) {
            return MinimumDesc<T, NThetas>(algo_name, steps, timer);
        } else {
            return MinimumDesc<T, NThetas>(algo_name, thetas,
                                           steps_count, timer);
        }
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent_momentum(
        const std::string& algo_name,
        theta_arr<T, NThetas> thetas,
        const GradFuncArr<T, NThetas> grad_func,
        const LossFuncArr<T, NThetas> loss_func,
        const utils::alpha<T> alpha,
        const utils::beta<T> beta,
        const utils::epsilon<double> epsilon = utils::epsilon<double>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerVec<T> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        uint64_t steps_count = 1;
        theta_arr_descent<T, NThetas> steps;
        if (keep_intermediate_thetas) {
            steps.push_back(thetas);
        }

        TimeMeter timer;
        {
            auto time_guard = timer.get_guard();
            Array<T, NThetas> velocity(0);

            while (true) {
                velocity = velocity * *beta + grad_func(thetas) * (1 - *beta);
                auto temp_thetas = thetas - velocity * *alpha;
                if (theta_handler) {
                    theta_handler(temp_thetas);
                }

                steps_count++;
                if (keep_intermediate_thetas) {
                    steps.push_back(temp_thetas);
                }

                if (print_loss_values) {
                    std::cout << "LOSS: " << loss_func(temp_thetas)
                              << std::endl;
                }

                if (is_equal(temp_thetas, thetas, loss_func,
                             EpsilonComparator(epsilon))) {
                    thetas = std::move(temp_thetas);
                    break;
                }

                if (is_contain_nan(temp_thetas)) {
                    return std::nullopt;;
                }

                thetas = std::move(temp_thetas);
            }
        }

        if (keep_intermediate_thetas) {
            return MinimumDesc<T, NThetas>(algo_name, steps, timer);
        } else {
            return MinimumDesc<T, NThetas>(algo_name, thetas,
                                           steps_count, timer);
        }
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent(
        const std::string& algo_name,
        theta_vec<T> thetas,
        const GradFuncVec<T> grad_func,
        const LossFuncVec<T> loss_func,
        const utils::alpha<T> alpha,
        const utils::epsilon<double> epsilon = utils::epsilon<double>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerVec<T> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        assert(static_cast<std::size_t>(thetas.GetNoElements()) == NThetas);

        uint64_t steps_count = 1;
        theta_vec_descent<T> steps;
        if (keep_intermediate_thetas) {
            steps.push_back(thetas);
        }

        TimeMeter timer;
        {
            auto time_guard = timer.get_guard();

            while (true) {
                auto re = grad_func(thetas);
                auto k = *alpha * re;
                auto temp_thetas = thetas - k;
                if (theta_handler) {
                    theta_handler(temp_thetas);
                }

                steps_count++;
                if (keep_intermediate_thetas) {
                    steps.push_back(temp_thetas);
                }

                if (print_loss_values) {
                    std::cout << "LOSS: " << loss_func(temp_thetas)
                              << std::endl;
                }

                // is_equal(temp_thetas, thetas, loss_func,
                //           EpsilonComparator(epsilon))
                if ((thetas - temp_thetas).Abs().Sum() <= *epsilon) {
                    // thetas = std::move(temp_thetas);
                    break;
                }

                if (is_contain_nan(temp_thetas)) {
                    return std::nullopt;;
                }

                thetas = std::move(temp_thetas);
            }
        }

        if (keep_intermediate_thetas) {
            return MinimumDesc<T, NThetas>(algo_name, steps, timer);
        } else {
            return MinimumDesc<T, NThetas>(algo_name, thetas,
                                           steps_count, timer);
        }
    }

    // Calc gradient descent momentum
    // https://medium.com/@omkar.nallagoni/gradient-descent-with-momentum-73a8ae2f0954
    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent_momentum(
        const std::string& algo_name,
        theta_vec<T> thetas,
        const GradFuncVec<T> grad_func,
        const LossFuncVec<T> loss_func,
        const utils::alpha<T> alpha,
        const utils::beta<T> beta = utils::beta<T>(0.9),
        const utils::epsilon<double> epsilon = utils::epsilon<double>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerVec<T> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        assert(static_cast<std::size_t>(thetas.GetNoElements()) == NThetas);

        uint64_t steps_count = 1;
        theta_vec_descent<T> steps;
        if (keep_intermediate_thetas) {
            steps.push_back(thetas);
        }

        TimeMeter timer;
        {
            auto time_guard = timer.get_guard();
            TVectorT<T> velocity = make_zero_vec<T>(thetas.GetNrows());

            while (true) {
                velocity = *beta * velocity + (1 - *beta) * grad_func(thetas);
                auto temp_thetas = thetas - *alpha * velocity;
                if (theta_handler) {
                    theta_handler(temp_thetas);
                }

                steps_count++;
                if (keep_intermediate_thetas) {
                    steps.push_back(temp_thetas);
                }

                if (print_loss_values) {
                    std::cout << "LOSS: " << loss_func(temp_thetas)
                              << std::endl;
                }

                if (is_equal(temp_thetas, thetas, loss_func,
                             EpsilonComparator(epsilon))) {
                    thetas = std::move(temp_thetas);
                    break;
                }

                if (is_contain_nan(temp_thetas)) {
                    return std::nullopt;;
                }

                thetas = std::move(temp_thetas);
            }
        }

        if (keep_intermediate_thetas) {
            return MinimumDesc<T, NThetas>(algo_name, steps, timer);
        } else {
            return MinimumDesc<T, NThetas>(algo_name, thetas,
                                               steps_count, timer);
        }
    }


    template<typename T, std::size_t NInputVars>
    Array<T, NInputVars> calc_params_avg_values(
        const FuncIOVariants<T, NInputVars>& data
    )
    {
        Array<double, NInputVars> result;
        for (std::size_t in_idx = 0; in_idx < NInputVars; ++in_idx) {
            T sum = 0.0;
            for (std::size_t var_idx = 0; var_idx < data.size(); ++var_idx) {
                sum += data[var_idx][in_idx];
            }

            result[in_idx] = sum / static_cast<T>(data.size());
        }

        return result;
    }

    template<typename T, std::size_t NInputVars>
    Array<T, NInputVars> calc_params_rms_deviation(
        const FuncIOVariants<T, NInputVars>& data,
        const Array<T, NInputVars>& in_params_avg_values
    )
    {
        Array<T, NInputVars> result;
        for (std::size_t var_idx = 0; var_idx < NInputVars; ++var_idx) {
            const auto avg_value = in_params_avg_values[var_idx];

            T sum = 0.0;
            for (std::size_t i = 0; i < data.size(); ++i) {
                const auto in = data[i][var_idx];
                sum += std::pow(in - avg_value, 2);
            }

            result[var_idx] = std::sqrt(sum / static_cast<T>(data.size()));
        }

        return result;
    }

    template<typename T, std::size_t NInputVars>
    Array<T, NInputVars> calc_params_ranges(
        const FuncIOVariants<T, NInputVars>& data
    )
    {
        Array<T, NInputVars> result;
        for (std::size_t var_idx = 0; var_idx < NInputVars; ++var_idx) {
            const auto min = data.get_minimal(var_idx);
            const auto max = data.get_maximal(var_idx);

            result[var_idx] = max - min;
            if (result[var_idx] == 0) {
                result[var_idx] = 1;
            }
        }

        return result;
    }

    template<typename T, std::size_t NInputVars>
    void normalize_vec(
        x_vec<T>& vec,
        const Array<T, NInputVars>& in_params_avg,
        const Array<T, NInputVars>& in_params_rms
    )
    {
        for (std::size_t in_idx = 1; in_idx < NInputVars + 1; ++in_idx) {
            const auto var = vec[static_cast<Int_t>(in_idx)];
            const auto avg = in_params_avg[in_idx - 1];
            const auto rms = in_params_rms[in_idx - 1];

            vec[static_cast<Int_t>(in_idx)] = (var - avg) / rms;
        }
    }

    template<typename T, std::size_t NInputVars>
    x_vec<T> normalize_vec(
        const x_vec<T>& vec,
        const Array<T, NInputVars>& in_params_avg,
        const Array<T, NInputVars>& in_params_rms
    )
    {
        x_vec<T> result(vec.GetNrows());

        result[0] = 1;
        for (std::size_t in_idx = 1; in_idx < NInputVars + 1; ++in_idx) {
            const auto var = vec[in_idx];
            const auto avg_value = in_params_avg[in_idx - 1];
            const auto rms_deviation = in_params_rms[in_idx - 1];

            result[in_idx] = (var - avg_value) / rms_deviation;
        }

        return result;
    }

    template<typename T, std::size_t NInputVars>
    Variant<T, NInputVars> normalize_data(
        const Variant<T, NInputVars>& variant,
        const Array<T, NInputVars>& in_params_avg,
        const Array<T, NInputVars>& in_params_rms
    )
    {
        Array<T, NInputVars> norm_in_params;
        for (std::size_t in_idx = 0; in_idx < NInputVars; ++in_idx) {
            const auto var = variant[in_idx];
            const auto avg_value = in_params_avg[in_idx];
            const auto rms_deviation = in_params_rms[in_idx];

            norm_in_params[in_idx] = (var - avg_value) / rms_deviation;
        }

        return {norm_in_params, variant.out()};
    }

    template<typename T, std::size_t NInputVars>
    FuncIOVariants<T, NInputVars> normalize_data(
        const FuncIOVariants<T, NInputVars>& data,
        const Array<T, NInputVars> in_params_avg,
        const Array<T, NInputVars> in_params_rms
    )
    {
        FuncIOVariants<T, NInputVars> result;
        for (std::size_t i = 0; i < data.size(); ++i) {
            result.emplace_back(
                normalize_data(data[i], in_params_avg, in_params_rms)
            );
        }

        return result;
    }

    template<typename T, std::size_t NInputVars>
    FuncIOVariants<T, NInputVars> normalize_data(
        const FuncIOVariants<T, NInputVars>& data
    )
    {
        const auto params_avg_values = calc_params_avg_values(data);

        const auto params_rms_deviation = calc_params_rms_deviation(
            data,
            params_avg_values
        );

        return normalize_data(data, params_avg_values, params_rms_deviation);
    }
} // namespace ml::opdesc
#endif // ML_OPDESC_H
