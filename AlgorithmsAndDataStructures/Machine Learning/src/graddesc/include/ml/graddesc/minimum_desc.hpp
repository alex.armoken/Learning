#if !(defined(ML_GRAD_DESCENT_DESC_H))
#define ML_GRAD_DESCENT_DESC_H

#include <array>
#include <iostream>

#include <TCanvas.h>
#include <TVectorT.h>

#include "ml/utils/time_meter.hpp"
#include "ml/utils/helper_types.hpp"
#include "ml/utils/io_helpers.hpp"
#include "ml/utils/plot_helpers.hpp"


namespace ml::opdesc
{
    using namespace utils;

    template<typename T, std::size_t NThetas>
    using theta_arr = Array<T, NThetas>;

    template<typename T, std::size_t NThetas>
    using theta_arr_descent = std::vector<theta_arr<T, NThetas>>;

    template<typename T>
    using theta_vec = TVectorT<T>;

    template<typename T>
    using theta_matrix = TMatrixT<T>;

    template<typename T>
    using theta_vec_descent = std::vector<TVectorT<T>>;

    template<typename T, std::size_t NThetas>
    class MinimumDesc
    {
    public:
        MinimumDesc(
            const std::string& algo_name,
            const theta_arr<T, NThetas>& minimum,
            const uint64_t steps_count,
            const TimeMeter& timer
        ) : _algo_name(algo_name)
          , _arr_min(std::make_unique<theta_arr<T, NThetas>>(minimum))
          , _steps_count(steps_count)
          , _duration(timer.get_duration())
        {
        }

        MinimumDesc(
            const std::string& algo_name,
            const theta_vec<T>& minimum,
            const uint64_t steps_count,
            const TimeMeter& timer
        ) : _algo_name(algo_name), _steps_count(steps_count)
        {
            _vec_min = std::make_unique<theta_vec<T>>(minimum);
            _duration = timer.get_duration();
        }

        MinimumDesc(
            const std::string& algo_name,
            theta_vec<T>&& minimum,
            const uint64_t steps_count,
            const TimeMeter& timer
        ) : _algo_name(algo_name), _steps_count(steps_count)
        {
            _vec_min = std::make_unique<theta_vec<T>>(std::move(minimum));
            _duration = timer.get_duration();
        }


        MinimumDesc(
            const std::string& algo_name,
            const theta_vec_descent<T>& steps,
            const TimeMeter& timer
        ) : _algo_name(algo_name)
          , _steps_count(steps.size())
          , _vec_steps(steps)
        {
            _vec_min = std::make_unique<theta_vec<T>>(steps.back());
            _duration = timer.get_duration();
        }

        MinimumDesc(
            const std::string& algo_name,
            theta_vec_descent<T>&& steps,
            const TimeMeter& timer
        ) : _algo_name(algo_name)
          , _steps_count(steps.size())
          , _vec_steps(std::move(steps))
        {
            _vec_min = std::make_unique<theta_vec<T>>(steps.back());
            _duration = timer.get_duration();
        }

        MinimumDesc(
            const std::string& algo_name,
            const theta_arr_descent<T, NThetas>& steps,
            const TimeMeter& timer
        ) : _algo_name(algo_name)
          , _steps_count(steps.size())
          , _arr_steps(steps)
        {
            _arr_min = std::make_unique<theta_arr<T, NThetas>>(steps.back());
            _duration = timer.get_duration();
        }


        static std::optional<MinimumDesc<T, NThetas>> read(
            const std::filesystem::path& path
        )
        {

        }

        void save(const std::filesystem::path& path) const
        {

        }


        template<typename V = std::chrono::seconds>
        int64_t get_duration_length() const
        {
            return std::chrono::duration_cast<V>(_duration).count();
        }

        uint64_t get_steps_count() const { return _steps_count; }


        const theta_vec<T>& get_vec_minimum() const
        {
            if (not _vec_min) {
                auto& vec_min
                    = const_cast<MinimumDesc<T, NThetas>*>(this)->_vec_min;
                vec_min = std::make_unique<theta_vec<T>>(_to_vec(**_arr_min));
            }

            return **_vec_min;
        }

        const theta_arr<T, NThetas>& get_arr_minimum() const
        {
            if (not _arr_min) {
                auto& arr_min
                    = const_cast<MinimumDesc<T, NThetas>*>(this)->_arr_min;
                arr_min = std::make_unique<theta_arr<T,NThetas>>(
                    _to_arr(**_vec_min)
                );
            }

            return **_arr_min;
        }

        std::optional<std::reference_wrapper<const theta_vec_descent<T>>>
        get_vec_steps() const
        {
            if (not _vec_steps) {
                if (not _arr_steps) {
                    return std::nullopt;
                }

                const_cast<MinimumDesc<T, NThetas>*>(this)->_vec_steps
                    = _to_vec_of_vectors(*_arr_steps);
            }

            return std::ref(*_vec_steps);
        }

        std::optional<std::reference_wrapper<const theta_arr_descent<T, NThetas>>>
        get_arr_steps() const
        {
            if (not _arr_steps) {
                if (not _vec_steps) {
                    return std::nullopt;
                }

                const_cast<MinimumDesc<T, NThetas>*>(this)->_arr_steps
                    = _to_vec_of_arrays(*_vec_steps);
            }

            return std::ref(*_vec_steps);
        }

        T operator[](const std::size_t idx) const
        {
            assert(idx < NThetas);

            if (_vec_min) {
                return (**_vec_min)[static_cast<int>(idx)];
            }

            return (**_arr_min)[idx];
        }

        void print_info(const std::string& prefix) const
        {
            const auto intro = prefix
                + " minimum computed by "
                + _algo_name + ": ";
            if (_vec_min) {
                print_vec(**_vec_min, intro);
            } else {
                print_els2(**_arr_min, intro);
            }

            std::cout << "Iterations count: "
                      << this->get_steps_count() << "\n";
            std::cout << "It took " << this->get_duration_length()
                      << " seconds" << std::endl;
            std::cout << std::endl;
        }

    private:
        const std::string _algo_name;

        std::optional<std::unique_ptr<theta_vec<T>>> _vec_min;
        std::optional<std::unique_ptr<theta_arr<T, NThetas>>> _arr_min;

        uint64_t _steps_count = 0;

        std::optional<theta_vec_descent<T>> _vec_steps;
        std::optional<theta_arr_descent<T, NThetas>> _arr_steps;

        std::chrono::nanoseconds _duration;

        static theta_vec<T> _to_vec(const theta_arr<T, NThetas>& arr)
        {
            theta_vec<T> result(NThetas);
            for (int i = 0; i < static_cast<int>(NThetas); ++i) {
                result[i] = arr[static_cast<std::size_t>(i)];
            }

            return result;
        }

        static theta_arr<T, NThetas> _to_arr(const theta_vec<T>& vec)
        {
            theta_arr<T, NThetas> result;
            for (int i = 0; i < static_cast<int>(NThetas); ++i) {
                result[static_cast<std::size_t>(i)] = vec[i];
            }

            return result;
        }

        static theta_arr_descent<T, NThetas> _to_vec_of_arrays(
            const theta_vec_descent<T>& descent
        )
        {
            theta_arr_descent<T, NThetas> result;
            std::transform(descent.begin(), descent.end(),
                           result.begin(), _to_arr);

            return result;
        }

        static theta_vec_descent<T> _to_vec_of_vectors(
            const theta_arr_descent<T, NThetas>& descent
        )
        {
            theta_vec_descent<T> result;
            std::transform(descent.begin(), descent.end(),
                           result.begin(), _to_vec);

            return result;
        }
    };
}
#endif // ML_GRAD_DESCENT_DESC_H
