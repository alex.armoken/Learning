#if !(defined(ML_OPDESC_LOGREG_H))
#define ML_OPDESC_LOGREG_H

#include <array>
#include <cstddef>
#include <chrono>
#include <vector>
#include <numeric>

#include <Math/Functor.h>
#include <Math/Factory.h>
#include <Math/Minimizer.h>
#include <Math/MultiNumGradFunction.h>
#include <TMatrixT.h>
#include <TVectorT.h>

#include "ml/utils/comparators.hpp"
#include "ml/utils/io_helpers.hpp"
#include "ml/utils/print_helpers.hpp"
#include "ml/utils/exceptions.hpp"
#include "ml/utils/linalg_helpers.hpp"
#include "ml/utils/time_meter.hpp"
#include "ml/utils/helper_types.hpp"

#include "./minimum_desc.hpp"
#include "./grad_descent.hpp"

namespace ml::opdesc::logreg
{
    using namespace utils;

    template<typename T>
    T calc_h(const theta_vec<T>& thetas, const x_vec<T>& x)
    {
        assert(AreCompatible(thetas, x));

        return sigmoid(Dot(thetas, x));
    }

    template<typename T>
    TVectorT<T> calc_h2(const theta_vec<T>& thetas, const x_matrix<T>& x)
    {
        TVectorT<T> result(x.GetNrows());
        for (int32_t i = 0; i < result.GetNrows(); ++i) {
            const auto x_i = get_row(x, i);
            result(i) = calc_h(thetas, x_i);
        }

        return result;
    }

    template <typename T>
    T calc_loss(const theta_vec<T>& thetas,
                const x_matrix<T>& x,
                const y_vec<T>& y)
    {
        const auto m = x.GetNrows();

        T sum = 0;
        for (int32_t i = 0; i < m; ++i) {
            const auto x_i = get_row(x, i);
            const T y_i = y[i];
            const T h = calc_h(thetas, x_i);

            constexpr T EPS = 1e-8;
            T cost_1 = y_i * std::log(h + EPS);
            T cost_2 = (1 - y_i) * std::log(1 - h + EPS);
            sum += cost_1 + cost_2;
        }

        return -sum / static_cast<T>(m);
    }

    template <typename T>
    T calc_loss_l2(const theta_vec<T>& thetas,
                   const x_matrix<T>& x, const y_vec<T>& y,
                   const lambda<T> lambda)
    {
        const auto loss = calc_loss(thetas, x, y);

        const auto reg = *lambda * (Dot(thetas, thetas)
                                    - std::pow(thetas[0], 2))
                         / (static_cast<T>(2 * x.GetNrows()));

        return loss + reg;
    }

    template<typename T>
    T calc_loss_deriv(const theta_vec<T>& thetas,
                      const x_matrix<T>& x, const y_vec<T>& y,
                      const std::size_t deriv_idx)
    {
        const auto rows_count = x.GetNrows();

        T sum = 0;
        for (int32_t row_idx = 0; row_idx < rows_count; ++row_idx) {
            const auto& x_vars = get_row(x, row_idx);
            const auto x_val =
                x_vars[static_cast<int32_t>(deriv_idx)];
            const auto y_val = y[row_idx];
            const auto h = calc_h(thetas, x_vars);

            sum += x_val * (h - y_val);
        }

        return sum / (static_cast<T>(rows_count));
    }

    template<typename T>
    T calc_loss_deriv_l2(const theta_vec<T>& thetas,
                         const x_matrix<T>& x, const y_vec<T>& y,
                         const lambda<T> lambda,
                         const std::size_t deriv_idx)
    {
        const auto loss_deriv = calc_loss_deriv(thetas, x, y, deriv_idx);

        const auto theta_with_deriv_idx = deriv_idx == 0
            ? 0
            : thetas[static_cast<int32_t>(deriv_idx)];
        const auto reg = *lambda * theta_with_deriv_idx
            / static_cast<T>(x.GetNrows());

        return loss_deriv + reg;
    }

    template<typename T>
    TVectorT<T> calc_grad(const theta_vec<T>& thetas,
                          const x_matrix<T>& x,
                          const x_matrix<T>& x_T,
                          const y_vec<T>& y)
    {
        const auto m = static_cast<T>(x.GetNrows());

        return (1 / m) * x_T * (calc_h2(thetas, x) - y);
    }

    template<typename T>
    TVectorT<T> calc_grad_l2(const theta_vec<T>& thetas,
                             const x_matrix<T>& x,
                             const x_matrix<T>& x_T,
                             const y_vec<T>& y,
                             const lambda<T> lambda)
    {
        const auto grad = calc_grad(thetas, x, x_T, y);

        const auto m = static_cast<T>(x.GetNrows());
        auto reg = (*lambda / m) * thetas;
        reg[0] = 0;

        return grad + reg;
    }


    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent(
        theta_vec<T> thetas,
        const x_matrix<T>& x, const y_vec<T>& y,
        const alpha<T> alpha,
        const epsilon<T> epsilon = opdesc::epsilon<T>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerVec<T> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        const auto x_T = transpose(x);

        return opdesc::calc_grad_descent_momentum<T, NInputVars>(
            "Logistic regression",
            thetas,
            [=, &x, &x_T, &y](const theta_vec<T>& t) -> TVectorT<T> {
                return calc_grad(t, x, x_T, y);
            },
            [&x, &x_T, &y](const theta_vec<T>& t) -> double {
                return calc_loss(t, x, y);
            },
            alpha, beta<T>(0.9), epsilon,
            keep_intermediate_thetas,
            theta_handler,
            print_loss_values
        );
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent_l2(
        theta_vec<T> thetas,
        const x_matrix<T>& x, const y_vec<T>& y,
        const alpha<T> alpha, const lambda<T> lambda,
        const epsilon<T> epsilon = opdesc::epsilon<T>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerVec<T> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        const auto x_T = transpose(x);

        return opdesc::calc_grad_descent<T, NInputVars>(
            "Logistic regression with L2 regularization",
            thetas,
            [=, &x, &x_T, &y] (const theta_vec<T>& t) {
                return calc_grad_l2(t, x, x_T, y, lambda);
            },
            [=, &x, &x_T, &y] (const theta_vec<T>& t) {
                return calc_loss_l2(t, x, y, lambda);
            },
            alpha, epsilon,
            keep_intermediate_thetas,
            theta_handler,
            print_loss_values
        );
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent_momentum(
        theta_vec<T> thetas,
        const x_matrix<T>& x, const y_vec<T>& y,
        const alpha<T> alpha,
        const beta<T> beta = opdesc::beta<T>(0.9),
        const epsilon<T> epsilon = opdesc::epsilon<T>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerVec<T> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        const auto x_T = transpose(x);

        return opdesc::calc_grad_descent_momentum<T, NInputVars>(
            "Logistic regression",
            thetas,
            [=, &x, &x_T, &y](const theta_vec<T>& t) -> TVectorT<T> {
                return calc_grad(t, x, x_T, y);
            },
            [&x, &x_T, &y](const theta_vec<T>& t) -> double {
                return calc_loss(t, x, y);
            },
            alpha, beta, epsilon,
            keep_intermediate_thetas,
            theta_handler,
            print_loss_values
        );
    }

    template<typename T, std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars>
    std::optional<MinimumDesc<T, NThetas>> calc_grad_descent_l2_momentum(
        theta_vec<T> thetas,
        const x_matrix<T>& x, const y_vec<T>& y,
        const alpha<T> alpha,
        const lambda<T> lambda,
        const beta<T> beta = opdesc::beta<T>(0.9),
        const epsilon<T> epsilon = opdesc::epsilon<T>(1e-8),
        const bool keep_intermediate_thetas = false,
        const TempThetaHandlerVec<T> theta_handler = nullptr,
        const bool print_loss_values = false
    )
    {
        const auto x_T = transpose(x);

        return opdesc::calc_grad_descent_momentum<T, NInputVars>(
            "Logistic regression with L2 regularization and with Momentum",
            thetas,
            [=, &x, &x_T, &y] (const theta_vec<T>& t) {
                return calc_grad_l2(t, x, x_T, y, lambda);
            },
            [=, &x, &x_T, &y] (const theta_vec<T>& t) {
                return calc_loss_l2(t, x, y, lambda);
            },
            alpha, beta, epsilon,
            keep_intermediate_thetas,
            theta_handler,
            print_loss_values
        );
    }


    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars,
             std::size_t NDims = NXVars + 1>
    std::optional<MinimumDesc<double, NThetas>> call_bfgs_minimizer(
        const start_simplex<double, NThetas>& start_simplex,
        const x_matrix<double>& x, const y_vec<double>& y,
        const alpha<double> alpha,
        const epsilon<double> epsilon = opdesc::epsilon<double>(1e-8),
        const bool keep_intermediate_thetas = true,
        const TempThetaHandlerVec<double> theta_handler = nullptr
    )
    {
        const std::unique_ptr<ROOT::Math::Minimizer> minimizer(
            ROOT::Math::Factory::CreateMinimizer(
                "GSLMultiMin", "BFGS"
            )
        );
        if (not minimizer) {
            return std::nullopt;
        }

        minimizer->SetMaxFunctionCalls(1'000'000'000);
        minimizer->SetMaxIterations(100'000'000);

        minimizer->SetTolerance(1e-8);
        minimizer->SetPrecision(*epsilon);
        minimizer->SetPrintLevel(0);

        theta_vec_descent<double> steps;
        if (keep_intermediate_thetas) {
            steps.push_back(make_vec(start_simplex));
        }

        ROOT::Math::GradFunctor min_func(
            [&] (const double* const theta_ptr) {
                const theta_vec<double> thetas(NThetas, theta_ptr);

                if (theta_handler) {
                    theta_handler(thetas);
                }

                if (keep_intermediate_thetas) {
                    steps.push_back(thetas);
                }

                const auto cost = calc_loss<double>(thetas, x, y);
                if (std::isnan(cost)) {
                    throw NanException();
                }

                return cost;
            },
            [&] (const double* const theta_ptr, int32_t icoord) {
                const auto result = calc_loss_deriv<double>(
                    theta_vec<double>(NThetas, theta_ptr), x, y,
                    static_cast<std::size_t>(icoord)
                );
                if (std::isnan(result)) {
                    throw NanException();
                }

                return result;
            },
            NThetas
        );
        minimizer->SetFunction(min_func);

        for (uint32_t i = 0; i < static_cast<uint32_t>(NThetas); ++i) {
            minimizer->SetVariable(i, "var_" + std::to_string(i),
                                   start_simplex[i], *alpha);
        }

        TimeMeter timer;
        try {
            {
                const auto time_guard = timer.get_guard();
                minimizer->Minimize();
            }
        } catch (const NanException&) {
            return std::nullopt;
        }

        if (keep_intermediate_thetas) {
            steps.emplace_back(NThetas, minimizer->X());

            return MinimumDesc<double, NThetas>("BFGS", steps, timer);
        } else {
            return MinimumDesc<double, NThetas>(
                "BFGS", TVectorT<double>(NThetas, minimizer->X()),
                minimizer->NIterations(),
                timer
            );
        }
    }

    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars,
             std::size_t NDims = NXVars + 1>
    std::optional<MinimumDesc<double, NThetas>> call_nma_minimizer(
        const start_simplex<double, NDims>& start_simplex,
        const x_matrix<double>& x, const y_vec<double>& y,
        const alpha<double> alpha,
        const epsilon<double> epsilon = opdesc::epsilon<double>(1e-8),
        const bool keep_intermediate_thetas = true,
        const TempThetaHandlerVec<double> theta_handler = nullptr
    )
    {
        const std::unique_ptr<ROOT::Math::Minimizer> minimizer(
            ROOT::Math::Factory::CreateMinimizer(
                "Minuit2", "Simplex"
            )
        );
        if (not minimizer) {
            return std::nullopt;
        }

        minimizer->SetMaxFunctionCalls(1'000'000'000);
        minimizer->SetMaxIterations(100'000'000);

        minimizer->SetTolerance(1e-8);
        minimizer->SetPrecision(*epsilon);
        minimizer->SetPrintLevel(0);

        theta_vec_descent<double> steps;
        if (keep_intermediate_thetas) {
            steps.emplace_back(make_vec(start_simplex, nfirst(NThetas)));
        }

        ROOT::Math::GradFunctor min_func(
            [&] (const double * const theta_ptr) {
                const theta_vec<double> thetas(NThetas, theta_ptr);

                if (theta_handler) {
                    theta_handler(thetas);
                }
                if (keep_intermediate_thetas) {
                    steps.push_back(thetas);
                }

                const auto cost = calc_loss<double>(thetas, x, y);
                if (std::isnan(cost)) {
                    throw NanException();
                }

                return cost;
            },
            [&] (const double * const theta_ptr, int32_t icoord) {
                const auto result = calc_loss_deriv<double>(
                    theta_vec<double>(NThetas, theta_ptr), x, y,
                    static_cast<std::size_t>(icoord)
                );
                if (std::isnan(result)) {
                    throw NanException();
                }

                return result;
            },
            NThetas
        );
        minimizer->SetFunction(min_func);

        for (uint32_t i = 0; i < static_cast<uint32_t>(NDims); ++i) {
            minimizer->SetVariable(i, "var_" + std::to_string(i),
                                   start_simplex[i], *alpha);
        }

        TimeMeter timer;
        try {
            {
                const auto time_guard = timer.get_guard();
                minimizer->Minimize();
            }
        } catch (const NanException&) {
            return std::nullopt;
        }

        if (keep_intermediate_thetas) {
            steps.emplace_back(NThetas, minimizer->X());
            return MinimumDesc<double, NThetas>(
                "Nelder-Mead method", steps, timer
            );
        } else {
            return MinimumDesc<double, NThetas>(
                "Nelder-Mead method",
                theta_vec<double>(NThetas, minimizer->X()),
                minimizer->NIterations(),
                timer
            );
        }
    }

    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars,
             std::size_t NDims = NXVars + 1>
    std::optional<MinimumDesc<double, NThetas>> call_bfgs_minimizer_l2(
        const start_simplex<double, NThetas>& start_simplex,
        const x_matrix<double>& x, const y_vec<double>& y,
        const alpha<double> alpha, const lambda<double> lambda,
        const epsilon<double> epsilon = opdesc::epsilon<double>(1e-8),
        const bool keep_intermediate_thetas = true,
        const TempThetaHandlerVec<double> theta_handler = nullptr
    )
    {
        const std::unique_ptr<ROOT::Math::Minimizer> minimizer(
            ROOT::Math::Factory::CreateMinimizer(
                "GSLMultiMin", "BFGS"
            )
        );
        if (not minimizer) {
            return std::nullopt;
        }

        minimizer->SetMaxFunctionCalls(1'000'000'000);
        minimizer->SetMaxIterations(100'000'000);

        minimizer->SetTolerance(*epsilon);
        minimizer->SetPrecision(*epsilon);
        minimizer->SetPrintLevel(-1);

        theta_vec_descent<double> steps;
        if (keep_intermediate_thetas) {
            steps.push_back(make_vec(start_simplex));
        }

        ROOT::Math::GradFunctor min_func(
            [&] (const double * const theta_ptr) {
                const theta_vec<double> thetas(NThetas, theta_ptr);

                if (theta_handler) {
                    theta_handler(thetas);
                }
                if (keep_intermediate_thetas) {
                    steps.push_back(thetas);
                }

                const auto cost = calc_loss_l2<double>(thetas, x, y, lambda);
                if (std::isnan(cost)) {
                    throw NanException();
                }

                return cost;
            },
            [&] (const double * const theta_ptr, int32_t icoord) {
                const auto result = calc_loss_deriv_l2<double>(
                    theta_vec<double>(NThetas, theta_ptr), x, y,
                    lambda, static_cast<std::size_t>(icoord)
                );
                if (std::isnan(result)) {
                    throw NanException();
                }

                return result;
            },
            NThetas
        );
        minimizer->SetFunction(min_func);

        for (uint32_t i = 0; i < static_cast<uint32_t>(NDims); ++i) {
            minimizer->SetVariable(i, "var_" + std::to_string(i),
                                   start_simplex[i], *alpha);
        }

        TimeMeter timer;
        try {
            {
                const auto time_guard = timer.get_guard();
                minimizer->Minimize();
            }
        } catch (const NanException&) {
            return std::nullopt;
        }

        if (keep_intermediate_thetas) {
            steps.emplace_back(NThetas, minimizer->X());

            return MinimumDesc<double, NThetas>(
                "BFGS with L2 regularization", steps, timer);
        } else {
            return MinimumDesc<double, NThetas>(
                "BFGS with L2 regularization",
                TVectorT<double>(NThetas, minimizer->X()),
                minimizer->NIterations(),
                timer
            );
        }
    }

    template<std::size_t NInputVars,
             std::size_t NXVars = NInputVars + 1,
             std::size_t NThetas = NXVars,
             std::size_t NDims = NXVars + 1>
    std::optional<MinimumDesc<double, NThetas>> call_nma_minimizer_l2(
        const start_simplex<double, NDims>& start_simplex,
        const x_matrix<double>& x, const y_vec<double>& y,
        const alpha<double> alpha, const lambda<double> lambda,
        const epsilon<double> epsilon = opdesc::epsilon<double>(1e-8),
        const bool keep_intermediate_thetas = true,
        const TempThetaHandlerVec<double> theta_handler = nullptr
    )
    {
        const std::unique_ptr<ROOT::Math::Minimizer> minimizer(
            ROOT::Math::Factory::CreateMinimizer(
                "Minuit2", "Simplex"
            )
        );
        if (not minimizer) {
            return std::nullopt;
        }

        minimizer->SetMaxFunctionCalls(1'000'000'000);
        minimizer->SetMaxIterations(100'000'000);

        minimizer->SetTolerance(*epsilon);
        minimizer->SetPrecision(*epsilon);
        minimizer->SetPrintLevel(-1);

        theta_vec_descent<double> steps;
        if (keep_intermediate_thetas) {
            steps.emplace_back(make_vec(start_simplex, nfirst(NThetas)));
        }

        ROOT::Math::GradFunctor min_func(
            [&] (const double * const theta_ptr) {
                const theta_vec<double> thetas(NThetas, theta_ptr);

                if (theta_handler) {
                    theta_handler(thetas);
                }
                if (keep_intermediate_thetas) {
                    steps.push_back(thetas);
                }

                const auto cost = calc_loss_l2<double>(thetas, x, y, lambda);
                if (std::isnan(cost)) {
                    throw NanException();
                }

                return cost;
            },
            [&] (const double * const thetas, int32_t icoord) {
                const auto result = calc_loss_deriv_l2<double>(
                    theta_vec<double>(NThetas, thetas),
                    x, y, lambda, static_cast<std::size_t>(icoord)
                );
                if (std::isnan(result)) {
                    throw NanException();
                }

                return result;
            },
            NThetas
        );
        minimizer->SetFunction(min_func);

        for (uint32_t i = 0; i < static_cast<uint32_t>(NDims); ++i) {
            minimizer->SetVariable(i, "var_" + std::to_string(i),
                                   start_simplex[i], *alpha);
        }

        TimeMeter timer;
        try {
            {
                const auto time_guard = timer.get_guard();
                minimizer->Minimize();
            }
        } catch (const NanException&) {
            return std::nullopt;
        }

        if (keep_intermediate_thetas) {
            steps.emplace_back(NThetas, minimizer->X());
            return MinimumDesc<double, NThetas>(
                "Nelder-Mead method with L2 regularization", steps, timer
            );
        } else {
            return MinimumDesc<double, NThetas>(
                "Nelder-Mead method with L2 regularization",
                theta_vec<double>(NThetas, minimizer->X()),
                minimizer->NIterations(),
                timer
            );
        }
    }
} // namespace ml::opdesc::logreg
#endif // ML_OPDESC_LOGREG_H
