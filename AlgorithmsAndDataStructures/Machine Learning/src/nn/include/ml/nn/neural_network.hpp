#if !(defined(ML_NN_NEURAL_NETWORK_H))
#define ML_NN_NEURAL_NETWORK_H

#include <cassert>
#include <vector>

#include <tl/expected.hpp>

#include "ml/opdesc/minimum_desc.hpp"
#include "ml/utils/activation_funcs.hpp"
#include "ml/utils/io_helpers.hpp"
#include "ml/utils/template_helpers.hpp"
#include "ml/utils/x_helpers.hpp"

#include "./weights.hpp"

namespace ml::nn
{
    using namespace opdesc;
    using namespace utils::activation_funcs;


    template <std::size_t I, std::size_t Value, std::size_t... Values>
    struct nth_element_impl
    {
        static constexpr std::size_t value
            = nth_element_impl<I - 1, Values...>::value;
    };

    template <std::size_t Value, std::size_t... Values>
    struct nth_element_impl<0, Value, Values...>
    {
        static constexpr std::size_t value = Value;
    };

    template <std::size_t I, std::size_t... Values>
    using nth_element = nth_element_impl<I, Values...>;


    template <typename T, std::size_t... NThetasNeurons>
    class NeuralNetwork
    {
    public:
        static_assert(
            is_even(sizeof...(NThetasNeurons)),
            "NThetasNeurons should be pairs of thetas and neurons count"
        );

        // Hiddent layers count
        static constexpr std::size_t LAYERS_COUNT
            = sizeof...(NThetasNeurons) / 2;

        // Don't forget about input layers
        static constexpr std::size_t ABSOLUTE_LAYERS_COUNT = LAYERS_COUNT + 1;

        static constexpr std::size_t OUTPUT_UNITS_COUNT
            = nth_element<LAYERS_COUNT * 2 - 1, NThetasNeurons...>::value;
        static constexpr std::size_t LAST_WEIGHTS_IDX = LAYERS_COUNT - 1;

    private:
        template<std::size_t LayerIdx>
        using weight_type = WeightsLayer<
            T,
            nth_element<LayerIdx * 2, NThetasNeurons...>::value, //
            nth_element<LayerIdx * 2 + 1, NThetasNeurons...>::value
        >;

        template<std::size_t... I>
        struct TupleTypeCreator
        {
            using weights_tuple_t = std::tuple<weight_type<I>...>;
            using optional_weights_tuple_t \
                = std::tuple<std::optional<weight_type<I>>...>;
        };
        template<std::size_t... I>
        static TupleTypeCreator<I...> get_instance_impl(
            const std::index_sequence<I...>
        )
        {
            return TupleTypeCreator<I...>();
        }
        static auto get_instance()
        {
            return get_instance_impl(
                std::make_index_sequence<LAYERS_COUNT>()
            );
        }

        using weights_tuple_t \
            = typename std::result_of<decltype(&get_instance)()>
                          ::type::weights_tuple_t;
        using optional_weights_tuple_t \
            = typename std::result_of<decltype(&get_instance)()>
                          ::type::optional_weights_tuple_t;

    private:
        NeuralNetwork() = default;

    public:
        NeuralNetwork(const NeuralNetwork&) = default;
        NeuralNetwork(NeuralNetwork&&) = default;
        NeuralNetwork(
            weights_tuple_t&& data,
            std::unique_ptr<Activator<T>>&& activator
        ): _data(std::move(data)), _activator(std::move(activator))
        {
        }

    private:
        //////////////////// Neural network loading ////////////////////
        template<std::size_t I>
        static std::optional<weight_type<I>> _load_weights(
            const utils::MatFileReader& reader,
            const std::string& var_name,
            bool& all_weights_loaded
        )
        {
            if (not all_weights_loaded) {
                return std::nullopt;
            }

            auto maybe_weights = weight_type<I>::create(reader, var_name);
            if (not maybe_weights) {
                all_weights_loaded = false;
            }

            return maybe_weights;
        }

        template<std::size_t... I>
        static std::optional<NeuralNetwork> _create_impl(
            const utils::MatFileReader& reader,
            const std::vector<std::string>& var_names,
            std::unique_ptr<Activator<T>>&& activator,
            const std::index_sequence<I...>
        )
        {
            bool all_weights_loaded = true;
            optional_weights_tuple_t data(
                _load_weights<I>(reader, var_names[I], all_weights_loaded)...
            );

            if (all_weights_loaded) {
                return NeuralNetwork(
                    weights_tuple_t(std::move(*(std::get<I>(data)))...),
                    std::move(activator)
                );
            }

            return std::nullopt;
        }

        //////////////////// Neural network generating ////////////////////
        template<std::size_t I>
        static weight_type<I> _generate_weights(
            const T min, const T max,
            const uint32_t random_generator_seed
        )
        {
            return weight_type<I>::create(
                min, max,
                random_generator_seed + static_cast<uint32_t>(I)
            );
        }

        template<std::size_t... I>
        static NeuralNetwork _create_impl(
            const T min, const T max,
            const uint32_t random_generator_seed,
            std::unique_ptr<Activator<T>>&& activator,
            const std::index_sequence<I...>
        )
        {
            return NeuralNetwork(
                weights_tuple_t(
                    _generate_weights<I>(min, max, random_generator_seed)...
                ),
                std::move(activator)
            );
        }

        //////////////////// Forward propagation ////////////////////
        template<std::size_t WeightsIdx>
        y_vec<T> _forward_propagation_step(const TVectorT<T>& features) const
        {
            const auto& weights = std::get<WeightsIdx>(_data);
            assert(static_cast<std::size_t>(features.GetNrows())
                   == weights.THETAS_COUNT - 1);

            auto z = weights.get_thetas() * make_x_vec(features);

            return _activator->activate(z);
        }

        template<std::size_t WeightsIdx, std::size_t... Other>
        y_vec<T> _forward_propagation_recursive_impl(
            const TVectorT<T>& features
        ) const
        {
            const auto g = _forward_propagation_step<WeightsIdx>(features);

            if constexpr (sizeof...(Other) == 0) {
                return g;
            } else {
                return _forward_propagation_recursive_impl<Other...>(g);
            }
        }

        template<std::size_t... I>
        y_vec<T> _forward_propagation_impl(
            const TVectorT<T>& features,
            const std::index_sequence<I...>
        ) const
        {
            return _forward_propagation_recursive_impl<I...>(features);
        }

        //////////////////// Regularization addend ////////////////////
        template<std::size_t AbsoluteLayerIdx>
        std::size_t _get_neurons_count() const
        {
            static_assert(AbsoluteLayerIdx < ABSOLUTE_LAYERS_COUNT);

            if constexpr (AbsoluteLayerIdx == 0) {
                return std::get<0>(_data).THETAS_COUNT - 1;
            } else {
                return std::get<AbsoluteLayerIdx - 1>(_data).NEURONS_COUNT;
            }
        }

        template<std::size_t AbsoluteLayerIdx>
        T _calc_regularization_addend_step() const
        {
            const auto cur_layer_neurons_count
                = _get_neurons_count<AbsoluteLayerIdx>();
            const auto next_layer_neurons_count
                = _get_neurons_count<AbsoluteLayerIdx + 1>();

            const auto& weights = std::get<AbsoluteLayerIdx>(_data);
            const auto& thetas_matrix = weights.get_thetas();

            T sum_of_squares = 0;
            for (std::size_t i = 0; i < cur_layer_neurons_count; ++i) {
                for (std::size_t j = 0; j < next_layer_neurons_count; ++j) {
                    sum_of_squares += std::pow(
                        thetas_matrix(static_cast<Int_t>(j),
                                      static_cast<Int_t>(i + 1)),
                        2
                    );
                }
            }

            return sum_of_squares;
        }

        template<std::size_t AbsoluteLayerIdx, std::size_t... Other>
        T _calc_regularization_addend_recursive_impl() const
        {
            if constexpr (sizeof...(Other) == 0) {
                return 0;
            } else {
                return _calc_regularization_addend_step<AbsoluteLayerIdx>()
                    + _calc_regularization_addend_recursive_impl<Other...>();
            }
        }

        template<std::size_t... I>
        T _calc_regularization_addend_impl(
            const lambda<T> lambda,
            const T m,
            const std::index_sequence<I...>
        ) const
        {
            return (*lambda / (2 * m))
                * _calc_regularization_addend_recursive_impl<I...>();
        }

        T _calc_regularization_addend(const lambda<T> lambda, const T m) const
        {
            return _calc_regularization_addend_impl(
                lambda, m,
                std::make_index_sequence<ABSOLUTE_LAYERS_COUNT>()
            );
        }

        //////////////////// Calc a and z layers values ////////////////////
        struct AZValues
        {
            std::vector<TMatrixT<T>> a;
            std::vector<TMatrixT<T>> z;
        };

        template<std::size_t WeightsIdx, std::size_t... Other>
        void _calc_az_layers_values_recursive_impl(AZValues& az_values) const
        {
            const auto& weights = std::get<WeightsIdx>(_data);
            const auto& thetas = weights.get_thetas();
            std::cout << "THETAS SUM: " << thetas.Sum() << "\n";

            const TMatrixT<T> z(thetas,
                                TMatrixT<T>::kMultTranspose,
                                az_values.a.back());
            const auto h = _activator->activate(z);
            std::cout << "HH: " << h.Sum() << std::endl;
            auto h_T = transpose(h);

            az_values.z.emplace_back(std::move(z));
            if constexpr (sizeof...(Other) == 0) {
                az_values.a.emplace_back(std::move(h_T));
            } else {
                az_values.a.emplace_back(make_x_matrix(h_T));

                _calc_az_layers_values_recursive_impl<Other...>(az_values);
            }
        }

        template<std::size_t... I>
        void _calc_az_layers_values_impl(
            AZValues& az_values,
            const std::index_sequence<I...>
        ) const
        {
            _calc_az_layers_values_recursive_impl<I...>(az_values);
        }

        AZValues _calc_az_layers_values(const x_matrix<T>& x) const
        {
            AZValues result;
            result.a.push_back(x);

            _calc_az_layers_values_impl(
                result,
                std::make_index_sequence<LAYERS_COUNT>()
            );

            return result;
        }

        //////////////////// H calculation ////////////////////
        template<std::size_t WeightsIdx, std::size_t... Other>
        T _calc_h_recursive_impl(const x_vec<T>& x, const std::size_t k) const
        {
            const auto& weights = std::get<WeightsIdx>(_data);
            assert(x.GetNrows() == weights.THETAS_COUNT);

            if constexpr (sizeof...(Other) == 0) {
                auto z = Dot(weights.get_thetas(k), x);

                return _activator->activate(z);
            } else {
                const auto z = weights.get_thetas() * x;
                const auto h = _activator->activate(z);
                const auto new_x = make_x_vec(h);

                return _calc_h_recursive_impl<Other...>(new_x, k);
            }
        }

        template<std::size_t... I>
        T _calc_h_impl(const x_vec<T>& x, const std::size_t k,
                       const std::index_sequence<I...>) const
        {
            return _calc_h_recursive_impl<I...>(x, k);
        }

        T _calc_h(const x_vec<T>& x, const std::size_t k) const
        {
            return _calc_h_impl(
                x, k,
                std::make_index_sequence<LAYERS_COUNT>()
            );
        }

        TMatrixT<T> _calc_h(const x_matrix<T>& x) const
        {
            return _calc_az_layers_values(x).a.back();
        }

        //////////////////// Calc deltas ////////////////////
        template<std::size_t LastWeightsIdx, std::size_t WeightsIdx,
                 std::size_t... OtherWeightsIdxs>
        void _calc_deltas_recursive_impl(
            std::vector<TMatrixT<T>>& deltas,
            const AZValues& az_values
        ) const
        {
            constexpr auto NEXT_WEIGHTS_IDX = LastWeightsIdx - WeightsIdx + 1;
            const auto weights_next = std::get<NEXT_WEIGHTS_IDX>(_data);
            const auto weights_next_thetas = weights_next.get_thetas();
            std::cout << "weights_next_thetas ROWS: "
                      << weights_next_thetas.GetNrows() << "\n";
            std::cout << "weights_next_thetas COLS: "
                      << weights_next_thetas.GetNcols() << "\n";

            const auto weights_next_thetas_sub = weights_next_thetas.GetSub(
                0, weights_next_thetas.GetNrows() - 1,
                1, weights_next_thetas.GetNcols() - 1
            );
            std::cout << "WEIGHTS_NEXT_THETAS_SUB ROWS: "
                      << weights_next_thetas_sub.GetNrows() << "\n";
            std::cout << "WEIGHTS_NEXT_THETAS_SUB COLS: "
                      << weights_next_thetas_sub.GetNcols() << "\n";

            constexpr auto WEIGHTS_IDX = LastWeightsIdx - WeightsIdx;
            const auto weights = std::get<WEIGHTS_IDX>(_data);
            const auto weights_thetas = weights.get_thetas();

            std::cout << "deltas.front() ROWS: "
                      << transpose(deltas.front()).GetNrows() << "\n";
            std::cout << "deltas.front() COLS: "
                      << transpose(deltas.front()).GetNcols() << "\n";

            std::cout << "GOPA" << "\n";
            auto new_deltas = transpose(weights_next_thetas_sub)
                * transpose(deltas.front());
            std::cout << "HOP" << "\n";
            std::cout << "new_deltas ROWS: " << new_deltas.GetNrows() << "\n";
            std::cout << "new_deltas COLS: " << new_deltas.GetNcols() << "\n";

            const auto mmmm = _activator->activate_derivative(
                az_values.z[WEIGHTS_IDX]
            );
            std::cout << "mmmm ROWS: " << mmmm.GetNrows() << "\n";
            std::cout << "mmmm COLS: " << mmmm.GetNcols() << "\n";

            ElementMult(new_deltas, mmmm);
            deltas.emplace(deltas.begin(), std::move(new_deltas));

            if constexpr (sizeof...(OtherWeightsIdxs) != 0) {
                _calc_deltas_recursive_impl<LastWeightsIdx,
                                            OtherWeightsIdxs...>(
                    deltas, az_values
                );
            }
        }

        template<std::size_t LastWeightsIdx, std::size_t... I>
        void _calc_deltas_impl(
            std::vector<TMatrixT<T>>& next_deltas,
            const AZValues& az_values,
            const std::index_sequence<I...>
        ) const
        {
            _calc_deltas_recursive_impl<LastWeightsIdx, I...>(next_deltas,
                                                              az_values);
        }

        std::vector<TMatrixT<T>> _calc_deltas(
            const y_matrix<T>& one_hot_encoded_y,
            const AZValues& az_values
        ) const
        {
            std::cout << "az_values.a.back() ROWS: "
                      << az_values.a.back().GetNrows() << "\n";
            std::cout << "az_values.a.back() COLS: "
                      << az_values.a.back().GetNcols() << "\n";

            std::cout << "one_hot_encoded_y ROWS: "
                      << one_hot_encoded_y.GetNrows() << "\n";
            std::cout << "one_hot_encoded_y COLS: "
                      << one_hot_encoded_y.GetNcols() << "\n";

            const auto last_deltas = az_values.a.back() - one_hot_encoded_y;

            std::vector<TMatrixT<T>> result;
            result.insert(result.begin(), last_deltas);

            constexpr auto LAST_WEIGHTS_IDX = LAYERS_COUNT - 2;
            _calc_deltas_impl<LAST_WEIGHTS_IDX>(
                result,
                az_values,
                std::make_index_sequence<LAYERS_COUNT - 1>()
            );

            return result;
        }

        //////////////////// Calc grad ////////////////////
        template<std::size_t WeightsIdx, std::size_t... Other>
        void _calc_grad_recursive_impl(
            std::vector<TMatrixT<T>>& grads,
            const non_zero_value<std::size_t> m,
            const AZValues& az_values,
            const std::vector<TMatrixT<T>>& deltas
        ) const
        {
            std::cout << "WEIGHTSIDX----: " << WeightsIdx << "\n";
            std::cout << "deltas[WeightsIdx] ROWS: "
                      << deltas[WeightsIdx].GetNrows() << "\n";
            std::cout << "deltas[WeightsIdx] COLS: "
                      << deltas[WeightsIdx].GetNcols() << "\n";

            std::cout << "az_values.a[WeightsIdx] ROWS: "
                      << az_values.a[WeightsIdx].GetNrows() << "\n";
            std::cout << "az_values.a[WeightsIdx] COLS: "
                      << az_values.a[WeightsIdx].GetNcols() << "\n";

            auto grad = deltas[WeightsIdx] * az_values.a[WeightsIdx]
                            * (1 / static_cast<T>(*m));
            grads.emplace_back(std::move(grad));

            if constexpr (sizeof...(Other) != 0) {
                _calc_grad_recursive_impl<Other...>(
                    grads, m, az_values, deltas
                );
            }
        }

        template<std::size_t... I>
        void _calc_grad_recursive_impl(
            std::vector<TMatrixT<T>>& grad,
            const non_zero_value<std::size_t> m,
            const AZValues& az_values,
            const std::vector<TMatrixT<T>>& deltas,
            const std::index_sequence<I...>
        ) const
        {
            _calc_grad_recursive_impl<I...>(grad, m, az_values, deltas);
        }

        std::vector<TMatrixT<T>> _calc_grad_impl(
            const non_zero_value<std::size_t> m,
            const AZValues& az_values,
            const std::vector<TMatrixT<T>>& deltas
        ) const
        {
            std::vector<TMatrixT<T>> result;
            _calc_grad_recursive_impl(
                result, m, az_values, deltas,
                std::make_index_sequence<LAYERS_COUNT>()
            );

            return result;
        }

    public:
        static std::optional<NeuralNetwork> create(
            const utils::MatFileReader& reader,
            const std::vector<std::string>& var_names,
            std::unique_ptr<Activator<T>> activator
        )
        {
            assertm(var_names.size() == LAYERS_COUNT,
                    "Please add more variables to allow fully loading "
                    "current neural network configuration");
            return _create_impl(
                reader, var_names, std::move(activator),
                std::make_index_sequence<LAYERS_COUNT>()
            );
        }

        static NeuralNetwork create(const T min, const T max,
                                    const uint32_t random_generator_seed,
                                    std::unique_ptr<Activator<T>>&& activator)
        {
            return _create_impl(
                min, max,
                random_generator_seed, std::move(activator),
                std::make_index_sequence<LAYERS_COUNT>()
            );
        }


        y_vec<T> forward_propagation(const TVectorT<T>& features) const
        {
            return this->_forward_propagation_impl(
                features,
                std::make_index_sequence<LAYERS_COUNT>()
            );
        }

        T calc_loss(
            const std::vector<x_vec<T>>& x_variants,
            const std::vector<TVectorT<T>>& one_hot_encoded_y_variants
        ) const
        {
            T sum = 0;
            const std::size_t m = x_variants.size();
            for (std::size_t i = 0; i < m; ++i) {
                const auto& x = x_variants[i];
                const auto& y = one_hot_encoded_y_variants[i];

                T inner_sum = 0;
                for (std::size_t k = 0; k < OUTPUT_UNITS_COUNT; ++k) {
                    const T h = _calc_h(x, k);
                    const T y_k = y[static_cast<Int_t>(k)];

                    constexpr T EPS = 1e-8;
                    const T cost_1 = y_k * std::log(h + EPS);
                    const T cost_2 = (1 - y_k) * std::log(1 - h + EPS);
                    inner_sum += cost_1 + cost_2;
                }

                sum += inner_sum;
            }

            return -sum / static_cast<T>(m);
        }

        T calc_loss_l2(
            const std::vector<x_vec<T>>& x_variants,
            const std::vector<TVectorT<T>>& one_hot_encoded_y_variants,
            const lambda<T> lambda
        ) const
        {
            const auto loss = calc_loss(
                x_variants,
                one_hot_encoded_y_variants
            );
            const auto reg = _calc_regularization_addend(
                lambda,
                static_cast<T>(x_variants.size())
            );

            return loss + reg;
        }

        T calc_loss(
            const x_matrix<T>& x,
            const y_matrix<T>& one_hot_encoded_y
        ) const
        {
            const auto h = _calc_h(x);
            const T EPS = 1e-8;

            TMatrixT<T> cost_1(one_hot_encoded_y);
            ElementMult(
                cost_1,
                TMatrixT<T>(
                    h.GetNrows(), h.GetNcols(),
                    (h + EPS).Apply(LogAction<T>()).GetMatrixArray()
                )
            );

            TMatrixT<T> cost_2((-1.0) * one_hot_encoded_y + 1.0);
            ElementMult(
                cost_2,
                TMatrixT<T>(
                    h.GetNrows(), h.GetNcols(),
                    ((-1.0) * h + 1.0 + EPS).Apply(LogAction<T>())
                                            .GetMatrixArray()
                )
            );

            const auto m = static_cast<std::size_t>(x.GetNrows());
            return -(cost_1 + cost_2).Sum() / static_cast<T>(m);
        }

        std::vector<TMatrixT<T>> calc_grad(
            const x_matrix<T>& x,
            const y_matrix<T>& one_hot_encoded_y
        ) const
        {
            const auto az_values = _calc_az_layers_values(x);
            for (const auto& zval: az_values.z) {
                std::cout << "ZVAl: " << zval.Sum() << "\n";
            }
            for (const auto& aval: az_values.a) {
                std::cout << "AAVAl: " << aval.Sum() << "\n";
            }
            std::cout << "KEK" << "\n";
            const auto deltas = _calc_deltas(one_hot_encoded_y, az_values);
            for (const auto& delta: deltas) {
                std::cout << "SUM: " << delta.Sum() << "\n";
            }

            std::cout << "CHEBUREK" << "\n";
            const non_zero_value<std::size_t> m(
                static_cast<std::size_t>(x.GetNrows())
            );
            const auto grads = _calc_grad_impl(m, az_values, deltas);
            std::cout << "SUPERKEK" << "\n";

            return grads;
        }

    private:
        weights_tuple_t _data;
        std::unique_ptr<Activator<T>> _activator;
    };

    template<typename T>
    std::vector<y_vec<T>> one_hot_encode(const y_vec<T>& y,
                                         const std::size_t classes_count)
    {
        const auto variants_count = static_cast<std::size_t>(y.GetNrows());
        std::vector<y_vec<T>> result;
        result.reserve(variants_count);

        for (std::size_t i = 0; i < variants_count; ++i) {
            const auto class_mark = y[static_cast<Int_t>(i)];
            assert(std::trunc(class_mark) == class_mark
                   and std::isgreater(class_mark, 0)
                   and std::islessequal(class_mark, classes_count));

            y_vec<T> one_hot_variant = make_zero_vec<T>(classes_count);
            one_hot_variant[static_cast<Int_t>(class_mark - 1)] = 1;
            result.emplace_back(std::move(one_hot_variant));
        }

        return result;
    }

    template<typename T>
    y_matrix<T> one_hot_encode_m(const y_vec<T>& y,
                                 const std::size_t classes_count)
    {
        const auto variants_count = static_cast<std::size_t>(y.GetNrows());
        y_matrix<T> result = make_zero_matrix<T>(
            static_cast<std::size_t>(y.GetNrows()),
            classes_count
        );

        for (std::size_t i = 0; i < variants_count; ++i) {
            const auto class_mark = y[static_cast<Int_t>(i)];
            assert(std::trunc(class_mark) == class_mark
                   and std::isgreater(class_mark, 0)
                   and std::islessequal(class_mark, classes_count));

            result(static_cast<Int_t>(i),
                   static_cast<Int_t>(class_mark - 1)) = 1;
        }

        return result;
    }
}  // namespace ml::nn
#endif // ML_NN_NEURAL_NETWORK_H
