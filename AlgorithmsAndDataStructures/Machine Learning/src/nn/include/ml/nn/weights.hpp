#if !(defined(ML_NN_WEIGHTS_H))
#define ML_NN_WEIGHTS_H

#include <cassert>
#include <random>
#include <vector>

#include <tl/expected.hpp>

#include "ml/opdesc/minimum_desc.hpp"
#include "ml/utils/io_helpers.hpp"
#include "ml/utils/linalg_helpers.hpp"

namespace ml::nn
{
    using namespace opdesc;

    template <typename T, std::size_t NThetas, std::size_t NNeurons>
    class WeightsLayer
    {
    public:
        static constexpr std::size_t THETAS_COUNT = NThetas;
        static constexpr std::size_t NEURONS_COUNT = NNeurons;

    private:
        WeightsLayer() = default;
        WeightsLayer(theta_matrix<T>&& data)
            : _thetas_matrix(std::move(data))
        {
            assertm(is_valid_proportions(_thetas_matrix,
                                         NEURONS_COUNT,
                                         THETAS_COUNT),
                    "Invalid matrix proportions");

            _maybe_thetas_rows.resize(
                static_cast<std::size_t>(data.GetNrows())
            );
        }

    public:
        WeightsLayer(const WeightsLayer&) = default;
        WeightsLayer(WeightsLayer&&) = default;

        static std::optional<WeightsLayer> create(
            const utils::MatFileReader& reader,
            const std::string var_name
        )
        {
            auto maybe_data = reader.read_matrix<T>(var_name, false);
            if (not maybe_data) {
                return std::nullopt;
            } else if (not is_valid_proportions(*maybe_data,
                                                NEURONS_COUNT,
                                                THETAS_COUNT)) {
                return std::nullopt;
            }

            return WeightsLayer(std::move(*maybe_data));
        }

        static WeightsLayer create(
            const T min, const T max,
            const uint32_t random_generator_seed
        )
        {
            std::mt19937 generator(random_generator_seed);
            std::uniform_real_distribution<T> distribution(min, max);

            TMatrixT<T> weights(static_cast<Int_t>(NEURONS_COUNT),
                                static_cast<Int_t>(THETAS_COUNT));
            for (Int_t i = 0; i < static_cast<Int_t>(NEURONS_COUNT); ++i) {
                for (Int_t j = 0; j < static_cast<Int_t>(THETAS_COUNT); ++j) {
                    weights(i, j) = distribution(generator);
                }
            }

            return WeightsLayer(std::move(weights));
        }

        const theta_matrix<T>& get_thetas() const
        {
            return _thetas_matrix;
        }

        const theta_vec<T>& get_thetas(const std::size_t idx) const
        {
            if (not _maybe_thetas_rows[idx]) {
                const_cast<WeightsLayer*>(this)->_maybe_thetas_rows[idx]
                    = std::make_optional(get_row(_thetas_matrix, idx));
            }

            return *_maybe_thetas_rows[idx];
        }

    private:
        theta_matrix<T> _thetas_matrix;
        std::vector<std::optional<theta_vec<T>>> _maybe_thetas_rows;
    };
}  // namespace ml::nn
#endif // ML_NN_WEIGHTS_H
