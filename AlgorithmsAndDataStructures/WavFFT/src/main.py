import sys
import wave
import math
import struct
import timeit
import matplotlib
import lib.fftlib as fftlib


test_song_path = "/home/armoken/Temp/Pink Floyd/"\
                 "1973 - Dark Side Of The Moon/05 - Money.wav"
song_array = None
song_spectrum = None


def get_array_from_song(path):
    waveFile = wave.open(path, "r")
    result = []
    length = waveFile.getnframes()

    for i in range(0, length):
        waveData = waveFile.readframes(1)
        data = struct.unpack("<h", waveData)
        result.append(int(data[0]))

    return result


def time_read():
    global song_array
    song_array = get_array_from_song(test_song_path)


def fft_time_simple():
    global song_array, song_spectrum
    song_spectrum = fftlib.test_simple(song_array)


def fft_time_openmp():
    global song_array, song_spectrum
    song_spectrum = fftlib.test_openmp(song_array)


def init_matplotlib():
    matplotlib.use('cairo')
    import matplotlib.pyplot as plot
    return plot


def adjust_array_to_power_of_two(arr):
    arr_size = len(arr)
    new_arr_size = 2 ** (int(math.log2(arr_size)) + 1)
    for _ in range(int(new_arr_size - arr_size)):
        arr.append(complex(0, 0))

    return arr


def main():
    # t = timeit.Timer("time_read()",
    #                  "from __main__ import time_read").timeit(1)
    # print(t, "seconds --- file reading")

    global song_array
    song_array = [i for i in range(132100)]
    song_array = adjust_array_to_power_of_two(song_array)
    print(math.log2(len(song_array)))

    for _ in range(10):
        t1 = timeit.Timer("fft_time_simple()",
                          "from __main__ import fft_time_simple").timeit(10)
        # spectrum1 = song_spectrum
        t2 = timeit.Timer("fft_time_openmp()",
                          "from __main__ import fft_time_openmp").timeit(10)
        print(t1, t2)
        # spectrum2 = song_spectrum

        # for i, j in zip(spectrum1, spectrum2):
        #     if i != j:
        #         print("WRONG!!!")
        #         break

    # d = len(song_spectrum)
    # plot = init_matplotlib()
    # plot.plot([abs(i) for i in song_spectrum[:(d - 1)]], "r")
    # plot.savefig("kek.png")


if __name__ == "__main__":
    sys.exit(main())
