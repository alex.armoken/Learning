#include <iostream>
#include <boost/python.hpp>

#include "../include/fft.hpp"
#include "../include/fft_openmp.hpp"
#include "../include/python_utils.hpp"

using namespace std;
using namespace boost::python;

void printPythonIntList(list &l) {
	for (int i = 0; i < len(l); i++) {
		std::cout << extract<int>(l[i]) << std::endl;
	}
}

void printPythonComplexList(list &l) {
	for (int i = 0; i < len(l); i++) {
		complex<double> tmp = extract<complex<double>>(l[i]);
		std::cout << tmp.real() << " + i * " << tmp.imag() << std::endl;
	}
}

void printIntVector(std::vector<int> *vec) {
	for (int i = 0; i < vec->size(); i++) {
		std::cout << (*vec)[i] << std::endl;
	}
}

list test_simple(list &l) {
	// auto vec = pythonListToComplexVector(l);
	auto arr_with_size = pythonListToComplexArr(l);
	SerialFFT1D(arr_with_size->first, arr_with_size->second);
	list &complex_l = complexArrToPythonList(
		arr_with_size->first, arr_with_size->second);
	// delete(arr_with_size->first);
	// delete(arr_with_size);
	return complex_l;
}

list test_openmp(list &l) {
	auto arr_with_size = pythonListToComplexArr(l);
	SerialFFT1DOpenMP(arr_with_size->first, arr_with_size->second);
	list &complex_l = complexArrToPythonList(
		arr_with_size->first, arr_with_size->second);
	// delete(arr_with_size->first);
	// delete(arr_with_size);
	return complex_l;
}

BOOST_PYTHON_MODULE(fftlib) {
    using namespace boost::python;
	def("printPythonIntList", printPythonIntList);
	def("printPythonComplexList", printPythonComplexList);
	def("test_simple", test_simple);
	def("test_openmp", test_openmp);
}
