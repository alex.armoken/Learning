#include "../include/python_utils.hpp"

pair<complex<double>*, unsigned int>* pythonListToComplexArr(list &l) {
	unsigned int listSize = len(l);
	auto result = new pair<complex<double>*, unsigned int>();
	auto arr = new complex<double>[listSize];
	for (unsigned int i = 0; i < listSize; i++) {
		arr[i] = extract<complex<double>>(l[i]);
	}

	result->first = arr;
	result->second = listSize;
	return result;
}

vector<int>* pythonListToIntVector(list &l) {
	std::vector<int> *result = new std::vector<int>();
	for (int i = 0; i < len(l); i++) {
		result->push_back(extract<int>(l[i]));
	}
	return result;
}

vector<complex<double>>* pythonListToComplexVector(list &l) {
	auto *result = new std::vector<complex<double>>();
	for (int i = 0; i < len(l); i++) {
		result->push_back(extract<complex<double>>(l[i]));
	}
	return result;
}

list& intVectorToPythonList(vector<int> *v) {
	list *result = new list();
	for (int i = 0; i < v->size(); i++) {
		result->append((*v)[i]);
	}
	return *result;
}

list& complexArrToPythonList(complex<double> *a, unsigned int size) {
	list *result = new list();
	for (int i = 0; i < size; i++) {
		result->append(a[i]);
	}
	return *result;
}

list& complexDoubleVectorToPythonList(vector<complex<double>> *v) {
	list *result = new list();
	for (int i = 0; i < v->size(); i++) {
		result->append((*v)[i]);
	}
	return *result;
}
