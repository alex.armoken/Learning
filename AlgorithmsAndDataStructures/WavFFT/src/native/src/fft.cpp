#include "../include/fft.hpp"

static inline void swap(complex<double> *data,
                        unsigned int a, unsigned int b) {

    complex<double> tmp = data[b];
    data[b] = data[a];
    data[a] = tmp;
}

static void BitReversing(complex<double> *data,
                         unsigned int size) {

    unsigned int target = 0;
    for (unsigned int pos = 0; pos < size; pos++) {
        if (target > pos) {
            swap(data, pos, target);
        }

        unsigned int mask = size;
        while (target & (mask >>= 1)) {
            target &= ~mask;
        }
        target |= mask;
    }
}

static unsigned int reverse(unsigned int num, unsigned int lg_n) {
    unsigned int res = 0;
    for (unsigned int i = 0; i < lg_n; ++i) {
        if (num & (1 << i)) {
            res |= 1 << (lg_n - 1 - i);
        }
    }

    return res;
}

static void BitReversing2(
    complex<double> *signal, unsigned int size) {

    unsigned int n = size;
    unsigned int lg_n = 0;
    while ((1 << lg_n) < n) ++lg_n;

    unsigned int i = 0;
    for (unsigned int i = 0; i < n; ++i) {
        unsigned int rev = reverse(i, lg_n);
        if (i < rev) {
            rev = reverse(i, lg_n);
            {
                swap(signal, i, rev);
            }
        }
    }
}


static inline complex<double> W(
    unsigned int n, unsigned int N) {
    auto result = exp(complex<double>(0, -1) * 2.0 *\
                      M_PI * (n * 1.0 / N));
    return result;
}

static void Butterfly(complex<double> *signal,
                      complex<double> w, int offset, int butterflySize) {

    auto tem = signal[offset +  butterflySize] * w;
    signal[offset +  butterflySize] = signal[offset] - tem;
    signal[offset] += tem;
}

static void SerialFFTCalculation(
    complex<double> *signal, unsigned int first, unsigned int size) {

    if(size == 1) return;

    SerialFFTCalculation(signal, first, size / 2);
    SerialFFTCalculation(signal, first + (size / 2), size / 2);

    for (int k = first; k < first + size / 2; k++)
        Butterfly(signal, W(k, size), k, size / 2);
}

void SerialFFT1D(complex<double> *signal, unsigned int size) {
    BitReversing2(signal, size);
    SerialFFTCalculation(signal, 0, size);
}
