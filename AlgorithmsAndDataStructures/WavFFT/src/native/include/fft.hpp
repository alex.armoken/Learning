#ifndef FFT_H
#define FFT_H

#include <cmath>
#include <vector>
#include <complex>
#include <algorithm>

using namespace std;

void SerialFFT1D(complex<double> *signal, unsigned int size);

#endif // FFT_H
