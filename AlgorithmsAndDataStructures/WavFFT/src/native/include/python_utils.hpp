#ifndef PYTHON_UTILS_H
#define PYTHON_UTILS_H

#include <iostream>
#include <boost/python.hpp>

using namespace std;
using namespace boost::python;

pair<complex<double>*, unsigned int>* pythonListToComplexArr(list &l);
vector<int>* pythonListToIntVector(list &l);
vector<complex<double>>* pythonListToComplexVector(list &l);
list& intVectorToPythonList(vector<int> *v);
list& complexArrToPythonList(complex<double> *a, unsigned int size);
list& complexDoubleVectorToPythonList(vector<complex<double>> *v);

#endif // PYTHON_UTILS_H
