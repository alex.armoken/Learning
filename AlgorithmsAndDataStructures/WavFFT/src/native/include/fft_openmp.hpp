#ifndef FFT_OPENMP_H
#define FFT_OPENMP_H

#include <omp.h>
#include <cmath>
#include <vector>
#include <complex>
#include <algorithm>
#include "./fft.hpp"

using namespace std;

void SerialFFT1DOpenMP(complex<double> *signal, unsigned int size);

#endif // FFT_OPENMP_H
