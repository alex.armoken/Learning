\section{Дискретное преобразование Фурье}
\subsection{Введение}
Дискретное преобразование Фурье (ДПФ) --- один из распространенных
инструментов спектрального анализа сигналов, широко применяемый
в самых разных отраслях науки и техники.

Пара непрервного преобразования Фурье имеет вид:
\begin{eqnarray}\label{ft_and_ift}
  S(\omega) = \int_{-\infty}^{\infty} s(t) \cdot \exp(-j \cdot \omega \cdot t)dt
  \nonumber\\
  s(t) = \frac{1}{2\pi} \cdot \int_{-\infty}^{\infty} S(\omega) \cdot
  \exp(j \cdot \omega \cdot t) d\omega,
\end{eqnarray}
где $S(\omega)$ --- спектр сигнала $s(t)$.

Выражение для прямого ДПФ и обратного ДПФ (ОДПФ) имеют вид:
\begin{eqnarray}\label{dft_and_idft}
  S_d(k) = \sum_{n = 0}^{N - 1} s(n) \cdot
  \exp(-j \cdot \frac{2 \cdot \pi}{N} \cdot n \cdot k), k = 0\ldots N - 1;
  \nonumber\\
  s(n) = \frac{1}{N} \cdot \sum_{k = 0}^{N - 1} S_d(k) \cdot
  \exp(j \cdot \frac{2 \cdot \pi}{N} \cdot n \cdot k), n = 0\ldots N - 1.
\end{eqnarray}


ДПФ ставит в соответствие N отсчетам сигнала $s(n), n = 0 \ldots N - 1$,
$N$ отсчетов комлексного спектра $S_d{k}, k = 0 \ldots N - 1$.
Здесь и далее в данном разделе переменная $n$ индексирует временные
отсчеты сигнала, а переменная $k$ индексирует спектральные отсчеты ДПФ.

Как в непрерывном, так и в дискретном случаях в выражениях для обратного
преобразования имеется нормировочный коэффициент. В случае интеграла Фурье
это $\frac{1}{2\pi}$, в случае ОДПФ --- $\frac{1}{N}$.

Нормировочный коэффициент необходим для корректного масштабирования сигнала
из частотной области во временную. Нормировочный коэффициент уменьшает
амплитуду сигнала на выходе обратного преобразования, для того чтобы она
совпадала с амплитудой исходного сигнала. Если последовательно рассчитать
прямое преобразование Фурье некоторого сигнала, а после взять обратное
преобразование Фурье, то результат обратного преобразования должен полностью
совпадать с исходным сигналом.

\subsection{Дискретизация сигнала по времени.
  Дискретно-временное преобразование Фурье.}

Рассмотрим дискретный сигнал $s_d(t)$ как результат умножения непрерывного
сигнала $s(t)$ на решетчатую функцию:
\begin{equation}\label{discret_signal}
  s_d(t) = s(t) \cdot \sum_{n = 0}^{N - 1} \delta(t - n \cdot \Delta \cdot t) =
  \sum_{n = 0}^{N - 1} s(t) \cdot \delta(t - n \cdot \Delta \cdot t),
\end{equation}
где $\delta(t)$ --- дельта-функция:
\begin{align*}
  \delta(x) &=
              \begin{cases}
                \infty        & \text{если } x = 0 \\
                0             & \text{если } x \neq 0,
              \end{cases}\numberthis
\end{align*}
где $\Delta t$ --- интервал дискретизаци.

Вычислим преобразование Фурье дискретного сигнала $s_d(t)$, для этого
подставим выражение (\ref{discret_signal}) для преобразования
Фурье (\ref{ft_and_ift}):
\begin{eqnarray}\label{ft_for_discret_signal}
  S_d(\omega) = \int_{-\infty}^{\infty}
  s_d(t) \cdot \exp(-j \cdot \omega \cdot t) dt = \nonumber\\
  = \int_{-\infty}^{\infty} \sum_{n = 0}^{N - 1} s(t)
  \delta(t - n \cdot \Delta t) \cdot \exp(-j \cdot \omega \cdot t)dt.
\end{eqnarray}

Поменяем местами операции суммирования и интегрирования
и используем фильтрующее свойство дельта-функции:
\begin{equation}\label{filter_property_of_delta_func}
  \int_{-\infty}^{\infty} s(t) \cdot \delta(t - n \cdot \Delta t)dt = s(\tau).
\end{equation}

Тогда выражение (\ref{ft_for_discret_signal}) с учетом
(\ref{filter_property_of_delta_func}) принимает вид:
\begin{eqnarray}\label{ft_for_discret_signal_exponential_form}
  S_d(\omega) = \sum_{n = 0}^{N - 1}\int_{-\infty}^{\infty} s(t) \cdot
  \delta(t - n \cdot \Delta t) \cdot \exp(-j \cdot \omega \cdot t)dt =
  \nonumber\\
  = \sum_{n = 0}^{N - 1} s(n \cdot \Delta t) \cdot
  \exp(-j \cdot \omega \cdot n \cdot \Delta t).
\end{eqnarray}

Таким образом, мы избавились от интегрирования в бесконечных пределах,
заменив его конечным суммированием комплексных экспонент.

Комплексные экспоненты $\exp(-j \cdot \omega \cdot n \cdot \Delta t)$
в выражении (\ref{ft_for_discret_signal_exponential_form}) являются
периодическими функциями с периодом:
\begin{equation}\label{period_of_complex_exp}
  \Omega(n) = \frac{2\pi}{n \cdot \Delta t} = 2\pi \cdot n \cdot F_s,
  \text{ рад/с}, n = 1 \ldots N - 1,
\end{equation}
где $F_s = \frac{1}{\Delta t}$ --- частота дискретизации сигнала (Гц).

Необходимо отметить, что n = 0 исключено из выражения
(\ref{period_of_complex_exp}), так как при n = 0 комплексная экспонента
равна единице. Максимальный период повторения
спектра $S_d(\omega)$ будет при $n = 1$, в этом случае он равен
$\Omega_{\max} = \Omega(1) = \frac{2\pi}{\Delta t} = 2\pi \cdot F_s$ рад/с.

Таким образом, спектр $S_d(\omega)$ дискретного сигнала $s_d(t)$,
есть $2\pi \cdot F_s$ --- периодическая функция циклической
частоты $\omega$, определенная
как (\ref{ft_for_discret_signal_exponential_form}). Если мы введем
нормировку частоты дискретизации $F_s$ = 1 Гц, то
(\ref{ft_for_discret_signal_exponential_form}) переходит к выражению
дискретно-временного преобразования Фурье (ДВПФ):
\begin{equation}
  S(\omega_H) = \sum_{n = 0}^{N - 1} s(n) \cdot \exp(-j \cdot \omega_H \cdot n).
\end{equation}

ДВПФ использует только индексы отсчетов входного сигнала $s(n)$ при частоте
дискретизации $F_s = 1$ Гц. В результате ДВПФ мы получим $2\pi$ периодическую
функцию $S_d(\omega_H)$ нормированной циклической частоты
$\omega_H = \frac{\omega}{F_s}$.

Поскольку спектр дискретного сигнала --- периодическая функция, то можно
рассматривать только один период повторения спектра $S_d(\omega)$ при
$\omega = [0, 2\pi \cdot F_s]$ рад/с или $S_d(f)$ при $f = [0, F_s]$ Гц.

\subsubsection{Повторение сигнала во времени}
Для програмной реализации алгоритмов цифровой обработки требуются как
дискретные отсчеты сигнала, так и дискретные отсчеты спектра. Известно
что дискретным спектром обладают периодические сигналы. При этом
дискретный спектр получается путем разложения в ряд Фурье периодического сигнала.
Значит, чтобы получить дискретный спектр, надо сделать исходный дискретный
сигнал периодическим путем повторения данного сигнала во времени
бесконечное количество раз с некоторым периодом $T$. Тогда спектр периодического
сигнала будет содержать дискретные гармоники,
кратные $\Delta\omega = \frac{2\pi}{T}$ рад/с.

Повторять сигнал можно с различным периодом $T$, однако необходимо, чтобы
период повторения был больше или равен длительности сигнала
$T \geq N \cdot \Delta t$, чтобы сигнал и его периодические повторения не
перекрывались во времени. При этом минимальный период повторения сигнала
$T_{\min}$, при котором сигнал и его повторения не накладываются друг на друга,
равен
\begin{equation}
  T_{\min} = N \cdot \Delta t, \text{ сек}.
\end{equation}

При повторении сигнала с минимальным периодом $T_{\min}$ получим линейчатый спектр
сигнала, состоящий из гармоник, кратных:
\begin{equation}\label{harmonic_delta}
  \Delta\omega = \frac{2\pi}{T_{\min}} = \frac{2\pi}{N \cdot \Delta t} (\text{рад/с}).
\end{equation}

Таким образом, можно продискретизировать спектр $S_d(\omega)$ дискретного
сигнала $s(n)$ на одном периоде повторения $2\pi \cdot F_s$ с шагом
$\Delta\omega = \frac{2\pi}{N \cdot \Delta t}$ и получим
\begin{equation}
  \frac{2\pi \cdot F_s}{\Delta\omega} =
  \frac{\frac{2\pi}{\Delta t}}{\frac{2\pi}{N \cdot \Delta t}} = N
\end{equation}
отсчетов спектра.

Учтем вышесказанное в выражении
(\ref{ft_for_discret_signal_exponential_form}):
\begin{eqnarray}\label{ft_for_discret_signal_almost_end}
  S_d(k \cdot \Delta\omega) = \sum_{n = 0}^{N - 1} s(n \cdot \Delta t)
  \exp{-j \cdot n \cdot \Delta t \cdot k \cdot \Delta\omega} =
  \nonumber\\ =
  \sum_{n = 0}^{N - 1} s(n \cdot \Delta t)
  \exp{-j \cdot n \cdot \Delta t \cdot k \cdot \frac{2\pi}{N \cdot \Delta t}}
  = \nonumber\\ =
  \sum_{n = 0}^{N - 1} s(n \cdot \Delta t)
  \exp{-j \cdot \frac{2\pi}{N} \cdot n \cdot k}, k = 0 \ldots N - 1.
\end{eqnarray}

Если опустить в выражении (\ref{ft_for_discret_signal_almost_end}) шаг
дискретизации по времени $\Delta t$ и по частоте $\Delta\omega$, то получим
окончательное выражение для ДПФ:
\begin{equation}\label{end_equation_for_dft}
  S_d(k) = \sum_{n = 0}^{N - 1} s(n) \cdot
  \exp(-j \cdot \frac{2\pi}{N} \cdot n \cdot k), k = 0 \ldots N - 1.
\end{equation}

ДПФ ставит в соответствие $N$ отсчетам дискретного сигнала $s(n)$,
$N$ отсчетов дискретного спектра $S_d(k)$, при этом предполагается, что и
сигнал и спектр являются периодическими и анализируются на одном периоде
повторения.

\subsubsection{Обратное дискретное преобразование Фурье}
Аналогично (\ref{discret_signal}) можно записать выражение для дискретного
спектра через решетчатую функцию:
\begin{equation}\label{discret_spectrum}
  S_D(\omega) = \sum_{k = 0}^{N - 1}
  S_d(\omega) \cdot \delta(\omega - k \cdot \Delta \omega),
\end{equation}
где $S_D(\omega)$ --- дискретные отсчеты спектра $S_d(\omega)$ на одном
периоде повторения $2\pi \cdot F_s$ рад/с.

Подставим (\ref{discret_spectrum}) в выражения для обратного
преобразования Фурье (\ref{ft_and_ift}):
\begin{eqnarray}
  s(t) = C \cdot \int_{-\infty}^{\infty} S_D(\omega) \cdot
  \exp(j \cdot \omega \cdot t) d\omega = \nonumber\\
  = C \cdot \int_{-\infty}^{\infty} \sum_{k = 0}^{N - 1} S_d(\omega)
  \cdot \delta(\omega - k \cdot \Delta \omega) \cdot
  \exp(j \cdot \omega \cdot t) d\omega,
\end{eqnarray}
где $C$ --- коэфициент пропорциональности, задача которого --- обеспечить
равенство по амплитуде исходного дискретного сигнала и результата ОДПФ.
Коэфициент пропорциональности $C$ учитывает коэффициент $\frac{1}{2\pi}$
обратного преобразования Фурье.

Поменяем местами операции суммирования и интегрирования и учтем фильтрующее
свойство дельта-функции:
\begin{eqnarray}\label{discrete_spectrum_in_dft_with_delta_filter_swaped}
  s(t) = C \cdot \sum_{k = 0}^{N - 1} \int_{-\infty}^{\infty}
  S_d(\omega) \cdot \delta(\omega - k \cdot \Delta\omega) \cdot
  \exp(j \cdot \omega \cdot t) d\omega = \nonumber\\ =
  C \cdot \sum_{k = 0}^{N - 1} S_d(k \cdot \Delta\omega) \cdot
  \exp(j \cdot k \cdot \Delta\omega \cdot t).
\end{eqnarray}

Возьмем дискретные отсчеты $s(t)$ через интервал $\Delta t = \frac{1}{F_s}$
сек, тогда (\ref{discrete_spectrum_in_dft_with_delta_filter_swaped}) можно записать:
\begin{equation}
  s(n \cdot \Delta t) = C \cdot \sum_{k = 0}^{N - 1}
  S_d(k \cdot \Delta\omega) \cdot
  \exp(j \cdot k \cdot \Delta\omega \cdot n \cdot \Delta t), n = 0 \ldots N - 1.
\end{equation}

Учтем (\ref{harmonic_delta}) и получим:
\begin{equation}\label{idft_of_spectrum_func}
  s(n \cdot \Delta t) = C \cdot \sum_{k = 0}^{N - 1}
  S_d(k \cdot \Delta\omega) \cdot
  \exp(j \cdot \frac{2\pi}{N} \cdot n \cdot k), n = 0 \ldots N - 1.
\end{equation}

Опустив в (\ref{idft_of_spectrum_func}) интервалы дискретизации по частоте
и по времени, получим выражение ОДПФ, в котором остался неизвестным коэфициент
пропорциональности $C$:
\begin{equation}\label{almost_full_idft}
  s(n) = C \cdot \sum_{k = 0}^{N - 1} S_d(k) \cdot
  \exp(j \cdot \frac{2\pi}{N} \cdot n \cdot k), n = 0 \ldots N - 1.
\end{equation}

Для того, чтобы расчитать коэфициент $C$, необходимо в выражении для ОДПФ
(\ref{almost_full_idft}) подставить выражение для ДПФ (\ref{end_equation_for_dft}):
\begin{eqnarray}\label{dft_in_idft}
  s(n) = C \cdot \sum_{k = 0}^{N - 1} \sum_{m = 0}^{N - 1} s(m) \cdot
  \exp(-j \cdot \frac{2\pi}{N} \cdot m \cdot k) \cdot \nonumber\\
  \cdot\exp(j \cdot \frac{2\pi}{N} \cdot n \cdot k), n = 0 \ldots N - 1.
\end{eqnarray}

Поменяем местами в (\ref{dft_in_idft}) порядок суммирования
и обьединим экспоненты:
\begin{equation}\label{dft_in_idft_common_exp}
  s(n) = C \cdot \sum_{m = 0}^{N - 1} \cdot s(m) \cdot \sum_{k = 0}^{N - 1}
  \exp(j \cdot \frac{2\pi}{N} \cdot (n - m) \cdot k), n = 0 \ldots N - 1.
\end{equation}

Рассмотрим подробнее сумму комплексных экспонент входящую
в (\ref{dft_in_idft_common_exp}). При $m = n$ имеем:
\begin{equation}
  \sum_{k = 0}^{N - 1} \exp(j \cdot \frac{2\pi}{N} \cdot (n - n) \cdot k) =
  \sum_{k = 0}^{N - 1} 1 = N.
\end{equation}

Теперь рассмотрим эту же сумму при $m \neq n$.
Пусть $L = n - m$, тогда:
\begin{equation}\label{sum_of_complex_exp_m_new_n}
  \sum_{k = 0}^{N - 1} \exp(j \cdot \frac{2\pi}{N} \cdot (n - m) \cdot k) =
  \sum_{k = 0}^{N - 1} \exp(j \cdot \frac{2\pi}{N} \cdot L \cdot k).
\end{equation}

Каждая комплексная экспонента, входящая в сумму
(\ref{sum_of_complex_exp_m_new_n}), есть вектор на комплексной плоскости
единичной длины, повернутый на угол:
\begin{equation}
  \phi(k) = \frac{2\pi \cdot L}{N} \cdot k, k = 0 \ldots N - 1.
\end{equation}

Таких векторов будет N, и они повернуты относительно друг друга на
одинаковые углы $\Delta\phi$.

Поскольку все векторы выходят из одной точки (из 0 комплексной плоскости) и
повернуты относительно друг друга на одинаковые углы $\Delta\phi$, то их
сумма
равна нулю.

Тогда в выражении (\ref{dft_in_idft_common_exp}) от суммы
по $m = 0 \ldots N - 1$ останется только слагаемое при $n = m$,
и (\ref{dft_in_idft_common_exp}) можно представить в виде:
\begin{eqnarray}\label{c_in_idft}
  s(n) = C \cdot \sum_{m = 0}^{N - 1} \cdot s(m) \cdot \sum_{k = 0}^{N - 1}
  \exp(j \cdot \frac{2\pi}{N} \cdot (n - m) \cdot k) = \nonumber\\
  = C \cdot s(n) \cdot N, n = 0 \ldots N - 1.
\end{eqnarray}

Из (\ref{c_in_idft}) можно заключить, что $C = \frac{1}{N}$.

Таким образом, окончательное выражения для ОДПФ имееи вид:
\begin{equation}
  s(n) = \frac{1}{N} \cdot \sum_{k = 0}^{N - 1} S_d(k) \cdot
  \exp(j \cdot \frac{2\pi}{N} \cdot n \cdot k), n = 0 \ldots N - 1.
\end{equation}
