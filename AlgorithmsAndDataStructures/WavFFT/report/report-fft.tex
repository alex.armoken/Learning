\section{Быстрое преобразование Фурье}
\subsection{Принцип построения алгоритмов БПФ}
Рассмотрим выражение для дискретного преобразования Фурье:
\begin{equation}
  S(k) = \sum_{n = 0}^{N - 1} s(n) \cdot
  \exp(-j \cdot \frac{2\pi}{N} \cdot n \cdot k), k = 0 \ldots N - 1
\end{equation}

ДПФ $N$ отсчетам сигнала $s(n), n = 0 \ldots N - 1$
(в общем случае комплексного) ставит в соотвествие $N$ комплексных
спектральных отсчетов $S(k), k = 0 \ldots N - 1$. Для вычисления одного
спектрального отсчета требуется $N$ операций комплексного умножения и сложения.
Таким образом, вычислительная сложность алгоритма ДПФ составляет $N^2$ операций
комплексного умножения и сложения.

Поскольку сложность алгоритма растет квадратично относительно размера входного
сигнала, можно достичь существенного ускорения вычисления, если нам удасться
свести расчет $N$-точечного ДПФ к двум $\frac{N}{2}$-точечным ДПФ, как это
показано на рисунке (~\ref{fft_fig_1}):

\begin{figure}[h]
  \center\includegraphics[scale=1]{fft_introduction_fig1.png}
  \caption{Замена $N$-точечного ДПФ двуми $\frac{N}{2}$-точечными ДПФ}\label{fft_fig_1}
\end{figure}

Замена одного $N$-точечного ДПФ двумя $\frac{N}{2}$-точечными ДПФ приведет к
уменьшению количества операций в 2 раза, но дополнительно требуются операции
разделения последовательности на две и обьединения двух $\frac{N}{2}$-точечных
ДПФ в одно $N$-точечное.

При этом каждое из $\frac{N}{2}$-точечных ДПФ также можно вычислить путем
замены $\frac{N}{2}$-точечного ДПФ на два $\frac{N}{4}$-точечных, которые, в
свою очередь, можно рассчитать через $\frac{N}{8}$-точечные ДПФ. Эту рекурсию
можно продолжать, пока возможно разбить входную последовательность на две.

В нашем случае, если $N = 2^L$, $L$ --- это положительное целое, мы можем
разделить последовательность пополам $L$ раз. Для $N = 8 (L = 3)$ такое
разделение представлено на рисунке (~\ref{fft_fig_2}).

\begin{figure}[h]
  \center\includegraphics[scale=1]{fft_introduction_fig2.png}
  \caption{Разделение и объединение последовательности для $N = 8$}\label{fft_fig_2}
\end{figure}

Алгоритмы БПФ, которые используют выборки длиной $N = 2^L$, называются
алгоритмами БПФ по основанию 2.

\subsection{Обратное быстрое преобразование Фурье}
Эффективный алгоритм вычисления прямого БПФ можно использовать и для обратного
преобразования. Следует обратить внимание на то, что комплексные экспоненты в
выражениях для прямого и обратного ДПФ являются комплексно-сопряженными.
\begin{equation}
  \exp(j \cdot \frac{2\pi}{N} \cdot n \cdot k) =
  {(\exp(-j \cdot \frac{2\pi}{N} \cdot n \cdot k))}^*,
\end{equation}
где ${(\bullet)}^*$ --- оператор комплексного сопряжения.

Можно показать, что для двух комплексных чисел $x = a + j \cdot b$ и
$y = c + j \cdot d$ справедливо следующее равенство:
\begin{equation}
  x \cdot {y}^* = {({x}^* \cdot y)}^*.
\end{equation}

Применительно для выражения ОДПФ можно записать:
\begin{eqnarray}
  s(n) = \frac{1}{N} \sum_{k = 0}^{N - 1} S(k) \cdot
  {\Bigl(\exp(-j \cdot \frac{2\pi}{N} \cdot n \cdot k)\Bigr)}^* = \nonumber\\
  = \frac{1}{N} {\Biggl( \sum_{k = 0}^{N - 1} {S}^*(k) \cdot
  \exp(-j \cdot \frac{2\pi}{N} \cdot n \cdot k) \Biggr)}^*
\end{eqnarray}

Таким образом, берется комплексно-сопряженный спектр ${S}^*(k)$, выполняется
прямое ДПФ и результат подвергается комплексному сопряжению. Вычисление ОДПФ
при использовании ДПФ приведено на рисунке (~\ref{fft_fig_3}).

\begin{figure}[h]
  \center\includegraphics[scale=0.9]{fft_introduction_fig3.png}
  \caption{Вычисление обратного БПФ}\label{fft_fig_3}
\end{figure}

Если вместо ДПФ использовать БПФ, то получим обратное быстрое преобразование
Фурье (ОБПФ). При этом для выполнения комплексного сопряжения необходимо лишь
поменять знак перед мнимой частю спектра до вызова функции БПФ и результата
после БПФ.

\subsection{Алгоритм БПФ по основанию два с прореживанием по времени}

Прореживание по времени заключается в разделении исходной последовательности
$s(n), n = 0 \ldots N - 1$, на две последовательности половинной
длительности $s_0(m)$ и $s_1(m)$, $m = 0 \ldots \frac{N}{2} - 1$,
таким образом, что $s_0(m) = s(2 \cdot m)$, а $s_1(m) = s(2 \cdot m + 1)$.
Последовательность $s_0(m)$ содержит отсчеты с четными индексами,
а $s_1(m)$ --- с нечетными. Прореживание по времени для $N = 8$
представлена на рисунке (\ref{fft_dec_in_time_fig_1}).
\begin{figure}[h]
  \center\includegraphics[scale=1]{fft_dec_in_time_fig1.png}
  \caption{Прореживание по времени для $N = 8$}\label{fft_dec_in_time_fig_1}
\end{figure}

\paragraph{Процедура объединения}
Рассмотрим ДПФ прореженного по времени сигнала:
\begin{eqnarray}\label{dft_of_discrete_sig_in_time}
  S(k) = \sum_{m = 0}^{\frac{N}{2} - 1} s(2 \cdot m) \cdot W_N^{2 \cdot m \cdot k}
  + \sum_{m = 0}^{\frac{N}{2} - 1} s(2 \cdot m + 1) \cdot W_N^{(2 \cdot m + 1) \cdot k}
  = \nonumber\\ =
  \sum_{m = 0}^{\frac{N}{2} - 1} s(2 \cdot m) \cdot W_N^{2 \cdot m \cdot k} +
  + W_N^k\sum_{m = 0}^{\frac{N}{2} - 1} s(2 \cdot m + 1) \cdot W_N^{2 \cdot m \cdot k},
  k = 0 \ldots N - 1.
\end{eqnarray}

Если рассмотреть только первую половину ДПФ $S(k)$ для индексов $k = 0 \ldots
\frac{N}{2} - 1$, а также учесть, что
\begin{equation}
  W_N^{2 \cdot m \cdot k } = \exp
  \Biggl(-j \cdot \frac{2\pi}{N} \cdot 2 \cdot m \cdot k  \Biggr) =
  \exp \Biggl( -j \cdot \frac{2\pi}{\frac{N}{2}} \cdot m \cdot k \Biggr) =
  W_{\frac{N}{2}}^{m \cdot k},
\end{equation}

то (\ref{dft_of_discrete_sig_in_time}) преобразуется к виду:
\begin{eqnarray}
  S(k) = \sum_{m = 0}^{\frac{N}{2} - 1} s(2 \cdot m) \cdot W_{\frac{N}{2}}^{m \cdot k} +
  W_N^k \cdot \sum_{m = 0}^{\frac{N}{2} - 1} s(2 \cdot m + 1)
  \cdot W_{\frac{N}{2}}^{m \cdot k} = \nonumber\\ =
  S_0(k) + W_N^k \cdot S_1(k),
  k = 0 \ldots \frac{N}{2} - 1,
\end{eqnarray}
где
\begin{eqnarray}
  S_0(k) = \sum_{m = 0}^{\frac{N}{2} - 1} s_0(m) \cdot W_{\frac{N}{2}}^{m \cdot k},
  k = 0 \ldots \frac{N}{2} - 1; \nonumber\\
  S_1(k) = \sum_{m = 0}^{\frac{N}{2} - 1} s_1(m) \cdot W_{\frac{N}{2}}^{m \cdot k},
  k = 0 \ldots \frac{N}{2} - 1,
\end{eqnarray}
$\frac{N}{2}$-точечные ДПФ прореженных последовательностей $s_0(m)$ и
$s_1(m)$, $m = 0 \ldots \frac{N}{2} - 1$ соответственно.
Таким образом, прореживание по времени можно считать алгоритмом разделения
последовательности на две последовательности половинной длительности. Первая
половина ДПФ есть сумма ДПФ $S_0(k)$ `четной` последовательности $s_0(m)$ и
ДПФ $S_1(k)$ `нечетной` последовательности $s_1(m)$, умноженного на поворотные
коэфициенты $W_N^k$.

Проанализируем теперь вторую половину ДПФ $S(k + \frac{N}{2})$ для
$k = 0 \ldots \frac{N}{2} - 1$:
\begin{eqnarray}\label{analysis_of_second_half_of_dft}
  S\bigl(k + \frac{N}{2}\bigr)  = \sum_{m = 0}^{\frac{N}{2} - 1} s(2 \cdot m)
  W_N^{2 \cdot m \cdot (k + \frac{N}{2})} + \nonumber\\ +
  \sum_{m = 0}^{\frac{N}{2} - 1} s(2 \cdot m + 1)
  W_N^{(2 \cdot m + 1) \cdot (k + \frac{N}{2})}, k = 0 \ldots \frac{N}{2} - 1.
\end{eqnarray}

Рассмотрим более подробно поворотные коэффициенты
$W_N^{2 \cdot m \cdot (k + \frac{N}{2})}$:
\begin{equation}\label{rotary coefficients_1}
  W_N^{2 \cdot m \cdot (k + \frac{N}{2})} =
  \underbrace{W_N^{2 \cdot m \cdot k}}_{W_{\frac{N}{2}}^{m \cdot k}} \cdot
  W_N^{2 \cdot m \cdot \frac{N}{2}} = W_{\frac{N}{2}}^{m \cdot k} \cdot
  \underbrace{W_N^{m \cdot N}}_{1} = W_{\frac{N}{2}}^{m \cdot k}.
\end{equation}

Аналогично можно упроситить $W_N^{(2 \cdot m + 1) \cdot (k + \frac{N}{2})}$:
\begin{equation}\label{rotary coefficients_2}
  W_N^{(2 \cdot m + 1) \cdot (k + \frac{N}{2})} =
  \underbrace{W_N^{2 \cdot m \cdot k}}_{W_{\frac{N}{2}}^{m \cdot k}} \cdot
  \underbrace{W_N^{2 \cdot m \cdot \frac{N}{2}}}_{1} \cdot W_N^k \cdot
  \underbrace{W_N^{\frac{N}{2}}}_{-1} = -W_N^k \cdot W_{\frac{N}{2}}^{m \cdot k}.
\end{equation}

Тогда выражение (\ref{analysis_of_second_half_of_dft}) с учетом
(\ref{rotary coefficients_1}) и (\ref{rotary coefficients_2}) принимает вид:
\begin{eqnarray}
  S\bigl(k + \frac{N}{2} \bigr) = \sum_{m = 0}^{\frac{N}{2} - 1} s(2 \cdot m)
  W_{\frac{N}{2}}^{m \cdot k} - W_N^k \cdot \sum_{m = 0}^{\frac{N}{2} - 1}
  s(2 \cdot m + 1) \cdot W_{\frac{N}{2}}^{m \cdot k} = \nonumber\\
  = S_0(k) - W_N^k \cdot S_1(k), k = 0 \ldots \frac{N}{2} - 1.
\end{eqnarray}

Используя выражение для первой и второй половин ДПФ, окончательно можно
записать процедуру их обьединения как:
\begin{eqnarray}\label{fft_time_sep_common}
  S(k) = S_0(k) + W_N^k \cdot S_1(k),
  k = 0 \ldots \frac{N}{2} - 1;
  \nonumber\\
  S(k + \frac{N}{2}) = S_0(k) - W_N^k \cdot S_1(k),
  k = 0 \ldots \frac{N}{2} - 1.
\end{eqnarray}


\paragraph{Граф бабочка}
Выражение (\ref{fft_time_sep_common}) объединяет два $\frac{N}{2}$-точечных
ДПФ $S_0(k)$ и $S_1(k), k = 0 \ldots \frac{N}{2} - 1$, прореженных сигналов
половинной длительности $s_0(m) = s(2 \cdot m)$ и
$s_1(m) = s(2 \cdot m + 1), m = 0 \ldots \frac{N}{2} - 1$, прореженных
сигналов половинной длительности $s_0(m) = s(2 \cdot m)$ и
$s_1(m) = s(2 \cdot m + 1), m = 0 \ldots \frac{N}{2} - 1$, в результирующее
$N$-точечное ДПФ $S(k), k = 0 \ldots N - 1$, исходного сигнала.

Графически процесс объединения представлен на рисунке (\ref{fft_dec_in_time_fig_2}).
\begin{figure}[h]
  \center\includegraphics[scale=1]{fft_dec_in_time_fig2.png}
  \caption{Граф 'бабочка'}\label{fft_dec_in_time_fig_2}
\end{figure}


Из-за специфической формы графа он получил название `бабочка`. Данная
процедура объединения является основной при построении алгоритмов БПФ по
основанию два.


\subsection{Алгоритм БПФ по основанию два с
  прореживанием по частоте}
Пусть имеется $N$ отсчетов входного сигнала $s(n), n = 0 \ldots N - 1$, при
этом $N$ представляет собой целую степень двойки $N = 2^L$.

В алгоритме БПФ с прореживанием по времени производилось разделение исходного
сигнала в соответствии с двоично-инверсной перестановкой. Таким образом мы
получили первую и вторую половины ДПФ.

В алгоритме с прореживанием по частоте исходный сигнал
$s(n), n = 0 \ldots N - 1$, делится пополам, т.е. $s_0(m) = s(m)$ и
$s_1(m) = s(m + \frac{N}{2}), m = 0 \ldots \frac{N}{2} - 1$.

Тогда ДПФ сигнала $s(n)$ может быть записано в виде:
\begin{eqnarray}\label{fft_dec_in_freq_new}
  S(k) = \sum_{n = 0}^{N - 1} s(n) \cdot W_N^{n \cdot k} =
  \sum_{m = 0}^{\frac{N}{2} - 1} \Biggl[s(m) \cdot W_N^{m \cdot k}
  + s\Bigl(m + \frac{N}{2}\Bigr) \cdot W_N^{(m + \frac{N}{2}) \cdot k}\Biggr]
  = \nonumber\\ =
  \sum_{m = 0}^{\frac{N}{2} - 1} \Biggl[s(m) \cdot W_N^{m \cdot k} +
  s\Bigl(m + \frac{N}{2} \Bigl) \cdot W_N^{m \cdot k}
  \cdot W_N^{\frac{N}{2} \cdot k} \Biggr] = \nonumber\\ =
  \sum_{m = 0}^{\frac{N}{2} - 1} \Biggl[s(m) + s\Bigl(m + \frac{N}{2} \Bigl)
  \cdot W_N^{\frac{N}{2} \cdot k} \Biggr] \cdot W_N^{m \cdot k}, k = 0 \ldots N - 1.
\end{eqnarray}
Рассмотрим выражение (\ref{fft_dec_in_freq_new}) для спектральных отсчетов
$S(2 \cdot p), p = 0 \ldots \frac{N}{2} - 1$ с четными индексами:
\begin{equation}\label{fft_dec_in_freq_for_even_spectrums}
  S(2 \cdot p) = \sum_{m = 0}^{\frac{N}{2} - 1}
  \Bigl[s(m) + s\Bigl(m + \frac{N}{2}\Bigr)
  \cdot W_N^{\frac{N}{2} \cdot 2 \cdot p} \Bigr] \cdot W_N^{m \cdot 2 \cdot p},
  p = 0 \ldots \frac{N}{2} - 1.
\end{equation}

Учтем в выражении (\ref{fft_dec_in_freq_for_even_spectrums}), что
$W_N^{\frac{N}{2} \cdot 2 \cdot p} = W_N^{N \cdot p} = 1$, а также
$W_N^{m \cdot 2 \cdot p} = W_{\frac{N}{2}}^{m \cdot p}$, тогда получим:
\begin{eqnarray}
  S(2 \cdot p) = \sum_{m = 0}^{\frac{N}{2} - 1}
  \underbrace{\Bigl[s(m) + s \Bigl(m + \frac{N}{2}\Bigr)\Bigr]}_{x_0(m)}
  \cdot W_{\frac{N}{2}}^{m \cdot p} = \nonumber\\ =
  \underbrace{\sum_{m = 0}^{\frac{N}{2} - 1} x_0(m)
  \cdot W_{\frac{N}{2}}^{m \cdot p}}_{\frac{N}{2}-\text{точечное ДПФ}},
  p = 0 \ldots \frac{N}{2} - 1.
\end{eqnarray}

Таким образом, спектральные отсчеты $S(2 \cdot p),
p = 0 \ldots \frac{N}{2} - 1$ с четными индексами есть
$\frac{N}{2}$-точечное ДПФ сигнала $x_0(m) = s(m) + s(m + \frac{N}{2})$.

Аналогично рассмотрим выражение (\ref{fft_dec_in_freq_new}) для спектральных
отсчетов \\ $S(2 \cdot p + 1), p = \ldots \frac{N}{2} - 1$ с
нечетными индексами:
\begin{eqnarray}\label{fft_dec_in_freq_for_not_even_spectrums}
  S(2 \cdot p + 1) = \sum_{m = 0}^{\frac{N}{2} - 1} 
  \Bigl[s(m) + s\Bigl(m + \frac{N}{2}\Bigr) \Bigr] 
  \cdot W_N^{m \cdot (2 \cdot p + 1)}, p = 0 \ldots \frac{N}{2} - 1
\end{eqnarray}

Учтем в выражении (\ref{fft_dec_in_freq_for_even_spectrums}), что
$W_N^{\frac{N}{2} \cdot (2 \cdot p + 1)} = W_N^{\frac{N}{2} \cdot 2 \cdot p} \cdot
W_N^{\frac{N}{2}} = -1$, а также что $W_N^{m \cdot (2 \cdot p + 1)} = W_N^m \cdot
W_N^{m \cdot 2 \cdot p} = W_N^m \cdot W_{\frac{N}{2}}^{m \cdot p}$. Тогда выражение
(\ref{fft_dec_in_freq_for_even_spectrums}) можно представить в виде

\begin{eqnarray}
  S(2 \cdot p + 1) = \sum_{m = 0}^{\frac{N}{2} - 1} 
  \underbrace{\Bigl[s(m) - s\Bigl(m + \frac{N}{2} \Bigr) \Bigr] 
  \cdot W_N^m}_{x_1(m)} \cdot W_{\frac{N}{2}}^{m \cdot p} = \nonumber\\ =
  \underbrace{\sum_{m = 0}^{\frac{N}{2} - 1} x_1(m)
  \cdot W_{\frac{N}{2}}^{m \cdot p}}_{\frac{N}{2}-\text{точечное ДПФ}}, 
  p = 0 \ldots \frac{N}{2} - 1.
\end{eqnarray}

Спектральные отсчеты $S(2 \cdot p + 1), p = 0 \ldots \frac{N}{2} - 1$ c
нечетными индексами $2 \cdot p + 1$ есть $\frac{N}{2}$-точечное ДПФ сигнала
$x_1(m) = [s(m) - s(m + \frac{N}{2})] \cdot W_N^(m)$.

Таким образом, процедура разделения заключается в расчете сигналов 
половинной длительности $x_0(m)$ и $x_1(m), m = 0 \ldots \frac{N}{2} - 1$.
После чего можно заменить $N$-точечное ДПФ двумя $\frac{N}{2}$-точечными
ДПФ сигналов $x_0(m)$ и $x_1(m)$.

\clearpage
\paragraph{Граф «бабочка» алгоритма БПФ с прореживанием по частоте}
Процедура расчета сигналов половинной длительности
\begin{equation}
  \begin{aligned}
    x_0(m) &= s(m) + s\Bigl(m + \frac{N}{2}\Bigr),
    && m = 0 \ldots \frac{N}{2} - 1; \\
    x_1(m) &= \Bigl[s(m) - s\Bigl(m + \frac{N}{2}\Bigr)\Bigr] \cdot W_N^m,
    && m = 0 \ldots \frac{N}{2} - 1
  \end{aligned}
\end{equation}

может быть представлена в виде графа `бабочка` как это показано
на рисунке (\ref{fft_dec_in_freq_fig_1}).
\begin{figure}[h]
  \center\includegraphics[scale=1]{fft_dec_in_freq_fig1.png}
  \caption{Граф 'бабочка'}\label{fft_dec_in_freq_fig_1}
\end{figure}

Отличие графа `бабочка' алгоритма с прореживанием по частоте от
аналогичного графа для алгоритма с прореживанием по времени заключается в том,
что умножение на поворотный коэффициент $W_N^m$ производится после вычитания
ветвей. В графе `бабочка` алгоритма с прореживанием по времени
умножение на поворотный коэффициент производилось до вычитания ветвей.
