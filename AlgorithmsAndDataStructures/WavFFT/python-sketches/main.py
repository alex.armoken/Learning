import sys
import dft
import fft


def test_direct_1d_transformation(x):
    print("\n DIRECT 1D TRANSFORMATION: \n")
    X = dft.DFT(x.copy())
    for i in X:
        print(i)

    print()
    for i in fft.FFT(x.copy()):
        print(i)

    print()
    for i in fft.FFT2(x.copy()):
        print(i)

    print()
    for i in fft.FFT3(x.copy()):
        print(i)

    return X


def test_inverse_1d_transformation(X):
    print("\n INVERSE 1D TRANSFORMATION: \n")
    for i in dft.IDFT(X.copy()):
        print(i)

    print()
    for i in fft.IFFT(X.copy()):
        print(i)

    print()
    for i in fft.IFFT2(X.copy()):
        print(i)


def test_direct_2d_transformation(x):
    print("\n DIRECT 2D TRANSFORMATION: \n")
    result = dft.DFT2D(x)
    for i in result:
        print(i)

    print()
    for i in dft.DFT2D2(x):
        print(i)

    print()
    for i in fft.FFT2D2(x):
        print(i)

    return result


def test_inverse_2d_transformation(X):
    print("\n INVERSE 2D TRANSFORMATION: \n")
    for i in dft.IDFT2D(X):
        print(i)

    print()
    for i in fft.IFFT2D(X):
        print(i)


def main():
    x = [i for i in range(8)]
    print(x)
    X = test_direct_1d_transformation(x)
    test_inverse_1d_transformation(X)

    x = [[i for i in range(4)] for _ in range(4)]
    X = test_direct_2d_transformation(x)
    test_inverse_2d_transformation(X)


if __name__ == "__main__":
    sys.exit(main())
