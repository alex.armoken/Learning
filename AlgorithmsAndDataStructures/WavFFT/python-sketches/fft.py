import dft
import cmath


def W(n, N):
    return cmath.exp(-1j * 2 * cmath.pi * n / N)


def FFT(x):
    """Fast Discrete Fourier transform.
    Using simple Discrete Fourier transform
    Arguments:
        x - input array
    """

    N = len(x)
    if N % 2 > 0:
        raise ValueError("size of x must be a power of 2")
    elif N <= 32:
        return dft.DFT(x)
    else:
        even = FFT(x[0::2])
        odd = FFT(x[1::2])
        X = even + odd
        for k in range(N // 2):
            X[k] = even[k] + W(k, N) * X[k + N // 2]
            X[k + N // 2] = even[k] - W(k, N) * X[k + N // 2]

        return X


def FFT2(x):
    """Fast Discrete Fourier transform.
    Without using simple Discrete Fourier transform
    Arguments:
        x - input array
    """

    N = len(x)
    if N <= 1:
        return x
    else:
        even = FFT(x[0::2])
        odd = FFT(x[1::2])
        X = even + odd
        for k in range(N // 2):
            X[k] = even[k] + W(k, N) * odd[k]
            X[k + N // 2] = even[k] - W(k, N) * odd[k]

        return X


def FFT3(x):
    """Fast Discrete Fourier transform.
    With realisation of BitReversing
    Arguments:
        x - input array
    """

    def bit_reversing(arr):
        bits_count = 0
        tmp_size = len(arr)
        result = [0] * len(arr)

        while tmp_size > 1:
            tmp_size /= 2
            bits_count += 1

        for index in range(len(arr)):
            mask = 1 << (bits_count - 1)
            reversed_index = 0

            for i in range(bits_count):
                val = bool(index & mask)
                reversed_index |= val << i
                mask = mask >> 1

            result[reversed_index] = arr[index]

        return result

    N = len(x)
    if N <= 1:
        return x
    else:
        reversed_x = bit_reversing(x)
        even = FFT(reversed_x[:len(reversed_x) // 2])
        odd = FFT(reversed_x[len(reversed_x) // 2:])
        X = even + odd
        for k in range(N // 2):
            X[k] = even[k] + W(k, N) * odd[k]
            X[k + N // 2] = even[k] - W(k, N) * odd[k]

        return X


def IFFT(X):
    """Fast Inverse Discrete Fourier transform.
    Arguments:
        X - input array
    """

    N = len(X)
    if N % 2 > 0:
        raise ValueError("size of x must be a power of 2")
    elif N <= 32:
        return dft.IDFT(X)
    else:
        even = IFFT(X[0::2])
        odd = IFFT(X[1::2])
        x = even + odd
        for k in range(N // 2):
            x[k] = even[k] + W(-k, N) * odd[k]
            x[k + N // 2] = even[k] - W(-k, N) * odd[k]

        return x


def IFFT2(X):
    """Fast Inverse Discrete Fourier transform.
    Arguments:
        X - input array
    """

    def sub_ifft(sub_X):
        N = len(sub_X)
        if N <= 1:
            return sub_X
        else:
            even = sub_ifft(sub_X[0::2])
            odd = sub_ifft(sub_X[1::2])
            x = even + odd
            for k in range(N // 2):
                x[k] = even[k] + W(-k, N) * odd[k]
                x[k + N // 2] = even[k] - W(-k, N) * odd[k]

            return x

    result = sub_ifft(X)
    for i in range(len(result)):
        result[i] /= len(result)

    return result


def FFT2D2(x):
    fft_rows = [FFT(row) for row in x]
    return dft.transpose(FFT(row) for row in dft.transpose(fft_rows))


def IFFT2D(X):
    ifft_rows = [IFFT(row) for row in X]
    return dft.transpose(IFFT(row) for row in dft.transpose(ifft_rows))
