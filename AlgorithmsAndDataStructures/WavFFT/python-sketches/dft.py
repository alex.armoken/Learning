import cmath


def DFT(x):
    """Discrete Fourier transform.
    Arguments:
        x - input array
    """

    X = [complex(0) for _ in range(len(x))]
    N = len(x)
    for k in range(N):
        for n in range(N):
            X[k] += x[n] * cmath.exp(-1j * (2 * cmath.pi / N) * n * k)

    return X


def IDFT(X):
    """Inverse Discrete Fourier transform.
    Arguments:
        x - input array
    """

    N = len(X)
    x = [complex(0) for _ in range(N)]
    for n in range(N):
        for k in range(N):
            x[n] += X[k] * cmath.exp(1j * (2 * cmath.pi / N) * n * k)

        x[n] /= N

    return x


def DFT2D(x):
    N = len(x)
    X = [[complex(0) for i in range(N)] for _ in range(N)]
    for x1 in range(N):
        for x2 in range(N):
            for s in range(N):
                for t in range(N):
                    X[x1][x2] += x[s][t] *\
                                 cmath.exp(-1j * 2 * cmath.pi *
                                           (s * x1 / N + t * x2 / N))

    return X


def transpose(arr):
    return list(map(list, zip(*arr)))


def DFT2D2(x):
    dft_rows = [DFT(row) for row in x]
    return transpose(DFT(row) for row in transpose(dft_rows))


def IDFT2D(X):
    idft_rows = [IDFT(row) for row in X]
    return transpose(IDFT(row) for row in transpose(idft_rows))
