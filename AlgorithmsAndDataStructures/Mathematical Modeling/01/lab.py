#!/usr/bin/env python3
import sys
from functools import reduce
from typing import Dict, List, Tuple

from matplotlib import pyplot


Config = Dict[str, int]
DEFAULT_CONFIG = {"m": 2 ** 32, "k": 987654321, "A0": 2}


def init(config: Config) -> Tuple[int, int, int]:
    A0 = config["A0"]
    k = config["k"]
    m = config["m"]

    return A0, k, m


def multiplicative_congruential_method(
        count: int,
        config: Config = DEFAULT_CONFIG
) -> List[int]:
    A0, k, m = init(config)
    A = [A0]
    for i in range(count):
        new_item = (k * A[i]) % m
        A.append(new_item)

    return [A[i] / float(m) for i in range(1, len(A))]


def zfill_number(number: int) -> str:
    pass

def add_zeros_from_left(number: int) -> str:
    number_str = str(number)
    zeros_to_add = "0" * (8 - len(number_str))
    number_str = zeros_to_add + number_str

    return number_str


def get_new_pseudo_random(number: int) -> int:
    number = number ** 2
    number_str = add_zeros_from_left(number)

    return int(number_str[2:6])  # get from center


def mid_square_method(source_number: int, count: int) -> List[int]:
    A = []
    for _ in range(count):
        source_number = get_new_pseudo_random(source_number)
        A.append(source_number)

    return A


def get_correlation(z: List[int], s: int = 3) -> int:
    r = 12 \
        * (1.0 / (len(z) - s)) \
        * sum([z[i] * z[i + s] for i in range(len(z) - s)]) - 2.8

    return r


def show_math_coeffs(z: List[int]) -> None:
    expected_value = sum(z) / len(z)
    dispersion = sum([z[i] ** 2 - expected_value ** 2 for i in range(len(z))]) / len(z)

    print(f"Length {len(z)}. Expected value = {expected_value}. Dispersion = {dispersion}")


def show_plot(z: List[int]) -> None:
    pyplot.style.use(["dark_background"])
    pyplot.subplot(1, 1, 1)
    pyplot.hist(z, color="C4")
    pyplot.show()


def M(arr: List[int]) -> int:
    return reduce(lambda x, y: x + y, arr) / len(arr)


def D(arr: List[int]) -> int:
    arr2 = [i for i in map(lambda x: x * x, arr)]

    return M(arr2) - pow(M(arr), 2)


def R(x: int, y: int) -> int:
    Mxy = M([i * j for i, j in zip(x, y)])
    MxMy = M(x) * M(y)
    DxDy = D(x) * D(y)

    return (Mxy - MxMy) / pow(DxDy, 0.5)


def main() -> None:
    print("Phase 1")
    while True:
        try:
            source_number = int(input("Input source (1994 for check) or type 'Q' to exit:"))
            count = int(input("Input count (20 for check) or type 'Q' to exit:"))
        except ValueError:
            break

        z = mid_square_method(source_number, count)
        print(z)
        show_plot(z)

    print("Phase 2")
    pyplot.figure(figsize=(6, 6))
    while True:
        try:
            count_of_numbers = int(input("Input count or type 'Q' to exit:"))
        except ValueError:
            break

        z = multiplicative_congruential_method(count_of_numbers)
        show_plot(z)
        show_math_coeffs(z)
        # print get_correlation(z)

    DEFAULT_CONFIG = {"m": 2 ** 31, "k": 987654321, "A0": 2}
    print(
        R(
            multiplicative_congruential_method(10000, DEFAULT_CONFIG),
            multiplicative_congruential_method(10000)
        )
    )


if __name__ == "__main__":
    sys.exit(main())
