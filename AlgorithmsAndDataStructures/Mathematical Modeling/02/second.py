#!/usr/bin/env python3

import sys
import math
from typing import List, Dict, Tuple, Callable

import scipy.stats as st
from matplotlib import pyplot
from matplotlib.mlab import frange


Config = Dict[str, int]
DEFAULT_CONFIG = {'m': 2 ** 32, 'k': 987654321, 'A0': 2}


def init(config: Config) -> Tuple[int, int, int]:
    A0 = config['A0']
    k = config['k']
    m = config['m']

    return A0, k, m


def multiplicative_congruential_method(count: int, config: Config=DEFAULT_CONFIG) -> List[int]:
    A0, k, m = init(config)
    A = [A0]
    for i in range(count):
        newItem = (k * A[i]) % m
        A.append(newItem)

    return [A[i] / float(m) for i in range(1, len(A))]


def plot_init() -> None:
    pyplot.style.use('seaborn')
    pyplot.figure(figsize=(6, 6))


def exp_init() -> None:
    global _lambda, x_min, x_max, n, x, y
    _lambda = 2.5
    x_min = 0.0
    x_max = 6.0
    n = 100
    h = (x_max - x_min) / n
    x = [i * h for i in range(n)]
    # экспоненциальное распределение
    y = [_lambda * (math.e ** (-_lambda * x[i])) for i in range(len(x))]


def reversed_function(source_func: List[int]) -> List[int]:
    return [-1.0 * (1.0 / _lambda) * math.log(1 - source_func[i])
            for i in range(len(source_func))]


def view_math_details(reversed_func: List[int]) -> None:
    M1 = sum(reversed_func) / len(reversed_func)
    D1 = sum([reversed_func[i] ** 2 - M1 ** 2 for i in range(len(reversed_func))]) / len(reversed_func)
    print('{0} numbers. M = {1}. D = {2}'.format(len(reversed_func), M1, D1))


def show_histogram_and_plot(func: List[int]) -> None:
    pyplot.subplot(1, 1, 1)
    pyplot.hist(func, normed=1, color='C2')
    pyplot.plot(x, y)
    pyplot.show()


def get_function_of_continuous_random_variable(interval: List[int]) -> List[int]:
    return [1 - math.e ** (-_lambda * interval[i]) for i in range(len(interval))]


def get_criterion_of_consent_kolomogorov(practice_func: List[int], interval: List[int]) -> int:
    teor = get_function_of_continuous_random_variable(practice_func)

    return math.sqrt(n) * max(abs(interval[i] - teor[i]) for i in range(len(practice_func)))


def show_point_estimate_graph(practice_func: List[int], interval: List[int]) -> None:
    pyplot.subplot(1, 1, 1)
    pyplot.plot(practice_func, interval)
    pyplot.plot(x, F)
    pyplot.show()


def get_interval_func(practice_func: List[int]) -> List[int]:
    return [i / float(len(practice_func)) for i in range(len(practice_func))]


def main() -> None:
    plot_init()
    exp_init()
    #  сравнение экспоненциального распределения с гистограммой

    count = 100
    x1 = multiplicative_congruential_method(count)
    y1 = reversed_function(x1)
    view_math_details(y1)
    show_histogram_and_plot(y1)

    count = 10000
    x2 = multiplicative_congruential_method(count)
    y2 = reversed_function(x2)
    view_math_details(y2)
    show_histogram_and_plot(y2)

    # phase 2 точечная оценка
    plot_init()
    # функция непрерывной случайной величины
    global F
    F = get_function_of_continuous_random_variable(x)

    y1.sort()
    F1 = get_interval_func(y1)
    show_point_estimate_graph(y1, F1)

    y2.sort()
    F2 = get_interval_func(y2)
    show_point_estimate_graph(y2, F2)

    # phase 3

    print(get_criterion_of_consent_kolomogorov(y1, F1))
    print(get_criterion_of_consent_kolomogorov(y2, F2))

    # интервальная оценка
    x_min = 0.01
    x_max = 0.99
    dx = 0.01
    d1 = sum([y1[i] ** 2 - (sum(y1) / len(y1)) ** 2 for i in range(len(y1))]) / len(y1)
    interval = frange(x_min, x_max, dx)
    s = math.sqrt(d1)  # корень из оценки дисперсии
    ylist = [st.norm.ppf((i + 1) / 2) * 2 * s / math.sqrt(n - 1) for i in interval]
    pyplot.plot(interval, ylist, 'b')
    pyplot.show()


if __name__ == "__main__":
    sys.exit(main())
