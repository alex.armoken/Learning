#include <gtest/gtest.h>

#include "crypto/utils/bit_helpers.hpp"

#include "crypto/belt/octet.hpp"
#include "crypto/belt/word.hpp"
#include "crypto/belt/belt.hpp"
#include "crypto/belt/key_schedule.hpp"


using namespace crypto::belt;
using namespace crypto::utils::bit_helpers;


TEST(belt, octet_get_data_test1)
{
    Octet octet(0b00001100);

    ASSERT_TRUE(octet.get_data() == 12);
    ASSERT_TRUE(static_cast<uint8_t>(octet) == 12);
    ASSERT_TRUE(static_cast<uint32_t>(octet) == 12);
}


TEST(belt, octet_get_tetrad_test1)
{
    Octet octet(0b00001100);

    ASSERT_TRUE(octet.get_left_tetrad() == 0);
    ASSERT_TRUE(octet.get_right_tetrad() == 12);
}


TEST(belt, octet_get_tetrad_test2)
{
    Octet octet(0b11101000);

    ASSERT_TRUE(octet.get_left_tetrad() == 0b1110);
    ASSERT_TRUE(octet.get_right_tetrad() == 0b1000);
}


TEST(belt, get_h_subst_test1)
{
    const Octet octet(0b10100010);
    const auto result = octet.get_h_subst();
    const Octet desired_result(0b10011011);

    ASSERT_TRUE(result == desired_result);
}


TEST(belt, get_h_subst_test2)
{
    const Octet octet(0b11111111);
    const auto result = octet.get_h_subst();
    const Octet desired_result(0b00011101);

    ASSERT_TRUE(result == desired_result);
}


TEST(belt, get_h_subst_test3)
{
    const Octet octet(0b00000000);
    const auto result = octet.get_h_subst();
    const Octet desired_result(0b10110001);

    ASSERT_TRUE(result == desired_result);
}


TEST(belt, word_get_octet_test1)
{
    Word word(0b11001000101110101001010010110001);
    ASSERT_TRUE(word[OctetIdx::FIRST]  == Octet(0b10110001));
    ASSERT_TRUE(word[OctetIdx::SECOND] == Octet(0b10010100));
    ASSERT_TRUE(word[OctetIdx::THIRD]  == Octet(0b10111010));
    ASSERT_TRUE(word[OctetIdx::FOURTH] == Octet(0b11001000));
}


TEST(belt, word_rot_hi_test1)
{
    Word word(0b11001000101110101001010010110001);
    ASSERT_TRUE(word.rot_hi(1).get_data() == 0b10010001011101010010100101100011);
}


TEST(belt, words_swap_test1)
{
    Word a(0b11011001010101110011101111010111);
    Word b(0b11001010100011101110111010110111);
    std::swap(a, b);
    ASSERT_TRUE(a == Word(0b11001010100011101110111010110111));
    ASSERT_TRUE(b == Word(0b11011001010101110011101111010111));
}


TEST(belt, block_get_word_test1)
{
    const Block block(
        0b01101001110011001010000111001001,
        0b00110101010101111100100111100011,
        0b11010110011010111100001111100000,
        0b11111010100010001111101001101110
    );
    ASSERT_TRUE(block.get_word(0) == Word(0b01101001110011001010000111001001));
    ASSERT_TRUE(block.get_word(1) == Word(0b00110101010101111100100111100011));
    ASSERT_TRUE(block.get_word(2) == Word(0b11010110011010111100001111100000));
    ASSERT_TRUE(block.get_word(3) == Word(0b11111010100010001111101001101110));
}


TEST(belt, get_keyschedule_test1)
{
    const std::vector<uint8_t> raw_key = {
        0b11101001, 0b11011110, 0b11100111, 0b00101100,
        0b10001111, 0b00001100, 0b00001111, 0b10100110,
        0b00101101, 0b11011011, 0b01001001, 0b11110100,
        0b01101111, 0b01110011, 0b10010110, 0b01000111,
        0b00000110, 0b00000111, 0b01010011, 0b00010110,
        0b11101101, 0b00100100, 0b01111010, 0b00110111,
        0b00111001, 0b11001011, 0b10100011, 0b10000011,
        0b00000011, 0b10101001, 0b10001011, 0b11110110
    };
    const auto key_schedule = *KeySchedule::get_schedule(raw_key);

    ASSERT_TRUE(key_schedule[1] == Word(0b11101001110111101110011100101100));
    ASSERT_TRUE(key_schedule[2] == Word(0b10001111000011000000111110100110));
    ASSERT_TRUE(key_schedule[3] == Word(0b00101101110110110100100111110100));
    ASSERT_TRUE(key_schedule[4] == Word(0b01101111011100111001011001000111));
    ASSERT_TRUE(key_schedule[5] == Word(0b00000110000001110101001100010110));
    ASSERT_TRUE(key_schedule[6] == Word(0b11101101001001000111101000110111));
    ASSERT_TRUE(key_schedule[7] == Word(0b00111001110010111010001110000011));
    ASSERT_TRUE(key_schedule[8] == Word(0b00000011101010011000101111110110));

    ASSERT_TRUE(key_schedule[9] == key_schedule[1]);
    ASSERT_TRUE(key_schedule[15] == key_schedule[7]);
}


TEST(belt, get_keyschedule_test2)
{
    const std::vector<uint8_t> raw_key = {
        0b11101001, 0b11011110, 0b11100111, 0b00101100,
        0b10001111, 0b00001100, 0b00001111, 0b10100110,
        0b00101101, 0b11011011, 0b01001001, 0b11110100,
        0b01101111, 0b01110011, 0b10010110, 0b01000111
    };
    const auto key_schedule = *KeySchedule::get_schedule(raw_key);

    ASSERT_TRUE(key_schedule[1] == Word(0b11101001110111101110011100101100));
    ASSERT_TRUE(key_schedule[2] == Word(0b10001111000011000000111110100110));
    ASSERT_TRUE(key_schedule[3] == Word(0b00101101110110110100100111110100));
    ASSERT_TRUE(key_schedule[4] == Word(0b01101111011100111001011001000111));
    ASSERT_TRUE(key_schedule[5] == Word(0b11101001110111101110011100101100));
    ASSERT_TRUE(key_schedule[6] == Word(0b10001111000011000000111110100110));
    ASSERT_TRUE(key_schedule[7] == Word(0b00101101110110110100100111110100));
    ASSERT_TRUE(key_schedule[8] == Word(0b01101111011100111001011001000111));

    ASSERT_TRUE(key_schedule[9] == key_schedule[1]);
    ASSERT_TRUE(key_schedule[15] == key_schedule[7]);
}


TEST(belt, get_keyschedule_test3)
{
    const std::vector<uint8_t> raw_key = {
        0b11101001, 0b11011110, 0b11100111, 0b00101100,
        0b10001111, 0b00001100, 0b00001111, 0b10100110,
        0b00101101, 0b11011011, 0b01001001, 0b11110100,
        0b01101111, 0b01110011, 0b10010110, 0b01000111,
        0b00000110, 0b00000111, 0b01010011, 0b00010110,
        0b11101101, 0b00100100, 0b01111010, 0b00110111
    };
    const auto key_schedule = *KeySchedule::get_schedule(raw_key);

    ASSERT_TRUE(key_schedule[1] == Word(0b11101001110111101110011100101100));
    ASSERT_TRUE(key_schedule[2] == Word(0b10001111000011000000111110100110));
    ASSERT_TRUE(key_schedule[3] == Word(0b00101101110110110100100111110100));
    ASSERT_TRUE(key_schedule[4] == Word(0b01101111011100111001011001000111));
    ASSERT_TRUE(key_schedule[5] == Word(0b00000110000001110101001100010110));
    ASSERT_TRUE(key_schedule[6] == Word(0b11101101001001000111101000110111));
    ASSERT_TRUE(key_schedule[7] == Word(0b01001011000010011010000101111110));
    ASSERT_TRUE(key_schedule[8] == Word(0b10000100010100001011111101100110));

    ASSERT_TRUE(key_schedule[9] == key_schedule[1]);
    ASSERT_TRUE(key_schedule[15] == key_schedule[7]);
}


TEST(belt, first_step_test1)
{
    const std::vector<uint8_t> raw_key = {
        0b11101001, 0b11011110, 0b11100111, 0b00101100,
        0b10001111, 0b00001100, 0b00001111, 0b10100110,
        0b00101101, 0b11011011, 0b01001001, 0b11110100,
        0b01101111, 0b01110011, 0b10010110, 0b01000111,
        0b00000110, 0b00000111, 0b01010011, 0b00010110,
        0b11101101, 0b00100100, 0b01111010, 0b00110111,
        0b00111001, 0b11001011, 0b10100011, 0b10000011,
        0b00000011, 0b10101001, 0b10001011, 0b11110110
    };
    const auto key_schedule = *KeySchedule::get_schedule(raw_key);

    Word a(0b10110001100101001011101011001000);
    Word b(0b00001010000010001111010100111011);
    b ^= BelT::G_5(a + key_schedule[1]);

    Word desired_result(0b01100110110111001001100001101000);
    ASSERT_TRUE(b == desired_result);
}


TEST(belt, first_step_test2)
{
    const std::vector<uint8_t> raw_key = {
        0b11101001, 0b11011110, 0b11100111, 0b00101100,
        0b10001111, 0b00001100, 0b00001111, 0b10100110,
        0b00101101, 0b11011011, 0b01001001, 0b11110100,
        0b01101111, 0b01110011, 0b10010110, 0b01000111,
        0b00000110, 0b00000111, 0b01010011, 0b00010110,
        0b11101101, 0b00100100, 0b01111010, 0b00110111,
        0b00111001, 0b11001011, 0b10100011, 0b10000011,
        0b00000011, 0b10101001, 0b10001011, 0b11110110
    };
    const auto key_schedule = *KeySchedule::get_schedule(raw_key);

    Word d(0b01011000010010100101110111100100);
    Word c(0b00110110011011010000000010001110);
    c ^= BelT::G_21(d + key_schedule[2]);

    Word desired_result(0b11111001010111100110100110011000);
    ASSERT_TRUE(c == desired_result);
}


TEST(belt, first_step_test3)
{
    const std::vector<uint8_t> raw_key = {
        0b11101001, 0b11011110, 0b11100111, 0b00101100,
        0b10001111, 0b00001100, 0b00001111, 0b10100110,
        0b00101101, 0b11011011, 0b01001001, 0b11110100,
        0b01101111, 0b01110011, 0b10010110, 0b01000111,
        0b00000110, 0b00000111, 0b01010011, 0b00010110,
        0b11101101, 0b00100100, 0b01111010, 0b00110111,
        0b00111001, 0b11001011, 0b10100011, 0b10000011,
        0b00000011, 0b10101001, 0b10001011, 0b11110110
    };
    const auto key_schedule = *KeySchedule::get_schedule(raw_key);

    Word a(0b10110001100101001011101011001000);
    Word b(0b1100110110111001001100001101000);
    a -= BelT::G_13(b + key_schedule[3]);

    Word desired_result(0b1001101110101101011100000010);
    ASSERT_TRUE(a == desired_result);
}


TEST(belt, first_step_test4)
{
    const std::vector<uint8_t> raw_key = {
        0b11101001, 0b11011110, 0b11100111, 0b00101100,
        0b10001111, 0b00001100, 0b00001111, 0b10100110,
        0b00101101, 0b11011011, 0b01001001, 0b11110100,
        0b01101111, 0b01110011, 0b10010110, 0b01000111,
        0b00000110, 0b00000111, 0b01010011, 0b00010110,
        0b11101101, 0b00100100, 0b01111010, 0b00110111,
        0b00111001, 0b11001011, 0b10100011, 0b10000011,
        0b00000011, 0b10101001, 0b10001011, 0b11110110
    };
    const auto key_schedule = *KeySchedule::get_schedule(raw_key);

    Word c(0b11011001010101110011101111010111);
    Word d(0b11001010100011101110111010110111);
    c ^= BelT::G_5(d + key_schedule[7]);

    Word desired_result(0b11001100010011100100010000011101);
    ASSERT_TRUE(c == desired_result);
}


TEST(belt, first_step_test5)
{
    const std::vector<uint8_t> raw_key = {
        0b11101001, 0b11011110, 0b11100111, 0b00101100,
        0b10001111, 0b00001100, 0b00001111, 0b10100110,
        0b00101101, 0b11011011, 0b01001001, 0b11110100,
        0b01101111, 0b01110011, 0b10010110, 0b01000111,
        0b00000110, 0b00000111, 0b01010011, 0b00010110,
        0b11101101, 0b00100100, 0b01111010, 0b00110111,
        0b00111001, 0b11001011, 0b10100011, 0b10000011,
        0b00000011, 0b10101001, 0b10001011, 0b11110110
    };
    const auto key_schedule = *KeySchedule::get_schedule(raw_key);

    Word d(0b1011000010010100101110111100100);
    Word c(0b11011001010101110011101111010111);
    d += BelT::G_13(c + key_schedule[5]);

    Word desired_result(0b11001010100011101110111010110111);
    ASSERT_TRUE(d == desired_result);
}


TEST(belt, encrypt_block_test1)
{
    const Block input_block(
        0b10110001100101001011101011001000,
        0b00001010000010001111010100111011,
        0b00110110011011010000000010001110,
        0b01011000010010100101110111100100
    );
    const std::vector<uint8_t> raw_key = {
        0b11101001, 0b11011110, 0b11100111, 0b00101100,
        0b10001111, 0b00001100, 0b00001111, 0b10100110,
        0b00101101, 0b11011011, 0b01001001, 0b11110100,
        0b01101111, 0b01110011, 0b10010110, 0b01000111,
        0b00000110, 0b00000111, 0b01010011, 0b00010110,
        0b11101101, 0b00100100, 0b01111010, 0b00110111,
        0b00111001, 0b11001011, 0b10100011, 0b10000011,
        0b00000011, 0b10101001, 0b10001011, 0b11110110
    };
    const auto key_schedule = *KeySchedule::get_schedule(raw_key);
    const auto result = BelT::encrypt_block(input_block, key_schedule);

    const Block desired_result(
        0b01101001110011001010000111001001,
        0b00110101010101111100100111100011,
        0b11010110011010111100001111100000,
        0b11111010100010001111101001101110
    );
    ASSERT_TRUE(result == desired_result);
}


TEST(belt, decrypt_block_test1)
{
    const Block input_block(
        0b01101001110011001010000111001001,
        0b00110101010101111100100111100011,
        0b11010110011010111100001111100000,
        0b11111010100010001111101001101110
    );
    const Block desired_result(
        0b10110001100101001011101011001000,
        0b00001010000010001111010100111011,
        0b00110110011011010000000010001110,
        0b01011000010010100101110111100100
    );
    const std::vector<uint8_t> raw_key = {
        0b11101001, 0b11011110, 0b11100111, 0b00101100,
        0b10001111, 0b00001100, 0b00001111, 0b10100110,
        0b00101101, 0b11011011, 0b01001001, 0b11110100,
        0b01101111, 0b01110011, 0b10010110, 0b01000111,
        0b00000110, 0b00000111, 0b01010011, 0b00010110,
        0b11101101, 0b00100100, 0b01111010, 0b00110111,
        0b00111001, 0b11001011, 0b10100011, 0b10000011,
        0b00000011, 0b10101001, 0b10001011, 0b11110110
    };
    const auto key_schedule = *KeySchedule::get_schedule(raw_key);

    const auto result = BelT::decrypt_block(input_block, key_schedule);

    ASSERT_TRUE(result == desired_result);
}
