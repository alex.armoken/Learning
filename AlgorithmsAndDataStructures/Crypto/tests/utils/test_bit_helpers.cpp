#include <gtest/gtest.h>

#include "crypto/utils/bit_helpers.hpp"

using namespace crypto::utils::bit_helpers;


TEST(helpers, get_bit_value_test)
{
    uint8_t byte1 = 0b01101101;
    ASSERT_TRUE(get_bit_value(byte1, 0));
    ASSERT_FALSE(get_bit_value(byte1, 1));
    ASSERT_TRUE(get_bit_value(byte1, 6));
    ASSERT_FALSE(get_bit_value(byte1, 7));

    uint8_t byte2 = 0b10101000;
    ASSERT_FALSE(get_bit_value(byte2, 0));
    ASSERT_FALSE(get_bit_value(byte2, 2));
    ASSERT_TRUE(get_bit_value(byte2, 3));
    ASSERT_TRUE(get_bit_value(byte2, 7));
}


TEST(helpers, set_bit_value_test)
{
    uint8_t byte1 = 0b00000000;
    uint8_t mutated1 = 0b00001000;
    ASSERT_TRUE(set_bit_value(byte1, 3, true) == mutated1);

    uint8_t byte2 = 0b10101000;
    uint8_t mutated2 = 0b00101000;
    ASSERT_TRUE(set_bit_value(byte2, 7, false) == mutated2);
}


TEST(helpers, split_bytes_test)
{
    const auto result = split_bytes(0b10110001100101001011101011001000);
    std::array<uint8_t, sizeof(uint32_t)> desired_result = {
       0b10110001,
       0b10010100,
       0b10111010,
       0b11001000
    };

    ASSERT_TRUE(result == desired_result);
}
