#include <gtest/gtest.h>

#include "crypto/utils/bit_block_helpers.hpp"
#include "crypto/utils/constants.hpp"

using namespace crypto::utils;
using namespace crypto::utils::bit_block;


TEST(helpers, cycle_left_shift_test1)
{
    std::bitset<5> original1("01101");
    std::bitset<5> mutated1("11010");
    rotate_left(original1);
    ASSERT_TRUE(original1 == mutated1);

    std::bitset<7> original2("1011010");
    std::bitset<7> mutated2("0110101");
    rotate_left(original2);
    ASSERT_TRUE(original2 == mutated2);
}


TEST(helpers, cycle_left_shift_test2)
{
    std::bitset<5> original1("01101");
    std::bitset<5> mutated1("01011");
    rotate_left(original1, 3);
    ASSERT_TRUE(original1 == mutated1);

    std::bitset<7> original2("1011010");
    std::bitset<7> mutated2("1010110");
    rotate_left(original2, 5);
    ASSERT_TRUE(original2 == mutated2);
}


TEST(helpers, cycle_right_shift_test1)
{
    std::bitset<5> original1("01101");
    std::bitset<5> mutated1("10110");
    rotate_right(original1);
    ASSERT_TRUE(original1 == mutated1);

    std::bitset<7> original2("1011010");
    std::bitset<7> mutated2("0101101");
    rotate_right(original2);
    ASSERT_TRUE(original2 == mutated2);
}


TEST(helpers, cycle_right_shift_test2)
{
    std::bitset<5> original1("01101");
    std::bitset<5> mutated1("10101");
    rotate_right(original1, 3);
    ASSERT_TRUE(original1 == mutated1);

    std::bitset<7> original2("1011010");
    std::bitset<7> mutated2("1101010");
    rotate_right(original2, 5);
    ASSERT_TRUE(original2 == mutated2);
}


TEST(helpers, append_to_arr_of_bytes_test1)
{
    const auto BYTES_COUNT = 2;
    BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock(
        "00011011"
        "00010001"
    );
    std::vector<uint8_t> desired_result = {27, 17};

    std::vector<uint8_t> result;
    append_to_arr_of_bytes<BYTES_COUNT>(bitblock, std::back_inserter(result));

    ASSERT_TRUE(desired_result == result);
}


TEST(helpers, append_to_arr_of_bytes_test2)
{
    const auto BYTES_COUNT = 3;
    BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock(
        "00011011"
        "00000000"
        "00010001"
    );
    std::vector<uint8_t> desired_result = {27, 0, 17};

    std::vector<uint8_t> result;
    append_to_arr_of_bytes<BYTES_COUNT>(bitblock, std::back_inserter(result));

    ASSERT_TRUE(desired_result == result);
}


TEST(helpers, find_idx_of_last_bit_test1)
{
    const auto BYTES_COUNT = 1;
    BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock("00001000");

    const auto idx = find_idx_of_last_bit<BYTES_COUNT * BYTE_SIZE>(bitblock);
    const auto desired_idx = 4;

    ASSERT_TRUE(idx == desired_idx);
}


TEST(helpers, find_idx_of_last_bit_test2)
{
    const auto BYTES_COUNT = 4;
    BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock(
        "00001000"
        "10000000"
        "00000000"
        "00000000"
    );

    const auto idx = find_idx_of_last_bit<BYTES_COUNT * BYTE_SIZE>(bitblock);
    const auto desired_idx = 8;

    ASSERT_TRUE(idx == desired_idx);
}


TEST(helpers, find_idx_of_last_bit_test3)
{
    const auto BYTES_COUNT = 4;
    BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock(
        "00001000"
        "00000000"
        "00000000"
        "10000000"
    );

    const auto idx = find_idx_of_last_bit<BYTES_COUNT * BYTE_SIZE>(bitblock);
    const auto desired_idx = 24;

    ASSERT_TRUE(idx == desired_idx);
}


TEST(helpers, append_last_block_to_arr_of_bytes_test1)
{
    const auto BYTES_COUNT = 4;
    BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock(
        "00001000"
        "00000000"
        "00000000"
        "10000000"
    );
    const std::vector<uint8_t> desired_result = {8, 0, 0};

    std::vector<uint8_t> result;
    append_last_block_to_arr_of_bytes<BYTES_COUNT>(
        bitblock,
        std::back_inserter(result)
    );

    ASSERT_TRUE(result == desired_result);
}


TEST(helpers, append_last_block_to_arr_of_bytes_test2)
{
    const auto BYTES_COUNT = 7;
    BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock(
        "00001000"
        "00000000"
        "10000000"
        "00000000"
        "11100100"
        "10000000"
        "00000000"
    );
    const std::vector<uint8_t> desired_result = {8, 0, 128, 0, 228};

    std::vector<uint8_t> result;
    append_last_block_to_arr_of_bytes<BYTES_COUNT>(
        bitblock,
        std::back_inserter(result)
    );

    ASSERT_TRUE(result == desired_result);
}


TEST(helpers, split_blocks_test1)
{
    const auto BYTES_COUNT = 8;
    BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock(
        "00001000"
        "00000000"
        "10000000"
        "00000000"
        "11100100"
        "10000000"
        "00000000"
        "01100100"
    );

    const BitBlock<BYTES_COUNT / 2 * BYTE_SIZE> desired_result_1(
        "00001000"
        "00000000"
        "10000000"
        "00000000"

    );
    const BitBlock<BYTES_COUNT / 2 * BYTE_SIZE> desired_result_2(
        "11100100"
        "10000000"
        "00000000"
        "01100100"
    );

    const auto splitted_blocks = split_blocks<BYTES_COUNT / 2 * BYTE_SIZE>(bitblock);

    ASSERT_TRUE(std::get<0>(splitted_blocks) == desired_result_1);
    ASSERT_TRUE(std::get<1>(splitted_blocks) == desired_result_2);
}


TEST(helpers, split_blocks_test2)
{
    const auto BYTES_COUNT = 4;
    BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock(
        "00001000"
        "00000000"
        "10000000"
        "00000000"
    );

    const BitBlock<BYTES_COUNT / 2 * BYTE_SIZE> desired_result_1(
        "00001000"
        "00000000"

    );
    const BitBlock<BYTES_COUNT / 2 * BYTE_SIZE> desired_result_2(
        "10000000"
        "00000000"
    );

    const auto splitted_blocks = split_blocks<BYTES_COUNT / 2 * BYTE_SIZE>(bitblock);

    ASSERT_TRUE(std::get<0>(splitted_blocks) == desired_result_1);
    ASSERT_TRUE(std::get<1>(splitted_blocks) == desired_result_2);
}


TEST(helpers, concat_blocks_test1)
{
    const auto BYTES_COUNT = 4;
    const BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock_1(
        "00001000"
        "00000000"
        "10000000"
        "00000000"

    );
    const BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock_2(
        "11100100"
        "10000000"
        "00000000"
        "01100100"
    );

    BitBlock<BYTES_COUNT * 2 * BYTE_SIZE> desired_result(
        "00001000"
        "00000000"
        "10000000"
        "00000000"
        "11100100"
        "10000000"
        "00000000"
        "01100100"
    );

    const auto result = concat_blocks<BYTES_COUNT * BYTE_SIZE>(bitblock_1, bitblock_2);

    ASSERT_TRUE(result == desired_result);
}


TEST(helpers, concat_blocks_test2)
{
    const auto BYTES_COUNT = 2;
    const BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock_1(
        "00001000"
        "00000000"

    );
    const BitBlock<BYTES_COUNT * BYTE_SIZE> bitblock_2(
        "11100100"
        "10000000"
    );

    BitBlock<BYTES_COUNT * 2 * BYTE_SIZE> desired_result(
        "00001000"
        "00000000"
        "11100100"
        "10000000"
    );

    const auto result = concat_blocks<BYTES_COUNT * BYTE_SIZE>(bitblock_1, bitblock_2);

    ASSERT_TRUE(result == desired_result);
}
