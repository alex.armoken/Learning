#include <gtest/gtest.h>

#include "crypto/sha256/sha256.hpp"

void iterator_test1(SHA256::DataIter &iter)
{
    std::vector<uint8_t> var_out = {0b01011110, 0b01011111,
                                    0b00011110, 0b11111111};
    for (uint8_t i = 0; i < 4; i++) {
        ASSERT_EQ(var_out[i], *iter) << (int)i;
        iter++;
    }
    ASSERT_EQ(0x80, *iter);
    iter++;
    for (uint8_t i = 0; i < 13 * 4 - 1; i++) {
        ASSERT_EQ(0, *iter) << (int)i;
        iter++;
    }
    for (uint8_t i = 0; i < 7; i++) {
        ASSERT_EQ(0, *iter) << (int)i;
        iter++;
    }
    ASSERT_EQ(32, *iter);
    ASSERT_TRUE(iter.is_end());
}

void iterator_test2(SHA256::DataIter &iter)
{
    std::vector<uint8_t> var_out = {
        0, 0, 0, 1,
        0, 0, 0, 3,
        0, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 3,
        0, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 9,

        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 9,
        0x80, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0b10, 0b01000000
    };

    for (uint8_t i = 0; i < var_out.size(); i++) {
        ASSERT_EQ(var_out[i], *iter) << (int)i;
        iter++;
    }
    ASSERT_TRUE(iter.is_end());
}


TEST(ArrIter, arr_iter_test1)
{
    std::vector<uint8_t> var_out = {0b01011110, 0b01011111,
                                    0b00011110, 0b11111111};
    SHA256::ArrIter iter(var_out);
    iterator_test1(iter);
}

TEST(ArrIter, arr_iter_test2)
{
    std::vector<uint8_t> var_out = {
        0, 0, 0, 1,
        0, 0, 0, 3,
        0, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 3,
        0, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 9,

        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,

        0, 0, 0, 0,
        0, 0, 0, 9,
    };

    SHA256::ArrIter iter(var_out);
    iterator_test2(iter);
}

TEST(SHA256, get_word_test1)
{
    std::string path = "test.txt";
    std::ofstream out(path);
    std::vector<uint8_t> var_out = {0b01011110, 0b01011111,
                                    0b00011110, 0b11111111};
    for (uint32_t i = 0; i < SHA256::BYTES_IN_WORD; i++) {
        out << var_out[i];
    }
    out.close();

    uint32_t word = 0;
    SHA256::FileIter iter(path);
    ASSERT_EQ(SHA256::BUF_FULL, SHA256::get_word(iter, word));
    ASSERT_EQ(1583292159, word);
}

TEST(SHA256, get_block_test1)
{
    std::vector<uint8_t> data = {
        'a', 'b', 'c', 'd',
        'b', 'c', 'd', 'e',
        'c', 'd', 'e', 'f',
        'd', 'e', 'f', 'g',

        'e', 'f', 'g', 'h',
        'f', 'g', 'h', 'i',
        'g', 'h', 'i', 'j',
        'h', 'i', 'j', 'k',

        'i', 'j', 'k', 'l',
        'j', 'k', 'l', 'm',
        'k', 'l', 'm', 'n',
        'l', 'm', 'n', 'o',

        'm', 'n', 'o', 'p',
        'n', 'o', 'p', 'q',
        'm', 'n', 'o', 'p',
        'n', 'o', 'p', 'q',

        'a', 'b', 'c', 'd',
        'b', 'c', 'd', 'e',
        'c', 'd', 'e', 'f',
        'd', 'e', 'f', 'g',

        'e', 'f', 'g', 'h',
        'f', 'g', 'h', 'i',
        'g', 'h', 'i', 'j',
        'h', 'i', 'j', 'k',

        'i', 'j', 'k', 'l',
        'j', 'k', 'l', 'm',
        'k', 'l', 'm', 'n',
        'l', 'm', 'n', 'o',

        'm', 'n', 'o', 'p',
        'n', 'o', 'p', 'q',
        'n', 'o', 'p', 'q',
    };

    SHA256::ArrIter iter(data);
    std::vector<uint32_t> var_in;
    ASSERT_EQ(SHA256::BUF_FULL, SHA256::get_block(iter, var_in));
    ASSERT_EQ(SHA256::BUF_FULL, SHA256::get_block(iter, var_in));
    ASSERT_EQ(SHA256::LAST_BUF, SHA256::get_block(iter, var_in));
}

TEST(SHA256, get_block_test2)
{
    std::vector<uint8_t> data = {
        'a', 'b', 'c', 'd',
        'b', 'c', 'd', 'e',
        'c', 'd', 'e', 'f',
        'd', 'e', 'f', 'g'
    };

    SHA256::ArrIter iter(data);
    std::vector<uint32_t> var_in;
    ASSERT_EQ(SHA256::LAST_BUF, SHA256::get_block(iter, var_in));
}

TEST(SHA256, hash_block1)
{
    std::vector<uint32_t> hash;
    SHA256::init_digest(hash);

    std::vector<uint32_t> block1 = {
        0x61626364, 0x62636465, 0x63646566, 0x64656667,
        0x65666768, 0x66676869, 0x6768696a, 0x68696a6b,
        0x696a6b6c, 0x6a6b6c6d, 0x6b6c6d6e, 0x6c6d6e6f,
        0x6d6e6f70, 0x6e6f7071, 0x80000000, 0x00000000,
    };

    for (uint8_t i = 16; i < 64; i++) {
        uint32_t s0 = SHA256::f3(block1[i - 15]);
        uint32_t s1 = SHA256::f4(block1[i - 2]);
        block1.push_back(block1[i - 16] + s0 + block1[i - 7] + s1);
    }

    SHA256::hash_block(block1, hash);
    std::vector<uint32_t> desired_hash = {
        0x85e655d6, 0x417a1795, 0x3363376a, 0x624cde5c,
        0x76e09589, 0xcac5f811, 0xcc4b32c1, 0xf20e533a
    };

    for (uint8_t i = 0; i < 8; i++) {
        ASSERT_EQ(desired_hash[i], hash[i]) << (int)i;
    }

    std::vector<uint32_t> block2 = {
        0x00000000, 0x00000000, 0x00000000, 0x00000000,
        0x00000000, 0x00000000, 0x00000000, 0x00000000,
        0x00000000, 0x00000000, 0x00000000, 0x00000000,
        0x00000000, 0x00000000, 0x00000000, 0x000001c0,
    };
    SHA256::hash_block(block2, hash);
    std::vector<uint32_t> desired_hash2 = {
        0x248d6a61, 0xd20638b8, 0xe5c02693, 0x0c3e6039,
        0xa33ce459, 0x64ff2167, 0xf6ecedd4, 0x19db06c1,
    };

    for (uint8_t i = 0; i < 8; i++) {
        ASSERT_EQ(desired_hash2[i], hash[i]) << (int)i;
    }
}


int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);

    return RUN_ALL_TESTS();
}
