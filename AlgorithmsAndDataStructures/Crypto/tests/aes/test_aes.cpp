#include <gtest/gtest.h>

#include "crypto/aes/aes.hpp"

using namespace crypto::aes;


bool compare_words(gword& a, gword& b)
{
    bool result = true;
    auto cmp_result = std::mismatch(a.begin(), a.end(), b.begin(), b.end());
    if (cmp_result.first != a.end()) {
        std::cout << a[0] << " "
                  << a[1] << " "
                  << a[2] << " "
                  << a[3] << std::endl;
        result = false;
    }
    return result;
}

bool compare_blocks(gblock& a, gblock& b)
{
    bool result = true;
    for (uint32_t i = 0; i < b.size(); i++) {
        if (!compare_words(a[i], b[i])) {
            result = false;
            std::cout << "Index: " << i << std::endl;
            break;
        }
    }
    return result;
}

TEST(aes, cycle_left_shift_test)
{
    gword w = { 1, 3, 4, 6 };
    gword w2 = { 3, 4, 6, 1 };
    AES::cycle_left_shift(w);
    ASSERT_TRUE(compare_words(w, w2));
}

TEST(aes, cycle_right_shift_test)
{
    gword w = { 1, 3, 4, 6 };
    gword w2 = { 6, 1, 3, 4 };
    AES::cycle_right_shift(w);
    ASSERT_TRUE(compare_words(w, w2));
}

TEST(aes, shift_rows_test)
{
    gblock b = {
        { 0x63, 0xca, 0xb7, 0x04 },
        { 0x09, 0x53, 0xd0, 0x51 },
        { 0xcd, 0x60, 0xe0, 0xe7 },
        { 0xba, 0x70, 0xe1, 0x8c },
    };
    gblock b2 = {
        { 0x63, 0x53, 0xe0, 0x8c },
        { 0x09, 0x60, 0xe1, 0x04 },
        { 0xcd, 0x70, 0xb7, 0x51 },
        { 0xba, 0xca, 0xd0, 0xe7 },
    };
    AES::shift_rows(b);
    ASSERT_TRUE(compare_blocks(b, b2));
}

TEST(aes, inverse_shift_rows_test)
{
    gblock b = {
        { 0x63, 0x53, 0xe0, 0x8c },
        { 0x09, 0x60, 0xe1, 0x04 },
        { 0xcd, 0x70, 0xb7, 0x51 },
        { 0xba, 0xca, 0xd0, 0xe7 },
    };
    gblock b2 = {
        { 0x63, 0xca, 0xb7, 0x04 },
        { 0x09, 0x53, 0xd0, 0x51 },
        { 0xcd, 0x60, 0xe0, 0xe7 },
        { 0xba, 0x70, 0xe1, 0x8c },
    };
    AES::inverse_shift_rows(b);
    ASSERT_TRUE(compare_blocks(b, b2));
}

TEST(aes, inverse_shift_rows_test2)
{
    gblock b = {
        {0xa7, 0xbe, 0x1a, 0x69},
        {0x97, 0xad, 0x73, 0x9b},
        {0xd8, 0xc9, 0xca, 0x45},
        {0x1f, 0x61, 0x8b, 0x61},
    };
    gblock b2 = {
        {0xa7, 0x61, 0xca, 0x9b},
        {0x97, 0xbe, 0x8b, 0x45},
        {0xd8, 0xad, 0x1a, 0x61},
        {0x1f, 0xc9, 0x73, 0x69},
    };
    AES::inverse_shift_rows(b);
    ASSERT_TRUE(compare_blocks(b, b2));
}

TEST(aes, inverse_shift_rows_test3)
{
    gblock b = {
        {0xa7, 0xbe, 0x1a, 0x69},
        {0x97, 0xad, 0x73, 0x9b},
        {0xd8, 0xc9, 0xca, 0x45},
        {0x1f, 0x61, 0x8b, 0x61},
    };
    gblock b2 = {
        {0xa7, 0x61, 0xca, 0x9b},
        {0x97, 0xbe, 0x8b, 0x45},
        {0xd8, 0xad, 0x1a, 0x61},
        {0x1f, 0xc9, 0x73, 0x69},
    };
    AES::inverse_shift_rows(b);
    ASSERT_TRUE(compare_blocks(b, b2));
}

TEST(aes, mix_columns_test)
{
    gblock b = {
        { 0x63, 0x53, 0xe0, 0x8c },
        { 0x09, 0x60, 0xe1, 0x04 },
        { 0xcd, 0x70, 0xb7, 0x51 },
        { 0xba, 0xca, 0xd0, 0xe7 }
    };

    gblock b2 = {
        { 0x5f, 0x72, 0x64, 0x15 },
        { 0x57, 0xf5, 0xbc, 0x92 },
        { 0xf7, 0xbe, 0x3b, 0x29 },
        { 0x1d, 0xb9, 0xf9, 0x1a }
    };

    AES::mix_columns(b);
    ASSERT_TRUE(compare_blocks(b, b2));
}

TEST(aes, inverse_mix_columns_test)
{
    gblock b = {
        { 0x5f, 0x72, 0x64, 0x15 },
        { 0x57, 0xf5, 0xbc, 0x92 },
        { 0xf7, 0xbe, 0x3b, 0x29 },
        { 0x1d, 0xb9, 0xf9, 0x1a }
    };

    gblock b2 = {
        { 0x63, 0x53, 0xe0, 0x8c },
        { 0x09, 0x60, 0xe1, 0x04 },
        { 0xcd, 0x70, 0xb7, 0x51 },
        { 0xba, 0xca, 0xd0, 0xe7 }
    };

    AES::inverse_mix_columns(b);
    ASSERT_TRUE(compare_blocks(b, b2));
}

TEST(aes, substitue_word_test)
{
    gword w = { 0x00, 0x10, 0x20, 0x30 };
    gword w2 = { 0x63, 0xca, 0xb7, 0x04 };
    AES::substitute_word(w);
    compare_words(w, w2);
}

TEST(aes, substitue_block_test)
{
    gblock b = {
        { 0x00, 0x10, 0x20, 0x30 },
        { 0x40, 0x50, 0x60, 0x70 },
        { 0x80, 0x90, 0xa0, 0xb0 },
        { 0xc0, 0xd0, 0xe0, 0xf0 }
    };
    gblock b2 = {
        { 0x63, 0xca, 0xb7, 0x04 },
        { 0x09, 0x53, 0xd0, 0x51 },
        { 0xcd, 0x60, 0xe0, 0xe7 },
        { 0xba, 0x70, 0xe1, 0x8c }
    };
    AES::substitute_block(b);
    ASSERT_TRUE(compare_blocks(b, b2));
}

TEST(aes, inverse_substitue_word_test)
{
    gword w = { 0x63, 0xca, 0xb7, 0x04 };
    gword w2 = { 0x00, 0x10, 0x20, 0x30 };
    AES::inverse_substitute_word(w);
    ASSERT_TRUE(compare_words(w, w2));
}

TEST(aes, inverse_substitue_block_test)
{
    gblock b = {
        { 0x63, 0xca, 0xb7, 0x04 },
        { 0x09, 0x53, 0xd0, 0x51 },
        { 0xcd, 0x60, 0xe0, 0xe7 },
        { 0xba, 0x70, 0xe1, 0x8c }
    };
    gblock b2 = {
        { 0x00, 0x10, 0x20, 0x30 },
        { 0x40, 0x50, 0x60, 0x70 },
        { 0x80, 0x90, 0xa0, 0xb0 },
        { 0xc0, 0xd0, 0xe0, 0xf0 }
    };
    AES::inverse_substitute_block(b);
    ASSERT_TRUE(compare_blocks(b, b2));
}

TEST(aes, add_word_test)
{
    gword w = { 0x8a, 0x84, 0xeb, 0x01 };
    gword w2 = { 0x01, 0x00, 0x00, 0x00 };
    AES::add_word(w, w2);
    gword w3 = { 0x8b, 0x84, 0xeb, 0x01 };
    ASSERT_TRUE(compare_words(w, w3));
}

TEST(aes, add_word_test2)
{
    gword w = { 0x50, 0x38, 0x6b, 0xe5 };
    gword w2 = { 0x02, 0x00, 0x00, 0x00 };
    AES::add_word(w, w2);
    gword w3 = { 0x52, 0x38, 0x6b, 0xe5 };
    ASSERT_TRUE(compare_words(w, w3));
}

TEST(aes, set_opts_test)
{
    AES::SessionOptions o;
    gword key = { 'q', 'w', 'e', 'r',
                  't', 'y', 'u', 'i',
                  'o', 'a', 's', 'd',
                  'f', 'g', 'h', 'z' };
    gword key2 = { 'q', 'w', 'e', 'r',
                   't', 'y', 'u', 'i',
                   'o', 'a', 's', 'd',
                   'f', 'g', 'h', 'z' };
    AES::set_opts(key, o);
    ASSERT_EQ(o.words_in_key, 4);
    ASSERT_EQ(o.rounds_count, 10);
    ASSERT_EQ(o.schedule_size, 44);

    ASSERT_TRUE(key.size() == key2.size()) << "Key size: " << key.size()
                                           << "Key2 size: " << key2.size();
    ASSERT_EQ(key, key2);
}

TEST(aes, set_opts_test2)
{
    AES::SessionOptions o;
    gword key = {
        'q', 'w', 'e', 'r',
        't', 'y', 'u', 'i',
        'o', 'a', 's', 'd',
    };
    gword key2 = { 'q', 'w', 'e', 'r',
                   't', 'y', 'u', 'i',
                   'o', 'a', 's', 'd',
                   0x01, 0x01, 0x01, 0x01 };
    AES::set_opts(key, o);
    ASSERT_EQ(o.words_in_key, 4);
    ASSERT_EQ(o.rounds_count, 10);
    ASSERT_EQ(o.schedule_size, 44);
    ASSERT_TRUE(key.size() == key2.size()) << "Key size: " << key.size()
                                           << "Key2 size: " << key2.size();
    ASSERT_EQ(key, key2);
}

TEST(aes, fill_schedule_with_key_test)
{
    AES::SessionOptions o;
    gword key = {
        'q', 'w', 'e', 'r',
        't', 'y', 'u', 'i',
        'o', 'a', 's', 'd',
        'f', 'g', 'h', 'z'
    };
    gblock b = {
        { 'q', 'w', 'e', 'r' },
        { 't', 'y', 'u', 'i' },
        { 'o', 'a', 's', 'd' },
        { 'f', 'g', 'h', 'z' }
    };
    AES::set_opts(key, o);
    AES::fill_schedule_with_key(key, o);
    ASSERT_TRUE(compare_blocks(*(o.schedule), b));
    delete o.schedule;
}

TEST(aes, schedule_expansion_test)
{
    AES::SessionOptions o;
    gword key = { 0xff, 0xff, 0xff, 0xff,
                  0xff, 0xff, 0xff, 0xff,
                  0xff, 0xff, 0xff, 0xff,
                  0xff, 0xff, 0xff, 0xff };
    gblock b = {
        { 0xff, 0xff, 0xff, 0xff },
        { 0xff, 0xff, 0xff, 0xff },
        { 0xff, 0xff, 0xff, 0xff },
        { 0xff, 0xff, 0xff, 0xff },

        { 0xe8, 0xe9, 0xe9, 0xe9 },
        { 0x17, 0x16, 0x16, 0x16 },
        { 0xe8, 0xe9, 0xe9, 0xe9 },
        { 0x17, 0x16, 0x16, 0x16 },

        { 0xad, 0xae, 0xae, 0x19 },
        { 0xba, 0xb8, 0xb8, 0x0f },
        { 0x52, 0x51, 0x51, 0xe6 },
        { 0x45, 0x47, 0x47, 0xf0 },

        { 0x09, 0x0e, 0x22, 0x77 },
        { 0xb3, 0xb6, 0x9a, 0x78 },
        { 0xe1, 0xe7, 0xcb, 0x9e },
        { 0xa4, 0xa0, 0x8c, 0x6e },

        { 0xe1, 0x6a, 0xbd, 0x3e },
        { 0x52, 0xdc, 0x27, 0x46 },
        { 0xb3, 0x3b, 0xec, 0xd8 },
        { 0x17, 0x9b, 0x60, 0xb6 },

        { 0xe5, 0xba, 0xf3, 0xce },
        { 0xb7, 0x66, 0xd4, 0x88 },
        { 0x04, 0x5d, 0x38, 0x50 },
        { 0x13, 0xc6, 0x58, 0xe6 },

        { 0x71, 0xd0, 0x7d, 0xb3 },
        { 0xc6, 0xb6, 0xa9, 0x3b },
        { 0xc2, 0xeb, 0x91, 0x6b },
        { 0xd1, 0x2d, 0xc9, 0x8d },

        { 0xe9, 0x0d, 0x20, 0x8d },
        { 0x2f, 0xbb, 0x89, 0xb6 },
        { 0xed, 0x50, 0x18, 0xdd },
        { 0x3c, 0x7d, 0xd1, 0x50 },

        { 0x96, 0x33, 0x73, 0x66 },
        { 0xb9, 0x88, 0xfa, 0xd0 },
        { 0x54, 0xd8, 0xe2, 0x0d },
        { 0x68, 0xa5, 0x33, 0x5d },

        { 0x8b, 0xf0, 0x3f, 0x23 },
        { 0x32, 0x78, 0xc5, 0xf3 },
        { 0x66, 0xa0, 0x27, 0xfe },
        { 0x0e, 0x05, 0x14, 0xa3 },

        { 0xd6, 0x0a, 0x35, 0x88 },
        { 0xe4, 0x72, 0xf0, 0x7b },
        { 0x82, 0xd2, 0xd7, 0x85 },
        { 0x8c, 0xd7, 0xc3, 0x26 },

    };
    AES::set_opts(key, o);
    AES::fill_schedule_with_key(key, o);
    AES::schedule_expansion(o);
    ASSERT_TRUE(compare_blocks(*(o.schedule), b));
    delete o.schedule;
}

TEST(aes, schedule_expansion_test2)
{
    AES::SessionOptions o;
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    gblock b = {
        { 0x00, 0x01, 0x02, 0x03 },
        { 0x04, 0x05, 0x06, 0x07 },
        { 0x08, 0x09, 0x0a, 0x0b },
        { 0x0c, 0x0d, 0x0e, 0x0f },

        { 0xd6, 0xaa, 0x74, 0xfd },
        { 0xd2, 0xaf, 0x72, 0xfa },
        { 0xda, 0xa6, 0x78, 0xf1 },
        { 0xd6, 0xab, 0x76, 0xfe },

        { 0xb6, 0x92, 0xcf, 0x0b },
        { 0x64, 0x3d, 0xbd, 0xf1 },
        { 0xbe, 0x9b, 0xc5, 0x00 },
        { 0x68, 0x30, 0xb3, 0xfe },

        { 0xb6, 0xff, 0x74, 0x4e },
        { 0xd2, 0xc2, 0xc9, 0xbf },
        { 0x6c, 0x59, 0x0c, 0xbf },
        { 0x04, 0x69, 0xbf, 0x41 },

        { 0x47, 0xf7, 0xf7, 0xbc },
        { 0x95, 0x35, 0x3e, 0x03 },
        { 0xf9, 0x6c, 0x32, 0xbc },
        { 0xfd, 0x05, 0x8d, 0xfd },

        { 0x3c, 0xaa, 0xa3, 0xe8 },
        { 0xa9, 0x9f, 0x9d, 0xeb },
        { 0x50, 0xf3, 0xaf, 0x57 },
        { 0xad, 0xf6, 0x22, 0xaa },

        { 0x5e, 0x39, 0x0f, 0x7d },
        { 0xf7, 0xa6, 0x92, 0x96 },
        { 0xa7, 0x55, 0x3d, 0xc1 },
        { 0x0a, 0xa3, 0x1f, 0x6b },

        { 0x14, 0xf9, 0x70, 0x1a },
        { 0xe3, 0x5f, 0xe2, 0x8c },
        { 0x44, 0x0a, 0xdf, 0x4d },
        { 0x4e, 0xa9, 0xc0, 0x26 },

        { 0x47, 0x43, 0x87, 0x35 },
        { 0xa4, 0x1c, 0x65, 0xb9 },
        { 0xe0, 0x16, 0xba, 0xf4 },
        { 0xae, 0xbf, 0x7a, 0xd2 },

        { 0x54, 0x99, 0x32, 0xd1 },
        { 0xf0, 0x85, 0x57, 0x68 },
        { 0x10, 0x93, 0xed, 0x9c },
        { 0xbe, 0x2c, 0x97, 0x4e },

        { 0x13, 0x11, 0x1d, 0x7f },
        { 0xe3, 0x94, 0x4a, 0x17 },
        { 0xf3, 0x07, 0xa7, 0x8b },
        { 0x4d, 0x2b, 0x30, 0xc5 },
    };
    AES::set_opts(key, o);
    AES::fill_schedule_with_key(key, o);
    AES::schedule_expansion(o);
    ASSERT_TRUE(compare_blocks(*(o.schedule), b));
    delete o.schedule;
}

TEST(aes, add_round_key_test)
{
    AES::SessionOptions o;
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    gblock b = {
        { 0x00, 0x11, 0x22, 0x33 },
        { 0x44, 0x55, 0x66, 0x77 },
        { 0x88, 0x99, 0xaa, 0xbb },
        { 0xcc, 0xdd, 0xee, 0xff },
    };
    gblock res = {
        { 0x00, 0x10, 0x20, 0x30 },
        { 0x40, 0x50, 0x60, 0x70 },
        { 0x80, 0x90, 0xa0, 0xb0 },
        { 0xc0, 0xd0, 0xe0, 0xf0 },
    };
    o = AES::set_key(key);
    AES::add_round_key(b, o);

    ASSERT_TRUE(compare_blocks(b, res));
    delete o.schedule;
}

TEST(aes, add_round_key_test2)
{
    AES::SessionOptions o;
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    gblock b = {
        { 0xc5, 0x7e, 0x1c, 0x15 },
        { 0x9a, 0x9b, 0xd2, 0x86 },
        { 0xf0, 0x5f, 0x4b, 0xe0 },
        { 0x98, 0xc6, 0x34, 0x39 },
    };
    gblock res = {
        { 0xd1, 0x87, 0x6c, 0x0f },
        { 0x79, 0xc4, 0x30, 0x0a },
        { 0xb4, 0x55, 0x94, 0xad },
        { 0xd6, 0x6f, 0xf4, 0x1f },
    };
    o = AES::set_key(key);
    o.cur_round = 7;
    AES::add_round_key(b, o);
    ASSERT_TRUE(compare_blocks(b, res));
    delete o.schedule;
}

TEST(aes, run_one_round_test)
{
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    gblock b = {
        { 0x00, 0x11, 0x22, 0x33 },
        { 0x44, 0x55, 0x66, 0x77 },
        { 0x88, 0x99, 0xaa, 0xbb },
        { 0xcc, 0xdd, 0xee, 0xff },
    };
    gblock out = {
        { 0xc8, 0x16, 0x77, 0xbc },
        { 0x9b, 0x7a, 0xc9, 0x3b },
        { 0x25, 0x02, 0x79, 0x92 },
        { 0xb0, 0x26, 0x19, 0x96 },
    };
    auto o = AES::set_key(key);
    AES::add_round_key(b, o);
    o.cur_round++;
    for (auto i = 0; i < 5; i++) {
        AES::run_one_round(b, o);
        o.cur_round++;
    }

    ASSERT_TRUE(compare_blocks(b, out));
    delete o.schedule;
}

TEST(aes, encrypt_block_test)
{
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    gblock b = {
        { 0x00, 0x11, 0x22, 0x33 },
        { 0x44, 0x55, 0x66, 0x77 },
        { 0x88, 0x99, 0xaa, 0xbb },
        { 0xcc, 0xdd, 0xee, 0xff },
    };
    gblock out = {
        { 0x69, 0xc4, 0xe0, 0xd8 },
        { 0x6a, 0x7b, 0x04, 0x30 },
        { 0xd8, 0xcd, 0xb7, 0x80 },
        { 0x70, 0xb4, 0xc5, 0x5a },
    };
    auto o = AES::set_key(key);
    AES::encrypt_block(b, o);
    ASSERT_TRUE(compare_blocks(b, out));
}

TEST(aes, run_one_inverse_round_test)
{
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    gblock b = {
        { 0x69, 0xc4, 0xe0, 0xd8 },
        { 0x6a, 0x7b, 0x04, 0x30 },
        { 0xd8, 0xcd, 0xb7, 0x80 },
        { 0x70, 0xb4, 0xc5, 0x5a },
    };
    gblock out = {
        { 0xb4, 0x58, 0x12, 0x4c },
        { 0x68, 0xb6, 0x8a, 0x01 },
        { 0x4b, 0x99, 0xf8, 0x2e },
        { 0x5f, 0x15, 0x55, 0x4c },
    };

    auto o = AES::set_key(key);
    o.cur_round = o.rounds_count;
    AES::add_round_key(b, o);
    o.cur_round--;
    for (uint8_t i = 0; i < 3; i++) {
        AES::run_one_inverse_round(b, o);
        o.cur_round--;
    }
    delete o.schedule;

    ASSERT_TRUE(compare_blocks(b, out));
}

TEST(aes, decrypt_block_test)
{
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    gblock b = {
        { 0x69, 0xc4, 0xe0, 0xd8 },
        { 0x6a, 0x7b, 0x04, 0x30 },
        { 0xd8, 0xcd, 0xb7, 0x80 },
        { 0x70, 0xb4, 0xc5, 0x5a },
    };
    gblock out = {
        { 0x00, 0x11, 0x22, 0x33 },
        { 0x44, 0x55, 0x66, 0x77 },
        { 0x88, 0x99, 0xaa, 0xbb },
        { 0xcc, 0xdd, 0xee, 0xff },
    };
    auto o = AES::set_key(key);
    AES::decrypt_block(b, o);
    ASSERT_TRUE(compare_blocks(b, out));
}

TEST(aes, decrypt_block_test2)
{
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    gblock b_in = {
        { 0x69, 0xc4, 0xe0, 0xd8 },
        { 0x6a, 0x7b, 0x04, 0x30 },
        { 0xd8, 0xcd, 0xb7, 0x80 },
        { 0x70, 0xb4, 0xc5, 0x5a },
    };
    gblock b_out = {
        { 0x00, 0x11, 0x22, 0x33 },
        { 0x44, 0x55, 0x66, 0x77 },
        { 0x88, 0x99, 0xaa, 0xbb },
        { 0xcc, 0xdd, 0xee, 0xff },
    };

    auto o = AES::set_key(key);
    AES::decrypt_block(b_in, o);
    ASSERT_TRUE(compare_blocks(b_in, b_out));
}

TEST(aes, read_word_test)
{
    std::string path = "test.txt";
    std::ofstream out(path);
    std::ifstream in(path);
    gword w_out = { 12, 34, 56, 88 };
    for (uint32_t i = 0; i < AES::BYTES_IN_WORD; i++) {
        out << w_out[i];
    }
    out.close();

    gword w_in;
    bool is_addition = false;
    AES::read_word(in, w_in, is_addition);
    ASSERT_TRUE(compare_words(w_in, w_out));
    in.close();
    std::remove(path.c_str());
}

TEST(aes, read_word_test2)
{
    std::string path = "test.txt";
    std::ofstream out(path);
    std::ifstream in(path);
    gword w_out = { 12, 34 };
    gword w_in_maybe = { 12, 34, 0x80, 0 };
    for (uint32_t i = 0; i < w_out.size(); i++) {
        out << w_out[i];
    }
    out.close();

    gword w_in;
    bool is_addition = false;
    AES::read_word(in, w_in, is_addition);
    ASSERT_TRUE(compare_words(w_in, w_in_maybe));
    in.close();
    std::remove(path.c_str());
}

TEST(aes, read_word_test3)
{
    std::string path = "test.txt";
    std::ofstream out(path);
    std::ifstream in(path);
    gword w_in_maybe = { 0, 0, 0, 0 };
    out.close();

    gword w_in;
    bool is_addition = true;
    AES::read_word(in, w_in, is_addition);
    ASSERT_TRUE(compare_words(w_in, w_in_maybe));
    in.close();
    std::remove(path.c_str());
}

TEST(aes, read_block_test)
{
    std::string path = "test.txt";
    std::ofstream out(path);
    std::ifstream in(path);
    gblock b_out = {
        { 1, 2, 4, 6 },
        { 3, 5, 7, 11 },
        { 1, 3, 66, 67 },
        { 23, 5, 6, 8 }
    };
    for (uint8_t i = 0; i < b_out.size(); i++) {
        for (uint8_t j = 0; j < b_out[i].size(); j++) {
            out << b_out[i][j];
        }
    }
    out.close();

    gblock b_in(AES::COLUMNS_COUNT);
    ASSERT_EQ(AES::read_block(in, b_in), AES::BUF_FULL);
    ASSERT_TRUE(compare_blocks(b_in, b_out));
    in.close();
    std::remove(path.c_str());
}

TEST(aes, read_block_test2)
{
    std::string path = "test.txt";
    std::ofstream out(path);
    std::ifstream in(path);
    gblock b_out = {
        { 'a', 'b', 'c', 'd' },
        { 'e', 'f', 'g', 'h' },
    };
    for (uint8_t i = 0; i < b_out.size(); i++) {
        for (uint8_t j = 0; j < b_out[i].size(); j++) {
            out << b_out[i][j];
        }
    }
    out.close();

    gblock b_in_maybe = {
        { 'a', 'b', 'c', 'd' },
        { 'e', 'f', 'g', 'h' },
        { 0x80, 0, 0, 0 },
        { 0, 0, 0, 0 }
    };
    gblock b_in(AES::COLUMNS_COUNT);
    AES::READING_STATE state = AES::read_block(in, b_in);

    ASSERT_EQ(state, AES::BUF_NOT_FULL);
    ASSERT_TRUE(compare_blocks(b_in, b_out));
    in.close();
    std::remove(path.c_str());
}

TEST(aes, write_block_test)
{
    std::string path = "test.txt";
    std::ofstream out(path);
    std::ifstream in(path);
    gblock b_out = {
        { 'a', 'b', 'c', 'd' },
        { 'e', 'f', 'g', 'h' },
    };
    AES::write_block(out, b_out);
    out.close();

    gblock b_in_maybe = {
        { 'a', 'b', 'c', 'd' },
        { 'e', 'f', 'g', 'h' },
        { 0x80, 0, 0, 0 },
        { 0, 0, 0, 0 }
    };
    gblock b_in(AES::COLUMNS_COUNT);
    AES::READING_STATE state = AES::read_block(in, b_in);

    ASSERT_EQ(state, AES::BUF_NOT_FULL);
    ASSERT_TRUE(compare_blocks(b_in, b_out));
    in.close();
    std::remove(path.c_str());
}

TEST(aes, encrypt_file_test)
{
    std::string path_in = "test_in.txt";
    std::string path_out = "test_out.txt";
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    gblock b_in = {
        { 0x00, 0x11, 0x22, 0x33 },
        { 0x44, 0x55, 0x66, 0x77 },
        { 0x88, 0x99, 0xaa, 0xbb },
        { 0xcc, 0xdd, 0xee, 0xff },
    };
    gblock b_out1 = {
        { 0x69, 0xc4, 0xe0, 0xd8 },
        { 0x6a, 0x7b, 0x04, 0x30 },
        { 0xd8, 0xcd, 0xb7, 0x80 },
        { 0x70, 0xb4, 0xc5, 0x5a },
    };
    gblock b_out2 = {
        { 0x43, 0x99, 0x57, 0x2c },
        { 0xd6, 0xea, 0x53, 0x41 },
        { 0xb8, 0xd3, 0x58, 0x76 },
        { 0xa7, 0x09, 0x8a, 0xf7 },
    };

    std::ofstream out0(path_in);
    AES::write_block(out0, b_in);
    out0.close();

    std::ifstream in1(path_in);
    std::ofstream out1(path_out);
    AES::encrypt_file(in1, out1, key);
    in1.close();
    out1.close();

    gblock b(AES::COLUMNS_COUNT);
    gblock b2(AES::COLUMNS_COUNT);
    std::ifstream in2(path_out);
    AES::read_block(in2, b);
    AES::read_block(in2, b2);
    ASSERT_TRUE(compare_blocks(b, b_out1));
    ASSERT_TRUE(compare_blocks(b2, b_out2));

    std::remove(path_in.c_str());
    std::remove(path_out.c_str());
}

TEST(aes, write_block_without_indent_bytes_test)
{
    std::string path = "test.txt";
    std::ofstream out(path);
    gblock b_out = {
        { 'a', 'b', 'c', 'd' },
        { 'e', 'f', 'g', 'h' },
        { 0x80, 0, 0, 0 },
        { 0, 0, 0, 0 }
    };
    gword b_in = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h' };
    AES::write_block_without_indent_bytes(out, b_out);
    out.close();
    out.clear();

    gword b;
    int8_t buf;
    std::ifstream in(path);
    while ((buf = in.get()) != EOF) {
        b.push_back(buf);
    }

    ASSERT_EQ(8, b.size());
    ASSERT_TRUE(compare_words(b_in, b));
    std::remove(path.c_str());
}

TEST(aes, decrypt_file_test)
{
    std::string path_in = "test_in.txt";
    std::string path_out = "test_out.txt";
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    gblock b_in1 = {
        { 0x69, 0xc4, 0xe0, 0xd8 },
        { 0x6a, 0x7b, 0x04, 0x30 },
        { 0xd8, 0xcd, 0xb7, 0x80 },
        { 0x70, 0xb4, 0xc5, 0x5a },
    };
    gblock b_in2 = {
        { 0x43, 0x99, 0x57, 0x2c },
        { 0xd6, 0xea, 0x53, 0x41 },
        { 0xb8, 0xd3, 0x58, 0x76 },
        { 0xa7, 0x09, 0x8a, 0xf7 },
    };
    gblock b_out = {
        { 0x00, 0x11, 0x22, 0x33 },
        { 0x44, 0x55, 0x66, 0x77 },
        { 0x88, 0x99, 0xaa, 0xbb },
        { 0xcc, 0xdd, 0xee, 0xff },
    };

    std::ofstream out0(path_in);
    AES::write_block(out0, b_in1);
    AES::write_block(out0, b_in2);
    out0.close();

    std::ifstream in1(path_in);
    std::ofstream out1(path_out);
    AES::decrypt_file(in1, out1, key);

    std::ifstream in2(path_out);
    gblock b(AES::COLUMNS_COUNT);
    AES::read_block(in2, b);
    in2.close();

    ASSERT_TRUE(compare_blocks(b, b_out));

    std::remove(path_in.c_str());
    std::remove(path_out.c_str());
}

TEST(aes, vector_encryption_test1)
{
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    std::vector<uint8_t> b_in = { 'f', 'u', 'n', ',', 'k', 'e', 'k' };

    std::vector<uint8_t> b_out1 = AES::encrypt_data(b_in, key);

    std::vector<uint8_t> b_out2;
    AES::decrypt_data(b_out1, b_out2, key);

    ASSERT_EQ(b_in.size(), b_out2.size());
    for (uint8_t i = 0; i < b_in.size(); i++) {
        ASSERT_EQ(b_in[i], b_out2[i]);
    }
}

TEST(aes, vector_encryption_test2)
{
    gword key = {
        0x00, 0x01, 0x02, 0x03,
        0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0a, 0x0b,
        0x0c, 0x0d, 0x0e, 0x0f,
    };
    std::vector<uint8_t> b_in = { '5', '5', 'g', ';', 'q', 'v',
                                  '5', '5', 'g', ';', 'q', 'v',
                                  '5', '5', 'g', ';', 'q', 'v'};

    std::vector<uint8_t> b_out1 = AES::encrypt_data(b_in, key);

    std::vector<uint8_t> b_out2;
    AES::decrypt_data(b_out1, b_out2, key);

    ASSERT_EQ(b_in.size(), b_out2.size());
    for (uint8_t i = 0; i < b_in.size(); i++) {
        ASSERT_EQ(b_in[i], b_out2[i]);
    }
}
