#include <gtest/gtest.h>

#include "crypto/aes/galois_field.hpp"

using namespace crypto::aes;


TEST(Galois256El, substitute_test1)
{
    Galois256El el(0x6e);
    el.substitute();
    ASSERT_EQ(0x9f, el.get_number());
}


TEST(Galois256El, substitute_test2)
{
    Galois256El el(0x15);
    el.substitute();
    ASSERT_EQ(0x59, el.get_number());
}


TEST(Galois256El, substitute_test3)
{
    Galois256El el(0x00);
    el.substitute();
    ASSERT_EQ(0x63, el.get_number());
}
