#include <gtest/gtest.h>

#include "crypto/des/addition_bitblock_iter.hpp"

using namespace crypto::des;
using namespace crypto::utils;


TEST(bitblock_iter, test1)
{
    const std::vector<uint8_t> input = {3, 4};
    AdditionBitBlockIter<3> iter(input);

    auto block = iter.getCur();
    BitBlock<3 * 8> desired_block("000000110000010010000000");

    ASSERT_TRUE(block == desired_block);
    ASSERT_FALSE(iter.hasNext());
}


TEST(bitblock_iter, test2)
{
    const std::vector<uint8_t> input = {3, 4};
    AdditionBitBlockIter<1> iter(input);

    auto block1 = iter.getCur();
    BitBlock<8> desired_block1("00000011");
    ASSERT_TRUE(block1 == desired_block1);
    iter.next();

    auto block2 = iter.getCur();
    BitBlock<8> desired_block2("00000100");
    ASSERT_TRUE(block1 == desired_block1);
    iter.next();

    auto block3 = iter.getCur();
    BitBlock<8> desired_block3("10000000");
    ASSERT_TRUE(block3 == desired_block3);

    ASSERT_FALSE(iter.hasNext());
}


TEST(bitblock_iter, test3)
{
    const std::vector<uint8_t> input = {3, 4};
    AdditionBitBlockIter<2> iter(input);

    auto block1 = iter.getCur();
    BitBlock<2 * 8> desired_block1("0000001100000100");
    ASSERT_TRUE(block1 == desired_block1);
    iter.next();

    auto block2 = iter.getCur();
    BitBlock<2 * 8> desired_block2("1000000000000000");
    ASSERT_TRUE(block2 == desired_block2);
    ASSERT_FALSE(iter.hasNext());
}
