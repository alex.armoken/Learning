#include <gtest/gtest.h>

#include "crypto/utils/bit_block.hpp"

#include "crypto/des/des.hpp"

using namespace crypto::des;
using namespace crypto::utils;
using namespace crypto::utils::bit_block;


TEST(des, get_substitution_row_idx_test1)
{
    const auto result = DES::get_substitution_row_idx(
        BitBlock<DES::BIT_GROUP_SIZE>("101011")
    );
    const auto desired_result = 3;
    ASSERT_TRUE(result == desired_result);
}


TEST(des, get_substitution_column_idx_test1)
{
    const auto result = DES::get_substitution_column_idx(
        BitBlock<DES::BIT_GROUP_SIZE>("101011")
    );
    const auto desired_result = 5;
    ASSERT_TRUE(result == desired_result);
}


TEST(des, get_group_substitution_test1)
{
    const auto result = DES::get_group_substitution(
        BitBlock<DES::BIT_GROUP_SIZE>("011000"),
        0
    );
    const BitBlock<DES::SUBSTITUTION_SIZE> desired_result("0101");
    ASSERT_TRUE(result == desired_result);
}


TEST(des, get_substitution_test2)
{
    const auto result = DES::get_group_substitution(
        BitBlock<DES::BIT_GROUP_SIZE>("011011"),
        0
    );
    const BitBlock<DES::SUBSTITUTION_SIZE> desired_result("0101");
    ASSERT_TRUE(result == desired_result);
}


TEST(des, get_substitution_test3)
{
    const auto result = DES::get_group_substitution(
        BitBlock<DES::BIT_GROUP_SIZE>("100111"),
        7
    );
    const BitBlock<DES::SUBSTITUTION_SIZE> desired_result("0111");
    ASSERT_TRUE(result == desired_result);
}


TEST(des, get_substitution_test4)
{
    const auto result = DES::get_group_substitution(
        BitBlock<DES::BIT_GROUP_SIZE>("111010"),
        3
    );
    const BitBlock<DES::SUBSTITUTION_SIZE> desired_result("0010");
    ASSERT_TRUE(result == desired_result);
}


TEST(des, extend_test)
{
    auto result1 = DES::extend(
        DES::HalfBlock("00001000000000000000000000100000")
    );
    auto desired_result1 = DES::ExtendedHalfBlock(
        "000001010000000000000000000000000000000100000000"
    );
    ASSERT_TRUE(result1 == desired_result1);

    auto result2 = DES::extend(
        DES::HalfBlock("00001000010000000000000000100000")
    );
    auto desired_result2 = DES::ExtendedHalfBlock(
        "000001010000001000000000000000000000000100000000"
    );
    ASSERT_TRUE(result2 == desired_result2);

    auto result3 = DES::extend(
        DES::HalfBlock("00001000010000000000000000100001")
    );
    auto desired_result3 = DES::ExtendedHalfBlock(
        "100001010000001000000000000000000000000100000010"
    );
    ASSERT_TRUE(result3 == desired_result3);
}


TEST(des, initial_permutation_test1)
{
    const DES::Block block(
        "11000000"
        "10110111"
        "10101000"
        "11010000"
        "01011111"
        "00111010"
        "10000010"
        "10011100"
    );
    const auto result = DES::make_init_permutation(block);
    const DES::Block desired_result(
        "00011001"
        "10111010"
        "10010010"
        "00010010"
        "11001111"
        "00100110"
        "10110100"
        "01110010"
    );

    ASSERT_TRUE(result == desired_result);
}


TEST(des, initial_permutation_test2)
{
    const DES::Block block(
        "00010010"
        "00110100"
        "01010110"
        "10101011"
        "11001101"
        "00010011"
        "00100101"
        "00110110"
    );
    const auto result = DES::make_init_permutation(block);
    const DES::Block desired_result(
        "00010100"
        "10100111"
        "11010110"
        "01111000"
        "00011000"
        "11001010"
        "00011000"
        "10101101"
    );

    ASSERT_TRUE(result == desired_result);
}



TEST(des, initial_permutation_test3)
{
    const DES::Block block(
        "00000000"
        "00000010"
        "00000000"
        "00000000"
        "00000000"
        "00000000"
        "00000000"
        "00000001"
    );
    const auto result = DES::make_init_permutation(block);
    const DES::Block desired_result(
        "00000000"
        "00000000"
        "00000000"
        "10000000"
        "00000000"
        "00000000"
        "00000000"
        "00000010"
    );

    ASSERT_TRUE(result == desired_result);
}


TEST(des, inversed_initial_permutation_test1)
{
    const DES::Block block(
        "00011001"
        "10111010"
        "10010010"
        "00010010"
        "11001111"
        "00100110"
        "10110100"
        "01110010"
    );
    const auto result = DES::make_inversed_init_permutation(block);
    const DES::Block desired_result(
        "11000000"
        "10110111"
        "10101000"
        "11010000"
        "01011111"
        "00111010"
        "10000010"
        "10011100"
    );

    ASSERT_TRUE(result == desired_result);
}


TEST(des, inversed_initial_permutation_test2)
{
    const DES::Block block(
        "00010100"
        "10100111"
        "11010110"
        "01111000"
        "00011000"
        "11001010"
        "00011000"
        "10101101"
    );
    const auto result = DES::make_inversed_init_permutation(block);
    const DES::Block desired_result(
        "00010010"
        "00110100"
        "01010110"
        "10101011"
        "11001101"
        "00010011"
        "00100101"
        "00110110"
    );

    ASSERT_TRUE(result == desired_result);
}



TEST(des, inversed_initial_permutation_test3)
{
    const DES::Block block(
        "00000000"
        "00000000"
        "00000000"
        "10000000"
        "00000000"
        "00000000"
        "00000000"
        "00000010"
    );
    const auto result = DES::make_inversed_init_permutation(block);
    const DES::Block desired_result(
        "00000000"
        "00000010"
        "00000000"
        "00000000"
        "00000000"
        "00000000"
        "00000000"
        "00000001"
    );

    ASSERT_TRUE(result == desired_result);
}


TEST(des, inversed_initial_permutation_test4)
{
    const DES::Block block(
        "00001010"
        "01001100"
        "11011001"
        "10010101"
        "01000011"
        "01000010"
        "00110010"
        "00110100"
    );
    const auto result = DES::make_inversed_init_permutation(block);
    const DES::Block desired_result(
        "10000101"
        "11101000"
        "00010011"
        "01010100"
        "00001111"
        "00001010"
        "10110100"
        "00000101"
    );

    ASSERT_TRUE(result == desired_result);
}


TEST(des, generate_round_keys_test1)
{
    const std::vector<DES::RoundKey> desired_result = {
        DES::RoundKey("000110110000001011101111111111000111000001110010"),
        DES::RoundKey("011110011010111011011001110110111100100111100101"),
        DES::RoundKey("010101011111110010001010010000101100111110011001"),
        DES::RoundKey("011100101010110111010110110110110011010100011101"),
        DES::RoundKey("011111001110110000000111111010110101001110101000"),
        DES::RoundKey("011000111010010100111110010100000111101100101111"),
        DES::RoundKey("111011001000010010110111111101100001100010111100"),
        DES::RoundKey("111101111000101000111010110000010011101111111011"),
        DES::RoundKey("111000001101101111101011111011011110011110000001"),
        DES::RoundKey("101100011111001101000111101110100100011001001111"),
        DES::RoundKey("001000010101111111010011110111101101001110000110"),
        DES::RoundKey("011101010111000111110101100101000110011111101001"),
        DES::RoundKey("100101111100010111010001111110101011101001000001"),
        DES::RoundKey("010111110100001110110111111100101110011100111010"),
        DES::RoundKey("101111111001000110001101001111010011111100001010"),
        DES::RoundKey("110010110011110110001011000011100001011111110101")
    };

    const DES::SourceKey source_key(
        "00010011"
        "00110100"
        "01010111"
        "01111001"
        "10011011"
        "10111100"
        "11011111"
        "11110001"
    );
    const auto result = DES::generate_round_keys(source_key);
    ASSERT_TRUE(result == desired_result);
}


TEST(des, substract_key_test1)
{
    const DES::SubstructedKey desired_result(
        "11110000110011001010101011110101010101100110011110001111"
    );
    const DES::SourceKey source_key(
        "00010011"
        "00110100"
        "01010111"
        "01111001"
        "10011011"
        "10111100"
        "11011111"
        "11110001"
    );
    const auto result = DES::substract_key(source_key);
    ASSERT_TRUE(result == desired_result);
}


TEST(des, f_function_test1)
{
    const DES::HalfBlock block("11110000101010101111000010101010");
    const DES::RoundKey key(
        "000110110000001011101111111111000111000001110010"
    );

    const auto result = DES::f_function(block, key);
    const DES::HalfBlock desired_result(
        "00100011010010101010100110111011"
    );
    ASSERT_TRUE(result == desired_result);
}


TEST(des, encrypt_block_test1)
{
    const DES::Block block(
        "0000000100100011010001010110011110001001101010111100110111101111"
    );
    const std::vector<DES::RoundKey> round_keys = {
        DES::RoundKey("000110110000001011101111111111000111000001110010"),
        DES::RoundKey("011110011010111011011001110110111100100111100101"),
        DES::RoundKey("010101011111110010001010010000101100111110011001"),
        DES::RoundKey("011100101010110111010110110110110011010100011101"),
        DES::RoundKey("011111001110110000000111111010110101001110101000"),
        DES::RoundKey("011000111010010100111110010100000111101100101111"),
        DES::RoundKey("111011001000010010110111111101100001100010111100"),
        DES::RoundKey("111101111000101000111010110000010011101111111011"),
        DES::RoundKey("111000001101101111101011111011011110011110000001"),
        DES::RoundKey("101100011111001101000111101110100100011001001111"),
        DES::RoundKey("001000010101111111010011110111101101001110000110"),
        DES::RoundKey("011101010111000111110101100101000110011111101001"),
        DES::RoundKey("100101111100010111010001111110101011101001000001"),
        DES::RoundKey("010111110100001110110111111100101110011100111010"),
        DES::RoundKey("101111111001000110001101001111010011111100001010"),
        DES::RoundKey("110010110011110110001011000011100001011111110101")
    };
    const auto result = DES::encrypt_block(block, round_keys);
    const DES::Block desired_result(
        "1000010111101000000100110101010000001111000010101011010000000101"
    );
    ASSERT_TRUE(result == desired_result);
}


TEST(des, decrypt_block_test1)
{
    const DES::Block block(
        "1000010111101000000100110101010000001111000010101011010000000101"
    );
    const std::vector<DES::RoundKey> round_keys = {
        DES::RoundKey("000110110000001011101111111111000111000001110010"),
        DES::RoundKey("011110011010111011011001110110111100100111100101"),
        DES::RoundKey("010101011111110010001010010000101100111110011001"),
        DES::RoundKey("011100101010110111010110110110110011010100011101"),
        DES::RoundKey("011111001110110000000111111010110101001110101000"),
        DES::RoundKey("011000111010010100111110010100000111101100101111"),
        DES::RoundKey("111011001000010010110111111101100001100010111100"),
        DES::RoundKey("111101111000101000111010110000010011101111111011"),
        DES::RoundKey("111000001101101111101011111011011110011110000001"),
        DES::RoundKey("101100011111001101000111101110100100011001001111"),
        DES::RoundKey("001000010101111111010011110111101101001110000110"),
        DES::RoundKey("011101010111000111110101100101000110011111101001"),
        DES::RoundKey("100101111100010111010001111110101011101001000001"),
        DES::RoundKey("010111110100001110110111111100101110011100111010"),
        DES::RoundKey("101111111001000110001101001111010011111100001010"),
        DES::RoundKey("110010110011110110001011000011100001011111110101")
    };
    const auto result = DES::decrypt_block(block, round_keys);
    const DES::Block desired_result(
        "0000000100100011010001010110011110001001101010111100110111101111"
    );
    ASSERT_TRUE(result == desired_result);
}
