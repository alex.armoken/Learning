#include "crypto/steganography/lsb.hpp"
#include "crypto/steganography/patchwork.hpp"
#include "crypto/utils/byte_helpers.hpp"
#include <gtest/gtest.h>

using namespace crypto::steganography;
using namespace crypto::utils::byte_helpers;

TEST(ChannelSwitcher, operator_indirection_test1)
{
    ChannelSwitcher ch(RGBA_CHANNEL::RED);
    ++ch;
    ASSERT_EQ(RGBA_CHANNEL::GREEN, *ch);
}

TEST(ChannelSwitcher, operator_indirection_test2)
{
    ChannelSwitcher ch(RGBA_CHANNEL::GREEN);
    ASSERT_EQ(false, ++ch);
    ASSERT_EQ(RGBA_CHANNEL::BLUE, *ch);
}

TEST(ChannelSwitcher, operator_indirection_test3)
{
    ChannelSwitcher ch(RGBA_CHANNEL::BLUE);
    ASSERT_EQ(true, ++ch);
    ASSERT_EQ(RGBA_CHANNEL::RED, *ch);
}

TEST(ChannelSwitcher, operator_indirection_test4)
{
    ChannelSwitcher ch(RGBA_CHANNEL::BLUE, true);
    ASSERT_EQ(false, ++ch);
    ASSERT_EQ(RGBA_CHANNEL::ALPHA, *ch);
}

TEST(ChannelSwitcher, operator_indirection_test5)
{
    ChannelSwitcher ch(RGBA_CHANNEL::ALPHA, true);
    ASSERT_EQ(true, ++ch);
    ASSERT_EQ(RGBA_CHANNEL::RED, *ch);
}

TEST(CurImagePos, increment_pos1)
{
    CurImagePos pos(2, 2, 3);
    for (uint8_t i = 0; i < 1 * 3; i++) {
        pos++;
    }
    ASSERT_EQ(pos.getb(), 0);
    ASSERT_EQ(pos.getx(), 0);
    ASSERT_EQ(pos.gety(), 0);
    ASSERT_EQ(pos.getc(), to_underlying(RGBA_CHANNEL::GREEN));
}

TEST(CurImagePos, increment_pos2)
{
    CurImagePos pos(2, 2, 3);
    for (uint8_t i = 0; i < 7 * 3; i++) {
        pos++;
    }
    ASSERT_EQ(pos.getb(), 0);
    ASSERT_EQ(pos.getx(), 0);
    ASSERT_EQ(pos.gety(), 1);
    ASSERT_EQ(pos.getc(), to_underlying(RGBA_CHANNEL::GREEN));
}

TEST(CurImagePos, increment_pos3)
{
    CurImagePos pos(1, 3, 1);
    for (uint8_t i = 0; i < 1 * 3; i++) {
        pos++;
    }
    ASSERT_EQ(pos.getb(), 0);
    ASSERT_EQ(pos.getx(), 0);
    ASSERT_EQ(pos.gety(), 1);
    ASSERT_EQ(pos.getc(), to_underlying(RGBA_CHANNEL::RED));
}

TEST(CurImagePos, increment_pos4)
{
    CurImagePos pos(1, 1, 3);
    for (uint8_t i = 0; i < 2; i++) {
        pos++;
    }
    ASSERT_EQ(pos.getb(), 2);
    ASSERT_EQ(pos.getx(), 0);
    ASSERT_EQ(pos.gety(), 0);
    ASSERT_EQ(pos.getc(), to_underlying(RGBA_CHANNEL::RED));
}

TEST(CurImagePos, increment_pos5)
{
    CurImagePos pos(1, 2, 1);
    for (uint8_t i = 0; i < 3; i++) {
        pos++;
    }
    ASSERT_EQ(pos.getb(), 0);
    ASSERT_EQ(pos.getx(), 0);
    ASSERT_EQ(pos.gety(), 1);
    ASSERT_EQ(pos.getc(), to_underlying(RGBA_CHANNEL::RED));
}

TEST(CurImagePos, increment_pos6)
{
    CurImagePos pos(1, 1, 1);
    for (uint8_t i = 0; i < 2; i++) {
        pos++;
    }
    ASSERT_EQ(true, ++pos);
    ASSERT_EQ(pos.getb(), 0);
    ASSERT_EQ(pos.getx(), 0);
    ASSERT_EQ(pos.gety(), 1);
    ASSERT_EQ(pos.getc(), to_underlying(RGBA_CHANNEL::RED));
}


TEST(CurImagePos, increment_pos7)
{
    CurImagePos pos(5, 5, 3);
    for (uint8_t i = 0; i < 3 * 3 * 5 * 5 - 1; i++) {
        pos++;
    }
    ASSERT_EQ(pos.getb(), 2);
    ASSERT_EQ(pos.getx(), 4);
    ASSERT_EQ(pos.gety(), 4);
    ASSERT_EQ(pos.getc(), to_underlying(RGBA_CHANNEL::BLUE));
    ASSERT_EQ(true, pos++);
    ASSERT_EQ(pos.getb(), 0);
    ASSERT_EQ(pos.getx(), 0);
    ASSERT_EQ(pos.gety(), 5);
    ASSERT_EQ(pos.getc(), to_underlying(RGBA_CHANNEL::RED));
}

TEST(CurImagePos, increment_pos8)
{
    CurImagePos pos(5, 5, 3);
    for (uint8_t i = 0; i < (3 * 3 * 5 * 2) + (3 * 3 * 3) + 2; i++) {
        pos++;
    }
    ASSERT_EQ(pos.getb(), 2);
    ASSERT_EQ(pos.getx(), 3);
    ASSERT_EQ(pos.gety(), 2);
    ASSERT_EQ(pos.getc(), to_underlying(RGBA_CHANNEL::RED));
}

TEST(lsb, bytes_to_int_test1)
{
    std::vector<uint8_t> bytes = {0, 0, 0, 0b11001};
    ASSERT_EQ(25, bytes_to_int(bytes));
}

TEST(lsb, bytes_to_int_test2)
{
    std::vector<uint8_t> bytes = {0, 0, 0b10011, 0b10001000};
    ASSERT_EQ(5000, bytes_to_int(bytes));
}

TEST(lsb, bytes_to_int_test3)
{
    std::vector<uint8_t> bytes = {0, 0b1, 0b10000110, 0b10100000};
    ASSERT_EQ(100000, bytes_to_int(bytes));
}

TEST(lsb, bytes_to_int_test4)
{
    std::vector<uint8_t> bytes = {0b101, 0b11110101, 0b11100001, 0b00000000};
    ASSERT_EQ(100000000, bytes_to_int(bytes));
}

TEST(lsb, bytes_to_int_test5)
{
    std::vector<uint8_t> var_out = {0b01011110, 0b01011111,
                                    0b00011110, 0b11111111};
    auto word = bytes_to_int(var_out);
    ASSERT_EQ(1583292159, word);
}
