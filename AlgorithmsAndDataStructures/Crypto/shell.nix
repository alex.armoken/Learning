{ pkgs ? import <nixpkgs>{} }:
with import <nixpkgs>{};
let
  debugBoost = enableDebugging pkgs.boost;
in pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    cimg
    clang-tools
    cmake
    debugBoost
    fish
    gdb
    gmp
    gtest
    libjpeg
    libpng
    libtiff
    lldb_12
    mono # For cpptools (debug)
    opencv
    zlib
  ];

  inputsFrom = with pkgs; [
    debugBoost
  ];

  runScript = "fish";
}
