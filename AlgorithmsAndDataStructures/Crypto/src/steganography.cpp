#include "crypto/steganography/lsb.hpp"
#include "crypto/steganography/patchwork.hpp"
#include "crypto/utils/byte_helpers.hpp"

#include <gtest/gtest.h>
#include "boost/gil/extension/io/png.hpp"
#include "boost/gil/extension/io/jpeg.hpp"
#include "boost/gil/extension/io/tiff.hpp"

#include <optional>
#include <vector>
#include <iostream>
#include <stdint.h>
#include <getopt.h>
#include <functional>
#include <filesystem>

using namespace crypto::steganography;
using namespace crypto::utils::byte_helpers;

struct global_args_t {
    bool is_check;
    bool is_patchwork;

    char *key;
    char *input_filename;
    char *output_filename;

    bool is_get_msg;
    uint8_t delta;
    uint32_t iterations;
    uint8_t bits_count;
} global_args;

static struct option OPTS[] = {{"input", required_argument, NULL, 'i'},
                               {"output", required_argument, NULL, 'o'},
                               {"bits", required_argument, NULL, 'b'},
                               {"get_msg", no_argument, NULL, 'g'},
                               {"patchwork", no_argument, NULL, 'p'},
                               {"check", no_argument, NULL, 'c'},
                               {"key", required_argument, NULL, 'k'},
                               {"delta", required_argument, NULL, 'd'},
                               {"iterations", required_argument, NULL, 'n'},
                               {"run_tests", no_argument, NULL, 't'},
                               {0, 0, 0, 0}};

int parse_arguments(int argc, char *argv[])
{
    int c;
    int opt_idx;
    global_args.delta = 0;
    global_args.iterations = 0;
    global_args.is_check = false;
    global_args.is_get_msg = false;
    global_args.is_patchwork = false;

    const char *args = "pk:tgi:o:b:n:d:c";
    while ((c = getopt_long(argc, argv, args, OPTS, &opt_idx)) != -1) {
        switch (c) {
        case 'k':
            global_args.key = optarg;
            break;
        case 'p':
            global_args.is_patchwork = true;
            break;
        case 'c':
            global_args.is_check = true;
            break;
        case 'i':
            global_args.input_filename = optarg;
            break;
        case 'o':
            global_args.output_filename = optarg;
            break;
        case 'b':
            global_args.bits_count = atoi(optarg);
            break;
        case 'd':
            global_args.delta = atoi(optarg);
            break;
        case 'n':
            global_args.iterations = atoi(optarg);
            break;
        case 'g':
            global_args.is_get_msg = true;
            break;
        default:
            return -1;
        }
    }
    return 0;
}

void msg_input(std::vector<uint8_t>& msg)
{
    std::string str_input;
    std::cin >> str_input;

    std::vector<uint8_t> length_bytes = int_to_bytes(str_input.length() + 2);
    std::cout << str_input.length() << std::endl;
    std::cout << bytes_to_int(length_bytes) << std::endl;

    for (auto it = length_bytes.begin(); it != length_bytes.end(); it++) {
        msg.push_back(*it);
    }

    msg.push_back('/');
    for (auto it = str_input.begin(); it != str_input.end(); it++) {
        msg.push_back(*it);
    }
    msg.push_back('/');
}

bool check_args_logic()
{
    if (!global_args.is_patchwork && !global_args.is_get_msg
               && (global_args.input_filename == NULL
                   || global_args.output_filename == NULL)) {
        std::cout << "1" << "\n";
        return false;
    } else if (!global_args.is_patchwork && (global_args.bits_count == 0
                                             || global_args.bits_count > 8)) {
        std::cout << "2" << "\n";
        return false;
    } else if (global_args.is_patchwork && !global_args.is_check
               && (global_args.input_filename == NULL
                   || global_args.output_filename == NULL
                   || global_args.key == NULL || global_args.delta == 0
                   || global_args.iterations == 0)) {
        std::cout << "3" << "\n";
        return false;
    } else if (global_args.is_patchwork && global_args.is_check
               && (global_args.input_filename == NULL
                   || global_args.key == NULL || global_args.delta == 0
                   || global_args.iterations == 0)) {
        std::cout << "4" << "\n";
        return false;
    }
    return true;
}

std::optional<boost::gil::rgb8_image_t> read_image(
    const std::filesystem::path& path
)
{
    boost::gil::rgb8_image_t img;
    const auto extension = path.extension();
    if (extension == ".jpeg" || extension == ".jpg") {
        boost::gil::read_image(path.c_str(), img, boost::gil::jpeg_tag());
        return img;
    } else if (extension == ".png") {
        boost::gil::read_image(path.c_str(), img, boost::gil::png_tag());
        return img;
    } else if (extension == ".tiff") {
        boost::gil::read_image(path.c_str(), img, boost::gil::tiff_tag());
        return img;
    }

    std::cout << "Can' load image at path '" << path.c_str()
              << "'!" << std::endl;
    return std::nullopt;
}

void write_image(const std::filesystem::path& path,
                 boost::gil::rgb8_image_t& img)
{
    const auto extension = path.extension();
    if (extension == ".jpeg" || extension == ".jpg") {
        boost::gil::write_view(path.c_str(), boost::gil::view(img), boost::gil::jpeg_tag());
    } else if (extension == ".png") {
        boost::gil::write_view(path.c_str(), boost::gil::view(img), boost::gil::png_tag());
    } else if (extension == ".tiff") {
        boost::gil::write_view(path.c_str(), boost::gil::view(img), boost::gil::tiff_tag());
    }

    std::cout << "Can' save image at path '" << path.c_str()
              << "'!" << std::endl;
}

int lsb_program()
{
    std::vector<uint8_t> data;
    if (global_args.is_get_msg) {
        const auto maybe_image = read_image(global_args.input_filename);
        if (!maybe_image) {
            return 1;
        }

        LSB::extract_data_from_img(*maybe_image, global_args.bits_count);

        for (uint64_t i = 0; i < data.size(); i++) {
            std::cout << data[i];
        }
        std::cout << std::endl;
    } else {
        std::cout << "Please input message: ";
        msg_input(data);

        auto maybe_image = read_image(global_args.input_filename);
        if (!maybe_image) {
            return 2;
        }

        LSB::hide_data_into_img(data, *maybe_image, global_args.bits_count);
        write_image(global_args.output_filename, *maybe_image);
    }
    return 0;
}

int patchwork_program()
{
    auto key = std::hash<std::string>()(global_args.key);
    std::cout << "PATCHWORK!" << "\n";
    std::string in_filename(global_args.input_filename);

    if (global_args.is_check) {
        Patchwork::test(in_filename, key, global_args.delta,
                        global_args.iterations);
    } else {
        std::cout << "READING!" << "\n";
        std::string out_filename(global_args.output_filename);
        Patchwork::hide(in_filename, out_filename, key, global_args.delta,
                        global_args.iterations);
    }
    return 0;
}

int main_program()
{
    if (global_args.is_patchwork) {
        return patchwork_program();
    }

    return lsb_program();
}

int main(int argc, char* argv[])
{
    if (parse_arguments(argc, argv) == -1) {
        return 1;
    } else if (!check_args_logic()) {
        return 1;
    }
    return main_program();
}
