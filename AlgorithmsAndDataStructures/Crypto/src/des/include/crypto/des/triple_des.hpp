#include <stdint.h>
#include <algorithm>
#include <iomanip>
#include <array>
#include <iostream>
#include <array>
#include <vector>
#include <bitset>
#include <string>

#include <optional>

#include "crypto/utils/bit_block.hpp"
#include "crypto/utils/constants.hpp"
#include "crypto/utils/bit_block.hpp"

#include "des.hpp"


#if !(defined(CRYPTO_TRIPLE_DES_H))
#define CRYPTO_TRIPLE_DES_H
namespace crypto
{
    using namespace utils;
    using namespace utils::bit_block;

    namespace des
    {
        class TripleDES
        {
        public:
            TripleDES() = delete;

            static const auto KEYS_COUNT = 3;
            static const auto KEY_SIZE = DES::KEY_SIZE * KEYS_COUNT;
            using SourceKey = std::array<DES::SourceKey, KEYS_COUNT>;
            using RoundKeyGroup = std::vector<DES::RoundKey>;
            using GroupOfRoundKeyGroups = std::array<RoundKeyGroup, KEYS_COUNT>;


            static std::optional<SourceKey> convert_key(
                const unsigned char* data,
                std::size_t length
            );
            static std::optional<SourceKey> convert_key(const std::string& key);
            static std::optional<SourceKey> convert_key(const std::vector<uint8_t>& key);


            static GroupOfRoundKeyGroups generate_round_keys(
                const SourceKey& key
            );


            static DES::Block encrypt_block(
                const DES::Block& data,
                const GroupOfRoundKeyGroups& round_keys
            );
            static std::vector<uint8_t> encrypt_data(
                const std::vector<uint8_t> &data,
                const SourceKey& key
            );
            static DES::Block decrypt_block(
                const DES::Block& data,
                const GroupOfRoundKeyGroups& round_keys
            );
            static std::vector<uint8_t> decrypt_data(
                const std::vector<uint8_t> &data,
                const SourceKey& key
            );
        };
    }
}
#endif // CRYPTO_TRIPLE_DES_H
