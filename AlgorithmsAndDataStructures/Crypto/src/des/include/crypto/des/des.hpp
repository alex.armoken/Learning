#include <stdint.h>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <array>
#include <vector>
#include <bitset>
#include <string>

#include <optional>

#include "crypto/utils/bit_block.hpp"
#include "crypto/utils/constants.hpp"
#include "crypto/utils/bit_block.hpp"


#if !(defined(CRYPTO_DES_H))
#define CRYPTO_DES_H
namespace crypto
{
    using namespace utils;
    using namespace utils::bit_block;

    namespace des
    {
        class DES
        {
        public:
            DES() = delete;

            static const auto ROUNDS_COUNT = 16;

            static const auto KEY_SIZE = 8; // Bytes
            static const auto ROUND_KEY_SIZE = 6; // Bytes
            static const auto SUBSTRACTED_KEY_SIZE = 7; // Bytes

            static const auto BLOCK_SIZE = 8; // Bytes
            static const auto HALF_BLOCK_SIZE = BLOCK_SIZE / 2; // Bytes
            static const auto EXTENDED_HALF_BLOCK_SIZE = 6; // Bytes

            static const auto BIT_GROUP_SIZE = 6; // Bits
            static const auto SUBSTITUTION_SIZE = 4; // Bits


            using Block = BitBlock<DES::BLOCK_SIZE * BYTE_SIZE>;
            using HalfBlock = BitBlock<HALF_BLOCK_SIZE * BYTE_SIZE>;
            using ExtendedHalfBlock = BitBlock<EXTENDED_HALF_BLOCK_SIZE * BYTE_SIZE>;

            using SourceKey = BitBlock<KEY_SIZE * BYTE_SIZE>;
            using RoundKey = BitBlock<ROUND_KEY_SIZE * BYTE_SIZE>;
            using SubstructedKey = BitBlock<SUBSTRACTED_KEY_SIZE * BYTE_SIZE>;
            using HalfSubstructedKey = BitBlock<SUBSTRACTED_KEY_SIZE * BYTE_SIZE /  2>;


            static SubstructedKey substract_key(const SourceKey& key);
            static RoundKey generate_round_key(
                HalfSubstructedKey& c_part,
                HalfSubstructedKey& d_part,
                uint8_t round_num
            );
            static std::vector<RoundKey> generate_round_keys(const SourceKey& key);


            static std::optional<SourceKey> convert_key(
                const unsigned char* data,
                std::size_t length
            );
            static std::optional<SourceKey> convert_key(const std::string& key);
            static std::optional<SourceKey> convert_key(const std::vector<uint8_t>& key);


            static Block make_init_permutation(const Block& data);
            static Block make_inversed_init_permutation(const Block& data);


            static BitBlock<BIT_GROUP_SIZE> get_bit_group(
                const ExtendedHalfBlock& data,
                uint8_t group_num
            );
            static BitBlock<SUBSTITUTION_SIZE> get_group_substitution(
                const BitBlock<BIT_GROUP_SIZE> group,
                uint8_t group_num
            );
            static uint8_t get_substitution_row_idx(
                const BitBlock<DES::BIT_GROUP_SIZE> group
            );
            static uint8_t get_substitution_column_idx(
                const BitBlock<DES::BIT_GROUP_SIZE> group
            );
            static HalfBlock make_block_substitution(
                const ExtendedHalfBlock& block
            );
            static ExtendedHalfBlock extend(const HalfBlock& data);
            static HalfBlock f_function(const HalfBlock& data, const RoundKey& key);


            static Block encrypt_block(
                const Block& data,
                const std::vector<RoundKey>& round_keys
            );
            static std::vector<uint8_t> encrypt_data(
                const std::vector<uint8_t> &data,
                const SourceKey& key
            );

            static Block decrypt_block(
                const Block& data,
                const std::vector<RoundKey>& round_keys
            );
            static std::vector<uint8_t> decrypt_data(
                const std::vector<uint8_t> &data,
                const SourceKey& key
            );
        };
    }
}
#endif // CRYPTO_DES_H
