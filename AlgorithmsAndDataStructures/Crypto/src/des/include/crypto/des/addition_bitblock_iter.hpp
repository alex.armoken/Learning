#include <vector>
#include <stdint.h>

#include "crypto/utils/bit_block.hpp"
#include "crypto/utils/constants.hpp"
#include "crypto/utils/bit_block_helpers.hpp"


#if !(defined(ADDITION_BITBLOCK_ITER_DES_H))
#define ADDITION_BITBLOCK_ITER_DES_H
namespace crypto
{
    namespace des
    {
        using namespace crypto::utils;
        using namespace crypto::utils::bit_block;

        /*
         * Iterator used to hide addition
         */
        template<std::size_t BYTES_COUNT>
        class AdditionBitBlockIter
        {
        public:
            AdditionBitBlockIter(const std::vector<uint8_t>& data)
                : _data(data)
            {
                _cur_it = _data.cbegin();

                next();
            }

            void next()
            {
                _buffer = loadBlock();
            }

            bool hasNext()
            {
                return not _was_addition;
            }

            BitBlock<BYTES_COUNT * BYTE_SIZE> getCur()
            {
                return _buffer;
            }

        private:
            const std::vector<uint8_t>& _data;
            std::vector<uint8_t>::const_iterator _cur_it;

            bool _was_addition = false;
            BitBlock<BYTES_COUNT * BYTE_SIZE> _buffer = 0;


            BitBlock<BYTES_COUNT * BYTE_SIZE> loadBlock()
            {
                BitBlock<BYTES_COUNT * BYTE_SIZE> result(0);

                for (auto byte_idx = 0; byte_idx < BYTES_COUNT; ++byte_idx)
                {
                    for (auto bit_idx = 0; bit_idx < BYTE_SIZE; ++bit_idx)
                    {
                        const auto block_bit_idx = byte_idx * BYTE_SIZE + bit_idx;
                        if (_cur_it == _data.cend())
                        {
                            result[block_bit_idx] = true;
                            _was_addition = true;
                            goto exit;
                        }
                        else
                        {
                            const auto bit_value = get_bit_value(
                                *_cur_it,
                                calc_reversed_bit_idx(bit_idx)
                            );
                            result[block_bit_idx] = bit_value;
                        }
                    }
                    ++_cur_it;
                }

            exit:
                return result;
            }
        };
    }
}
#endif // ADDITION_BITBLOCK_ITER_DES_H
