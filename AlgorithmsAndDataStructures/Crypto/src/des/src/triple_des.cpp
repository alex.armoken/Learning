#include "crypto/des/triple_des.hpp"

#include <bitset>
#include <tuple>

#include "crypto/utils/bit_block_helpers.hpp"

#include "crypto/des/des.hpp"
#include "crypto/des/bitblock_iter.hpp"
#include "crypto/des/addition_bitblock_iter.hpp"


namespace crypto
{
    namespace des
    {
        using namespace crypto::utils;
        using namespace crypto::utils::bit_block;

        std::optional<TripleDES::SourceKey> TripleDES::convert_key(
            const unsigned char* const data,
            std::size_t length
        )
        {
            if (length != DES::KEY_SIZE
                && length != DES::KEY_SIZE * 2
                && length != TripleDES::KEY_SIZE) {
                return std::optional<SourceKey>();
            }

            TripleDES::SourceKey result;
            if (length == DES::KEY_SIZE) {
                for (auto i = 0; i < TripleDES::KEYS_COUNT; ++i) {
                    result[i] = *DES::convert_key(data, DES::KEY_SIZE);
                }
            } else if (length == DES::KEY_SIZE * 2) {
                result[0] = *DES::convert_key(data, DES::KEY_SIZE);
                result[1] = *DES::convert_key(data + DES::KEY_SIZE, DES::KEY_SIZE);
                result[2] = *DES::convert_key(data, DES::KEY_SIZE);
            } else {
                for (auto i = 0; i < TripleDES::KEYS_COUNT; ++i) {
                    result[i] = *DES::convert_key(
                        data + i * DES::KEY_SIZE,
                        DES::KEY_SIZE
                    );
                }
            }

            return result;
        }


        std::optional<TripleDES::SourceKey> TripleDES::convert_key(
            const std::vector<uint8_t>& key
        )
        {
            return convert_key(key.data(), key.size());
        }


        std::optional<TripleDES::SourceKey> TripleDES::convert_key(
            const std::string& key
        )
        {
            return convert_key(
                reinterpret_cast<const unsigned char*>(key.c_str()),
                key.length()
            );
        }


        TripleDES::GroupOfRoundKeyGroups TripleDES::generate_round_keys(
            const SourceKey& key
        )
        {
            GroupOfRoundKeyGroups result;
            for (auto i = 0; i < TripleDES::KEYS_COUNT; ++i) {
                result[i] = DES::generate_round_keys(key[i]);
            }

            return result;
        }


        DES::Block TripleDES::encrypt_block(
            const DES::Block& data,
            const GroupOfRoundKeyGroups& round_keys
        )
        {
            return DES::encrypt_block(
                DES::encrypt_block(
                    DES::encrypt_block(data, round_keys[0]),
                    round_keys[1]
                ),
                round_keys[2]
            );
        }

        std::vector<uint8_t> TripleDES::encrypt_data(
            const std::vector<uint8_t>& data,
            const SourceKey& key
        )
        {
            AdditionBitBlockIter<DES::BLOCK_SIZE> block_iter(data);
            const auto round_keys = generate_round_keys(key);

            std::vector<uint8_t> encrypted_data;
            encrypted_data.reserve(data.size() + DES::BLOCK_SIZE); // For additional block

            do {
                const auto data_block = block_iter.getCur();
                const auto encrypted_block = encrypt_block(data_block, round_keys);
                append_to_arr_of_bytes<DES::BLOCK_SIZE>(
                    encrypted_block,
                    std::back_inserter(encrypted_data)
                );

                if (block_iter.hasNext()) {
                    block_iter.next();
                } else {
                    break;
                }
            } while (true);

            return encrypted_data;
        }


        DES::Block TripleDES::decrypt_block(
            const DES::Block& data,
            const GroupOfRoundKeyGroups& round_keys
        )
        {
            return DES::decrypt_block(
                DES::decrypt_block(
                    DES::decrypt_block(data, round_keys[2]),
                    round_keys[1]
                ),
                round_keys[0]
            );
        }


        std::vector<uint8_t> TripleDES::decrypt_data(
            const std::vector<uint8_t>& data,
            const TripleDES::SourceKey& key
        )
        {
            BitBlockIter<DES::BLOCK_SIZE> block_iter(data);
            const auto round_keys = generate_round_keys(key);

            std::vector<uint8_t> decrypted_data;
            decrypted_data.reserve(data.size() + DES::BLOCK_SIZE); // For additional block

            do {
                const auto data_block = block_iter.getCur();
                const auto decrypted_block = decrypt_block(data_block, round_keys);

                if (block_iter.hasNext()) {
                    append_to_arr_of_bytes<DES::BLOCK_SIZE>(
                        decrypted_block,
                        std::back_inserter(decrypted_data)
                    );

                    block_iter.next();
                } else {
                    append_last_block_to_arr_of_bytes<DES::BLOCK_SIZE>(
                        decrypted_block,
                        std::back_inserter(decrypted_data)
                    );

                    break;
                }
            } while (true);

            return decrypted_data;
        }
    }
}
