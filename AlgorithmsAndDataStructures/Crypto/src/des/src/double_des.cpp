#include "crypto/des/double_des.hpp"

#include <bitset>
#include <tuple>

#include "crypto/utils/bit_block_helpers.hpp"

#include "crypto/des/des.hpp"
#include "crypto/des/addition_bitblock_iter.hpp"
#include "crypto/des/bitblock_iter.hpp"


namespace crypto
{
    namespace des
    {
        using namespace crypto::utils;
        using namespace crypto::utils::bit_block;

        std::optional<DoubleDES::SourceKey> DoubleDES::convert_key(
            const unsigned char* const data,
            std::size_t length
        )
        {
            if (length != DoubleDES::KEY_SIZE) {
                return std::optional<SourceKey>();
            }

            DoubleDES::SourceKey result;
            result[0] = *DES::convert_key(data, DES::KEY_SIZE);
            result[1] = *DES::convert_key(data + DES::KEY_SIZE, DES::KEY_SIZE);

            return result;
        }


        std::optional<DoubleDES::SourceKey> DoubleDES::convert_key(
            const std::vector<uint8_t>& key
        )
        {
            return convert_key(key.data(), key.size());
        }


        std::optional<DoubleDES::SourceKey> DoubleDES::convert_key(
            const std::string& key
        )
        {
            return convert_key(
                reinterpret_cast<const unsigned char*>(key.c_str()),
                key.length()
            );
        }


        DoubleDES::GroupOfRoundKeyGroups DoubleDES::generate_round_keys(
            const SourceKey& key
        )
        {
            GroupOfRoundKeyGroups result;
            result[0] = DES::generate_round_keys(key[0]);
            result[1] = DES::generate_round_keys(key[1]);

            return result;
        }


        DES::Block DoubleDES::encrypt_block(
            const DES::Block& data,
            const GroupOfRoundKeyGroups& round_keys
        )
        {
            return DES::encrypt_block(
                DES::encrypt_block(data, round_keys[0]),
                round_keys[1]
            );
        }


        std::vector<uint8_t> DoubleDES::encrypt_data(
            const std::vector<uint8_t>& data,
            const SourceKey& key
        )
        {
            AdditionBitBlockIter<DES::BLOCK_SIZE> block_iter(data);
            const auto round_keys = generate_round_keys(key);

            std::vector<uint8_t> encrypted_data;
            encrypted_data.reserve(data.size() + DES::BLOCK_SIZE); // For additional block

            do {
                const auto data_block = block_iter.getCur();
                const auto encrypted_block = encrypt_block(data_block, round_keys);
                append_to_arr_of_bytes<DES::BLOCK_SIZE>(
                    encrypted_block,
                    std::back_inserter(encrypted_data)
                );

                if (block_iter.hasNext()) {
                    block_iter.next();
                } else {
                    break;
                }
            } while (true);

            return encrypted_data;
        }


        DES::Block DoubleDES::decrypt_block(
            const DES::Block& data,
            const GroupOfRoundKeyGroups& round_keys
        )
        {
            return DES::decrypt_block(
                DES::decrypt_block(data, round_keys[1]),
                round_keys[0]
            );
        }


        std::vector<uint8_t> DoubleDES::decrypt_data(
            const std::vector<uint8_t>& data,
            const DoubleDES::SourceKey& key
        )
        {
            BitBlockIter<DES::BLOCK_SIZE> block_iter(data);
            const auto round_keys = generate_round_keys(key);

            std::vector<uint8_t> decrypted_data;
            decrypted_data.reserve(data.size() + DES::BLOCK_SIZE); // For additional block

            do {
                const auto data_block = block_iter.getCur();
                const auto decrypted_block = decrypt_block(data_block, round_keys);

                if (block_iter.hasNext()) {
                    append_to_arr_of_bytes<DES::BLOCK_SIZE>(
                        decrypted_block,
                        std::back_inserter(decrypted_data)
                    );

                    block_iter.next();
                } else {
                    append_last_block_to_arr_of_bytes<DES::BLOCK_SIZE>(
                        decrypted_block,
                        std::back_inserter(decrypted_data)
                    );

                    break;
                }
            } while (true);

            return decrypted_data;
        }
    }
}
