#include "crypto/des/des.hpp"

#include <bitset>
#include <tuple>

#include "crypto/utils/bit_block_helpers.hpp"

#include "crypto/des/bitblock_iter.hpp"
#include "crypto/des/addition_bitblock_iter.hpp"


namespace crypto
{
    namespace des
    {
        using namespace crypto::utils;
        using namespace crypto::utils::bit_block;

        const std::vector<uint8_t> INITIAL_PERMUTATION = {
            58, 50, 42, 34, 26, 18, 10, 2,
            60, 52, 44, 36, 28, 20, 12, 4,
            62, 54, 46, 38, 30, 22, 14, 6,
            64, 56, 48, 40, 32, 24, 16, 8,
            57, 49, 41, 33, 25, 17,  9, 1,
            59, 51, 43, 35, 27, 19, 11, 3,
            61, 53, 45, 37, 29, 21, 13, 5,
            63, 55, 47, 39, 31, 23, 15, 7
        };

        const std::vector<uint8_t> INVERSED_INITIAL_PERMUTATION = {
            40, 8, 48, 16, 56, 24, 64, 32,
            39, 7, 47, 15, 55, 23, 63, 31,
            38, 6, 46, 14, 54, 22, 62, 30,
            37, 5, 45, 13, 53, 21, 61, 29,
            36, 4, 44, 12, 52, 20, 60, 28,
            35, 3, 43, 11, 51, 19, 59, 27,
            34, 2, 42, 10, 50, 18, 58, 26,
            33, 1, 41,  9, 49, 17, 57, 25
        };

        const std::vector<uint8_t> S_BOXES = {
            // S1
            14,  4, 13,  1,  2, 15, 11,  8,  3, 10,  6, 12,  5,  9,  0,  7,
             0, 15,  7,  4, 14,  2, 13,  1, 10,  6, 12, 11,  9,  5,  3,  8,
             4,  1, 14,  8, 13,  6,  2, 11, 15, 12,  9,  7,  3, 10,  5,  0,
            15, 12,  8,  2,  4,  9,  1,  7,  5, 11,  3, 14, 10,  0,  6, 13,

            // S2
            15,  1,  8, 14,  6, 11,  3,  4,  9,  7,  2, 13, 12,  0,  5, 10,
            3,  13,  4,  7, 15,  2,  8, 14, 12,  0,  1, 10,  6,  9, 11,  5,
            0,  14,  7, 11, 10,  4, 13,  1,  5,  8, 12,  6,  9,  3,  2, 15,
            13,  8, 10,  1,  3, 15,  4,  2, 11,  6,  7, 12,  0,  5, 14,  9,

            // S3
            10,  0,  9, 14,  6,  3, 15,  5,  1, 13, 12,  7, 11,  4,  2,  8,
            13,  7,  0,  9,  3,  4,  6, 10,  2,  8,  5, 14, 12, 11, 15,  1,
            13,  6,  4,  9,  8, 15,  3,  0, 11,  1,  2, 12,  5, 10, 14,  7,
             1, 10, 13,  0,  6,  9,  8,  7,  4, 15, 14,  3, 11,  5,  2, 12,

            // S4
             7, 13, 14,  3,  0,  6,  9, 10,  1,  2,  8,  5, 11, 12,  4, 15,
            13,  8, 11,  5,  6, 15,  0,  3,  4,  7,  2, 12,  1, 10, 14,  9,
            10,  6,  9,  0, 12, 11,  7, 13, 15,  1,  3, 14,  5,  2,  8,  4,
             3, 15,  0,  6, 10,  1, 13,  8,  9,  4,  5, 11, 12,  7,  2, 14,

            // S5
            2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
            14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
            4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
            11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3,

            // S6
            12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
            10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
            9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
            4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13,

            // S7
            4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
            13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
            1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
            6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12,

            // S8
            13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
            1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
            7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
            2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11
        };

        const std::vector<uint8_t> LAST_PERMUTATION = {
            16,  7, 20, 21,
            29, 12, 28, 17,
             1, 15, 23, 26,
             5, 18, 31, 10,
             2,  8, 24, 14,
            32, 27,  3,  9,
            19, 13, 30,  6,
            22, 11,  4, 25
        };

        const std::vector<uint8_t> E_BIT_SELECTION_TABLE = {
            32,  1,  2,  3,  4,  5,
             4,  5,  6,  7,  8,  9,
             8,  9, 10, 11, 12, 13,
            12, 13, 14, 15, 16, 17,
            16, 17, 18, 19, 20, 21,
            20, 21, 22, 23, 24, 25,
            24, 25, 26, 27, 28, 29,
            28, 29, 30, 31, 32,  1
        };

        const std::vector<uint8_t> F_PERMUTATION = {
            16,  7, 20, 21,
            29, 12, 28, 17,
             1, 15, 23, 26,
             5, 18, 31, 10,
             2,  8, 24, 14,
            32, 27,  3,  9,
            19, 13, 30,  6,
            22, 11,  4, 25
        };

        const std::vector<uint8_t> ITER_NUM_LEFT_SHIFTS_COUNT_MAP = {
            1,
            1,
            2,
            2,
            2,
            2,
            2,
            2,
            1,
            2,
            2,
            2,
            2,
            2,
            2,
            1
        };

        const std::vector<uint8_t> PERMUTED_CHOICE_1 = {
            57, 49, 41, 33, 25, 17,  9,
             1, 58, 50, 42, 34, 26, 18,
            10,  2, 59, 51, 43, 35, 27,
            19, 11,  3, 60, 52, 44, 36,

            63, 55, 47, 39, 31, 23, 15,
             7, 62, 54, 46, 38, 30, 22,
            14,  6, 61, 53, 45, 37, 29,
            21, 13,  5, 28, 20, 12,  4
        };

        const std::vector<uint8_t> PERMUTED_CHOICE_2 = {
            14, 17, 11, 24,  1,  5,
             3, 28, 15,  6, 21, 10,
            23, 19, 12,  4, 26,  8,
            16,  7, 27, 20, 13,  2,

            41, 52, 31, 37, 47, 55,
            30, 40, 51, 45, 33, 48,
            44, 49, 39, 56, 34, 53,
            46, 42, 50, 36, 29, 32
        };


        std::optional<DES::SourceKey> DES::convert_key(
            const unsigned char* const data,
            std::size_t length
        )
        {
            if (length != DES::KEY_SIZE) {
                return std::optional<DES::SourceKey>();
            }

            DES::SourceKey result;
            for (auto byte_idx = 0; byte_idx < DES::KEY_SIZE; ++byte_idx)
            {
                for (auto bit_idx = 0; bit_idx < BYTE_SIZE; ++bit_idx)
                {
                    const auto bit_value = get_bit_value(
                        data[byte_idx],
                        calc_reversed_bit_idx(bit_idx)
                    );
                    const auto key_bit_idx = byte_idx * BYTE_SIZE + bit_idx;
                    result[key_bit_idx] = bit_value;
                }
            }

            return result;
        }


        std::optional<DES::SourceKey> DES::convert_key(const std::vector<uint8_t>& key)
        {
            return DES::convert_key(key.data(), key.size());
        }


        std::optional<DES::SourceKey> DES::convert_key(const std::string& key)
        {
            return DES::convert_key(
                reinterpret_cast<const unsigned char*>(key.c_str()),
                key.length()
            );
        }


        DES::SubstructedKey DES::substract_key(const DES::SourceKey& key)
        {
            DES::SubstructedKey result;

            uint8_t count_of_skipped_bits = 0;
            for (auto bit_idx = 0; bit_idx < result.size; ++bit_idx)
            {
                const auto choiced_bit_idx = PERMUTED_CHOICE_1[bit_idx] - 1;
                result[bit_idx] = key[choiced_bit_idx];
            }

            return result;
        }


        DES::RoundKey DES::generate_round_key(
            DES::HalfSubstructedKey& c_part,
            DES::HalfSubstructedKey& d_part,
            uint8_t round_num
        )
        {
            if (round_num <= 16)
            {
                const auto shifts_count = ITER_NUM_LEFT_SHIFTS_COUNT_MAP[round_num - 1];
                rotate_left(c_part, shifts_count);
                rotate_left(d_part, shifts_count);
            }

            DES::RoundKey round_key;
            for (auto bit_idx = 0; bit_idx < round_key.size; ++bit_idx)
            {
                const auto choiced_bit_idx = PERMUTED_CHOICE_2[bit_idx] - 1;

                const bool bit_val = choiced_bit_idx < DES::HalfSubstructedKey::size
                    ? c_part[choiced_bit_idx]
                    : d_part[choiced_bit_idx - DES::HalfSubstructedKey::size];

                round_key[bit_idx] = bit_val;
            }

            return round_key;
        }


        std::vector<DES::RoundKey> DES::generate_round_keys(
            const DES::SourceKey& key
        )
        {
            std::vector<DES::RoundKey> round_keys;
            round_keys.reserve(DES::ROUNDS_COUNT);

            const auto substructed_key = DES::substract_key(key);

            DES::HalfSubstructedKey c_part, d_part;
            const auto part_size = decltype(c_part)::size;
            std::tie(c_part, d_part) = split_blocks<part_size>(substructed_key);

            for (auto round_num = 1; round_num <= DES::ROUNDS_COUNT; ++round_num)
            {
                const auto round_key = generate_round_key(c_part, d_part, round_num);
                round_keys.push_back(round_key);
            }

            return round_keys;
        }


        template<std::size_t BITS_COUNT>
        BitBlock<BITS_COUNT> make_permuration(
            const BitBlock<BITS_COUNT>& bitblock,
            const std::vector<uint8_t>& permutation_table
        )
        {
            BitBlock<BITS_COUNT> result;
            for (auto bit_idx = 0; bit_idx < BITS_COUNT; ++ bit_idx) {
                const auto choiced_bit_idx = permutation_table[bit_idx] - 1;
                result[bit_idx] = bitblock[choiced_bit_idx];
            }

            return result;
        }

        DES::Block DES::make_init_permutation(const DES::Block& data)
        {
            return make_permuration(data, INITIAL_PERMUTATION);
        }

        DES::Block DES::make_inversed_init_permutation(const DES::Block& data)
        {
            return make_permuration(data, INVERSED_INITIAL_PERMUTATION);
        }


        BitBlock<DES::BIT_GROUP_SIZE> DES::get_bit_group(
            const DES::ExtendedHalfBlock& data,
            uint8_t group_idx
        )
        {
            BitBlock<DES::BIT_GROUP_SIZE> group;
            const auto first_data_bit_idx = group_idx * DES::BIT_GROUP_SIZE;

            for (auto i = 0; i < DES::BIT_GROUP_SIZE; ++i)
            {
                group[i] = data[first_data_bit_idx + i];
            }

            return group;
        }

        uint8_t DES::get_substitution_row_idx(
            const BitBlock<DES::BIT_GROUP_SIZE> group
        )
        {
            uint8_t row_idx = 0;
            row_idx = set_bit_value(row_idx, 1, group.first());
            row_idx = set_bit_value(row_idx, 0, group.last());

            return row_idx;
        }

        uint8_t DES::get_substitution_column_idx(
            const BitBlock<DES::BIT_GROUP_SIZE> group
        )
        {
            uint8_t column_idx = 0;
            for (auto i = 1; i <= 4; ++i)
            {
                const auto reversed_bit_idx = 4 - i;
                column_idx = set_bit_value(column_idx, reversed_bit_idx, group[i]);
            }

            return column_idx;
        }

        BitBlock<DES::SUBSTITUTION_SIZE> DES::get_group_substitution(
            const BitBlock<DES::BIT_GROUP_SIZE> group,
            const uint8_t group_idx
        )
        {
            const auto row_idx = get_substitution_row_idx(group);
            const auto column_idx = get_substitution_column_idx(group);

            const auto SBOX_ROWS_COUNT = 4;
            const auto SBOX_COLUMNS_COUNT = 16;
            const auto substitution_idx =
                SBOX_ROWS_COUNT * SBOX_COLUMNS_COUNT * group_idx
                    + row_idx * SBOX_COLUMNS_COUNT + column_idx;
            const auto substitution = S_BOXES[substitution_idx];

            return BitBlock<DES::SUBSTITUTION_SIZE>(substitution);
        }

        DES::HalfBlock DES::make_block_substitution(
            const DES::ExtendedHalfBlock& block
        )
        {
            DES::HalfBlock substituted_block;
            const auto SUBSTITUTED_GROUPS_COUNT = 8;
            for (auto group_idx = 0; group_idx < SUBSTITUTED_GROUPS_COUNT; ++group_idx) {
                const auto group = DES::get_bit_group(block, group_idx);
                const auto substitution = DES::get_group_substitution(group, group_idx);

                for (auto bit_idx = 0; bit_idx < substitution.size; ++bit_idx) {
                    const auto block_bit_num = group_idx * DES::SUBSTITUTION_SIZE + bit_idx;
                    substituted_block[block_bit_num] = substitution[bit_idx];
                }
            }

            return substituted_block;
        }

        DES::ExtendedHalfBlock DES::extend(const DES::HalfBlock& data)
        {
            DES::ExtendedHalfBlock ext_data;

            for (auto bit_idx = 0; bit_idx < ext_data.size; ++bit_idx)
            {
                const auto data_bit_num = E_BIT_SELECTION_TABLE[bit_idx];
                ext_data[bit_idx] = data[data_bit_num - 1];
            }

            return ext_data;
        }

        DES::HalfBlock DES::f_function(const DES::HalfBlock& data, const DES::RoundKey& key)
        {
            const auto extended_data = extend(data);
            const auto key_ext_data_xor = extended_data ^ key;
            const auto substituted_block = make_block_substitution(key_ext_data_xor);

            // shrinking extended block
            return make_permuration(substituted_block, F_PERMUTATION);
        }


        DES::Block DES::encrypt_block(
            const DES::Block& data,
            const std::vector<DES::RoundKey>& round_keys
        )
        {
            const auto permuted_data = make_init_permutation(data);
            DES::HalfBlock l_block;
            DES::HalfBlock r_block;
            std::tie(l_block, r_block) = split_blocks<DES::HalfBlock::size>(permuted_data);

            DES::HalfBlock temp_block;
            for (auto round_num = 1; round_num <= DES::ROUNDS_COUNT; ++round_num)
            {
                temp_block = r_block;

                const auto& round_key = round_keys[round_num - 1];
                const auto f_result = f_function(r_block, round_key);
                r_block = l_block ^ f_result;

                l_block = temp_block;
            }

            const auto concated_blocks = concat_blocks(r_block, l_block);

            return make_inversed_init_permutation(concated_blocks);
        }


        std::vector<uint8_t> DES::encrypt_data(
            const std::vector<uint8_t>& data,
            const DES::SourceKey& key
        )
        {
            AdditionBitBlockIter<DES::BLOCK_SIZE> block_iter(data);
            const auto round_keys = generate_round_keys(key);

            std::vector<uint8_t> encrypted_data;
            encrypted_data.reserve(data.size() + DES::BLOCK_SIZE); // For additional block

            do {
                const auto data_block = block_iter.getCur();
                const auto encrypted_block = DES::encrypt_block(data_block, round_keys);
                append_to_arr_of_bytes<DES::BLOCK_SIZE>(
                    encrypted_block,
                    std::back_inserter(encrypted_data)
                );

                if (block_iter.hasNext()) {
                    block_iter.next();
                } else {
                    break;
                }
            } while (true);

            return encrypted_data;
        }


        DES::Block DES::decrypt_block(
            const DES::Block& data,
            const std::vector<DES::RoundKey>& round_keys
        )
        {
            const auto permuted_data = make_init_permutation(data);
            DES::HalfBlock l_block;
            DES::HalfBlock r_block;
            std::tie(l_block, r_block) = split_blocks<DES::HalfBlock::size>(permuted_data);

            DES::HalfBlock temp_block;
            for (auto round_num = DES::ROUNDS_COUNT; round_num >= 1; --round_num)
            {
                temp_block = r_block;

                const auto& round_key = round_keys[round_num - 1];
                const auto f_result = f_function(r_block, round_key);
                r_block = l_block ^ f_result;

                l_block = temp_block;
            }

            const auto concated_blocks = concat_blocks(r_block, l_block);

            return make_inversed_init_permutation(concated_blocks);
        }


        std::vector<uint8_t> DES::decrypt_data(
            const std::vector<uint8_t>& data,
            const DES::SourceKey& key
        )
        {
            BitBlockIter<DES::BLOCK_SIZE> block_iter(data);
            const auto round_keys = generate_round_keys(key);

            std::vector<uint8_t> decrypted_data;
            decrypted_data.reserve(data.size() + DES::BLOCK_SIZE); // For additional block

            do {
                const auto data_block = block_iter.getCur();
                const auto decrypted_block = DES::decrypt_block(data_block, round_keys);

                if (block_iter.hasNext()) {
                    append_to_arr_of_bytes<DES::BLOCK_SIZE>(
                        decrypted_block,
                        std::back_inserter(decrypted_data)
                    );

                    block_iter.next();
                } else {
                    append_last_block_to_arr_of_bytes<DES::BLOCK_SIZE>(
                        decrypted_block,
                        std::back_inserter(decrypted_data)
                    );

                    break;
                }
            } while (true);

            return decrypted_data;
        }
    }
}
