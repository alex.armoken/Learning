#include "stdio.h"
#include "stdint.h"

int main(int argc, char *argv[])
{
    char buf;
    uint32_t pos = 0;
    char a[10] = "000000000";
    char b[10] = "111111111";
    while ((buf = getc(stdin) != '\n')) {
        a[pos++] = buf;
    }
    a[pos] = '\0';

    printf("a - %s\n", a);
    printf("b - %s\n", b);
    return 0;
}
