#include "crypto/sha256/sha256.hpp"
#include "crypto/utils/byte_helpers.hpp"

using namespace crypto::utils::byte_helpers;

void SHA256::DataIter::split_size_num(uint64_t num,
                                      std::vector<uint8_t> &bytes)
{
    for (int8_t i = 7; i > -1; i--) {
        bytes.push_back((num >> 8 * i) & 0xFF);
    }
}

bool SHA256::DataIter::is_end()
{
    return size_byte_num >= 8;
}

bool SHA256::ArrIter::bad()
{
    return false;
}

void SHA256::ArrIter::operator++()
{
    if (iter < end) {
        iter++;
        if (iter == end) {
            buf = 0x80;
        } else {
            data_size++;
            buf = *iter;
        }
    } else if (is_size_addition) {
        buf = size_bytes_arr[size_byte_num++];
    } else if (((data_size + zeros_count + 1) * 8) % 512 == 448) {
        is_size_addition = true;
        split_size_num(data_size * 8, size_bytes_arr);
        buf = size_bytes_arr[size_byte_num++];
    } else {
        buf = 0;
        zeros_count++;
    }
}

void SHA256::ArrIter::operator++(int)
{
    operator++();
}

uint8_t SHA256::ArrIter::operator*()
{
    return buf;
}

bool SHA256::FileIter::bad()
{
    return input.bad();
}

uint8_t SHA256::FileIter::operator*()
{
    return buf;
}

void SHA256::FileIter::operator++()
{
    if (!input.eof()) {
        buf = input.get();
        if (input.eof()) {
            buf = 0x80;
        } else {
            data_size++;
        }
    } else if (is_size_addition) {
        buf = size_bytes_arr[size_byte_num++];
    } else if (((data_size + zeros_count + 1) * 8) % 512 == 448) {
        is_size_addition = true;
        split_size_num(data_size * 8, size_bytes_arr);
        buf = size_bytes_arr[size_byte_num++];
    } else {
        buf = 0;
        zeros_count++;
    }
}

void SHA256::FileIter::operator++(int)
{
    operator++();
}

constexpr uint32_t SHA256::k[64];


uint32_t SHA256::shr(uint32_t x, uint8_t n)
{
    return (x >> n);
}

uint32_t SHA256::rotr(uint32_t x, uint8_t n)
{
    return ((x >> n) | (x << ((sizeof(uint32_t) << 3) - n)));
}

uint32_t SHA256::ch(uint32_t x, uint32_t y, uint32_t z)
{
    return ((x & y) ^ (~x & z));
}

uint32_t SHA256::maj(uint32_t x, uint32_t y, uint32_t z)
{
    return ((x & y) ^ (x & z) ^ (y & z));
}

uint32_t SHA256::f1(uint32_t x)
{
    return (rotr(x, 2) ^ rotr(x, 13) ^ rotr(x, 22));
}

uint32_t SHA256::f2(uint32_t x)
{
    return (rotr(x, 6) ^ rotr(x, 11) ^ rotr(x, 25));
}

uint32_t SHA256::f3(uint32_t x)
{
    return (rotr(x, 7) ^ rotr(x, 18) ^ shr(x, 3));
}

uint32_t SHA256::f4(uint32_t x)
{
    return rotr(x, 17) ^ rotr(x, 19) ^ shr(x, 10);
}

SHA256::READING_STATE SHA256::get_word(DataIter &iter, uint32_t &word)
{
    READING_STATE state = BUF_FULL;
    std::vector<uint8_t> bytes(BYTES_IN_WORD);

    for (uint8_t i = 0; i < BYTES_IN_WORD; i++) {
        if (iter.bad()) {
            return ERROR;
        }
        bytes[i] = *iter;
        iter++;
    }
    word = bytes_to_int(bytes);
    return state;
}

SHA256::READING_STATE SHA256::get_block(DataIter &iter,
                                        std::vector<uint32_t> &data)
{
    uint32_t word = 0;
    READING_STATE state = BUF_FULL;

    for (uint8_t i = 0; i < WORD_IN_BLOCK; i++) {
        if ((state = get_word(iter, word)) == ERROR) {
            return ERROR;
        }
        data.push_back(word);
    }
    return iter.is_end() ? LAST_BUF : state;
}

void SHA256::init_digest(std::vector<uint32_t> &digest)
{
    digest.push_back(0x6a09e667);
    digest.push_back(0xbb67ae85);
    digest.push_back(0x3c6ef372);
    digest.push_back(0xa54ff53a);
    digest.push_back(0x510e527f);
    digest.push_back(0x9b05688c);
    digest.push_back(0x1f83d9ab);
    digest.push_back(0x5be0cd19);
}

void SHA256::hash_round(
    std::vector<uint32_t> &vars,
    std::vector<uint32_t> &w,
    uint8_t i
)
{
    uint32_t t1 = vars[7] + f2(vars[4]) +
        ch(vars[4], vars[5], vars[6]) + k[i] + w[i];
    uint32_t t2 = f1(vars[0]) + maj(vars[0], vars[1], vars[2]);

    vars[7] = vars[6];
    vars[6] = vars[5];
    vars[5] = vars[4];
    vars[4] = vars[3] + t1;
    vars[3] = vars[2];
    vars[2] = vars[1];
    vars[1] = vars[0];
    vars[0] = t1 + t2;
}

void SHA256::hash_block(
    std::vector<uint32_t> &w,
    std::vector<uint32_t> &digest
)
{
    for (uint8_t i = 16; i < 64; i++) {
        uint32_t s0 = f3(w[i - 15]);
        uint32_t s1 = f4(w[i - 2]);
        w.push_back(w[i - 16] + s0 + w[i - 7] + s1);
    }
    std::vector<uint32_t> vars(digest);

    for (uint8_t i = 0; i < 64; i++) {
        hash_round(vars, w, i);
    }

    for (uint8_t i = 0; i < 8; i++) {
        digest[i] += vars[i];
    }
}

bool SHA256::get_hash(std::string &path, std::vector<uint32_t> &hash)
{
    std::vector<uint32_t> w;
    READING_STATE st = BUF_FULL;

    init_digest(hash);
    FileIter it(path);

    while ((st = SHA256::get_block(it, w)) != ERROR && st != LAST_BUF) {
        hash_block(w, hash);
        w.clear();
    }
    if (st == ERROR) {
        return false;
    }

    hash_block(w, hash);
    return true;
}

bool SHA256::get_hash(
    const std::vector<uint8_t> &data,
    std::vector<uint32_t> &hash
)
{
    READING_STATE st;
    ArrIter it(data);
    init_digest(hash);
    std::vector<uint32_t> w;

    while ((st = get_block(it, w)) != ERROR && st != LAST_BUF) {
        hash_block(w, hash);
        w.clear();
    }
    if (st == ERROR) {
        return false;
    }

    hash_block(w, hash);
    return true;
}
