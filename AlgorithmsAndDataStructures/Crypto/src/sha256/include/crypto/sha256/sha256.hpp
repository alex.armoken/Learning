#ifndef SHA256_H
#define SHA256_H

#include <vector>
#include <bitset>
#include <string>
#include <cstring>
#include <fstream>
#include <iostream>

class SHA256
{
private:
    SHA256();

public:
    class DataIter
    {
    protected:
        uint8_t buf;
        uint64_t data_size;
        uint8_t zeros_count;
        uint8_t size_byte_num;
        bool is_size_addition;
        std::vector<uint8_t> size_bytes_arr;

    public:
        DataIter()
        {
            data_size = 0;
            zeros_count = 0;
            size_byte_num = 0;
            is_size_addition = false;
        }
        virtual ~DataIter() {};

        bool is_end();
        static void split_size_num(uint64_t num, std::vector<uint8_t> &bytes);

        virtual bool bad() = 0;
        virtual uint8_t operator*() = 0;
        virtual void operator++() = 0;
        virtual void operator++(int) = 0;
    };

    class ArrIter: public DataIter
    {
    private:
        std::vector<uint8_t>::const_iterator iter;
        std::vector<uint8_t>::const_iterator end;

    public:
        ArrIter(const std::vector<uint8_t> &data): DataIter()
        {
            iter = data.cbegin();
            end = data.cend();
            buf = *iter;
            data_size++;
        }

        ArrIter(
            std::vector<uint8_t>::const_iterator iter,
            std::vector<uint8_t>::const_iterator end
        ) : DataIter(), iter(iter), end(end)
        {
            buf = *iter;
            data_size++;
        }

        ~ArrIter(){}

        bool bad();
        uint8_t operator*();
        void operator++();
        void operator++(int);
    };

    class FileIter: public DataIter
    {
    private:
        std::ifstream input;

    public:
        FileIter(std::string &path): DataIter()
            {
                input.open(path);
                buf = input.get();
                data_size++;
            }

        ~FileIter()
            {
                input.close();
            }

        bool bad();
        uint8_t operator*();
        void operator++();
        void operator++(int);
    };

    enum READING_STATE {
        BUF_FULL,
        LAST_BUF,
        ERROR
    };

    static constexpr uint32_t k[64] = {
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
        0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
        0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
        0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
        0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
        0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
        0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
        0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
        0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
        0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
        0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
        0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
        0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
        0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
        0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    };

    static const uint8_t BYTES_IN_WORD = 4;
    static const uint8_t WORD_IN_BLOCK = 16;
    static const uint8_t BLOCK_SIZE = 512 / 8;
    static const uint8_t DIGEST_SIZE = 256 / 8;

    static uint32_t shr(uint32_t x, uint8_t n);
    static uint32_t rotr(uint32_t x, uint8_t n);
    static uint32_t ch(uint32_t x, uint32_t y, uint32_t z);
    static uint32_t maj(uint32_t x, uint32_t y, uint32_t z);

    static uint32_t f1(uint32_t x);
    static uint32_t f2(uint32_t x);
    static uint32_t f3(uint32_t x);
    static uint32_t f4(uint32_t x);

    static void init_digest(std::vector<uint32_t> &digest);

    static READING_STATE get_word(DataIter &iter, uint32_t &word);
    static READING_STATE get_block(
        DataIter &iter,
        std::vector<uint32_t> &data
    );

    static void hash_round(
        std::vector<uint32_t> &vars,
        std::vector<uint32_t> &w,
        uint8_t i
    );
    static void hash_block(
        std::vector<uint32_t> &w,
        std::vector<uint32_t> &digest
    );

    static bool get_hash(std::string &path, std::vector<uint32_t> &hash);
    static bool get_hash(
        const std::vector<uint8_t> &data,
        std::vector<uint32_t> &hash
    );
};

std::string sha256(std::string input);

#endif
