#include "crypto/rsa/rsa.hpp"

#include "iostream"

#include <boost/random.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/miller_rabin.hpp>

namespace crypto::rsa
{
using boost::multiprecision::cpp_int;

cpp_int generate_random_number(uint16_t bits_count)
{

}

void calc_test()
{
    cpp_int one = 1;
    cpp_int first = one << 201; //2^201
    cpp_int second = one << 127; //2^127

    std::cout << "first = " << first << std::endl;
    std::cout << "second = " << second << std::endl;
    std::cout << "first * second = " << first * second << std::endl;
}
}
