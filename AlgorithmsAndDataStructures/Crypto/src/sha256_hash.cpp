#include "getopt.h"

#include "crypto/sha256/sha256.hpp"

struct global_args_t {
    char *input_filename;
} global_args;


static struct option OPTS[] = {
    {"input", required_argument, NULL, 'i'},
    {0, 0, 0, 0}
};


int parse_arguments(int argc, char *argv[])
{
    int c;
    int opt_idx;

    const char *args = "i:";
    while ((c = getopt_long(argc, argv, args, OPTS, &opt_idx)) != -1) {
        switch (c) {
        case 'i':
            global_args.input_filename = optarg;
            break;
        default:
            return -1;
        }
    }
    return 0;
}


bool cnheck_args_logic()
{
    if (global_args.input_filename == NULL) {
        return false;
    }
    return true;
}


int hash_program()
{
    std::string path(global_args.input_filename);
    std::vector<uint32_t> hash;
    if (!SHA256::get_hash(path, hash)) {
        return 5;
    }

    for (uint8_t i = 0; i < hash.size(); i++) {
        std::cout << std::hex << hash[i] << " ";
    }
    std::cout << std::endl;
    return 0;
}


int main(int argc, char* argv[])
{
    if (parse_arguments(argc, argv) == -1) {
        return 1;
    }

    return hash_program();
}
