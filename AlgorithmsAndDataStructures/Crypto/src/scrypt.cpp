#include <functional>
#include <string>
#include <locale>
#include <iostream>
#include <variant>
#include <optional>

#include <expected.hpp>
#include <boost/program_options.hpp>

#include "crypto/aes/aes.hpp"
#include "crypto/belt/belt.hpp"
#include "crypto/belt/key_schedule.hpp"

#include "crypto/des/des.hpp"
#include "crypto/des/double_des.hpp"
#include "crypto/des/triple_des.hpp"

#include "crypto/sha256/sha256.hpp"
#include "crypto/hmac/hmac.hpp"
#include "crypto/rsa/rsa.hpp"

#include "crypto/utils/io_helpers.hpp"
#include "crypto/utils/enum_helpers.hpp"
#include "crypto/utils/string_helpers.hpp"
#include "crypto/utils/encryption_mode.hpp"


using namespace crypto::utils::io_helpers;
using namespace crypto::utils::enum_helpers;
using namespace crypto::utils::string_helpers;


namespace scrypt
{
    using nonstd::expected;
    using nonstd::make_unexpected;

    using std::optional;
    namespace boost_po = boost::program_options;

    using namespace crypto::aes;
    using namespace crypto::des;
    using namespace crypto::belt;
    using namespace crypto::hmac;

    enum class ProgramReturnCode
    {
        FIRST = 0,
        ALL_IS_GOOD = 0,
        ERROR_DURING_ARGS_PARSING = 1,
        ERROR_DURING_KEY_CONVERSION = 2,
        ERROR_DURING_ENCRYPTION = 3,
        ERROR_DURING_DECRYPTION = 4,
        ALGORITHM_NOT_FOUND = 5,
        ERROR_DURING_READING_FROM_FILE = 6,
        ERROR_DURING_READING_FROM_STDIN = 7,
        ERROR_DURING_OUTPUT_SAVING = 8,
        ERROR_DURING_OUTPUT_PRINTING = 9,

        LAST = 9
    };
    static const std::map<ProgramReturnCode, std::string> ReturnValExplanationMap= {
        {ProgramReturnCode::ALL_IS_GOOD, "All is good"},
        {ProgramReturnCode::ERROR_DURING_KEY_CONVERSION, "Error during key conversion"}
    };

    enum class ProgramType {
        FIRST = 0,
        ENCRYPTION = 0,
        STEGANOGRAPHY = 1,
        HASH = 2,
        HMAC = 3,
        LAST = 3
    };
    enum class ArgsLogicError {
      FIRST = 0,
      ENCRYPTION_AND_DECRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY = 0,
      STEGANOGRAPHY_AND_ENCRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY = 1,
      HASH_AND_STEGANOGRAPHY_OR_ENCRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY = 2,
      HMAC_OR_HASH_OR_STEGANOGRAPHY_OR_ENCRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY = 3,
      LAST = 3
    };
    static const std::map<ArgsLogicError, std::string> ArgsLogicErrorsExplanationMap = {
        {
            ArgsLogicError::ENCRYPTION_AND_DECRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY,
            "You use --encryption and --decryption flags simultaneously. Use "
            "only one."
        },
        {
            ArgsLogicError::STEGANOGRAPHY_AND_ENCRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY,
            "You use --steganography and one of encryption flags "
            "(--encryption or --decryption) simultaneously. Use only one --steganography."
        },
        {
            ArgsLogicError::HASH_AND_STEGANOGRAPHY_OR_ENCRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY,
            "You use --hash or --steganography or one of encryption flags."
        }
    };

    enum class EncryptionState
    {
        ENCRYPTION,
        DESCRYPTION
    };
    using RawKey = std::string;
    using EncryptionProgramPtr = std::function<
        expected<std::vector<uint8_t>, ProgramReturnCode>
        (
            const std::vector<uint8_t>&,
            const RawKey&,
            const EncryptionState
        )
    >;

    using HashProgramPtr = std::function<
        expected<std::vector<uint32_t>, ProgramReturnCode>
        (
            const std::vector<uint8_t>&
        )
    >;

    enum class HMACState
    {
        CREATE,
        INVALIDATE,
        REMOVE
    };
    using HMACProgramPtr = std::function<
        expected<std::vector<uint8_t>, ProgramReturnCode>
        (
            const std::vector<uint8_t>&,
            const RawKey&,
            HMACState
        )
    >;

    enum class SteganographyState
    {
        HIDING,
        EXTRACTING
    };
    using SteganographyProgramPtr = std::function<
        expected<bool, ProgramReturnCode>
        (
            bool,
            const std::vector<uint8_t>&,
            SteganographyState
        )
    >;

    using ProgramPtr = std::function<ProgramReturnCode(const boost_po::variables_map&)>;

    const auto DEFAULT_HASH_ALGO_NAME = "sha256";
    const auto DEFAULT_HMAC_ALGO_NAME = "sha256";
    const auto DEFAULT_ENCRYPTION_ALGO_NAME = "des";
    const auto DEFAULT_STEGANOGRAPHY_ALGO_NAME = "lsb";

    const auto HELP_OPTION = "help";
    const auto INPUT_FILE_OPTION = "input";
    const auto OUTPUT_FILE_OPTION = "output";
    const auto KEY_OPTION = "key";
    const auto ALGO_OPTION = "algorithm";
    const auto ENCRYPT_OPTION = "encrypt";
    const auto DECRYPT_OPTION = "decrypt";

    const auto HASH_OPTION = "hash";
    const auto HMAC_OPTION = "hmac";
    const auto INVALIDATE_HMAC_OPTION = "invalidate-hmac";
    const auto REMOVE_HMAC_OPTION = "remove-hmac";

    const auto HIDE_OPTION = "hide";
    const auto EXTRACT_OPTION = "extract";
    const auto STEGANOGRAPHY_OPTION = "steganography";
    const auto MESSAGE_OPTION = "message";
    const auto MESSAGE_FILE_OPTION = "message-file";
    const auto MESSAGE_OUT_FILE_OPTION = "message-out-file";


    std::string concat_with_comma(
        const std::string& long_option,
        const std::string& short_option
    )
    {
        return long_option + "," + short_option;
    }


    boost_po::options_description get_options_description()
    {
        boost_po::options_description opts_desc;
        opts_desc.add_options()
            (
                concat_with_comma(HELP_OPTION, "h").c_str(),
                "Print programs description"
            )
            (
                concat_with_comma(INPUT_FILE_OPTION, "i").c_str(),
                boost_po::value<std::string>(),
                "Path to file, which will be encrypted/descrypted"
            )
            (
                concat_with_comma(OUTPUT_FILE_OPTION, "o").c_str(),
                boost_po::value<std::string>(),
                "Path to encrypted/descrypted file"
            )
            (
                concat_with_comma(ALGO_OPTION, "a").c_str(),
                boost_po::value<std::string>(),
                "Encryption algorithm, default is DES"
            )
            (
                concat_with_comma(ENCRYPT_OPTION, "e").c_str(),
                "Encrypt input file"
            )
            (
                concat_with_comma(DECRYPT_OPTION, "d").c_str(),
                "Decrypt input file"
            )
            (
                concat_with_comma(HIDE_OPTION, "hd").c_str(),
                "Hide message in data"
            )
            (
                concat_with_comma(EXTRACT_OPTION, "ex").c_str(),
                "Extract message from data"
            )
            (
                concat_with_comma(STEGANOGRAPHY_OPTION, "s").c_str(),
                "Hide data in image file"
            )
            (
                concat_with_comma(HASH_OPTION, "hs").c_str(),
                "Generate hash"
            )
            (
                HMAC_OPTION,
                "Add HMAC to the end"
            )
            (
                INVALIDATE_HMAC_OPTION,
                "Invalidate HMAC in data"
            )
            (
                REMOVE_HMAC_OPTION,
                "Remove HMAC from data"
            )
            (
                concat_with_comma(KEY_OPTION, "k").c_str(),
                "Key which will be used for encryption/decryption"
            );

        return opts_desc;
    }


    optional<ArgsLogicError> check_args_logic(const boost_po::variables_map& var_map)
    {
        const auto is_hash = var_map.count(HASH_OPTION) != 0;
        const auto is_encryption = var_map.count(ENCRYPT_OPTION) != 0;
        const auto is_decryption = var_map.count(DECRYPT_OPTION) != 0;
        const auto is_steganography = var_map.count(STEGANOGRAPHY_OPTION) != 0;
        const auto is_hmac = var_map.count(HMAC_OPTION) != 0
            || var_map.count(INVALIDATE_HMAC_OPTION) != 0
            || var_map.count(REMOVE_HMAC_OPTION) != 0;

        if (is_encryption && is_decryption)
        {
            return ArgsLogicError::ENCRYPTION_AND_DECRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY;
        }
        else if (is_steganography && (is_encryption || is_decryption))
        {
            return ArgsLogicError::STEGANOGRAPHY_AND_ENCRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY;
        }
        else if (is_hash && (is_steganography || (is_encryption || is_decryption)))
        {
            return ArgsLogicError::HASH_AND_STEGANOGRAPHY_OR_ENCRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY;
        }
        else if (is_hmac && (is_hash || is_steganography || (is_encryption || is_decryption)))
        {
            return ArgsLogicError::HMAC_OR_HASH_OR_STEGANOGRAPHY_OR_ENCRYPTION_FLAGS_EXISTS_SIMULTANEOUSLY;
        }

        return optional<ArgsLogicError>();
    }


    void print_arguments_error(const std::string& msg)
    {
        std::cerr << "Arguments error!" << std::endl
                  <<  msg << std::endl;
    }


    void print_program_error(const std::string& msg)
    {
        std::cerr << "Program error!" << std::endl
                  <<  msg << std::endl;
    }


    expected<boost_po::variables_map, std::string> parse_arguments(
        int argc,
        char **argv
    )
    {
        auto opts_desc = get_options_description();
        boost_po::parse_command_line(argc, argv, opts_desc);

        boost_po::variables_map var_map;
        try
        {
            auto parsed_opts = boost_po::command_line_parser(argc, argv)
                .options(opts_desc).run();
            boost_po::store(parsed_opts, var_map);
        }
        catch (const boost_po::error& err)
        {
            return make_unexpected(err.what());
        }

        const auto maybe_error = check_args_logic(var_map);
        if (maybe_error) {
            return make_unexpected(
                ArgsLogicErrorsExplanationMap.at(*maybe_error)
            );
        }

        return var_map;
    }


    expected<std::vector<uint8_t>, ProgramReturnCode> des_program(
        const std::vector<uint8_t>& data,
        const RawKey& key,
        const EncryptionState encryption_state
    )
    {
        const auto maybe_converted_key = DES::convert_key(key);
        if (not maybe_converted_key) {
            return make_unexpected(ProgramReturnCode::ERROR_DURING_KEY_CONVERSION);
        }

        if (encryption_state == EncryptionState::ENCRYPTION) {
            return DES::encrypt_data(data, *maybe_converted_key);
        } else {
            return DES::decrypt_data(data, *maybe_converted_key);
        }
    }


    expected<std::vector<uint8_t>, ProgramReturnCode> double_des_program(
        const std::vector<uint8_t>& data,
        const RawKey& key,
        const EncryptionState encryption_state
    )
    {
        const auto maybe_converted_key = DoubleDES::convert_key(key);
        if (not maybe_converted_key) {
            return make_unexpected(ProgramReturnCode::ERROR_DURING_KEY_CONVERSION);
        }

        if (encryption_state == EncryptionState::ENCRYPTION) {
            return DoubleDES::encrypt_data(data, *maybe_converted_key);
        } else {
            return DoubleDES::decrypt_data(data, *maybe_converted_key);
        }
    }


    expected<std::vector<uint8_t>, ProgramReturnCode> triple_des_program(
        const std::vector<uint8_t>& data,
        const RawKey& key,
        const EncryptionState encryption_state
    )
    {
        const auto maybe_converted_key = TripleDES::convert_key(key);
        if (not maybe_converted_key) {
            return make_unexpected(ProgramReturnCode::ERROR_DURING_KEY_CONVERSION);
        }

        if (encryption_state == EncryptionState::ENCRYPTION) {
            return TripleDES::encrypt_data(data, *maybe_converted_key);
        } else {
            return TripleDES::decrypt_data(data, *maybe_converted_key);
        }
    }


    expected<std::vector<uint8_t>, ProgramReturnCode> belt_program(
        const std::vector<uint8_t>& data,
        const RawKey& key,
        const EncryptionState encryption_state,
        const EncryptionMode encryption_mode
    )
    {
        const auto maybe_converted_key = KeySchedule::get_schedule(key);
        if (not maybe_converted_key) {
            return make_unexpected(ProgramReturnCode::ERROR_DURING_KEY_CONVERSION);
        }

        if (encryption_state == EncryptionState::ENCRYPTION) {
            return BelT::encrypt_data(
                data,
                *maybe_converted_key,
                encryption_mode
            );
        } else {
            return BelT::decrypt_data(
                data,
                *maybe_converted_key,
                encryption_mode
            );
        }
    }


    optional<EncryptionProgramPtr> get_encryption_program(const std::string& algo_name)
    {
        using namespace std::placeholders;

        // DES group
        if (algo_name == "des") {
            return EncryptionProgramPtr(des_program);
        } else if (algo_name == "2des") {
            return EncryptionProgramPtr(double_des_program);
        } else if (algo_name == "3des") {
            return EncryptionProgramPtr(triple_des_program);
        }

        // BelT group
        if (algo_name == "belt-ecb") {
            return EncryptionProgramPtr(std::bind(belt_program, _1, _2, _3, EncryptionMode::ECB));
        } else if (algo_name == "belt-cbc") {
            return EncryptionProgramPtr(std::bind(belt_program, _1, _2, _3, EncryptionMode::CBC));
        } else if (algo_name == "belt-cfb") {
            return EncryptionProgramPtr(std::bind(belt_program, _1, _2, _3, EncryptionMode::CFB));
        }

        return optional<EncryptionProgramPtr>();
    }


    expected<std::vector<uint8_t>, ProgramReturnCode> read_data(
        const boost_po::variables_map& var_map
    )
    {
        if (var_map.count(INPUT_FILE_OPTION) == 0) {
            auto maybe_result = read_from_stdin();
            if (maybe_result) {
                return *maybe_result;
            }

            return make_unexpected(ProgramReturnCode::ERROR_DURING_READING_FROM_STDIN);
        }

        const auto path_to_file = var_map[INPUT_FILE_OPTION].as<std::string>();
        auto maybe_result = read_file(path_to_file);
        if (maybe_result) {
            return *maybe_result;
        }

        return make_unexpected(ProgramReturnCode::ERROR_DURING_READING_FROM_FILE);
    }


    ProgramReturnCode write_data(
        const boost_po::variables_map& var_map,
        const std::vector<uint8_t>& data
    )
    {
        const auto is_stdout = var_map.count(OUTPUT_FILE_OPTION) == 0;
        if (is_stdout) {
            const auto is_success = write_to_stdout(data);

            return is_success
                ? ProgramReturnCode::ALL_IS_GOOD
                : ProgramReturnCode::ERROR_DURING_OUTPUT_PRINTING;
        }

        const auto path_to_file = var_map[OUTPUT_FILE_OPTION].as<std::string>();
        const auto is_success = write_to_file(path_to_file, data);

        return is_success
            ? ProgramReturnCode::ALL_IS_GOOD
            : ProgramReturnCode::ERROR_DURING_OUTPUT_SAVING;
    }


    ProgramReturnCode encryption_program(const boost_po::variables_map& var_map)
    {
        const auto maybe_input_data = read_data(var_map);
        if (!maybe_input_data) {
            return maybe_input_data.error();
        }

        const auto algo_name = to_lowercase(
            var_map.count(ALGO_OPTION) != 0
                ? var_map[ALGO_OPTION].as<std::string>()
                : DEFAULT_ENCRYPTION_ALGO_NAME
        );
        const auto maybe_program_ptr = get_encryption_program(algo_name);
        if (!maybe_program_ptr) {
            return ProgramReturnCode::ALGORITHM_NOT_FOUND;
        }

        const auto raw_key = var_map[KEY_OPTION].as<std::string>();

        const auto encryption_state = var_map.count(ENCRYPT_OPTION)
            ? EncryptionState::ENCRYPTION
            : EncryptionState::DESCRYPTION;

        const auto expected_proccessed_data = (*maybe_program_ptr)(
            *maybe_input_data,
            raw_key,
            encryption_state
        );
        if (not expected_proccessed_data) {
            return expected_proccessed_data.error();
        }

        return write_data(var_map, *expected_proccessed_data);
    }


    expected<std::vector<uint32_t>, ProgramReturnCode> sha256_program(
        const std::vector<uint8_t>& data
    )
    {
        std::vector<uint32_t> result;
        SHA256::get_hash(data, result);

        return result;
    }


    optional<HashProgramPtr> get_hash_program(const std::string& algo_name)
    {
        if (algo_name == "sha256") {
            return HashProgramPtr(sha256_program);
        }

        return optional<HashProgramPtr>();
    }


    ProgramReturnCode hash_program(const boost_po::variables_map& var_map)
    {
        const auto maybe_input_data = read_data(var_map);
        if (!maybe_input_data) {
            return maybe_input_data.error();
        }

        const auto algo_name = to_lowercase(
            var_map.count(ALGO_OPTION) != 0
                ? var_map[ALGO_OPTION].as<std::string>()
                : DEFAULT_HASH_ALGO_NAME
        );
        const auto maybe_program_ptr = get_hash_program(algo_name);
        if (!maybe_program_ptr) {
            return ProgramReturnCode::ALGORITHM_NOT_FOUND;
        }

        const auto expected_proccessed_data = (*maybe_program_ptr)(
            *maybe_input_data
        );
        if (not expected_proccessed_data) {
            return expected_proccessed_data.error();
        }

        for (const auto hash_part : *expected_proccessed_data) {
            std::cout << std::hex
                      << std::setfill('0') << std::setw(8)
                      << hash_part;
        }
        std::cout << std::endl;

        return ProgramReturnCode::ALL_IS_GOOD;
    }


    expected<std::vector<uint8_t>, ProgramReturnCode> hmac_sha256_program(
        const std::vector<uint8_t>& data,
        const RawKey& key,
        HMACState state
    )
    {
        if (state == HMACState::CREATE) {
            return HMAC::sha256_hmac(
                std::vector<uint8_t>(key.begin(), key.end()),
                data
            );
        } else if (state == HMACState::INVALIDATE) {
            const auto is_valid = HMAC::invalidate_sha256_hmac(
                std::vector<uint8_t>(key.begin(), key.end()),
                data
            );

            const std::string valid_msg("Valid HMAC");
            const std::string invalid_msg("Invalid HMAC");

            return is_valid
                ? std::vector<uint8_t>(valid_msg.begin(), valid_msg.end())
                : std::vector<uint8_t>(invalid_msg.begin(), invalid_msg.end());
        } else {
            std::vector<uint8_t> mutable_data(data);
            HMAC::remove_sha256_hmac(mutable_data);

            return mutable_data;
        }
    }

    optional<HMACProgramPtr> get_hmac_program(const std::string& algo_name)
    {
        if (algo_name == "sha256") {
            return HMACProgramPtr(hmac_sha256_program);
        }

        return optional<HMACProgramPtr>();
    }


    ProgramReturnCode hmac_program(const boost_po::variables_map& var_map)
    {
        const auto maybe_input_data = read_data(var_map);
        if (!maybe_input_data) {
            return maybe_input_data.error();
        }

        const auto algo_name = to_lowercase(
            var_map.count(ALGO_OPTION) != 0
                ? var_map[ALGO_OPTION].as<std::string>()
                : DEFAULT_HMAC_ALGO_NAME
        );
        const auto maybe_program_ptr = get_hmac_program(algo_name);
        if (!maybe_program_ptr) {
            return ProgramReturnCode::ALGORITHM_NOT_FOUND;
        }

        const auto hmac_state = var_map.count(HMAC_OPTION) != 0 ? HMACState::CREATE
            : var_map.count(INVALIDATE_HMAC_OPTION) != 0 ? HMACState::INVALIDATE
            : HMACState::REMOVE;

        const auto raw_key = var_map[KEY_OPTION].as<std::string>();
        const auto expected_proccessed_data = (*maybe_program_ptr)(
            *maybe_input_data,
            raw_key,
            hmac_state
        );
        if (not expected_proccessed_data) {
            return expected_proccessed_data.error();
        }

        return write_data(var_map, *expected_proccessed_data);
    }


    expected<boost::gil::rgb8_image_t, ProgramReturnCode> read_image(
        const boost_po::variables_map& var_map
    )
    {
        const auto path_to_file = var_map[INPUT_FILE_OPTION].as<std::string>();
        auto maybe_result = io_helpers::read_image(path_to_file);
        if (maybe_result) {
            return *maybe_result;
        }

        return make_unexpected(ProgramReturnCode::ERROR_DURING_READING_FROM_FILE);
    }


    ProgramReturnCode write_image(
        const boost::gil::rgb8_image_t& img,
        const boost_po::variables_map& var_map
    )
    {
        const auto path_to_file = var_map[INPUT_FILE_OPTION].as<std::string>();
        if (io_helpers::write_image(img, path_to_file)) {
            return ProgramReturnCode::ALL_IS_GOOD;
        }

        return ProgramReturnCode::ERROR_DURING_OUTPUT_SAVING;
    }

    ProgramReturnCode steganography_program(const boost_po::variables_map& var_map)
    {
        const auto algo_name = to_lowercase(var_map.count(ALGO_OPTION) != 0
                ? var_map[ALGO_OPTION].as<std::string>()
                : DEFAULT_STEGANOGRAPHY_ALGO_NAME);
        if (algo_name == "lsb") {

        } else if (algo_name == "patchwork") {
            const auto raw_key = var_map[KEY_OPTION].as<std::string>();
        } else {
            return ProgramReturnCode::ALGORITHM_NOT_FOUND;
        }
    }

    ProgramPtr get_program(const boost_po::variables_map& var_map)
    {
        const auto is_hash = var_map.count(HASH_OPTION) != 0;
        const auto is_encryption = var_map.count(ENCRYPT_OPTION) != 0;
        const auto is_decryption = var_map.count(DECRYPT_OPTION) != 0;
        const auto is_steganography = var_map.count(STEGANOGRAPHY_OPTION) != 0;
        const auto is_hmac = var_map.count(HMAC_OPTION) != 0
            || var_map.count(INVALIDATE_HMAC_OPTION) != 0
            || var_map.count(REMOVE_HMAC_OPTION) != 0;

        if (is_encryption || is_decryption) {
            return encryption_program;
        } else if (is_steganography) {
            return steganography_program;
        } else if (is_hash) {
            return hash_program;
        } else if (is_hmac) {
            return hmac_program;
        } else {
            assert(false);
        }
    }
}


int main(int argc, char* argv[])
{
    const auto expected_opts_var_map = scrypt::parse_arguments(argc, argv);
    if (not expected_opts_var_map) {
        scrypt::print_arguments_error(expected_opts_var_map.error());
        return to_underlying(scrypt::ProgramReturnCode::ERROR_DURING_ARGS_PARSING);
    }

    const auto program = scrypt::get_program(*expected_opts_var_map);
    const auto program_return_code = program(*expected_opts_var_map);
    if (program_return_code != scrypt::ProgramReturnCode::ALL_IS_GOOD) {
        scrypt::print_program_error(
            scrypt::ReturnValExplanationMap.at(program_return_code)
        );
    }

    return to_underlying(program_return_code);
}



// void bytes_to_uint32(
//     std::vector<uint8_t> &data_in,
//     std::vector<uint32_t> &data_out
// )
// {
//     std::vector<uint8_t> bytes;
//     for (uint8_t i = 0; i < SHA256::DIGEST_SIZE / 4; i++) {
//         for (uint8_t j = 0; j < 4; j++) {
//             bytes.push_back(data_in[i * 4 + j]);
//         }
//         data_out.push_back(SHA256::bytes_to_int(bytes));
//         bytes.clear();
//     }
// }


// void encrypt_with_mac_codes(
//     std::vector<uint8_t> &data_in,
//     std::ofstream &output,
//     gword &key
// )
// {
//     std::vector<uint32_t> hash;
//     std::vector<uint8_t> byte_hash;
//     SHA256::get_hash(data_in, hash);
//     uint32_to_bytes(hash, byte_hash);
//     std::vector<uint8_t> encrypted_hash = AES::encrypt_data(byte_hash, key);

//     std::vector<uint8_t> data_out = AES::encrypt_data(data_in, key);

//     write_to_file(output, data_out);
//     write_to_file(output, encrypted_hash);

//     output.close();
// }


// void decrypt_with_mac_codes(
//     std::vector<uint8_t>& data_in,
//     std::ofstream& output,
//     gword& key
// )
// {
//     std::vector<uint8_t> encrypted_hash(48);
//     std::vector<uint8_t> decrypted_hash;
//     std::vector<uint32_t> old_hash;
//     for (int8_t i = 47; i > -1; i--) {
//         encrypted_hash[i] = data_in.back();
//         data_in.pop_back();
//     }
//     AES::decrypt_data(encrypted_hash, decrypted_hash, key);
//     bytes_to_uint32(decrypted_hash, old_hash);

//     std::vector<uint8_t> data_out;
//     std::vector<uint32_t> new_hash;
//     AES::decrypt_data(data_in, data_out, key);
//     SHA256::get_hash(data_out, new_hash);
//     for (uint8_t i = 0; i < 8; i++) {
//         if (old_hash[i] != new_hash[i]) {
//             std::cout << "Invalid data!" << std::endl;
//             break;
//         }
//     }
//     write_to_file(output, data_out);
//     output.close();
// }


// const auto& path_to_output_file = var_map[OUTPUT_FILE_OPTION].as<std::string>();
// const auto& raw_key = var_map[KEY_OPTION].as<std::string>();

// std::ifstream input(path_to_input_file);
// if (!input.good()) {
//     std::cout << "File not exist!" << std::endl;
//     return ProgramReturnValue::INPUT_FILE_NOT_EXISTS;
// }

// AES SPECIFIC CODE
// if (raw_key.length() > 32) {
//     std::cout << "Non standard key length." << std::endl;
//     return 4;
// }

// const auto& algo_name = var_map[ALGO_OPTION].as<std::string>();
// std::cout << algo_name << std::endl;

// AES SPECIFIC CODE
// gword key;
// for(const auto key_char: raw_key) {
//     key.push_back(key_char);
// }

// std::ofstream output(path_to_output_file);

// if (var_map.count(MAC_CODES_OPTION)) {
//     std::vector<uint8_t> data_in;
//     if (!read_file(input, data_in)) {
//         std::cout << "Read error!" << std::endl;
//         return 5;
//     } else if (var_map.count(ENCRYPT_OPTION)) {
//         encrypt_with_mac_codes(data_in, output, key);
//     } else {
//         decrypt_with_mac_codes(data_in, output, key);
//     }
// } else {
//     if (var_map.count(DECRYPT_OPTION)) {
//         AES::encrypt_file(input, output, key);
//     } else {
//         AES::decrypt_file(input, output, key);
//     }
// }
// return 0;
