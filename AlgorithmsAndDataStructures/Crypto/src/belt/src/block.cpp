#include "crypto/belt/block.hpp"
#include <algorithm>

namespace crypto
{
    namespace belt
    {
        Block::Block(std::vector<uint8_t>&& arr)
        {
            assert(arr.size() == size);
            _data = std::move(arr);
        }

        Block::Block(const std::vector<uint8_t>& arr)
        {
            assert(arr.size() == size);
            _data = arr;
        }

        Block::Block(std::initializer_list<uint8_t> list)
        {
            assert(list.size() == size);
            _data = list;
        }


        Block::Block(const std::vector<uint32_t>& vec)
        {
            initialize_data(vec);
        }

        Block::Block(std::initializer_list<uint32_t> list)
        {
            initialize_data(list);
        }

        Block::Block(uint32_t a, uint32_t b, uint32_t c, uint32_t d)
        {
            initialize_data({a, b, c, d});
        }

        void Block::initialize_data(const std::vector<uint32_t>& vec)
        {
            assert(vec.size() * sizeof(uint32_t) == size);
            for (auto word_idx = 0; word_idx < vec.size(); ++word_idx) {
                const auto bytes = split_bytes(vec[word_idx]);
                for (const auto byte: bytes) {
                    _data.push_back(byte);
                }
            }
        }


        Word Block::get_word(std::size_t idx) const
        {
            const auto word_idx = idx * sizeof(Word);

            return Word(
                concat_bytes(
                    _data[word_idx],
                    _data[word_idx + 1],
                    _data[word_idx + 2],
                    _data[word_idx + 3]
                )
            );
        }


        Block Block::operator^(const Block& block) const
        {
            std::vector<uint8_t> result(size);
            for (auto i = 0; i < size; ++i) {
                result[i] = _data[i] ^ block[i];
            }

            return result;
        }

        Block& Block::operator^=(const Block& block)
        {
            for (auto i = 0; i < size; ++i) {
                _data[i] ^= block[i];
            }

            return *this;
        }


        std::ostream& operator<<(std::ostream& os, const Block& block)
        {
            os << "Block(" << std::endl;
            for (auto i = 0; i < block.size / sizeof(Word); ++i) {
                os << block.get_word(i) << "," << std::endl;
            }
            os << ")" << std::endl;

            return os;
        }
    }
}
