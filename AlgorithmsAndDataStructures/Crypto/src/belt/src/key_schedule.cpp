#include "crypto/belt/key_schedule.hpp"

#include <bitset>
#include <array>
#include <tuple>
#include <cassert>
#include <vector>
#include <stdint.h>

namespace crypto
{
    namespace belt
    {
        std::vector<Word> regroup_bytes_into_words(
            const uint8_t* const bytes,
            const uint8_t length
        )
        {
            assert(length % 4 == 0);

            std::vector<Word> result;
            for (auto i = 0; i < length; i += 4) {
                result.emplace_back(
                    concat_bytes(
                        bytes[i],
                        bytes[i + 1],
                        bytes[i + 2],
                        bytes[i + 3]
                    )
                );
            }

            return result;
        }


        std::optional<KeySchedule> KeySchedule::get_schedule(
            const uint8_t* const raw_key,
            const uint8_t length
        )
        {
            switch (length)
            {
            case 16: // 128 bits
            {
                auto result = regroup_bytes_into_words(raw_key, length);
                for (auto i = 0; i < 4; ++i) { // 5..8
                    result.push_back(result[i]);
                }

                return KeySchedule(result);
            }
            case 24: // 192 bits
            {
                auto result = regroup_bytes_into_words(raw_key, length);
                result.push_back(result[0] ^ result[1] ^ result[2]); // 7
                result.push_back(result[3] ^ result[4] ^ result[5]); // 8

                return KeySchedule(result);
            }
            case 32: // 256 bits
            {
                auto result = regroup_bytes_into_words(raw_key, length);

                return KeySchedule(result);
            }
            default:
                return std::optional<KeySchedule>();
            }
        }

        std::optional<KeySchedule> KeySchedule::get_schedule(
            const std::vector<uint8_t>& raw_key
        )
        {
            return get_schedule(raw_key.data(), raw_key.size());
        }

        std::optional<KeySchedule> KeySchedule::get_schedule(
            const std::string& raw_key
        )
        {
            return get_schedule(
                reinterpret_cast<const unsigned char*>(raw_key.c_str()),
                raw_key.length()
            );
        }
    }
}
