#include "crypto/belt/belt.hpp"

#include <bitset>
#include <array>
#include <cstdlib>
#include <tuple>
#include <vector>
#include <stdint.h>
#include <limits>

#include "crypto/utils/bit_helpers.hpp"
#include "crypto/utils/exceptions.hpp"

#include "crypto/belt/block_iter.hpp"
#include "crypto/belt/key_schedule.hpp"


namespace crypto
{
    namespace belt
    {
        using namespace crypto::utils::exceptions;
        using namespace crypto::utils::bit_helpers;


        Block BelT::generate_initialization_block()
        {
            std::vector<uint8_t> result;
            result.resize(Block::size);

            for (auto i = 0; i < Block::size; ++i) {
                result[i] = rand() % std::numeric_limits<uint8_t>::max();
            }

            return Block(std::move(result));
        }


        void BelT::append_to_vec(
            const Block& block,
            std::back_insert_iterator<std::vector<uint8_t>> it
        )
        {
            for (auto i = 0; i < block.size; ++i) {
                it = block[i];
            }
        }


        void BelT::append_to_vec(
            const Block& block,
            const uint8_t count_of_bytes,
            std::back_insert_iterator<std::vector<uint8_t>> it
        )
        {
            for (auto i = 0; i < count_of_bytes; ++i) {
                it = block[i];
            }
        }


        std::vector<uint8_t> xor_vec(
            const std::vector<uint8_t>& a,
            const std::vector<uint8_t>& b
        )
        {
            assert(a.size() == b.size());

            std::vector<uint8_t> result(a.size());
            for (auto i = 0; i < a.size(); ++i) {
                result[i] = a[i] ^ b[i];
            }

            return result;
        }


        Block BelT::encrypt_block(
            const Block& block,
            const KeySchedule& k
        )
        {
            Word a = block.get_word(0);
            Word b = block.get_word(1);
            Word c = block.get_word(2);
            Word d = block.get_word(3);
            Word e(0);

            for (uint32_t i = 1; i <= 8; ++i) {
                b ^= G_5 (a +     k[7 * i - 6]);           // 1
                c ^= G_21(d +     k[7 * i - 5]);           // 2
                a -= G_13(b +     k[7 * i - 4]);           // 3
                e =  G_21(b + c + k[7 * i - 3]) ^ i;       // 4
                b += e;                                    // 5
                c -= e;                                    // 6
                d += G_13(c +     k[7 * i - 2]);           // 7
                b ^= G_21(a +     k[7 * i - 1]);           // 8
                c ^= G_5 (d +     k[7 * i]);               // 9
                std::swap(a, b);                           // 10
                std::swap(c, d);                           // 11
                std::swap(b, c);                           // 12
            }

            return Block(
                b.get_data(),
                d.get_data(),
                a.get_data(),
                c.get_data()
            );
        }

        std::vector<uint8_t> BelT::encrypt_data_ecb(
            const std::vector<uint8_t>& data,
            const KeySchedule& key_schedule
        )
        {
            std::vector<uint8_t> result;
            result.reserve(data.size());

            BlockIter block_iter(data);
            auto prev_encrypted_block = encrypt_block(*block_iter.get_cur(), key_schedule);

            while (true) {
                if (not block_iter.has_next()) {
                    append_to_vec(prev_encrypted_block, std::back_inserter(result));
                    break;
                }

                auto block_expected = block_iter.next();
                if (block_expected) {
                    append_to_vec(prev_encrypted_block, std::back_inserter(result));
                    prev_encrypted_block = encrypt_block(*block_expected, key_schedule);
                    continue;
                }

                {
                    auto& just_bytes = block_expected.error();
                    const auto count_of_exists_bytes = just_bytes.size();
                    for (auto i = count_of_exists_bytes; i < Block::size; ++i ) {
                        just_bytes.push_back(prev_encrypted_block[i]);
                    }
                    const auto encrypted_block = encrypt_block(just_bytes, key_schedule);

                    append_to_vec(encrypted_block, std::back_inserter(result));
                    append_to_vec(
                        prev_encrypted_block,
                        count_of_exists_bytes,
                        std::back_inserter(result)
                    );
                    break;
                }
            }

            return result;
        }

        std::vector<uint8_t> BelT::encrypt_data_cbc(
            const std::vector<uint8_t>& data,
            const KeySchedule& key_schedule
        )
        {
            std::vector<uint8_t> result;
            result.reserve(data.size() + Block::size); // initialization block

            BlockIter block_iter(data);
            Block prev_prev_y;
            Block prev_prev_x;

            Block prev_y = generate_initialization_block();
            Block prev_x;

            while (true) {
                auto x_expected = block_iter.get_cur();
                if (not x_expected)
                {
                    auto y_and_r = encrypt_block(
                        prev_x ^ prev_prev_y,
                        key_schedule
                    );

                    std::vector<uint8_t> x_bytes = x_expected.error();
                    const auto exists_bytes_count = x_bytes.size();
                    auto xored_bytes = xor_vec(
                        x_bytes,
                        y_and_r(0, exists_bytes_count)
                    );
                    for (auto i = exists_bytes_count; i < Block::size; ++i) {
                        xored_bytes.push_back(y_and_r[i]);
                    }

                    prev_y = encrypt_block(xored_bytes, key_schedule);
                    append_to_vec(prev_y, std::back_inserter(result));

                    append_to_vec(
                        y_and_r,
                        exists_bytes_count,
                        std::back_inserter(result)
                    );
                    break;
                }

                {
                    append_to_vec(prev_y, std::back_inserter(result));

                    prev_prev_y = prev_y;
                    prev_prev_x = prev_x;

                    auto& x = *x_expected;
                    prev_y = encrypt_block(x ^ prev_y, key_schedule);
                    prev_x = x;
                }

                if (not block_iter.has_next()) {
                    append_to_vec(prev_y, std::back_inserter(result));
                    break;
                }
                block_iter.next();
            }

            return result;
        }

        std::vector<uint8_t> BelT::encrypt_data(
            const std::vector<uint8_t>& data,
            const KeySchedule& key_schedule,
            EncryptionMode encryption_mode
        )
        {
            if (encryption_mode == EncryptionMode::ECB) {
                return encrypt_data_ecb(data, key_schedule);
            } else if (encryption_mode == EncryptionMode::CBC) {
                return encrypt_data_cbc(data, key_schedule);
            } else {
                throw NotImplementedException();
            }
        }


        Block BelT::decrypt_block(
            const Block& block,
            const KeySchedule& k
        )
        {
            Word a = block.get_word(0);
            Word b = block.get_word(1);
            Word c = block.get_word(2);
            Word d = block.get_word(3);
            Word e(0);

            for (uint32_t i = 8; i >= 1; --i) {
                b ^= G_5 (a +     k[7 * i]);         // 1
                c ^= G_21(d +     k[7 * i - 1]);     // 2
                a -= G_13(b +     k[7 * i - 2]);     // 3
                e =  G_21(b + c + k[7 * i - 3]) ^ i; // 4
                b += e;                              // 5
                c -= e;                              // 6
                d += G_13(c +     k[7 * i - 4]);     // 7
                b ^= G_21(a +     k[7 * i - 5]);     // 8
                c ^= G_5 (d +     k[7 * i - 6]);     // 9
                std::swap(a, b);                     // 10
                std::swap(c, d);                     // 11
                std::swap(a, d);                     // 12
            }

            return Block(
                c.get_data(),
                a.get_data(),
                d.get_data(),
                b.get_data()
            );
        }

        std::vector<uint8_t> BelT::decrypt_data_ecb(
            const std::vector<uint8_t> &data,
            const KeySchedule& key_schedule
        )
        {
            std::vector<uint8_t> result;
            result.reserve(data.size());

            BlockIter block_iter(data);
            auto prev_decrypted_block = decrypt_block(*block_iter.get_cur(), key_schedule);

            while (true) {
                if (not block_iter.has_next()) {
                    append_to_vec(prev_decrypted_block, std::back_inserter(result));
                    break;
                }

                auto block_expected = block_iter.next();
                if (block_expected) {
                    append_to_vec(prev_decrypted_block, std::back_inserter(result));
                    prev_decrypted_block = decrypt_block(*block_expected, key_schedule);
                    continue;
                }

                const auto count_of_exists_bytes = block_expected.error().size();
                {
                    auto& just_bytes = block_expected.error();
                    for (auto i = count_of_exists_bytes; i < Block::size; ++i) {
                        just_bytes.push_back(prev_decrypted_block[i]);
                    }
                    const auto decrypted_block = decrypt_block(just_bytes, key_schedule);

                    append_to_vec(decrypted_block, std::back_inserter(result));
                    append_to_vec(
                        prev_decrypted_block,
                        count_of_exists_bytes,
                        std::back_inserter(result)
                    );
                    break;
                }
            }

            return result;
        }

        std::vector<uint8_t> BelT::decrypt_data_cbc(
            const std::vector<uint8_t> &data,
            const KeySchedule& key_schedule
        )
        {
            std::vector<uint8_t> result;
            result.reserve(data.size() + Block::size); // initialization block

            BlockIter block_iter(data);
            Block prev_prev_y;
            Block prev_prev_x;

            bool is_init_block = true;
            Block prev_y = *block_iter.get_cur(); // initialization block
            block_iter.next();
            Block prev_x = prev_y;

            while (true) {
                auto x_expected = block_iter.get_cur();
                if (not x_expected)
                {
                    std::vector<uint8_t> x_bytes = x_expected.error();
                    const auto exists_bytes_count = x_bytes.size();
                    for (auto i = exists_bytes_count; i < Block::size; ++i) {
                        x_bytes.push_back(0);
                    }
                    auto y_and_r = decrypt_block(prev_x, key_schedule) ^ Block(x_bytes);

                    for (auto i = exists_bytes_count; i < Block::size; ++i) {
                        x_bytes[i] = y_and_r[i];
                    }
                    prev_y = decrypt_block(x_bytes, key_schedule) ^ prev_prev_x;
                    append_to_vec(prev_y, std::back_inserter(result));

                    append_to_vec(
                        y_and_r,
                        exists_bytes_count,
                        std::back_inserter(result)
                    );
                    break;
                }

                {
                    if (not is_init_block) {
                        append_to_vec(prev_y, std::back_inserter(result));
                    }
                    is_init_block = false;

                    prev_prev_y = prev_y;
                    prev_prev_x = prev_x;

                    auto& x = *x_expected;
                    prev_y = decrypt_block(x, key_schedule) ^ prev_x;
                    prev_x = x;
                }

                if (not block_iter.has_next()) {
                    append_to_vec(prev_y, std::back_inserter(result));
                    break;
                }
                block_iter.next();
            }

            return result;
        }

        std::vector<uint8_t> BelT::decrypt_data(
            const std::vector<uint8_t>& data,
            const KeySchedule& key_schedule,
            EncryptionMode encryption_mode
        )
        {
            if (encryption_mode == EncryptionMode::ECB) {
                return decrypt_data_ecb(data, key_schedule);
            } else if (encryption_mode == EncryptionMode::CBC) {
                return decrypt_data_cbc(data, key_schedule);
            } else {
                throw NotImplementedException();
            }
        }
    }
}
