#include "crypto/belt/word.hpp"
#include <cassert>

namespace crypto
{
    namespace belt
    {
        Octet Word::get_octet(OctetIdx octet_idx) const
        {
            const auto shifts_count = (to_underlying(OctetIdx::LAST) - to_underlying(octet_idx)) * 8;

            return (_data >> shifts_count) & 0xFF;
        }


        Word Word::rot_hi(uint32_t rot_count) const
        {
            // assumes width is a power of 2.
            const uint32_t mask = (CHAR_BIT * sizeof(_data) - 1);
            assert((rot_count <= mask) && "Rotate by type width or more");

            rot_count &= mask;
            return Word(
                reverse_byte_order(
                    (_data << rot_count) | (_data >> ((-rot_count) & mask))
                )
            );
        }


        Word Word::g_func(const uint8_t rot_count) const
        {
            const auto u1 = get_octet(OctetIdx::FIRST).get_h_subst();
            const auto u2 = get_octet(OctetIdx::SECOND).get_h_subst();
            const auto u3 = get_octet(OctetIdx::THIRD).get_h_subst();
            const auto u4 = get_octet(OctetIdx::FOURTH).get_h_subst();
            const Word substituted_word(u1, u2, u3, u4);

            return substituted_word.rot_hi(rot_count);
        }


        std::ostream& operator<<(std::ostream& os, const Word word)
        {
            os << std::to_string(reverse_byte_order(word._data));

            return os;
        }
    }
}
