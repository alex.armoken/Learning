#include <stdint.h>
#include <vector>
#include <cassert>

#include <optional>

#include "word.hpp"

#if !(defined(CRYPTO_BELT_KEY_SCHEDULE_H))
#define CRYPTO_BELT_KEY_SCHEDULE_H
namespace crypto
{
    namespace belt
    {
        class KeySchedule
        {
        public:
            KeySchedule() = delete;

            static std::optional<KeySchedule> get_schedule(
                const uint8_t* const raw_key,
                const uint8_t length
            );
            static std::optional<KeySchedule> get_schedule(
                const std::vector<uint8_t>& raw_key
            );
            static std::optional<KeySchedule> get_schedule(
                const std::string& raw_key
            );

            Word operator[](const uint32_t key_num) const
            {
                assert(key_num != 0);
                const auto key_idx = (key_num % 8 == 0) ? 7 : (key_num % 8 - 1);

                return _data[key_idx];
            }

        private:
            std::vector<Word> _data;

            KeySchedule(const std::vector<Word>& key)
                : _data(key)
            {
            }
        };
    }
}
#endif // CRYPTO_BELT_KEY_SCHEDULE_H
