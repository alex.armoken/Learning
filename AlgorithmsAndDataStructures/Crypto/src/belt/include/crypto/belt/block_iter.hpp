#include <stdint.h>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <array>
#include <vector>
#include <bitset>
#include <string>

#include <expected.hpp>
#include <optional>

#include "crypto/utils/constants.hpp"

#include "belt.hpp"


#if !(defined(CRYPTO_BELT_BLOCK_ITER_H))
#define CRYPTO_BELT_BLOCK_ITER_H
namespace crypto
{
    namespace belt
    {
        using nonstd::expected;
        using nonstd::make_unexpected;

        class BlockIter
        {
        public:
            BlockIter(const std::vector<uint8_t>& data)
            {
                _cur_it = data.cbegin();
                _end_it = data.cend();

                next();
            }

            expected<Block, std::vector<uint8_t>> next()
            {
                _buffer = loadBlock();

                return get_cur();
            }

            bool has_next()
            {
                return _cur_it != _end_it;
            }

            expected<Block, std::vector<uint8_t>> get_cur()
            {
                if (_buffer.size() == Block::size) {
                    return Block(_buffer);
                }

                return make_unexpected(_buffer);
            }

        private:
            std::vector<uint8_t>::const_iterator _cur_it;
            std::vector<uint8_t>::const_iterator _end_it;

            bool _is_was_last = false;
            std::vector<uint8_t> _buffer;


            std::vector<uint8_t> loadBlock()
            {
                std::vector<uint8_t> result;
                result.reserve(Block::size);

                for (auto byte_idx = 0; byte_idx < Block::size; ++byte_idx)
                {
                    if (_cur_it == _end_it) {
                        break;
                    }
                    result.push_back(*_cur_it);
                    _cur_it++;
                }

                return result;
            }
        };
    }
}
#endif // CRYPTO_BELT_BLOCK_ITER_H
