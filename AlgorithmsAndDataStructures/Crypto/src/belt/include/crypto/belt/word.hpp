#include <iostream>
#include <limits.h>

#include "crypto/utils/enum_helpers.hpp"
#include "crypto/utils/constants.hpp"
#include "crypto/utils/bit_helpers.hpp"

#include "octet.hpp"


#if !(defined(CRYPTO_BELT_WORD_H))
#define CRYPTO_BELT_WORD_H
namespace crypto
{
    namespace belt
    {
        using namespace utils;
        using namespace utils::bit_helpers;
        using namespace utils::enum_helpers;

        enum class OctetIdx
        {
            FIRST = 0,
            SECOND = 1,
            THIRD = 2,
            FOURTH = 3,
            LAST = FOURTH,
        };

        // Class for all operations in belt encryption algorithm
        // makes all manipulations as it inner data in little-endian format
        class Word
        {
        public:
            explicit Word(uint32_t big_endian_data)
            {
                _data = reverse_byte_order(big_endian_data);
            }

            Word(const Word& word)
            {
                _data = word._data;
            }

            ~Word() = default;

            operator uint32_t() const { return _data; }
            uint32_t get_data() const { return reverse_byte_order(_data); }

            Octet operator[](OctetIdx octet_idx) const
            {
                return get_octet(octet_idx);
            }

            // Cycle left shift
            Word rot_hi(uint32_t rot_count) const;
            Word g_func(const uint8_t rot_count) const;

            Word operator^(Word word) const
            {
                return Word(reverse_byte_order(_data ^ word._data));
            }
            Word& operator^=(Word word)
            {
                _data ^= word._data;

                return *this;
            }
            Word operator^(uint32_t word) const
            {
                return Word(reverse_byte_order(_data ^ word));
            }
            Word& operator^=(uint32_t word)
            {
                _data ^= word;

                return *this;
            }


            Word operator+(Word word) const
            {
                return Word(reverse_byte_order(_data + word._data));
            }
            Word& operator+=(Word word)
            {
                _data += word._data;

                return *this;
            }
            Word operator+(uint32_t word) const
            {
                return Word(reverse_byte_order(_data + word));
            }
            Word& operator+=(uint32_t word)
            {
                _data += word;

                return *this;
            }


            Word operator-(Word word) const
            {
                return Word(reverse_byte_order(_data - word._data));
            }
            Word& operator-=(Word word)
            {
                _data -= word._data;

                return *this;
            }
            Word operator-(uint32_t word) const
            {
                return Word(reverse_byte_order(_data - word));
            }
            Word& operator-=(uint32_t word)
            {
                _data -= word;

                return *this;
            }


            bool operator==(Word word) const
            {
                return _data == word._data;
            }
            bool operator!=(Word word) const
            {
                return _data != word._data;
            }

            friend std::ostream& operator<<(
                std::ostream& os,
                const Word word
            );

        private:
            uint32_t _data = 0;

            Word(Octet u1, Octet u2, Octet u3, Octet u4)
            {
                _data = static_cast<uint32_t>(u1) << 24
                    | static_cast<uint32_t>(u2) << 16
                    | static_cast<uint32_t>(u3) << 8
                    | static_cast<uint32_t>(u4);
            }

            Octet get_octet(OctetIdx octet_idx) const;
        };
    }
}
#endif // CRYPTO_BELT_WORD_H
