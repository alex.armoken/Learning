#include <iostream>
#include <limits.h>
#include <array>
#include <vector>
#include <cassert>
#include <initializer_list>

#include "word.hpp"


#if !(defined(CRYPTO_BELT_BLOCK_H))
#define CRYPTO_BELT_BLOCK_H
namespace crypto
{
    namespace belt
    {
        class Block
        {
        public:
            Block() : _data(size, 0)
            {
            }

            Block(std::vector<uint8_t>&& arr);
            Block(const std::vector<uint8_t>& arr);
            Block(std::initializer_list<uint8_t> list);

            explicit Block(const std::vector<uint32_t>& vec);
            explicit Block(std::initializer_list<uint32_t> list);
            explicit Block(uint32_t a, uint32_t b, uint32_t c, uint32_t d);

            Block(const Block& block) = default;
            Block& operator=(const Block&) = default;

            static const std::size_t size = 4 * sizeof(Word);

            std::vector<uint8_t> operator()(uint8_t start_idx, uint8_t count) const
            {
                std::vector<uint8_t> result;
                for (auto i = start_idx; i < start_idx + count; ++i) {
                    result.push_back(_data[i]);
                }

                return result;
            }

            Word get_word(std::size_t idx) const;
            uint8_t& operator[](std::size_t idx)
            {
                return _data[idx];
            }
            const uint8_t& operator[](std::size_t idx) const
            {
                return _data[idx];
            }

            Block operator^(const Block& block) const;
            Block& operator^=(const Block& block);
            bool operator==(const Block& block) const
            {
                return _data == block._data;
            }

            friend std::ostream& operator<<(
                std::ostream& os,
                const Block& word
            );

        private:
            std::vector<uint8_t> _data;

            void initialize_data(const std::vector<uint32_t>& vec);
        };
    }
}
#endif // CRYPTO_BELT_BLOCK_H
