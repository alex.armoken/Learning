#include <stdint.h>
#include <algorithm>
#include <iomanip>
#include <iostream>
#include <array>
#include <vector>
#include <bitset>
#include <string>

#include <optional>

#include "crypto/utils/bit_block.hpp"
#include "crypto/utils/constants.hpp"
#include "crypto/utils/encryption_mode.hpp"

#include "word.hpp"
#include "block.hpp"

#if !(defined(CRYPTO_BELT_H))
#define CRYPTO_BELT_H
namespace crypto::belt
{
class KeySchedule;

class BelT
{
public:
    BelT() = delete;

    static void append_to_vec(
        const Block& block,
        std::back_insert_iterator<std::vector<uint8_t>> it
    );
    static void append_to_vec(
        const Block& block,
        uint8_t count_of_bytes,
        std::back_insert_iterator<std::vector<uint8_t>> it);

    static Word G_5(Word word) { return word.g_func(05); }
    static Word G_13(Word word) { return word.g_func(13); }
    static Word G_21(Word word) { return word.g_func(21); }

    static Block generate_initialization_block();

    static Block encrypt_block(
        const Block& block,
        const KeySchedule& key_schedule
    );
    static std::vector<uint8_t> encrypt_data_ecb(
        const std::vector<uint8_t>& data,
        const KeySchedule& key_schedule
    );
    static std::vector<uint8_t> encrypt_data_cbc(
        const std::vector<uint8_t>& data,
        const KeySchedule& key_schedule
    );
    static std::vector<uint8_t> encrypt_data(
        const std::vector<uint8_t>& data,
        const KeySchedule& key_schedule,
        EncryptionMode encryption_mode
    );

    static Block decrypt_block(
        const Block& block,
        const KeySchedule& key_schedule
    );
    static std::vector<uint8_t> decrypt_data_ecb(
        const std::vector<uint8_t>& data,
        const KeySchedule& key_schedule
    );
    static std::vector<uint8_t> decrypt_data_cbc(
        const std::vector<uint8_t>& data,
        const KeySchedule& key_schedule
    );
    static std::vector<uint8_t> decrypt_data(
        const std::vector<uint8_t>& data,
        const KeySchedule& key_schedule,
        EncryptionMode encryption_mode
    );
};
}
#endif // CRYPTO_BELT_H
