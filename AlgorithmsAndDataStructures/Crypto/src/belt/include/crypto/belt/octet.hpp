#include <vector>
#include <stdint.h>


#if !(defined(CRYPTO_BELT_OCTET_H))
#define CRYPTO_BELT_OCTET_H
namespace crypto
{
    namespace belt
    {
        class Octet
        {
        public:
            Octet(uint8_t data): _data(data) {}
            ~Octet() = default;

            uint8_t get_data() { return _data; };
            operator uint8_t() const { return _data; }
            operator uint32_t() const { return _data; }

            Octet get_h_subst() const;

            // Row idx in H substitution table
            uint8_t get_left_tetrad() const { return _data >> 4; };
            // Column idx in H substitution table
            uint8_t get_right_tetrad() const { return _data & 0b00001111; };

            bool operator==(Octet octet) const { return _data == octet._data; }

        private:
            uint8_t _data = 0;
        };
    }
}
#endif // CRYPTO_BELT_OCTET_H
