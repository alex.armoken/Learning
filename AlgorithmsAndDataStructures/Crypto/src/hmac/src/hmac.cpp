#include "crypto/hmac/hmac.hpp"

#include "crypto/sha256/sha256.hpp"
#include "crypto/utils/byte_helpers.hpp"
#include "crypto/utils/vector_helpers.hpp"

namespace crypto::hmac
{
    using namespace crypto::utils::byte_helpers;
    using namespace crypto::utils::vector_helpers;

    std::vector<uint8_t> HMAC::sha256_hmac(
        std::vector<uint8_t> key,
        std::vector<uint8_t> data
    )
    {
        constexpr auto BLOCK_SIZE = 64; // Bytes
        constexpr auto KEY_LENGTH = 32; // Bytes

        if (key.size() > BLOCK_SIZE) {
            std::vector<uint32_t> hash;
            SHA256::get_hash(key, hash);

            key = int_to_bytes(hash);
        }

        if (key.size() < BLOCK_SIZE) {
            for (auto i = key.size(); i < BLOCK_SIZE; ++i) {
                key.push_back(0);
            }
        }
        const std::vector<uint8_t> ipad(BLOCK_SIZE, 0x36);
        const std::vector<uint8_t> opad(BLOCK_SIZE, 0x5C);

        auto ikeypad = xor_vec(ipad, key);
        auto okeypad = xor_vec(opad, key);

        std::vector<uint8_t> data_for_hash_calc(data.begin(), data.end());
        for (const auto byte: ikeypad) {
            data_for_hash_calc.insert(data_for_hash_calc.begin(), byte);
        }
        std::vector<uint32_t> first_hash;
        SHA256::get_hash(data_for_hash_calc, first_hash);
        const auto first_byte_hash = int_to_bytes(first_hash);

        for (const auto byte: first_byte_hash) {
            okeypad.push_back(byte);
        }
        std::vector<uint32_t> second_hash;
        SHA256::get_hash(data, second_hash);
        const auto second_byte_hash = int_to_bytes(second_hash);

        std::vector<uint8_t> result_data(data.begin(), data.end());
        for (const auto byte: second_byte_hash) {
            result_data.push_back(byte);
        }

        return result_data;
    }


    const auto HMAC_SIZE = 256 / 8; // Bytes
    bool HMAC::invalidate_sha256_hmac(
        std::vector<uint8_t> key,
        std::vector<uint8_t> data
    )
    {
        const std::vector<uint8_t> data_hmac(
            data.begin() + (data.size() - HMAC_SIZE),
            data.end()
        );
        std::cout << "asdasd" << std::endl;

        const std::vector<uint8_t> data_only(data.begin(), data.end() - HMAC_SIZE);
        const auto new_hmac_data = sha256_hmac(key, data_only);
        const std::vector<uint8_t> new_hmac(
            new_hmac_data.begin() + (new_hmac_data.size() - HMAC_SIZE),
            new_hmac_data.end()
        );

        return data_hmac == new_hmac;
    }


    void HMAC::remove_sha256_hmac(std::vector<uint8_t>& data)
    {
        data.resize(data.size() - HMAC_SIZE);
    }
}
