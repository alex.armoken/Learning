#include <vector>
#include <bitset>
#include <string>
#include <cstring>
#include <fstream>
#include <iostream>
#include <stdint.h>

#if !(defined(CRYPTO_HMAC_H))
#define CRYPTO_HMAC_H
namespace crypto::hmac
{
class HMAC
{
public:
    HMAC() = delete;

    static std::vector<uint8_t> sha256_hmac(
        std::vector<uint8_t> key,
        std::vector<uint8_t> data
    );

    static bool invalidate_sha256_hmac(
        std::vector<uint8_t> key,
        std::vector<uint8_t> data
    );

    static void remove_sha256_hmac(std::vector<uint8_t>& data);
};
} // namespace crypto::hmac
#endif // CRYPTO_HMAC_H
