#include "crypto/aes/aes.hpp"

namespace crypto::aes
{
    const gblock Rcon = {
        { 0x00, 0x00, 0x00, 0x00 },
        { 0x01, 0x00, 0x00, 0x00 },
        { 0x02, 0x00, 0x00, 0x00 },
        { 0x04, 0x00, 0x00, 0x00 },
        { 0x08, 0x00, 0x00, 0x00 },
        { 0x10, 0x00, 0x00, 0x00 },
        { 0x20, 0x00, 0x00, 0x00 },
        { 0x40, 0x00, 0x00, 0x00 },
        { 0x80, 0x00, 0x00, 0x00 },
        { 0x1b, 0x00, 0x00, 0x00 },
        { 0x36, 0x00, 0x00, 0x00 }
    };

    void AES::cycle_left_shift(gword& arr)
    {
        Galois256El tmp = arr.front();
        copy(arr.begin() + 1, arr.end(), arr.begin());
        arr.back() = tmp;
    }

    void AES::cycle_right_shift(gword& arr)
    {
        Galois256El tmp = arr.back();
        arr.pop_back();
        arr.insert(arr.begin(), tmp);
    }

    void AES::shift_rows(gblock& state)
    {
        for (uint32_t i = 0; i < AES::BYTES_IN_WORD; i++) {
            for (uint32_t j = 0; j < i; j++) {
                auto tmp = state.front()[i];
                for (uint32_t k = 0; k < AES::COLUMNS_COUNT - 1; k++) {
                    state[k][i] = state[k + 1][i];
                }
                state.back()[i] = tmp;
            }
        }
    }

    void AES::inverse_shift_rows(gblock& state)
    {
        for (int32_t i = 0; i < AES::BYTES_IN_WORD; i++) {
            for (int32_t j = 0; j < i; j++) {
                auto tmp = state.back()[i];
                for (int32_t k = AES::COLUMNS_COUNT - 1; k != 0; k--) {
                    state[k][i] = state[k - 1][i];
                }
                state.front()[i] = tmp;
            }
        }
    }

    void AES::substitute_word(gword& word)
    {
        std::for_each(word.begin(), word.end(),
                      [](Galois256El& a) { a.substitute(); });
    }

    void AES::inverse_substitute_word(gword& word)
    {
        std::for_each(word.begin(), word.end(),
                      [](Galois256El& a) { a.inverse_substitute(); });
    }

    void AES::substitute_block(gblock& state)
    {
        for (auto it = state.begin(); it != state.end(); it++) {
            for (auto jt = it->begin(); jt != it->end(); jt++) {
                jt->substitute();
            }
        }
    }

    void AES::inverse_substitute_block(gblock& state)
    {
        for (auto it = state.begin(); it != state.end(); it++) {
            for (auto jt = it->begin(); jt != it->end(); jt++) {
                jt->inverse_substitute();
            }
        }
    }

    void AES::mix_columns(gblock& state)
    {
        for (auto it = state.begin(); it != state.end(); it++) {
            auto s0 = it->at(0) * 0x02 + it->at(1) * 0x03 + it->at(2) + it->at(3);
            auto s1 = it->at(0) + it->at(1) * 0x02 + it->at(2) * 0x03 + it->at(3);
            auto s2 = it->at(0) + it->at(1) + it->at(2) * 0x02 + it->at(3) * 0x03;
            auto s3 = it->at(0) * 0x03 + it->at(1) + it->at(2) + it->at(3) * 0x02;

            it->at(0) = s0;
            it->at(1) = s1;
            it->at(2) = s2;
            it->at(3) = s3;
        }
    }

    void AES::inverse_mix_columns(gblock& state)
    {
        for (auto it = state.begin(); it != state.end(); it++) {
            auto s0 = it->at(0) * 0x0e + it->at(1) * 0x0b + it->at(2) * 0x0d + it->at(3) * 0x09;
            auto s1 = it->at(0) * 0x09 + it->at(1) * 0x0e + it->at(2) * 0x0b + it->at(3) * 0x0d;
            auto s2 = it->at(0) * 0x0d + it->at(1) * 0x09 + it->at(2) * 0x0e + it->at(3) * 0x0b;
            auto s3 = it->at(0) * 0x0b + it->at(1) * 0x0d + it->at(2) * 0x09 + it->at(3) * 0x0e;

            it->at(0) = s0;
            it->at(1) = s1;
            it->at(2) = s2;
            it->at(3) = s3;
        }
    }

    void AES::add_word(gword& a, const gword& b)
    {
        for (uint32_t i = 0; i < a.size(); i++) {
            a.at(i) += b.at(i);
        }
    }

    void AES::set_opts(gword& key, AES::SessionOptions& o)
    {
        uint8_t size = key.size();
        o.words_in_key = (size <= 16) ? 4 : (size <= 24) ? 6 : 8;
        for (uint32_t i = 0; i < o.words_in_key * BYTES_IN_WORD - size; i++) {
            key.push_back(0x01);
        }

        o.cur_round = 0;
        o.rounds_count = (size <= 16) ? 10 : (size <= 24) ? 12 : 14;
        o.schedule_size = COLUMNS_COUNT * (o.rounds_count + 1);
    }

    void AES::fill_schedule_with_key(gword& key, AES::SessionOptions& o)
    {
        o.schedule = new gblock(o.schedule_size);
        for (uint32_t i = 0; i < o.schedule_size; i++) {
            o.schedule->at(i).resize(COLUMNS_COUNT);
        }

        auto it = key.begin();
        for (uint32_t i = 0; i < o.words_in_key; i++, it += 4) {
            copy(it, it + 4, o.schedule->at(i).begin());
        }
    }

    void AES::schedule_expansion(AES::SessionOptions& o)
    {
        for (uint32_t i = o.words_in_key; i < o.schedule->size(); i++) {
            std::vector<Galois256El> tmp(o.schedule->at(i - 1));
            if (i % o.words_in_key == 0) {
                cycle_left_shift(tmp);
                substitute_word(tmp);
                add_word(tmp, Rcon[i / o.words_in_key]);
            } else if (o.words_in_key > 6 and i % o.words_in_key == 4) {
                substitute_word(tmp);
            }
            auto word = o.schedule->at(i - o.words_in_key);
            std::copy(word.begin(), word.end(), o.schedule->at(i).begin());
            add_word(o.schedule->at(i), tmp);
        }
    }

    AES::SessionOptions AES::set_key(gword& key)
    {
        auto size = key.size();
        AES::SessionOptions o;
        if (size > 32) {
            std::cout << "Non standard key length." << std::endl;
            o.schedule = nullptr;
        } else {
            set_opts(key, o);
            fill_schedule_with_key(key, o);
            schedule_expansion(o);
        }
        return o;
    }

    void AES::add_round_key(gblock& state, AES::SessionOptions& o)
    {
        for (uint32_t i = 0; i < AES::COLUMNS_COUNT; i++) {
            for (uint32_t j = 0; j < AES::BYTES_IN_WORD; j++) {
                auto it = AES::COLUMNS_COUNT * o.cur_round + i;
                state[i][j] += o.schedule->at(it)[j];
            }
        }
    }

    void AES::run_one_round(gblock& b, AES::SessionOptions& o)
    {
        substitute_block(b);
        shift_rows(b);
        mix_columns(b);
        add_round_key(b, o);
    }

    void AES::encrypt_block(gblock& b, AES::SessionOptions& o)
    {
        o.cur_round = 0;
        add_round_key(b, o);
        o.cur_round++;
        while (o.cur_round < o.rounds_count) {
            run_one_round(b, o);
            o.cur_round++;
        }
        substitute_block(b);
        shift_rows(b);
        add_round_key(b, o);
    }

    void AES::run_one_inverse_round(gblock& b, AES::SessionOptions& o)
    {
        inverse_shift_rows(b);
        inverse_substitute_block(b);
        add_round_key(b, o);
        inverse_mix_columns(b);
    }

    void AES::decrypt_block(gblock& b, AES::SessionOptions& o)
    {
        o.cur_round = o.rounds_count;
        add_round_key(b, o);
        o.cur_round--;
        while (o.cur_round > 0) {
            run_one_inverse_round(b, o);
            o.cur_round--;
        }
        inverse_shift_rows(b);
        inverse_substitute_block(b);
        add_round_key(b, o);
    }

    AES::READING_STATE AES::read_word(std::ifstream& in, gword& w,
                                      bool& is_addition)
    {
        w.resize(AES::BYTES_IN_WORD);
        for (uint32_t j = 0; j < AES::BYTES_IN_WORD; j++) {
            if (!is_addition) {
                char buf = in.get();
                if (in.bad()) {
                    return ERROR;
                } else if (!in.eof()) {
                    w[j] = Galois256El(buf);
                } else if (!is_addition && in.eof()) {
                    is_addition = true;
                    w[j] = Galois256El(0x80);
                }
            } else {
                w[j] = Galois256El(0);
            }
        }
        return is_addition ? BUF_NOT_FULL : BUF_FULL;
    }


    AES::READING_STATE AES::read_word(
        std::vector<uint8_t>::const_iterator& iter,
        const std::vector<uint8_t>::const_iterator& end,
        gword& w,
        bool& is_end,
        bool& is_addition
    )
    {
        w.resize(AES::BYTES_IN_WORD);
        for (uint32_t j = 0; j < AES::BYTES_IN_WORD; j++) {
            if (!is_end) {
                if (iter + 1 >= end) {
                    is_end = true;
                }
                w[j] = Galois256El(*iter++);
            } else if (!is_addition) {
                is_addition = true;
                w[j] = Galois256El(0x80);
            } else {
                w[j] = Galois256El(0);
            }
        }
        return is_addition ? BUF_NOT_FULL : BUF_FULL;
    }


    AES::READING_STATE AES::read_block(std::ifstream& in, gblock& b)
    {
        bool is_addition = false;
        READING_STATE state = BUF_FULL;
        for (uint32_t i = 0; i < AES::COLUMNS_COUNT; i++) {
            if ((state = read_word(in, b[i], is_addition)) == ERROR) {
                break;
            }
        }
        return state;
    }


    AES::READING_STATE AES::read_block(
        std::vector<uint8_t>::const_iterator &iter,
        std::vector<uint8_t>::const_iterator &end,
        gblock& b
    )
    {
        bool is_end = false;
        bool is_addition = false;
        READING_STATE state = BUF_FULL;
        for (uint32_t i = 0; i < AES::COLUMNS_COUNT; i++) {
            state = read_word(iter, end, b[i], is_end, is_addition);
        }
        return state;
    }


    void AES::write_block(std::ofstream& out, gblock& b)
    {
        for (auto it = b.begin(); it != b.end(); it++) {
            for (auto jt = it->begin(); jt != it->end(); jt++) {
                out << *jt;
            }
        }
    }


    void AES::write_block(std::vector<uint8_t> &out, gblock& b)
    {
        for (auto it = b.begin(); it != b.end(); it++) {
            for (auto jt = it->begin(); jt != it->end(); jt++) {
                out.push_back(*jt);
            }
        }
    }


    void AES::encrypt_file(
        std::ifstream& in,
        std::ofstream& out,
        gword& key
    )
    {
        gblock buf(AES::COLUMNS_COUNT);
        READING_STATE state = BUF_FULL;
        AES::SessionOptions o;
        if ((o = set_key(key)).schedule != nullptr
            && (state = read_block(in, buf)) != ERROR) {
            do {
                encrypt_block(buf, o);
                write_block(out, buf);
                if (state == BUF_NOT_FULL) {
                    break;
                }
                state = read_block(in, buf);
            } while (state != ERROR);
            delete o.schedule;
        }
        in.close();
        out.close();
    }


    std::vector<uint8_t> AES::encrypt_data(
        const std::vector<uint8_t> &in,
        gword& key
    )
    {
        gblock buf(AES::COLUMNS_COUNT);
        READING_STATE state = BUF_FULL;
        AES::SessionOptions o;
        auto iter = in.begin();
        auto end = in.end();

        std::vector<uint8_t> out;
        if ((o = set_key(key)).schedule != nullptr) {
            state = read_block(iter, end, buf);
            do {
                encrypt_block(buf, o);
                write_block(out, buf);
                if (state == BUF_NOT_FULL) {
                    break;
                }
                state = read_block(iter, end, buf);
            } while (true);
            delete o.schedule;
        }

        return out;
    }

    void AES::write_block_without_indent_bytes(std::ofstream& out, gblock& b)
    {
        uint8_t word_num = 0;
        uint8_t byte_num = 0;
        const uint8_t padding_flag = 0x80;
        for (int8_t i = AES::COLUMNS_COUNT - 1; i > -1; i--) {
            for (int8_t j = AES::BYTES_IN_WORD - 1; j > -1; j--) {
                if (b[i][j] == padding_flag) {
                    word_num = i;
                    byte_num = j;
                    goto write_block_without_indent_bytes_stop;
                }
            }
        }
    write_block_without_indent_bytes_stop:

        for (uint8_t i = 0; i < AES::COLUMNS_COUNT; i++) {
            for (uint8_t j = 0; j < AES::BYTES_IN_WORD; j++) {
                if (word_num == i && byte_num == j) {
                    return;
                }
                out << b[i][j];
            }
        }
    }

    void AES::write_block_without_indent_bytes(
        std::vector<uint8_t>& out,
        gblock& b
    )
    {
        uint8_t word_num = 0;
        uint8_t byte_num = 0;
        const uint8_t padding_flag = 0x80;
        for (int8_t i = AES::COLUMNS_COUNT - 1; i > -1; i--) {
            for (int8_t j = AES::BYTES_IN_WORD - 1; j > -1; j--) {
                if (b[i][j] == padding_flag) {
                    word_num = i;
                    byte_num = j;
                    goto write_block_without_indent_bytes_stop;
                }
            }
        }
    write_block_without_indent_bytes_stop:

        for (uint8_t i = 0; i < AES::COLUMNS_COUNT; i++) {
            for (uint8_t j = 0; j < AES::BYTES_IN_WORD; j++) {
                if (word_num == i && byte_num == j) {
                    return;
                }
                out.push_back(b[i][j]);
            }
        }
    }

    bool AES::decrypt_file(
        std::ifstream& in,
        std::ofstream& out,
        gword& key
    )
    {
        AES::SessionOptions o;
        gblock buf1(AES::COLUMNS_COUNT);
        gblock buf2(AES::COLUMNS_COUNT);
        READING_STATE state1 = AES::BUF_FULL;
        READING_STATE state2 = AES::BUF_FULL;

        if ((o = set_key(key)).schedule != nullptr
            && (state1 = read_block(in, buf1)) != ERROR
            && state1 != BUF_NOT_FULL
            && (state2 = read_block(in, buf2)) != ERROR) {
            do {
                decrypt_block(buf1, o);
                if (state2 == BUF_NOT_FULL) {
                    write_block_without_indent_bytes(out, buf1);
                    break;
                }
                write_block(out, buf1);

                buf1.swap(buf2);
                state1 = state2;
                state2 = read_block(in, buf2);
            } while (state2 != ERROR);
            delete o.schedule;
        }

        in.close();
        out.close();
        return state1 != ERROR && state2 != ERROR ? true : false;
    }

    bool AES::decrypt_data(
        std::vector<uint8_t> &in,
        std::vector<uint8_t> &out,
        gword& key
    )
    {
        AES::SessionOptions o;
        gblock buf1(AES::COLUMNS_COUNT);
        gblock buf2(AES::COLUMNS_COUNT);
        READING_STATE state1 = AES::BUF_FULL;
        READING_STATE state2 = AES::BUF_FULL;

        auto iter = in.cbegin();
        auto end = in.cend();
        if ((o = set_key(key)).schedule != nullptr && state1 != BUF_NOT_FULL) {
            state1 = read_block(iter, end, buf1);
            state2 = read_block(iter, end, buf2);
            do {
                decrypt_block(buf1, o);
                if (state2 == BUF_NOT_FULL) {
                    write_block_without_indent_bytes(out, buf1);
                    break;
                }
                write_block(out, buf1);

                buf1.swap(buf2);
                state1 = state2;
                state2 = read_block(iter, end, buf2);
            } while (true);
            delete o.schedule;
        }

        return state1 != ERROR && state2 != ERROR ? true : false;
    }
}
