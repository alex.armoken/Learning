#include <vector>
#include <fstream>
#include <iostream>
#include <stdint.h>


namespace crypto::aes
{
#if !(defined(GALOIS_FIELD))
#define GALOIS_FIELD
    class Galois256El {
    private:
        uint8_t number;

    public:
        Galois256El(){};
        Galois256El(uint8_t num)
            : number(num){};

        uint8_t get_number() const;

        void substitute();
        void inverse_substitute();

        Galois256El inverse() const;
        Galois256El pow(uint8_t power) const;

        Galois256El operator+(int b) const;
        Galois256El operator+(Galois256El b) const;

        Galois256El operator*(int b) const;
        Galois256El operator*(Galois256El b) const;

        Galois256El operator+=(Galois256El b);
        Galois256El operator*=(Galois256El b);

        bool operator==(uint8_t num);
        bool operator==(Galois256El b);
        operator int() const { return number; };

        friend std::istream& operator>>(std::istream& s, Galois256El& a);
        friend std::ostream& operator<<(std::ostream& s, Galois256El& a);

        friend std::ifstream& operator>>(std::ifstream& s, Galois256El& a);
        friend std::ofstream& operator<<(std::ofstream& s, Galois256El& a);
    };

    using gword = std::vector<Galois256El>;
    using gblock = std::vector<gword>;

#endif // GALOIS_FIELD
}
