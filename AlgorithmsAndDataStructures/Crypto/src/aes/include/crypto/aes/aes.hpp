#include <algorithm>
#include <iomanip>
#include <iostream>
#include <stdint.h>
#include <vector>

#include <optional>

#include "galois_field.hpp"


namespace crypto::aes
{
#if !(defined(AES_H))
#define AES_H
    class AES {
    private:
        AES() {}

    public:
        enum KEY_LENGHT {
            BIT128 = 0,
            BIT192 = 1,
            BIT256 = 2,
        };

        enum READING_STATE {
            BUF_NOT_FULL,
            BUF_FULL,
            ERROR
        };

        struct SessionOptions {
            gblock* schedule;
            uint8_t cur_round;
            uint8_t rounds_count;
            uint32_t words_in_key;
            uint32_t schedule_size;
        };

        static const uint8_t BYTES_IN_WORD = 4;
        static const uint32_t COLUMNS_COUNT = 4;

        static void cycle_left_shift(gword& word);
        static void cycle_right_shift(gword& word);
        static void shift_rows(gblock& state);
        static void inverse_shift_rows(gblock& state);

        static void mix_columns(gblock& state);
        static void inverse_mix_columns(gblock& state);

        static void substitute_word(gword& word);
        static void substitute_block(gblock& state);

        static void inverse_substitute_word(gword& word);
        static void inverse_substitute_block(gblock& state);

        static void add_word(gword& a, const gword& b);

        static void set_opts(gword& key, SessionOptions& o);
        static void fill_schedule_with_key(gword& key, SessionOptions& o);
        static void schedule_expansion(SessionOptions& o);
        static SessionOptions set_key(gword& key);

        static void add_round_key(gblock& state, SessionOptions& o);
        static void run_one_round(gblock&b, SessionOptions& o);

        static READING_STATE read_word(
            std::ifstream& in,
            gword& w,
            bool& is_addition
        );
        static READING_STATE read_block(std::ifstream& in, gblock& b);

        static READING_STATE read_word(
            std::vector<uint8_t>::const_iterator& iter,
            const std::vector<uint8_t>::const_iterator& end,
            gword& w,
            bool& is_end,
            bool& is_addition
        );
        static READING_STATE read_block(
            std::vector<uint8_t>::const_iterator& iter,
            std::vector<uint8_t>::const_iterator& end,
            gblock& b
        );

        static void write_block(std::ofstream& out, gblock& b);
        static void write_block(std::vector<uint8_t> &out, gblock& b);
        static void write_block_without_indent_bytes(
            std::ofstream& out,
            gblock& b
        );
        static void write_block_without_indent_bytes(
            std::vector<uint8_t> &out,
            gblock& b
        );

        static void encrypt_block(gblock& b, SessionOptions& o);

        static void encrypt_file(
            std::ifstream& in,
            std::ofstream& out,
            gword& key
        );
        static std::vector<uint8_t> encrypt_data(
            const std::vector<uint8_t> &in,
            gword& key
        );

        static void run_one_inverse_round(gblock &b, AES::SessionOptions& o);
        static void decrypt_block(gblock& b, SessionOptions& o);

        static bool decrypt_file(
            std::ifstream& in,
            std::ofstream& out,
            gword& key
        );
        static bool decrypt_data(
            std::vector<uint8_t> &in,
            std::vector<uint8_t> &out,
            gword& key
        );
    };
#endif // AES_H
}
