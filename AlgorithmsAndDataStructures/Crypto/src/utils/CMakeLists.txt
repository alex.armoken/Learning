FILE(GLOB SOURCES_LIST "src/*.cpp")
ADD_LIBRARY(crypto_utils SHARED ${SOURCES_LIST})
TARGET_INCLUDE_DIRECTORIES(crypto_utils
    PUBLIC
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>
        ${Boost_INCLUDE_DIRS}
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/src
)
TARGET_LINK_LIBRARIES(crypto_utils
    stdc++fs # For std::filesystem in C++17 support

    Boost::headers
    ZLIB::ZLIB
    PNG::PNG JPEG::JPEG TIFF::TIFF
)
