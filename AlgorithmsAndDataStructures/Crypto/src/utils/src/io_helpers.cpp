#include "crypto/utils/io_helpers.hpp"

#include <iostream>

#define int_p_NULL                (int *)NULL
#include <boost/gil/extension/io/png/old.hpp>

#include <boost/gil/extension/io/jpeg/old.hpp>
#include <boost/gil/extension/io/tiff/old.hpp>

namespace crypto::utils::io_helpers
{
std::optional<std::vector<uint8_t>> read_file(std::ifstream& in)
{
    std::vector<uint8_t> data;

    while (true) {
        uint8_t buf = in.get();
        if (in.eof()) {
            return data;
        } else if (in.fail()) {
            return std::optional<std::vector<uint8_t>>();
        }

        data.push_back(buf);
    }

    return data;
}


std::optional<std::vector<uint8_t>> read_file(
    const std::string& path_to_file
)
{
    std::ifstream file(path_to_file);
    if (file.is_open()) {
        return read_file(file);
    }

    return std::optional<std::vector<uint8_t>>();
}


bool write_to_file(std::ofstream& out, const std::vector<uint8_t>& data)
{
    for (const auto output_byte : data) {
        out << output_byte;
        if (out.fail()) {
            return false;
        }
    }

    return true;
}


bool write_to_file(
    const std::string& path_to_file,
    const std::vector<uint8_t>& data
)
{
    std::ofstream file(path_to_file);
    if (file.is_open()) {
        return write_to_file(file, data);
    }

    return false;
}


std::optional<std::vector<uint8_t>> read_from_stdin()
{
    std::istreambuf_iterator<char> begin(std::cin), end;

    return std::vector<uint8_t>(begin, end);
}


bool write_to_stdout(const std::vector<uint8_t>& data)
{
    std::cout.write(
        reinterpret_cast<const char*>(data.data()),
        data.size());

    return not std::cout.fail();
}


std::optional<boost::gil::rgb8_image_t> read_image(
    const std::filesystem::path& path
)
{
    if (not path.has_extension()) {
        return std::optional<boost::gil::rgb8_image_t>();
    }

    try {
        boost::gil::rgb8_image_t img;
        std::ifstream img_stream(path, std::ios::binary);

        const auto ext = path.extension();
        if (ext == "jpeg" || ext == "jpg") {
            boost::gil::jpeg_read_and_convert_image(std::string(path), img);
        } else if (ext == "tiff" || ext == "tif") {
            boost::gil::tiff_read_and_convert_image(std::string(path), img);
        } else if (ext == "png") {
            boost::gil::png_read_and_convert_image(std::string(path), img);
        } else {
            return std::optional<boost::gil::rgb8_image_t>();
        }

        return img;
    } catch (const std::ios_base::failure& exc) {
        return std::optional<boost::gil::rgb8_image_t>();
    }
}


std::optional<boost::gil::rgb8_image_t> read_image(
    const std::string& path_to_file
)
{
    return read_image(path_to_file);
}


bool write_image(
    const boost::gil::rgb8_image_t& img,
    const std::filesystem::path& path
)
{
    if (not path.has_extension()) {
        return false;
    }

    try {
        boost::gil::rgb8_image_t img;
        std::ifstream img_stream(path, std::ios::binary);

        const auto ext = path.extension();
        if (ext == "jpeg" || ext == "jpg") {
            boost::gil::jpeg_write_view(std::string(path), boost::gil::view(img));
        } else if (ext == "tiff" || ext == "tif") {
            boost::gil::tiff_write_view(std::string(path), boost::gil::view(img));
        } else if (ext == "png") {
            boost::gil::png_write_view(std::string(path), boost::gil::view(img));
        } else {
            return false;
        }

        return true;
    } catch (const std::ios_base::failure& exc) {
        return false;
    }
}


bool write_image(
    const boost::gil::rgb8_image_t& image,
    const std::string& path_to_file
)
{
    return write_image(image, path_to_file);
}
}
