#include "crypto/utils/string_helpers.hpp"

#include <algorithm>


namespace crypto
{
    namespace utils
    {
        namespace string_helpers
        {
            std::string to_lowercase(const std::string& str)
            {
                std::string result;
                result.resize(str.length());

                std::transform(
                    str.begin(), str.end(), result.begin(),
                    [](unsigned char ch) { return std::tolower(ch); }
                );

                return result;
            }
        }
    }
}
