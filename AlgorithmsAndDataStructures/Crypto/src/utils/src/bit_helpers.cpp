#include "crypto/utils/bit_helpers.hpp"

namespace crypto::utils::bit_helpers
{
bool get_bit_value(uint8_t byte, uint8_t bit_num)
{
    return (byte >> bit_num) & 1;
}


uint8_t set_bit_value(uint8_t byte, uint8_t bit_idx, bool value)
{
    if (value) {
        return byte | (1 << bit_idx);
    }

    return byte & ~(1 << bit_idx);
}


uint8_t calc_reversed_bit_idx(const uint8_t bit_idx)
{
    return (BYTE_SIZE - 1) - bit_idx;
}


uint32_t reverse_byte_order(uint32_t x)
{
    x = (x & 0x00FF00FF) << 8 | (x & 0xFF00FF00) >> 8;
    x = (x & 0x0000FFFF) << 16 | (x & 0xFFFF0000) >> 16;
    return x;
}


std::array<uint8_t, sizeof(uint32_t)> split_bytes(uint32_t data)
{
    std::array<uint8_t, sizeof(uint32_t)> result;

    result[0] = data >> 24;
    result[1] = data >> 16;
    result[2] = data >> 8;
    result[3] = data;

    return result;
}


uint32_t concat_bytes(uint8_t a, uint8_t b, uint8_t c, uint8_t d)
{
    return static_cast<uint32_t>(a) << 24
        | static_cast<uint32_t>(b) << 16
        | static_cast<uint32_t>(c) << 8
        | static_cast<uint32_t>(d);
}


uint32_t concat_bytes(std::array<uint8_t, sizeof(uint32_t)> arr)
{
    return concat_bytes(arr[0], arr[1], arr[2], arr[3]);
}
}
