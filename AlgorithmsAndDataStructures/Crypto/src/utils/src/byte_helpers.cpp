#include "crypto/utils/byte_helpers.hpp"

namespace crypto::utils::byte_helpers
{
    uint32_t bytes_to_int(std::vector<uint8_t> &byte_arr)
    {
        uint32_t result = 0;
        uint32_t byte = 0;
        for (uint8_t i = 0; i < 4; i++) {
            byte = byte_arr[i] << (3 - i) * 8;
            result |= byte;
        }

        return result;
    }

    std::vector<uint8_t> int_to_bytes(const uint32_t integer)
    {
        std::vector<uint8_t> byte_arr;

        byte_arr.push_back((integer >> 24) & 0xFF);
        byte_arr.push_back((integer >> 16) & 0xFF);
        byte_arr.push_back((integer >> 8) & 0xFF);
        byte_arr.push_back(integer & 0xFF);

        return byte_arr;
    }

    std::vector<uint8_t> int_to_bytes(const std::vector<uint32_t>& data_in)
    {
        std::vector<uint8_t> result;
        for (auto it = data_in.begin(); it != data_in.end(); it++) {
            const auto bytes = int_to_bytes(*it);
            for (auto jt = bytes.begin(); jt != bytes.end(); jt++) {
                result.push_back(*jt);
            }
        }

        return result;
    }
} // namespace crypto::utils::byte_helpers
