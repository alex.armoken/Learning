#include <bitset>
#include <tuple>
#include <stdint.h>

#if !(defined(CRYPTO_BIT_BLOCK_H))
#define CRYPTO_BIT_BLOCK_H
namespace crypto::utils::bit_block
{
template <std::size_t BITS_COUNT>
class BitBlock
{
public:
    class BitReference
    {
    public:
        BitReference(BitBlock<BITS_COUNT>& data, std::size_t bit_num)
            : _bit_block(data)
            , _bit_num(bit_num)
        {
        }

        operator bool() const
        {
            return _bit_block.get(_bit_num);
        }

        bool operator=(bool value)
        {
            _bit_block.set(_bit_num, value);

            return value;
        }

        bool operator=(const BitReference& bit_reference)
        {
            _bit_block.set(_bit_num, bit_reference);

            return bit_reference;
        }

    private:
        std::size_t _bit_num = 0;
        BitBlock<BITS_COUNT>& _bit_block;
    };

    BitBlock() = default;
    BitBlock(const std::string& str);
    BitBlock(const unsigned char value);
    BitBlock(const std::bitset<BITS_COUNT>& bitset);
    ~BitBlock() = default;

    static const std::size_t size = BITS_COUNT;

    bool first() const; // Will return left-most bit
    bool last() const; // Will return right-most bit

    bool get(std::size_t index);
    void set(std::size_t index, bool value = true);
    void reset(std::size_t index);

    BitReference operator[](const std::size_t index);
    bool operator[](const std::size_t index) const;

    BitBlock<BITS_COUNT> operator^(const BitBlock<BITS_COUNT>& bit_block) const;
    BitBlock<BITS_COUNT> operator&(const BitBlock<BITS_COUNT>& bit_block) const;
    BitBlock<BITS_COUNT> operator|(const BitBlock<BITS_COUNT>& bit_block) const;
    bool operator==(const BitBlock<BITS_COUNT>& bit_block) const;

    BitBlock<BITS_COUNT>& operator<<=(std::size_t pos);
    BitBlock<BITS_COUNT>& operator>>=(std::size_t pos);

    template <std::size_t _BITS_COUNT>
    friend std::ostream& operator<<(
        std::ostream& os,
        const BitBlock<_BITS_COUNT>& bit_block
    );

private:
    std::bitset<BITS_COUNT> _data;
};

template <std::size_t BITS_COUNT>
BitBlock<BITS_COUNT>::BitBlock(const std::string& str)
    : _data(str)
{
}

template <std::size_t BITS_COUNT>
BitBlock<BITS_COUNT>::BitBlock(const unsigned char value)
    : _data(value)
{
}

template <std::size_t BITS_COUNT>
BitBlock<BITS_COUNT>::BitBlock(const std::bitset<BITS_COUNT>& bitset)
    : _data(bitset)
{
}

template <std::size_t BITS_COUNT>
bool BitBlock<BITS_COUNT>::first() const
{
    return _data[BITS_COUNT - 1];
}

template <std::size_t BITS_COUNT>
bool BitBlock<BITS_COUNT>::last() const
{
    return _data[0];
}

template <std::size_t BITS_COUNT>
void BitBlock<BITS_COUNT>::set(std::size_t index, bool value)
{
    _data[BITS_COUNT - 1 - index] = value;
}

template <std::size_t BITS_COUNT>
bool BitBlock<BITS_COUNT>::get(std::size_t index)
{
    return _data[BITS_COUNT - 1 - index];
}

template <std::size_t BITS_COUNT>
void BitBlock<BITS_COUNT>::reset(std::size_t index)
{
    set(index, false);
}

template <std::size_t BITS_COUNT>
typename BitBlock<BITS_COUNT>::BitReference
    BitBlock<BITS_COUNT>::operator[](const std::size_t index)
{
    return BitReference(*this, index);
}

template <std::size_t BITS_COUNT>
bool BitBlock<BITS_COUNT>::operator[](const std::size_t index) const
{
    return _data[BITS_COUNT - 1 - index];
}

template <std::size_t BITS_COUNT>
BitBlock<BITS_COUNT>& BitBlock<BITS_COUNT>::operator<<=(std::size_t pos)
{
    _data <<= pos;

    return *this;
}

template <std::size_t BITS_COUNT>
BitBlock<BITS_COUNT>& BitBlock<BITS_COUNT>::operator>>=(std::size_t pos)
{
    _data >>= pos;

    return *this;
}

template <std::size_t BITS_COUNT>
bool BitBlock<BITS_COUNT>::operator==(
    const BitBlock<BITS_COUNT>& bit_block) const
{
    return _data == bit_block._data;
}

template <std::size_t BITS_COUNT>
BitBlock<BITS_COUNT> BitBlock<BITS_COUNT>::operator^(
    const BitBlock<BITS_COUNT>& bit_block) const
{
    return _data ^ bit_block._data;
}

template <std::size_t BITS_COUNT>
BitBlock<BITS_COUNT> BitBlock<BITS_COUNT>::operator&(
    const BitBlock<BITS_COUNT>& bit_block) const
{
    return _data & bit_block._data;
}

template <std::size_t BITS_COUNT>
BitBlock<BITS_COUNT> BitBlock<BITS_COUNT>::operator|(
    const BitBlock<BITS_COUNT>& bit_block) const
{
    return _data | bit_block._data;
}

template <std::size_t BITS_COUNT>
std::ostream& operator<<(
    std::ostream& os,
    const BitBlock<BITS_COUNT>& bit_block)
{
    os << bit_block._data;

    return os;
}
}
#endif // CRYPTO_BIT_BLOCK_H
