#if !(defined(CRYPTO_CONSTANTS_H))
#define CRYPTO_CONSTANTS_H
namespace crypto
{
    namespace utils
    {
        const auto BYTE_SIZE = 8;
    }
}
#endif // CRYPTO_CONSTANTS_H
