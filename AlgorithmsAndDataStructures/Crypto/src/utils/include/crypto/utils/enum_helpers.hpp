#include <type_traits>


#if !(defined(CRYPTO_ENUM_HELPERS_H))
#define CRYPTO_ENUM_HELPERS_H
namespace crypto
{
    namespace utils
    {
        namespace enum_helpers
        {
            template <typename E>
            constexpr auto to_underlying(E e) noexcept
            {
                return static_cast<std::underlying_type_t<E>>(e);
            }
        }
    }
}
#endif // CRYPTO_ENUM_HELPERS_H
