#include <vector>
#include <cassert>

#if !(defined(CRYPTO_STRING_HELPERS_H))
#define CRYPTO_VECTOR_HELPERS_H
namespace crypto
{
    namespace utils
    {
        namespace vector_helpers
        {
            template <typename T>
            std::vector<T> xor_vec(
                const std::vector<T>& a,
                const std::vector<T>& b
            )
            {
                assert(a.size() == b.size());

                std::vector<T> result(a.size());
                for (auto i = 0; i < a.size(); ++i) {
                    result[i] = a[i] ^ b[i];
                }

                return result;
            }
        }
    }
}
#endif // CRYPTO_VECTOR_HELPERS_H
