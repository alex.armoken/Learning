#include <stdexcept>

#if !(defined(CRYPTO_EXCEPTIONS_H))
#define CRYPTO_EXCEPTIONS_H
namespace crypto
{
    namespace utils
    {
        namespace exceptions
        {
            class NotImplementedException : public std::logic_error
            {
            public:
                NotImplementedException()
                    : std::logic_error("Function not yet implemented.")
                {
                }
            };
        }
    }
}
#endif // CRYPTO_EXCEPTIONS_H
