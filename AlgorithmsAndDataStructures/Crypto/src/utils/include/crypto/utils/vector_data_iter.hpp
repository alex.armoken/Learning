#include <vector>
#include <type_traits>

#include "data_iter.hpp"

#if !(defined(CRYPTO_VECTOR_DATA_ITER_H))
#define CRYPTO_VECTOR_DATA_ITER_H
namespace crypto::utils::data_iter
{
template<typename TIn, typename TOut>
class VectorDataIter: DataIter<TIn, TOut>
{
public:
    static_assert(std::is_trivially_constructible<TIn>::value);

    struct IterPair
    {
        typename std::vector<TIn>::const_iterator cur_it;
        typename std::vector<TIn>::const_iterator end_it;
    };

    VectorDataIter(const std::vector<TIn>& vec)
    {
        this->iterators.emplace_back(vec.cbegin(), vec.cend());
    }

    VectorDataIter(
        const typename std::vector<TIn>::const_iterator begin,
        const typename std::vector<TIn>::const_iterator end
    )
    {
        this->iterators.emplace_back(begin, end);
    };

    VectorDataIter(std::vector<IterPair>&& iterators)
        : _iterators(std::move(iterators))
    {
    }

    VectorDataIter(std::initializer_list<IterPair> iterators)
        : _iterators(iterators)
    {
    }

    virtual ~VectorDataIter() = default;

    operator bool() override
    {
        const auto& iter_pair = this->iterators.first();

        return iter_pair.cur != iter_pair.end;
    }

    TOut operator*() override
    {
        return buf;
    }

    void operator++() override
    {
        load_next_buf();
    }

    void operator++(int) override
    {
        load_next_buf();
    }

private:
    TOut buf;
    std::size_t _count_of_not_used_bits;
    std::vector<IterPair> _iterators;

    bool switch_to_valid_iter_pair()
    {
        while (_iterators.first().cur == _iterators.first().end
            and not _iterators.empty()) {
            _iterators.erase(_iterators.begin());
        }

        return not _iterators.empty();
    }

    void load_next_buf()
    {
        if (_count_of_not_used_bits == 0
            and this->iterators.first().cur == this->iterators.first().end
            and not switch_to_valid_iter_pair()) {
            throw DataIter<TIn, TOut>::NoMoreDataExists();
        }

        if constexpr (sizeof(TOut) < sizeof(TIn)) {
            buf = 0;
        } else if (sizeof(TOut) > sizeof(TIn)) {
            buf = 0;

            // Get bits, which was unused in previous buf loading
            auto& iter_pair = _iterators.first();
            auto count_of_not_ready_bits = sizeof(TOut);
            if (_count_of_not_used_bits != 0) {
                const auto data = *iter_pair.cur;
                const auto used_bits_count = sizeof(TIn) - _count_of_not_used_bits;
                const auto not_used_bits = (data << used_bits_count) >> used_bits_count;

                buf |= not_used_bits;
                iter_pair.cur++;
                count_of_not_ready_bits -= _count_of_not_used_bits;
                _count_of_not_used_bits = 0;
            }

            do {
                if (iter_pair.cur == iter_pair.end
                    and not switch_to_valid_iter_pair()) {
                    throw DataIter<TIn, TOut>::BufferNotFull(buf, count_of_not_ready_bits);
                }
                iter_pair = _iterators.first();

                const auto data = *iter_pair.cur;
                _count_of_not_used_bits = sizeof(TIn);

                if (count_of_not_ready_bits >= sizeof(TIn)) {
                    // Load all data
                    buf <<= sizeof(TIn);
                    buf |= data;
                    count_of_not_ready_bits -= sizeof(TIn);
                    iter_pair.cur++;
                } else {
                    // Load part ot data
                    buf <<= count_of_not_ready_bits;
                    buf |= data >> (sizeof(TIn) - count_of_not_ready_bits);
                    count_of_not_ready_bits = 0; // Break cycle
                }
            } while (count_of_not_ready_bits != 0);
        } else {
            auto& iter_pair = _iterators.first();
            buf = *iter_pair.cur++;
        }
    }
};
}
#endif // CRYPTO_VECTOR_DATA_ITER_H
