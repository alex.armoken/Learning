#include <filesystem>
#include <fstream>
#include <ostream>
#include <stdint.h>
#include <string>
#include <string>
#include <vector>
#include <optional>

#include <boost/gil.hpp>

#if !(defined(CRYPTO_IO_HELPERS_H))
#define CRYPTO_IO_HELPERS_H
namespace crypto
{
    namespace utils
    {
        namespace io_helpers
        {
            std::optional<std::vector<uint8_t>> read_file(std::ifstream &in);

            std::optional<std::vector<uint8_t>> read_file(
                const std::string& path_to_file
            );

            bool write_to_file(std::ofstream &out, const std::vector<uint8_t> &data);

            bool write_to_file(
                const std::string& path_to_file,
                const std::vector<uint8_t> &data
            );

            std::optional<std::vector<uint8_t>> read_from_stdin();

            bool write_to_stdout(const std::vector<uint8_t>& data);

            std::optional<boost::gil::rgb8_image_t> read_image(
                const std::filesystem::path& path
            );
            std::optional<boost::gil::rgb8_image_t> read_image(
                const std::string& path_to_file
            );

            bool write_image(
                const boost::gil::rgb8_image_t& img,
                const std::filesystem::path& path
            );
            bool write_image(
                const boost::gil::rgb8_image_t& image,
                const std::string& path_to_file
            );
        }
    }
}
#endif // CRYPTO_IO_HELPERS_H
