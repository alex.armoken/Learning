#if !(defined(CRYPTO_BIT_HELPERS_H))
#define CRYPTO_BIT_HELPERS_H
#include <type_traits>
#include <bitset>
#include <vector>
#include <array>
#include <fstream>
#include <ostream>
#include <string>
#include <exception>

#include <optional>

#include "bit_block.hpp"
#include "constants.hpp"

namespace crypto
{
    namespace utils
    {
        namespace bit_helpers
        {
            bool get_bit_value(uint8_t byte, uint8_t bit_num);

            uint8_t set_bit_value(uint8_t byte, uint8_t bit_num, bool value);

            uint8_t calc_reversed_bit_idx(const uint8_t bit_idx);

            uint32_t reverse_byte_order(uint32_t x);

            std::array<uint8_t, sizeof(uint32_t)> split_bytes(uint32_t data);

            uint32_t concat_bytes(std::array<uint8_t, sizeof(uint32_t)> arr);
            uint32_t concat_bytes(uint8_t a, uint8_t b, uint8_t c, uint8_t d);
        }
    }
}
#endif // CRYPTO_BIT_HELPERS_H
