#include <cstring>
#include <stdint.h>
#include <exception>

#if !(defined(CRYPTO_DATA_ITER_H))
#define CRYPTO_DATA_ITER_H
namespace crypto::utils::data_iter
{
template<typename TIn, typename TOut>
class DataIter
{
public:
    class BufferNotFull: public std::exception
    {
    public:
        struct NotReadyData
        {
            TOut data;
            std::size_t count_of_not_ready_bits;
        };

        BufferNotFull(TOut data, std::size_t count_of_not_ready_bits)
            : data(data, count_of_not_ready_bits)
        {
        }

        BufferNotFull(NotReadyData data): data(data)
        {
        }

        const char* what()
        {
            return "Buffer not ready";
        }

        NotReadyData get_not_ready_data() const
        {
            return this->data;
        };

    private:
        NotReadyData data;
    };


    class NoMoreDataExists: public std::exception
    {
    public:
        const char* what()
        {
            return "No more data exists";
        }
    };


    DataIter() = default;
    virtual ~DataIter() = default;

    virtual operator bool() = 0;
    virtual TOut operator*() = 0;
    virtual void operator++() = 0;
    virtual void operator++(int) = 0;
};
}
#endif // CRYPTO_DATA_ITER_H
