#include <string>

#if !(defined(CRYPTO_STRING_HELPERS_H))
#define CRYPTO_STRING_HELPERS_H
namespace crypto
{
    namespace utils
    {
        namespace string_helpers
        {
            std::string to_lowercase(const std::string& str);
        }
    }
}
#endif // CRYPTO_STRING_HELPERS_H
