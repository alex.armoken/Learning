#if !(defined(CRYPTO_ENCRYPTION_MODE_H))
#define CRYPTO_ENCRYPTION_MODE_H
namespace crypto
{
    namespace utils
    {
        enum class EncryptionMode
        {
            FIRST = 0,
            ECB = 0, // Electronic Codebook
            CBC = 1, // Cipher Block Chaining
            CFB = 2, // Cipher Feedback Mode
            LAST = CFB
        };
    }
}
#endif // CRYPTO_ENCRYPTION_MODE_H
