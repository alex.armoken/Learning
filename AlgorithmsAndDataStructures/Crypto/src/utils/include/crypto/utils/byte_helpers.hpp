#if !defined(CRYPTO_BYTE_HELPERS_H)
#define CRYPTO_BYTE_HELPERS_H
#include <vector>
#include <cstdint>

namespace crypto::utils::byte_helpers
{
    uint32_t bytes_to_int(std::vector<uint8_t> &byte_arr);
    std::vector<uint8_t> int_to_bytes(const uint32_t integer);
    std::vector<uint8_t> int_to_bytes(const std::vector<uint32_t>& data_in);
} // namespace crypto::utils::byte_helpers
#endif // CRYPTO_BYTE_HELPERS_H
