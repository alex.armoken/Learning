#include <bitset>
#include <tuple>
#include <stdint.h>
#include <vector>

#include "bit_block.hpp"
#include "constants.hpp"
#include "bit_helpers.hpp"

#if !(defined(CRYPTO_BIT_BLOCK_HELPERS_H))
#define CRYPTO_BIT_BLOCK_HELPERS_H
namespace crypto::utils::bit_block {
using namespace bit_helpers;

template <std::size_t BITS_COUNT>
void rotate_left(BitBlock<BITS_COUNT>& bitblock)
{
    const bool left_most_bit = bitblock[0];
    bitblock <<= 1;
    bitblock[BITS_COUNT - 1] = left_most_bit;
}

template <std::size_t BITS_COUNT>
void rotate_left(BitBlock<BITS_COUNT>& bitblock, std::size_t shifts_count)
{
    for (auto _ = 0; _ < shifts_count; ++_) {
        rotate_left(bitblock);
    }
}

template <std::size_t BITS_COUNT>
void rotate_right(BitBlock<BITS_COUNT>& bitblock)
{
    const bool right_most_bit = bitblock[BITS_COUNT - 1];
    bitblock >>= 1;
    bitblock[0] = right_most_bit;
}

template <std::size_t BITS_COUNT>
void rotate_right(BitBlock<BITS_COUNT>& bitblock, std::size_t shifts_count)
{
    for (auto _ = 0; _ < shifts_count; ++_) {
        rotate_right(bitblock);
    }
}

template <std::size_t BITS_COUNT>
void rotate_left(std::bitset<BITS_COUNT>& bitset)
{
    const bool left_most_bit = bitset[BITS_COUNT - 1];
    bitset <<= 1;
    bitset[0] = left_most_bit;
}

template <std::size_t BITS_COUNT>
void rotate_left(std::bitset<BITS_COUNT>& bitset, std::size_t shifts_count)
{
    for (auto _ = 0; _ < shifts_count; ++_) {
        rotate_left(bitset);
    }
}

template <std::size_t BITS_COUNT>
void rotate_right(std::bitset<BITS_COUNT>& bitset)
{
    const bool right_most_bit = bitset[0];
    bitset >>= 1;
    bitset[BITS_COUNT - 1] = right_most_bit;
}

template <std::size_t BITS_COUNT>
void rotate_right(
    std::bitset<BITS_COUNT>& bitset,
    std::size_t shifts_count
)
{
    for (auto _ = 0; _ < shifts_count; ++_) {
        rotate_right(bitset);
    }
}

template <std::size_t BYTES_COUNT>
void append_to_arr_of_bytes(
    const BitBlock<BYTES_COUNT * BYTE_SIZE>& block,
    std::back_insert_iterator<std::vector<uint8_t>> inserter
)
{
    uint8_t tmp_byte = 0;
    for (auto bit_idx = 0; bit_idx < block.size; ++bit_idx) {
        const auto byte_bit_idx = bit_idx % BYTE_SIZE;
        tmp_byte = set_bit_value(
            tmp_byte,
            calc_reversed_bit_idx(byte_bit_idx),
            block[bit_idx]);

        if (byte_bit_idx == BYTE_SIZE - 1) {
            inserter = tmp_byte;
            tmp_byte = 0;
        }
    }
}

template <std::size_t BITS_COUNT>
std::size_t find_idx_of_last_bit(const BitBlock<BITS_COUNT>& block)
{
    for (auto bit_num = block.size; bit_num >= 1; --bit_num) {
        const auto bit_idx = bit_num - 1;
        if (block[bit_idx]) {
            return bit_idx;
        }
    }

    std::terminate();
}

template <std::size_t BYTES_COUNT>
void append_last_block_to_arr_of_bytes(
    const BitBlock<BYTES_COUNT * BYTE_SIZE>& block,
    std::back_insert_iterator<std::vector<uint8_t>> inserter
)
{
    uint8_t tmp_byte = 0;
    const auto last_bit_idx = find_idx_of_last_bit(block);
    for (auto bit_idx = 0; bit_idx < last_bit_idx; ++bit_idx) {
        const auto byte_bit_idx = bit_idx % BYTE_SIZE;
        tmp_byte = set_bit_value(
            tmp_byte,
            calc_reversed_bit_idx(byte_bit_idx),
            block[bit_idx]);

        if (byte_bit_idx == BYTE_SIZE - 1) {
            inserter = tmp_byte;
            tmp_byte = 0;
        }
    }
}

template <std::size_t BITS_COUNT>
std::tuple<BitBlock<BITS_COUNT>, BitBlock<BITS_COUNT>> split_blocks(
    const BitBlock<BITS_COUNT * 2>& block
)
{
    BitBlock<BITS_COUNT> block_a(0);
    BitBlock<BITS_COUNT> block_b(0);

    for (auto bit_idx = 0; bit_idx < BITS_COUNT * 2; ++bit_idx) {
        if (bit_idx < BITS_COUNT) {
            block_a[bit_idx] = block[bit_idx];
        } else {
            block_b[bit_idx - BITS_COUNT] = block[bit_idx];
        }
    }

    return std::make_tuple(block_a, block_b);
}

template <std::size_t BITS_COUNT>
BitBlock<BITS_COUNT * 2> concat_blocks(
    const BitBlock<BITS_COUNT>& block_a,
    const BitBlock<BITS_COUNT>& block_b
)
{
    BitBlock<BITS_COUNT * 2> result_block;
    for (auto bit_idx = 0; bit_idx < BITS_COUNT * 2; ++bit_idx) {
        if (bit_idx < BITS_COUNT) {
            result_block[bit_idx] = block_a[bit_idx];
        } else {
            result_block[bit_idx] = block_b[bit_idx - BITS_COUNT];
        }
    }

    return result_block;
}
}
#endif // CRYPTO_BIT_BLOCK_HELPERS_H
