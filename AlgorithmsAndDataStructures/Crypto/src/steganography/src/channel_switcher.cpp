#include "crypto/steganography/channel_switcher.hpp"

namespace crypto::steganography
{
bool ChannelSwitcher::next_channel()
{
    if (ch == RGBA_CHANNEL::RED) {
        ch = RGBA_CHANNEL::GREEN;
    } else if (ch == RGBA_CHANNEL::GREEN) {
        ch = RGBA_CHANNEL::BLUE;
    } else if (ch == RGBA_CHANNEL::BLUE && !is_alpha_exist) {
        ch = RGBA_CHANNEL::RED;
        return true;
    } else if (ch == RGBA_CHANNEL::BLUE && is_alpha_exist) {
        ch = RGBA_CHANNEL::ALPHA;
    } else if (ch == RGBA_CHANNEL::ALPHA) {
        ch = RGBA_CHANNEL::RED;
        return true;
    }
    return false;
}
}
