#include "crypto/steganography/cur_image_pos.hpp"

namespace crypto::steganography
{
CurImagePos::CurImagePos(
    uint64_t width, uint64_t height, uint8_t bits_count,
    uint64_t x, uint64_t y, RGBA_CHANNEL c, bool is_alpha_exist
)
{
    this->b = 0;
    this->x = x;
    this->y = y;
    this->width = width;
    this->height = height;
    this->bits_count = bits_count;
    this->c = ChannelSwitcher(c, is_alpha_exist);
}


CurImagePos::CurImagePos(uint64_t width, uint64_t height, uint8_t bits_count)
{
    this->b = 0;
    this->x = 0;
    this->y = 0;
    this->width = width;
    this->height = height;
    this->bits_count = bits_count;
    this->c = ChannelSwitcher(RGBA_CHANNEL::RED);
};


CurImagePos::CurImagePos(
    uint64_t width, uint64_t height, uint8_t bits_count,
    RGBA_CHANNEL c, bool is_alpha_exist
)
{
    this->b = 0;
    this->x = 0;
    this->y = 0;
    this->width = width;
    this->height = height;
    this->bits_count = bits_count;
    this->c = ChannelSwitcher(c, is_alpha_exist);
}


CurImagePos::CurImagePos(
    uint64_t width, uint64_t height,
    uint8_t bits_count, bool is_alpha_exist
)
{
    this->b = 0;
    this->x = 0;
    this->y = 0;
    this->width = width;
    this->height = height;
    this->bits_count = bits_count;
    this->c = ChannelSwitcher(RGBA_CHANNEL::RED, is_alpha_exist);
}


bool CurImagePos::increment_pos()
{
    if (++b == bits_count) {
        b = 0;
        if (++c) {
            if (++x == width) {
                x = 0;
                if (++y == height) {
                    return true;
                }
            }
        }
    }
    return false;
}
}
