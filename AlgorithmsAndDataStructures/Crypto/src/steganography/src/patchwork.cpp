#include "crypto/steganography/patchwork.hpp"

#include <iomanip>
#include <cstdlib>
#include <iostream>
#include <algorithm>

#include "crypto/steganography/channel_switcher.hpp"


namespace crypto::steganography
{
void Patchwork::hide(
    cimg_library::CImg<uint8_t> &img, std::size_t key,
    uint8_t delta, uint32_t steps_count
)
{
    srand(key);
    std::cout << (int)delta << "\n";

    for (uint32_t i = 0; i < steps_count; i++) {
        uint64_t a_x = rand() % img.width();
        uint64_t a_y = rand() % img.height();
        uint64_t b_x = rand() % img.width();
        uint64_t b_y = rand() % img.height();

        for (uint8_t c = 0; c < 1; c++) {
            img.atXY(a_x, a_y, c) += delta;
            img.atXY(b_x, b_y, c) -= delta;
        }
    }
}


bool Patchwork::hide(
    std::string &input_filename,
    std::string &output_filename,
    std::size_t key, uint8_t delta, uint32_t steps_count
)
{
    try {
        cimg_library::CImg<uint8_t> img(input_filename.c_str());
        hide(img, key, delta, steps_count);
        img.save(output_filename.c_str());
        return true;
    } catch (std::exception &error) {
        std::cout << "Caught exception: " << error.what() << std::endl;
        return false;
    }
}


bool Patchwork::test(
    cimg_library::CImg<uint8_t> &img, std::size_t key,
    uint8_t delta, uint32_t steps_count
)
{
    srand(key);
    double startm = 0;
    double finishm = 0;
    std::cout << (int)delta << "\n";
    for (uint32_t i = 0; i < steps_count; i++) {
        uint64_t a_x = rand() % img.width();
        uint64_t a_y = rand() % img.height();
        uint64_t b_x = rand() % img.width();
        uint64_t b_y = rand() % img.height();

        for (uint8_t c = 0; c < 1; c++) {
            startm += img.atXY(a_x, a_y, c) - img.atXY(b_x, b_y, c);
            img.atXY(a_x, a_y, c) -= delta;
            img.atXY(b_x, b_y, c) += delta;
            finishm += img.atXY(a_x, a_y, c) - img.atXY(b_x, b_y, c);
        }
    }
    std::cout << startm << " - " << finishm << "\n";
    return true;
}


bool Patchwork::test(
    std::string &input_filename,
    std::size_t key,
    uint8_t delta, uint32_t steps_count
)
{
    try {
        cimg_library::CImg<uint8_t> img(input_filename.c_str());
        return test(img, key, delta, steps_count);
    } catch (std::exception &error) {
        std::cout << "Caught exception: " << error.what() << std::endl;
        return false;
    }
}
}
