#include <array>
#include <cstdint>
#include <iomanip>
#include <iostream>
#include <algorithm>

#include "crypto/steganography/lsb.hpp"
#include "crypto/utils/bit_helpers.hpp"

namespace crypto::steganography
{
using namespace utils::bit_helpers;

uint8_t LSB::extract_byte_from_img(
    const boost::gil::rgb8_image_t& img,
    CurImagePos& pos
)
{
    uint8_t data_byte = 0;
    for (auto _ = 0; _ < 8; _++, pos++) {
        const auto pixel = *img._view.xy_at(pos.getx(), pos.gety());
        const auto channel_byte = pixel.at_c_dynamic(pos.getc());
        if (get_bit_value(channel_byte, pos.getb())) {
            data_byte = (data_byte >> 1) | 0b10000000;
        } else {
            data_byte = data_byte >> 1;
        }
    }

    return data_byte;
}


std::vector<uint8_t> LSB::extract_data_from_img(
    const boost::gil::rgb8_image_t& img,
    uint8_t lsb_count
)
{
    CurImagePos curPos(img.width(), img.height(), lsb_count);

    std::array<uint8_t, sizeof(uint32_t)> size_arr = { 0, 0, 0, 0 };
    for (uint8_t i = 0; i < size_arr.size(); i++) {
        size_arr[i] = LSB::extract_byte_from_img(img, curPos);
    }
    uint32_t size = concat_bytes(size_arr);

    std::vector<uint8_t> data;
    for (int i = 0; i < size; i++) {
        data.push_back(LSB::extract_byte_from_img(img, curPos));
    }
    return data;
}


void LSB::hide_data_into_img(
    const std::vector<uint8_t>& msg,
    boost::gil::rgb8_image_t& img,
    uint8_t lsb_count
)
{
    CurImagePos pos(img.width(), img.height(), lsb_count);
    for (auto it = msg.begin(); it != msg.end(); it++) {
        for (uint8_t data_bit_num = 0; data_bit_num < 8; data_bit_num++) {
            auto pixel = *img._view.xy_at(pos.getx(), pos.gety());

            uint8_t byte = pixel[pos.getc()];
            const bool bit_val = get_bit_value(byte, data_bit_num);
            byte = set_bit_value(byte, pos.getb(), bit_val);

            pixel[pos.getc()] = byte;
            pos++;
        }
    }
}
}
