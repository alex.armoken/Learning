#include <vector>
#include <stdint.h>

#include <optional>
#include <boost/gil.hpp>

#include "cur_image_pos.hpp"
#include "channel_switcher.hpp"

#if !(defined(STEGANOGRAPHY_LSB_H))
#define STEGANOGRAPHY_LSB_H
namespace crypto::steganography
{
class LSB
{
public:
    LSB() = delete;

    static bool get_bit_from_channel(uint8_t byte, uint8_t bit_num);

    static uint8_t extract_byte_from_img(
        const boost::gil::rgb8_image_t& img,
        CurImagePos& pos
    );
    static std::vector<uint8_t> extract_data_from_img(
        const boost::gil::rgb8_image_t& img,
        uint8_t lsb_count
    );

    static void hide_data_into_img(
        const std::vector<uint8_t>& msg,
        boost::gil::rgb8_image_t& img,
        uint8_t lsb_count
    );
};
}
#endif // STEGANOGRAPHY_LSB_H
