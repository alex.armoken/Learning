#if !(defined (STEGANOGRAPHY_CHANNEL_SWITCHER_H))
#define STEGANOGRAPHY_CHANNEL_SWITCHER_H
namespace crypto::steganography
{
enum class RGBA_CHANNEL
{
  RED = 0,
  GREEN = 1,
  BLUE = 2,
  ALPHA = 3,
};

class ChannelSwitcher
{
public:
    ChannelSwitcher()
    {
        this->ch = RGBA_CHANNEL::RED;
        this->is_alpha_exist = false;
    }

    ChannelSwitcher(const ChannelSwitcher& ch)
    {
        this->ch = ch.ch;
        this->is_alpha_exist = ch.is_alpha_exist;
    }

    ChannelSwitcher(RGBA_CHANNEL ch)
    {
        this->ch = ch;
        this->is_alpha_exist = false;
    };

    ChannelSwitcher(RGBA_CHANNEL ch, bool is_alpha_exist)
    {
        this->ch = ch;
        this->is_alpha_exist = is_alpha_exist;
    };

    RGBA_CHANNEL operator*() { return ch;};
    bool operator++() { return next_channel(); }
    bool operator++(int) { return next_channel(); }

private:
    RGBA_CHANNEL ch;
    bool is_alpha_exist;

    bool next_channel();
};
}
#endif // STEGANOGRAPHY_CHANNEL_SWITCHER_H
