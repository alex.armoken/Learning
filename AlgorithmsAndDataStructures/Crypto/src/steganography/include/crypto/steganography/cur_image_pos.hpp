#include <vector>
#include <stdint.h>

#include "crypto/utils/enum_helpers.hpp"

#include "channel_switcher.hpp"

#if !(defined(STEGANOGRAPHY_CUR_IMAGE_POS_H))
#define STEGANOGRAPHY_CUR_IMAGE_POS_H
namespace crypto::steganography
{
    using namespace crypto::utils::enum_helpers;

    class CurImagePos
    {
    public:
        CurImagePos(
            uint64_t width, uint64_t height, uint8_t bits_count,
            uint64_t x, uint64_t y, RGBA_CHANNEL c, bool is_alpha_exist
        );

        CurImagePos(uint64_t width, uint64_t height, uint8_t bits_count);

        CurImagePos(
            uint64_t width, uint64_t height, uint8_t bits_count,
            RGBA_CHANNEL c, bool is_alpha_exist
        );

        CurImagePos(
            uint64_t width, uint64_t height,
            uint8_t bits_count, bool is_alpha_exist
        );

        uint64_t getx() { return x; }
        uint64_t gety() { return y; }
        uint64_t getb() { return b; }
        uint64_t getc() { return to_underlying(*c); }

        bool operator++() { return increment_pos(); };
        bool operator++(int) { return increment_pos(); };

    private:
        uint8_t b;
        uint64_t x;
        uint64_t y;
        uint64_t width;
        uint64_t height;
        uint64_t bits_count;
        ChannelSwitcher c;

        bool increment_pos();
    };
}
#endif // STEGANOGRAPHY_CUR_IMAGE_POS_H
