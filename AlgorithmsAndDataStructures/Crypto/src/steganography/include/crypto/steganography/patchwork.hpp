#if !(defined(STEGANOGRAPHY_PATCHWORK_H))
#define STEGANOGRAPHY_PATCHWORK_H
#include <vector>
#include <stdint.h>
#include <string>

#include <iomanip>
#include <cstdlib>
#include <iostream>
#include <algorithm>

#define cimg_display 0
#include "CImg.h"

namespace crypto::steganography
{
class Patchwork
{
public:
    Patchwork() = delete;

    static void hide(
        cimg_library::CImg<uint8_t>& img,
        std::size_t key,
        uint8_t delta,
        uint32_t steps_count
    );
    static bool hide(
        std::string& input_filename,
        std::string& output_filename,
        std::size_t key,
        uint8_t delta,
        uint32_t steps_count
    );

    static bool test(
        cimg_library::CImg<uint8_t>& img,
        std::size_t key,
        uint8_t delta,
        uint32_t steps_count
    );
    static bool test(
        std::string& input_filename,
        std::size_t key,
        uint8_t delta,
        uint32_t steps_count
    );
};
}
#endif // STEGANOGRAPHY_PATCHWORK_H
