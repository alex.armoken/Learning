#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdint.h"
#include "unistd.h"
#include "getopt.h"
#include "sqlite3.h"

struct global_args_t {
    char *filename;
    char *query_data;
    bool is_init_db;
} global_args;

static struct option OPTS[] = {
    {"file", required_argument, NULL, 'f'},
    {"init", no_argument, NULL, 'i'},
    {"query_data", required_argument, NULL, 'q'},
    {0, 0, 0, 0}
};

int parse_arguments(int argc, char *argv[])
{
    int c;
    int opt_idx;
    const char *args = "if:q:";
    global_args.filename = NULL;
    global_args.query_data = NULL;
    global_args.is_init_db = false;

    while ((c = getopt_long(argc, argv, args, OPTS, &opt_idx)) != -1) {
        switch (c) {
        case 'i':
            global_args.is_init_db = true;
            break;
        case 'f':
            global_args.filename = optarg;
            break;
        case 'q':
            global_args.query_data = optarg;
            break;
        default:
            return -1;
        }
    }
    return 0;
}

bool check_args_logic()
{
    if (global_args.filename == NULL
        || global_args.query_data == NULL) {
        return false;
    }
    return true;
}

bool init_db(sqlite3 *db, char **error)
{
    const char *init_sql = "CREATE TABLE IF NOT EXISTS TOP(i INTEGER PRIMARY KEY, name TEXT, city TEXT);"
        "CREATE TABLE IF NOT EXISTS KEK(i INTEGER PRIMARY KEY, name TEXT, city TEXT, secret TEXT);"

        "INSERT INTO TOP VALUES(1,'alexander','minsk');"
        "INSERT INTO TOP VALUES(2,'boris','bereza');"
        "INSERT INTO TOP VALUES(3,'bill','bereza');"
        "INSERT INTO TOP VALUES(4,'linus','helsingfors');"

        "INSERT INTO KEK VALUES(1,'alexander','minsk','emacs');"
        "INSERT INTO KEK VALUES(2,'boris','bereza','kek');"
        "INSERT INTO KEK VALUES(3,'bill','bereza','topkek');"
        "INSERT INTO KEK VALUES(4,'linus','helsingfors','git');";
    return sqlite3_exec(db, init_sql, NULL, NULL, error);
}

char* construct_query(const char *pre_query, const char *query_data)
{
    int pre_query_len = strlen(pre_query);
    int query_data_len = strlen(query_data);
    char* query = (char*)calloc(1, pre_query_len + query_data_len + 1);
    if (query == NULL) {
        return NULL;
    }

    strcpy(query, pre_query);
    strcpy(query + pre_query_len, query_data);
    return query;
}

int callback(void *notused, int coln, char **rows, char **colnm)
{
    for(int i = 0; i < coln; i++) {
        printf("%s\t", colnm[i]);
    }
    printf("\n");

    for(int i = 0; i < coln; i++) {
        printf("-------\t");
    }
    printf("\n");

    for(int i = 0; i < coln; i++) {
        printf("%s\t|", rows[i]);
    }
    printf("\n");
    return 0;
}

int main_program()
{
    int result = 0;
    sqlite3 *db = 0;
    char *error = NULL;
    const char *pre_query = "SELECT * FROM TOP WHERE city LIKE ";
    char *query = NULL;

    if (sqlite3_open(global_args.filename, &db)) {
        printf("Can't open database!\n");
        result = 3;
    } else if (global_args.is_init_db && init_db(db, &error)) {
        printf("DB init error! %s\n", error);
        sqlite3_free(error);
        result = 4;
    } else if ((query = construct_query(pre_query, global_args.query_data)) == NULL) {
        printf("Memory error!\n");
        result = 5;
    } else if (sqlite3_exec(db, query, callback, NULL, &error)) {
        printf("SQL query error! %s\n", error);
        sqlite3_free(error);
    }

    sqlite3_close(db);
    return 0;
}

// How to use this:
// ./sqlinjection.bin -f kek.sqlite3 -q "'bereza';
// SELECT name FROM sqlite_master; SELECT * FROM KEK;"

int main(int argc, char *argv[])
{
    if (parse_arguments(argc, argv) == -1) {
        return 1;
    } else if (!check_args_logic()) {
        return 2;
    }
    return main_program();
}
