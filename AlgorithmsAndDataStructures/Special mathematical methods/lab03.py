import sys
from typing import List, Set, Tuple

import numpy


def find_nash_equilibrium_numpy(game_matrix):
    players_number = game_matrix.ndim - 1
    assert(players_number > 1)
    assert(game_matrix.shape[-1] == players_number)

    best_players_strategies = []
    for player in range(players_number):
        player_matrix = game_matrix[..., player]
        max_rewards = player_matrix.max(axis=player, keepdims=True)
        best_player_strategies = numpy.argwhere(player_matrix == max_rewards)
        best_players_strategies.append(
            set(tuple(map(tuple, best_player_strategies)))
        )

    return set.intersection(*best_players_strategies)


def main() -> int:
    game_matrix=numpy.array([
        [[1, 1], [2, 0]],
        [[0, 2], [1, 1]]
    ])
    print(find_nash_equilibrium(game_matrix))


if __name__ == '__main__':
    sys.exit(main())
