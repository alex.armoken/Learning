import sys

import numpy as np
from scipy import linalg
from scipy import optimize


def solve_subtask(A, B, b, c, uf, ul):
    obj_func_v = np.matmul(uf, A) - c
    linprog_res = optimize.linprog(obj_func_v, A_eq=B, b_eq=b)
    if linprog_res.success:
        return linprog_res.x, linprog_res.fun + ul


def find_new_components(A, Anew_end, c, xp):
    return np.concatenate((np.matmul(A, xp), Anew_end)), np.matmul(c, xp)


def generate_columns_method(A1, A2, B1, B2, c1, c2, b0,
                            b1, b2, A_basis, d_basis, eps=1e-10):
    be = np.concatenate((b0, np.array([1, 1])))
    while True:
        # Step 1: Find inverse basis matrix
        A_basis_inverse = linalg.inv(A_basis)

        # Step 2: Find vector with estimates u'
        u = np.matmul(d_basis, A_basis_inverse)
        # To find estimate we need to solve linear programming subtasks
        # solve subtask u'A_1x+u_{m_0+1}-(c^1)'x_i^+->min
        xp1, delta1 = solve_subtask(A1, B1, b1, c1, u[:-2], u[-2])
        # If value of target function is positive solve second subtask
        if delta1 >= -eps:
            # solve subtask u'A_2x+u_{m_0+2}-(c^2)'x_i^+->min
            xp2, delta2 = solve_subtask(A2, B2, b2, c2, u[:-2], u[-1])
            # If values of both target functions are positive then optimal
            # solution was found
            if delta2 >= -eps:
                return u @ be, np.concatenate((xp1, xp2))

        # Step 3: find not basis column to be added to basis columns
        if delta1 < -eps:
            # Not basis column B_i and new component of cost vector d_{new}
            Anew, d_new = find_new_components(A1, np.array([1, 0]), c1, xp1)
        else:
            Anew, d_new = find_new_components(A2, np.array([0, 1]), c2, xp2)

        # Step 4: find basis column A_i that need to be removed from matrix
        # A_{basis}
        z = np.matmul(A_basis_inverse, Anew)
        z_pos_ind = np.argwhere(z > eps)[0]
        if z_pos_ind.shape[0] == 0:
            # The object function is unbounded on the set of permissible plans
            return None
        theta = A_basis_inverse[z_pos_ind, :] @ be / z[z_pos_ind]
        # Index in basis matrix where minimum was achieved
        min_theta = np.argmin(theta)
        replacing_column = z_pos_ind[min_theta]

        # Step 5: Replace column i with A_{new}
        A_basis[:, replacing_column] = Anew
        # Replace component d_i with d_new
        d_basis[replacing_column] = d_new


def main() -> int:
    A_basis = np.array([
        [0, 6, -4, 0],
        [0, 6, 0, 0],
        [1, 1, 0, 0],
        [0, 0, 1, 1]
    ])
    d_basis = np.array([0, 6, 0, 0])
    A1 = np.array([
        [1, 1, 0],
        [1, 0, 0]
    ])
    c1 = np.array([1, 1, 0])
    B1 = np.array([1, 2, 1]).reshape(1, 3)
    b1 = np.array([6])
    A2 = np.array([
        [0, 0, -1],
        [0, -1, 0]
    ])
    c2 = np.array([0, 0, 0])
    B2 = np.array([1, 1, 1]).reshape(1, 3)
    b2 = np.array([4])
    b0 = np.array([1, 1])

    print(generate_columns_method(A1, A2, B1, B2, c1, c2, b0, b1, b2,
                            A_basis, d_basis, 1e-12))


if __name__ == '__main__':
    sys.exit(main())
