import sys
import copy

import numpy as np


def find_stable_marriages(man_preferences, woman_preferences):
    man_preferences = copy.deepcopy(man_preferences)
    woman_preferences = copy.deepcopy(woman_preferences)

    lonely_men = list(man_preferences.keys())
    woman_husband = {
        woman: -1 for woman in woman_preferences.keys()
    }
    man_wife = {
        man: -1 for man in man_preferences.keys()
    }

    while len(lonely_men) != 0:
        current_man = lonely_men[0]
        if len(man_preferences[current_man]) > 0:
            current_woman = man_preferences[current_man][0]
            woman_current_husband = woman_husband[current_woman]
            if not woman_current_husband != -1:
                woman_husband[current_woman] = current_man
                lonely_men.remove(current_man)
                man_wife[current_man] = current_woman
            elif woman_preferences[current_woman].index(current_man) < woman_preferences[current_woman].index(woman_current_husband):
                woman_husband[current_woman] = current_man
                lonely_men.remove(current_man)
                man_wife[current_man] = current_woman
                man_preferences[woman_current_husband].remove(current_woman)
                man_wife[woman_current_husband] = -1
                lonely_men.append(woman_current_husband)
            else:
                man_preferences[current_man].remove(current_woman)
        else:
            lonely_men.remove(current_man)

    return man_wife


def main() -> int:
    # (0)Коля — ((0)Ира, (1)Аня, (2)Яна, (3)Ева)              (0)Ира — ((1)Петя, (2)Вася, (0)Коля, (3)Миша, (4)Саша)
    # (1)Петя — ((3)Ева, (1)Аня, (2)Яна, (0)Ира)              (1)Аня — ((2)Вася, (0)Коля, (1)Петя, (3)Миша, (4)Саша)
    # (2)Вася — ((3)Ева, (2)Яна, (0)Ира, (1)Аня)              (2)Яна — ((4)Саша, (3)Миша, (0)Коля, (1)Петя, (2)Вася)
    # (3)Миша — ((0)Ира, (3)Ева, (2)Яна, (1)Аня)              (3)Ева — ((0)Коля, (3)Миша, (4)Саша, (1)Петя, (2)Вася)
    # (4)Саша — ((0)Ира, (1)Аня, (3)Ева)

    man_preferences={
        0: [0, 1, 2, 3],
        1: [3, 1, 2, 0],
        2: [3, 2, 0, 1],
        3: [0, 3, 2, 1],
        4: [0, 1, 3]
    }

    woman_preferences={
        0: [1, 2, 0, 3, 4],
        1: [2, 0, 1, 3, 4],
        2: [4, 3, 0, 1, 2],
        3: [0, 3, 4, 1, 2]
    }

    #Epected result:
    # (0)Коля — (0)Ира
    # (1)Петя — (1)Аня
    # (2)Вася — (2)Яна
    # (3)Миша — (3)Ева
    # (4)Саша

    print(find_stable_marriages(man_preferences, woman_preferences))


if __name__ == '__main__':
    sys.exit(main())
