import sys

import numpy
import scipy.optimize

def solve(per_task_time, workers_assigment):
    assert(per_task_time.shape[0] == workers_assigment.shape[0])
    tasks_num = per_task_time.shape[0]
    worker_num = workers_assigment.shape[1]
    task_time_variable_last_idx = numpy.cumsum(
        numpy.sum(workers_assigment, axis=1)
    )
    task_time_variable_idxes = numpy.array(
        [[start_idx, end_idx-1] for start_idx, end_idx in zip(
            numpy.concatenate((numpy.zeros(1), task_time_variable_last_idx[:-1])),
            task_time_variable_last_idx)],
        dtype=int
    )

    variables_num = task_time_variable_last_idx[-1]+1
    A_eq = numpy.zeros((tasks_num, variables_num))
    for task_idx, idxes in enumerate(task_time_variable_idxes):
        var_start_idx, var_end_idx = idxes[0], idxes[1]
        A_eq[task_idx, var_start_idx: var_end_idx+1] = 1

    b_eq = per_task_time
    A_ub = numpy.zeros((worker_num, variables_num))
    A_ub[:, -1] = -1
    for worker_idx in range(worker_num):
        for assigned_task_idx in numpy.where(workers_assigment[:, worker_idx]!=0)[0]:
            offset = numpy.sum(
                workers_assigment[assigned_task_idx, :worker_idx]
            )
            A_ub[
                worker_idx,
                task_time_variable_idxes[assigned_task_idx][0] + offset
            ] = 1

    b_ub = numpy.zeros(worker_num)
    c = numpy.zeros((variables_num,))
    c[-1] = 1
    linprog_result = numpy.optimize.linprog(
        c, A_ub=A_ub, b_ub=b_ub, A_eq=A_eq, b_eq=b_eq
    )
    working_time = numpy.zeros((tasks_num, worker_num))
    for task_idx, idxes in enumerate(task_time_variable_idxes):
        var_start_idx, var_end_idx = idxes[0], idxes[1]
        var_values=linprog_result.x[var_end_idx: var_end_idx+1]
        working_time[task_idx, workers_assigment[task_idx]!=0] = var_values

    return working_time, linprog_result.fun


def main() -> int:
    per_task_time=numpy.array([0.1, 0.2, 0.3, 0.4, 0.5])
    workers_assigment=numpy.array([
        [1, 1, 0],
        [1, 0, 0],
        [1, 0, 1],
        [1, 1, 1],
        [0, 1, 1]
    ])
    solve(per_task_time, workers_assigment)


if __name__ == '__main__':
    sys.exit(main())
