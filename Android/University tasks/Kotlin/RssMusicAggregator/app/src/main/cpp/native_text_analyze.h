//
// Created by developer on 10/25/16.
//

#include <jni.h>
#include <time.h>
#include <android/log.h>
#include <vector>
#include <algorithm>

#ifndef RSSMUSICAGGREGATOR_NATEVI_TEXT_ANALYZE_H
#define RSSMUSICAGGREGATOR_NATEVI_TEXT_ANALYZE_H
extern "C" {
JNIEXPORT jobjectArray
        Java_com_bulbasoft_reallylastaggregator_utils_ndk_JavaNativeTextAnalyzer_QSort(
        JNIEnv *env, jclass cls, jobjectArray arr);
};
#endif //RSSMUSICAGGREGATOR_NATEVI_TEXT_ANALYZE_H
