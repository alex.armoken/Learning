//
// Created by developer on 10/25/16.
//

#include "native_text_analyze.h"

jclass arrClass;
jmethodID arrGet;
jmethodID arrSet;
jmethodID arrSize;

jclass newsClass;
jfieldID wordsCount;



int news_cmp(JNIEnv *env, jobjectArray arr, jsize aNum, jsize bNum) {
    jobject a = env->GetObjectArrayElement(arr, aNum);
    jobject b = env->GetObjectArrayElement(arr, bNum);

    jlong  aCount = (jlong)env->GetObjectField(a, wordsCount);
    jlong  bCount = (jlong)env->GetObjectField(b, wordsCount);

    env->DeleteLocalRef(a);
    env->DeleteLocalRef(b);

    return (aCount > bCount) ? -1 : (aCount < bCount) ? 1 : 0;
}

void news_swap(JNIEnv *env, jobjectArray arr, jsize i, jsize j) {
    jobject a = env->GetObjectArrayElement(arr, i);
    jobject b = env->GetObjectArrayElement(arr, j);

    env->SetObjectArrayElement(arr, j, a);
    env->SetObjectArrayElement(arr, i, b);

    env->DeleteLocalRef(a);
    env->DeleteLocalRef(b);
}


jsize partition(JNIEnv *env, jobjectArray arr, jsize start, jsize end) {
    jsize marker = start;
    for (jsize i = start; i <= end; i++) {
        if (news_cmp(env, arr, i, end) == -1) {
            news_swap(env, arr, marker, i);
            marker++;
        }
    }
    return marker;
}


void qs(JNIEnv *env, jobjectArray arr, jsize start, jsize end) {
    if (start < end) {
        jsize pivot = partition(env, arr, start, end);
        qs(env, arr, start, pivot - 1);
        qs(env, arr, pivot + 1, end);
    }
}

void init(JNIEnv *env) {
    arrClass = static_cast<jclass>(env->NewGlobalRef(
            env->FindClass("java/util/List")));
    arrSize = env->GetMethodID (arrClass, "size", "()I");
    arrGet = env->GetMethodID(arrClass, "get", "(I)Ljava/lang/Object;");
    arrSet = env->GetMethodID(arrClass, "set", "(ILjava/lang/Object;)Ljava/lang/Object;");

    newsClass = static_cast<jclass>(env->NewGlobalRef(
            env->FindClass("com/bulbasoft/reallylastaggregator/mvp/models/rss/News")));
    wordsCount = env->GetFieldID(newsClass, "wordsCount", "J");
}

JNIEXPORT jobjectArray
Java_com_bulbasoft_reallylastaggregator_utils_ndk_JavaNativeTextAnalyzer_QSort(
        JNIEnv *env, jclass cls, jobjectArray arr) {

    init(env);
    jsize size = env->GetArrayLength(arr);
    qs(env, arr, 0, size - 1);
    return arr;
}