package com.bulbasoft.reallylastaggregator.ui.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.ImageButton
import com.bulbasoft.reallylastaggregator.R
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bulbasoft.reallylastaggregator.mvp.views.MusicView
import com.bulbasoft.reallylastaggregator.mvp.models.music.Song
import com.bulbasoft.reallylastaggregator.ui.adapters.SongAdapter
import com.bulbasoft.reallylastaggregator.mvp.presenters.MusicPresenter
import com.bulbasoft.reallylastaggregator.mvp.common.MvpAppCompatActivity

class MusicActivity : MvpAppCompatActivity(), MusicView {
    @InjectPresenter lateinit var presenter: MusicPresenter

    lateinit var songRv: RecyclerView
    lateinit var currentSongTv: TextView
    lateinit var nextSongButton: ImageButton
    lateinit var playOrPauseButton: ImageButton
    lateinit var previousSongButton: ImageButton
    lateinit var reloadSongsListButton: ImageButton


    private fun findButtons() {
        nextSongButton = findViewById(R.id.song_next_button) as ImageButton
        nextSongButton.setOnClickListener { presenter.playNextSong() }

        previousSongButton = findViewById(R.id.song_previous_button) as ImageButton
        previousSongButton.setOnClickListener { presenter.playPreviousSong() }

        playOrPauseButton = findViewById(R.id.music_pause) as ImageButton
        playOrPauseButton.setOnClickListener { presenter.playOrPauseSong() }

        reloadSongsListButton = findViewById(R.id.reload_songs_list) as ImageButton
        reloadSongsListButton.setOnClickListener { presenter.reloadContent() }
    }

    private fun findRecycleView() {
        songRv = findViewById(R.id.music_song_list) as RecyclerView
        songRv.layoutManager = LinearLayoutManager(this)
        ItemTouchHelper(SongAdapter.SimpleTouchCallback()).attachToRecyclerView(songRv)
    }

    private fun findViews() {
        findButtons()
        findRecycleView()
        currentSongTv = findViewById(R.id.music_current_song_title) as TextView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music)
        findViews()
    }



    override fun reloadSongListView(list: MutableList<Song>) {
        songRv.adapter = SongAdapter(presenter.songDao, list, presenter)
        songRv.adapter.notifyDataSetChanged()
    }

    override fun updateSongListView() {
        songRv.adapter.notifyDataSetChanged()
    }

    override fun updateCurrentSongView(song: Song?) {
        currentSongTv.text = song?.title ?: ""
    }

    override fun updatePlayOrPauseButton(isPause: Boolean) {
        if (isPause) {
            playOrPauseButton.setImageResource(R.drawable.ic_pause_white_24dp)
        } else {
            playOrPauseButton.setImageResource(R.drawable.ic_play_arrow_white_24dp)
        }
    }

    override fun setReloadPossibility(state: Boolean) {
        reloadSongsListButton.isEnabled = state
    }


    override fun onStart() {
        super.onStart()
        presenter.connectToMusicService(applicationContext)
    }

    override fun onStop() {
        super.onStop()
        presenter.disconnectFromMusicService(applicationContext)
    }


    override fun onCreateOptionsMenu(menu: Menu) = true
    override fun onOptionsItemSelected(menuItem: MenuItem) = true
}
