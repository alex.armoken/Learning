package com.bulbasoft.reallylastaggregator.mvp.presenters

import javax.inject.Inject
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.InjectViewState
import com.bulbasoft.reallylastaggregator.NewsApplication
import com.bulbasoft.reallylastaggregator.mvp.models.rss.News
import com.bulbasoft.reallylastaggregator.mvp.views.NewsView
import com.bulbasoft.reallylastaggregator.mvp.models.rss.NewsDao


@InjectViewState
class NewsPresenter : MvpPresenter<NewsView>() {
    @Inject lateinit var newsDao: NewsDao

    var currentNews: Int = -1
    var currentCategory: Long = -1
    lateinit var news: List<News>

    init {
         NewsApplication.graph.inject(this)
    }

    fun loadNews(categoryID: Long) {
        if (categoryID != currentCategory) {
            currentCategory = categoryID
            news = newsDao.LoadNewsFromCategory(currentCategory)
            viewState.connectAdapter(news)
        }
    }
}

