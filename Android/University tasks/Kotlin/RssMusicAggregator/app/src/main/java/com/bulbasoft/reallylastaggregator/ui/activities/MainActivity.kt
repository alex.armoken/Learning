package com.bulbasoft.reallylastaggregator.ui.activities

import android.os.Bundle
import android.view.View
import android.content.Intent
import android.widget.TextView
import android.view.animation.Animation
import android.support.v7.widget.Toolbar
import com.bulbasoft.reallylastaggregator.R
import com.melnykov.fab.FloatingActionButton
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bulbasoft.reallylastaggregator.mvp.views.MainView
import com.bulbasoft.reallylastaggregator.mvp.models.Category
import com.bulbasoft.reallylastaggregator.ui.adapters.CategoryAdapter
import com.bulbasoft.reallylastaggregator.mvp.presenters.MainPresenter
import com.bulbasoft.reallylastaggregator.mvp.common.MvpAppCompatActivity

class MainActivity : MvpAppCompatActivity(), MainView {
    @InjectPresenter lateinit var presenter: MainPresenter

    private lateinit var toolbar: Toolbar
    private lateinit var fab: FloatingActionButton
    private lateinit var tvCategoriesIsEmpty : TextView
    private lateinit var rvCategoriesList : RecyclerView


    fun findRecycleView() {
        rvCategoriesList = findViewById(R.id.rvCategoriesList) as RecyclerView
        rvCategoriesList.layoutManager = LinearLayoutManager(this)
    }

    fun findFab() {
        fab = findViewById(R.id.fab) as FloatingActionButton
        fab.attachToRecyclerView(rvCategoriesList)
        fab.setOnClickListener { presenter.fabAction(this) }
    }

    fun findToolbar() {
        toolbar = findViewById(R.id.main_toolbar) as Toolbar
        setSupportActionBar(toolbar)
    }

    fun findViews() {
        tvCategoriesIsEmpty = findViewById(R.id.tvCategoriesIsEmpty) as TextView


        findRecycleView()
        findFab()
        findToolbar()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViews()
        presenter.DataLoadContext = applicationContext
    }



    override fun updateRecycleViewState() {
        rvCategoriesList.adapter.notifyDataSetChanged()
        if (rvCategoriesList.adapter.itemCount == 0) {
            rvCategoriesList.visibility = View.GONE
            tvCategoriesIsEmpty.visibility = View.VISIBLE
        } else {
            rvCategoriesList.visibility = View.VISIBLE
            tvCategoriesIsEmpty.visibility = View.GONE
        }
    }

    override fun updateFabViewState(animation: Animation?, fabIconId: Int?) {
        if (animation != null) {
            fab.startAnimation(animation)
        } else {
            fab.setImageResource(fabIconId!!)
        }
    }


    override fun onResume() {
        super.onResume()
        updateRecycleViewState()
    }


    override fun onCategoriesLoaded(categories: List<Category>) {
        rvCategoriesList.adapter = CategoryAdapter(categories, presenter, this)
        updateRecycleViewState()
    }

    override fun onCategoryDeleted() {
        presenter.updateSelectionState()
    }



    private fun getNewCategoryActivityResult(resultCode: Int, data: Intent) {
        if (resultCode == 0) {
            val newCategoryId = data.getLongExtra("id", -1)
            if (newCategoryId >= 0) {
                presenter.updateCategoryListWith(newCategoryId)
            }
        }
    }

    private fun getNewsListActivityResult(resultCode: Int, data: Intent) {
        if (resultCode == 0) {
            presenter.LastNewsTitle = data.getStringExtra("news_title")
        }
    }

    override fun changeTitle(newTitle: String) {
        title = newTitle
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == 1) {
            getNewCategoryActivityResult(resultCode, data)
        } else if (requestCode == 2) {
            getNewsListActivityResult(resultCode, data)
        }
    }
}
