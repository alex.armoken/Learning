package com.bulbasoft.reallylastaggregator.ui.adapters

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.ImageView
import android.view.LayoutInflater
import com.bulbasoft.reallylastaggregator.R
import android.support.v7.widget.RecyclerView
import com.bulbasoft.reallylastaggregator.mvp.models.rss.News
import com.bulbasoft.reallylastaggregator.mvp.presenters.NewsListPresenter

class NewsAdapter(private var newsList: List<News>,
                  private var presenter: NewsListPresenter,
                  private var activity: Activity) :
        RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var NewsId: Int = 0
        var Name: TextView = itemView.findViewById(R.id.tvItemNewsName) as TextView
        var Image: ImageView = itemView.findViewById(R.id.ivItemNewsImage) as ImageView
        var Description = itemView.findViewById(R.id.tvItemNewsDescription) as TextView
    }


    override fun getItemCount(): Int = newsList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return NewsAdapter.ViewHolder(LayoutInflater.from(parent.context).
                inflate(R.layout.item_news, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        val news = newsList[pos]
        holder.NewsId = pos
        holder.Name.text = news.title
        holder.Description.text = news.description
        holder.Image.setImageBitmap(news.littleImage)
        holder.itemView.setOnClickListener { v: View -> presenter.openNews(activity, pos) }
    }
}