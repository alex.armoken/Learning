package com.bulbasoft.reallylastaggregator.mvp.presenters

import java.util.*
import javax.inject.Inject
import android.app.Activity
import android.content.Intent
import android.content.Context
import android.view.animation.Animation
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.InjectViewState
import com.bulbasoft.reallylastaggregator.R
import android.view.animation.AnimationUtils
import com.bulbasoft.reallylastaggregator.NewsApplication
import com.bulbasoft.reallylastaggregator.utils.java.QSort
import com.bulbasoft.reallylastaggregator.mvp.models.Category
import com.bulbasoft.reallylastaggregator.mvp.models.rss.News
import com.bulbasoft.reallylastaggregator.mvp.models.rss.NewsDao
import com.bulbasoft.reallylastaggregator.mvp.models.CategoryDao
import com.bulbasoft.reallylastaggregator.mvp.views.NewsListView
import com.bulbasoft.reallylastaggregator.mvp.models.NewsSourceDao
import com.bulbasoft.reallylastaggregator.ui.activities.NewsActivity
import com.bulbasoft.reallylastaggregator.utils.ndk.JavaNativeTextAnalyzer
import com.bulbasoft.reallylastaggregator.mvp.models.rss.OnNewsLoadListener
import com.bulbasoft.reallylastaggregator.mvp.models.rss.NewsFeedsDownloader

@InjectViewState
class NewsListPresenter : MvpPresenter<NewsListView>(), OnNewsLoadListener {
    @Inject lateinit var newsDao: NewsDao
    @Inject lateinit var categoryDao: CategoryDao
    @Inject lateinit var newsSourcesDao: NewsSourceDao

    companion object {
        val UNSORTED_LIST = 0
        val SORTED_BY_TITLE = 1

        val NDK_SORT = 2
        val JAVA_SORT = 3
    }

    init {
        NewsApplication.graph.inject(this)
    }

    var IsNewsLoading: Boolean = false
    private var curCategoryId: Long = -1
    private lateinit var rotate: Animation
    private var loadSortType: Int = UNSORTED_LIST
    private var analyzeSortType: Int = NDK_SORT

    var AnalyzeSortType: Int
        get() = this.analyzeSortType
        set(value) {
            this.analyzeSortType = value
            viewState.changeSortedState(loadSortType, analyzeSortType)
        }

    private lateinit var urlList: List<String>
    var lastNewsTitle: String = "No news read yet"
    private lateinit var newsList: MutableList<News>
    private lateinit var newsFeedsDownloader: NewsFeedsDownloader


    private fun loadOneCategory() {
        if (loadSortType == UNSORTED_LIST) {
            newsList = newsDao.LoadNewsFromCategory(curCategoryId)
        } else if (loadSortType == SORTED_BY_TITLE) {
            newsList = newsDao.LoadNewsFromCategorySortByTitle(curCategoryId)
        }
    }

    private fun loadAllCategories() {
        if (loadSortType == UNSORTED_LIST) {
            newsList = newsDao.LoadNewsFromAllCategories()
        } else if (loadSortType == SORTED_BY_TITLE) {
            newsList = newsDao.LoadNewsFromAllCategoriesSortedByTitle()
        }
    }

    fun loadNewsList() {
        when (curCategoryId >= 0) {
            true -> loadOneCategory()
            false -> loadAllCategories()
        }
        viewState.onNewsLoaded(newsList)
    }

    var SortType: Int
        get() = this.loadSortType
        set(value) {
            this.loadSortType = value
            viewState.changeSortedState(loadSortType, analyzeSortType)
            loadNewsList()
        }


    private var context: Context? = null
    var DataLoadContext: Context?
        get() = this.context
        set(value) {
            if (this.context == null) {
                this.context = value
                rotate = AnimationUtils.loadAnimation(context, R.anim.rotate)
            }
        }


    private fun loadUrls(categoryID: Long) : List<List<String>> {
        val result = ArrayList<List<String>>()
        if (categoryID == -1L) {
            categoryDao.LoadAllCategories().forEach { category ->
                result.add(newsSourcesDao.GetNewsSourcesForCategory(category.id).map {el -> el.Url})
            }
        } else {
            result.add(newsSourcesDao.GetNewsSourcesForCategory(categoryID).map { el -> el.Url})
        }
        return result
    }

    private fun initFeedsDownloader() {
        val categories: MutableList<Category>
        if (curCategoryId == -1L) {
            categories = categoryDao.LoadAllCategories()
        } else {
            categories = ArrayList<Category>()
            categories.add(categoryDao.GetCategoryById(curCategoryId))
        }
        val urls = loadUrls(curCategoryId)
        newsFeedsDownloader = NewsFeedsDownloader(categories, urls, newsDao, this)
    }

    fun reloadNewsList() {
        if (!IsNewsLoading) {
            newsDao.DeleteAllNewsFromCategory(curCategoryId)
            newsList = ArrayList<News>()

            initFeedsDownloader()
            viewState.onNewsLoaded(newsList)
            newsFeedsDownloader.execute()
            viewState.updateLoadingButtonState(rotate)
        }
    }

    var CurCategoryId: Long
        get() = this.curCategoryId
        set(value) {
            curCategoryId = value
            if (curCategoryId >= 0) {
                urlList = newsSourcesDao.GetNewsSourcesForCategory(curCategoryId).
                        map { el -> el.Url }
            } else {
                urlList = newsSourcesDao.LoadAllNewsSources().map { el -> el.Url }
            }
        }


    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        viewState.onPresenterLoaded()
    }


    fun openNews(activity: Activity, pos: Int) {
        val intent = Intent(activity, NewsActivity::class.java)
        intent.putExtra("news_id", pos)
        intent.putExtra("category_id", curCategoryId)
        activity.startActivityForResult(intent, 1)
    }

    private fun countWords(word: String) {
        for (news: News in newsList) {
            news.wordsCount = 0
            for (w: String in news.description.split(" ")) {
                if (w == word) {
                    news.wordsCount++
                }
            }
        }
    }


    private fun sortNewsByWordFrequencyJava(word: String): MutableList<News> {
        countWords(word)
        QSort().sort(newsList, { a: News, b: News ->
            if (a.wordsCount > b.wordsCount) {
                -1
            } else if (a.wordsCount < b.wordsCount) {
                1
            } else {
                0
            }
        })
        return newsList
    }

    private fun sortNewsByWordFrequencyNDK(word: String): MutableList<News> {
        countWords(word)
        val result = JavaNativeTextAnalyzer().QSort(newsList.toTypedArray())
        return ArrayList(result.toList())
    }

    fun sortNewsByWordFrequency(word: String) {
        val startTime = System.currentTimeMillis()
        when (analyzeSortType) {
            NDK_SORT -> { newsList = sortNewsByWordFrequencyNDK(word) }
            JAVA_SORT -> { newsList = sortNewsByWordFrequencyJava(word) }
        }
        val endTime = System.currentTimeMillis()

        viewState.onNewsLoaded(newsList)
        viewState.showTimer(endTime - startTime)
    }


    fun stopLoadingNews() {
        if (IsNewsLoading) {
            newsFeedsDownloader.cancel(false)
        }
        IsNewsLoading = false
    }


    override fun onNewsItemUpdate(news: News) {
        newsList.add(news)
        IsNewsLoading = true
        viewState.onUpdateNewsList()
        viewState.updateLoadingButtonState(rotate)
    }

    override fun onSuccessfulLoadingNews() {
        IsNewsLoading = false
    }
}