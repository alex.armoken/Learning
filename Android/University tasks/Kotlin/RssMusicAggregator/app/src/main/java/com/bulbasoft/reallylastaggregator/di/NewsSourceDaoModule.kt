package com.bulbasoft.reallylastaggregator.di

import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import com.bulbasoft.reallylastaggregator.mvp.models.NewsSourceDao

@Module
class NewsSourceDaoModule {
    @Provides
    @Singleton
    fun provideNewsSourceDao() : NewsSourceDao = NewsSourceDao()
}