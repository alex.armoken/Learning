package com.bulbasoft.reallylastaggregator.ui.activities

import android.os.Bundle
import android.view.Menu
import android.content.Intent
import android.support.v4.view.PagerAdapter
import android.widget.ImageButton
import android.support.v7.widget.Toolbar
import android.support.v4.view.ViewPager
import com.bulbasoft.reallylastaggregator.R
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bulbasoft.reallylastaggregator.mvp.views.NewsView
import com.bulbasoft.reallylastaggregator.mvp.presenters.NewsPresenter
import com.bulbasoft.reallylastaggregator.mvp.common.MvpAppCompatActivity
import com.bulbasoft.reallylastaggregator.mvp.models.rss.News
import com.bulbasoft.reallylastaggregator.ui.adapters.NewsFragmentPagerAdapter
import org.jetbrains.anko.support.v4.onPageChangeListener

class NewsActivity : MvpAppCompatActivity(), NewsView {
    @InjectPresenter lateinit var presenter: NewsPresenter

    private lateinit var viewPager: ViewPager
    private lateinit var adapter: NewsFragmentPagerAdapter

    fun exitWithoutResult() {
        val intent: Intent = Intent(applicationContext, NewCategoryActivity::class.java)
        intent.putExtra("news_title", presenter.news[presenter.currentNews].title)
        setResult(0, intent)

        finish()
    }

    private fun findToolbar() {
        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        toolbar.setNavigationOnClickListener { v -> exitWithoutResult() }
    }

    private fun findPager() {
        viewPager = findViewById(R.id.container) as ViewPager
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(arg0: Int) {}
            override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {}

            override fun onPageSelected(pos: Int) {
                presenter.currentNews = pos
            }
        })
    }

    private fun findButtons() {
        (findViewById(R.id.news_next_button) as ImageButton).setOnClickListener { v ->
            viewPager.setCurrentItem(++presenter.currentNews, true)
        }
        (findViewById(R.id.news_previous_button) as ImageButton).setOnClickListener { v ->
            viewPager.setCurrentItem(--presenter.currentNews, true)
        }
    }

    private fun findViews() {
        findToolbar()
        findButtons()
        findPager()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)
        findViews()

        this.presenter.currentNews = intent.getIntExtra("news_id", 0)
        presenter.loadNews(intent.getLongExtra("category_id", -1))
    }


    override fun connectAdapter(news: List<News>) {
        adapter = NewsFragmentPagerAdapter(news.size, news, supportFragmentManager)
        viewPager.adapter = adapter
        viewPager.currentItem = this.presenter.currentNews
        adapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean = true
}
