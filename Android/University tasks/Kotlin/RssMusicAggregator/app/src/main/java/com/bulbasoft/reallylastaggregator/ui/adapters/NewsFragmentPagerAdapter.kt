package com.bulbasoft.reallylastaggregator.ui.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.bulbasoft.reallylastaggregator.mvp.models.rss.News
import com.bulbasoft.reallylastaggregator.mvp.models.rss.NewsDao
import com.bulbasoft.reallylastaggregator.ui.fragments.NewsFragment

class NewsFragmentPagerAdapter(var page_count: Int,
                               var news: List<News>,
                               fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(pos: Int): Fragment {
        return NewsFragment.newInstance(news[pos])
    }

    override fun getCount(): Int = page_count

    override fun getPageTitle(pos: Int): CharSequence? = news[pos].title
}