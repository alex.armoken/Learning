package com.bulbasoft.reallylastaggregator.mvp.models

import java.util.*
import com.activeandroid.Model
import java.text.SimpleDateFormat
import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import com.activeandroid.annotation.Table
import com.activeandroid.annotation.Column

@Table(name = "Categories")
class Category() : Model() {
    @Column var Name: String = ""
    @Column var ViewingDate: Date = Date()
    @Column var IsFavourite: Boolean = false
    var IsSelected: Boolean = false

    constructor(name: String, viewingDate: Date, isSelected: Boolean) : this() {
        Name = name
        ViewingDate = viewingDate
        IsFavourite = isSelected
    }

    private val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")

    private fun formatDate(date: Date): String = dateFormat.format(date)

    fun formatedDate(): String = formatDate(this.ViewingDate)

    fun GetInfo(): String {
        return "Название:\n$Name\n" + "Время изменения:\n${formatDate(ViewingDate)}"
    }
}

class CategoryDao {
    fun CreateCategory(name: String, viewingDate: Date, isSelected: Boolean): Category  {
        return Category(name, viewingDate, isSelected)
    }

    fun CreateAndSaveCategory(name: String, viewingDate: Date, isSelected: Boolean): Category  {
        val category = Category(name, viewingDate, isSelected)
        category.save()
        return category
    }

    fun SaveCategory(category: Category) {
        category.save()
    }

    fun LoadAllCategories() : MutableList<Category> {
        return Select().from(Category::class.java).execute<Category>()
    }

    fun GetCategoryById(id: Long): Category {
        return Select().from(Category::class.java).where("id = ?", id).executeSingle<Category>()
    }

    fun DeleteAllCategories() {
        Delete().from(Category::class.java).execute<Category>()
    }

    fun DeleteCategory(category: Category) {
        category.delete()
    }
}
