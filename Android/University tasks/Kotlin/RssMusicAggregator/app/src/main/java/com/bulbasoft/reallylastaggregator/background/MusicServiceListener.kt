package com.bulbasoft.reallylastaggregator.background

interface MusicServiceListener {
    fun setCurrentSongView(songID: Int)

    fun setPlayOrPauseButtonState(isPaused: Boolean)
}