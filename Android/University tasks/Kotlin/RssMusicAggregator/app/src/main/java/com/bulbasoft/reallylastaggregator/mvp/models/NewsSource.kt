package com.bulbasoft.reallylastaggregator.mvp.models

import com.activeandroid.Model
import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import com.activeandroid.annotation.Table
import com.activeandroid.annotation.Column

@Table(name = "NewsSource")
class NewsSource() : Model() {
    @Column var Name: String = ""
    @Column var Url: String = ""
    @Column(onDelete = Column.ForeignKeyAction.CASCADE) var Category: Category = Category()
    var IsSelected: Boolean = false

    constructor(name: String, url: String, category: Category) : this() {
        this.Name = name
        this.Url = url
        this.Category = category
    }
}


class NewsSourceDao {
    fun CreateNewsSource(name: String, url: String, category: Category) : NewsSource {
        val newsSource = NewsSource(name, url, category)
        newsSource.save()
        return newsSource
    }

    fun SaveNewsSource(newsSource: NewsSource) {
        newsSource.save()
    }

    fun LoadAllNewsSources(): MutableList<NewsSource> {
        return Select().from(NewsSource::class.java).execute<NewsSource>()
    }

    fun GetNewsSourcesForCategory(id: Long) : MutableList<NewsSource> {
        return Select().from(NewsSource::class.java).where("category = ?", id).execute()
    }

    fun GetNewsSourcesById(id: Int): NewsSource {
        return Select().from(NewsSource::class.java).where("id = ?", id).executeSingle<NewsSource>()
    }

    fun DeleteAllNewsSources() {
        Delete().from(NewsSource::class.java).execute<NewsSource>()
    }

    fun DeleteNewsSouces(newsSource: NewsSource) {
        newsSource.delete()
    }
}