package com.bulbasoft.reallylastaggregator.mvp.models.music


interface OnSongLoadListener {
    fun onSongListUpdate(song: Song)

    fun onEndLoading()
}