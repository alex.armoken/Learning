package com.bulbasoft.reallylastaggregator.mvp.models.rss

interface OnNewsLoadListener {
    fun onNewsItemUpdate(news: News)

    fun onSuccessfulLoadingNews()
}