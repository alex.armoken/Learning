package com.bulbasoft.reallylastaggregator.mvp.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.bulbasoft.reallylastaggregator.mvp.models.rss.News

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface NewsView : MvpView {
    fun connectAdapter(news: List<News>)
}