package com.bulbasoft.reallylastaggregator.mvp.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.bulbasoft.reallylastaggregator.mvp.models.music.Song

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface MusicView : MvpView {
    fun reloadSongListView(list: MutableList<Song>)

    fun updateSongListView()

    fun updateCurrentSongView(song: Song?)

    fun updatePlayOrPauseButton(isPause: Boolean)

    fun setReloadPossibility(state: Boolean)
}