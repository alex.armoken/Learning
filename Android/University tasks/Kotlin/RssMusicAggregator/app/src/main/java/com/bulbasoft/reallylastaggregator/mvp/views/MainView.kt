package com.bulbasoft.reallylastaggregator.mvp.views

import com.arellomobile.mvp.MvpView
import android.view.animation.Animation
import com.bulbasoft.reallylastaggregator.mvp.models.Category
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface MainView : MvpView {
    fun onCategoriesLoaded(categories: List<Category>)

    fun onCategoryDeleted()

    fun changeTitle(newTitle: String)

    fun updateFabViewState(animation: Animation?, fabIconId: Int?)

    fun updateRecycleViewState()
}