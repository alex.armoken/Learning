package com.bulbasoft.reallylastaggregator.background

import android.os.Binder
import android.os.IBinder
import android.os.Handler
import android.os.Message
import javax.inject.Inject
import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import com.bulbasoft.reallylastaggregator.R
import android.support.v4.app.NotificationCompat
import java.util.concurrent.ConcurrentLinkedQueue
import com.bulbasoft.reallylastaggregator.NewsApplication
import com.bulbasoft.reallylastaggregator.mvp.models.music.SongDao

class MusicService : Service() {
    var currentSongID: Int = -1

    class MusicHandler(private var listener: MusicServiceListener): Handler() {
        companion object {
            val PLAY_VALUE = 1
            val PAUSE_VALUE = 0
            val CHANGE_BUTTON_VIEW = 0
            val CHANGE_CURRENT_SONG_VIEW = 1
        }

        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            if (msg.arg1 == CHANGE_CURRENT_SONG_VIEW) {
                listener.setCurrentSongView(msg.arg2)
            } else if (msg.arg1 == CHANGE_BUTTON_VIEW) {
                listener.setPlayOrPauseButtonState(msg.arg2 == PLAY_VALUE)
            }
        }
    }
    var handler: MusicHandler? = null



    class Player(private var songDao: SongDao,
            // First element is task type, second is value for task
                 var taskQueue: ConcurrentLinkedQueue<Pair<Int, Int>>,
                 private var service: MusicService) :
            Thread(), MediaPlayer.OnCompletionListener {

        companion object {
            // Task types
            val ABSOLUTE_STOP: Int = 0
            val STOP: Int = 1
            val PLAY_SELECTED: Int = 2
            val PLAY_OR_PAUSE: Int = 3
            val NEXT: Int = 4
            val PREVIOUS: Int = 5
            val UPDATE_VIEW: Int = 6
        }

        var isPaused = true
        var curSongTimeProgress: Int = 0
        var audioPlayer: MediaPlayer? = MediaPlayer()

        init {
            audioPlayer!!.setOnCompletionListener(this)
        }

        private fun createOrResetAudioPlayer() {
            if (audioPlayer != null) {
                audioPlayer!!.reset()
            } else {
                audioPlayer = MediaPlayer()
            }
        }

        private fun startPlayingCurrent() {
            isPaused = false
            createOrResetAudioPlayer()
            curSongTimeProgress = 0
            val song = songDao.GetSongBySerialNumber(service.currentSongID)
            if (song != null) {
                audioPlayer!!.setDataSource(song.title)
                audioPlayer!!.prepare()
                audioPlayer!!.start()
            }
        }

        private fun playOrPause() {
            if (service.currentSongID >= 0) {
                if (isPaused) {
                    isPaused = false
                    audioPlayer!!.seekTo(curSongTimeProgress)
                    audioPlayer!!.start()
                } else {
                    isPaused = true
                    audioPlayer!!.pause()
                    curSongTimeProgress = audioPlayer!!.currentPosition
                }
            }
        }

        private fun stopPlaying(task: Pair<Int, Int>) {
            isPaused = true
            service.currentSongID = -1
            audioPlayer!!.release()
            audioPlayer = null
        }

        private fun absoluteStopPlaying(task: Pair<Int, Int>) {
            stopPlaying(task)
            service.stopSelf(task.second)
        }

        private fun playNextOrPrevious(task: Pair<Int, Int>) {
            if (service.currentSongID >= 0) {
                service.currentSongID += if (task.first == NEXT) 1 else -1
                with(service) {
                    if (songDao.GetSongsCount() < currentSongID || currentSongID < 0) {
                        currentSongID = 0
                    }
                }
                startPlayingCurrent()
            }
        }


        private fun sendCurrentSongIdToView() {
            if (service.handler != null) {
                val msg = Message()
                msg.arg1 = MusicHandler.CHANGE_CURRENT_SONG_VIEW
                msg.arg2 = service.currentSongID
                service.handler!!.sendMessage(msg)
            }
        }

        private fun sendCurrentPauseStateToView() {
            if (service.handler != null) {
                val msg = Message()
                msg.arg1 = MusicHandler.CHANGE_BUTTON_VIEW
                msg.arg2 = if (isPaused) MusicHandler.PAUSE_VALUE else MusicHandler.PLAY_VALUE
                service.handler!!.sendMessage(msg)
            }
        }

        private fun viewUpdate() {
            sendCurrentPauseStateToView()
            sendCurrentSongIdToView()
        }

        private fun performTheTask(task: Pair<Int, Int>): Boolean {
            when (task.first) {
                PLAY_SELECTED -> { startPlayingCurrent(); viewUpdate() }
                PLAY_OR_PAUSE -> { playOrPause(); viewUpdate()}
                NEXT, PREVIOUS -> { playNextOrPrevious(task); viewUpdate() }
                UPDATE_VIEW -> { viewUpdate() }
                STOP -> { stopPlaying(task); viewUpdate() }
                ABSOLUTE_STOP -> { stopPlaying(task); viewUpdate(); return false }
            }
            return true
        }

        override fun run() {
            while (true) {
                if (taskQueue.isEmpty()) {
                    sleep(1)
                } else {
                    val taskResult = performTheTask(taskQueue.poll())
                    if (!taskResult) return
                }
            }
        }

        override fun onCompletion(mp: MediaPlayer?) {
            taskQueue.add(Pair(NEXT, 0))
        }
    }
    private lateinit var player: Player



    class LocalMusicBinder(private var service: MusicService) : Binder() {
        fun getService(): MusicService {
            return service
        }
    }
    private var binder: IBinder = LocalMusicBinder(this)



    @Inject lateinit var songDao: SongDao
    private var taskQueue: ConcurrentLinkedQueue<Pair<Int, Int>> = ConcurrentLinkedQueue()
    lateinit var notificationBuilder: NotificationCompat.Builder


    init {
        NewsApplication.graph.inject(this)
        notificationBuilder = NotificationCompat.Builder(this).
                setSmallIcon(R.drawable.ic_play_circle_filled_white_24dp).
                setContentTitle("Strange player").
                setContentText("KEK")
    }


    override fun onBind(intent: Intent?): IBinder = binder


    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        player = Player(songDao, taskQueue, this)
        player.start()
        startForeground(1488, notificationBuilder.build())
        return START_STICKY
    }

    fun playSelectedSong(songID: Int) {
        this.currentSongID = songID
        taskQueue.add(Pair(Player.PLAY_SELECTED, songID))
    }
    fun playOrPause() = taskQueue.add(Pair(Player.PLAY_OR_PAUSE, 0))
    fun playNextSong() = taskQueue.add(Pair(Player.NEXT, 0))
    fun playPreviousSong() = taskQueue.add(Pair(Player.PREVIOUS, 0))
    fun stopPlaying() = taskQueue.add(Pair(Player.STOP, 0))
    fun absoluteStopPlaying() = taskQueue.add(Pair(Player.ABSOLUTE_STOP, 0))
    fun updateView() = taskQueue.add(Pair(Player.UPDATE_VIEW, 0))


    override fun onDestroy() {
        super.onDestroy()
        taskQueue.add(Pair(Player.STOP, 0))
    }
}