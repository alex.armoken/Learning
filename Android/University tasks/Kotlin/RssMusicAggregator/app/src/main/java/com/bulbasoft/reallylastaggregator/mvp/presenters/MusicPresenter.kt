package com.bulbasoft.reallylastaggregator.mvp.presenters

import java.util.*
import android.os.IBinder
import javax.inject.Inject
import android.content.Intent
import android.content.Context
import android.app.ActivityManager
import android.content.ComponentName
import com.arellomobile.mvp.MvpPresenter
import android.content.ServiceConnection
import com.arellomobile.mvp.InjectViewState
import com.bulbasoft.reallylastaggregator.NewsApplication
import com.bulbasoft.reallylastaggregator.mvp.views.MusicView
import com.bulbasoft.reallylastaggregator.mvp.models.music.Song
import com.bulbasoft.reallylastaggregator.background.MusicService
import com.bulbasoft.reallylastaggregator.mvp.models.music.SongDao
import com.bulbasoft.reallylastaggregator.mvp.models.music.SongFinder
import com.bulbasoft.reallylastaggregator.background.MusicServiceListener
import com.bulbasoft.reallylastaggregator.mvp.models.music.OnSongLoadListener

@InjectViewState
class MusicPresenter : MvpPresenter<MusicView>(), OnSongLoadListener, MusicServiceListener {
    private var isBound = false
    private lateinit var context: Context
    @Inject lateinit var songDao: SongDao

    private lateinit var musicService: MusicService
    private var handler: MusicService.MusicHandler? = null
    private val connection = object : ServiceConnection {
        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            val binder = service as MusicService.LocalMusicBinder
            musicService = binder.getService()
            isBound = true

            handler = MusicService.MusicHandler(this@MusicPresenter)
            musicService.handler = handler
            musicService.updateView()
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            isBound = false
            handler = null
        }
    }

    lateinit var songFinder: SongFinder
    lateinit var songList: MutableList<Song>

    init {
        NewsApplication.graph.inject(this)
    }


    override fun onFirstViewAttach() {
        songList = songDao.LoadAllSongs()
        viewState.reloadSongListView(songList)
    }

    fun reloadContent() {
        viewState.setReloadPossibility(false)
        songDao.DeleteAllSongs()
        songList = ArrayList()
        songFinder = SongFinder(this)
        songFinder.execute()
        viewState.reloadSongListView(songList)
    }


    override fun setCurrentSongView(songID: Int) {
        viewState.updateSongListView()
        val song = if (songID >= 0) songDao.GetSongBySerialNumber(songID) else null
        viewState.updateCurrentSongView(song)
    }

    override fun setPlayOrPauseButtonState(isPaused: Boolean) {
        viewState.updatePlayOrPauseButton(isPaused)
    }


    private fun isMusicServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }

    fun connectToMusicService(context: Context) {
        this.context = context
        val intent = Intent(context, MusicService::class.java)
        if (!isMusicServiceRunning(context, MusicService::class.java)) {
            context.startService(intent)
        }

        context.bindService(intent, connection, Context.BIND_AUTO_CREATE)
    }

    fun disconnectFromMusicService(context: Context) {
        if (isBound) {
            context.unbindService(connection)
            isBound = false
        }
    }


    fun playSelectedSong(id: Int) {
        if (isBound) {
            musicService.playSelectedSong(id)
        }
    }

    fun playOrPauseSong() {
        if (isBound) {
            musicService.playOrPause()
        }
    }

    fun playNextSong() {
        if (isBound) {
            musicService.playNextSong()
        }
    }

    fun playPreviousSong() {
        if (isBound) {
            musicService.playPreviousSong()
        }
    }

    fun stopPlaying() {
        if (isBound) {
            musicService.stopPlaying()
        }
    }


    override fun onSongListUpdate(song: Song) {
        songList.add(song)
        songDao.SaveSong(song)
        viewState.updateSongListView()
    }

    override fun onEndLoading() {
        viewState.setReloadPossibility(true)
    }
}