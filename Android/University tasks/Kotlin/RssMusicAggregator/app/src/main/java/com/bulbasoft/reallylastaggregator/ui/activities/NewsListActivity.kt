package com.bulbasoft.reallylastaggregator.ui.activities

import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import android.view.MenuItem
import android.content.Intent
import android.widget.EditText
import android.widget.ImageButton
import android.view.animation.Animation
import android.support.v7.widget.Toolbar
import android.support.v7.app.AlertDialog
import com.bulbasoft.reallylastaggregator.R
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bulbasoft.reallylastaggregator.mvp.models.rss.News
import com.bulbasoft.reallylastaggregator.mvp.views.NewsListView
import com.bulbasoft.reallylastaggregator.ui.adapters.NewsAdapter
import com.bulbasoft.reallylastaggregator.mvp.common.MvpAppCompatActivity
import com.bulbasoft.reallylastaggregator.mvp.presenters.NewsListPresenter


class NewsListActivity : MvpAppCompatActivity(), NewsListView {
    @InjectPresenter lateinit var presenter: NewsListPresenter

    var ndkSortState: MenuItem? = null
    var javaSortState: MenuItem? = null
    var unsortedState: MenuItem? = null
    var sortByTitleState: MenuItem? = null

    lateinit var newsListRv: RecyclerView
    lateinit var reloadButton: ImageButton
    lateinit var sortButton: ImageButton

    fun exitWithResult() {
        val intent: Intent = Intent(applicationContext, NewsListActivity::class.java)
        intent.putExtra("news_title", presenter.lastNewsTitle)
        setResult(0, intent)
        presenter.stopLoadingNews()
        finish()
    }

    fun findRecycleView() {
        newsListRv = findViewById(R.id.news_list_rv) as RecyclerView
        newsListRv.layoutManager = LinearLayoutManager(this)
    }

    fun showWordForSortDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.word_for_sort_dialog, null)
        dialogBuilder.setView(dialogView)

        dialogBuilder.setTitle("Word for sort")
        dialogBuilder.setMessage("Please enter the word: ")

        val edit = dialogView.findViewById(R.id.word_for_sort_edittext) as EditText
        dialogBuilder.setPositiveButton("Sort", { dialog, number ->
            presenter.sortNewsByWordFrequency(edit.text.toString())
        })
        dialogBuilder.setNegativeButton("Cancel", { dialog, number -> Unit })

        val b = dialogBuilder.create()
        b.show()
    }


    fun findToolbar() {
        val toolbar = findViewById(R.id.news_list_toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
        toolbar.setNavigationOnClickListener { v -> exitWithResult() }

        reloadButton = toolbar.findViewById(R.id.reload_news_list) as ImageButton
        reloadButton.setOnClickListener { v -> presenter.reloadNewsList() }

        sortButton = toolbar.findViewById(R.id.sort_news_list) as ImageButton
        sortButton.setOnClickListener {v -> showWordForSortDialog() }
    }

    fun findAllViews() {
        findToolbar()
        findRecycleView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_list)
        findAllViews()
        presenter.DataLoadContext = applicationContext
    }



    override fun onPresenterLoaded() {
        presenter.CurCategoryId = intent.getLongExtra("category_id", -1)
        presenter.loadNewsList()
    }

    override fun onNewsLoaded(news: List<News>) {
        newsListRv.adapter = NewsAdapter(news, presenter, this)
    }

    override fun onUpdateNewsList() {
        if (!presenter.IsNewsLoading) {
            presenter.reloadNewsList()
        }
        newsListRv.adapter.notifyDataSetChanged()
    }

    override fun updateLoadingButtonState(animation: Animation?) {
        if (animation != null) {
            reloadButton.startAnimation(animation)
        } else {
            reloadButton.clearAnimation()
        }
    }



    override fun onSuccessfulLoadingNews() {
        Toast.makeText(this, "News were successfully loaded", Toast.LENGTH_LONG).show()
    }

    override fun onFailureLoadingNews(msg: String) {
        Toast.makeText(this, "There were some problems with: \n" + msg, Toast.LENGTH_LONG).show()
    }


    override fun showTimer(kek: Long) {
        Toast.makeText(this, kek.toString() + " milliseconds", Toast.LENGTH_LONG).show()
    }



    override fun onBackPressed() {
        exitWithResult()
    }

    override fun changeSortedState(state: Int, analyzeState: Int) {
        sortByTitleState!!.isChecked = if (state == NewsListPresenter.SORTED_BY_TITLE) true else false
        unsortedState!!.isChecked = if (state == NewsListPresenter.UNSORTED_LIST) true else false

        ndkSortState!!.isChecked = if (analyzeState == NewsListPresenter.NDK_SORT) true else false
        javaSortState!!.isChecked = if (analyzeState == NewsListPresenter.JAVA_SORT) true else false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.news_list, menu)
        unsortedState = menu.findItem(R.id.unsorted_menu) as MenuItem?
        sortByTitleState = menu.findItem(R.id.by_title_menu) as MenuItem?

        ndkSortState = menu.findItem(R.id.ccccc_sort) as MenuItem?
        javaSortState = menu.findItem(R.id.jjjjjava_sort) as MenuItem?
        changeSortedState(presenter.SortType, presenter.AnalyzeSortType)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.unsorted_menu -> { presenter.SortType = NewsListPresenter.UNSORTED_LIST }
            R.id.by_title_menu -> { presenter.SortType = NewsListPresenter.SORTED_BY_TITLE }
            R.id.ccccc_sort -> { presenter.AnalyzeSortType = NewsListPresenter.NDK_SORT }
            R.id.jjjjjava_sort -> { presenter.AnalyzeSortType = NewsListPresenter.JAVA_SORT }
        }
        return true
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == 1) {
            if (resultCode == 0) {
                presenter.lastNewsTitle = data.getStringExtra("news_title")
            }
        }
    }
}
