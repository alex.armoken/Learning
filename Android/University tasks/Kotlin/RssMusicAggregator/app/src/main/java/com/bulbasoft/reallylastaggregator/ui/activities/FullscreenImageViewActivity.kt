package com.bulbasoft.reallylastaggregator.ui.activities

import android.os.Bundle
import android.view.Window
import javax.inject.Inject
import android.graphics.Matrix
import android.view.MotionEvent
import android.widget.ImageView
import android.view.WindowManager
import android.view.ScaleGestureDetector
import com.bulbasoft.reallylastaggregator.R
import android.support.v7.app.AppCompatActivity
import com.bulbasoft.reallylastaggregator.NewsApplication
import com.bulbasoft.reallylastaggregator.mvp.models.rss.NewsDao


class FullscreenImageViewActivity : AppCompatActivity() {
    @Inject lateinit var newsDao: NewsDao

    private var scale = 1f
    private val matrix = Matrix()
    private lateinit var imageView: ImageView
    private lateinit var SGD: ScaleGestureDetector

    init {
        NewsApplication.graph.inject(this)
    }

    fun setBitmap() {
        imageView = findViewById(R.id.news_scale_image_view) as ImageView

        val newsID = intent.getIntExtra("news_id", -1)
        if (newsID != -1) {
            val bitmap = newsDao.GetNewsByIndex(newsID).image
            imageView.setImageBitmap(bitmap)
        }
    }

     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

         requestWindowFeature(Window.FEATURE_NO_TITLE)
         window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                 WindowManager.LayoutParams.FLAG_FULLSCREEN)

         setContentView(R.layout.activity_fullscreen_image_view)
         setBitmap()
         SGD = ScaleGestureDetector(this, ScaleListener())
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        SGD.onTouchEvent(ev)
        return true
    }

    private inner class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(detector: ScaleGestureDetector): Boolean {
            scale *= detector.scaleFactor
            scale = Math.max(0.1f, Math.min(scale, 5.0f))

            matrix.setScale(scale, scale)
            imageView.imageMatrix = matrix

            imageView.scaleX = scale
            imageView.scaleY = scale
            return true
        }
    }
}
