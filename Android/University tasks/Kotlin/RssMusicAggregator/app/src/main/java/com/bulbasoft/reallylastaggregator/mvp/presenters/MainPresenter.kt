package com.bulbasoft.reallylastaggregator.mvp.presenters

import java.util.*
import javax.inject.Inject
import android.app.Activity
import android.content.Intent
import android.content.Context
import com.activeandroid.ActiveAndroid
import android.view.animation.Animation
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.InjectViewState
import com.bulbasoft.reallylastaggregator.R
import android.view.animation.AnimationUtils
import com.bulbasoft.reallylastaggregator.NewsApplication
import com.bulbasoft.reallylastaggregator.mvp.views.MainView
import com.bulbasoft.reallylastaggregator.mvp.models.Category
import com.bulbasoft.reallylastaggregator.mvp.models.CategoryDao
import com.bulbasoft.reallylastaggregator.mvp.models.NewsSourceDao
import com.bulbasoft.reallylastaggregator.ui.activities.NewsListActivity
import com.bulbasoft.reallylastaggregator.ui.activities.NewCategoryActivity

@InjectViewState
class MainPresenter : MvpPresenter<MainView>() {
    @Inject lateinit var categoryDao: CategoryDao
    @Inject lateinit var newsSourceDao: NewsSourceDao

    private lateinit var rotate_forward: Animation
    private lateinit var rotate_backward: Animation
    private lateinit var categoriesList: MutableList<Category>

    private var selectedCount: Int = 0
    private var isSelected: Boolean = false
    private var isSelectedPrevState: Boolean = false

    private var lastNewsTitle = "No news read yet"
    var LastNewsTitle: String
        get() = this.lastNewsTitle
        set(value) {
            this.lastNewsTitle = value
            viewState.changeTitle(lastNewsTitle)
        }


    private fun loadAnimations() {
        rotate_forward = AnimationUtils.loadAnimation(context, R.anim.rotate_forward)
        rotate_backward = AnimationUtils.loadAnimation(context, R.anim.rotate_backward)
    }

    private var context: Context? = null
    var DataLoadContext: Context?
        get() = this.context
        set(value) {
            if (this.context == null) {
                this.context = value
                loadAnimations()
            }
        }

    init {
        NewsApplication.graph.inject(this)
    }



    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        categoriesList = categoryDao.LoadAllCategories()
        viewState.onCategoriesLoaded(categoriesList)
        viewState.changeTitle(lastNewsTitle)
    }


    private fun updateFabView() {
        if (selectedCount == 1 && !isSelectedPrevState) {
            viewState.updateFabViewState(rotate_forward, null)
            viewState.updateFabViewState(null, R.drawable.ic_clear_white_24dp)
        } else if (selectedCount == 0 && isSelectedPrevState) {
            viewState.updateFabViewState(rotate_backward, null)
            viewState.updateFabViewState(null, R.drawable.ic_add_white_24dp)
        }
    }

    fun updateSelectionState() {
        selectedCount = 0
        categoriesList.forEach { category -> selectedCount += if (category.IsSelected) 1 else 0 }
        isSelectedPrevState = isSelected
        isSelected = if (selectedCount == 0) false else true
        updateFabView()
    }



    fun deleteSelectedFromDB(): ArrayList<Category> {
        val removedList = ArrayList<Category>()
        ActiveAndroid.beginTransaction()
        try {
            for (category: Category in categoriesList) {
                if (category.IsSelected) {
                    categoryDao.DeleteCategory(category)
                    removedList.add(category)
                }
            }
            ActiveAndroid.setTransactionSuccessful()
        } finally {
            ActiveAndroid.endTransaction()
        }
        return removedList
    }

    fun deleteSelectedCategories() {
        for (category: Category in deleteSelectedFromDB()) {
            categoriesList.remove(category)
        }
        updateSelectionState()
        viewState.updateRecycleViewState()
    }


    fun openCategory(activity: Activity, pos: Int) {
        val intent = Intent(activity, NewsListActivity::class.java)
        intent.putExtra("category_id", categoriesList[pos].id)
        activity.startActivityForResult(intent, 2)
    }

    fun openAllNewsCategories(activity: Activity) {
        val intent = Intent(activity, NewsListActivity::class.java)
        intent.putExtra("category_id", -1)
        activity.startActivityForResult(intent, 2)
    }

    fun openNewCategory(activity: Activity) {
        val intent = Intent(activity, NewCategoryActivity::class.java)
        activity.startActivityForResult(intent, 1)
    }


    fun updateCategoryListWith(newCategoryId: Long) {
        categoriesList.add(0, categoryDao.GetCategoryById(newCategoryId))
        viewState.updateRecycleViewState()
    }

    fun fabAction(activity: Activity) {
        if (isSelected) {
            deleteSelectedCategories()
            updateSelectionState()
        } else {
            openNewCategory(activity)
        }
    }
}
