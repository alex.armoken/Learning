package com.bulbasoft.reallylastaggregator.utils.ndk

import com.bulbasoft.reallylastaggregator.mvp.models.rss.News

class JavaNativeTextAnalyzer {
    init {
        System.loadLibrary("native_text_analyze")
    }

    external fun getKEK(): String
    external fun getExecutionTime(): Int
    external fun QSort(arr: Array<News>): Array<News>
}