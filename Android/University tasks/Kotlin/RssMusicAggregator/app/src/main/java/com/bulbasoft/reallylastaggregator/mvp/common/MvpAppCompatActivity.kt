package com.bulbasoft.reallylastaggregator.mvp.common

import android.os.Bundle
import com.arellomobile.mvp.MvpDelegate
import android.support.v7.app.AppCompatActivity

open class MvpAppCompatActivity() : AppCompatActivity() {
    val mvpDelegate: MvpDelegate<MvpAppCompatActivity> by lazy { MvpDelegate(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mvpDelegate.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mvpDelegate.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mvpDelegate.onSaveInstanceState(outState)
    }

    override fun onStart() {
        super.onStart()
        mvpDelegate.onStart()
    }

    override fun onStop() {
        super.onStop()
        mvpDelegate.onStop()
    }
}