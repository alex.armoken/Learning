package com.bulbasoft.reallylastaggregator.di

import com.bulbasoft.reallylastaggregator.mvp.models.CategoryDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CategoryDaoModule {
    @Provides
    @Singleton
    fun provideCategoryDao() : CategoryDao = CategoryDao()
}