package com.bulbasoft.reallylastaggregator.mvp.presenters

import java.util.*
import javax.inject.Inject
import android.app.Activity
import android.app.FragmentManager
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.InjectViewState
import com.bulbasoft.reallylastaggregator.NewsApplication
import com.bulbasoft.reallylastaggregator.mvp.models.Category
import com.bulbasoft.reallylastaggregator.mvp.models.NewsSource
import com.bulbasoft.reallylastaggregator.mvp.models.CategoryDao
import com.bulbasoft.reallylastaggregator.mvp.models.NewsSourceDao
import com.bulbasoft.reallylastaggregator.mvp.views.NewCategoryView
import com.bulbasoft.reallylastaggregator.ui.adapters.NewsSourcesAdapter
import com.bulbasoft.reallylastaggregator.ui.dialogs.NewNewsSourceDialog

@InjectViewState
class NewCategoryPresenter : MvpPresenter<NewCategoryView>() {
    @Inject lateinit var categoryDao: CategoryDao
    @Inject lateinit var newsSourceDao: NewsSourceDao

    lateinit var activity: Activity
    lateinit var currentCategory: Category
    lateinit var newsSourcesAdapter: NewsSourcesAdapter
    lateinit var newsSourcesList: MutableList<NewsSource>
    lateinit var newNewsSourceDialog: NewNewsSourceDialog

    init {
        NewsApplication.graph.inject(this)
        newNewsSourceDialog = NewNewsSourceDialog()
        newNewsSourceDialog.presenter = this
    }


    fun viewAttach(activity: Activity) {
        this.activity = activity
        currentCategory = Category()
        newsSourcesList = newsSourceDao.LoadAllNewsSources()
        newsSourcesAdapter = NewsSourcesAdapter(newsSourcesList)
    }

    fun showNewNewsSourceDialog(fragmentManager: FragmentManager) {
        currentCategory = categoryDao.CreateCategory("", Date(), false)
        newNewsSourceDialog.show(fragmentManager, "New news source dialog")
    }

    fun newNewsSourceDialogAccept(name: String, url: String) {
        newsSourcesList.add(0, newsSourceDao.CreateNewsSource(name, url, currentCategory))
        newsSourcesAdapter.notifyDataSetChanged()
    }

    fun createNewCategory(name: String) {
        currentCategory.Name = name
        currentCategory.save()
        newsSourcesList.forEach { newsSource -> if (newsSource.IsSelected) newsSource.save() }
    }
}