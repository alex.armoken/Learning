package com.bulbasoft.reallylastaggregator.ui.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.content.Intent
import android.widget.EditText
import android.support.v7.widget.Toolbar
import com.bulbasoft.reallylastaggregator.R
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import android.support.v7.widget.helper.ItemTouchHelper
import android.support.design.widget.FloatingActionButton
import com.bulbasoft.reallylastaggregator.mvp.views.NewCategoryView
import com.bulbasoft.reallylastaggregator.ui.adapters.NewsSourcesAdapter
import com.bulbasoft.reallylastaggregator.mvp.common.MvpAppCompatActivity
import com.bulbasoft.reallylastaggregator.mvp.presenters.NewCategoryPresenter


class NewCategoryActivity : MvpAppCompatActivity(), NewCategoryView {
    @InjectPresenter lateinit var presenter: NewCategoryPresenter

    lateinit var categoryNameET: EditText
    lateinit var recycleView: RecyclerView


    fun exitWithoutResult() {
        val intent: Intent = Intent(applicationContext, NewCategoryActivity::class.java)
        intent.putExtra("id", -1L)
        setResult(0, intent)

        finish()
    }

    fun findToolbar() {
        val toolbar = findViewById(R.id.new_category_toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_close_white_24dp)
        toolbar.setNavigationOnClickListener { v -> exitWithoutResult() }
    }

    fun findRecycleView() {
        recycleView = findViewById(R.id.rvNewCategorySourcesList) as RecyclerView
        recycleView.layoutManager = LinearLayoutManager(this)
        recycleView.adapter = presenter.newsSourcesAdapter
        ItemTouchHelper(NewsSourcesAdapter.SimpleTouchCallback()).attachToRecyclerView(recycleView)
    }

    fun findFab() {
        val fab = (findViewById(R.id.fab) as FloatingActionButton)
        fab.setOnClickListener { presenter.showNewNewsSourceDialog(fragmentManager) }
    }

    fun findViews() {
        title = "New category"
        findToolbar()
        categoryNameET = findViewById(R.id.etNewCategoryName) as EditText
        findRecycleView()
        findFab()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_category)
        presenter.viewAttach(this)
        findViews()
    }



    override fun onBackPressed() {
        exitWithoutResult()
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.new_category, menu)
        return true
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        if (menuItem.itemId == R.id.new_category_accept) {
            presenter.createNewCategory(categoryNameET.text.toString())

            val intent: Intent = Intent(applicationContext, NewCategoryActivity::class.java)
            intent.putExtra("id", presenter.currentCategory.id)
            setResult(0, intent)

            finish()
        }
        return true
    }
}
