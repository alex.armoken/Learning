package com.bulbasoft.reallylastaggregator.mvp.views

import android.view.animation.Animation
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.bulbasoft.reallylastaggregator.mvp.models.rss.News
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface NewsListView : MvpView {
    fun onUpdateNewsList()

    fun onPresenterLoaded()

    fun onNewsLoaded(news: List<News>)

    fun updateLoadingButtonState(animation: Animation?)

    fun onSuccessfulLoadingNews()

    fun onFailureLoadingNews(msg: String)

    fun changeSortedState(state: Int, analyzeState: Int)

    fun showTimer(kek: Long)
}