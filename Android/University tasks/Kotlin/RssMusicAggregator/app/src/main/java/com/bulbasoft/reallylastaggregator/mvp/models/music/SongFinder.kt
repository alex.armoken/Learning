package com.bulbasoft.reallylastaggregator.mvp.models.music

import java.io.File
import android.os.AsyncTask
import android.os.Environment
import android.media.MediaMetadataRetriever
import android.util.Log


class SongFinder(private var listener: OnSongLoadListener) : AsyncTask<Unit, Song, Unit>() {
    var currentSerialNumber = 0
    var startPath: String = Environment.getExternalStorageDirectory().path

    private fun extractMp3Metadata(path: String): String {
        val mmr = MediaMetadataRetriever()
        mmr.setDataSource(path)
        return mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST) + " - " +
                mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM) + " - " +
                mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)
    }

    private fun findSongs(f: File) {
        val files = f.listFiles()
        if (f.isDirectory && files != null) {
            Log.d("SONGS", "We need to go deeper")
            files.forEach { file -> findSongs(file) }
        } else {
            Log.d("SONGS", "Song found")
            val path = f.path
            if (path.substring(path.length-4, path.length).equals(".mp3")) {
                publishProgress(Song(extractMp3Metadata(path), path, currentSerialNumber++))
            }
        }
    }



    override fun doInBackground(vararg params: Unit?) {
        findSongs(File(startPath))
    }


    override fun onProgressUpdate(vararg songs: Song) {
        super.onProgressUpdate(*songs)
        this.listener.onSongListUpdate(songs[0])
    }

    override fun onPostExecute(result: Unit?) {
        super.onPostExecute(result)
        listener.onEndLoading()
    }
}