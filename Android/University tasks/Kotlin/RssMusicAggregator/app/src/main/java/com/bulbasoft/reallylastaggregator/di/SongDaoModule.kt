package com.bulbasoft.reallylastaggregator.di

import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import com.bulbasoft.reallylastaggregator.mvp.models.music.SongDao

@Module
class SongDaoModule {
    @Provides
    @Singleton
    fun provideSongDao() : SongDao = SongDao()
}