package com.bulbasoft.reallylastaggregator.ui.fragments

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.content.Intent
import android.widget.TextView
import android.widget.ImageView
import android.view.LayoutInflater
import com.bulbasoft.reallylastaggregator.R
import com.bulbasoft.reallylastaggregator.mvp.models.rss.News
import com.bulbasoft.reallylastaggregator.mvp.views.NewsFragmentView
import com.bulbasoft.reallylastaggregator.mvp.common.MvpAppCompatFragment
import com.bulbasoft.reallylastaggregator.ui.activities.FullscreenImageViewActivity


class NewsFragment() : MvpAppCompatFragment(), NewsFragmentView {
    companion object {
        fun newInstance(news: News): NewsFragment {
            val newsFragment = NewsFragment()
            newsFragment.news = news
            return newsFragment
        }
    }

    lateinit var news: News
    lateinit var textView: TextView
    lateinit var imageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun findViews(v: View) {
        textView = v.findViewById(R.id.news_fragment_text) as TextView
        textView.text = news.description

        imageView = v.findViewById(R.id.news_fragment_image) as ImageView
        imageView.setImageBitmap(news.image)
        imageView.setOnClickListener { v ->
            val intent = Intent(activity, FullscreenImageViewActivity::class.java)
            intent.putExtra("news_id", this@NewsFragment.news.id)
            activity.startActivity(intent)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_news, container, false)
        findViews(view)
        return view
    }
}