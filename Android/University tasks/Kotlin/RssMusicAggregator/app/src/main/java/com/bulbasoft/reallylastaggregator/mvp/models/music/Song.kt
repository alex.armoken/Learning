package com.bulbasoft.reallylastaggregator.mvp.models.music

import android.util.Log
import com.activeandroid.Model
import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import com.activeandroid.annotation.Table
import com.activeandroid.annotation.Column

@Table(name = "Songs")
class Song() : Model() {
    @Column var path: String = ""
    @Column var title: String = ""
    @Column var serialNumber: Int = 0

    constructor(path: String, title: String, serialNumber: Int) : this() {
        this.path = path
        this.title = title
        this.serialNumber = serialNumber
    }
}

class SongDao {
    fun CreateAndSaveSong(path: String, title: String, serialNumber: Int) : Song {
        val song = Song(path, title, serialNumber)
        song.save()
        return song
    }

    fun SaveSong(song: Song) {
        song.save()
    }

    fun GetSongsCount(): Int {
        return Select().from(Song::class.java).count()
    }

    fun GetSongBySerialNumber(i: Int): Song? {
        return Select().from(Song::class.java).where("serialNumber = ?", i).executeSingle<Song>()
    }

    fun DeleteAllSongs() {
        Delete().from(Song::class.java).execute<Song>()
    }

    fun DeleteSong(song: Song) {
        song.delete()
    }

    fun LoadAllSongs(): MutableList<Song> {
        Log.d("SONGS", "Loading songs")
        return Select().from(Song::class.java).execute<Song>()
    }
}