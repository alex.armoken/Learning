package com.bulbasoft.reallylastaggregator.mvp.models.rss

import android.database.Cursor
import com.activeandroid.Cache
import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import com.bulbasoft.reallylastaggregator.mvp.models.Category
import java.util.*

class NewsDao() {
    fun CreateNews(category: Category, link: String, title: String,
                   description: String, imageUrl: String): News {
        return News(category, link, title, description, imageUrl)
    }

    fun CreateAndSaveNews(category: Category, link: String, title: String,
                          description: String, imageUrl: String): News {
        val news = News(category, link, title, description, imageUrl)
        news.save()
        return news
    }

    fun SaveNews(news: News): News {
        news.save()
        return news
    }


    private fun getNewsFromCursor(cursor: Cursor): MutableList<News> {
        val result = ArrayList<News>()
        if (cursor.moveToFirst()) {
            do {
                val news = News()
                news.description = cursor.getString(cursor.getColumnIndex("description"))
                news.title = cursor.getString(cursor.getColumnIndex("title"))
                news.imageUrl = cursor.getString(cursor.getColumnIndex("imageUrl"))
                news.link = cursor.getString(cursor.getColumnIndex("link"))
                result.add(news)
            } while (cursor.moveToNext())
        }
        cursor.close()
        return result
    }

    fun LoadNewsFromAllCategories(): MutableList<News> {
        val query = Select().
                from(News::class.java).
                innerJoin(Category::class.java).
                on("News.category=Categories.id")
        val cursor = Cache.openDatabase().rawQuery(query.toSql(), query.arguments)
        return getNewsFromCursor(cursor)
    }

    fun LoadNewsFromAllCategoriesSortedByTitle(): MutableList<News> {
        val query = Select().
                from(News::class.java).
                innerJoin(Category::class.java).
                on("News.category=Categories.id").
                orderBy("title ASC")
        val cursor = Cache.openDatabase().rawQuery(query.toSql(), query.arguments)
        return getNewsFromCursor(cursor)
    }


    fun LoadNewsFromCategory(id: Long): MutableList<News> {
        return Select().
                from(News::class.java).
                where("category = ?", id).
                execute<News>()
    }

    fun LoadNewsFromCategorySortByTitle(id: Long): MutableList<News> {
        return Select().
                from(News::class.java).
                where("category = ?", id).
                orderBy("title ASC").
                execute()
    }


    fun GetNewsByIndex(id: Int): News {
        return Select().
                from(News::class.java).
                where("id = ?", id).
                executeSingle<News>()
    }

    fun GetItemsCount(): Int {
        return Select().
                from(News::class.java).
                count()
    }

    fun DeleteAllNewsFromCategory(id: Long) {
        Delete().
                from(News::class.java).
                where("category = ?", id).
                execute<News>()
    }

    fun DeleteAllNews() {
        Delete().
                from(News::class.java).
                execute<News>()
    }
}