package com.bulbasoft.reallylastaggregator.di

import com.bulbasoft.reallylastaggregator.mvp.models.rss.NewsDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RssItemDaoModule {
    @Provides
    @Singleton
    fun provideRssItemDao() : NewsDao = NewsDao()
}