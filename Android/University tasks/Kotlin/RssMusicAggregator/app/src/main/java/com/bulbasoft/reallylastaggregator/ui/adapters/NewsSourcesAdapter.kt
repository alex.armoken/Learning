package com.bulbasoft.reallylastaggregator.ui.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import android.view.LayoutInflater
import com.bulbasoft.reallylastaggregator.R
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.support.v7.widget.helper.ItemTouchHelper.*
import com.bulbasoft.reallylastaggregator.mvp.models.NewsSource

class NewsSourcesAdapter(private var newsSourcesList: MutableList<NewsSource>) :
        RecyclerView.Adapter<NewsSourcesAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var newsSource: NewsSource
        lateinit var adapter: NewsSourcesAdapter
        var Name: TextView = itemView.findViewById(R.id.tvItemNewsSourceName) as TextView
        var Url: TextView = itemView.findViewById(R.id.tvItemNewsSourceUrl) as TextView
        var ChkSelected = itemView.findViewById(R.id.chkNewsSourceSelected) as CheckBox
    }

    class SimpleTouchCallback() : ItemTouchHelper.SimpleCallback(0, LEFT or RIGHT) {
        override fun onMove(rv: RecyclerView?, vh: RecyclerView.ViewHolder?,
                            target: RecyclerView.ViewHolder?): Boolean {
            return true
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val holder = viewHolder as ViewHolder
            holder.newsSource.delete()
            holder.adapter.newsSourcesList.remove(holder.newsSource)
            holder.adapter.notifyDataSetChanged()
        }
    }



    override fun getItemCount(): Int = newsSourcesList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return NewsSourcesAdapter.ViewHolder(LayoutInflater.from(parent.context).
                inflate(R.layout.item_news_source, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        val newsSource = newsSourcesList[pos]
        holder.adapter = this
        holder.newsSource = newsSource
        holder.Url.text = newsSource.Url
        holder.Name.text = newsSource.Name

        holder.ChkSelected.isChecked = newsSourcesList[pos].IsSelected
        holder.ChkSelected.setOnClickListener { v: View ->
            newsSourcesList[pos].IsSelected = (v as CheckBox).isChecked
        }
    }
}