package com.bulbasoft.reallylastaggregator.di

import com.bulbasoft.reallylastaggregator.background.MusicService
import dagger.Component
import javax.inject.Singleton
import com.bulbasoft.reallylastaggregator.mvp.presenters.*
import com.bulbasoft.reallylastaggregator.ui.activities.FullscreenImageViewActivity

@Component(modules = arrayOf(
        CategoryDaoModule::class,
        NewsSourceDaoModule::class,
        RssItemDaoModule::class,
        SongDaoModule::class))
@Singleton
interface AppComponent
{
    fun inject(musicService: MusicService)
    fun inject(mainPresenter: MainPresenter)
    fun inject(newsPresenter: NewsPresenter)
    fun inject(musicPresenter: MusicPresenter)
    fun inject(newsListPresenter: NewsListPresenter)
    fun inject(newCategoryPresenter: NewCategoryPresenter)
    fun inject(imageFullscreenImageViewActivity: FullscreenImageViewActivity)
}