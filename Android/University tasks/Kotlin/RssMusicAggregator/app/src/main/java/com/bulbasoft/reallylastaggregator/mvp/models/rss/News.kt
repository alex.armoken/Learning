package com.bulbasoft.reallylastaggregator.mvp.models.rss

import android.database.Cursor
import java.io.IOException
import android.graphics.Matrix
import com.activeandroid.Model
import android.graphics.Bitmap
import java.net.HttpURLConnection
import com.activeandroid.query.Delete
import com.activeandroid.query.Select
import android.graphics.BitmapFactory
import com.activeandroid.Cache
import com.activeandroid.annotation.Table
import com.activeandroid.annotation.Column
import com.bulbasoft.reallylastaggregator.mvp.models.Category
import java.util.*

@Table(name = "News")
class News() : Model() {
    @Column var category: Category = Category()

    @Column var link: String = ""
    @Column var title: String = ""
    @Column var description: String = ""

    var wordsCount: Long = 0
    var image: Bitmap? = null
    var littleImage: Bitmap? = null
    @Column var imageUrl: String = ""

    constructor(category: Category, link: String, title: String,
                description: String, imageUrl: String) : this() {
        this.category = category
        this.link = link
        this.title = title
        this.description = description
        this.imageUrl = imageUrl
    }


    private fun downloadBitmap(src: String): Bitmap? {
        var result: Bitmap? = null
        if (src != "") {
            try {
                val url = java.net.URL(src)
                val connection = url.openConnection() as HttpURLConnection
                connection.doInput = true
                connection.connect()
                val input = connection.inputStream
                val myBitmap = BitmapFactory.decodeStream(input)
                result = myBitmap
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return result
    }

    private fun resizeBitmap(bm: Bitmap?, newHeight: Int, newWidth: Int): Bitmap? {
        if (bm != null) {
            val width = bm.width
            val height = bm.height
            val scaleWidth = newWidth.toFloat() / width
            val scaleHeight = newHeight.toFloat() / height

            val matrix = Matrix()
            matrix.postScale(scaleWidth, scaleHeight)

            val resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false)
            return resizedBitmap
        }
        return null
    }

    fun loadImages() {
        this.image = downloadBitmap(this.imageUrl)
        this.littleImage = resizeBitmap(this.image, 70, 70)
    }
}


