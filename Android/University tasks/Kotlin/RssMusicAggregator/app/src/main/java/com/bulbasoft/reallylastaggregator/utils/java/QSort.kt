package com.bulbasoft.reallylastaggregator.utils.java

import com.bulbasoft.reallylastaggregator.mvp.models.rss.News
import java.util.*


class QSort {
    companion object {
        val RND = Random()
    }

    private fun swap(array: MutableList<News>, i: Int, j: Int) {
        val tmp = array[i]
        array[i] = array[j]
        array[j] = tmp
    }

    private fun partition(array: MutableList<News>, begin: Int, end: Int, cmp: (News, News) -> Int): Int {
        var index = begin + RND.nextInt(end - begin + 1)
        val pivot = array[index]
        swap(array, index, end)

        index = begin
        for (i in begin..end - 1) {
            if (cmp(array[i], pivot) <= 0) {
                swap(array, index++, i)
            }
        }
        swap(array, index, end)
        return index
    }

    private fun qsort(array: MutableList<News>, begin: Int, end: Int, cmp: (News, News) -> Int) {
        if (end > begin) {
            val index = partition(array, begin, end, cmp)
            qsort(array, begin, index - 1, cmp)
            qsort(array, index + 1, end, cmp)
        }
    }

    fun sort(array: MutableList<News>, cmp: (News, News) -> Int): MutableList<News> {
        qsort(array, 0, array.size - 1, cmp)
        return array
    }
}
