package com.bulbasoft.reallylastaggregator.ui.adapters

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.view.LayoutInflater
import com.bulbasoft.reallylastaggregator.R
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.support.v7.widget.helper.ItemTouchHelper.*
import com.bulbasoft.reallylastaggregator.mvp.models.music.Song
import com.bulbasoft.reallylastaggregator.mvp.models.music.SongDao
import com.bulbasoft.reallylastaggregator.mvp.presenters.MusicPresenter

class SongAdapter(private var songDao: SongDao,
                  private var songsList: MutableList<Song>,
                  private var presenter: MusicPresenter) :
        RecyclerView.Adapter<SongAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var song: Song
        lateinit var songDao: SongDao
        lateinit var adapter: SongAdapter
        lateinit var presenter: MusicPresenter
        var path: TextView = itemView.findViewById(R.id.music_song_path) as TextView
        var title: TextView = itemView.findViewById(R.id.music_song_title) as TextView
    }

    class SimpleTouchCallback() : ItemTouchHelper.SimpleCallback(0, LEFT or RIGHT) {
        override fun onMove(rv: RecyclerView?, vh: RecyclerView.ViewHolder?,
                            target: RecyclerView.ViewHolder?): Boolean {
            return true
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val holder = viewHolder as ViewHolder
            holder.presenter.stopPlaying()
            holder.songDao.DeleteSong(holder.song)
            holder.adapter.songsList.remove(holder.song)
            holder.adapter.notifyDataSetChanged()
        }
    }


    override fun getItemCount(): Int = songsList.size

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        val song = songsList[pos]
        holder.song = song
        holder.adapter = this
        holder.songDao = songDao
        holder.presenter = presenter
        holder.path.text = song.path
        holder.title.text = song.title
        holder.itemView.setOnClickListener { presenter.playSelectedSong(song.serialNumber) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return SongAdapter.ViewHolder(LayoutInflater.from(parent.context).
                inflate(R.layout.item_song, parent, false))
    }
}