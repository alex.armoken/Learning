package com.bulbasoft.reallylastaggregator

import android.content.Context
import com.activeandroid.app.Application
import com.bulbasoft.reallylastaggregator.di.AppComponent
import com.bulbasoft.reallylastaggregator.di.CategoryDaoModule
import com.bulbasoft.reallylastaggregator.di.DaggerAppComponent

class NewsApplication : Application()
{
    companion object
    {
        lateinit var graph: AppComponent
        lateinit var context: Context
    }

    override fun onCreate()
    {
        super.onCreate()

        context = this
        graph = DaggerAppComponent.builder().categoryDaoModule(CategoryDaoModule()).build()
    }
}