package com.bulbasoft.reallylastaggregator.mvp.models.rss

import org.jsoup.Jsoup
import java.io.IOException
import android.os.AsyncTask
import org.jsoup.nodes.Element
import org.jsoup.parser.Parser
import org.jsoup.select.Elements
import com.bulbasoft.reallylastaggregator.mvp.models.Category

class NewsFeedsDownloader(private var categories: List<Category>,
                          private var urlList: List<List<String>>,
                          private var newsDao: NewsDao,
                          private var onNewsLoadListener: OnNewsLoadListener) :
        AsyncTask<Unit, News, Unit>() {

    companion object {
        private val URL = "url"
        private val SRC = "src"
        private val IMG = "img"
        private val LINK = "link"
        private val TITLE = "title"
        private val IMAGE = "image"
        private val DESCRIPTION = "description"
        private val MEDIA_CONTENT = "media|content"
        private val MEDIA_THUMBNAIL = "media|thumbnail"
    }

    private fun downloadFeed(url: String): Elements {
        var items = Elements()
        try {
            items = Jsoup.connect(url).ignoreContentType(true).
                    parser(Parser.xmlParser()).get().select("item")
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            return items
        }
    }


    private fun getImageUrl(element: Element): String {
        if (!element.select(MEDIA_THUMBNAIL).isEmpty()) {
            return element.select(MEDIA_THUMBNAIL).attr(URL)
        } else if (!element.select(MEDIA_CONTENT).isEmpty()) {
            return element.select(MEDIA_CONTENT).attr(URL)
        } else if (!element.select(IMAGE).isEmpty()) {
            return element.select(IMAGE).attr(URL)
        }
        return Jsoup.parse(element.select(DESCRIPTION).first().text()).
                select(IMG)?.first()?.attr(SRC) ?: ""
    }

    private fun processItem(item: Element, category: Category): News {
        val news = News()

        news.category = category
        news.imageUrl = getImageUrl(item)
        news.link = item.select(LINK).first().text()
        news.title = item.select(TITLE).first().text()
        news.description = Jsoup.parse(item.select(DESCRIPTION).first().text()).text()

        return newsDao.SaveNews(news)
    }

    override fun doInBackground(vararg params: Unit?) {
        categories.forEachIndexed { i, category ->
            urlList[i].forEach { url ->
                for (item: Element in downloadFeed(url)) {
                    publishProgress(processItem(item, category))
                }
            }
        }
    }


    override fun onProgressUpdate(vararg items: News) {
        super.onProgressUpdate(*items)
        onNewsLoadListener.onNewsItemUpdate(items[0])
    }

    override fun onPostExecute(result: Unit?) {
        super.onPostExecute(result)
        onNewsLoadListener.onSuccessfulLoadingNews()
    }
}