package com.bulbasoft.reallylastaggregator.mvp.views

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface NewCategoryView : MvpView {
}