package com.bulbasoft.reallylastaggregator.ui.dialogs

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.app.DialogFragment
import android.view.LayoutInflater
import android.content.DialogInterface
import android.text.SpannableStringBuilder
import com.bulbasoft.reallylastaggregator.R
import com.bulbasoft.reallylastaggregator.mvp.presenters.NewCategoryPresenter
import com.bulbasoft.reallylastaggregator.utils.ndk.JavaNativeTextAnalyzer

class NewNewsSourceDialog() : DialogFragment(), View.OnClickListener {
    lateinit var nameEV: EditText
    lateinit var linkEV: EditText
    var presenter: NewCategoryPresenter? = null

    fun findButtons(v: View) {
        v.findViewById(R.id.new_news_source_accept).setOnClickListener(this)
        v.findViewById(R.id.new_news_source_cancel).setOnClickListener(this)
    }

    fun findEdits(v: View) {
        nameEV = v.findViewById(R.id.etNewNewsSourceName) as EditText
        linkEV = v.findViewById(R.id.etNewNewsSourceUrl) as EditText
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, bundle: Bundle?) : View {
        dialog?.setTitle("Creating new news source")
        val v = inflater.inflate(R.layout.dialog_new_news_source, container, false)
        findButtons(v)
        findEdits(v)
        return v
    }

    override fun onCancel(dialogInterface: DialogInterface) {
        super.onCancel(dialogInterface)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.new_news_source_accept) {
                presenter?.newNewsSourceDialogAccept(nameEV.text.toString(), linkEV.text.toString())
        }
        super.dismiss()
    }
}