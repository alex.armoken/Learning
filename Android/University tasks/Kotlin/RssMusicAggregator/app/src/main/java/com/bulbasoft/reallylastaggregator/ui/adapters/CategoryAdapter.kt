package com.bulbasoft.reallylastaggregator.ui.adapters

import android.view.View
import android.app.Activity
import android.view.ViewGroup
import android.widget.TextView
import android.widget.CheckBox
import android.view.LayoutInflater
import com.bulbasoft.reallylastaggregator.R
import android.support.v7.widget.RecyclerView
import com.bulbasoft.reallylastaggregator.mvp.models.Category
import com.bulbasoft.reallylastaggregator.mvp.presenters.MainPresenter

class CategoryAdapter(var categoriesList: List<Category>,
                      var presenter: MainPresenter,
                      var activity: Activity) :
        RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {

    companion object {
        val SIMPLE_CATEGORY: Int = 0
        val ALL_CATEGORIES: Int = 1
    }


    open class ViewHolder(v: View) : RecyclerView.ViewHolder(v)
    class NormalViewHolder(itemView: View) : CategoryAdapter.ViewHolder(itemView) {
        var CategoryId: Long = 0
        var Name: TextView = itemView.findViewById(R.id.tvItemCategoryName) as TextView
        var ViewingDate: TextView = itemView.findViewById(R.id.tvItemCategoryDate) as TextView
        var ChkSelected: CheckBox = itemView.findViewById(R.id.chkCategorySelected) as CheckBox
    }
    class FooterViewHolder(itemView: View) : CategoryAdapter.ViewHolder(itemView) {
        var ViewingDate: TextView = itemView.findViewById(R.id.tvItemAllCategoriesDate) as TextView
    }


    override fun getItemCount(): Int {
        if (categoriesList.size == 0) {
            return 0
        }
        return categoriesList.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        if (position == itemCount - 1) {
            return ALL_CATEGORIES
        }
        return  SIMPLE_CATEGORY
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryAdapter.ViewHolder {
        if (viewType == SIMPLE_CATEGORY) {
            return CategoryAdapter.NormalViewHolder(LayoutInflater.from(parent.context).
                    inflate(R.layout.item_category, parent, false))
        }
        return CategoryAdapter.FooterViewHolder(LayoutInflater.from(parent.context).
                inflate(R.layout.item_all_categories, parent, false))
    }


    private fun bindNormalViewHolder(holder: NormalViewHolder, pos: Int) {
        val category = categoriesList[pos]
        holder.CategoryId = category.id
        holder.Name.text = category.Name
        holder.ViewingDate.text = category.formatedDate()

        holder.ChkSelected.isChecked = categoriesList[pos].IsSelected
        holder.ChkSelected.setOnClickListener { v: View ->
            categoriesList[pos].IsSelected = (v as CheckBox).isChecked
            presenter.updateSelectionState()
        }

        holder.itemView.setOnClickListener { v: View -> presenter.openCategory(activity, pos) }
    }

    private fun bindFooterViewHolder(holder: FooterViewHolder) {
        holder.itemView.setOnClickListener { presenter.openAllNewsCategories(activity) }
    }

    override fun onBindViewHolder(viewHolder: CategoryAdapter.ViewHolder, pos: Int) {
        if (viewHolder.itemViewType == SIMPLE_CATEGORY) {
            bindNormalViewHolder(viewHolder as NormalViewHolder, pos)
        } else {
            bindFooterViewHolder(viewHolder as FooterViewHolder)
        }
    }
}