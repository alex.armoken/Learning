#if !(defined (UCLIENT_H))
#define UCLIENT_H

#include <vector>
#include <thread>
#include <iostream>
#include <functional>

#include <getopt.h>
#include <arpa/inet.h>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/threadpool.hpp>
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/enable_shared_from_this.hpp>

using namespace boost::asio;

typedef boost::shared_ptr<ip::udp::socket> usock_ptr;
typedef ip::udp::endpoint uep;

class ServerConnection:
    public boost::enable_shared_from_this<ServerConnection>,
    boost::noncopyable
{
protected:
    std::vector<char> buf;
    usock_ptr sock;
    io_service &ios;
    bool is_connected;
    uep server_ep;
    uep listen_ep;

    static const std::string connect_msg;
    static const std::string connect_answer_msg;

    ServerConnection(io_service &ios, uep server_ep, uep listen_ep):
        ios(ios), server_ep(server_ep), listen_ep(listen_ep)
        {
            this->is_connected = true;
            this->sock = usock_ptr(new ip::udp::socket(ios));
            this->sock->open(ip::udp::v4());
            this->sock->set_option(socket_base::reuse_address(true));
            this->sock->bind(listen_ep);
        }

public:
    bool is_open();
    void not_connected(const boost::system::error_code &err);
    void on_connect(std::function<void(bool)> conn_callback,
                    const boost::system::error_code &err, size_t bytes);
    void connect(std::function<void(bool)> conn_callback);
    void stop();
    void on_read(const boost::system::error_code &err, size_t bytes);
    void do_read();
    void on_write(const boost::system::error_code &err, size_t bytes);
    void do_write(std::string msg);
    void start(std::function<void(bool)> conn_callback);

    static boost::shared_ptr<ServerConnection>create(
        io_service &ios, uep server_ep, uep listen_ep);

};

class Client
{
protected:
    io_service &ios;
    uep server_ep;
    uep listen_ep;
    boost::shared_ptr<ServerConnection> con;

public:
    bool is_connected();

    Client(io_service &ios, uep server_ep, uep listen_ep,
           std::function<void(bool)> conn_callback):
        ios(ios), server_ep(server_ep), listen_ep(listen_ep)
        {
            this->con = ServerConnection::create(ios, server_ep, listen_ep);
            con->start(conn_callback);
        };

    ~Client()
        {
            con->stop();
        }
};

#endif // UCLIENT_H
