#include "ncurses.h"

class UIPanel {
private:
    WINDOW* win;
    bool is_init;
    uint8_t vert_ratio;
    uint8_t horiz_ratio;

public:
    void draw_borders();
    void display();
    void resize();

    UIPanel(uint8_t vert_ratio, uint8_t horiz_ratio)
        : vert_ratio(vert_ratio)
        , horiz_ratio(horiz_ratio)
        , is_init(false)
        , win(NULL){};

    ~UIPanel()
        {
            if (is_init) {
                delwin(win);
            }
        };
};
