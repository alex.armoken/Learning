#include "./client.hpp"

const std::string ServerConnection::connect_msg = "Connect\n";
const std::string ServerConnection::connect_answer_msg = "YesConnect\n";

bool ServerConnection::is_open()
{
    return sock->is_open();
}

void ServerConnection::not_connected(const boost::system::error_code &err)
{

}

void ServerConnection::on_connect(std::function<void(bool)> conn_callback,
                                  const boost::system::error_code &err,
                                  size_t bytes)
{
    std::string answer_msg(buf.begin(), buf.end());
    is_connected = answer_msg == connect_answer_msg;
    conn_callback(answer_msg == connect_answer_msg);
}

void ServerConnection::connect(std::function<void(bool)> conn_callback)
{
    sock->send_to(buffer(connect_msg), server_ep);
    sock->async_receive_from(buffer(buf), server_ep,
                             boost::bind(&ServerConnection::on_connect,
                                         shared_from_this(),
                                         conn_callback, _1, _2));
}

void ServerConnection::stop()
{

}

void ServerConnection::on_read(const boost::system::error_code &err,
                               size_t bytes)
{

}

void ServerConnection::do_read()
{

}

void ServerConnection::on_write(const boost::system::error_code &err,
                                size_t bytes)
{

}

void ServerConnection::do_write(std::string msg)
{

}

void ServerConnection::start(std::function<void(bool)> conn_callback)
{
    connect(conn_callback);
}

boost::shared_ptr<ServerConnection> ServerConnection::create(
    io_service &ios, uep server_ep, uep listen_ep)
{
    return boost::shared_ptr<ServerConnection>(
        new ServerConnection(ios, server_ep, listen_ep));
}

bool Client::is_connected()
{
    return con->is_open();
}
