#include <vector>
#include <thread>
#include <iostream>

#include <getopt.h>
#include <arpa/inet.h>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/threadpool.hpp>
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/enable_shared_from_this.hpp>

using namespace boost::asio;

class ClientConnection
{
public:
    bool is_open();
    void stop();
    void on_read(const boost::system::error_code &err, size_t bytes);
    void do_read();
    void on_write(const boost::system::error_code &err, size_t bytes);
    void do_write(std::string);
    void start();

protected:
    ClientConnection();

public:
    static boost::shared_ptr<ClientConnection> create(ip::udp::socket *sock);
    ~ClientConnection();
};

class Server
{
protected:
    Server();

public:
    ~Server();
};
