#include <unistd.h>
#include <getopt.h>
#include <ncurses.h>

#include "./client.hpp"

struct global_args_t {
    uint32_t listen_port;
    uint32_t server_port;
    char *server_address;
} global_args;

static struct option OPTS[] = {
    {"listen_port", required_argument, NULL, 'l'},
    {"server_port", required_argument, NULL, 'p'},
    {"server_address", required_argument, NULL, 'a'},
    {0, 0, 0, 0}
};

typedef struct ui_t {
    int new_x;
    int new_y;
    int parent_x;
    int parent_y;
    WINDOW *msgs;
    WpINDOW *input;
} ui;
static const int INPUT_SIZE = 5;


int parse_arguments(int argc, char *argv[])
{
    int c;
    int opt_idx;
    const char *args = "a:p:l:";
    global_args.listen_port = 0;
    global_args.server_port = 0;
    global_args.server_address = NULL;

    while ((c = getopt_long(argc, argv, args, OPTS, &opt_idx)) != -1) {
        switch (c) {
        case 'l':
            global_args.listen_port = atoi(optarg);
            break;
        case 'a':
            global_args.server_address = optarg;
            break;
        case 'p':
            global_args.server_port = atoi(optarg);
            break;
        default:
            return -1;
        }
    }
    return 0;
}

bool check_args_logic()
{
    if (global_args.listen_port == 0) {
        std::cout << "Set listen port" << std::endl;
        return false;
    } else if (global_args.server_port == 0) {
        std::cout << "Set server port" << std::endl;
        return false;
    } else if (global_args.server_address == NULL) {
        std::cout << "Set server address" << std::endl;
        return false;
    }
    return true;
}

void sig_handler(io_service &ios,
                 const boost::system::error_code& error, int signal_number)
{
    std::cout << "Signal handled!" << "\n";
    if (!error)
    {
        ios.stop();
        std::cout << "All async works stoped!" << "\n";
    }
}

void connection_callback(bool is_good, ui &u)
{
    if (is_good) {
        std::cout << "Successfully connected!" << std::endl;
    } else {
        std::cout << "Connection refused!" << std::endl;
    }
}

void draw_borders(WINDOW *screen)
{
    int x, y, i;

    getmaxyx(screen, y, x);

    // 4 corners
    mvwprintw(screen, 0, 0, "+");
    mvwprintw(screen, y - 1, 0, "+");
    mvwprintw(screen, 0, x - 1, "+");
    mvwprintw(screen, y - 1, x - 1, "+");

    // sides
    for (i = 1; i < (y - 1); i++) {
        mvwprintw(screen, i, 0, "|");
        mvwprintw(screen, i, x - 1, "|");
    }

    // top and bottom
    for (i = 1; i < (x - 1); i++) {
        mvwprintw(screen, 0, i, "-");
        mvwprintw(screen, y - 1, i, "-");
    }
}

void worker_func(io_service &ios, ui &u)
{
    ios.run();
}

void init_signals(io_service &ios, signal_set &signals)
{
    signals.async_wait(bind(sig_handler, std::ref(ios), _1, _2));
}

void init_ui(ui *u)
{
    initscr();
    // noecho();
    cbreak();
    curs_set(FALSE);
    nodelay(stdscr, TRUE);

    getmaxyx(stdscr, u->parent_y, u->parent_x);
    u->msgs = newwin(u->parent_y - INPUT_SIZE, u->parent_x, 0, 0);
    u->input = newwin(INPUT_SIZE, u->parent_x, u->parent_y - INPUT_SIZE, 0);

    draw_borders(u->msgs);
    draw_borders(u->input);
}

Client* init_connection(io_service &ios, ui &u)
{
    try {
        uep server_ep(ip::address::from_string(global_args.server_address),
                      global_args.server_port);
        uep listen_ep(ip::address::from_string(global_args.server_address),
                      global_args.listen_port);
        return new Client(ios, server_ep,
                          listen_ep, boost::bind(connection_callback, _1,
                                                 boost::ref(u)));
    } catch (const boost::system::system_error& ex) {
        std::cout << ex.what() << std::endl;
        return NULL;
    }
}

boost::thread* init_worker(io_service &ios, ui &u)
{
    try {
        return new boost::thread(boost::bind(worker_func, boost::ref(ios),
                                             boost::ref(u)));
    } catch (const boost::system::system_error& ex) {
        std::cout << ex.what() << std::endl;
        return NULL;
    }
}

void ui_cycle(ui &u)
{
    char ch;
    bool is_exit = false;
    std::string input_msg = "";
    while (!is_exit) {
        getmaxyx(stdscr, u.new_y, u.new_x);
        if (u.new_y != u.parent_y || u.new_x != u.parent_x) {
            u.parent_x = u.new_x;
            u.parent_y = u.new_y;

            wresize(u.msgs, u.new_y - INPUT_SIZE, u.new_x);
            wresize(u.input, INPUT_SIZE, u.new_x);
            mvwin(u.input, u.new_y - INPUT_SIZE, 0);

            wclear(stdscr);
            wclear(u.msgs);
            wclear(u.input);

            draw_borders(u.msgs);
            draw_borders(u.input);
        }

        // draw to our windows
        mvwprintw(u.msgs, 1, 1, "Messages: ");
        mvwprintw(u.input, 1, 1, "Input: ");
        // refresh each window

        // mvwprintw(u.input, 2, 2, "ddd");
        wrefresh(u.msgs);
        wrefresh(u.input);

        ch = mvwgetch(u.input, 2, 2);
        switch (ch) {
        case KEY_HOME:
            is_exit = true;
            break;
        case ERR:
            break;
        default:
            input_msg += ch;
            mvwprintw(u.input, 0, 3, "ddd");
            break;
        }
    }
}

int main_program()
{
    int result = 0;
    io_service ios;
    Client *client;
    boost::thread *t = NULL;
    ui u = {0, 0, 0, 0, NULL, NULL};
    signal_set signals(ios, SIGINT);

    init_signals(ios, signals);
    init_ui(&u);
    if ((client = init_connection(ios, u)) == NULL) {
        result = 3;
        goto end;
    }

    ui_cycle(u);

end:
    endwin();
    ios.stop();
    return result;
}

int main(int argc, char *argv[])
{
    if (parse_arguments(argc, argv) == -1) {
        return 1;
    } else if (!check_args_logic()) {
        return 2;
    }
    return main_program();
}
