#include <vector>
#include <memory>
#include "ncurses.h"

#include "./ui_panel.hpp"

class UIWindowVerticalStack
{
private:
    std::vector<std::shared_ptr<UIPanel>> panels;

public:
    void push(std::shared_ptr<UIPanel> panel);
    void display();

    UIWindowVerticalStack();
    ~UIWindowVerticalStack();
};
