#include <vector>
#include <iostream>

#include <getopt.h>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/threadpool.hpp>
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>

using namespace boost::asio;

struct global_args_t {
    bool is_run_tests;
    uint16_t port;
} global_args;

static struct option OPTS[] = {
    {"port", required_argument, NULL, 'p'},
    {"run_tests", no_argument, NULL, 't'},
    {0, 0, 0, 0}
};

typedef boost::system::error_code error_code;
typedef boost::shared_ptr<ip::tcp::socket> socket_ptr;

int parse_arguments(int argc, char *argv[])
{
    int c;
    int opt_idx;
    const char *args = "tp:";
    global_args.port = 0;
    global_args.is_run_tests = false;

    while ((c = getopt_long(argc, argv, args, OPTS, &opt_idx)) != -1) {
        switch (c) {
        case 'p':
            global_args.port = atoi(optarg);
            break;
        case 't':
            global_args.is_run_tests = true;
            break;
        default:
            return -1;
        }
    }
    return 0;
}

bool check_args_logic()
{
    if (global_args.port == 0) {
        std::cout << "Set server port" << std::endl;
        return false;
    }
    return true;
}

void connection(socket_ptr sock)
{
    int8_t byte;
    auto buf = buffer(&byte, 1);
    try {
        boost::system::error_code error;
        while (sock->is_open()) {
            sock->read_some(buf, error);
            if (error == error::eof) {
                break;
            } else if(error) {
                throw boost::system::system_error(error);
            }
            sock->write_some(buf);
        }
        sock->close();
    } catch (boost::system::system_error const& error) {
        std::cout << error.what() << std::endl;
    }
}

int main_program()
{
    io_service service;
    ip::tcp::endpoint ep(ip::tcp::v4(), global_args.port);
    ip::tcp::acceptor acceptor(service, ep);

    while (true) {
        socket_ptr sock(new ip::tcp::socket(service));
        acceptor.accept(*sock);
        boost::thread t(boost::bind(connection, sock));
    }
    return 0;
}

int main(int argc, char *argv[])
{
    if (parse_arguments(argc, argv) == -1) {
        return 1;
    } else if (!check_args_logic()) {
        return 2;
    }
    return main_program();
}
