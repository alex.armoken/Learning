#include <vector>
#include <thread>
#include <iostream>

#include <getopt.h>
#include <arpa/inet.h>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/threadpool.hpp>
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/enable_shared_from_this.hpp>

using namespace boost::asio;

struct global_args_t {
    uint16_t port;
} global_args;

static struct option OPTS[] = {
    {"port", required_argument, NULL, 'p'},
    {0, 0, 0, 0}
};

typedef std::shared_ptr<ip::tcp::socket> tcp_socket_ptr;

class EchoConnection : public boost::enable_shared_from_this<EchoConnection>,
                       boost::noncopyable
{
public:
    bool is_open();
    void stop();
    void on_read(const boost::system::error_code &err, size_t bytes);
    void do_read();
    void on_write(const boost::system::error_code &err, size_t bytes);
    void do_write();
    void start();

    static boost::shared_ptr<EchoConnection> create(ip::tcp::socket *sock)
        {
            return boost::shared_ptr<EchoConnection>(
                new EchoConnection(sock));
        }

protected:
    uint8_t buf;
    tcp_socket_ptr sock;

    EchoConnection(ip::tcp::socket *sock): buf(0)
        {
            this->sock = tcp_socket_ptr(sock);
            std::cout << "Created EchoConnection!" << "\n";
        };

public:

    ~EchoConnection()
        {
            stop();
            std::cout << "Destroyed EchoConnection!" << "\n";
        };
};

bool EchoConnection::is_open()
{
    return sock->is_open();
}

void EchoConnection::stop()
{
    if (sock->is_open()) {
        sock->close();
    }
}

void EchoConnection::on_read(const boost::system::error_code &err,
                             size_t bytes)
{
    if (err) {
        stop();
    } else {
        if (sock->is_open()) {
            do_write();
        }
    }
}

void EchoConnection::do_read()
{
    async_read(*sock, buffer(&buf, sizeof(buf)),
               boost::bind(&EchoConnection::on_read,
                           shared_from_this(), _1, _2));
}

void EchoConnection::on_write(const boost::system::error_code &err,
                              size_t bytes)
{
    if (err) {
        stop();
    } else if (sock->is_open()) {
        do_read();
    }
}

void EchoConnection::do_write()
{
    async_write(*sock, buffer(&buf, sizeof(buf)),
                boost::bind(&EchoConnection::on_write,
                            shared_from_this(), _1, _2));
}

void EchoConnection::start()
{
    do_read();
}


int parse_arguments(int argc, char *argv[])
{
    int c;
    int opt_idx;
    const char *args = "p:";
    global_args.port = 0;

    while ((c = getopt_long(argc, argv, args, OPTS, &opt_idx)) != -1) {
        switch (c) {
        case 'p':
            global_args.port = atoi(optarg);
            break;
        default:
            return -1;
        }
    }
    return 0;
}

bool check_args_logic()
{
    if (global_args.port == 0) {
        std::cout << "Set server port" << std::endl;
        return false;
    }
    return true;
}

void sig_handler(io_service &ios,
                 const boost::system::error_code& error, int signal_number)
{
    std::cout << "Signal handled!" << "\n";
    if (!error)
    {
        ios.stop();
        std::cout << "All async works stoped!" << "\n";
    }
}

void wait_for_connection(
    io_service &ios, ip::tcp::acceptor &acceptor,
    ip::tcp::socket *sock_ready,
    std::vector<boost::shared_ptr<EchoConnection>> &conarr)
{
    auto ptr = EchoConnection::create(sock_ready);
    ptr->start();
    auto sock = new ip::tcp::socket(ios);
    acceptor.async_accept(*sock, std::bind(
                              wait_for_connection, std::ref(ios),
                              std::ref(acceptor), sock, std::ref(conarr)));
}

void start_clients_accept(
    io_service &ios, ip::tcp::acceptor &acc, uint16_t port,
    std::vector<boost::shared_ptr<EchoConnection>> &conarr)
{
    acc.open(ip::tcp::v4());
    acc.set_option(socket_base::reuse_address(true));
    uint8_t opt_val = 1;
    setsockopt(acc.native_handle(), SOL_SOCKET,
               SO_REUSEADDR, &opt_val, sizeof(opt_val));
    acc.bind(ip::tcp::endpoint(ip::tcp::v4(), port));
    acc.listen();
    auto sock = new ip::tcp::socket(ios);
    acc.async_accept(*sock, std::bind(wait_for_connection, std::ref(ios),
                                      std::ref(acc), sock, std::ref(conarr)));
}

void worker_func(io_service &ios, ip::tcp::acceptor &acc, uint16_t port)
{
    try {
        ios.run();
    } catch (boost::system::system_error const& error) {
        std::cout << error.what() << std::endl;
    }
}

int main_program()
{
    io_service ios;
    boost::thread_group tg;
    ip::tcp::acceptor acc(ios);
    std::vector<boost::shared_ptr<EchoConnection>> conarr;

    start_clients_accept(ios, acc, global_args.port, conarr);
    signal_set signals(ios, SIGINT);
    signals.async_wait(bind(sig_handler, std::ref(ios), _1, _2));

    for (uint16_t i = 0; i < std::thread::hardware_concurrency(); i++) {
        tg.add_thread(new boost::thread(boost::bind(worker_func,
                                                    boost::ref(ios),
                                                    boost::ref(acc),
                                                    global_args.port)));
    }

    ios.run();
    acc.close();
    tg.join_all();
    return 0;
}

int main(int argc, char *argv[])
{
    if (parse_arguments(argc, argv) == -1) {
        return 1;
    } else if (!check_args_logic()) {
        return 2;
    }
    return main_program();
}
