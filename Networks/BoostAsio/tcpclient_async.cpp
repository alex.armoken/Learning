#include <vector>
#include <iostream>

#include "getopt.h"
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/threadpool.hpp>
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>

using namespace boost::asio;

struct global_args_t {
    uint32_t server_port;
    char *server_address;
    char *msg;
} global_args;

static struct option OPTS[] = {
    {"message", required_argument, NULL, 'm'},
    {"server_port", required_argument, NULL, 'p'},
    {"server_address", required_argument, NULL, 'a'},
    {0, 0, 0, 0}
};

typedef boost::system::error_code error_code;

int parse_arguments(int argc, char *argv[])
{
    int c;
    int opt_idx;
    const char *args = "a:p:m:";
    global_args.server_port = 0;
    global_args.server_address = NULL;

    while ((c = getopt_long(argc, argv, args, OPTS, &opt_idx)) != -1) {
        switch (c) {
        case 'm':
            global_args.msg = optarg;
            break;
        case 'a':
            global_args.server_address = optarg;
            break;
        case 'p':
            global_args.server_port = atoi(optarg);
            break;
        default:
            return -1;
        }
    }
    return 0;
}

bool check_args_logic()
{
    if (global_args.server_port == 0) {
        std::cout << "Set server port" << std::endl;
        return false;
    }
    return true;
}

void sig_handler(io_service &ios,
                 const boost::system::error_code& error, int signal_number)
{
    std::cout << "Signal handled!" << "\n";
    if (!error)
    {
        ios.stop();
        std::cout << "All async works stoped!" << "\n";
    }
}

void on_read(io_service &ios, ip::tcp::socket &sock, streambuf &buf,
             uint32_t length, const boost::system::error_code &error,
             std::size_t bytes_transferred)
{
    streambuf::const_buffers_type buf_msg = buf.data();
    std::string echo_msg(buffers_begin(buf_msg),
                         buffers_begin(buf_msg) + length);
    std::cout << echo_msg << std::endl;
    sock.close();
    ios.stop();
}

void on_write(io_service &ios, ip::tcp::socket &sock, streambuf &buf,
              uint32_t length, const boost::system::error_code& error,
              std::size_t bytes_transferred)
{
    async_read(sock, buf, transfer_at_least(length),
               boost::bind(on_read, std::ref(ios), std::ref(sock),
                           std::ref(buf), length , _1, _2));
}

void on_connect(io_service &ios, ip::tcp::socket &sock, streambuf &buf,
                std::string &msg, const boost::system::error_code &error)
{
    async_write(sock, buffer(msg, msg.length()),
                boost::bind(on_write, std::ref(ios), std::ref(sock),
                            std::ref(buf), msg.length(), _1, _2));
}

int main_program()
{
    streambuf buf;
    io_service ios;
    ip::tcp::socket sock(ios);
    std::string msg(global_args.msg);
    ip::tcp::endpoint ep(ip::address::from_string(global_args.server_address),
                         global_args.server_port);

    signal_set signals(ios, SIGINT);
    signals.async_wait(bind(sig_handler, std::ref(ios), _1, _2));

    sock.async_connect(ep, boost::bind(on_connect, std::ref(ios),
                                       std::ref(sock),
                                       std::ref(buf), std::ref(msg), _1));
    ios.run();
    return 0;
}

int main(int argc, char *argv[])
{
    if (parse_arguments(argc, argv) == -1) {
        return 1;
    } else if (!check_args_logic()) {
        return 2;
    }
    return main_program();
}
