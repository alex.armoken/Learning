#include <vector>
#include <iostream>

#include <getopt.h>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/threadpool.hpp>
#include <boost/thread/thread.hpp>
#include <boost/lockfree/queue.hpp>

using namespace boost::asio;

struct global_args_t {
    bool is_run_tests;
    uint16_t port;
} global_args;

static struct option OPTS[] = {
    {"port", required_argument, NULL, 'p'},
    {"run_tests", no_argument, NULL, 't'},
    {0, 0, 0, 0}
};

typedef boost::system::error_code error_code;
typedef boost::shared_ptr<ip::tcp::socket> socket_ptr;

int parse_arguments(int argc, char *argv[])
{
    int c;
    int opt_idx;
    const char *args = "tp:";
    global_args.port = 0;
    global_args.is_run_tests = false;

    while ((c = getopt_long(argc, argv, args, OPTS, &opt_idx)) != -1) {
        switch (c) {
        case 'p':
            global_args.port = atoi(optarg);
            break;
        case 't':
            global_args.is_run_tests = true;
            break;
        default:
            return -1;
        }
    }
    return 0;
}

bool check_args_logic()
{
    if (global_args.port == 0) {
        std::cout << "Set server port" << std::endl;
        return false;
    }
    return true;
}

void echo_server(io_service &ios, uint16_t port)
{
    std::vector<uint8_t> buf;
    ip::udp::endpoint sender_ep;
    socket_base::bytes_readable command(true);
    ip::udp::socket sock(ios, ip::udp::endpoint(ip::udp::v4(), port));
    try {
        while (true) {
            sock.receive_from(null_buffers(), sender_ep);
            sock.io_control(command);
            buf.resize(command.get());
            sock.receive_from(buffer(buf, buf.size()), sender_ep);
            sock.send_to(buffer(buf, buf.size()), sender_ep);
        }
    } catch (boost::system::system_error const& error) {
        std::cout << error.what() << std::endl;
    }
}

int main_program()
{
    io_service service;
    echo_server(service, global_args.port);
    return 0;
}

int main(int argc, char *argv[])
{
    if (parse_arguments(argc, argv) == -1) {
        return 1;
    } else if (!check_args_logic()) {
        return 2;
    }
    return main_program();
}
