let
  nixpkgs     = import <nixpkgs> {};
  pkgs        = with nixpkgs; {
    mkDerivation = import ./autotools.nix nixpkgs;

    hello        = lib.callPackage ./hello/default.nix {};
    simple       = lib.callPackage ./simple/default.nix {};
    graphviz     = lib.callPackage ./graphviz/default.nix {};
    graphvizCore = lib.callPackage ./graphviz/default.nix { gdSupport = false; };
  };
  allPkgs     = nixpkgs // pkgs;

  lib         = import ./lib.nix { pkgs = allPkgs; };
in pkgs
