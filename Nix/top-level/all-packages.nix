{ stdenv, fetchurl, perl }:
rec {
  hello = import ../hello {
    inherit stdenv fetchurl perl;
  };
}
