{ mkDerivation, bash, ... }:
let
in
mkDerivation {
  name    = "simple";
  builder = "${bash}/bin/bash";
  args    = [ ./simple_builder.sh ];
  src     = ./simple.c;
}
