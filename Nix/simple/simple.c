#include "stdio.h"

#define VAL(str) #str
#define TOSTRING(str) VAL(str)

void main()
{
  printf("Simple %s!\n", TOSTRING(KEK_SYS));
}
