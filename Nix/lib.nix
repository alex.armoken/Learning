{ pkgs }: rec {
  makeOverridable = f: origArgs:
    let
      origRes     = f origArgs;
    in
      origRes // { override = newArgs: makeOverridable f (origArgs // newArgs); };

  callPackage     = path: overrides:
    let
      f                = import path;
      intersectedArgs = (builtins.intersectAttrs (builtins.functionArgs f) pkgs) // overrides;
    in
      makeOverridable f intersectedArgs;
}
