	.model   small
	.stack   100h
	.386

	.data
	file_name db "hello.txt", 0
	buffer    db "asmworld.ru", 13, 10, "Hello!"
	size      db 19
	s_error   db "Error!", 13, 10, "$"
	s_pak     db "Press any key...$"
	handle    dw 1              ;Дескриптор файла

	.code
	assume ds:@data, es:@data

	main:
	mov ax, @data
	mov ds, ax
	mov es, ax

	start:
	    mov ah, 3Ch              ;Функция DOS 3Ch (создание файла)
	    lea dx, file_name        ;Имя файла
	    xor cx, cx               ;Нет атрибутов - обычный файл
	    int 21h                 ;Обращение к функции DOS
	    jnc @F                  ;Если нет ошибки, то продолжаем
	    call error_msg          ;Иначе вывод сообщения об ошибке
	    jmp exit                ;Выход из программы
	@F:
		mov [handle], ax         ;Сохранение дескриптора файла

	    mov bx, ax               ;Дескриптор файла
	    mov ah, 40h              ;Функция DOS 40h (запись в файл)
	    lea dx, buffer           ;Адрес буфера с данными
	    movzx cx, [size]         ;Размер данных
	    int 21h                 ;Обращение к функции DOS
	    jnc close_file          ;Если нет ошибки, то закрыть файл
	    call error_msg          ;Вывод сообщения об ошибке

	close_file:
	    mov ah, 3Eh              ;Функция DOS 3Eh (закрытие файла)
	    mov bx, [handle]         ;Дескриптор
	    int 21h                 ;Обращение к функции DOS
	    jnc exit                ;Если нет ошибки, то выход из программы
	    call error_msg          ;Вывод сообщения об ошибке

	exit:
	    mov ah, 9
	    lea dx, s_pak
	    int 21h                 ;Вывод строки 'Press any key...'
	    mov ah, 8                ;\
	    int 21h                 ;/ Ввод символа без эха
	    mov ax, 4C00h            ;\
	    int 21h                 ;/ Завершение программы

	error_msg PROC
	    mov ah, 9
	    lea dx, s_error
		int 21h
		ret
	error_msg ENDP

	end     main
