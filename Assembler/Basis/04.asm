	.model   small
	.stack   100h
	.386
	.data
	text_endline db 13, 10, "$"			;Line end in DOS
	text_check db "Compare working$"
	input_buf db 1000 dup(?)

	word_buf_one db 1000 dup(?)
	word_buf_two db 1000 dup(?)
	word_buf_temp db 1000 dup(?)

	word_one_count dw 0
	word_two_count dw 0

	word_one_length dw 0
	word_two_length dw 0
	word_temp_length dw 0

	max_buf_length dw 1000

	.code
	assume ds:@data, es:@data

	main:
	mov ax, @data
	mov ds, ax
	mov es, ax

	lea di, input_buf
	call Input_Text
	lea dx, text_endline
	call Output_Text

	xor cx, cx
	lea si, input_buf
	Most_Frequent:
		test cx, cx					;;Check buffer's end
		jnz Most_Frequent_Found

		;; Get next word
		lea di, word_buf_one
		call Get_Word
		;; Get word's length
		push cx
		push si
		xor ax, ax
		lea si, word_buf_one
		call Get_Word_Length
		mov word_one_length, cx
		pop si
		pop cx

		push si
		push cx
		lea si, input_buf
		xor cx, cx
		xor bx, bx					;;Word count
		Most_Frequent_Subcycle:
			test cx, cx  				;;Check buffer's end
			jnz Most_Frequent_Subcycle_End

			;; Get next word
			lea di, word_buf_temp
			call Get_Word
			;; Get word's length
			push cx
			push si
			xor ax, ax
			lea si, word_buf_temp
			call Get_Word_Length
			mov word_temp_length, cx
			pop si
			pop cx

			;;Word count
			push cx
			push di
			push si
			push ax
			push bx
			lea si, word_buf_one
			lea di, word_buf_temp
			mov ax, word_one_length
			mov bx, word_temp_length
			call Compare_Words
			pop bx
			pop ax
			test cx, cx
			jnz SubCycle_Words_Equal
			jz SubCycle_Words_Not_Equal

			SubCycle_Words_Equal:
			inc bx
			SubCycle_Words_Not_Equal:
			pop si
			pop di
			pop cx
			jmp Most_Frequent_Subcycle
		Most_Frequent_Subcycle_End:
		pop cx
		pop si

	mov word_one_count, bx

	mov ax, word_one_count
	mov bx, word_two_count
	cmp ax, bx
	ja Word_One_Offer
	jna Word_Two_Offer

	Word_One_Offer:
		push di
		push si
		push cx
		mov cx, max_buf_length
		lea si, word_buf_one
		lea di, word_buf_two
		call Swap_Buffers

		xchg ax, bx
		mov word_one_count, ax
		mov word_two_count, bx
		pop cx
		pop si
		pop di
	Word_Two_Offer:

	jmp Most_Frequent
	Most_Frequent_Found:

	lea dx, word_buf_two
	call Output_Text
	Exit:								;Exit
		mov ax, 4c00h
		int 21h

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Functions;;;;;;;;;;;;;;;;;;;;;;

	Get_Word PROC
		push bx
		push ax
		push di

		xor ax, ax
		xor bx, bx						;bl - current symbol, bh - previous symbol
										;cx - check end of the string
		mov bl, ' '
		cld

		Skip_Spaces:
			lodsb

			xchg bh, bl
			mov bl, al

			cmp al, ' '
			jz Space
			cmp al, '$'
			jz Buffer_End
			jmp Not_Space

		Space:
			cmp bh, ' '
			jz Skip_Spaces
			jnz Buffer_End

		Not_Space:
			stosb
			jmp Skip_Spaces

		Buffer_End:
			cmp al, '$'
			jz String_End
			jnz Not_String_End

			String_End:
				mov cx, 1
			Not_String_End:
				mov al, '$'
				stosb
		pop di
		pop ax
		pop bx
		ret
	Get_Word ENDP

	Swap_Buffers PROC
	;;Input words must be in si & di
	;;Buffet length must be in cx
	;;Length of buffers must be equal
		push ax

		Swap_Buffers_Cycle:
			xor ax, ax
			lodsb
			mov ah, al

			xchg si, di
			lodsb

			dec si
			dec di

			stosb
			xchg si, di
			xchg ah, al
			stosb
		loop Swap_Buffers_Cycle

		pop ax
		ret
	Swap_Buffers ENDP

	Get_Word_Length PROC
		push ax
		push si
		xor ax, ax
		xor cx, cx 				;; Cx is out

		Get_Word_Length_Cycle:
			lodsb
			cmp al, '$'
			jz Get_Word_Length_End_Cycle
			inc cx
			jmp Get_Word_Length_Cycle

		Get_Word_Length_End_Cycle:
		pop si
		pop ax
		ret
	Get_Word_Length ENDP

	Compare_Words PROC
	;;Input words must be in si & di
	;;Their length must be in ax & bx respectively
	;;Output will be in cx, if 1 they are equal, if 0 they aren't equal

		push ax
		push di
		push si

		xor cx, cx
		mov cx, 1

		cmp ax, bx
		jnz Words_Length_Not_Equal
		jz Continue_Compare

		Words_Length_Not_Equal:
			xor cx, cx
			jmp Compare_Words_End

		Continue_Compare:
			xor ax, ax
			lodsb
			xchg ah, al

			cmp ah, '$'
			jz Compare_Words_End

			xchg si, di
			lodsb
			xchg si, di

			cmp ah, al
			jz Continue_Compare
			jnz Words_Not_Equal

		Words_Not_Equal:
			xor cx, cx

		Compare_Words_End:

		pop si
		pop di
		pop ax
		ret
	Compare_Words ENDP

	Input_Remove_Symbol PROC
			push ax
			push bx
			push cx
	        push dx

			xor dx, dx					;;Get cursor position
			mov ah, 03h
			int 10h

			mov ah, 02h					;;Move cursor back by one position
			dec dl
			int 10h

			push dx						;;Remove last symbol
			mov ah, 02h
			mov dl, ' '
			int 21h
			pop dx
			mov ah, 02h
			int 10h

			pop dx
			pop cx
			pop bx
			pop ax
			ret
	Input_Remove_Symbol ENDP

	Input_Text PROC
		push ax							;;Ax get input symbol
		push bx							;;Bx store begining adress of buffer
										;;Cx is count of symbols
		push dx							;;Dx using for check overflow
		push di							;;Di store number
		push si							;;Si using for check overflow

		xor si, si
		xor cx, cx
		xor bx, bx
		Get:
			xor ax, ax
			mov ah, 08h
			int 21h

			cmp al, 13					;;Enter
			jz DOS_Enter
			cmp al, 8					;;Backspace
			jz DOS_Backspace
			cmp al, 27					;;Esc
			jz DOS_Esc
			cmp al, '$'
			jz Get
			jmp Do_It

			DOS_Esc:
				test cx, cx
				jz Get
				Cycle:
				call Input_Remove_Symbol
				dec di
				loop Cycle

			xor cx, cx
			jmp Get

			DOS_Backspace:
				push dx
	 			push ax
				push bx

				test cx, cx
				jz Deleted

				Remove_Symbol:
					call Input_Remove_Symbol
					dec di
					dec cx

				Deleted:
					pop bx
	 					pop ax
					pop dx
			jmp Get

			DOS_Enter:
				mov byte ptr [di], '$'
				mov max_buf_length, cx
				jmp End_Input

			Do_It:
				mov byte ptr [di], al
				inc di
				inc cx

				mov dx, max_buf_length
				cmp cx, dx
				jnb Overflow
				jmp Show

				Overflow:
					dec di
					dec cx
					jmp Get

			Show:
				xor dx, dx
				mov dl, al
				mov ah, 02h
				int 21h
			jmp Get

		End_Input:
			pop si
			pop di
			pop dx
			pop bx
			pop ax
			ret
	Input_Text ENDP

	Output_Text PROC 					;;Output text
		push ax
		mov ah, 09h
		int 21h
		pop ax
		xor dx, dx
		ret
	Output_Text ENDP

	Output_Unsign PROC					;;Unsign to string
		push ax							;;Ax is input number
		push bx
		push cx
		push dx
		xor cx, cx						;;Char count
		mov bx, 10

		Output_Unsign_Split:
			xor dx, dx					;;Zeroing the rest
			div bx
			add dl, '0'
			push dx
			inc cx						;;Increase the char count
			test ax, ax
		jnz Output_Unsign_Split

		mov ah, 02h
		Output_Unsign_Merge:
			pop dx
			int 21h
		loop Output_Unsign_Merge		;;Counter is CX

		pop dx
		pop cx
		pop bx
		pop ax
		ret
	Output_Unsign ENDP

	end     main
	o
