	.386
	.model   small
	.stack   100h

	.data
	text_endline db 13, 10, "$"			;Line end in DOS
	text_space db " $", "$"
	text_matrix_dimension db "Please, input matrix dimension <= 3: $"
	text_test db "test", "$"

	input_buf db 1000 dup(?)
	max_buf_length dw 1000

	nmatrix dw 9 dup (0)
	matrix_dimension dw 0
	matrix_columns_space dw 0
	el_size dw 2

	.code
	assume ds:@data, es:@data

	main:
	mov ax, @data
	mov ds, ax
	mov es, ax

	lea dx, text_matrix_dimension
	call Output_Text
	call Input_Number
	mov matrix_dimension, ax
	lea dx, text_endline
	call Output_Text
	cmp ax, 3
	ja Exit
	test ax, ax
	jz Exit

	lea dx, text_endline
	call Output_Text

	mov bx, matrix_dimension
	lea si, matrix
	call Input_Matrix

	lea dx, text_endline
	call Output_Text
	lea dx, text_endline
	call Output_Text

	mov bx, matrix_dimension
	lea si, matrix
	call Output_Matrix

	cmp matrix_dimension, 1
	jz determinant_1
	cmp matrix_dimension, 2
	jz determinant_2
	cmp matrix_dimension, 3
	jz determinant_3
	jnz Exit

	determinant_1:
	lea si, matrix
	mov ax, [si]
	call Output_Unsign
	jmp Exit

	determinant_2:
	lea si, matrix
	mov ax, [si]

	add si, 6
	mov bx, [si]
	mul bx
	mov cx, ax

	lea si, matrix
	add si, 4
	mov ax, [si]
	sub si, 2
	mov bx, [si]
	mul bx

	sub cx, ax
	cmp cx, 0
	jl Negative_Determinant_2
	jmp Not_Negative_Determinant_2
	Negative_Determinant_2:
	neg cx
	Not_Negative_Determinant_2:
	xchg ax, cx
	call Output_Unsign
	jmp Exit

	determinant_3:
	lea si, matrix	;;a11 * a22 * a33
	mov ax, [si]
	add si, 8
	mov bx, [si]
	mul bx

	add si, 8
	mov bx, [si]
	mul bx

	mov di, ax

	lea si, matrix 	;;a21 * a32 * a13
	add si, 2
	mov ax, [si]

	add si, 8
	mov bx, [si]
	mul bx

	add si, 2
	mov bx, [si]
	mul bx
	add di, ax

	lea si, matrix 	;;a12 * a23 * a31
	add si, 4
	mov ax, [si]

	add si, 2
	mov bx, [si]
	mul bx

	add si, 8
	mov bx, [si]
	mul bx
	add di, ax


	lea si, matrix	;;a31 * a22 * a13
	add si, 4
	mov ax, [si]

	add si, 4
	mov bx, [si]
	mul bx

	add si, 4
	mov bx, [si]
	mul bx
	mov cx, ax

	lea si, matrix 	;;a21 * a12 * a33
	add si, 2
	mov ax, [si]

	add si, 4
	mov bx, [si]
	mul bx

	add si, 10
	mov bx, [si]
	mul bx
	add cx, ax

	lea si, matrix 	;;a11 * a32 * a23
	mov ax, [si]
	add si, 10
	mov bx, [si]
	mul bx

	add si, 4
	mov bx, [si]
	mul bx
	add cx, ax

	sub di, cx

	cmp di, 0
	jl Negative_Determinant_3
	jmp Not_Negative_Determinant_3

	Negative_Determinant_3:
	neg di
	Not_Negative_Determinant_3:
	xchg ax, di
	call Output_Unsign
	Exit:								;Exit
	mov ax, 4c00h
	int 21h

	;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Functions;;;;;;;;;;;;;;;;;;;;;;

	Input_Matrix PROC
	push ax
	push bx				;; Matrix dimension
	push cx				;; Counter
	push dx
	push si				;; Store matrix

	xor cx, cx
	inc cx

	matrix_input:
	push cx
	xor cx, cx
	inc cx
	push si
	column_cycle_input:
	call Input_Number
	mov [si], ax

	lea dx, text_space
	call Output_Text

	cmp cx, matrix_dimension
	jz column_cycle_input_end

	inc cx
	add si, bx
	add si, bx
	jmp column_cycle_input
	column_cycle_input_end:
	pop si
	pop cx

	cmp cx, matrix_dimension
	jz matrix_input_end

	xor dx, dx
	lea dx, text_endline
	call Output_Text

	inc cx
	inc si
	inc si
	jmp matrix_input
	matrix_input_end:

	pop si
	pop dx
	pop cx
	pop bx
	pop ax
	ret
	Input_Matrix ENDP

	Output_Matrix PROC
	push ax
	push bx				;; Matrix dimension
	push cx				;; Counter
	push dx
	push si				;; Store matrix
	push di				;; Store column's space

	mov ax, bx			;; Get column's space
	dec ax
	mov di, ax

	cld

	xor cx, cx
	inc cx

	matrix_output:
	push cx
	xor cx, cx
	inc cx
	push si
	column_cycle_output:
	lodsw
	call Output_Unsign

	lea dx, text_space
	call Output_Text

	cmp cx, bx
	jz column_cycle_output_end

	inc cx
	add si, di
	add si, di
	jmp column_cycle_output
	column_cycle_output_end:
	pop si
	pop cx

	cmp cx, bx
	jz matrix_output_end

	lea dx, text_endline
	call Output_Text

	inc cx
	inc si
	inc si
	jmp matrix_output
	matrix_output_end:

	pop di
	pop si
	pop dx
	pop cx
	pop bx
	pop ax
	ret
	Output_Matrix ENDP

	Input_Remove_Symbol PROC
	push ax
	push bx
	push cx
	push dx

	xor dx, dx					;;Get cursor position
	mov ah, 03h
	int 10h

	mov ah, 02h					;;Move cursor back by one position
	dec dl
	int 10h

	push dx						;;Remove last symbol
	mov ah, 02h
	mov dl, ' '
	int 21h
	pop dx
	mov ah, 02h
	int 10h

	pop dx
	pop cx
	pop bx
	pop ax
	ret
	Input_Remove_Symbol ENDP

	LOCALS
	Input_Text PROC
	push ax							;;Ax get input symbol
	push bx							;;Bx store begining adress of buffer
	;;Cx is count of symbols
	push dx							;;Dx using for check overflow
	push di							;;Di store number
	push si							;;Si using for check overflow

	xor si, si
	xor cx, cx
	xor bx, bx
	@@Get:
	xor ax, ax
	mov ah, 08h
	int 21h

	cmp al, 13					;;Enter
	jz @@DOS_Enter
	cmp al, 8					;;Backspace
	jz @@DOS_Backspace
	cmp al, 27					;;Esc
	jz @@DOS_Esc
	cmp al, '$'
	jz @@Get
	jmp @@Do_It

	@@DOS_Esc:
	test cx, cx
	jz @@Get
	@@Cycle:
	call Input_Remove_Symbol
	dec di
	loop @@Cycle

	xor cx, cx
	jmp @@Get

	@@DOS_Backspace:
	push dx
	push ax
	push bx

	test cx, cx
	jz @@Deleted

	@@Remove_Symbol:
	call Input_Remove_Symbol
	dec di
	dec cx

	@@Deleted:
	pop bx
	pop ax
	pop dx
	jmp @@Get

	@@DOS_Enter:
	mov byte ptr [di], '$'
	mov max_buf_length, cx
	jmp @@End_Input

	@@Do_It:
	mov byte ptr [di], al
	inc di
	inc cx

	mov dx, max_buf_length
	cmp cx, dx
	jnb @@Overflow
	jmp @@Show

	@@Overflow:
	dec di
	dec cx
	jmp Get

	@@Show:
	xor dx, dx
	mov dl, al
	mov ah, 02h
	int 21h
	jmp @@Get

	@@End_Input:
	pop si
	pop di
	pop dx
	pop bx
	pop ax
	ret
	Input_Text ENDP

	Input_Number PROC
	push bx							;Di store number
	push cx							;Final number will in Ax
	push dx							;Cx is count of symbols
	push si
	push di

	xor di, di
	xor si, si
	xor cx, cx
	Get:
	xor ax, ax
	mov ah, 08h
	int 21h

	cmp al, 13					;Enter
	jz DOS_Enter
	cmp al, 8					;Backspace
	jz DOS_Backspace
	cmp al, 27					;Esc
	jz DOS_Esc

	cmp al, '9'
	ja Get
	cmp al, '0'
	jb Get
	jmp Do_It

	DOS_Esc:
	test cx, cx
	jz Get
	Cycle:
	call Input_Remove_Symbol
	loop Cycle

	xor di, di
	jmp Get

	DOS_Backspace:
	push dx
	push ax
	push bx

	test cx, cx
	jz Deleted

	Remove_Symbol:
	call Input_Remove_Symbol
	dec cx
	test di, di
	je Deleted

	Reduce_Number:
	xor ax, ax
	xor dx, dx
	xor bx, bx

	mov ax, di
	mov bx, 10
	div bx
	mov di, ax

	Deleted:
	pop bx
	pop ax
	pop dx
	jmp Get

	DOS_Enter:
	mov ax, di
	test si, si
	jz Unsign
	neg ax
	Unsign:
	jmp End_Input

	Do_It:
	push ax
	push cx
	push di

	sub al, '0'				;Convert symbol
	mov cl, al

	xor dx, dx
	xor ax, ax
	mov ax, di
	mov bx, 10
	mul bx
	jc Overlimit
	add ax, cx
	jc Overlimit
	mov di, ax

	jnc Not_Overlimit
	Overlimit:
	pop di
	pop cx
	pop ax
	jmp Get
	Not_Overlimit:
	pop cx					;Erase old Di value
	xor cx, cx
	pop cx
	pop ax

	Show:
	inc cx
	xor dx, dx
	mov dl, al
	mov ah, 02h
	int 21h
	jmp Get

	End_Input:
	pop di
	pop si
	pop dx
	pop cx
	pop bx
	ret
	Input_Number ENDP


	Output_Text PROC 					;;Output text
	push ax
	mov ah, 09h
	int 21h
	pop ax
	xor dx, dx
	ret
	Output_Text ENDP

	Output_Unsign PROC					;;Unsign to string
	push ax							;;Ax is input number
	push bx
	push cx
	push dx
	xor cx, cx						;;Char count
	mov bx, 10

	Output_Unsign_Split:
	xor dx, dx					;;Zeroing the rest
	div bx
	add dl, '0'
	push dx
	inc cx						;;Increase the char count
	test ax, ax
	jnz Output_Unsign_Split

	mov ah, 02h
	Output_Unsign_Merge:
	pop dx
	int 21h
	loop Output_Unsign_Merge		;;Counter is CX

	pop dx
	pop cx
	pop bx
	pop ax
	ret
	Output_Unsign ENDP

	end     main
