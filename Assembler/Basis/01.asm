	.model   small
	.stack   200h
	.data
	a dw 3
	b dw 3
	c dw 3
	d dw 3
	.code
	main:
	mov ax, @data
	mov ds, ax

								;a * c + b * d = a * d + b * c
								;a * c + b * d - a * d = b * c

								;Left  expression
	mov	ax, a 					; a * c
	mul	c
	mov	cx, ax

	mov	ax, b					;b * d
	mul	d

	add	cx, ax

	mov	ax, a					;a * d
	mul	d

	sub cx, ax

								;Right expression
	mov ax, b					;b * c
	mul c

	cmp ax, cx
	jz Equally
	jmp NotEqually

	Equally:
	mov ax, a
	mul a
	jmp ProgrEnd

	NotEqually:					;a > c
	mov ax, a
	mov bx, c
	cmp ax, bx
	jnc More
	jmp NotMore

	More:						;с AND b
	mov ax, c
	mov bx, b
	and ax, bx
	jmp ProgrEnd

	NotMore:					;a – (b OR c)
	mov ax, b
	mov cx, a
	or ax, c
	sub cx, ax
	mov ax, cx

	ProgrEnd:
	mov ax, 4c00h
	int 21h

	end     main
