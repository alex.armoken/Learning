@echo off
cls
if not exist op.* goto Compile
del op.*

:Compile
tasm %1 op.obj
tlink op.obj
echo --------------------------------------------------------------------------------
if %2.==. op.exe
if %2.==d. td op
:Exit
echo .
