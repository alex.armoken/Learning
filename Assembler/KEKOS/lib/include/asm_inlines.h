#include <stdint.h>

#ifndef ASM_INLINES
#define ASM_INLINES

void write_to_port(uint16_t port, uint8_t val);
uint8_t read_from_port(uint16_t port);

#endif
