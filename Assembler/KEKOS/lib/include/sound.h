#include <stdint.h>

#include "asm_inlines.h"

#ifndef SOUND
#define SOUND

void play_sound(uint32_t frequence);
void nosound();
void timer_wait(unsigned int t);
void beep(uint32_t frequence, unsigned int time);
void play_melody(unsigned int time);
void mario_song();

#endif
