#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "string.h"

#ifndef VGA
#define VGA

enum vga_color {
	VGA_COLOR_BLACK = 0,
	VGA_COLOR_BLUE = 1,
	VGA_COLOR_GREEN = 2,
	VGA_COLOR_CYAN = 3,
	VGA_COLOR_RED = 4,
	VGA_COLOR_MAGENTA = 5,
	VGA_COLOR_BROWN = 6,
	VGA_COLOR_LIGHT_GREY = 7,
	VGA_COLOR_DARK_GREY = 8,
	VGA_COLOR_LIGHT_BLUE = 9,
	VGA_COLOR_LIGHT_GREEN = 10,
	VGA_COLOR_LIGHT_CYAN = 11,
	VGA_COLOR_LIGHT_RED = 12,
	VGA_COLOR_LIGHT_MAGENTA = 13,
	VGA_COLOR_LIGHT_BROWN = 14,
	VGA_COLOR_WHITE = 15,
};

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;

static const size_t TASK_GUI_START_Y = 1;
static const size_t TASK_GUI_START_X = 1;
static const size_t TASK_GUI_END_Y = 10;
static const size_t TASK_GUI_END_X = 78;
static const size_t TASK_GUI_WIDTH = 77;
static const size_t TASK_GUI_HEIGHT = 10;

static const size_t TERMINAL_START_Y = 12;
static const size_t TERMINAL_START_X = 1;
static const size_t TERMINAL_END_Y = 23;
static const size_t TERMINAL_END_X = 78;
static const size_t TERMINAL_WIDTH = 77;
static const size_t TERMINAL_HEIGHT = 13;

typedef struct vga {
	uint16_t *buffer;
} vga_t;

typedef struct terminal {
	vga_t base;

	size_t row;
	size_t column;

	char term_line[77];
	size_t command_row;
	size_t command_column;

	uint8_t color;
	bool isShiftPressed;
} Terminal;


uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg);
uint16_t vga_entry(unsigned char uc, uint8_t color);


Terminal* get_task_terminal_instance();
Terminal* get_interrupt_terminal_instance();

void clear_screen_with_colors(vga_t *vga, uint8_t colors);
void fill_screen_with_colors(vga_t *vga, uint8_t colors);
void draw_term_borders(Terminal *term, uint8_t color);

void vga_put_char_at(vga_t *vga, char c, uint8_t color,
	size_t x, size_t y);
void terminal_put_char(Terminal *instance, char c);

void terminal_write(Terminal *instance, const char* data, size_t size);
void terminal_write_string(Terminal *instance, const char* data);

void terminal_write_string_at(Terminal *instance, const char *data,
	size_t x, size_t y);

#endif
