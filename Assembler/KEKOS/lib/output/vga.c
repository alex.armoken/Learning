#include "../include/vga.h"


uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg) {
	return  bg << 4 | fg;
}

uint16_t vga_entry(unsigned char uc, uint8_t color) {
	return (uint16_t) uc | (uint16_t) color << 8;
}

void clear_screen_with_colors(vga_t *vga, uint8_t colors) {
	for (unsigned int i = 0; i < VGA_WIDTH * VGA_HEIGHT; i++) {
		vga->buffer[i] = (colors << 8) | 32;
	}
}

void fill_screen_with_colors(vga_t *vga, uint8_t colors) {
	for (unsigned int i = 0; i < VGA_WIDTH * VGA_HEIGHT; i++) {
		uint8_t data = vga->buffer[i];
		vga->buffer[i] = (colors << 8) | data;
	}
}


void scroll_down(Terminal *instance) {
	const uint16_t blank = 0x20 | instance->color << 8;
	instance->column = TERMINAL_START_X;
	instance->row++;

	if (instance->row > TERMINAL_END_Y) {
		uint16_t *buffer = ((vga_t*)instance)->buffer;
		uint16_t start = VGA_WIDTH * TERMINAL_START_Y;
		uint16_t end = VGA_WIDTH * TERMINAL_END_Y;

		for (uint32_t i = start; i < end; i += VGA_WIDTH) {
			for (uint32_t j = TERMINAL_START_X; j <= TERMINAL_END_X; j++) {
				buffer[i + j] = buffer[i + j + VGA_WIDTH];
			}
		}

		uint16_t blank_start = TERMINAL_END_Y * VGA_WIDTH + TERMINAL_START_X;
		uint16_t blank_end = TERMINAL_END_Y * VGA_WIDTH + VGA_WIDTH - 1;
		for (uint32_t i = blank_start; i < blank_end; i++) {
			buffer[i] = blank;
		}

		instance->row--;
	}
}

void vga_put_char_at(vga_t *vga,
	char c, uint8_t color, size_t x, size_t y) {

	const size_t index = y * VGA_WIDTH + x;
	vga->buffer[index] = vga_entry(c, color);
}

void terminal_put_char(Terminal *term, char c) {
	vga_put_char_at((vga_t*)term, c, term->color,
		term->column, term->row);

	if (++term->column > TERMINAL_END_X) {
		scroll_down(term);
	} else if(c == '\n') {
		vga_put_char_at((vga_t*)term, ' ', term->color,
			term->column - 1, term->row);
		scroll_down(term);
		term->command_column = 0;
		term->command_row = term->row;
	}
}


void terminal_write(Terminal *instance, const char* data, size_t size) {
	for (size_t i = 0; i < size; i++) {
		terminal_put_char(instance, data[i]);
	}
}

void terminal_write_string(Terminal *instance, const char *data) {
	terminal_write(instance, data, strlen(data));
}

void terminal_write_string_at(Terminal *instance, const char *data,
	size_t x, size_t y) {

	for (unsigned int i = 0; i < strlen(data); i++) {
		vga_put_char_at((vga_t*)instance, data[i],
			instance->color, x, y);

		if (++x == VGA_WIDTH) {
			x = 0;
			if (++y == VGA_HEIGHT) {
				y = 0;
			}
		}
	}
}

void draw_term_borders(Terminal *term, uint8_t color) {
	const uint8_t border = 32;
	for (uint16_t i = 0; i <= VGA_HEIGHT; i++) {
		vga_put_char_at((vga_t*)term, border, color, 0, i);
		vga_put_char_at((vga_t*)term, border, color, VGA_WIDTH - 1, i);
	}

	for (uint16_t i = 1; i < VGA_WIDTH - 1; i++) {
		vga_put_char_at((vga_t*)term, border, color, i, 0);
		vga_put_char_at((vga_t*)term, border, color, i, VGA_HEIGHT / 2 - 1);
		vga_put_char_at((vga_t*)term, border, color, i, VGA_HEIGHT - 1);
	}
}

Terminal* get_task_terminal_instance() {
	static bool isInit = false;
	static Terminal instance;

	if (!isInit) {
		instance.row = TASK_GUI_START_X;
		instance.column = TASK_GUI_START_Y;
		instance.color = vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_BLACK);
		((vga_t*)&instance)->buffer = (uint16_t*) 0xB8000;
		isInit = true;
	}

	return &instance;
}

Terminal* get_interrupt_terminal_instance() {
	static Terminal instance;
	static bool isInit = false;

	if (!isInit) {
		instance.row = TERMINAL_START_Y;
		instance.column = TERMINAL_START_X;
		instance.color = vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_BLACK);
		((vga_t*)&instance)->buffer = (uint16_t*) 0xB8000;
		fill_screen_with_colors((vga_t*)&instance, instance.color);
		isInit = true;
	}

	return &instance;
}
