#include "../include/sound.h"
#include "../include/asm_inlines.h"

//Play sound using built in speaker
void play_sound(uint32_t frequence) {
 	uint32_t Div;
 	uint8_t tmp;

	//Set the PIT to the desired frequency
 	Div = 1193180 / frequence;
 	write_to_port(0x43, 0xb6);
 	write_to_port(0x42, (uint8_t) (Div) );
 	write_to_port(0x42, (uint8_t) (Div >> 8));

	//And play the sound using the PC speaker
 	tmp = read_from_port(0x61);
  	if (tmp != (tmp | 3)) {
 		write_to_port(0x61, tmp | 3);
 	}
}


void nosound() {
 	uint8_t tmp = read_from_port(0x61) & 0xFC;
 	write_to_port(0x61, tmp);
}


void timer_wait(unsigned int t) {
	unsigned long long eticks = 1500 * t;
	while(eticks > 100) eticks--;
}


void beep(uint32_t frequence, unsigned int time) {
	play_sound(frequence);
	timer_wait(time);
	nosound();
}

void play_melody(unsigned int time) {
	beep(523, time);
	beep(587, time);
	beep(659, time);
	beep(698, time);
	beep(784, time);
}

void mario_song() {
	beep(330,100);
	beep(330,100);
	beep(330,100);
	beep(262,100);
	beep(330,100);
	beep(392,100);
	beep(196,100);
	beep(262,300);
	beep(196,300);
	beep(164,300);
	beep(220,300);
	beep(246,100);
	beep(233,200);
	beep(220,100);
	beep(196,100);
	beep(330,100);
	beep(392,100);
	beep(440,100);
	beep(349,100);
	beep(392,100);
	beep(330,100);
	beep(262,100);
	beep(294,100);
	beep(247,100);
	beep(262,300);
	beep(196,300);
	beep(164,300);
	beep(220,300);
	beep(246,100);
	beep(233,200);
	beep(220,100);
	beep(196,100);
	beep(330,100);
	beep(392,100);
	beep(440,100);
	beep(349,100);
	beep(392,100);
	beep(330,100);
	beep(262,100);
	beep(294,100);
	beep(247,100);
	beep(392,100);
	beep(370,100);
	beep(349,100);
	beep(311,100);
	beep(330,100);
	beep(207,100);
	beep(220,100);
	beep(262,100);
	beep(220,100);
	beep(262,100);
	beep(294,100);
	beep(392,100);
	beep(370,100);
	beep(349,100);
	beep(311,100);
	beep(330,100);
	beep(523,100);
	beep(523,100);
	beep(523,100);
	beep(392,100);
	beep(370,100);
	beep(349,100);
	beep(311,100);
	beep(330,100);
	beep(207,100);
	beep(220,100);
	beep(262,100);
	beep(220,100);
	beep(262,100);
	beep(294,100);
	beep(311,300);
	beep(296,300);
	beep(262,300);
	beep(262,100);
	beep(262,100);
	beep(262,100);
	beep(262,100);
	beep(294,100);
	beep(330,200);
	beep(262,200);
	beep(220,200);
	beep(196,100);
	beep(262,100);
	beep(262,100);
	beep(262,100);
	beep(262,100);
	beep(294,100);
	beep(330,100);
	beep(440,100);
	beep(392,100);
	beep(262,100);
	beep(262,100);
	beep(262,100);
	beep(262,100);
	beep(294,100);
	beep(330,200);
	beep(262,200);
	beep(220,200);
	beep(196,100);
	/*Intro*/
	beep(330,100);
	beep(330,100);
	beep(330,100);
	beep(262,100);
	beep(330,100);
	beep(392,100);
	beep(196,100);
	beep(196,100);
	beep(262,100);
	beep(330,100);
	beep(392,100);
	beep(523,100);
	beep(660,100);
	beep(784,100);
	beep(660,100);
	beep(207,100);
	beep(262,100);
	beep(311,100);
	beep(415,100);
	beep(523,100);
	beep(622,100);
	beep(830,100);
	beep(622,100);
	beep(233,100);
	beep(294,100);
	beep(349,100);
	beep(466,100);
	beep(587,100);
	beep(698,100);
	beep(932,100);
	beep(932,100);
	beep(932,100);
	beep(932,100);
	beep(1046,675);
}
