[BITS 32]
section .text

[extern isr_handler]
isr_common_stub:
	pusha

	mov ax, ds
	push eax

	mov ax, 16
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	call isr_handler

	pop eax
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax

	popa
	add esp, 8
	sti
	iret


%macro IST_NOERRCODE 1
[global isr%1]
	isr%1:
	cli
	push byte 0
	push byte %1
	jmp isr_common_stub
%endmacro

%macro IST_ERRCODE 1
[global isr%1]
	isr%1:
	cli
	push byte %1
	jmp isr_common_stub
%endmacro


	IST_NOERRCODE 0
	IST_NOERRCODE 1
	IST_NOERRCODE 2
	IST_NOERRCODE 3
	IST_NOERRCODE 4
	IST_NOERRCODE 5
	IST_NOERRCODE 6
	IST_NOERRCODE 7
	IST_ERRCODE 8
	IST_NOERRCODE 9
	IST_ERRCODE 10
	IST_ERRCODE 11
	IST_ERRCODE 12
	IST_ERRCODE 13
	IST_ERRCODE 14
	IST_NOERRCODE 15
	IST_NOERRCODE 16
	IST_NOERRCODE 17
	IST_NOERRCODE 18
	IST_NOERRCODE 19
	IST_NOERRCODE 20
	IST_NOERRCODE 21
	IST_NOERRCODE 22
	IST_NOERRCODE 23
	IST_NOERRCODE 24
	IST_NOERRCODE 25
	IST_NOERRCODE 26
	IST_NOERRCODE 27
	IST_NOERRCODE 28
	IST_NOERRCODE 29
	IST_NOERRCODE 30
	IST_NOERRCODE 31
