;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Protected mode output func ;;;;;;;;;;;;;;;;;;
out_print_pfunc:
        pushad
        cld
.out_print_pfunc_loop:                          ; Output loop
        lodsb
        test al, al                             ; Is end of line
        jz .exit_out_print_pfunc

        stosb
        mov al, 9bh
        stosb
        jmp .out_print_pfunc_loop

.exit_out_print_pfunc:
        cld
        popad
        ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


