;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Reset floppy disk ;;;;;;;;;;;;;;;;;;;;;;;;;;
reset_floppy:
    mov ah, 0
    ; DL = drive nummber
    int 0x13
    jc reset_floppy
    ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Disk load  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; load DH sectors to ES : BX from drive DL
disk_load :
	push dx

	mov ah, 0x02				; BIOS read sector function
	mov al, dh					; Read DH sectors
	mov ch, 0x00				; Select cylinder 0
	mov dh, 0x00				; Select head 0
	mov cl, 0x02				; Start reading from second sector

	int 0x13					; BIOS interrupt
	jc disk_error 				; Jump if error ( i.e. carry flag set )

	pop dx
	cmp dh, al					; if AL (sectors read) != DH (sectors expected)
	jne disk_error

	mov si, DISK_READ_SUCCESS_MSG
	call out_print_rfunc
	ret

disk_error:
	mov si, DISK_READ_ERROR_MSG
	call out_print_rfunc
	jmp $

;; Variables
	DISK_READ_ERROR_MSG db "Disk read error!", 0
	DISK_READ_SUCCESS_MSG db "Disk sectors successfully loaded!", 0
