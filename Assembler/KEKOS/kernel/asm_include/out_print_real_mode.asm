;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Real mode output func ;;;;;;;;;;;;;;;;;;;;;;;
out_print_rfunc:
	pushad
	mov ah, 0x0E				; Set BIOS func
	mov bh, 0x00				; Set video-memory page

	cld
.out_print_rfunc_loop:
	lodsb
	test al, al 				; Is end of line
	jz .exit_out_print_rfunc

	int 0x10				; Run BIOS func to show symbol
	jmp .out_print_rfunc_loop

.exit_out_print_rfunc:
	cld
	popad
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
