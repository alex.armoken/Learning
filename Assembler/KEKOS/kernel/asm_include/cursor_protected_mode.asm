
;; Set cursor invisable
;; mov bx, 0x1c1b
;; call ChangeCursor

;; Set cursor visable
;; mov bx, 0x0b0c
;; call ChangeCursor


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; GetCursor ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; out: bx = cursor attribs ;;
GetCursor:
    push  ax
    push  dx
    mov   dx, 0x3D4
    mov   al, 0x0E
    out   dx, al
    inc   dx
    in    al, dx
    mov   bh, al
    mov   al, 0x0F
    dec   dx
    out   dx, al
    inc   dx
    in    al, dx
    mov   bl, al
    pop   dx
    pop   ax
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ChangeCursor ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; in: bx = cursor attribs ;;
ChangeCursor:
    pushad
    mov   dx, 0x3D4
    mov   al, 0x0A
    mov   ah, bh
    out   dx, ax
    inc   ax
    mov   ah, bl
    out   dx, ax
    popad
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; GoToXY ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; See  CursorSetXY ;;
GoToXY:
    pushad
    mov   dl, byte[PosX]
    mov   dh, byte[PosY]
    mov   al, 80
    mul   dh
    xor   dh, dh
    add   ax, dx
    mov   cx, ax
    mov   dx, 0x03d4
    mov   al, 0x0e
    out   dx, al
    inc   dx
    mov   al, ch
    out   dx, al
    mov   dx, 0x3d4
    mov   al, 0x0f
    out   dx, al
    inc   dx
    mov   al, cl
    out   dx, al
    popad
    ret


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; CursorSetXY ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; in: al = cursorX   ah = cursorY ;;
CursorSetXY:
    mov   byte[PosX], al
    mov   byte[PosY], ah
    call  GoToXY
    ret

;; Variables
	Cursor dw 0
	PosX   db 0
	PosY   db 0
