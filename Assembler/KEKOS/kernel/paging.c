#include "./c_include/paging.h"

const int READ_ONLY = 0;
const int READ_AND_WRITE = 1;

const int KERNEL_PAGE = 0;
const int NOT_A_KERNEL_PAGE = 1;

const int CREATE_PAGE = 1;
const int NOT_CREATE_PAGE = 0;


uint32_t *phys_frames;
uint32_t phys_frames_count;

extern uint32_t placement_address;

page_directory_t *cur_directory;
page_directory_t *kernel_directory;



void* kmalloc(uint32_t size) {
	uint32_t tmp = placement_address;
	placement_address += size;
	return tmp;
}

void* kmalloc_a(uint32_t size) {
	if (placement_address & 0xFFFFF000) {
		placement_address &= 0xFFFFF000;
		placement_address += 0x1000;
	}
	return kmalloc(size);
}

void* kmalloc_p(uint32_t size, uint32_t *phys) {
	*phys = placement_address;
	return kmalloc(size);
}

void* kmalloc_ap(uint32_t size, uint32_t *phys) {
	uint32_t result = kmalloc_a(size);
	*phys = placement_address - size;
	return result;
}



static inline uint32_t index_in_frames_bitset(uint32_t a) {
	return a / (8 * 4);
}

static inline uint32_t offset_in_frames_bitset(uint32_t a) {
	return a % (8 * 4);
}

void set_phys_memory_frame_used(uint32_t frame_address) {
	uint32_t frame = frame_address / 0x1000; // 0x1000 = 4096
	uint32_t index = index_in_frames_bitset(frame);
	uint32_t offset = offset_in_frames_bitset(frame);
	phys_frames[index] |= (0x1 << offset); // =!BiTMaGic!=
}

void clear_phys_memory_frame(uint32_t frame_address) {
	uint32_t frame = frame_address / 0x1000; // 0x1000 = 4096
	uint32_t index = index_in_frames_bitset(frame);
	uint32_t offset = offset_in_frames_bitset(frame);
	phys_frames[index] &= ~(0x1 << offset); // =!BiTMaGic!=
}

uint32_t is_frame_used(uint32_t frame_address) {
	uint32_t frame = frame_address / 0x1000; // 0x1000 = 4096
	uint32_t index = index_in_frames_bitset(frame);
	uint32_t offset = offset_in_frames_bitset(frame);
	return (phys_frames[index] & (0x1 << offset)); // =!BiTMaGic!=
}

int32_t get_first_unused_frame() {
	for (uint32_t i = 0; i < index_in_frames_bitset(phys_frames_count); i++) {
		if (phys_frames[i] != 0xFFFFFFFF) {
			for (uint32_t j = 0; j < 32; j++) {
				if (!(phys_frames[i] & (0x1 << j))) { // =!BiTMaGic!=
					return i * 4 * 8 + j;
				}
			}
		}
	}
	return -1;
}



static inline void set_or_unset_bit(
	uint32_t *number, uint32_t bit_number, int flag) {

	if (flag) {
		*number |= 1 << bit_number;
	} else {
		*number &= ~(1 << bit_number);
	}
}

void alloc_phys_memory_frame(uint32_t *p, int user, int rw) {
	uint32_t page = *p;
	if (page >> 12 == 0) {
		int32_t index = get_first_unused_frame();
		set_phys_memory_frame_used((uint32_t)index * 0x1000);

		page &= !0b11111111111111111111000000000000; // Setting frame
		page |= index << 12;

		set_or_unset_bit(&page, 0, 1); // Setting present bit
		set_or_unset_bit(&page, 1, rw); // Setting rw bit
		set_or_unset_bit(&page, 2, user); // Setting user bit

		*p = page;
	}
}

void free_phys_memory_frame(uint32_t *page) {
	uint32_t frame = (*page >> 12) << 12;
	if (frame) {
		clear_phys_memory_frame(frame);
		*page &= !0b11111111111111111111000000000000;
	}
}



void create_page_table(uint32_t index, page_directory_t *dir) {
	uint32_t p = 0;
	dir->tables[index] = kmalloc_ap(sizeof(page_table_t), &p);
	memset(dir->tables[index], 0, sizeof(page_table_t));
	dir->tablesPhysAddresses[index] = p | 0x3; // Present, RW, US
}

uint32_t* get_page(uint32_t address, int create, page_directory_t *dir) {
	address /= 0x1000; // 4096
	uint32_t table_index = address / 1024;

	if (dir->tables[table_index]) {
		return &dir->tables[table_index]->pages[address % 1024];
	} else if (create) {
		create_page_table(table_index, dir);
		return &dir->tables[table_index]->pages[address % 1024];
	}
	return 0;
}



static inline int check_bit(uint32_t value, uint8_t bit_num) {
	return value >> bit_num & 1;
}

static inline uint32_t copy_flags(page_table_t *src, uint32_t index) {
	uint32_t page = src->pages[index];
	set_or_unset_bit(&page, 0, check_bit(page, 0));
	set_or_unset_bit(&page, 1, check_bit(page, 1));
	set_or_unset_bit(&page, 2, check_bit(page, 2));
	set_or_unset_bit(&page, 3, check_bit(page, 3));
	set_or_unset_bit(&page, 4, check_bit(page, 4));

	return page;
}

static page_table_t* clone_table(page_table_t *src, uint32_t *phys) {
	page_table_t *table = kmalloc_ap(sizeof(page_table_t), phys);
	memset(table, 0, sizeof(page_directory_t));

	for (uint32_t i = 0; i < 1024; i++) {
		if (src->pages[i] >> 12 != 0 ) {
			alloc_phys_memory_frame(&table->pages[i], 0, 0);

			table->pages[i] = copy_flags(src, i);

			/* copy_page_physicaly((src->pages[i] >> 12) * 0x1000, */
			/* 	(table->pages[i] >> 12) * 0x1000); */
		}
	}

	return table;
}

page_directory_t* clone_directory(page_directory_t *src) {
	uint32_t phys;
	page_directory_t *new = kmalloc_ap(sizeof(page_directory_t), &phys);
	memset(new, 0, sizeof(page_directory_t));

	uint32_t offset = (uint32_t)new->tablesPhysAddresses - (uint32_t)new;
	new->physAddress = phys + offset;

	// Copy or make link all page tables
	for (uint32_t i = 0; i < 1024; i++) {
		if (!src->tables[i]) continue;

		if (kernel_directory->tables[i] == src->tables[i]) {
			new->tables[i] = src->tables[i];
			new->tablesPhysAddresses[i] = src->tablesPhysAddresses[i];
		} else {
			uint32_t phys;
			new->tables[i] = clone_table(src->tables[i], &phys);
			new->tablesPhysAddresses[i] = phys | 0x07;
		}
	}

	return new;
}



static inline void init_phys_memory_frames() {
	uint32_t mem_last_page_address = 0x8000000; // 128M
	phys_frames_count = mem_last_page_address / 0x1000; // 4096
	phys_frames = (uint32_t*)kmalloc(
		index_in_frames_bitset(phys_frames_count));
	memset(phys_frames, 0, index_in_frames_bitset(phys_frames_count));
}

static inline page_directory_t* init_kernel_page_directory() {
	kernel_directory = (page_directory_t*)kmalloc_a(sizeof(page_directory_t));
	memset(kernel_directory, 0, sizeof(page_directory_t));
	return kernel_directory;
}

static inline void build_identify_map() {
	// physic address = virtual address
	unsigned int adress = 0;
	while (adress < placement_address) {
		uint32_t *p = get_page(adress, CREATE_PAGE, kernel_directory);
		alloc_phys_memory_frame(p, KERNEL_PAGE, READ_ONLY);
		adress += 0x1000;
	}
}

void set_page_directory(page_directory_t *directory) {
	cur_directory = directory;
	__asm("mov %0, %%cr3":: "r"(kernel_directory->tablesPhysAddresses));

	uint32_t cr0;
	__asm("mov %%cr0, %0": "=r"(cr0));

	cr0 |= 0x80000000; // Enable paging
	__asm("mov %0, %%cr0":: "r"(cr0));
}

void init_paging() {
	init_phys_memory_frames();
	cur_directory = init_kernel_page_directory();
	build_identify_map();
}



void page_fault_handler(registers_t regs) {
	Terminal *term = get_interrupt_terminal_instance();
	terminal_write_string_at(term, "KEKEKEKK", 10, 10);
}
