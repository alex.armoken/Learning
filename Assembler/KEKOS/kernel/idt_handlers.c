#include "./c_include/idt_handlers.h"


///////////// Exceptions interrupts ///////////////
const unsigned int DivideByZeroErrorException = 0;
const unsigned int DebugException = 1;
const unsigned int NonmaskableInterruptException = 2;
const unsigned int BreakpointException = 3;
const unsigned int OverflowException = 4;
const unsigned int BoundRangeExceededException = 5;
const unsigned int InvalidOpcodeException = 6;
const unsigned int DeviceNotAvailableException = 7;
const unsigned int DoubleFaultException = 8;
const unsigned int CoprocessorSegmentOverrunException = 9; // DEPRECATED
const unsigned int InvalidTSSException = 10;
const unsigned int SegmentNotPresentException = 11;
const unsigned int StackSegmentFaultException = 12;
const unsigned int GeneralProtectionFaultException = 13;
const unsigned int PageFaultException = 14;
const unsigned int Reserved0Exception = 15;
const unsigned int FloatingPointException = 16;
const unsigned int AlignmentCheckException = 17;
const unsigned int MachineCheckException = 18;
const unsigned int SIMDFloatingPointException = 19;
const unsigned int VirtualizationException = 20;
const unsigned int Reserved1Exception = 21;
const unsigned int Reserved2Exception = 22;
const unsigned int Reserved3Exception = 23;
const unsigned int Reserved4Exception = 24;
const unsigned int Reserved5Exception = 25;
const unsigned int Reserved6Exception = 26;
const unsigned int Reserved7Exception = 27;
const unsigned int Reserved8Exception = 28;
const unsigned int Reserved9Exception = 29;
const unsigned int SecurityExceptionException = 30;
const unsigned int Reserved10Exception = 31;


///////////// PIC Interrupts /////////////
const unsigned int IRQ0 = 32;
const unsigned int IRQ1 = 33;
const unsigned int IRQ2 = 34;
const unsigned int IRQ3 = 35;
const unsigned int IRQ4 = 36;
const unsigned int IRQ5 = 37;
const unsigned int IRQ6 = 38;
const unsigned int IRQ7 = 39;
const unsigned int IRQ8 = 40;
const unsigned int IRQ9 = 41;
const unsigned int IRQ10 = 42;
const unsigned int IRQ11 = 43;
const unsigned int IRQ12 = 44;
const unsigned int IRQ13 = 45;
const unsigned int IRQ14 = 46;
const unsigned int IRQ15 = 47;


isr_t interrupt_handlers[256];

char *interrupt_names[] = {
///////////// Exceptions interrupts ///////////////
	[0] = "Divide By Zero Error Exception",
	[1] = "Debug Exception",
	[2] = "Nonmaskable Interrupt Exception",
	[3] = "Breakpoint Exception",
	[4] = "Overflow Exception",
	[5] = "Bound Range Exceeded Exception",
	[6] = "Invalid Opcode Exception",
	[7] = "Device Not Available Exception",
	[8] = "Double Fault Exception",
	[9] = "Coprocessor Segment Overrun Exception",
	[10] = "Invalid TSS Exception",
	[11] = "Segment Not Present Exception",
	[12] = "Stack Segment Fault Exception",
	[13] = "General Protection Fault Exception",
	[14] = "Page Fault Exception",
	[15] = "Reserved 0 Exception",
	[16] = "Floating Point Exception",
	[17] = "Alignment Check Exception",
	[18] = "Machine Check Exception",
	[19] = "SIMD Floating Point Exception",
	[20] = "Virtualization Exception",
	[21] = "Reserved 1 Exception",
	[22] = "Reserved 2 Exception",
	[23] = "Reserved 3 Exception",
	[24] = "Reserved 4 Exception",
	[25] = "Reserved 5 Exception",
	[26] = "Reserved 6 Exception",
	[27] = "Reserved 7 Exception",
	[28] = "Reserved 8 Exception",
	[29] = "Reserved 9 Exception",
	[30] = "Security Exception Exception",
	[31] = "Reserved 10 Exception",

/////////////  Interrupts /////////////
	[32] = "IRQ 0",
	[33] = "IRQ 1",
	[34] = "IRQ 2",
	[35] = "IRQ 3",
	[36] = "IRQ 4",
	[37] = "IRQ 5",
	[38] = "IRQ 6",
	[39] = "IRQ 7",
	[40] = "IRQ 8",
	[41] = "IRQ 9",
	[42] = "IRQ 10",
	[43] = "IRQ 11",
	[44] = "IRQ 12",
	[45] = "IRQ 13",
	[46] = "IRQ 14",
	[47] = "IRQ 15"
};

static const char scancode[58] = {
	0, 0, '1', '2', '3', '4', '5',
	'6', '7', '8', '9', '0', '-',
	'=', 0, 0, 'q', 'w', 'e', 'r',
	't', 'y', 'u', 'i', 'o', 'p',
	'[', ']', 0, 0, 'a', 's', 'd',
	'f', 'g', 'h', 'j', 'k', 'l',
	';', '\'', '`', 0, '\\', 'z',
	'x', 'c', 'v', 'b', 'n',
	'm', ',', '.', '/', 0, 0, 0, ' '
};

static const char shift_scancode[58] = {
	0, 0, '!', '@', '#', '$', '%',
	'^', '&', '*', '(', ')', '_', '+',
	0, 0, 'Q', 'W', 'E', 'R', 'T',
	'Y', 'U', 'I', 'O', 'P', '{', '}',
	0, 0, 'A', 'S', 'D', 'F',
	'G', 'H', 'J', 'K', 'L',
	':', '"', '~', 0, '|', 'Z', 'X',
	'C', 'V', 'B', 'N', 'M', '<', '>',
	'?', 0, 0, 0, ' '
};


static void print_interrupt_name(uint8_t number) {
	Terminal *term = get_interrupt_terminal_instance();
	if (term->column != TERMINAL_START_X) {
		terminal_put_char(term, '\n');
	}
	terminal_write_string(term, interrupt_names[number]);
	terminal_put_char(term, '\n');
}

void isr_handler(registers_t regs) {
	if (interrupt_handlers[regs.int_no] != NULL) {
		interrupt_handlers[regs.int_no](regs);
	} else {
		print_interrupt_name(regs.int_no);
	}
}

void irq_handler(registers_t regs) {
	if (regs.int_no >= 40) {
		write_to_port(0xA0, 0x20);
	}
	write_to_port(0x20, 0x20);

	if (interrupt_handlers[regs.int_no] != 0 &&
		interrupt_handlers[regs.int_no] != NULL) {
		interrupt_handlers[regs.int_no](regs);
	} else {
		print_interrupt_name(regs.int_no);
	}
}


static const char *commands[5] = {
	[0] = "!", // Pause tasks
	[1] = ":", // Reset tasks positions
	[2] = "-", // Remove one task
	[3] = "+", // Add one tasks
	[4] = "=", // Change tasks direction
};

static void process_command(char *command, Terminal *term) {
	for (uint32_t i = 0; i < 5; i++) {
		if (strcmp(command,	commands[i]) == 0) {
			if (i == 0) {
				kernel_task->is_paused = !kernel_task->is_paused;
			} else if (i == 1) {
				kernel_task->is_need_to_reset_tasks_positions = true;
			} else if (i == 2) {
				if (kernel_task->cur_tasks_count > 0) {
					kernel_task->cur_tasks_count--;
					kernel_task->is_tasks_count_changed = true;
				}
			} else if (i == 3) {
				if (kernel_task->cur_tasks_count < MAX_TASKS_COUNT) {
					kernel_task->cur_tasks_count++;
					kernel_task->is_tasks_count_changed = true;
				}
			} else if (i == 4) {
				kernel_task->is_need_to_change_direction = true;
			}
		}
	}
}

void print_user_input(Terminal *t, uint8_t keycode) {
	char c = t->isShiftPressed ? shift_scancode[keycode] : scancode[keycode];
	terminal_put_char(t, c);
	t->term_line[t->command_column] = c;
}

void keyboard_handler(registers_t regs) {
	uint8_t keycode = read_from_port(0x60);
	Terminal *term = get_interrupt_terminal_instance();
	beep(700, 1000);

	switch(keycode) {
	case 0xAA: // LShift unpressed
		term->isShiftPressed = false;
		break;
	default:
		if((keycode & 128) != 128) {
			switch(keycode) {
			case 0x0F: // Tab
				kernel_task->is_something_changed = true;
				kernel_task->is_need_to_change_axis = true;
				terminal_write_string(term, "Changing axis\n");
				break;
			case 0x1C: // Enter
				term->term_line[term->command_column + 1] = '\0';
				terminal_put_char(term, '\n');
				process_command(term->term_line, term);
				kernel_task->is_something_changed = true;
				break;
			case 0x01: // Esc
				kernel_task->is_alive = false;
				terminal_write_string(term, "Stopping tasks, leaving OS\n");
				break;
			case 0x2A: // LShift pressed
				term->isShiftPressed = true;
				break;
			default:
				print_user_input(term, keycode);
				break;
			}
		}
		break;
	}
}


void register_exceptions_handlers() {
	for (uint32_t i = 0; i < IRQ0; i++) {
		interrupt_handlers[i] = NULL;
	}
	interrupt_handlers[PageFaultException] = &page_fault_handler;
}

void register_irq_handlers() {
	for (uint32_t i = IRQ0; i <= IRQ15; i++) {
		interrupt_handlers[i] = NULL;
	}
	interrupt_handlers[IRQ0] = &timer_handler;
	interrupt_handlers[IRQ1] = &keyboard_handler;
}
