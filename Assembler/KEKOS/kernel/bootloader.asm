[BITS 16]
[org 0x7c00]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Code ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
true_program_start:
start:
	jmp 0x0000:begin

%include "kernel/asm_include/out_print_real_mode.asm"
%include "kernel/asm_include/disk_load.asm"

begin:
;; Data segment = code segment: ;;
	mov ax, cs
	mov ds, ax


;; Save current drive id
	mov [bootDrive], dl


;; Clear screen
	mov ax, 0x0003
	int 0x10


;; Reset floppy drive
	call reset_floppy


;; Load 5 sectors
	mov dh, 0x30				; Load 5 sectors
	mov bx, 0x9000				; To 0x9000 memory adress
	call disk_load

;; Set cursor position ;;
	mov ah, 0x02
	mov bh, 0 					; BH = Page Number
	mov dh, 1					; DH = Row
	mov dl, 0					; DL = Column
	int 10h


	mov si, realModeMsg
	call out_print_rfunc

	jmp dword 0000:0x9000		; Jump to loaded code


.hang:	hlt
	jmp .hang

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Data ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; String constants ;;
	realModeMsg db 'Welcome to real world', 0

	bootDrive db 0

true_program_end:
	; Fill the remainder of program, except two lasts bytes, by zero
	times (0x1FE - true_program_end + true_program_start) db 0
	; Signature of loading sector (2 last bytes)
	db 0x55, 0xAA
