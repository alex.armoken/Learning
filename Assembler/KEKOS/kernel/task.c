#include "./c_include/task.h"

extern void switchTask(registers_t *from, registers_t *to);

static task_t *tasks[10];
static task_t *cur_task = NULL;
kernel_task_t *kernel_task = NULL;

static uint8_t next_task_number = 0;


void preempt() {
	task_t *last = cur_task;
	cur_task = cur_task->next;
	switch_task(&last->regs, &cur_task->regs);
}


static inline Directions update_x_direction(uint32_t x, Directions dir) {
	if (x == TASK_GUI_START_X) {
		return FORWARD;
	} else if (x == TASK_GUI_END_X)	{
		return BACKWARD;
	}
	return dir;
}

static inline Directions update_y_direction(uint32_t y, Directions dir) {
	if (y == TASK_GUI_START_Y) {
		return FORWARD;
	} else if (y == TASK_GUI_END_Y)	{
		return BACKWARD;
	}
	return dir;
}

void update_direction(task_t *p) {
	if (p->axis == HORIZONTAL) {
		p->direction = update_x_direction(p->x, p->direction);
	} else if (p->axis == VERTICAL){
		p->direction = update_y_direction(p->y, p->direction);
	}
}

void change_pos(task_t *p, uint8_t clear, Terminal *gui, uint8_t id) {
	vga_put_char_at((vga_t*)gui, ' ', clear, p->x, p->y);
	update_direction(p);
	p->x += (p->axis == HORIZONTAL) ? p->direction : 0;
	p->y += (p->axis == VERTICAL) ? p->direction : 0;
	vga_put_char_at((vga_t*)gui, '0' + id, p->color, p->x, p->y);
}

static void task_main() {
	Terminal *gui = get_task_terminal_instance();
	uint8_t clear = gui->color;

	uint8_t id = next_task_number++;
	task_t *p = tasks[id];

	uint64_t sleep_duration = 0;

	while(1) {
		sleep_duration++;
		if (sleep_duration >= p->required_sleep_duration) {
			change_pos(p, clear, gui, id);
			sleep_duration = 0;
		}
		preempt();
	}
}

void hide_all_tasks() {
	Terminal *gui = get_task_terminal_instance();
	uint8_t clear = gui->color;
	for (uint8_t i = 0; i < MAX_TASKS_COUNT; i++) {
		vga_put_char_at((vga_t*)gui, ' ', clear, tasks[i]->x, tasks[i]->y);
	}
}

void update_tasks_count() {
	hide_all_tasks();
	for (uint8_t i = 0; i < kernel_task->cur_tasks_count - 1; i++) {
		tasks[i]->next = tasks[i + 1];
	}
	if (kernel_task->cur_tasks_count != 0) {
		tasks[kernel_task->cur_tasks_count - 1]->next = (task_t*)kernel_task;
		((task_t*)kernel_task)->next = tasks[0];
	}
}

void change_tasks_axes() {
	for (uint8_t i = 0; i < MAX_TASKS_COUNT; i++) {
		task_t *p = tasks[i];
		p->axis = (p->axis == HORIZONTAL) ? VERTICAL : HORIZONTAL;
	}
}

void change_tasks_directions() {
	for (uint8_t i = 0; i < MAX_TASKS_COUNT; i++) {
		task_t *p = tasks[i];
		p->direction = (p->direction == FORWARD) ? BACKWARD : FORWARD;
	}
}

void reset_tasks_posotions() {
	hide_all_tasks();
	for (uint8_t i = 0; i < MAX_TASKS_COUNT; i++) {
		task_t *p = tasks[i];
		if (p->axis == HORIZONTAL) {
			p->x = TASK_GUI_START_X;
		} else if (p->axis == VERTICAL) {
			p->y = TASK_GUI_START_Y;
		}
	}
}


void create_task(task_t *task, uint16_t id,
	void (*main)(),	uint32_t flags, uint32_t dir) {

    task->regs.eax = 0;
    task->regs.ebx = 0;
    task->regs.ecx = 0;
    task->regs.edx = 0;
    task->regs.esi = 0;
    task->regs.edi = 0;
	task->regs.ebp = 8;
    task->regs.eflags = flags;

    task->regs.cr3 = (uint32_t) dir;
    task->regs.eip = (uint32_t) main;
	tasks_stacks_start_place += 0x1000;
    task->regs.esp = tasks_stacks_start_place;

	task->direction = FORWARD;
	task->x = TASK_GUI_START_X;
	task->y = TASK_GUI_START_Y + id;

	task->required_sleep_duration = 10 * ((id + 1) * 70);
	task->color = vga_entry_color(VGA_COLOR_BLACK + id, VGA_COLOR_BLUE + id);
}


static inline void init_kernel_task() {
	kernel_task = kmalloc(sizeof(kernel_task_t));
	__asm(
		"movl %%cr3, %%eax;"
		"movl %%eax, %0;"
		: "=m"(((task_t*)kernel_task)->regs.cr3)
		:: "%eax");

    __asm(
		"pushfl;"
		"movl (%%esp), %%eax;"
		"movl %%eax, %0;"
		"popfl;"
		: "=m"(((task_t*)kernel_task)->regs.eflags)
		:: "%eax");

	((task_t*)kernel_task)->regs.ebp = 8;
	kernel_task->is_alive = true;
	kernel_task->is_paused = false;
	kernel_task->is_need_to_change_axis = false;
	kernel_task->is_tasks_count_changed = false;
}

static inline void init_other_tasks() {
	for (uint16_t id = 0; id < MAX_TASKS_COUNT; id++) {
		tasks[id] = kmalloc(sizeof(task_t));
		create_task(tasks[id], id, &task_main,
			((task_t*)kernel_task)->regs.eflags,
			((task_t*)kernel_task)->regs.cr3);
	}
}

void init_tasking() {
	init_kernel_task();
	init_other_tasks();
	kernel_task->cur_tasks_count = START_TASKS_COUNT;
	update_tasks_count();
	cur_task = (task_t*)kernel_task;
}
