#include "c_include/kernel.h"

const unsigned int default_melody_time = 1000;

extern page_directory_t *cur_directory;
extern page_directory_t *kernel_directory;

Terminal *term;


void init_memory() {
	init_paging();

	uint32_t start = tasks_stacks_start_place;
	uint32_t end = tasks_stacks_start_place + (MAX_TASKS_COUNT) * 0x1000;
	for (uint32_t i = start; i < end; i += 0x1000) {
		uint32_t *p = get_page(i, 1, kernel_directory);
		alloc_phys_memory_frame(p, 0, 1);
	}

	set_page_directory(kernel_directory);

	// Need to do this, because old placement address was real adress,
	// now we need to make it virtual
	placement_address = tasks_stacks_start_place;
}

void test_interrupts() {
	__asm ("int $0x0");
	__asm ("int $0x1");
	__asm ("int $0x2");
	__asm ("int $0x3");
	__asm ("int $0x4");
	__asm ("int $0x5");

	/* int kek = 3 / 0; // Division by zero */
	/* Test page fault */
	/* uint32_t *ptr = (uint32_t*)0x6400000; // Page fault */
	/* uint32_t *ptr = (uint32_t*)0x100000; // No page fault */
	/* uint32_t do_page_fault = *ptr; */
}

void terminal_init() {
	term = get_interrupt_terminal_instance();

	uint8_t colors = vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_BLACK);
	clear_screen_with_colors((vga_t*)term, colors);

	uint8_t color = vga_entry_color(VGA_COLOR_BLACK, VGA_COLOR_DARK_GREY);
	draw_term_borders(term, color);

	uint8_t tmp_color = term->color;
	term->color = vga_entry_color(VGA_COLOR_WHITE, VGA_COLOR_DARK_GREY);
	terminal_write_string_at(term, "TASKS", 1, 0);
	terminal_write_string_at(term, "TERMINAL", 1, 11);
	term->color = tmp_color;
}

void run_tasks() {
	while(kernel_task->is_alive) {
		if (kernel_task->is_something_changed) {
			if (kernel_task->is_tasks_count_changed) {
				update_tasks_count();
				kernel_task->is_tasks_count_changed = false;
			} else if (kernel_task->is_need_to_change_axis) {
				change_tasks_axes();
				kernel_task->is_need_to_change_axis = false;
			} else if (kernel_task->is_need_to_change_direction) {
				change_tasks_directions();
				kernel_task->is_need_to_change_direction = false;
			} else if (kernel_task->is_need_to_reset_tasks_positions) {
				reset_tasks_posotions();
				kernel_task->is_need_to_reset_tasks_positions = true;
			}
			kernel_task->is_something_changed = false;
		}
		if (kernel_task->cur_tasks_count && !kernel_task->is_paused) {
			preempt();
		}
	}
}

void kernel_main () {
	terminal_init();
	init_timer(100);
	init_idt();
	init_memory();
	init_tasking();
	test_interrupts();
	run_tasks();
	clear_screen_with_colors((vga_t*)term,
		vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_DARK_GREY));
}
