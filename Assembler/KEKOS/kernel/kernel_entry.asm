%define bit_32_code_selector 8
%define bit_32_data_selector 16
%define bit_32_stack_selector 24
%define screen_selector 32

%define bit_16_code_selector 40
%define bit_16_data_selector 48

[BITS 16]
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Code ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
section .text
kernel_entry_start:
	jmp 0000:start_kernel

%include "kernel/asm_include/out_print_real_mode.asm"

start_kernel:
;; Set cursor position ;;
	mov ah, 0x02
	mov bh, 0 					; BH = Page Number
	mov dh, 1					; DH = Row
	mov dl, 0					; DL = Column
	int 10h


;; Print real world message ;;
	mov si, realModeMsg
	call out_print_rfunc


;; Save segment registers using in real mode: ;;
	mov	[R_Mode_SS], ss
	mov	[R_Mode_DS], ds
	mov	[R_Mode_ES], es
	mov	[R_Mode_FS], fs
	mov	[R_Mode_GS], gs


;; Protected mode entry: ;;
	; Open A20 line ;
	in al, 0x92
	or al, 2
	out 0x92, al

	; Save idt ;
	sidt [IDT]
	; Load size and adress of GDT to GDTR ;
	lgdt [GDTR]
	; Disable interrupts ;
	cli
	; Disable unmasked interrupts ;
	in al, 0x70
	or al, 0x80
	out 0x70, al

	; Saving stack pointer at the last moment ;
	mov	[R_Mode_SP], sp

	; Switch to protected mode ;
	mov eax, cr0
	or al, 1
	mov cr0, eax

	; Jump to protected mode code ;
	jmp dword 00001000b:protected_mode_entry



	use32
extern kernel_main
%include "kernel/asm_include/out_print_protected_mode.asm"
%include "kernel/asm_include/cursor_protected_mode.asm"
protected_mode_entry:
;; Set segment registers ;;
	mov ax, bit_32_data_selector
	mov ds, ax
	mov ax, screen_selector
	mov es, ax
	mov ax, bit_32_stack_selector
	mov ss, ax


;; Hide text cursor ;;
	mov bx, 0x1c1b
	call ChangeCursor


;; Print protected world message ;;
	xor edi, edi
	mov edi, 0x140				; 0xA0 = 160
	mov esi, protectedModeMsg
	call out_print_pfunc

	call kernel_main

	jmp bit_16_code_selector:pre_real_mode_entry



[BITS 16]
pre_real_mode_entry:
;; Disable interrupts
	cli


;; Switch to real mode ;;
	mov	eax, cr0
	and	al, 0feh					; FEh = 1111'1110b
	mov	cr0, eax

	jmp 0:real_mode_entry

real_mode_entry:
;; Restore real mode segment registers ;;
	mov	ss, [R_Mode_SS]
	mov	ds, [R_Mode_DS]
	mov	es, [R_Mode_ES]
	mov	fs, [R_Mode_FS]
	mov	gs, [R_Mode_GS]
	mov	sp, [R_Mode_SP]

;; Restore idt ;;
	lidt [IDT]

;; Enable interrupts ;;
	sti


;; Set cursor position ;;
	mov ah, 0x02
	mov bh, 0 					; BH = Page Number
	mov dh, 3					; DH = Row
	mov dl, 0					; DL = Column
	int 10h


;; Print real world again message ;;
	mov si, realModeAgainMsg
	call out_print_rfunc

program_real_stop:
.hang:	hlt
	jmp .hang

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Data ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

section .bss
;; Regigisters values before protected mode ;;
R_Mode_SP:	resb 2
R_Mode_SS:	resb 2
R_Mode_DS:	resb 2
R_Mode_ES:	resb 2
R_Mode_FS:	resb 2
R_Mode_GS:	resb 2


section .data
;; String constants ;;
realModeMsg: db 'Welcome to real kernel world', 0
protectedModeMsg: db 'Welcome to protected world', 0
realModeAgainMsg: db 'Welcome to real world again', 0

IDT:
    dw 0x03FF
    dd 0x00000000

;; Global descriptor table ;;
GDT:
	; Zero descriptor ;
	dd 0, 0

	; 32-bit Code  descriptor ;
	dw 0xFFFF                           ; Segment limit (64K)
	db 0                                ; Base address  17e00
	db 0
	db 0
	db 10011010b                        ; 1001, C/D – 0, 0, R/W – 1, 0
	db 11001111b                        ; G - 0, 000, Limit - 0000
	db 00000000b                        ; Base address
	; 32-bit Data descriptor ;
	dw 0xFFFF					        ; Segment Limit (64K) -
	db 0                                ; Base address
	db 0
	db 0
	db 10010010b                        ; 1001, C/D – 0, 0, R/W – 1, 0
	db 11001111b                        ; G - 0, 000, Limit - 0000
	db 00000000b                        ; Base address
	; 32-bit Stack descriptor ;
	dw 0xFFFF                           ; Segment limit (16K)
	db 0                                ; Base address
	db 0
	db 0
	db 10010010b                        ; 1001, C/D – 0, 0, R/W – 1, 0
	db 11001111b                        ; G - 0, 000, Limit - 1111
	db 00000000b                        ; Base address
	; Video memory descriptor ;
	dw 0xFFFF                           ; Segment limit (16K)
	dw 0x8000
	db 0x0B                             ;Base 0xB8000
	db 10010010b                        ;Data read/write (02h)
	db 00000000b                        ; G - 0, 000, Limit - 0000
	db 00000000b                        ;Segment Base 31:24



	; 16-bit Code  descriptor ;
	dw 0xFFFF                           ; Segment limit (64K)
	db 0                                ; Base address  17e00
	db 0
	db 0
	db 10011010b                        ; 1001, C/D – 0, 0, R/W – 1, 0
	db 00001111b                        ; G - 0, 000, Limit - 0000
	db 00000000b                        ; Base address
	; 16-bit Data descriptor ;
	dw 0xFFFF					        ; Segment Limit (64K) -
	db 0                                ; Base address
	db 0
	db 0
	db 10010010b                        ; 1001, C/D – 0, 0, R/W – 1, 0
	db 00001111b                        ; G - 0, 000, Limit - 0000
	db 00000000b                        ; Base address

;; Global descriptor table size ;;
GDT_Size:  equ $ - GDT

;; Data loaded in GDTR register ;;
GDTR:
	dw  GDT_Size - 1
	dd  GDT
