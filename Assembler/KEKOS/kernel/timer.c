#include "./c_include/timer.h"


const unsigned int pit_frequency = 1193180;


void init_timer(uint32_t frequency) {
	uint32_t divisor = pit_frequency / frequency;

	// Sending command byte
	write_to_port(0x43, 0x36);

	// Setting frequency
	uint8_t l = (uint8_t)(divisor & 0xFF);
	uint8_t h = (uint8_t)((divisor>>8) & 0xFF);
	write_to_port(0x40, l);
	write_to_port(0x40, h);
}

void timer_handler(registers_t regs) {
	beep(523, 10);
}
