#include <stdint.h>

#ifndef SETTINGS
#define SETTINGS

extern uint32_t placement_address;
extern const uint8_t MAX_TASKS_COUNT;
extern const uint8_t START_TASKS_COUNT;
extern uint32_t tasks_stacks_start_place;

#endif
