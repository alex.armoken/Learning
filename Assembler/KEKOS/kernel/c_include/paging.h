#include <stdint.h>
#include "./idt.h"
#include "./setting.h"
#include "../../lib/include/vga.h"
#include "../../lib/include/sound.h"

#ifndef PAGING_H
#define PAGING_H

// Probably this is unsed structure,
// but it very good shows page format and bits order
typedef struct page
{
	uint32_t is_present_in_memory :1;
	uint32_t rw                   :1;
	uint32_t user                 :1;
	uint32_t accessed             :1;
	uint32_t dirty                :1;
	uint32_t unused               :7;
	uint32_t frame                :20;
} page_t;

typedef struct page_table
{
	uint32_t pages[1024];
} page_table_t;

typedef struct page_directory
{
	uint32_t tablesPhysAddresses[1024];
	page_table_t *tables[1024];
	uint32_t physAddress;
} page_directory_t;


extern uint32_t *phys_frames;
extern uint32_t phys_frames_count;
extern page_directory_t *cur_directory;
extern page_directory_t *kernel_directory;


void* kmalloc(uint32_t size);
void* kmalloc_a(uint32_t size);
void* kmalloc_p(uint32_t size, uint32_t *phys);
void* kmalloc_ap(uint32_t size, uint32_t *phys);


void alloc_phys_memory_frame(uint32_t *page, int kernel, int rw);
void free_phys_memory_frame(uint32_t *page);


void init_paging();
void set_page_directory(page_directory_t *directory);
uint32_t* get_page(uint32_t address, int create, page_directory_t *dir);

void page_fault_handler(registers_t regs);

#endif
