#include <stddef.h>
#include <stdint.h>

#include "../../lib/include/vga.h"
#include "../../lib/include/itoa.h"
#include "../../lib/include/sound.h"
#include "../../lib/include/asm_inlines.h"

#include "./task.h"
#include "./timer.h"
#include "./paging.h"
#include "./idt_installer.h"

#ifndef KERNEL
#define KERNEL

void kernel_main ();

#endif
