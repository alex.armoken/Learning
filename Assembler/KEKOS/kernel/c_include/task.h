#include <stdint.h>

#include "./paging.h"
#include "../../lib/include/vga.h"

#ifndef TASK_H
#define TASK_H

typedef enum axes {
	HORIZONTAL = 0,
	VERTICAL = 1,
} Axes;

typedef enum directions {
	FORWARD = 1,
	BACKWARD = -1,
} Directions;

typedef struct {
    uint32_t eax;
	uint32_t ebx;
	uint32_t ecx;
	uint32_t edx;

	uint32_t esi;
	uint32_t edi;

	uint32_t esp;
	uint32_t ebp;
	uint32_t eip;

	uint32_t eflags;
	uint32_t cr3;
} task_registers_t;

typedef struct task_t {
	task_registers_t regs;
	struct task_t *next;

	uint32_t x;
	uint32_t y;
	uint8_t color;
	Axes axis;
	Directions direction;

	uint64_t required_sleep_duration;
} task_t;

typedef struct kernel_task_t {
	task_t base; // Inheritance in C... O_o

	bool is_alive;

	uint8_t cur_tasks_count;
	uint8_t next_task_number;

	bool is_something_changed;

	bool is_paused;
	bool is_need_to_change_axis;
	bool is_tasks_count_changed;
	bool is_need_to_change_direction;
	bool is_need_to_reset_tasks_positions;
} kernel_task_t;

extern kernel_task_t *kernel_task;

void init_tasking();
void create_task(task_t *task, uint16_t id, void (main)(),
	uint32_t flags, uint32_t dir);

void change_tasks_axes();
void update_tasks_count();
void reset_tasks_posotions();
void change_tasks_directions();

void preempt();
void switch_task(task_registers_t *old, task_registers_t *new);

#endif
