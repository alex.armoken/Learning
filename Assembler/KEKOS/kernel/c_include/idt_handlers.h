#include <stdint.h>
#include <stddef.h>

#include "idt.h"
#include "timer.h"
#include "./task.h"
#include "./paging.h"
#include "../../lib/include/sound.h"
#include "../../lib/include/string.h"
#include "../../lib/include/asm_inlines.h"


#ifndef IDT_HANDLERS
#define IDT_HANDLERS

///////////// Exceptions interrupts ///////////////
extern const unsigned int DivideByZeroErrorException;
extern const unsigned int DebugException;
extern const unsigned int NonmaskableInterruptException;
extern const unsigned int BreakpointException;
extern const unsigned int OverflowException;
extern const unsigned int BoundRangeExceededException;
extern const unsigned int InvalidOpcodeException;
extern const unsigned int DeviceNotAvailableException;
extern const unsigned int DoubleFaultException;
extern const unsigned int CoprocessorSegmentOverrunException; // DEPRECATED
extern const unsigned int InvalidTSSException;
extern const unsigned int SegmentNotPresentException;
extern const unsigned int StackSegmentFaultException;
extern const unsigned int GeneralProtectionFaultException;
extern const unsigned int PageFaultException;
extern const unsigned int Reserved0Exception;
extern const unsigned int FloatingPointException;
extern const unsigned int AlignmentCheckException;
extern const unsigned int MachineCheckException;
extern const unsigned int SIMDFloatingPointException;
extern const unsigned int VirtualizationException;
extern const unsigned int Reserved1Exception;
extern const unsigned int Reserved2Exception;
extern const unsigned int Reserved3Exception;
extern const unsigned int Reserved4Exception;
extern const unsigned int Reserved5Exception;
extern const unsigned int Reserved6Exception;
extern const unsigned int Reserved7Exception;
extern const unsigned int Reserved8Exception;
extern const unsigned int Reserved9Exception;
extern const unsigned int SecurityExceptionException;
extern const unsigned int Reserved10Exception;


///////////// PIC Interrupts /////////////
extern const unsigned int IRQ0;
extern const unsigned int IRQ1;
extern const unsigned int IRQ2;
extern const unsigned int IRQ3;
extern const unsigned int IRQ4;
extern const unsigned int IRQ5;
extern const unsigned int IRQ6;
extern const unsigned int IRQ7;
extern const unsigned int IRQ8;
extern const unsigned int IRQ9;
extern const unsigned int IRQ10;
extern const unsigned int IRQ11;
extern const unsigned int IRQ12;
extern const unsigned int IRQ13;
extern const unsigned int IRQ14;
extern const unsigned int IRQ15;


extern isr_t interrupt_handlers[256];

void register_exceptions_handlers();
void register_irq_handlers();

#endif
