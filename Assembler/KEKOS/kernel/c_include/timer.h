#include <stdint.h>
#include "../../lib/include/asm_inlines.h"
#include "../../lib/include/sound.h"
#include "./idt.h"

#ifndef TIMER
#define TIMER

extern const unsigned int pit_frequency;

void init_timer(uint32_t frequency);
void timer_handler(registers_t regs);

#endif
