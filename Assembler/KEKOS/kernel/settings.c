#include "./c_include/setting.h"

const uint8_t MAX_TASKS_COUNT = 10;
const uint8_t START_TASKS_COUNT = 5;

uint32_t placement_address = 0x100000;
uint32_t tasks_start_place = 0x7100000;
uint32_t tasks_stacks_start_place = 0x7400000;
