CC = gcc
ASM = nasm
LD = ld
CFLAGS = -m32 -std=c11 -c -Wall -Wextra -lgcc -ffreestanding
LDFLAGS = -m elf_i386 --oformat binary -nostdlib

C_KERNEL_SOURCES = lib/asm/asm_inlines.c lib/output/sound.c lib/output/vga.c kernel/kernel.c kernel/idt_installer.c lib/string/memcmp.c lib/string/memcpy.c lib/string/memmove.c lib/string/memset.c lib/string/strlen.c kernel/timer.c kernel/idt_handlers.c kernel/paging.c kernel/settings.c lib/string/itoa.c kernel/task.c lib/string/strcmp.c
ASM_KERNEL_SOURCES = kernel/kernel_entry.asm kernel/isr.asm kernel/irq.asm kernel/task_switch.asm
ASM_BOOTLOADER_SOURCES = kernel/bootloader.asm

KERNEL_OBJECTS = $(C_KERNEL_SOURCES:.c=.o)
KERNEL_OBJECTS += $(ASM_KERNEL_SOURCES:.asm=.o)

all: generate_image

generate_image: compile_bootloader compile_kernel
	cat tmp/bootloader.bin tmp/kernel.bin > tmp/os_img
	@echo

	dd if=/dev/zero of=os.img bs=1024 count=1440
	@echo

	dd if=tmp/os_img of=os.img conv=notrunc
	@echo

compile_bootloader:
	@mkdir -p tmp
	$(ASM) -f bin -o tmp/bootloader.bin $(ASM_BOOTLOADER_SOURCES)
	@echo

compile_kernel: $(KERNEL_OBJECTS)
	@mkdir -p tmp
	@echo
	$(LD) $(LDFLAGS) -T link.ld -o tmp/kernel.bin $(KERNEL_OBJECTS)
	@echo

%.o: %.c
	$(CC) $(CFLAGS) $< -o $@

%.o: %.asm
	$(ASM) $< -f elf32 -o $@

run_and_show:
	qemu-system-i386 -drive file=os.img,format=raw,index=0,if=floppy -boot a -soundhw pcspk -m 4G


run_and_debug:
	qemu-system-i386 -drive file=os.img,format=raw,index=0,if=floppy -s -S -vnc none -boot b -soundhw pcspk


clean:
	rm -f kernel/*~ kernel/*.o
	rm -f lib/asm/*.o
	rm -f lib/output/*.o
	rm -f lib/string/*.o
	rm os.img
	rm -r tmp
