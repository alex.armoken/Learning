#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>
#include <math.h>
#include <unistd.h>

const int IgnoreZero = 1;
const int NotIgnoreZero = 0;
const int DefArrSize = 10;
const int KeyBackspace = 127;
const double FantasticFour = 4;
const double FantasticTwo = 2;


int checkNonZeroChar(char* inStr)
{
	const int NineCharNum = 39;
	const int ZeroCharNum = 30;

	for (int i = ZeroCharNum + 1; i < NineCharNum; i++)
	{
		char* nonZeroChar;
		if ((nonZeroChar = strchr(inStr, (char)i)))
		{
			return 1;
		}
	}

	return 0;
}


void resizeStr (char** inStr, unsigned int* arraySize)
{
	char* newArr = (char*)calloc(*arraySize * 2, sizeof(char));
	strcpy(newArr, *inStr);
	free(*inStr);
	*inStr = newArr;
	*arraySize = *arraySize *2;
}


double numInput(int ignoreZero)
{
	char inChar;
	int isNeg = 0;
	int isFrac = 0;
	double result = 0;
	unsigned int arrSize = DefArrSize;
	char* allNumChars = (char*)calloc(DefArrSize, sizeof(char));

	refresh();

	while (1)
	{
		inChar = getch();
		if (inChar == '\n')
		{
			sscanf(allNumChars, "%lf", &result);
			if (strlen(allNumChars) != 0 &&
				allNumChars[strlen(allNumChars) - 1] != '.' &&
				!(isNeg && result == 0) &&
				!(ignoreZero && result == 0))
			{
				break;
			}
			result = 0;
		}
		else if ((int)inChar == KeyBackspace)
		{
			if (strlen(allNumChars) != 0 || isNeg)
			{
				printw("\b \b");
			}

			if (isFrac && allNumChars[strlen(allNumChars) - 1] == '.')
			{
				isFrac = 0;
				allNumChars[strlen(allNumChars) - 1] = 0;
			}
			else if (isNeg && strlen(allNumChars) == 0)
			{
				isNeg = 0;
			}
			else
			{
				allNumChars[strlen(allNumChars) - 1] = 0;

			}
		}
		else
		{
			if (inChar == '.')
			{
				if (!isFrac && strlen(allNumChars) != 0)
				{
					isFrac = 1;
					allNumChars[strlen(allNumChars)] = inChar;
					addch(inChar);
				}
			}
			else if (inChar == '-')
			{
				if (!isNeg && strlen(allNumChars) == 0)
				{
					isNeg = 1;
					addch(inChar);
				}
			}
			else if (inChar <= '9' && inChar >= '0')
			{
				if (strlen(allNumChars) == arrSize - 1)
				{
					resizeStr(&allNumChars, &arrSize);
				}
				allNumChars[strlen(allNumChars)] = inChar;
				addch(inChar);
			}
		}
		inChar = 0;
		refresh();
	}
	addch('\n');

	if (isNeg)
	{
		result = -result;
	}
	return result;
}


int main()
{
	double a;
	double b;
	double c;

	double x1 = 0;
	double x2 = 0;

	double discriminant;

	initscr();
	noecho();
	raw();
	refresh();

	char inChar = 0;
	while (inChar != 'q' || inChar == '\n')
	{
		inChar = 0;
		clear();

		printw("Please, input a: ");
		a = numInput(IgnoreZero);
		printw("Please, input b: ");
		b = numInput(NotIgnoreZero);
		printw("Please, input c: ");
		c = numInput(NotIgnoreZero);

		refresh();
		__asm (
			".intel_syntax noprefix \n" // Calculate discriminant (D)
			"finit\n"

			"fld %2\n" // b * b
			"fmul %2\n"

			"fld %1\n" // a * c * 4
			"fmul %3\n"
			"fmul %4\n"

			"fsubp st(1), st(0) \n" // b * b - 4 * a * c
			"fst %0\n"
			: "=g" (discriminant)
			: "g"  (a), "g"(b), "g"(c), "g"(FantasticFour));

		if (discriminant > 0)
		{
			const double FunTwo = 2;
			__asm (
				"fld %5\n" // sqrt(D)
				"fsqrt\n"
				"fchs\n"

				"fld %3\n" // (-b - sqrt(D)) / (2 * a)
				"fchs\n"
				"faddp st(1), st(0)\n"
				"fdiv %2\n"
				"fdiv %6\n"
				"fst %0\n"

				"fld %5\n" // sqrt(D)
				"fsqrt\n"

				"fld %3\n" // (-b + sqrt(D)) / (2 * a)
				"fchs\n"
				"faddp st(1), st(0)\n"
				"fdiv %2\n"
				"fdiv %6\n"
				"fst %1\n"
				: "=g" (x1), "=g" (x2)
				: "g" (a), "g" (b), "g" (c), "g" (discriminant) ,"g" (FunTwo));

			printw(" x1 = %lf\n x2 = %lf \n", x1, x2);
		}
		else if (discriminant == 0)
		{
			__asm (
				"fld %1\n" // (-b) / (2 * a)
				"fchs\n"
				"fdiv %2\n"
				"fdiv %3\n"
				"fst %0\n"
				: "=g" (x1)
				: "g" (b), "g" (a), "g"(FantasticTwo));
			printw("x = %lf \n", x1);
		}
		else
		{
			printw("Sorry, bad discriminant.\n");
		}

		printw("Press Enter to continue or Q to exit program.");
		refresh();
		while (inChar != 'q' && inChar != '\n')
		{
			inChar = getch();
		}
	}

	endwin();
	return 0;
}
