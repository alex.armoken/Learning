#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdint.h>

#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <glib-2.0/glib.h>
#include <libnotify/notify.h>
#include <glib-2.0/glib/gmacros.h>

#define MAX_USER 32
#define MAX_NAME 2048
#define MAX_READ 2048
#define MAX_STRING 4096
#define MAX_FILE_NAME 256


struct MainWindowObjects
{
    GtkWindow *topWindow;

	GtkLabel *aLabel;
	GtkLabel *bLabel;

	GtkLabel *fi0;
	GtkLabel *fi1;
	GtkLabel *fi2;
	GtkLabel *fi3;
	GtkLabel *fi4;
	GtkLabel *fi5;
	GtkLabel *fi6;
	GtkLabel *fi7;

	GtkEntry *ai0;
	GtkEntry *ai1;
	GtkEntry *ai2;
	GtkEntry *ai3;
	GtkEntry *ai4;
	GtkEntry *ai5;
	GtkEntry *ai6;
	GtkEntry *ai7;

	GtkEntry *bi0;
	GtkEntry *bi1;
	GtkEntry *bi2;
	GtkEntry *bi3;
	GtkEntry *bi4;
	GtkEntry *bi5;
	GtkEntry *bi6;
	GtkEntry *bi7;

	GtkEntry *ci0;
	GtkEntry *ci1;
	GtkEntry *ci2;
	GtkEntry *ci3;
	GtkEntry *ci4;
	GtkEntry *ci5;
	GtkEntry *ci6;
	GtkEntry *ci7;

	GtkEntry *di0;
	GtkEntry *di1;
	GtkEntry *di2;
	GtkEntry *di3;
	GtkEntry *di4;
	GtkEntry *di5;
	GtkEntry *di6;
	GtkEntry *di7;


} mainWindowObjects;

#ifndef MMXALGO
void  calcEquation(int8_t A[], int8_t B[], int8_t C[],
				   int16_t D[], int16_t Result[]);
int tryTransformeString(const char string[]);
int checkStringByte(char string[]);
int checkStringWord(char string[]);
#endif
