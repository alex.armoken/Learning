#include "ArrMultiplier.h"

#define MMXALGO 1

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

const int IgnoreZero = 1;
const int NotIgnoreZero = 0;
const int PositiveInput = 1;
const int NotPositiveInput = 0;
const int DefArrSize = 10;
const int KeyBackspace = 8;
const double FantasticFour = 4;
const double FantasticTwo = 2;
const double FantasticOne = 1;
const double MaxNum = 24.365750;
const unsigned int MaxLen = 12;


int tryTransformeString(const char string[])
{
	int number = 0;
	int isNeg = 0;
	int startI = 0;
	int len = strlen(string);

	if (string[0] == *"-")
	{
		isNeg = 1;
		startI = 1;
	}

	for(int i = startI; i < len; i++){
		number = number * 10 + (string[i] - '0' );
	}

	if (isNeg == 1)
	{
		number = -number;
	}

	return number;
}


int isStringInt(char string[])
{
	int isNeg = 0;
	if ((string[0] == *"0" && string[1] == *"0") ||
		(string[0] == *"-" && string[1] == *"0"))
	{
		return 0;
	}
	for (unsigned int i = 0; i < strlen(string); i++)
	{
		if (string[i] == *"-")
		{
			if (isNeg == 0 && i == 0)
			{
				isNeg = 1;
			}
			else
			{
				return 0;
			}
		}
		else if  (string[i] > '9' || string[i] < '0')

			return 0;
	}
	return 1;
}


int checkStringByte(char string[])
{
	if (isStringInt(string) != 0)
	{
		int transformedValue = atoi(string);
		if (transformedValue > 127 || transformedValue < -127)
		{
			return 0;
		}
		return 1;
	}
	else
	{
		return 0;
	}
}


int checkStringWord(char string[])
{
	if (isStringInt(string) != 0)
	{
		int transformedValue = atoi(string);
		if (transformedValue > 16384 || transformedValue < -16256)
		{
			return 0;
		}
		return 1;
	}
	else
	{
		return 0;
	}
}


void arrDif(int8_t A[], int16_t B[], int16_t res[])
{
	__asm (
		"mov rdx, %0\n"
		"mov rax, %1\n"
		"mov rbx, %2\n"

		"movq mm0, [rax]\n"
		"pcmpgtb mm7, mm0\n"
		"punpcklbw mm0, mm7\n"

		"movq mm1, [rbx]\n"
		"psubw mm0, mm1\n"
		"movq [rdx], mm0\n"


		"movq mm0, [rax]\n"
		"pcmpgtb mm7, mm0\n"
		"punpckhbw mm0, mm7\n"

		"movq mm1, [rbx + 8]\n"
		"psubw mm0, mm1\n"
		"movq [rdx + 8], mm0\n"
		: "=m" (res)		: "g"  (A), "g" (B));
}


void arrSum(int16_t *A, int16_t *B, int16_t *Result)
{
	__asm (
		"mov rax, %1\n"
		"mov rbx, %2\n"

		"mov rdx, %0\n"

		"movq mm0, [rax]\n"
		"movq mm1, [rbx]\n"
		"paddw mm0, mm1\n"
		"movq [rdx], mm0\n"

		"movq mm2, [rax + 8]\n"
		"movq mm3, [rbx + 8]\n"
		"paddw mm2, mm3\n"
		"movq [rdx + 8], mm2\n"
		: "=m" (Result)
		: "g"  (A), "g" (B));
}


void arrMul (int8_t *A, int8_t *B, int16_t *Result)
{
	__asm__ (
		"mov rax, %1\n" // Addres of first argument
		"mov rbx, %2\n" // Addres of second argument
		"mov rdx, %0\n" // Addres of result
		"por mm7, mm7\n"

		"movq mm0, qword ptr [rax]\n"
		"movq mm1, qword ptr [rbx]\n"
		"pcmpgtb mm7, mm0\n" // Compare with zero mm0 (mm7 is empty)
		"punpcklbw mm0, mm7\n" // Unpack low bytes of mm0 in mm2
		"movq mm2, mm0\n"
		"pcmpgtb mm7, mm1\n" // Compare with zero mm0 (mm7 is empty)
		"punpcklbw mm1, mm7\n" // Unpack low bytes of mm1 in mm3
		"movq mm3, mm1\n"

		/* Hight parts of multiplication of mm0 and mm1, result in mm0 */
		"pmulhw mm0, mm1\n"
		/* Low parts of multiplication of mm2 and mm3, result in mm2 */
		"pmullw mm2, mm3\n"

		"movq mm3, mm2\n"
		"movq mm1, mm2\n" // Copy hight parts of multiplication to mm2
		"movq mm2, mm0\n"

		"punpckhwd mm3, mm2\n" // Join register mm3 and mm2 to mm3
		"punpcklwd mm1, mm0\n" // Join register mm1 and mm0 to mm1
		"packssdw mm1, mm3\n" // Pack double words to words
		"movq mm5, mm1\n"
		"movq [rdx], mm5\n"

		"por mm7, mm7\n" // Clean all previous results
		"por mm2, mm2\n"
		"por mm3, mm3\n"

		"movq mm0, qword ptr [rax]\n"
		"movq mm1, qword ptr [rbx]\n"
		"pcmpgtb mm7, mm0\n" // Compare with zero mm0 (mm7 is empty)
		"punpckhbw mm0, mm7\n" // Unpack low bytes of mm0 in mm2
		"movq mm2, mm0\n"
		"pcmpgtb mm7, mm1\n" // Compare with zero mm0 (mm7 is empty)
		"punpckhbw mm1, mm7\n" // Unpack low bytes of mm1 in mm3
		"movq mm3, mm1\n"

		/* Hight parts of multiplication of mm0 and mm1, result in mm0 */
		"pmulhw mm0, mm1\n"
		/* Low parts of multiplication of mm2 and mm3, result in mm2 */
		"pmullw mm2, mm3\n"

		"movq mm3, mm2\n"
		"movq mm1, mm2\n" // Copy hight parts of multiplication to mm2
		"movq mm2, mm0\n"

		"punpckhwd mm3, mm2\n" // Join register mm3 and mm2 to mm3
		"punpcklwd mm1, mm0\n" // Join register mm1 and mm0 to mm1
		"packssdw mm1, mm3\n" // Pack double words to words
		"movq mm5, mm1\n"
		"movq [rdx + 8], mm5\n"
		: "=m" (Result)
		: "g"  (A), "g" (B));
}


void  calcEquation(int8_t A[], int8_t B[], int8_t C[],
				   int16_t D[], int16_t Result[])
{
	arrMul(B, C, Result);
	arrDif(A, Result, Result);
	arrSum(D, Result, Result);
}
