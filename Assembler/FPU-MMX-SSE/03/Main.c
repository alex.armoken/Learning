#include "ArrMultiplier.h"

#define UI_FILE "ui.glade"


const unsigned int arrSize = 8;

GtkEntry *AEntries[8];
GtkEntry *BEntries[8];
GtkEntry *CEntries[8];
GtkEntry *DEntries[8];
GtkLabel *FLabels[8];


void onTopWindowShow(GtkEntry *entry, gpointer user_data)
{
	gtk_entry_set_text(mainWindowObjects.ai0, "10");
	gtk_entry_set_text(mainWindowObjects.ai1, "2");
	gtk_entry_set_text(mainWindowObjects.ai2, "3");
	gtk_entry_set_text(mainWindowObjects.ai3, "6");
	gtk_entry_set_text(mainWindowObjects.ai4, "7");
	gtk_entry_set_text(mainWindowObjects.ai5, "12");
	gtk_entry_set_text(mainWindowObjects.ai6, "25");
	gtk_entry_set_text(mainWindowObjects.ai7, "100");

	gtk_entry_set_text(mainWindowObjects.bi0, "7");
	gtk_entry_set_text(mainWindowObjects.bi1, "5");
	gtk_entry_set_text(mainWindowObjects.bi2, "1");
	gtk_entry_set_text(mainWindowObjects.bi3, "0");
	gtk_entry_set_text(mainWindowObjects.bi4, "5");
	gtk_entry_set_text(mainWindowObjects.bi5, "8");
	gtk_entry_set_text(mainWindowObjects.bi6, "9");
	gtk_entry_set_text(mainWindowObjects.bi7, "30");

	gtk_entry_set_text(mainWindowObjects.ci0, "5");
	gtk_entry_set_text(mainWindowObjects.ci1, "1");
	gtk_entry_set_text(mainWindowObjects.ci2, "3");
	gtk_entry_set_text(mainWindowObjects.ci3, "5");
	gtk_entry_set_text(mainWindowObjects.ci4, "9");
	gtk_entry_set_text(mainWindowObjects.ci5, "1");
	gtk_entry_set_text(mainWindowObjects.ci6, "5");
	gtk_entry_set_text(mainWindowObjects.ci7, "1");

	gtk_entry_set_text(mainWindowObjects.di0, "4");
	gtk_entry_set_text(mainWindowObjects.di1, "2");
	gtk_entry_set_text(mainWindowObjects.di2, "1");
	gtk_entry_set_text(mainWindowObjects.di3, "5");
	gtk_entry_set_text(mainWindowObjects.di4, "6");
	gtk_entry_set_text(mainWindowObjects.di5, "7");
	gtk_entry_set_text(mainWindowObjects.di6, "9");
	gtk_entry_set_text(mainWindowObjects.di7, "1");
}


void calcAll()
{
	int8_t A[arrSize];
	int8_t B[arrSize];
	int8_t C[arrSize];
	int16_t D[arrSize];
	int16_t F[arrSize];

	for (unsigned int i = 0; i < arrSize; i++)
	{
		A[i] = tryTransformeString(gtk_entry_get_text(AEntries[i]));
		B[i] = tryTransformeString(gtk_entry_get_text(BEntries[i]));
		C[i] = tryTransformeString(gtk_entry_get_text(CEntries[i]));
		D[i] = tryTransformeString(gtk_entry_get_text(DEntries[i]));
		F[i] = 0;
	}

	calcEquation(A, B, C, D, F);
	for (unsigned int i = 0; i < 8; i++) {
		char buffer[6];
		snprintf(buffer, 7, "%d", F[i]);
		gtk_label_set_text(FLabels[i], buffer);
	}
}


void entryABCChanged(GtkEntry *entry, gpointer user_data)
{
	const char *entryTextConst;
	entryTextConst = gtk_entry_get_text(entry);

	char *newEntryText = malloc(strlen(entryTextConst)+1);;
	strcpy(newEntryText, entryTextConst);

	if (checkStringByte(newEntryText) == 1)
	{
		gtk_entry_set_text(GTK_ENTRY(entry), newEntryText);
		calcAll();
	}
	else
	{
		notify_init ("Number warning");
		NotifyNotification *Hello = notify_notification_new ("Input warning",
															 "This number must be integer from -127 to 127 and doesn't contain chars",
															 "dialog-warning");
		notify_notification_show (Hello, NULL);
		g_object_unref(G_OBJECT(Hello));
		notify_uninit();

		newEntryText[strlen(newEntryText) - 1] = 0;
		gtk_entry_set_text(GTK_ENTRY (entry), newEntryText);
	}
}


void entryDChanged(GtkEntry *entry, gpointer user_data)
{
	const char *entryTextConst;
	entryTextConst = gtk_entry_get_text(entry);

	char *newEntryText = malloc(strlen(entryTextConst)+1);;
	strcpy(newEntryText, entryTextConst);

	if (checkStringWord(newEntryText) == 1)
	{
		gtk_entry_set_text(GTK_ENTRY(entry), newEntryText);
		calcAll();
	}
	else
	{
		notify_init ("Number warning");
		NotifyNotification *Hello = notify_notification_new ("Input warning",
															 "This number must be integer from -16256 to 16384 and doesn't contain chars",
															 "dialog-warning");
		notify_notification_show (Hello, NULL);
		g_object_unref(G_OBJECT(Hello));
		notify_uninit();

		newEntryText[strlen(newEntryText) - 1] = 0;
		gtk_entry_set_text(GTK_ENTRY (entry), newEntryText);
	}
}


void onCalcButtonClick(GtkEntry *entry, gpointer user_data)
{
	calcAll();
}


void onClearButtonClick(GtkEntry *entry, gpointer user_data)
{
	for (int i = 0; i < 8; i++) {
		gtk_entry_set_text(AEntries[i], "0");
	}
	for (int i = 0; i < 8; i++) {
		gtk_entry_set_text(BEntries[i], "0");
	}
	for (int i = 0; i < 8; i++) {
		gtk_entry_set_text(CEntries[i], "0");
	}
	for (int i = 0; i < 8; i++) {
		gtk_entry_set_text(DEntries[i], "0");
	}
	for (int i = 0; i < 8; i++) {
		gtk_label_set_text(FLabels[i], "0");
	}
}


void onRandomButtonClick(GtkEntry *entry, gpointer user_data)
{
	char randNumStr[5] = "";

	for (int i = 0; i < 8; i++) {
		sprintf(randNumStr, "%d", rand() % 15);
		gtk_entry_set_text(AEntries[i], randNumStr);
	}
	for (int i = 0; i < 8; i++) {
		sprintf(randNumStr, "%d", rand() % 15);
		gtk_entry_set_text(BEntries[i], randNumStr);
	}
	for (int i = 0; i < 8; i++) {
		sprintf(randNumStr, "%d", rand() % 15);
		gtk_entry_set_text(CEntries[i], randNumStr);
	}
	for (int i = 0; i < 8; i++) {
		sprintf(randNumStr, "%d", rand() % 15);
		gtk_entry_set_text(DEntries[i], randNumStr);
	}
}


void onTopWindowDestroy(GtkWidget *window, gpointer data)
{
	gtk_main_quit();
}

void initWindowElements(GtkBuilder *builder)
{
	mainWindowObjects.fi0 = GTK_LABEL(
		gtk_builder_get_object(builder, "fi0"));
	mainWindowObjects.fi1 = GTK_LABEL(
		gtk_builder_get_object(builder, "fi1"));
	mainWindowObjects.fi2 = GTK_LABEL(
		gtk_builder_get_object(builder, "fi2"));
	mainWindowObjects.fi3 = GTK_LABEL(
		gtk_builder_get_object(builder, "fi3"));
	mainWindowObjects.fi4 = GTK_LABEL(
		gtk_builder_get_object(builder, "fi4"));
	mainWindowObjects.fi5 = GTK_LABEL(
		gtk_builder_get_object(builder, "fi5"));
	mainWindowObjects.fi6 = GTK_LABEL(
		gtk_builder_get_object(builder, "fi6"));
	mainWindowObjects.fi7 = GTK_LABEL(
		gtk_builder_get_object(builder, "fi7"));

	FLabels[0] = mainWindowObjects.fi0;
	FLabels[1] = mainWindowObjects.fi1;
	FLabels[2] = mainWindowObjects.fi2;
	FLabels[3] = mainWindowObjects.fi3;
	FLabels[4] = mainWindowObjects.fi4;
	FLabels[5] = mainWindowObjects.fi5;
	FLabels[6] = mainWindowObjects.fi6;
	FLabels[7] = mainWindowObjects.fi7;

	mainWindowObjects.ai0 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ai0"));
	mainWindowObjects.ai1 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ai1"));
	mainWindowObjects.ai2 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ai2"));
	mainWindowObjects.ai3 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ai3"));
	mainWindowObjects.ai4 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ai4"));
	mainWindowObjects.ai5 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ai5"));
	mainWindowObjects.ai6 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ai6"));
	mainWindowObjects.ai7 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ai7"));

	AEntries[0] = mainWindowObjects.ai0;
	AEntries[1] = mainWindowObjects.ai1;
	AEntries[2] = mainWindowObjects.ai2;
	AEntries[3] = mainWindowObjects.ai3;
	AEntries[4] = mainWindowObjects.ai4;
	AEntries[5] = mainWindowObjects.ai5;
	AEntries[6] = mainWindowObjects.ai6;
	AEntries[7] = mainWindowObjects.ai7;


	mainWindowObjects.bi0 = GTK_ENTRY(
		gtk_builder_get_object(builder, "bi0"));
	mainWindowObjects.bi1 = GTK_ENTRY(
		gtk_builder_get_object(builder, "bi1"));
	mainWindowObjects.bi2 = GTK_ENTRY(
		gtk_builder_get_object(builder, "bi2"));
	mainWindowObjects.bi3 = GTK_ENTRY(
		gtk_builder_get_object(builder, "bi3"));
	mainWindowObjects.bi4 = GTK_ENTRY(
		gtk_builder_get_object(builder, "bi4"));
	mainWindowObjects.bi5 = GTK_ENTRY(
		gtk_builder_get_object(builder, "bi5"));
	mainWindowObjects.bi6 = GTK_ENTRY(
		gtk_builder_get_object(builder, "bi6"));
	mainWindowObjects.bi7 = GTK_ENTRY(
		gtk_builder_get_object(builder, "bi7"));

	BEntries[0] = mainWindowObjects.bi0;
	BEntries[1] = mainWindowObjects.bi1;
	BEntries[2] = mainWindowObjects.bi2;
	BEntries[3] = mainWindowObjects.bi3;
	BEntries[4] = mainWindowObjects.bi4;
	BEntries[5] = mainWindowObjects.bi5;
	BEntries[6] = mainWindowObjects.bi6;
	BEntries[7] = mainWindowObjects.bi7;

	mainWindowObjects.ci0 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ci0"));
	mainWindowObjects.ci1 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ci1"));
	mainWindowObjects.ci2 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ci2"));
	mainWindowObjects.ci3 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ci3"));
	mainWindowObjects.ci4 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ci4"));
	mainWindowObjects.ci5 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ci5"));
	mainWindowObjects.ci6 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ci6"));
	mainWindowObjects.ci7 = GTK_ENTRY(
		gtk_builder_get_object(builder, "ci7"));

	CEntries[0] = mainWindowObjects.ci0;
	CEntries[1] = mainWindowObjects.ci1;
	CEntries[2] = mainWindowObjects.ci2;
	CEntries[3] = mainWindowObjects.ci3;
	CEntries[4] = mainWindowObjects.ci4;
	CEntries[5] = mainWindowObjects.ci5;
	CEntries[6] = mainWindowObjects.ci6;
	CEntries[7] = mainWindowObjects.ci7;

	mainWindowObjects.di0 = GTK_ENTRY(
		gtk_builder_get_object(builder, "di0"));
	mainWindowObjects.di1 = GTK_ENTRY(
		gtk_builder_get_object(builder, "di1"));
	mainWindowObjects.di2 = GTK_ENTRY(
		gtk_builder_get_object(builder, "di2"));
	mainWindowObjects.di3 = GTK_ENTRY(
		gtk_builder_get_object(builder, "di3"));
	mainWindowObjects.di4 = GTK_ENTRY(
		gtk_builder_get_object(builder, "di4"));
	mainWindowObjects.di5 = GTK_ENTRY(
		gtk_builder_get_object(builder, "di5"));
	mainWindowObjects.di6 = GTK_ENTRY(
		gtk_builder_get_object(builder, "di6"));
	mainWindowObjects.di7 = GTK_ENTRY(
		gtk_builder_get_object(builder, "di7"));

	DEntries[0] = mainWindowObjects.di0;
	DEntries[1] = mainWindowObjects.di1;
	DEntries[2] = mainWindowObjects.di2;
	DEntries[3] = mainWindowObjects.di3;
	DEntries[4] = mainWindowObjects.di4;
	DEntries[5] = mainWindowObjects.di5;
	DEntries[6] = mainWindowObjects.di6;
	DEntries[7] = mainWindowObjects.di7;
}

int main(int argc, char** argv)
{
	GtkBuilder *builder;
	GError *error = NULL;

	gtk_init(&argc, &argv);

	builder = gtk_builder_new();

	if(!gtk_builder_add_from_file(builder, UI_FILE, &error))
	{
		g_warning("%s\n", error->message);
		g_free(error);
		return(1);
	}

	initWindowElements(builder);
	gtk_builder_connect_signals(builder, &mainWindowObjects);

	GtkWidget *window = GTK_WIDGET (
		gtk_builder_get_object (builder, "topWindow"));

	if (!window)
	{
		g_critical ("Error of gettin windows widget");
	}

	g_object_unref(G_OBJECT(builder));
	gtk_widget_show (window);
	gtk_main ();
	return 0;
}
