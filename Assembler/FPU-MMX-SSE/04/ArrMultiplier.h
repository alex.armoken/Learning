#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <stdint.h>
#include <immintrin.h>

#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <glib-2.0/glib.h>
#include <libnotify/notify.h>
#include <glib-2.0/glib/gmacros.h>

struct MainWindowObjects
{
    GtkWindow *topWindow;

	GtkLabel *aLabel;
	GtkLabel *bLabel;

	GtkLabel *fi0;
	GtkLabel *fi1;
	GtkLabel *fi2;
	GtkLabel *fi3;
	GtkLabel *fi4;
	GtkLabel *fi5;
	GtkLabel *fi6;
	GtkLabel *fi7;

	GtkEntry *ai0;
	GtkEntry *ai1;
	GtkEntry *ai2;
	GtkEntry *ai3;
	GtkEntry *ai4;
	GtkEntry *ai5;
	GtkEntry *ai6;
	GtkEntry *ai7;

	GtkEntry *bi0;
	GtkEntry *bi1;
	GtkEntry *bi2;
	GtkEntry *bi3;
	GtkEntry *bi4;
	GtkEntry *bi5;
	GtkEntry *bi6;
	GtkEntry *bi7;

	GtkEntry *ci0;
	GtkEntry *ci1;
	GtkEntry *ci2;
	GtkEntry *ci3;
	GtkEntry *ci4;
	GtkEntry *ci5;
	GtkEntry *ci6;
	GtkEntry *ci7;

	GtkEntry *di0;
	GtkEntry *di1;
	GtkEntry *di2;
	GtkEntry *di3;
	GtkEntry *di4;
	GtkEntry *di5;
	GtkEntry *di6;
	GtkEntry *di7;


} mainWindowObjects;

#ifndef SSEALGO
void  calcEquation(float A[], float B[], float C[],
				   double D[], double Result[]);
double tryTransformeString(const char string[]);
int checkStringShortFloat(char string[]);
int checkStringLongFloat(char string[]);
#endif
