#include "ArrMultiplier.h"

#define SSEALGO 1

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif


double tryTransformeString(const char string[])
{
	double integerPart = 0;
	double fractionPart = 0;
	unsigned int isNeg = 0;
	unsigned int startI = 0;
	unsigned int len = strlen(string);

	if (string[0] == *"-")
	{
		isNeg = 1;
		startI = 1;
	}

	unsigned int i = startI;
	while (i < len)
	{
		if (string[i] == *".")
		{
			i++;
			break;
		}
		else
		{
			integerPart = integerPart * 10 + (string[i] - '0' );
			i++;
		}
	}

	while (i < len)
	{
		fractionPart = fractionPart * 10 + (string[i] - '0' );
		i++;
	}

	while (fractionPart >= 1)
	{
		fractionPart /= 10;
	}

	double result = integerPart + fractionPart;

	if (isNeg == 1)
	{
		result = -result;
	}

	return result;
}


int isStringFloat(char string[])
{
	int isNeg = 0;
	int isFloat = 0;
	for (unsigned int i = 0; i < strlen(string); i++)
	{
		if (string[i] == *"-")
		{
			if (isNeg == 0 && i == 0)
			{
				isNeg = 1;
			}
			else
			{
				return 0;
				break;
			}
		}
		else if (string[i] == *".")
		{
			if (isFloat == 0 &&
				((i >= 2 && isNeg == 1) || (i >= 1 && isNeg == 0)))
			{
				isFloat = 1;
			}
			else
			{
				return 0;
				break;
			}
		}
		else if  (string[i] > '9' || string[i] < '0')
		{
			return 0;
			break;
		}
	}
	return 1;
}


int checkStringShortFloat(char string[])
{
	if (isStringFloat(string) != 0)
	{
		int transformedValue = atof(string);
		if (transformedValue > 3.402823e+38 ||
			transformedValue < -3.402823e+38)
		{
			return 0;
		}
		return 1;
	}
	else
	{
		return 0;
	}
}


int checkStringLongFloat(char string[])
{
	if (isStringFloat(string) != 0)
	{
		int transformedValue = atof(string);
		if (transformedValue > 3.402823e+38 ||
			transformedValue < -3.402823e+38)
		{
			return 0;
		}
		return 1;
	}
	else
	{
		return 0;
	}
}


void arrMul (float *A, float *B, double *Result)
{
	for (unsigned int i = 0; i < 2; i++)
	{
		unsigned long int offset = i * 16;
		__asm__ (
			"mov rax, %1 \n" // Load A addres
			"mov rbx, %2 \n" // Load B addres
			"mov rcx, %0 \n" // Load result addres
			"mov rdx, %3 \n" // Load offset

			"add rax, rdx \n" // Do offset
			"add rbx, rdx \n"
			"add rcx, rdx \n" // Need do 2 offset, float - 32, double - 64
			"add rcx, rdx \n"

			"movups xmm0, xmmword ptr [rax] \n"
			"cvtps2pd xmm0, xmm0 \n" // Lower float to double
			"movups xmm1, xmmword ptr [rbx] \n"
			"cvtps2pd xmm1, xmm1 \n"
			"mulpd xmm0, xmm1 \n"
			"movups xmmword ptr [rcx], xmm0 \n"


			"movups xmm0, xmmword ptr [rax] \n"
			"shufps xmm0, xmm0, 78 \n" // 01001110 - 78
			"cvtps2pd xmm0, xmm0 \n"

			"movups xmm1, xmmword ptr [rbx] \n"
			"shufps xmm1, xmm1, 78 \n"
			"cvtps2pd xmm1, xmm1 \n"

			"mulpd xmm0, xmm1 \n"
			"movups xmmword ptr [rcx + 16], xmm0 \n"

			: "=m" (Result)
			: "g"  (A), "g" (B), "g" (offset));
	}
}


void arrSum (double *A, double *B, double *Result)
{
	for (unsigned int i = 0; i < 4; i++)
	{
		unsigned long int offset = i * 16;
		__asm__ (
			"mov rax, %1 \n"
			"mov rbx, %2 \n"
			"mov rcx, %0 \n"
			"mov rdx, %3 \n"

			"add rax, rdx \n"
			"add rbx, rdx \n"
			"add rcx, rdx \n"

			"movups xmm0, xmmword ptr [rax] \n"
			"movups xmm1, xmmword ptr [rbx] \n"
			"addpd xmm0, xmm1 \n"
			"movups xmmword ptr [rcx], xmm0 \n"

			: "=m" (Result)
			: "g"  (A), "g" (B), "g" (offset));
	}
}


void arrDif (float *A, double *B, double *Result)
{
	for (unsigned int i = 0; i < 2; i++)
	{
		unsigned long int offset = i * 16;
		__asm__ (
			"mov rax, %1 \n"
			"mov rbx, %2 \n"
			"mov rcx, %0 \n"
			"mov rdx, %3 \n"

			"add rax, rdx \n"
			"add rbx, rdx \n"
			"add rbx, rdx \n"
			"add rcx, rdx \n"
			"add rcx, rdx \n"

			"movups xmm0, xmmword ptr [rax] \n"
			"cvtps2pd xmm0, xmm0 \n"
			"movups xmm1, xmmword ptr [rbx] \n"
			"subpd xmm0, xmm1 \n"
			"movups xmmword ptr [rcx], xmm0 \n"

			"movups xmm0, xmmword ptr [rax] \n"
			"shufps xmm0, xmm0, 78 \n" // 01001110 - 78
			"cvtps2pd xmm0, xmm0 \n"
			"movups xmm1, xmmword ptr [rbx + 16] \n"
			"subpd xmm0, xmm1 \n"
			"movups xmmword ptr [rcx + 16], xmm0 \n"

			: "=m" (Result)
			: "g"  (A), "g" (B), "g" (offset));
	}
}


void  calcEquation(float A[], float B[], float C[],
				   double D[], double Result[])
{
	arrMul(B, C, Result);
	arrDif(A, Result, Result);
	arrSum(D, Result, Result);
}
