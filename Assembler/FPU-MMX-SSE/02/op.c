#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ncurses.h>
#include <math.h>
#include <unistd.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

const int IgnoreZero = 1;
const int NotIgnoreZero = 0;
const int PositiveInput = 1;
const int NotPositiveInput = 0;
const int DefArrSize = 10;
const int KeyBackspace = 8;
const double FantasticFour = 4;
const double FantasticTwo = 2;
const double FantasticOne = 1;
const double MaxNum = 24.365750;
/* const double MaxNum = 100; */
const unsigned int MaxLen = 12;


void resizeStr (char** inStr, unsigned int* arraySize)
{
    char* newArr = (char*)calloc(*arraySize * 2, sizeof(char));
    strcpy(newArr, *inStr);
    free(*inStr);
    *inStr = newArr;
    *arraySize = *arraySize *2;
}


double numInput(int isPos, int ignoreZero, double minNumber,
				double maxNumber,unsigned int maxLen)
{
    char inChar;
    int isNeg = 0;
    int isFrac = 0;
    double result = 0;
    unsigned int arrSize = DefArrSize;
    char* allNumChars = (char*)calloc(DefArrSize, sizeof(char));

    refresh();

    while (1)
	{
		inChar = getch();
		if (inChar == '\n')
		{
			sscanf(allNumChars, "%lf", &result);
			if (strlen(allNumChars) != 0 &&
				allNumChars[strlen(allNumChars) - 1] != '.' &&
				!(isNeg && result == 0) &&
				!(ignoreZero && result == 0))
			{
				break;
			}
			result = 0;
		}
		else if ((int)inChar == KeyBackspace)
		{
			if (strlen(allNumChars) != 0 || isNeg)
			{
				printw("\b \b");
			}

			if (isFrac && allNumChars[strlen(allNumChars) - 1] == '.')
			{
				isFrac = 0;
				allNumChars[strlen(allNumChars) - 1] = 0;
			}
			else if (isNeg && strlen(allNumChars) == 0)
			{
				isNeg = 0;
			}
			else
			{
				allNumChars[strlen(allNumChars) - 1] = 0;

			}
		}
		else
		{
			if (inChar == '.')
			{
				if (!isFrac && strlen(allNumChars) != 0)
				{
					isFrac = 1;
					allNumChars[strlen(allNumChars)] = inChar;
					addch(inChar);
				}
			}
			else if (inChar == '-')
			{
				if (!isNeg && strlen(allNumChars) == 0 && isPos == 0)
				{
					isNeg = 1;
					addch(inChar);
				}
			}
			else if (inChar <= '9' && inChar >= '0')
			{
				if (strlen(allNumChars) == arrSize - 1)
				{
					resizeStr(&allNumChars, &arrSize);
				}
				allNumChars[strlen(allNumChars)] = inChar;

				sscanf(allNumChars, "%lf", &result);
				if (result <= maxNumber &&
					result >= minNumber &&
					strlen(allNumChars) + 1 < maxLen)
				{
					addch(inChar);
				}
				else
				{
					allNumChars[strlen(allNumChars) - 1] = 0;
				}
			}
		}
		inChar = 0;
		refresh();
	}
    addch('\n');

    if (isNeg)
	{
		result = -result;
	}
    return result;
}


double YFunc(double x)
{
	double exactValue;
	__asm (
		".intel_syntax noprefix \n" // Calculate Y func
		"finit\n"

		"fldpi\n" // x * cos(pi / 4)
		"fdiv %2\n"
		"fcos\n"
		"fmul %1\n"

		"fldl2e\n" // exp()
		"fmulp\n"
		"fld st(0)\n"
		"frndint\n" // Round
		"fsub st(1), st(0)\n"
		"fxch st(1)\n"
		"f2xm1\n" // 2 ^ x - 1
		"fld1\n"
		"faddp\n"
		"fscale\n"

		"fldpi\n" // cos(x * sin(pi / 4))
		"fdiv %2\n"
		"fsin\n"
		"fmul %1\n"
		"fcos\n"

		"fmulp st(1), st(0)\n"
		"fstp %0\n"
		: "=g" (exactValue)
		: "g"  (x), "g" (FantasticFour));
	return exactValue;
}


double CYFunc(double x)
{
	return log((1 + x) / (1 - x)) / 4 + atan(x) / 2;
	/* return exp(x * cos(M_PI / 4)) * cos(x * sin(M_PI / 4)); */
}


double CFactorial(double x)
{
    if ( x == 0 )
        return 1;

    return(x * CFactorial(x - 1));
}


double CSFunc(double x, double k)
{
	return pow(x, 4 * k + 1) / (4 * k + 1);
	/* return cos(M_PI * k / 4) * pow(x, k) / CFactorial(k); */
}


double SFunc(double x, unsigned int k)
{
	double approxSummand = 0;
	__asm (
		".intel_syntax noprefix \n" // Calculate S func
		"finit\n"

		"fldpi\n" // cos (k * pi / 4)
		"fimul %2\n"
		"fdiv %3\n"
		"fcos\n"

		"wait\n" // x ^ k
		"mov ecx, %2\n"
		"cmp ecx, 0\n"
		"jz Power_Zero\n"
		"wait\n"

		"fld %1\n"
		"Power:\n"
		"cmp ecx, 1\n"
		"jz End_Calc_Numerator\n"
		"wait\n"
		"fmul %1\n"
		"wait\n"
		"dec ecx\n"
		"jmp Power\n"

		"Power_Zero:\n"
		"wait\n"
		"fld1\n"
		"End_Calc_Numerator:\n"

		"wait\n"
		"fmulp st(1), st(0)\n" // cos (k * pi / 4) * x ^ k

		"wait\n" // Factorial
		"cmp %2, 1\n"
		"ja Factorial_More_One\n"
		"fld1\n"
		"jmp Factorial_Equal_One\n"

		"Factorial_More_One:\n"
		"wait\n"
		"fild %2\n"
		"fild %2\n"
		"fsub %4\n"

		"Factorial:\n"
		"wait\n"
		"fmul st(1), st(0)\n"
		"fsub %4\n"

		"ftst\n"
		"fstsw ax\n"
		"sahf\n"
		"jnz Factorial\n"

		"wait\n"
		"fincstp\n"
		"Factorial_Equal_One:\n"
		"wait\n"

		"fdivp st(1), st(0)\n"

		"Final:\n"
		"wait\n"
		"fstp %0\n"
		: "=g" (approxSummand)
		: "g"  (x), "g" (k), "g" (FantasticFour), "g" (FantasticOne));

	return approxSummand;
}


int main()
{
    initscr();
    scrollok(stdscr, 1);
    noecho();
    raw();
    refresh();

    char inChar = 0;
    while (inChar != 'q' || inChar == '\n')
	{
		double a = 0;
		double b = 0;
		double h = 0;
		double e = 0;
		inChar = 0;

		clear();
		printw("Please, input a: ");
		a = numInput(NotPositiveInput , NotIgnoreZero,
					 -MaxNum, MaxNum, MaxLen);

		printw("Please, input b: ");
		b = numInput(NotPositiveInput, NotIgnoreZero,
					 -MaxNum, MaxNum, MaxLen);

		printw("Please, input h: ");
		h = numInput(NotPositiveInput, NotIgnoreZero,
					 -MaxNum, MaxNum, MaxLen);
		int isGoodStep = 1;
		if (a < 0 && b > 0 && h < 0) {
			isGoodStep = 0;
		}
		else if (a > 0 && b < 0 && h > 0) {
			isGoodStep = 0;
		}
		else if (a > b && h > 0) {
			isGoodStep = 0;
		}
		else if (a < b && h < 0) {
			isGoodStep = 0;
		}

		if (isGoodStep == 1) {
			printw("Please, input e: ");
			e = numInput(PositiveInput, NotIgnoreZero,
						 -MaxNum, MaxNum,MaxLen);

			double x = a;
			printw("------------------------------------------------\n");
			printw("|%8s|%15s|%15s|%5s|\n", "x", "Y", "S", "k");
			printw("------------------------------------------------\n");
			while ((x <= b && a < b && h > 0) || (x >= b && a > b && h < 0))
			{
				double S = 0;
				double Y = CYFunc(x);
				unsigned int k = 0;
				do
				{
					S += CSFunc(x, k);
					k++;
				}
				while (e < fabs(Y - S));
				printw("|%7lf|%15lf|%15lf|%5u|\n", x, Y, S, k);
				printw("------------------------------------------------\n");
				x += h;
			}
		}
		else
		{
			printw("Sorry, bad step, repeat please\n");
		}

		printw("Press Enter to continue or Q to exit program.");
		refresh();
		while (inChar != 'q' && inChar != '\n')
		{
			inChar = getch();
		}
	}

	endwin();
	return 0;
}
