;; -*- geiser-scheme-implementation: guile -*-

(define (f n)
    (if (< n 3) n
        (+ (f (- n 3))
           (f (- n 2))
           (f (- n 1)))))

(define (fi n)
    (define (f-iter cur-n fn1 fn2 fn3)
        (if (= cur-n n)  fn1
            (f-iter (+ cur-n 1)
                    (+ fn1 fn2 fn3)
                    fn1
                    fn2)))

    (if (< n 3) n
        (f-iter 2 2 1 0)))

;; f(n - 1) = f(n - 1 - 1) + f(n - 1 - 2) + f(n - 1 - 3) =
;;          = f(n - 2) + f(n - 3) + f(n - 4)
;; f(n - 2) = f(n - 2 - 1) + f(n - 2 - 2) + f(n - 2 - 3) =
;;          = f(n - 3) + f(n - 4) + f(n - 5)
;; f(n - 3) = f(n - 3 - 1) + f(n - 3 - 2) + f(n - 3 - 3) =
;;          = f(n - 4) + f(n - 5) + f(n - 6)
