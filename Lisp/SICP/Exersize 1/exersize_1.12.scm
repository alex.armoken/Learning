;; -*- geiser-scheme-implementation: guile -*-

(define (calc-pascal-triangle-el x y)
    (if (or (= x 1) (= x y)) 1
        (+ (calc-pascal-triangle-el (- x 1) (- y 1))
           (calc-pascal-triangle-el x (- y 1)))))

(calc-pascal-triangle-el 3 5)
