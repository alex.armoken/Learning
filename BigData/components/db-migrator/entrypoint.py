#!/usr/bin/env python3

import os
import sys
from typing import Dict

import alembic.command
import sqlalchemy.exc
import asa_db_models


def get_db_password() -> str:
    try:
        return os.environ.get("ASA_POSTGRES_PASSWORD", "asa")
    except KeyError:
        with open("/run/secrets/db_password", "r") as password_file:
            return password_file.read()


def get_db_url_params() -> Dict[str, str]:
    return {
        "user": os.environ.get("ASA_POSTGRES_USER", "asa"),
        "password": get_db_password(),
        "database": os.environ.get("ASA_POSTGRES_DB", "asa"),
        "host": os.environ.get("ASA_POSTGRES_HOST", "postgres"),
        "port": os.environ.get("ASA_POSTGRES_HOST", "5432")
    }


def construct_db_url() -> str:
    return "postgresql://{user}:{password}@{host}:{port}/{database}".format(
        **get_db_url_params()
    )


def main() -> int:
    db_url = construct_db_url()
    alembic_cfg = asa_db_models.get_alembic_config(db_url)

    while True:
        try:
            alembic.command.upgrade(alembic_cfg, "head")
            break
        except sqlalchemy.exc.OperationalError:
            pass

    return 0


if __name__ == "__main__":
    sys.exit(main())
