import os.path
from typing import List

from setuptools import setup, find_packages


PATH_TO_CUR_DIR = os.path.dirname(__file__)
PATH_TO_README = os.path.join(PATH_TO_CUR_DIR, "README.txt")
PATH_TO_REQUIRMENTS = os.path.join(PATH_TO_CUR_DIR, "requirments.txt")


def load_long_description() -> str:
    return open(PATH_TO_README).read()


def load_requirments() -> List[str]:
    return list(
        filter(
            lambda requirment: len(requirment) != 0,
            open(PATH_TO_REQUIRMENTS).read().split("\n")
        )
    )


setup(
    name="asa_db_migrator",
    version="0.5",
    packages=find_packages(),
    include_package_data=True,
    long_description=load_long_description(),
    install_requires=load_requirments()
)
