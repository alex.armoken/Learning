#!/usr/bin/env bash
set -xeuf -o pipefail

##############################################################################
# Create database
# Globals:
#     None
# Arguments:
#     $1 - database name
#     $2 - username
#     $3 - user password
# Returns:
#     None
##############################################################################
function create_db() {
    local db_name="$1";
    local username="$2";
    local user_pass="$3";

    if [[ "${user_pass}" == "" ]]; then
        echo "===============================";
        echo "!!! Use \$ASA_POSTGRES_PASSWORD env var to secure your database !!!";
        echo "===============================";
        local pass_expr="";
    else
        local pass_expr="PASSWORD '${user_pass}'";
    fi

    if [[ "${db_name}" != '[postgres]' ]]; then
        local create_sql_expr="CREATE DATABASE ${db_name}";
        echo ${create_sql_expr} | gosu postgres postgres --single -jE;
    fi

    if [[ "${username}" != 'postgres' ]]; then
        local operation="CREATE";
    else
        local operation="ALTER";
    fi

    local user_sql="${operation} USER ${username} WITH SUPERUSER ${pass_expr};";
    echo ${user_sql} | gosu postgres postgres --single -jE;
}


##############################################################################
# Add line to pg_hba.conf to allow connection for user to some database
# Globals:
#     PGDATA - path to directory with all data needed for a database. Not modified
# Arguments:
#     $1 - database name
#     $2 - username
#     $3 - user password
# Returns:
#     None
##############################################################################
function add_allowed_db_connections() {
    local db_name="$1";
    local username="$2";
    local user_pass="$3";
    local auth_method="trust";

    # if [[ "${user_pass}" == "" ]]; then
    #     local auth_method="trust";
    # else
    #     local auth_method="md5";
    # fi

    { echo; echo "host ${db_name} ${username} 0.0.0.0/0 ${auth_method}"; } >> "${PGDATA}"/pg_hba.conf;
}


##############################################################################
# Globals:
#     PGDATA - path to directory with all data needed for a database. Not modified
#     ASA_POSTGRES_USER - Not modified.
#     ASA_POSTGRES_DB - Not modified.
# Returns:
#     None
##############################################################################
function initialize_pgdata() {
    echo "=== Initializing postgres data ===\n";

    mkdir -p "${PGDATA}";
    chown postgres:postgres ${PGDATA};

    gosu postgres initdb;

    # Init if env variables not exists
    : ${ASA_POSTGRES_USER:="postgres"};
    : ${ASA_POSTGRES_DB:=${POSTGRES_USER}};
    local ASA_POSTGRES_PASSWORD="$(cat /run/secrets/db_password)"

    create_db ${ASA_POSTGRES_DB} \
              ${ASA_POSTGRES_USER} \
              ${ASA_POSTGRES_PASSWORD};
    add_allowed_db_connections ${ASA_POSTGRES_DB} \
                               ${ASA_POSTGRES_USER} \
                               ${ASA_POSTGRES_PASSWORD};

    chown --recursive postgres "${PGDATA}";
}

if [[ -z "$(ls -A "${PGDATA}")" ]]; then
    initialize_pgdata;
fi

if [[ -f /postgresql.conf ]]; then
    echo "==== Updating config file in ${PGDATA} ====";
    echo mv /postgresql.conf "${PGDATA}/postgresql.conf";
    sed -ri "s/^#(listen_addresses\s*=\s*)\S+/\1'*'/" "${PGDATA}"/postgresql.conf;
fi

exec gosu postgres "$@";
