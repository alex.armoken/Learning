import inspect
import io
import logging
import os
import pathlib
import sys
import time
import string
import random
from typing import Optional, List, Dict

import alembic.command
import click
import psycopg2
from scrapy.crawler import CrawlerProcess

# import asa_db_models
from scraper import (
    AmazonProductReviewsSpider,
    PrepareForSavingPipeline,
    SaveInDatabasePipeline,
    COUNTRY_AMAZON_STORE_MAP
)

LOGGER = logging.Logger(__name__, logging.WARNING)


def get_full_typename(obj: object) -> str:
    if not inspect.isclass(obj):
        obj = obj.__class__

    module = obj.__module__
    if module is None or module == str.__class__.__module__:
        return obj.__name__  # Avoid reporting __builtin__
    else:
        return module + '.' + obj.__name__


def get_db_password() -> str:
    try:
        return os.environ.get("ASA_POSTGRES_PASSWORD", "asa")
    except KeyError:
        with open("/run/secrets/db_password", "r") as password_file:
            return password_file.read()


def get_db_url_params() -> Dict[str, str]:
    return {
        "user": os.environ.get("ASA_POSTGRES_USER", "asa"),
        "password": get_db_password(),
        "database": os.environ.get("ASA_POSTGRES_DB", "asa"),
        "host": os.environ.get("ASA_POSTGRES_HOST", "postgres"),
        "port": os.environ.get("ASA_POSTGRES_PORT", "5432")
    }


def get_db_url() -> str:
    return "postgresql://{user}:{password}@{host}:{port}/{database}".format(
        **get_db_url_params()
    )


def get_dsn() -> str:
    return "dbname={database} user={user} password={password} " \
        "host={host} port={port}".format(**get_db_url_params())


def get_app_db_scheme_revision() -> str:
    pass
    # db_url = get_db_url()
    # alembic_config = asa_db_models.get_alembic_config(db_url)

    # alembic_output = io.StringIO()
    # alembic_config.stdout = alembic_output
    # alembic.command.heads(alembic_config)
    # alembic_output.seek(0)

    # return alembic_output.read().split(" ")[0]


def get_cur_db_scheme_revision() -> str:
    with psycopg2.connect(**get_db_url_params()) as connection:
        with connection.cursor() as cursor:
            cursor.execute("SELECT version_num FROM alembic_version;")
            return cursor.fetchone()[0]


def get_bot_random_name() -> str:
    NAME_LENGTH = 30
    return "".join(
        random.choices(string.ascii_uppercase + string.digits,
                       k=NAME_LENGTH)
    )


def wait_for_db() -> None:
    app_scheme_revision = get_app_db_scheme_revision()
    while True:
        try:
            cur_scheme_revision = get_cur_db_scheme_revision()
            LOGGER.info(
                "Current db scheme revision: {}, "
                "App scheme revision: {}".format(
                    cur_scheme_revision,
                    app_scheme_revision
                )
            )
            if app_scheme_revision == cur_scheme_revision:
                break

        except (psycopg2.OperationalError, psycopg2.ProgrammingError) as exc:
            LOGGER.error(exc)

        time.sleep(10)


def fetch_asin(store_url: str, key_phrase: str) -> None:
    crawler_process = CrawlerProcess(
        settings={
            "BOT_NAME": get_bot_random_name(),
            "SPIDER_MIDDLEWARES_BASE": {
                "scrapy.spidermiddlewares.httperror.HttpErrorMiddleware": 50
            },
            "ITEM_PIPELINES": {
                get_full_typename(PrepareForSavingPipeline): 100,
                get_full_typename(SaveInDatabasePipeline): 150
            },
            "USER_AGENT": ("Mozilla/6.0 (Windows NT 10.0; Win64; x64)"
                           " AppleWebKit/537.36 (KHTML, like Gecko)"
                           " Chrome/60.0.3112.113 Safari/537.36"),
            "ROBOTSTXT_OBEY": True,
            "ASA_POSTGRES_URL": get_db_url(),
            "ASA_POSTGRES_DSN": get_dsn()
        }
    )
    crawler_process.crawl(AmazonProductReviewsSpider,
                          store_url=store_url,
                          key_phrase=key_phrase)
    crawler_process.start(stop_after_crawl=True)
    crawler_process.join()


def fetch_asin_from_all_stores(
    key_phrase: str,
    result_dir: pathlib.Path
) -> Optional[List[pathlib.Path]]:
    output_dir = result_dir / "output_asins"
    output_dir.mkdir(parents=True, exist_ok=True)

    paths_to_result_files = []
    for store_url in COUNTRY_AMAZON_STORE_MAP.values():
        maybe_path_to_result_file = fetch_asin(store_url,
                                               key_phrase,
                                               output_dir)
        if maybe_path_to_result_file is not None:
            paths_to_result_files.append(maybe_path_to_result_file)

        time.sleep(1)

    return paths_to_result_files


@click.command()
@click.option("--country", help="Country for searching",
              type=click.STRING, required=False, default="US")
@click.option("--key-phrase", help="Search Key Phrase",
              type=click.STRING, default="Laptop bag")
def main(country: str = None, key_phrase: str = None) -> int:
    if country is None:
        LOGGER.error("Reviews will be fetched from all available stores")

    if key_phrase is None:
        LOGGER.error("Key Phrase required!")
        return 1

    wait_for_db()
    fetch_asin(COUNTRY_AMAZON_STORE_MAP[country], key_phrase)
    return 0


if __name__ == "__main__":
    sys.exit(main())
