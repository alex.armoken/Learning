import re

import scrapy
from scrapy.loader.processors import TakeFirst, MapCompose


def strip_string(text: str) -> str:
    without_html_tags = re.sub('<[^<]+?>', '', text)

    return without_html_tags.strip("\n\t ")


class Company(scrapy.Item):
    name = scrapy.Field()
    dependent_product_asin = scrapy.Field()

    url_path = scrapy.Field()
    store_url = scrapy.Field()


class CompanyLoader(scrapy.loader.ItemLoader):
    default_item_class = Company
    default_input_processor = MapCompose(strip_string)
    default_output_processor = TakeFirst()


class Product(scrapy.Item):
    asin = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()

    url_path = scrapy.Field()
    store_url = scrapy.Field()
    info_upload_date = scrapy.Field()


class ProductLoader(scrapy.loader.ItemLoader):
    default_item_class = Product
    default_input_processor = MapCompose(strip_string)
    default_output_processor = TakeFirst()

    info_upload_date_in = MapCompose()


class ProductReview(scrapy.Item):
    review_id = scrapy.Field()
    product_asin = scrapy.Field()

    author = scrapy.Field()
    title = scrapy.Field()
    text = scrapy.Field()

    date = scrapy.Field()
    stars = scrapy.Field()
    helpful_count = scrapy.Field()


class ProductReviewLoader(scrapy.loader.ItemLoader):
    default_item_class = ProductReview

    default_input_processor = MapCompose(strip_string)
    default_output_processor = TakeFirst()

    date_in = MapCompose(strip_string, str.lower)
    stars_in = MapCompose(strip_string, str.lower)
    helpful_count_in = MapCompose(strip_string, str.lower)
