import logging

from datetime import datetime
from typing import List, Dict, Optional

import scrapy

from .items import (Company,
                    CompanyLoader,

                    Product,
                    ProductReview,

                    ProductLoader,
                    ProductReviewLoader)

LOGGER = logging.Logger(__name__, logging.WARNING)


class AmazonProductReviewsSpider(scrapy.Spider):
    name = "asa-reviews-sentimental-analyser"

    def __init__(self, store_url, key_phrase, *args, **kwargs):
        super(AmazonProductReviewsSpider, self).__init__(*args, **kwargs)

        self._store_url = store_url
        self._key_phrase = key_phrase

        search_url = "https://www.{0}/s/?keywords={1}".format(
            self._store_url,
            self._key_phrase
        )
        self.start_urls = [search_url]

    def _is_valid_item(self, item: scrapy.Item, keys: List[str]) -> bool:
        for key in keys:
            if key not in item:
                return False

        return True

    def _parse_product_page(self, response: scrapy.http.Response):
        loader = CompanyLoader(selector=response)

        loader.add_css("name", "//*[@id='bylineInfo']/text()")

        loader.add_css("url_path", "//*[@id='bylineInfo']/@href")
        loader.add_value("store_url", self._store_url)
        loader.add_value("dependent_product_asin",
                         response.meta["product_asin"])
        maybe_valid_item = loader.load_item()
        if self._is_valid_item(maybe_valid_item, ["name"]):
            yield maybe_valid_item
        else:
            yield None

    def _parse_product_review_item(
        self,
        review_item: scrapy.selector.Selector,
        product_asin: str
    ) -> Optional[ProductReview]:
        loader = ProductReviewLoader(selector=review_item)

        loader.add_css("review_id", "div.a-section::attr(id)")
        loader.add_value("product_asin", product_asin)

        loader.add_css("author", "span.a-profile-name::text")
        loader.add_xpath(
            "title",
            ".//a[@data-hook='review-title']/span/text()"
        )
        loader.add_xpath("text", ".//span[@data-hook='review-body']")

        loader.add_css("date", "[data-hook='review-date']::text")
        loader.add_css("stars", "a::attr(title)")
        # Optional
        loader.add_css(
            "helpful_count",
            "span[data-hook='helpful-vote-statement']::text"
        )

        maybe_valid_item = loader.load_item()
        if self._is_valid_item(maybe_valid_item,
                               ["review_id", "author", "title", "text",
                                "date", "stars"]):
            return maybe_valid_item

        return None

    def _parse_product_reviews_page(self, response: scrapy.http.Response):
        for review_item in response.css("div[data-hook='review']"):
            maybe_item = self._parse_product_review_item(
                review_item,
                response.meta["product_asin"]
            )
            if maybe_item is None:
                LOGGER.info(
                    "Skipping response item because of incomplete data"
                )
                continue

            yield maybe_item

        next_page = response.css(".a-last > a::attr(href)").extract_first()
        if next_page is not None:
            yield response.follow(next_page,
                                  callback=self.parse,
                                  meta=response.meta)

    def _get_product_reviews_url(self, product: Product) -> str:
        return "https://www.{0}/product-reviews/" \
            "{1}/ref=dpx_acr_txt?showViewpoints=1".format(
                self._store_url,
                product["asin"]
            )

    def _parse_search_result_item(
        self,
        result_item: scrapy.selector.Selector
    ) -> Optional[Product]:
        loader = ProductLoader(selector=result_item)

        loader.add_css("asin", "div::attr(data-asin)")
        loader.add_xpath("name", "//div/h2/a/span/text()")

        loader.add_css("price", "div > div > a > span > span.a-offscreen")
        loader.add_css("price",
                       "div > div > div.a-section > div > span.a-color-base")

        loader.add_value("store_url", self._store_url)
        loader.add_xpath("url_path", "//div/h2/a/@href")
        loader.add_value("info_upload_date", datetime.utcnow())

        maybe_valid_item = loader.load_item()
        if self._is_valid_item(maybe_valid_item, ["asin", "name", "url_path"]):
            return maybe_valid_item

        return None

    def _get_product_page_url(self, product: Product) -> str:
        return "https://www.{0}{1}".format(product["store_url"],
                                           product["url_path"])

    def _is_valid_response(self, response: scrapy.http.Response) -> bool:
        sorry_text = (
            "Sorry, we just need to make sure you're not a robot. "
            "For best results, please make sure your browser "
            "is accepting cookies."
        )
        return response.text.find(sorry_text) == -1

    def parse(self, response: scrapy.http.Response):
        if not self._is_valid_response(response):
            print(response)
            return

        for result_item in response.css("div[data-asin].s-result-item"):
            maybe_item = self._parse_search_result_item(result_item)
            if maybe_item is None:
                LOGGER.info("Skipping result item because of incomplete data")
                continue

            # Order is meaningfull, if during reviews scraping spider will
            # failed, lack of product db entry will notify that not all
            # reviews were scraped
            yield scrapy.Request(
                self._get_product_page_url(maybe_item),
                callback=self._parse_product_page,
                meta={"product_asin": maybe_item["asin"]}
            )
            yield scrapy.Request(
                self._get_product_reviews_url(maybe_item),
                callback=self._parse_product_reviews_page,
                meta={"product_asin": maybe_item["asin"]}
            )
            yield maybe_item

        maybe_next_page_path = response.css(".a-last > a::attr(href)") \
                                       .extract_first()
        if maybe_next_page_path is not None and maybe_next_page_path != "":
            next_page_path = "https://www.{0}{1}".format(
                self._store_url,
                maybe_next_page_path
            )
            yield response.follow(next_page_path,
                                  callback=self.parse,
                                  meta=response.meta)
