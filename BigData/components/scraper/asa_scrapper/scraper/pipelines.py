import re
import time
import string
import datetime
import dateparser
from json import JSONDecodeError

import googletrans

from .items import Company, Product, ProductReview


def translate_string(text: str, translator: googletrans.Translator) -> str:
    text = translator.translate(text, dest="en", src="auto").text
    time.sleep(0.3)

    return text


class PrepareForSavingPipeline(object):
    """Translate some product and product review items fields."""

    def __init__(self):
        self._number_regex = re.compile("[0-9]+")
        self._real_number_regex = re.compile("-?[0-9]+(\.[0-9]*)?")
        self._translator = googletrans.Translator(
            service_urls=None,
            user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64)',
            proxies=None,
            timeout=None
        )

    def _convert_date_to_utc(self, date_string: str) -> datetime.datetime:
        return dateparser.parse(date_string)

    def _convert_stars_to_float(self, stars_string: str) -> float:
        parts = stars_string.split()
        for part in parts:
            if self._real_number_regex.match(part) is not None:
                return float(part)
        else:
            raise ValueError(f"Wrong stars string! <<{stars_string}>>")

    def _conver_helpful_count_to_int(self, helpful_count_string: str) -> int:
        if helpful_count_string == "":
            return 0

        try:
            enlish_translation = translate_string(helpful_count_string,
                                                  self._translator)
        except JSONDecodeError:
            return 0

        parts = enlish_translation.translate(
            str.maketrans("", "", string.punctuation)
        ).split()
        for part in parts:
            if self._number_regex.match(part) is not None:
                return int(part)

        return 0

    def process_item(self, item, spider):
        if isinstance(item, ProductReview):
            item["date"] = self._convert_date_to_utc(item["date"])
            item["stars"] = self._convert_stars_to_float(item["stars"])
            item["helpful_count"] = self._conver_helpful_count_to_int(
                item.get("helpful_count", "")
            )

        return item


class SaveInDatabasePipeline(object):
    """Save product and product reviews items to DB."""

    def __init__(self, postgres_db_url: str, postgres_dsn: str):
        self.postgres_db_url = postgres_db_url
        self.postgres_dsn = postgres_dsn

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            postgres_db_url=crawler.settings["ASA_POSTGRES_URL"],
            postgres_dsn=crawler.settings["ASA_POSTGRES_DSN"]
        )

    def open_spider(self, spider):
        # Create db client
        pass

    def close_spider(self, spider):
        # Destroy db client
        pass

    def _save_company_item(self, item: Company) -> None:
        pass

    def _save_product_item(self, item: Product) -> None:
        pass

    def _save_product_review_item(self, item: ProductReview) -> None:
        pass

    def process_item(self, item, spider):
        if isinstance(item, Company):
            self._save_company_item(item)
        elif isinstance(item, Product):
            self._save_product_item(item)
        elif isinstance(item, ProductReview):
            self._save_product_review_item(item)
        else:
            raise NotImplementedError(type(item))

        return item
