# db-models

## What about package?
* Read **asa_db_models/__init__.py**
* And read **asa_db_models/models.py**

## How to upgrade database to the latest scheme?
* Go to asa_db_models directory

* And run
``` shell
ASA_POSTGRES_HOST={url} ASA_POSTGRES_PASSWORD={password} alembic upgrade head
```

## How to upgrade database to specific scheme version?
* Go to asa_db_models directory

* Run
``` shell
ls alembic/version
```
Where you will see files in **{id}_tag.py** format.

* Copy id And run
``` shell
ASA_POSTGRES_HOST={url} ASA_POSTGRES_PASSWORD={password} alembic upgrade {id}
```

## How to create new migrations?
* Run database, because Alembic requires it to determine db scheme version.

* And run
``` shell
ASA_POSTGRES_HOST={url} ASA_POSTGRES_PASSWORD={password} alembic revision --autogenerate -m "{tag}"
```
