import datetime
from decimal import Decimal
from typing import Optional

import sqlalchemy
from sqlalchemy import (Column,
                        ForeignKey,
                        MetaData)
from sqlalchemy.types import (TEXT,
                              BOOLEAN,
                              FLOAT,
                              INTEGER,
                              BIGINT,
                              DECIMAL,
                              DateTime)
from sqlalchemy.ext.declarative import declarative_base
Base = declarative_base()
metadata = MetaData()


class Company(Base):
    __tablename__ = "compan1nies"

    id = Column(BIGINT, primary_key=True)
    name = Column(TEXT, nullable=False)

    def __init__(self, id: int, name: str):
        self.id = id
        self.name = name


class Product(Base):
    __tablename__ = "products"

    id = Column(BIGINT, primary_key=True)

    asin = Column(TEXT, nullable=False)
    url_path = Column(TEXT, nullable=False)
    store_url = Column(BOOLEAN, nullable=False)

    name = Column(TEXT, nullable=False)
    company = Column(ForeignKey("companies.id"), nullable=False)
    price = Column(DECIMAL, nullable=True)

    info_upload_date = Column(DateTime(datetime.timezone.utc), nullable=False)

    def __init__(self, id: int,
                 asin: str, url_path: str, store_url: str,
                 name: str, company_id: int, price: Optional[Decimal],
                 info_upload_date: datetime.datetime):
        self.id = id

        self.asin = asin
        self.url_path = url_path
        self.store_url = store_url

        self.name = name
        self.company = company_id
        self.price = price

        self.info_upload_date = info_upload_date


class ProductReview(Base):
    __tablename__ = "products_reviews"

    id = Column(BIGINT, primary_key=True)

    amazon_id = Column(TEXT, nullable=False)
    product = Column(ForeignKey("products.id"), nullable=False)

    author = Column(TEXT, nullable=False)
    title = Column(TEXT, nullable=False)
    text = Column(TEXT, nullable=True)

    date = Column(DateTime(datetime.timezone.utc), nullable=False)
    stars = Column(FLOAT, nullable=False)
    helpful_count = Column(INTEGER, nullable=False)

    def __init__(self, id: int, amazon_id: str, product_id: int,
                 author: str, title: str, text: Optional[str],
                 date: datetime.datetime, stars: float, helpful_count: int):
        self.id = id

        self.amazon_id = amazon_id
        self.product = product_id

        self.title = title
        self.author = author
        self.text = text

        self.stars = stars
        self.date = date
        self.helpful_count = helpful_count


class SpiderRequest(Base):
    __tablename__ = "spider_requests"

    id = Column(BIGINT, primary_key=True)

    request = Column(TEXT, nullable=False)
    is_crawled = Column(BOOLEAN, nullable=False)
    update_date = Column(DateTime(datetime.timezone.utc), nullable=False)

    def __init__(self, id: int, request: str, is_crawled: bool,
                 update_date: datetime.datetime):
        self.id = id
        self.request = request
        self.is_crawled = is_crawled
        self.update_date = update_date


class SpiderSettings(Base):
    __tablename__ = "spider_settings"

    id = Column(BIGINT, primary_key=True)

    key = Column(TEXT, nullable=False)
    value = Column(TEXT, nullable=False)

    def __init__(self, id: int, key: str, value: str):
        self.id = id
        self.key = key
        self.value = value
