import pathlib
import pkg_resources

import alembic.config

# from .models import Base,                               \


def _get_path_to_alembic_config() -> str:
    path_to_alembic_config = "./alembic.ini"

    return pkg_resources.resource_filename(
        __name__,
        path_to_alembic_config
    )


def get_alembic_config(db_url: str) -> alembic.config.Config:
    path_to_config = _get_path_to_alembic_config()

    config = alembic.config.Config(path_to_config)
    config.set_main_option(
        "script_location",
        "asa_db_models:alembic"
    )

    config.set_main_option("sqlalchemy.url", db_url)

    return config
