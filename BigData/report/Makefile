CC          := latexmk
BIBTEX      := bibtex
PYTHON      := /usr/bin/env python3
OUTPUT_DIR  := "./build"
BASE_CFLAGS := -f -xelatex -usepretex -shell-escape -output-directory=${OUTPUT_DIR}

TEX_FILES   := $(filter %.tex,$(shell ls))
PDF_FILES   := $(TEX_FILES:%.tex=%.pdf)
TEX_TARGETS := $(foreach tex_file,$(TEX_FILES),$(basename $(tex_file)))

define build_tex_cmd_ctor
	$(CC) ${BASE_CFLAGS} -jobname="$1" "$1"
endef

.PHONY: all
all: $(TEX_TARGETS)

.PHONY: $(TEX_TARGETS)
$(TEX_TARGETS):
	@$(MAKE) -s "$@.pdf"

%.pdf: %.tex
	@$(call build_tex_cmd_ctor,$(strip $(basename $<)))

.PHONY: trim_section_names
trim_section_names:
	@$(PYTHON) ./tools/section_names_trimmer.py \
						--path "./report_sections" \
						--recursive

.PHONY: clean
clean:
	rm --recursive --force ${OUTPUT_DIR}

