using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace op
{
    class Rational: IEquatable<Rational>, IComparable<Rational>
    {
        private int n;
        private int m;

        public Rational() {}

        public Rational(int a, int b)
        {
            n = a;
            m = (b > 0) ? b : 1;
        }

        public int N
        {
            get {return n;}
            set
            {
                if (value >= 0)
                    n = value;
                else
                    throw new Exception("Число должно быть натуральным!");
            }
        }

        public int M
        {
            get { return m;}
            set
            {
                if (value > 0)
                    m = value;
                else
                    throw new Exception("Число должно быть натуральным!");
            }
        }

        public static Rational operator +(Rational a, Rational b)
        {
            Rational result = new Rational(1, 1);

            result.m = a.m * b.m;
            result.n = result.m/a.n + result.m/b.n;

            return result;
        }

        public static Rational operator -(Rational a, Rational b)
        {
            Rational result = new Rational(1, 1);

            result.m = a.m * b.m;
            result.n = result.m/a.n - result.m/b.n;

            return result;
        }

        public static Rational operator *(Rational a, Rational b){
            Rational result = new Rational();
            result.n = a.n * b.n;
            result.m = a.m * b.m;
            return result;
        }

        public static Rational operator /(Rational a, Rational b){
            Rational result = new Rational();
            result.n = a.n * b.m;
            result.m = a.m * b.n;
            return result;
        }

        public static bool operator >(Rational a, Rational b)
        {
            if(a.CompareTo(b) == 1)
                return true;
            else
                return false;
        }

        public static bool operator >=(Rational a, Rational b)
        {
            if(a.CompareTo(b) >= 0)
                return true;
            else
				return false;
        }

            public static bool operator <(Rational a, Rational b)
        {
            if(a.CompareTo(b) == -1)
                return true;
            else
                return false;
        }

        public static bool operator <=(Rational a, Rational b)
        {
            if(a.CompareTo(b) <= 0)
                return true;
            else
				return false;
        }

            public static bool operator !=(Rational a, Rational b)
        {
            if(a.n == b.n && a.m == b.m)
                return false;
            else
				return true;
        }

                public static bool operator ==(Rational a, Rational b)
        {
            return a.Equals(b);
        }

                    public static implicit operator Rational(int a)
        {
            return new Rational(a, 1);
        }

        public static explicit operator double(Rational a)
        {
			double n = a.N;
			double m = a.M;
            return (n/m);
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}", n, m);
        }

        public  string ToStringDecFrac(int r)
        {
            double DecFrac = n / m;
            return string.Format("{0}", DecFrac);
        }

        public bool Equals(Rational other)
        {
            if(this.n == other.n && this.m == other.m)
                return true;
            else
                return false;
        }

        public override bool Equals(Object obj)
        {
            Rational RationalObj = obj as Rational;
            return Equals(RationalObj);
        }

        public override int GetHashCode()
        {
            return (this.n / this.m).GetHashCode();
        }

        public int CompareTo(Rational other)
        {
            if (this.Equals(other))return 0;
            else if (this.m > other.m) return 1;
            else if (this.m < other.m) return -1;
            else if (this.m == other.m && this.n > other.n) return 1;
            else if (this.m == other.m && this.n < other.n) return -1;
            else return 1;
        }
    }

    static class op
    {
        static public void Main()
        {
            Console.WriteLine("Read rational number as a fraction");
            string ast = Console.ReadLine();
            string bst = Console.ReadLine();
            int a = Convert.ToInt16(ast);
            int b = Convert.ToInt16(bst);

            Rational first = new Rational(a, b);
            Console.WriteLine("Nothing");
            Console.WriteLine(first.ToString());
            Console.WriteLine(first.ToStringDecFrac(0));

            Console.WriteLine("Read rational number as string");
            string rational_number = Console.ReadLine();

            Regex slash = new Regex("/");
            string[] two_numbers = slash.Split(rational_number);
            a = Convert.ToInt16(two_numbers[0]);
            b = Convert.ToInt16(two_numbers[1]);
            Rational second = new Rational(a, b);

            Console.WriteLine(second.ToString());
            Console.WriteLine(second.ToStringDecFrac(0));

            Console.WriteLine("Does First equals to Second?");
            if ((object)first == (object)second)
            {
                Console.WriteLine("True");
            }
            else
            {
                Console.WriteLine("False");
            }

            Console.WriteLine("What is more?");
            Console.WriteLine(first.CompareTo(second));

            first = 84927;
            Console.WriteLine(first.ToStringDecFrac(0));

            double end  = (double)second;
            Console.WriteLine(end);
        }
    }
}