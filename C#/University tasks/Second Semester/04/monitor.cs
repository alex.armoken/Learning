using System;
using System.Text;
using System.Threading;
using System.Runtime.InteropServices;
using System.Deployment.Internal;

namespace sys
{
	public class sys_mon
	{
		[DllImport ("libop.so", EntryPoint = "getUsedMem")] static public extern int getUsedMem();
		[DllImport ("libop.so", EntryPoint = "totalPhysMem")] static public extern ulong totalPhysMem();
		[DllImport ("libop.so", EntryPoint = "totalFreeMem")] static public extern ulong totalFreeMem();
		[DllImport ("libop.so", EntryPoint = "totalSwapMem")] static public extern ulong totalSwapMem();
		[DllImport ("libop.so", EntryPoint = "physSwapUsed")] static public extern ulong physSwapUsed();
		[DllImport ("libop.so", EntryPoint = "physCpuUsage")] static public extern double physCpuUsage();
	}

	public static class main_sys
	{
		public static void Main()
			{
				int prog_work = 1;


				while (prog_work != 0)
				{
					Thread.Sleep(500);
					Console.Clear();

					int getUsedMem = sys_mon.getUsedMem();
					ulong totalPhysMem = sys_mon.totalPhysMem();
					ulong totalFreeMem = sys_mon.totalFreeMem();
					ulong totalSwapMem = sys_mon.totalSwapMem();
					ulong physSwapUsed = sys_mon.physSwapUsed();
					double physCpuUsege = sys_mon.physCpuUsage();

					Console.WriteLine("UsedMem: " + getUsedMem + " mB");
					Console.WriteLine("TotalPhysmem: " + totalPhysMem + " mB");
					Console.WriteLine("TotalFreemem: " + totalFreeMem + " mB");
					Console.WriteLine("TotalSwapMem: " + totalSwapMem + " mB");
					Console.WriteLine("PhysSwapUsed: " + physSwapUsed + " mB");
					Console.WriteLine("PhysCpuUsage: {0:F2} %",physCpuUsege);
				}
			}
	}
}
