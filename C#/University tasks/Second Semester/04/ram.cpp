#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "sys/types.h"
#include "sys/sysinfo.h"
#include <stdexcept>

extern "C"
{
    unsigned long totalPhysMem();
    unsigned long totalFreeMem();
    unsigned long totalSwapMem();
    unsigned long totalFreeSwap();
    unsigned long physSwapUsed();
    int getUsedMem();
}

struct sysinfo memInfo;


unsigned long totalPhysMem()
{
    sysinfo (&memInfo);
    return (memInfo.totalram * memInfo.mem_unit / 1024 / 1024);
}
unsigned long totalFreeMem()
{
    sysinfo (&memInfo);
    return (memInfo.freeram * memInfo.mem_unit / 1024 / 1024);
}

unsigned long totalSwapMem()
{
    sysinfo (&memInfo);
    return (memInfo.totalswap * memInfo.mem_unit / 1024 / 1024);
}

unsigned long totalFreeSwap()
{
    sysinfo (&memInfo);
    return (memInfo.freeswap * memInfo.mem_unit / 1024 / 1024);
}

unsigned long physSwapUsed()
{
    sysinfo (&memInfo);
    return totalSwapMem() - totalFreeSwap();
}


int getUsedMem()
{
    FILE* file;
    char lose[200];
    char mem_str[200];
    unsigned long physAvailableMem = 0;

    file = fopen("/proc/meminfo", "r");
    fgets(lose, 200, file);
    fgets(lose, 200, file);
    fscanf(file, "MemAvailable:    %lu kB", &physAvailableMem);
    fclose(file);

    unsigned long physMemUsed = totalPhysMem() - physAvailableMem / 1024;

    return physMemUsed;
}
