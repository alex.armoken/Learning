using System;

namespace onedot
{
    public class onedot
	{
        static int mpx = 5;
        public static int mpy = 5;
        public static char [,] field = new char [9,18];
        public static int bpxc;
        public static int bpyc;
        public const char empty_p = '\u25cb';
        public const char full_p = '\u25cf';
        public const char pointer = '\u2687';
        public const char bad_point = '\u25e3';
        public static Random bad_rund = new Random();
        public static int back_value;
        public static int game_cond = 0;
        public static ConsoleKeyInfo actkey;

        public static void Main() {
            Random bad_point_x = new Random();
            Random bad_point_y = new Random();

            for (int i = 0; i<=8; i++) {
                for (int j = 0;j <= 17; j++) {
                    field[i,j] = empty_p;
                }

            }

            bpxc = bad_point_x.Next(2,5);
            bpyc = bad_point_y.Next(5,12);
            redraw();

            while ((actkey.Key != ConsoleKey.Escape) && (game_cond == 0)) {
                actkey = Console.ReadKey();


                if (actkey.Key == ConsoleKey.A) {
                    moveleft(mpx, mpy, pointer);
                    mpx = back_value;
                }
                if (actkey.Key == ConsoleKey.D) {
                    moveright(mpx, mpy, pointer);
                    mpx = back_value;
                }
                if (actkey.Key == ConsoleKey.W) {
                    moveup(mpx, mpy, pointer);
                    mpy = back_value;
                }
                if (actkey.Key == ConsoleKey.S) {
                    movedown(mpx, mpy, pointer);
                    mpy = back_value;
                }
                if (actkey.Key == ConsoleKey.Enter) setpoint();
                if (victory_check()) {
                    break;
                }
            }

            if (game_cond == 1) {
                Console.SetCursorPosition(50,25);
                Console.WriteLine("You win!");
            }
            else if (game_cond == 2) {
                Console.SetCursorPosition(50,25);
                Console.WriteLine("You lose!");
            }


        }

        public static void redraw () {
            Console.Clear();
            for (int i = 0; i <= 8; ++i ) {
                for (int j = 0; j <= 17; ++j ) {
                    Console.SetCursorPosition(j,i);
                    Console.WriteLine(field[i,j]);
                }
            }
            Console.SetCursorPosition(bpyc,bpxc);
            Console.WriteLine(bad_point);
        }
        public static void moveleft (int px, int py, char tpoint) {
            if (!(px-1 < 0)) {
                --px;
                redraw();
                Console.SetCursorPosition(px, py);
                Console.WriteLine(tpoint);
                back_value = px;
            }
        }

        public static void moveright (int px, int py, char tpoint) {
            if (!(px+1 > 17)) {
                ++px;
                redraw();
                Console.SetCursorPosition(px, py);
                Console.WriteLine(tpoint);
                back_value = px;
            }
        }

        public static void moveup (int px, int py, char tpoint) {
            if (!(py-1 < 0)) {
                --py;
                redraw();
                Console.SetCursorPosition(px, py);
                Console.WriteLine(tpoint);
                back_value = py;

            }
        }

        public static void movedown (int px, int py,char tpoint) {
            if (!(py+1 > 8)) {
                ++py;
                redraw();
                Console.SetCursorPosition(px, py);
                Console.WriteLine(tpoint);
                back_value = py;
            }
        }

        public static void setpoint () {
            if (!((bpxc == mpx) && (bpyc == mpy))) {
                field[mpy,mpx] = full_p;
                victory_check();
                redraw();
                if (game_cond == 0) {
                    bad_point_move();
                    redraw();}
                //ctory_check();

            }
        }


        public static void moveTo(int newx, int newy) {
            bpxc = newx;
            bpyc = newy;
            redraw();
        }

        public static void bad_point_move () {
            bool f = false;
            int[] dx = new int[] {-1, 0, 1, 0};
            int[] dy = new int[] {0, 1, 0, -1};
            while (f == false) {
                int i = bad_rund.Next(0, 4);
                int newx = bpxc + dx[i];
                int newy = bpyc + dy[i];
                if (newx >= 0 && newx < 8 && newy >= 0 && newy < 18 && field[newx, newy] == empty_p) {
                    moveTo(newx, newy);
                    f = true;
                }
            }
        }
        public static bool victory_check () {
            if (bpxc == 0 || bpxc == 8 || bpyc == 0 || bpyc == 17)
            {
                game_cond = 2;
                return true;
            }
            if (field[bpxc-1, bpyc] == full_p && field[bpxc+1, bpyc] == full_p && field[bpxc, bpyc-1] == full_p && field[bpxc, bpyc+1] == full_p) {
                game_cond = 1 ;
                return true;
            }
            return false;
        }
    }
}
