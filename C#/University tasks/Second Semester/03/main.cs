using System;
using System.Text;

class alu
{
    public static string country = "USA";

    int capacity;
    string model;
    string company;
    string architecture;

    public alu(int capacity, string company = "Intel", string model = "i7", string architecture = "x86")
    {
        this.capacity = capacity;
        this.model = model;
        this.company = company;
        this.architecture = architecture;
    }

    public int Capacity
	{

        get { return capacity; }
        set { capacity = value; }
    }

    public string Model
    {
        get { return model; }
        set { model = value; }
    }

    public string Architecture
    {
        get { return architecture; }
        set { architecture = value; }
    }

    public string Company
    {
        get { return company; }
        set { company = value; }
    }

    public void Write_info()
    {
        Console.WriteLine(capacity + "\n" + model + "\n" + architecture +
                          "\n" + company + "\n");
    }

    public void reduce_price()
    {
        Console.WriteLine("Cho!? Are you adequate!?");
    }

    public void do_something(string action = "Sell shares")
    {
        Console.WriteLine("Nu ok " + action + "\n");
    }


}

class MyLab
{
    public static void Main()
    {
        Console.WriteLine(alu.country);
        Console.WriteLine("\n");

        alu Intel = new alu(32, "Intel");
        Intel.Write_info();

        alu AMD = new alu(64, "AMD", "Athlon");
        AMD.Write_info();

        Console.WriteLine("What is the model of the Intel processors");
        Intel.Model = Console.ReadLine();

        Console.WriteLine("What is the architecture of the AMD? ");

        AMD.Architecture = Console.ReadLine();

        Console.WriteLine("\nNew information: \n");
        Intel.Write_info();
        AMD.Write_info();

        Console.WriteLine("Press any key to do nothing :)");
        Console.Read();
        Console.WriteLine();
        Intel.do_something();
        Console.WriteLine("");

        Console.WriteLine("What can the company do? ");
        AMD.do_something(Console.ReadLine());
    }
}
