using System;
using System.Text;
using System.Collections.Generic;

public struct tcache
{
    public int L1;
    public int L2;
    public int L3;
}

public interface company_actions
{
    void Write_info();
    void reduce_price();
    void do_something(string action = "Open source code");
}

public abstract class alu_base
{
    static public string country = "USA";

    protected int capacity;
    protected tcache cache;
    protected string model;
    protected string company;
    protected string architecture;

    public virtual int L1S
    {
        get { return cache.L1; }
        set { cache.L1 = value; }
    }

    public virtual int L2S
    {
        get { return cache.L2; }
        set { cache.L2 = value; }
    }

    public virtual int L3S
    {
        get { return cache.L3; }
        set { cache.L3 = value; }
    }

    public int Capacity
    {
        get { return capacity; }
        set { capacity = value; }
    }

    public string Model
    {
        get { return model; }
        set { model = value; }
    }

    public string Architecture
    {
        get { return architecture; }
        set { architecture = value; }
    }
}

public class another_alu : alu_base, company_actions, IEquatable<another_alu>, IComparable<another_alu>
{
    public another_alu(){}

    public another_alu(int capacity, string model,string company, string architecture)
    {
        this.capacity = capacity;
        this.model = model;
        this.company = company;
        this.architecture = architecture;
    }

    public bool Equals(another_alu obj)
    {
        if (obj == null)
            return false;
        if(this.capacity == obj.capacity && this.model == obj.model
		   && this.company == obj.company && this.architecture == obj.architecture
		   && this.cache.L1 == obj.cache.L1 && this.cache.L2 == obj.cache.L2
		   && this.cache.L3 == obj.cache.L3)
            return true;
        else
            return false;
    }

    public int CompareTo(another_alu other)
    {
        if (other == null) return 1;
        else if (Equals(other)) return 0;
        else if (this.cache.L1 > other.cache.L1) return 1;
        else if (this.cache.L1 < other.cache.L1) return -1;
        else return 0;
    }

    public virtual void Write_info()
    {
        Console.WriteLine(capacity + "\n" + model + "\n" + architecture +
						  "\n" + company + "\n" + "L1 Cache: " + cache.L1 +
						  " mB\n" + "L2 Cache: " + cache.L2 + " mB\n" +
						  "L3 Cache: " + cache.L3 + " mB\n");
    }

    public virtual void reduce_price()
    {
        Console.WriteLine("Cho!? Are you adequate!?");
    }

    public virtual void do_something(string action = "Open source code")
    {
        Console.WriteLine("Nu ok " + action + "\n");
    }
}

class MyLab
{
    public static void Main()
    {
        Console.Clear();
        Console.WriteLine(alu_base.country);

        another_alu Intel1 = new another_alu(32, "i7", "Intel", "x86");
        Intel1.L1S = 10;
        Intel1.L2S = 15;
        Intel1.L3S = 11;
        Intel1.Write_info();

        another_alu Intel2 = new another_alu(64, "i3", "Intel", "x86");
        Intel2.L1S = 5;
        Intel2.L2S = 15;
        Intel2.L3S = 11;
        Intel2.Write_info();

        another_alu Intel3 = new another_alu(32, "i7", "Intel", "x86");
        Intel3.L1S = 10;
        Intel3.L2S = 15;
        Intel3.L3S = 11;
        Intel3.Write_info();

        another_alu AMD = new another_alu(64, "Athlon", "AMD", "armV7");
        AMD.L1S = 8;
        AMD.L2S = 20;
        AMD.L3S = 15;
        AMD.Write_info();

        Console.WriteLine("Intel1 & Intel2 " + Intel1.Equals(Intel2));
        Console.WriteLine("Intel1 & Intel3 " + Intel1.Equals(Intel3));
        Console.WriteLine("Intel1 & AMD " + Intel1.Equals(AMD));
        Console.WriteLine();

        Console.WriteLine("Intel1 & Intel2 " + Intel1.CompareTo(Intel2));
        Console.WriteLine("Intel1 & Intel3 " + Intel1.CompareTo(Intel3));
        Console.WriteLine("Intel1 & AMD " + Intel1.CompareTo(AMD));
        Console.WriteLine("Intel2 & AMD " + Intel2.CompareTo(AMD));

    }
}
