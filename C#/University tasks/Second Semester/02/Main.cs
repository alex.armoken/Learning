using System;
using System.Globalization;

namespace Main
{
    class my_lab
    {
        static void Main()
        {
            Console.WriteLine("1 - Month Menu");
            Console.WriteLine("2 - Revert Sentence");
            Console.WriteLine("3 - Numbers in Date");
            Console.WriteLine();

            ConsoleKeyInfo menu = Console.ReadKey();
            Console.WriteLine();
            Console.WriteLine();

            switch (menu.KeyChar)
            {
                case '1':
                    month_menu();
                    break;
                case '2':
                    main_revert();
                    break;
                case '3':
                    num_in_date();
                    break;
            }
        }
        static void month_menu()
        {
            string[] lang_arr = {"fr-FR", "de-DE", "en-US", "ru-RU"};

            for (int i = 0; i < lang_arr.Length; i++) {
                Console.WriteLine(i.ToString() + " - " + lang_arr[i]);
            }

            ConsoleKeyInfo menu = Console.ReadKey();
            Console.WriteLine();

            switch (menu.KeyChar)
            {
                case '0':
                    write_monthes(lang_arr[0]);
                    break;
                case '1':
                    write_monthes(lang_arr[1]);
                    break;
                case '2':
                    write_monthes(lang_arr[2]);
                    break;
                case '3':
                    write_monthes(lang_arr[3]);
                    break;
            }
        }

        static void write_monthes(string lang)
        {

            CultureInfo month_lang = new CultureInfo(lang);
            DateTimeFormatInfo op = month_lang.DateTimeFormat;

            foreach (string month_name in op.MonthNames)
            {
                Console.WriteLine("\"{0}\"",month_name);
            }
        }
        static void main_revert()
        {   string sentence;
            string new_sentence = string.Empty;
            int sentence_length;
            int word_end;

            Console.WriteLine("Input the words, please");
            sentence = Console.ReadLine();

            sentence_length = sentence.Length - 1;
            word_end = sentence_length;

            for (int i = sentence_length; i >= 0; --i) {
                if (sentence[i] == ' ' || i == 0)
                {
                    new_sentence += exec_string(sentence, i, word_end );
                    new_sentence += ' ';
                    word_end = i;
                }
            }

            new_sentence = remove_substring(new_sentence, 0, 1);
            Console.WriteLine(new_sentence);
        }

        static string exec_string(string input_str, int begin, int end)
        {
            string out_str = string.Empty;

            for (int i = begin; i <= end; i++)
            {
                out_str += input_str[i];
            }

            return out_str;
        }

        static string remove_substring(string input_str, int begin, int count)
        {
            string out_str = string.Empty;

            for (int i = input_str.Length; i < begin; i++)
            {
                out_str += input_str[i];
            }

            int end = begin + count;

            for (int j = end; j <= input_str.Length - 1 ; j++)
            {
                out_str += input_str[j];
            }

            return out_str;
        }
        public static void num_in_date()
        {
            DateTime first = DateTime.Now;
            string now_time = first.ToString("hh:mm:ss dd.MM.yyyy");

            date_chars(now_time);

            now_time = first.ToString("mm/dd/yy hh:MM:ss");
            date_chars(now_time);


        }

        public static void date_chars(string date_str)
        {
            Console.WriteLine(date_str);

            for  (int j = 0; j <= 9; j++) {
                int char_count = 0;
                string num_char = Convert.ToString(j);
                for (int i = 0; i < date_str.Length; i++)
                {
                    if (date_str[i] == num_char[0])
                    {
                        ++char_count;
                    }
                }
                Console.WriteLine(j + "  -  " + char_count);
            }
            Console.WriteLine();
        }


    }
}