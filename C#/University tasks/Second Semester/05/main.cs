using System;
using System.Text;

public struct tcache
{
    public int L1;
    public int L2;
    public int L3;

    // public Cache(int L1Cache, int L2Cache, int L3Cache)
    // {
    //     L1 = L1Cache;
    //     L2 = L2Cache;
    //     L3 = L3Cache;
    // }
}

public abstract class alu_base
{
    static public string country = "USA";

    protected tcache cache;
    protected int capacity;
    protected string model;
    protected string company;
    protected string architecture;

    public virtual int L1S
    {
        get { return cache.L1; }
        set { cache.L1 = value; }
    }

    public virtual int L2S
    {
        get { return cache.L2; }
        set { cache.L2 = value; }
    }

    public virtual int L3S
    {
        get { return cache.L3; }
        set { cache.L3 = value; }
    }

    public int Capacity
    {
        get { return capacity; }
        set { capacity = value; }
    }

    public string Model
    {
        get { return model; }
        set { model = value; }
    }

    public string Architecture
    {
        get { return architecture; }
        set { architecture = value; }
    }

    abstract public void Write_info();
    abstract public void reduce_price();
    abstract public void do_something(string action = "Sell shares");
}

public class alu_intel : alu_base
{
    public alu_intel()
    {}

    public alu_intel(int capacity, string model, string architecture = "X86")
    {
        this.capacity = capacity;
        this.model = model;
        this.architecture = architecture;
    }

    public override void Write_info()
    {
        Console.WriteLine(capacity + "\n" + model + "\n" + architecture + "\n" + company + "\n" + "L1 Cache: " + cache.L1 + " mB\n" + "L2 Cache: " + cache.L2 + " mB\n" + "L3 Cache: " + cache.L3 + " mB\n");
    }

    public override void reduce_price()
    {
        Console.WriteLine("Cho!? Are you adequate!?");
    }

    public override void do_something(string action = "Open source code")
    {
        Console.WriteLine("Nu ok " + action + "\n");
    }
}

public class alu_amd : alu_intel
{
    public alu_amd()
    {}

    public alu_amd(int capacity, string model, string architecture = "amd64")
    {
        this.capacity = capacity;
        this.model = model;
        this.architecture = architecture;
    }

    public override void reduce_price()
    {
        Console.WriteLine("Cho!? Are you adequate!?");
    }

    public override void do_something(string action = "Do nothing")
    {
        Console.WriteLine("Yes my lord. " + action + "\n");
    }
}

class MyLab
{
    public static void Main()
    {
        Console.Clear();
        Console.WriteLine(alu_base.country);
        Console.WriteLine("\n");

        alu_intel Intel = new alu_intel(32, "i7");
        Intel.Write_info();

        alu_amd AMD = new alu_amd(64, "Athlon");
        AMD.Write_info();

        Console.WriteLine("What is the model of the Intel processors");
        Intel.Model = Console.ReadLine();

        Console.WriteLine("What is the architecture of the AMD? ");

        AMD.Architecture = Console.ReadLine();

        Console.Clear();
        Console.WriteLine("\nNew information: \n");
        Intel.Write_info();
        AMD.Write_info();

        Console.WriteLine("Intel: Press any key to do nothing :)");
        Console.Read();
        Console.WriteLine();
        Intel.do_something();
        Console.WriteLine("");

        Console.WriteLine("Amd: What can the company do? ");
        AMD.do_something(Console.ReadLine());
    }
}