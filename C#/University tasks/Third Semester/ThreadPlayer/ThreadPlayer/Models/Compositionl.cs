using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadPlayer
{
    public class Composition : IEquatable<Composition>
    {
        public enum Genres
        {
            Unknown,
            Rock,
            Rap,
            Pop,
            Country,
            Jazz,
            Electronic
        }

        Genres genre;
        private int id;
        private int rating;
        private string title;
        private string artist;
        private TimeSpan length;

        public string ID
        {
            get { return id.ToString(); }
            private set {; }
        }
        public string Title
        {
            get { return title; }
            set {; }
        }
        public string Length
        {
            get { return length.ToString(); }
            set {; }
        }
        public string Artist
        {
            get { return artist; }
            private set {; }
        }
        public string Genre
        {
            get
            {
                switch (genre)
                {
                    case Composition.Genres.Rock:
                        return "Rock";
                    case Composition.Genres.Rap:
                        return "Rap";
                    case Composition.Genres.Pop:
                        return "Pop";
                    case Composition.Genres.Country:
                        return "Country";
                    case Composition.Genres.Jazz:
                        return "Jazz";
                    case Composition.Genres.Electronic:
                        return "Electronic";
                    default:
                        return "Unknown";
                }
            }
            private set {; }
        }
        public string Rating
        {
            get { return rating.ToString(); }
            private set {; }
        }
        public int RatingInt
        {
            get { return rating; }
            private set { ; }
        }
        public TimeSpan LengthTimeSpan
        {
            get { return length; }
            private set {; }
        }

        public Composition(int id, string title,
            TimeSpan length, string artist,
            Genres genre, int rating)
        {
            this.id = id;
            this.title = title;
            this.genre = genre;
            this.length = length;
            this.artist = artist;
            this.rating = rating;
        }

        public bool Equals(Composition other)
        {
            if (other.ID == ID) return true;
            else return false;
        }
    }

    static public class CompositionStrConversion
    {
        static public int stringToRating(string str)
        {
            int rating = Int32.Parse(str);

            if (rating <= 10 && rating >= 0)
                return rating;
            else return 0;
        }
        static public TimeSpan stringToLength(string str)
        {
            TimeSpan newTime;
            try
            {
                string[] splitTime = str.Split(':');
                newTime = new TimeSpan(Int32.Parse(splitTime[0]), Int32.Parse(splitTime[1]), Int32.Parse(splitTime[2]));
            }
            catch
            {
                newTime = new TimeSpan(0, 0, 0);
            }
            return newTime;
        }
        static public Composition.Genres stringToGenre(string str)
        {
            Composition.Genres genre;
            switch (str)
            {
                case "Rock":
                    genre = Composition.Genres.Rock;
                    break;
                case "Rap":
                    genre = Composition.Genres.Rap;
                    break;
                case "Pop":
                    genre = Composition.Genres.Pop;
                    break;
                case "Country":
                    genre = Composition.Genres.Country;
                    break;
                case "Jazz":
                    genre = Composition.Genres.Jazz;
                    break;
                case "Electronic":
                    genre = Composition.Genres.Electronic;
                    break;
                default:
                    genre = Composition.Genres.Unknown;
                    break;
            }
            return genre;
        }
    }
}
