using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadPlayer
{
    class Playlist : IEnumerable<Composition>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        private SortedDictionary<int, Composition> compositions = new SortedDictionary<int, Composition>();
        public event NotifyCollectionChangedEventHandler CollectionChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        public int ID { get; set; }
        public string Title { get; set; }
        public TimeSpan Length { get; set; }
        public int Rating { get; set; }
        public int Count
        {
            get { return compositions.Count(); }
        }

        public IEnumerator<Composition> GetEnumerator()
        {
            foreach (var i in compositions)
            {
                yield return i.Value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private void UpdatePlaylist()
        {
            int totalSeconds = (compositions.Sum(composition => composition.Value.LengthTimeSpan.Hours) * 3600 +
                compositions.Sum(composition => composition.Value.LengthTimeSpan.Minutes) * 60 +
                compositions.Sum(composition => composition.Value.LengthTimeSpan.Seconds));
            Length = new TimeSpan(totalSeconds / 3600, (totalSeconds % 3600) / 60, (totalSeconds % 3600) % 60);

            if (compositions.Count != 0)
            {
                Rating = (int)Math.Ceiling(compositions.Average(composition => composition.Value.RatingInt));
            }
        }

        public bool Add(int id, Composition composition)
        {
            if (!compositions.ContainsKey(id))
            {
                compositions.Add(id, composition);
                UpdatePlaylist();

                if (CollectionChanged != null)
                {
                    CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                }

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Length"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Rating"));
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Remove(int id)
        {
            if (compositions.ContainsKey(id))
            {
                compositions.Remove(id);
                UpdatePlaylist();

                if (CollectionChanged != null)
                {
                    CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                }

                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Length"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Rating"));
                }
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
