using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ThreadPlayer;
using ThreadPlayer.ViewModels;

namespace ThreadPlayer.ViewModels
{
    class SongLoaderViewModel : BaseViewModel
    {
        public string IDText { get; set; }
        public string TitleText { get; set; }
        public string GenreText { get; set; }
        public string ArtistText { get; set; }
        public string RatingText { get; set; }
        public string LengthText { get; set; }
        public Composition SelectedSong { get; set; }
        private ObservableCollection<Composition> allSongs;

        public SongLoaderViewModel(ObservableCollection<Composition> allSongs)
        {
            this.header = "Song loader";
            this.allSongs = allSongs;

            IDText = "ID";
            TitleText = "Title";
            GenreText = "Genre";
            ArtistText = "Artist";
            RatingText = "Rating";
            LengthText = "Length";

            AddSongCommand = new Command(arg => AddSongClick());
            RemoveSongCommand = new Command(arg => RemoveSongClick());
        }

        public ObservableCollection<Composition> AllSongs
        {
            get { return allSongs; }
            private set {; }
        }

        public ICommand AddSongCommand { get; set; }
        private void AddSongClick()
        {
            Composition currentComposition = allSongs.FirstOrDefault(arg => arg.ID == IDText);
            if (currentComposition == null)
            {
                allSongs.Add(new Composition(Int32.Parse(IDText), TitleText, CompositionStrConversion.stringToLength(LengthText), ArtistText, CompositionStrConversion.stringToGenre(GenreText), CompositionStrConversion.stringToRating(RatingText)));
            }

            IDText = "ID";
            TitleText = "Title";
            GenreText = "Genre";
            ArtistText = "Artist";
            RatingText = "Rating";
            LengthText = "Length";
            NotifyPropertyChanged(String.Empty);
        }

        public ICommand RemoveSongCommand { get; set; }
        private void RemoveSongClick()
        {
            allSongs.Remove(SelectedSong);
            NotifyPropertyChanged(String.Empty);
        }
    }
}
