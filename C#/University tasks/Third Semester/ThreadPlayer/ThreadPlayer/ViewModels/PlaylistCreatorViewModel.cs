using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ThreadPlayer.ViewModels;

namespace ThreadPlayer.ViewModels
{
    class PlaylistCreatorViewModel : BaseViewModel
    {
        private ObservableCollection<Composition> allSongs;
        private ObservableCollection<PlaylistViewModel> allPlaylists;
        private ObservableCollection<Composition> playlistSongs;
        public Composition SelectedSongInAllSongs { get; set; }
        public Composition SelectedSongInPlaylist { get; set; }

        public PlaylistCreatorViewModel(ObservableCollection<Composition> allSongs, ObservableCollection<PlaylistViewModel> allPlaylists)
        {
            header = "Playlist creator";
       
            IDText = "ID";
            NameText = "Name";
            this.allSongs = allSongs;
            this.allPlaylists = allPlaylists;
            playlistSongs = new ObservableCollection<Composition>();

            AddSongCommand = new Command(arg => AddSongClick());
            RemoveSongCommand = new Command(arg => RemoveSongClick());
            CreatePlaylistCommand = new Command(arg => CreatePlaylistClick());
        }

        public string IDText { get; set; }
        public string  NameText { get; set; }
        public ObservableCollection<Composition> AllSongs
        {
            get { return allSongs; }
            private set { ; }
        }
        public ObservableCollection<Composition> PlaylistSongs
        {
            get { return playlistSongs; }
            private set {; }
        }

        public ICommand AddSongCommand { get; set; }
        private void AddSongClick()
        {
            playlistSongs.Add(SelectedSongInAllSongs);
            NotifyPropertyChanged(String.Empty);
        }

        public ICommand RemoveSongCommand { get; set; }
        private void RemoveSongClick()
        {
            playlistSongs.Remove(SelectedSongInPlaylist);
            NotifyPropertyChanged(String.Empty);
        }

        public ICommand CreatePlaylistCommand { get; set; }
        private void CreatePlaylistClick()
        {
            allPlaylists.Add(new PlaylistViewModel(playlistSongs, Int32.Parse(IDText), NameText));
            playlistSongs = new ObservableCollection<Composition>();
            IDText = "ID";
            NameText = "Name";
            NotifyPropertyChanged(String.Empty);
        }
    }
}
