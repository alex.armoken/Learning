using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ThreadPlayer;
using ThreadPlayer.ViewModels;

namespace ThreadPlayer.ViewModels
{
    class PlaylistsViewModel : BaseViewModel
    {
        private PlaylistViewModel selectedPlaylist;
        private ObservableCollection<Object> allViewModels;
        private ObservableCollection<PlaylistViewModel> allPlaylists;

        public PlaylistsViewModel(ObservableCollection<PlaylistViewModel> allPlaylists, ObservableCollection<Object> allViewModels)
        {
            header = "All playlists";
            this.allPlaylists = allPlaylists;
            this.allViewModels = allViewModels;

            StartPlaylistCommand = new Command(arg => StartPlaylistClick());
            RemovePlaylistCommand = new Command(arg => RemovePlaylistClick());
        }

        public PlaylistViewModel SelectedPlaylist
        {
            get { return selectedPlaylist; }
            set
            {
                selectedPlaylist = value;
                NotifyPropertyChanged(String.Empty);
            }
        }
        public Composition SelectedSong { get; set; }
        public ObservableCollection<PlaylistViewModel> AllPlaylists
        {
            get { return allPlaylists; }
            private set { ; }
        }
        
        public ICommand StartPlaylistCommand { get; set; }
        private void StartPlaylistClick()
        {
            allViewModels.Add(SelectedPlaylist);
            //SelectedPlaylist.IsAlive = true;
            //SelectedPlaylist.StartThread();
            NotifyPropertyChanged(String.Empty);
        }

        public ICommand RemovePlaylistCommand { get; set; }
        private void RemovePlaylistClick()
        {
            allViewModels.Remove(SelectedPlaylist);
            allPlaylists.Remove(SelectedPlaylist);
            NotifyPropertyChanged(String.Empty);
        }
    }
}
