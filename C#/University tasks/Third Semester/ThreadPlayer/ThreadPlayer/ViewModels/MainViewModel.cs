using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ThreadPlayer;
using ThreadPlayer.ViewModels;
using Microsoft.Win32;
using System.IO;
using Microsoft.Practices.Composite.Presentation.Commands;

namespace ThreadPlayer.ViewModels
{

    class MainViewModel
    {
        private ObservableCollection<Object> allViewModels;
        private ObservableCollection<PlaylistViewModel> allPlaylists;
        private ObservableCollection<Composition> allCompositions;

        public MainViewModel()
        {
            allPlaylists = new ObservableCollection<PlaylistViewModel>();
            allViewModels = new ObservableCollection<object>();
            allCompositions = new ObservableCollection<Composition>();

            AddPlaylistsCommand = new Command(arg => AddPlaylistsMethod());
            AddSongLoaderCommand = new Command(arg => AddSongLoaderMethod());
            AddPlaylistCreatorCommand = new Command(arg => AddPlaylistCreatorMethod());
            SaveCommand = new Command(arg => SaveMethod());
            OpenCommand = new Command(arg => OpenMethod());
            RemoveCommand = new Command(arg => RemoveMethod());
            OnClosingCommand = new Command(arg => OnClosingMethod());

            AddPlaylistsMethod();
            AddPlaylistCreatorMethod();
            AddSongLoaderMethod();
            SelectedTab = allViewModels[0];            
        }

        public Object SelectedTab { get; set; }
        public ObservableCollection<Object> AllViewModels
        {
            get { return allViewModels; }
            private set {; }
        }

        public ICommand AddPlaylistsCommand { get; set; }
        private void AddPlaylistsMethod()
        {
            allViewModels.Add(new PlaylistsViewModel(allPlaylists, allViewModels));
        }

        public ICommand AddSongLoaderCommand { get; set; }
        private void AddSongLoaderMethod()
        {
            allViewModels.Add(new SongLoaderViewModel(allCompositions));
        }

        public ICommand AddPlaylistCreatorCommand { get; set; }
        private void AddPlaylistCreatorMethod()
        {
            allViewModels.Add(new PlaylistCreatorViewModel(allCompositions, allPlaylists));
        }

        public ICommand OpenCommand { get; set; }
        private void OpenMethod()
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = ".jsh";
            dlg.Filter = "All file (*.*)|*.*|Thredplayer file (jsh)|*.jsh";

            string filename = null;
            if (dlg.ShowDialog() == true)
            {
                filename = dlg.FileName;
            }

            if (filename != null)
            {
                StreamReader stream = null;
                try
                {
                    stream = new StreamReader(new FileStream(filename, FileMode.Open));
                    PlaylistViewModel newPlaylist = new PlaylistViewModel();
                    newPlaylist.StringReader(stream);
                    allPlaylists.Add(newPlaylist);
                    
                    foreach(Composition i in newPlaylist.AllSongs)
                    {
                        Composition currentComposition = allCompositions.FirstOrDefault(arg => arg.ID == i.ID);
                        if(currentComposition == null)
                        {
                            allCompositions.Add(i);
                        }
                    }
                }
                catch (IOException exc)
                {
                    MessageBox.Show(exc.Message);
                }
                finally
                {
                    if (stream != null)
                        stream.Close();
                }
            }
        }

        public ICommand SaveCommand { get; set; }
        private void SaveMethod()
        {
            PlaylistViewModel selectedPlaylist = SelectedTab as PlaylistViewModel;

            if (selectedPlaylist != null)
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.DefaultExt = ".jsh";
                dlg.Filter = "All file (*.*)|*.*|Thredplayer file (jsh)|*.jsh";

                string filename = null;
                if (dlg.ShowDialog() == true)
                {
                    filename = dlg.FileName;
                }

                if (filename != null)
                {
                    StreamWriter stream = null;
                    try
                    {
                        stream = new StreamWriter(new FileStream(filename, FileMode.Create));
                        selectedPlaylist.StringWrite(stream);
                    }
                    catch (IOException exc)
                    {
                        MessageBox.Show(exc.Message);
                    }
                    finally
                    {
                        if (stream != null)
                            stream.Close();
                    }
                }
            }
        }

        public ICommand RemoveCommand { get; set; }
        private void RemoveMethod()
        {
            PlaylistViewModel selectedPlaylist = SelectedTab as PlaylistViewModel;
            if(selectedPlaylist != null)
            {
                selectedPlaylist.IsAlive = false;
            }
            allViewModels.Remove(SelectedTab);
        }

        public ICommand OnClosingCommand { get; set; }
        public void OnClosingMethod()
        {            
            foreach (PlaylistViewModel i in allPlaylists)
            {
                i.IsAlive = false;
            }
        }
    }
}
