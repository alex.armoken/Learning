using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ThreadPlayer.ViewModels
{
    class BaseViewModel : INotifyPropertyChanged
    {
        protected string header;

        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public string Header
        {
            get { return header; }
            set
            {
                header = value;
                NotifyPropertyChanged("ID");
            }
        }
    }
}
