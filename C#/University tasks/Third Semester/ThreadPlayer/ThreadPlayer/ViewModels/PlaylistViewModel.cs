using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Windows.Input;
using ThreadPlayer.ViewModels;
using System.IO;
using System.Media;
using System.Threading;
using System.Diagnostics;

namespace ThreadPlayer.ViewModels
{
    class PlaylistViewModel : BaseViewModel
    {
        private int id;
        private string name;
        private bool isPlay;
        private Thread playThread;
        private TimeSpan currentTime;
        private Stopwatch currentAccurateTime;
        private long iterateBeginTime;
        private Composition selectedSong;
        private ObservableCollection<Composition> allSongs;

        public string ID
        {
            get { return id.ToString(); }
            private set {; }
        }
        public string Name
        {
            get { return name; }
            private set {; }
        }
        public string Length
        {
            get
            {
                TimeSpan totalLength = new TimeSpan();
                foreach (Composition i in allSongs)
                {
                    totalLength += TimeSpan.Parse(i.Length);
                }
                return totalLength.ToString();
            }
            private set {; }
        }
        public string Rating
        {
            get { return (Math.Round(allSongs.Average(i => Int32.Parse(i.Rating)))).ToString(); }
            private set {; }
        }
        public string Count
        {
            get { return allSongs.Count().ToString(); }
            private set {; }
        }

        public bool IsAlive { get; set; }
        public Composition SelectedSong
        {
            get { return selectedSong; }
            set
            {
                selectedSong = value;
                isPlay = true;
                iterateBeginTime = 0;
                CurrentTime = 0;
                currentAccurateTime.Restart();
                NotifyPropertyChanged(String.Empty);
            }
        }
        public ObservableCollection<Composition> AllSongs
        {
            get { return allSongs; }
            private set {; }
        }
        public int CurrentTime
        {
            get { return (int)Math.Round(currentTime.TotalSeconds); }
            set
            {
                int hours = value / 3600;
                int minutes = (value - hours * 3600) / 60;
                int seconds = ((value - hours * 3600) - minutes * 60);
                currentTime = new TimeSpan(hours, minutes, seconds);
                NotifyPropertyChanged(String.Empty);
            }
        }
        public string CurrentTimeStr
        {
            get { return currentTime.ToString(); }
            private set {; }
        }

        public PlaylistViewModel()
        {
            isPlay = false;
            IsAlive = true;
            currentTime = new TimeSpan();
            currentAccurateTime = new Stopwatch();
            playThread = new Thread(this.Playing);
            PlayCommand = new Command(arg => PlayClick());
            PauseCommand = new Command(arg => PauseClick());

            playThread.Start();
        }
        public PlaylistViewModel(ObservableCollection<Composition> allSongs, int id, string name)
        {
            this.allSongs = allSongs;
            this.id = id;
            this.name = name;
            header = name;
            isPlay = false;
            IsAlive = true;
            currentAccurateTime = new Stopwatch();
            currentTime = new TimeSpan();

            playThread = new Thread(this.Playing);
            PlayCommand = new Command(arg => PlayClick());
            PauseCommand = new Command(arg => PauseClick());

            playThread.Start();
        }

        public void StartThread()
        {
            playThread.Start();
        }

        public void StringWrite(StreamWriter writer)
        {
            writer.WriteLine(ID);
            writer.WriteLine(Name);
            writer.WriteLine(allSongs.Count.ToString());
            foreach (Composition i in allSongs)
            {
                writer.WriteLine(i.ID);
                writer.WriteLine(i.Title);
                writer.WriteLine(i.Artist);
                writer.WriteLine(i.Length);
                writer.WriteLine(i.Genre);
                writer.WriteLine(i.Rating);
            }
        }
        public void StringReader(StreamReader reader)
        {
            id = Int32.Parse(reader.ReadLine());
            name = reader.ReadLine();
            header = name;

            int count = Int32.Parse(reader.ReadLine());
            allSongs = new ObservableCollection<Composition>();
            for (int i = 0; i < count; i++)
            {
                int id = Int32.Parse(reader.ReadLine());
                string title = reader.ReadLine();
                string artist = reader.ReadLine();
                TimeSpan time = CompositionStrConversion.stringToLength(reader.ReadLine());
                Composition.Genres genre = CompositionStrConversion.stringToGenre(reader.ReadLine());
                int rating = Int32.Parse(reader.ReadLine());               

                allSongs.Add(new Composition(id, title, time, artist, genre, rating));
            }
        }

        public ICommand PlayCommand { get; set; }
        private void PlayClick()
        {
            if (SelectedSong == null)
            {
                if (allSongs.Count != 0)
                {
                    SelectedSong = allSongs[0];
                }
            }

            isPlay = true;
            iterateBeginTime = 0;
            currentAccurateTime.Start();
        }

        public ICommand PauseCommand { get; set; }
        private void PauseClick()
        {
            isPlay = false;
            currentAccurateTime.Stop();
        }     

        private void Playing()
        {
            while (IsAlive)
            {
                if (isPlay)
                {
                    int iterateEndTime = (int)(currentAccurateTime.ElapsedMilliseconds / 1000);
                    if (iterateEndTime - iterateBeginTime >= 1)
                    {
                        SystemSounds.Beep.Play();
                        iterateBeginTime = iterateEndTime;
                        CurrentTime = iterateEndTime;

                        if (CurrentTime == SelectedSong.LengthTimeSpan.TotalSeconds)
                        {
                            int indexOfSelectedSong = allSongs.IndexOf(selectedSong);

                            if (indexOfSelectedSong == allSongs.Count - 1)
                            {
                                Thread.Sleep(300);
                                SelectedSong = allSongs[0];
                                currentAccurateTime.Reset();
                                isPlay = false;
                            }
                            else
                            {
                                Thread.Sleep(300);
                                SelectedSong = allSongs[indexOfSelectedSong + 1];
                            }
                        }
                    }
                }
                Thread.Sleep(50);
            }
        }
    }
}
