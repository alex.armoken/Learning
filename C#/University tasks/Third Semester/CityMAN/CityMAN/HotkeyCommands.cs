﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.IO.Compression;
using Microsoft.Win32;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Reflection;
using LAN_MAN;
using MahApps.Metro.Controls;

namespace WpfApplication1
{
    public class HotkeyCommands
    {
        private static RoutedUICommand exit;
        private static RoutedUICommand remove;
        private static RoutedUICommand clear_all;
        private static RoutedUICommand clear_lan;
        private static RoutedUICommand set_stat_ip;
        private static RoutedUICommand unset_stat_ip;
        private static RoutedUICommand move_to;
        private static RoutedUICommand combine_lans;

        static HotkeyCommands()
        {
            InputGestureCollection inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.Q, ModifierKeys.Control, "Ctrl+Q"));
            exit = new RoutedUICommand("Exit", "Exit", typeof(HotkeyCommands), inputs);


            inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.D, ModifierKeys.Control, "Ctrl+D"));
            remove = new RoutedUICommand("Remove desktop", "Remove desktop", typeof(HotkeyCommands), inputs);

            inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.F, ModifierKeys.Control, "Ctrl+F"));
            clear_all = new RoutedUICommand("Clear all", "Clear all", typeof(HotkeyCommands), inputs);

            inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.G, ModifierKeys.Control, "Ctrl+G"));
            clear_lan = new RoutedUICommand("Clear selected LAN", "Clear selected LAN", typeof(HotkeyCommands), inputs);


            inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.T, ModifierKeys.Control, "Ctrl+T"));
            set_stat_ip = new RoutedUICommand("Set static IP", "Set static IP", typeof(HotkeyCommands), inputs);

            inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.U, ModifierKeys.Control, "Ctrl+U"));
            unset_stat_ip = new RoutedUICommand("Unset static IP", "Unset static IP", typeof(HotkeyCommands), inputs);


            inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.M, ModifierKeys.Control, "Ctrl+M"));
            move_to = new RoutedUICommand("Move to", "Move to", typeof(HotkeyCommands), inputs);

            inputs = new InputGestureCollection();
            inputs.Add(new KeyGesture(Key.K, ModifierKeys.Control, "Ctrl+K"));
            combine_lans = new RoutedUICommand("Combine LAN's", "Combine LAN's", typeof(HotkeyCommands), inputs);
        }

        public static RoutedUICommand Exit
        {
            get { return exit; }
        }

        public static RoutedUICommand Remove
        {
            get { return remove; }
        }

        public static RoutedUICommand Clear_All
        {
            get { return clear_all; }
        }

        public static RoutedUICommand Clear_LAN
        {
            get { return clear_lan; }
        }

        public static RoutedUICommand Set_Stat_IP
        {
            get { return set_stat_ip; }
        }

        public static RoutedUICommand Unset_Stat_IP
        {
            get { return unset_stat_ip; }
        }

        public static RoutedUICommand Move_To
        {
            get { return move_to; }
        }

        public static RoutedUICommand Combine_LANs
        {
            get { return combine_lans; }
        }
    }
}