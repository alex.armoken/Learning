﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.IO;
using System.IO.Compression;

namespace LAN_MAN
{
    public class IPAddress : IEquatable<IPAddress>
    {
        public byte A;
        public byte B;
        public byte C;
        public byte D;
        public bool Equals(IPAddress item)
        {
            if (item == null)
                return false;

            if (this.A == item.A && this.B == item.B && this.C == item.C && this.D == item.D)
                return true;
            else
                return false;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;

            IPAddress item = obj as IPAddress;
            if (item == null)
                return false;
            else
                return Equals(item);
        }

        public override int GetHashCode()
        {
            return A * 1000000000 + B * 1000000 + C * 1000 + D;
        }

        public override string ToString()
        {
            return A + "." + B + "." + C + "." + D;
        }

        public void BinWrite(BinaryWriter outStream)
        {
            outStream.Write(A);
            outStream.Write(B);
            outStream.Write(C);
            outStream.Write(D);
        }

        public void BinRead(BinaryReader inStream)
        {
            A = inStream.ReadByte();
            B = inStream.ReadByte();
            C = inStream.ReadByte();
            D = inStream.ReadByte();
        }

        public void StringWrite(StreamWriter outStream)
        {
            outStream.WriteLine("{0}.{1}.{2}.{3}", A, B, C, D);
        }

        public void StringRead(StreamReader inStream)
        {
            string newIP = inStream.ReadLine();

            string[] ipParts = newIP.Split('.');

            A = Byte.Parse(ipParts[0]);
            B = Byte.Parse(ipParts[1]);
            C = Byte.Parse(ipParts[2]);
            D = Byte.Parse(ipParts[3]);
        }
    }

    public class Desktop
    {
        private IPAddress ip;
        private string hostname;

        public Desktop(string hostname)
        {
            ip = new IPAddress();
            this.hostname = hostname;
        }

        public Desktop()
        {
            ip = new IPAddress();
        }

        public string IPString
        {
            get { return this.ip.ToString(); }
            private set {; }
        }

        public IPAddress IP
        {
            get { return ip; }
            set { ip = value; }
        }

        public string Hostname
        {
            get { return hostname; }
            private set {; }
        }

        public void BinWrite(BinaryWriter outStream)
        {
            ip.BinWrite(outStream);
            outStream.Write(hostname);
        }

        public void BinRead(BinaryReader inStream)
        {
            ip = new IPAddress();
            ip.BinRead(inStream);
            hostname = inStream.ReadString();
        }

        public void StringWrite(StreamWriter outStream)
        {
            ip.StringWrite(outStream);
            outStream.WriteLine(Hostname);
        }

        public void StringRead(StreamReader inStream)
        {
            ip.StringRead(inStream);
            hostname = inStream.ReadLine();
        }
    }

    public class LAN : IEnumerable<Desktop>, INotifyCollectionChanged
    {
        private IPAddress ip;
        private string hostname;
        private Dictionary<string, IPAddress> staticIP;
        private Dictionary<string, Desktop> workstations;

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public LAN()
        {
            ip = new IPAddress();
            staticIP = new Dictionary<string, IPAddress>();
            workstations = new Dictionary<string, Desktop>();
        }

        public LAN(string hostname, IPAddress ip)
        {
            this.ip = ip;
            this.hostname = hostname;
            staticIP = new Dictionary<string, IPAddress>();
            workstations = new Dictionary<string, Desktop>();
        }

        public string IPString
        {
            get { return this.ip.ToString(); }
            private set {; }
        }

        public IPAddress IP
        {
            get { return this.ip; }
            set { this.ip = value; }
        }

        public string Hostname
        {
            get { return this.hostname.ToString(); }
            private set {; }
        }

        public int Count { get { return workstations.Count; } }

        public IEnumerator<Desktop> GetEnumerator()
        {
            foreach (var i in workstations)
            {
                yield return i.Value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool Add(string hostname, Desktop item)
        {
            bool result = false;

            if (!workstations.ContainsKey(hostname))
            {
                if (staticIP.ContainsKey(item.Hostname))
                {
                    IPAddress ip;
                    staticIP.TryGetValue(item.Hostname, out ip);
                    item.IP = ip;
                    result = true;
                }
                else
                {

                    item.IP.A = 192;
                    item.IP.B = 168;
                    item.IP.C = this.ip.C;

                    bool isSetIP = true;
                    for (int i = 0; i < MaxCount + 1; i++)
                    {
                        isSetIP = true;
                        item.IP.D = (byte)i;

                        foreach (var j in workstations)//Is there this IP used
                        {
                            if (item.IP.D == j.Value.IP.D)
                            {
                                isSetIP = false;
                            }
                        }

                        foreach (var j in staticIP)//Is this IP set as static IP
                        {
                            if (item.IP.D == j.Value.D)
                            {
                                isSetIP = false;
                            }
                        }

                        if (isSetIP)
                        {
                            break;
                        }
                    }

                    if (isSetIP)
                    {
                        result = true;
                    }
                }

                if (result)
                {
                    workstations.Add(hostname, item);
                    CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                }
            }
            return result;
        }

        public bool Remove(string hostname)
        {
            bool isRemove = workstations.Remove(hostname);
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            return isRemove;
        }

        public void Clear()
        {
            workstations.Clear();
            staticIP.Clear();
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public byte MaxCount { get { return byte.MaxValue; } }

        public bool ContainsKey(string hostname)
        {
            return workstations.ContainsKey(hostname);
        }

        public bool TryGetValue(string hostname, out Desktop item)
        {
            return workstations.TryGetValue(hostname, out item);
        }

        public bool MoveTo(string hostname, LAN network)
        {
            Desktop item;
            bool isMoved = false;
            if (workstations.TryGetValue(hostname, out item))
            {
                if (!network.ContainsKey(hostname))
                {
                    if (Add(hostname, item))
                    {
                        Remove(hostname);
                        CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                        isMoved = true;
                    }
                }
            }
            return isMoved;
        }

        public void SetStaticIP(string hostname)
        {
            Desktop foundDesktop;
            if (workstations.TryGetValue(hostname, out foundDesktop))
            {
                staticIP.Add(foundDesktop.Hostname, foundDesktop.IP);
            }
        }

        public void UnsetStaticIP(string hostname)
        {
            if (workstations.ContainsKey(hostname) && staticIP.ContainsKey(hostname))
            {
                staticIP.Remove(hostname);
            }
        }

        public void BinWrite(BinaryWriter outStream)
        {
            ip.BinWrite(outStream);
            outStream.Write(hostname);

            outStream.Write(workstations.Count);
            foreach (var i in workstations)
            {
                i.Value.BinWrite(outStream);
            }

            outStream.Write(staticIP.Count);
            foreach (var i in staticIP)
            {
                outStream.Write(i.Key);
                i.Value.BinWrite(outStream);
            }
        }

        public void BinRead(BinaryReader inStream)
        {
            this.ip.BinRead(inStream);
            this.hostname = inStream.ReadString();

            int workstationsCount = inStream.ReadInt32();
            Desktop newDesktop;
            for (int i = 0; i < workstationsCount; i++)
            {
                newDesktop = new Desktop();
                newDesktop.BinRead(inStream);
                workstations.Add(newDesktop.Hostname, newDesktop);
            }

            int staticIPCount = inStream.ReadInt32();
            for (int i = 0; i < staticIPCount; i++)
            {
                string hostname = inStream.ReadString();
                IPAddress ip = new IPAddress();
                ip.BinRead(inStream);
                staticIP.Add(hostname, ip);
            }
        }

        public void StringWrite(StreamWriter outStream)
        {
            ip.StringWrite(outStream);
            outStream.WriteLine(hostname);

            outStream.WriteLine(workstations.Count);
            foreach (var i in workstations)
            {
                i.Value.StringWrite(outStream);
            }

            outStream.WriteLine(staticIP.Count);
            foreach (var i in staticIP)
            {
                outStream.WriteLine(i.Key);
                i.Value.StringWrite(outStream);
            }
        }

        public void StringRead(StreamReader inStream)
        {
            this.ip.StringRead(inStream);
            this.hostname = inStream.ReadLine();

            int workstationsCount = Int32.Parse(inStream.ReadLine());
            Desktop newDesktop;
            for (int i = 0; i < workstationsCount; i++)
            {
                newDesktop = new Desktop();
                newDesktop.StringRead(inStream);
                workstations.Add(newDesktop.Hostname, newDesktop);
            }

            int staticIPCount = Int32.Parse(inStream.ReadLine());
            for (int i = 0; i < staticIPCount; i++)
            {
                string hostname = inStream.ReadLine();
                IPAddress ip = new IPAddress();
                ip.StringRead(inStream);
                staticIP.Add(hostname, ip);
            }
        }
    }
    
    public class MAN : IEnumerable<LAN>, INotifyCollectionChanged
    {
        private Dictionary<string, LAN> localNetworks;

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public MAN()
        {
            localNetworks = new Dictionary<string, LAN>();
        }

        public IEnumerator<LAN> GetEnumerator()
        {
            foreach (var i in localNetworks)
            {
                yield return i.Value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count { get { return localNetworks.Count; } }

        public int MaxCount { get { return byte.MaxValue; } }

        public bool Add(string hostname, LAN item)
        {
            bool result = false;

            if (!localNetworks.ContainsKey(hostname))
            {
                IPAddress ip = new IPAddress();
                ip.A = 192;
                ip.B = 168;
                ip.D = 0;

                bool isSetIP = true;
                for (int i = 0; i < MaxCount + 1; i++)
                {
                    isSetIP = true;
                    ip.C = (byte)i;

                    foreach (var j in localNetworks)//Is there this IP used
                    {
                        if (ip.C == j.Value.IP.C)
                        {
                            isSetIP = false;
                        }
                    }

                    if (isSetIP)
                    {
                        break;
                    }
                }

                if (isSetIP)
                {
                    item.IP = ip;
                    localNetworks.Add(hostname, item);
                    CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                }
                result = isSetIP;
            }
            return result;
        }

        public bool Remove(string hostname)
        {
            if (localNetworks.ContainsKey(hostname))
            {
                localNetworks.Remove(hostname);
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Clear()
        {
            localNetworks.Clear();
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public bool ContainsKey(string hostname)
        {
            return localNetworks.ContainsKey(hostname);
        }

        public bool TryGetValue(string hostname, out LAN local)
        {
            return localNetworks.TryGetValue(hostname, out local);
        }

        public void Combine(string selectedLAN1, string selectedLAN2)
        {
            LAN foundLAN1;
            LAN foundLAN2;

            if (localNetworks.TryGetValue(selectedLAN1, out foundLAN1) && localNetworks.TryGetValue(selectedLAN2, out foundLAN2))
            {
                foreach (Desktop i in foundLAN2)
                {
                    foundLAN1.Add(i.Hostname, i);
                }
                foundLAN2.Clear();
                localNetworks.Remove(selectedLAN2);
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            }
        }

        public void BinWrite(BinaryWriter outStream)
        {
            outStream.Write(localNetworks.Count);
            foreach (var i in localNetworks)
            {
                i.Value.BinWrite(outStream);
            }
        }

        public void BinRead(BinaryReader inStream)
        {
            LAN newLAN;
            int count = inStream.ReadInt32();
            for (int i = 0; i < count; i++)
            {
                newLAN = new LAN();
                newLAN.BinRead(inStream);
                localNetworks.Add(newLAN.Hostname, newLAN);
            }
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public void StringWrite(StreamWriter outStream)
        {
            outStream.WriteLine(localNetworks.Count);
            foreach (var i in localNetworks)
            {
                i.Value.StringWrite(outStream);
            }
        }

        public void StringRead(StreamReader inStream)
        {
            LAN newLAN;
            int count = Int32.Parse(inStream.ReadLine());
            for (int i = 0; i < count; i++)
            {
                newLAN = new LAN();
                newLAN.StringRead(inStream);
                localNetworks.Add(newLAN.Hostname, newLAN);
            }
            CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }
    }
}
