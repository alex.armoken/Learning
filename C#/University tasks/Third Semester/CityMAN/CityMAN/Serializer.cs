﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.IO.Compression;
using Microsoft.Win32;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Reflection;
using System.Text.RegularExpressions;



class Foo
{
    public Bar bar;
}

class Bar
{
    public Foo foo;
}



class Serializer
{
    public FileStream stream = null;
    protected StreamWriter writer = null;
    protected StreamReader reader = null;
    protected int spaces = 0;
    protected int isRootTag = 0;
    protected bool isValueWrite = false;
    protected bool isEventWrite = false;

    public Serializer(string filename, bool filemode)
    {
        if (filemode)
        {
            stream = new FileStream(filename, FileMode.Create);
            writer = new StreamWriter(stream);
        }
        else
        {
            stream = new FileStream(filename, FileMode.Open);
            reader = new StreamReader(stream);
        }
    }

    protected void WriteLine(string line, bool goNextLine, bool isTag, bool isClosingTag)
    {
        if (isTag && !isValueWrite && spaces > 0)
        {
            writer.Write(new string(' ', spaces << 1));
        }

        if (isValueWrite)
            isValueWrite = false;

        if (isTag)
        {
            if (isClosingTag)
                writer.Write("</");
            else
            {
                writer.Write("<");
            }
        }

        line = line.Replace("&", "&amp;");
        line = line.Replace("<", "&lt;");
        line = line.Replace(">", "&gt;");
        line = line.Replace("'", "&apos;");
        line = line.Replace("\"", "&quot;");
        writer.Write(line);

        if (!isTag)
        {
            isValueWrite = true;
        }

        if (isTag)
        {
            if (isClosingTag)
            {
                writer.WriteLine(">");
            }
            else
            {
                if (goNextLine)
                {
                    writer.Write(">");
                    writer.WriteLine();
                }
                else
                {
                    writer.Write(">");
                }
            }
        }
    }

    public void WriteObject(object @object)
    {
        if (@object != null)
        {
            Type objtype = @object.GetType();

            if (objtype.IsArray || objtype.IsEnum)
            {
                spaces++;
                foreach (object item in ((System.Array)@object))
                {
                    if (item != null)
                    {
                        WriteLine((item.GetType()).Name, true, true, false);
                        WriteObject(item);
                        WriteLine((item.GetType()).Name, false, true, true);
                    }
                }
                spaces--;
            }
            else
            {
                if (isRootTag == 0)
                    WriteLine(objtype.FullName, true, true, false);
                isRootTag++;

                foreach (FieldInfo finfo in objtype.GetFields(BindingFlags.GetField | BindingFlags.SetField
                    | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
                {
                    try
                    {
                        if (!finfo.IsLiteral && !finfo.IsInitOnly &&
                            !finfo.IsNotSerialized && finfo.FieldType.BaseType != null &&
                            !finfo.FieldType.BaseType.Equals(typeof(System.MulticastDelegate)))

                        {
                            object primitiveObj = finfo.GetValue(@object);

                            if ((primitiveObj.GetType()).IsPrimitive || (primitiveObj.GetType()).Equals(typeof(string)))
                            {
                                spaces++;
                                WriteLine(finfo.Name, false, true, false);
                                WriteLine(primitiveObj.ToString(), false, false, false);
                                WriteLine(finfo.Name, false, true, true);
                                spaces--;
                            }
                            else
                            {
                                spaces++;
                                WriteLine(finfo.Name, true, true, false);
                                WriteObject(primitiveObj);
                                WriteLine(finfo.Name, true, true, true);
                                spaces--;
                            }
                        }
                    }
                    catch (NullReferenceException) { }
                }

                isRootTag--;
                if (isRootTag == 0)
                    WriteLine(objtype.FullName, false, true, true);
            }
        }
    }

    protected string ReadLine(out string key, bool useconvert)
    {
        string line;

        while ((line = reader.ReadLine()) != null)
        {
            if (line.Trim().Length > 0)
                break;
        }

        if (line != null)
        {
            int eqpos = (line = line.TrimStart(null)).IndexOf('=');

            if (eqpos > 0)
            {
                key = line.Substring(0, eqpos);
                line = line.Substring(eqpos + 1);
            }
            else key = null;

            if (useconvert)
            {
                line = line.Replace("&amp;", "&");
                line = line.Replace("&lt;", "<");
                line = line.Replace("&gt;", ">");
                line = line.Replace("&apos;", "'");
                line = line.Replace("&quot;", "\"");
            }
        }
        else key = null;

        return line;
    }

    protected object LoadObject(string classname)
    {
        object resobject = null;
        string key = null;
        string line;

        if (classname == null)
            classname = ReadLine(out key, true);

        if (classname == null)
            return null;

        if (key != null || ReadLine(out key, false) != "{" || key != null)
            throw new Exception("Bad header object signature");

        if ((line = ReadLine(out key, true)) != "}")
        {
            Type objtype = Type.GetType(classname);

            if (objtype.IsPrimitive)
            {
                if (key != "value")
                    throw new Exception("Bad value for primitive object");

                resobject = (object)objtype.InvokeMember("Parse",
                  BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.Public,
                  null, null, new object[] { line });
            }
            else if (objtype.Equals(typeof(string)))
            {
                if (key != "value")
                    throw new Exception("Bad value for primitive object");

                resobject = line;
            }
            else if (line == "[" && key == null)
            {
                if (objtype.IsArray)
                {
                    List<object> list = new List<object>();

                    while ((line = ReadLine(out key, true)) != "]" || key != null)
                        list.Add(LoadObject(line));

                    resobject = Activator.CreateInstance(objtype, list.Count);

                    for (int index = 0; index < list.Count; index++)
                        ((System.Array)resobject).SetValue(list[index], index);
                }
                else
                {
                    resobject = Activator.CreateInstance(objtype);

                    while ((line = ReadLine(out key, true)) != "]" || key != null)
                    {
                        if (line == null || key != null)
                            throw new Exception("Bad header field signature");

                        string fieldname = line;

                        if ((line = ReadLine(out key, true)) != "(" || key != null)
                            throw new Exception("Bad header object signature");

                        object fieldobj = LoadObject(null);
                        FieldInfo fiendinf = objtype.GetField(fieldname, BindingFlags.GetField
                            | BindingFlags.SetField | BindingFlags.NonPublic
                            | BindingFlags.Public | BindingFlags.Instance);

                        if (fiendinf != null)
                            fiendinf.SetValue(resobject, fieldobj);

                        if ((line = ReadLine(out key, true)) != ")" || key != null)
                            throw new Exception("Bad tail object signature");
                    }
                }
            }
            else throw new Exception("Bad header content signature");

            if ((line = ReadLine(out key, true)) != "}" || key != null)
                throw new Exception("Bad tail object signature");
        }
        else if (key != null)
            throw new Exception("Bad tail object signature");

        return resobject;
    }

    public object ReadObject()
    {
        return LoadObject(null);
    }

    public void Close()
    {
        if (writer != null)
        {
            writer.Close();
            writer = null;
        }

        if (reader != null)
        {
            reader.Close();
            reader = null;
        }

        if (stream != null)
        {
            stream.Close();
            stream = null;
        }
    }
}

static class Compression
{
    static public bool CompressFile(string pathToInitialFile, string pathToCompressedFile)
    {
        if (File.Exists(pathToInitialFile))
        {
            using (FileStream originalFileStream = File.Open(pathToInitialFile, FileMode.Open))
            {
                using (FileStream compressedFileStream = File.Create(pathToCompressedFile))
                {
                    using (GZipStream compressionStream = new GZipStream(compressedFileStream,
                       CompressionMode.Compress))
                    {
                        originalFileStream.CopyTo(compressionStream);
                    }
                }
            }
        }
        else
        {
            return false;
        }
        return true;
    }

    static public bool DecompressFile(string pathToCompressedFile, string pathToDecompressedFile)
    {
        if (File.Exists(pathToCompressedFile))
        {
            using (FileStream inFile = File.Open(pathToCompressedFile, FileMode.Open))
            {
                using (FileStream outFile = File.Create(pathToDecompressedFile))
                {
                    using (GZipStream Decompress = new GZipStream(inFile,
                            CompressionMode.Decompress))
                    {
                        Decompress.CopyTo(outFile);
                    }
                }
            }
        }
        else
        {
            return false;
        }
        return true;
    }
}