﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.IO.Compression;
using Microsoft.Win32;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using System.Reflection;
using System.Globalization;
using LAN_MAN;
using MahApps.Metro.Controls;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        MAN mMAN = new MAN();
        List<string> allHosts = new List<string>();
        bool whatIsSelect = false;
        bool isCompression = false;
        string filename;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            listbox0.ItemsSource = mMAN;

            App.LanguageChanged += LanguageChanged;

            CultureInfo currLang = App.Language;

            menuLanguage.Items.Clear();
            foreach (var lang in App.Languages)
            {
                MenuItem menuLang = new MenuItem();
                menuLang.Header = lang.DisplayName;
                menuLang.Tag = lang;
                menuLang.IsChecked = lang.Equals(currLang);
                menuLang.Click += ChangeLanguageClick;
                menuLanguage.Items.Add(menuLang);
            }

            logowindows.Visibility = Visibility.Hidden;
        }

        private void LanguageChanged(Object sender, EventArgs e)
        {
            CultureInfo currLang = App.Language;

            foreach (MenuItem i in menuLanguage.Items)
            {
                CultureInfo ci = i.Tag as CultureInfo;
                i.IsChecked = ci != null && ci.Equals(currLang);
            }

            if (currLang.Name == "en-US")
            {
                logotux.Visibility = Visibility.Visible;
                logowindows.Visibility = Visibility.Hidden;
            }
            else if (currLang.Name == "ru-RU")
            {
                logotux.Visibility = Visibility.Hidden;
                logowindows.Visibility = Visibility.Visible;
            }
        }

        private void ChangeLanguageClick(Object sender, EventArgs e)
        {
            MenuItem mi = sender as MenuItem;
            if (mi != null)
            {
                CultureInfo lang = mi.Tag as CultureInfo;
                if (lang != null)
                {
                    App.Language = lang;
                }
            }
        }


        private void Show_Something_Click(Object sender, EventArgs e)
        {
            if (canvas0.Visibility == Visibility.Visible)
            {
                canvas0.Visibility = Visibility.Hidden;
            }
            else if (canvas0.Visibility == Visibility.Hidden)
            {
                canvas0.Visibility = Visibility.Visible;
            }
        }

        private void Add_LAN_Click(object sender, RoutedEventArgs e)
        {
            string hostname = textBox0.Text;
            textBox0.Clear();

            IPAddress ip = new IPAddress();
            listbox0.UnselectAll();
            if (!mMAN.Add(hostname, new LAN(hostname, ip)))
            {
                MessageBox.Show("You can'y add new LAN!");
            }
        }

        private void Add_Desktop_Click(object sender, RoutedEventArgs e)
        {
            if (listbox0.SelectedItem != null)
            {
                LAN selectedLAN = (LAN)listbox0.SelectedItem;

                string hostname = textBox1.Text;
                textBox1.Clear();

                listbox1.UnselectAll();
                if (!selectedLAN.Add(hostname, new Desktop(hostname)))
                {
                    MessageBox.Show("You can't add this desktop to this LAN!");
                }
            }
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            LAN selectedLAN;
            Desktop selectedDesktop;

            if (!whatIsSelect && listbox0.SelectedItem != null)
            {
                selectedLAN = (LAN)listbox0.SelectedItem;
                listbox1.ItemsSource = null;
                mMAN.Remove(selectedLAN.Hostname);
            }
            else if (whatIsSelect && listbox1.SelectedItem != null)
            {
                selectedLAN = (LAN)listbox0.SelectedItem;
                selectedDesktop = (Desktop)listbox1.SelectedItem;
                selectedLAN.Remove(selectedDesktop.Hostname);
            }
        }

        private void Clear_LAN_Click(object sender, RoutedEventArgs e)
        {
            if (listbox0.SelectedItem != null)
            {
                LAN selectedLAN = (LAN)listbox0.SelectedItem;
                selectedLAN.Clear();
            }
        }

        private void Clear_MAN_Click(object sender, RoutedEventArgs e)
        {
            mMAN.Clear();
            listbox1.ItemsSource = null;
        }

        private void Set_Stat_IP_Click(object sender, RoutedEventArgs e)
        {
            if (listbox1.SelectedItem != null)
            {
                LAN selectedLAN = (LAN)listbox0.SelectedItem;
                Desktop selectedDesktop = (Desktop)listbox1.SelectedItem;
                selectedLAN.SetStaticIP(selectedDesktop.Hostname);
            }
        }

        private void Unset_Stat_IP_Click(object sender, RoutedEventArgs e)
        {
            if (listbox1.SelectedItem != null)
            {
                LAN selectedLAN = (LAN)listbox0.SelectedItem;
                Desktop selectedDesktop = (Desktop)listbox1.SelectedItem;
                selectedLAN.UnsetStaticIP(selectedDesktop.Hostname);
            }
        }

        private void Combine_LANs_Click(object sender, RoutedEventArgs e)
        {
            listbox1.ItemsSource = null;
            LAN selectedLAN1 = (LAN)listbox0.SelectedItem;
            LAN selectedLAN2;
            mMAN.TryGetValue(textBox0.Text, out selectedLAN2);

            if (selectedLAN1.Hostname != selectedLAN2.Hostname)
            {
                listbox0.UnselectAll();
                mMAN.Combine(selectedLAN1.Hostname, selectedLAN2.Hostname);
            }
        }

        private void Move_To_Click(object sender, RoutedEventArgs e)
        {
            if (listbox1.SelectedItem != null)
            {
                Desktop selectedDesktop = (Desktop)listbox1.SelectedItem;
                listbox1.UnselectAll();
                LAN selectedLAN1 = (LAN)listbox0.SelectedItem;

                LAN selectedLAN2;
                mMAN.TryGetValue(textBox0.Text, out selectedLAN2);
                selectedLAN1.MoveTo(selectedDesktop.Hostname, selectedLAN2);
            }
        }




        private void ListBox0_Selected(object sender, RoutedEventArgs e)
        {
            if (listbox0.SelectedItem != null)
            {
                LAN selectedLAN = (LAN)listbox0.SelectedItem;
                listbox1.ItemsSource = selectedLAN;
            }

            whatIsSelect = false;
        }

        private void ListBox1_Selected(object sender, SelectionChangedEventArgs e)
        {
            whatIsSelect = true;
        }

        private void ListBox0_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (listbox0.SelectedItem != null)
            {
                LAN selectedLAN = (LAN)listbox0.SelectedItem;
                textBox0.Text = selectedLAN.Hostname;
            }
        }

        private void Compression_Checkbox_Click(object sender, RoutedEventArgs e)
        {
            isCompression = (bool)menuCompressionCheckbox.IsChecked;
        }



        private void Bin_Save(string filename, MAN selectedMAN)
        {
            BinaryWriter writer = null;
            try
            {
                writer = new BinaryWriter(new FileStream(filename, FileMode.Create));
                selectedMAN.BinWrite(writer);
            }
            catch (IOException exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        private void Bin_Read(string filename, MAN selectedMAN)
        {
            BinaryReader reader = null;
            try
            {
                reader = new BinaryReader(new FileStream(filename, FileMode.Open));
                selectedMAN.BinRead(reader);
            }
            catch (IOException exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            listbox0.ItemsSource = mMAN;
        }

        private void String_Save(string filename, MAN selectedMAN)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(new FileStream(filename, FileMode.Create));
                mMAN.StringWrite(writer);
            }
            catch (IOException exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        private void String_Read(string filename, MAN selectedMAN)
        {
            StreamReader reader = null;
            try
            {
                reader = new StreamReader(new FileStream(filename, FileMode.Open));
                mMAN.StringRead(reader);
            }
            catch (IOException exc)
            {
                MessageBox.Show(exc.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            listbox0.ItemsSource = mMAN;
        }

        private void Serialize(string filename, MAN selectedMAN)
        {
            Bar bubu = new Bar();
            Foo fufu = new Foo() { bar = bubu };
            bubu.foo = fufu;

            Serializer sl = new Serializer(filename, true);
            sl.WriteObject(selectedMAN);
            sl.Close();
        }

        private void Deserialize(string filename, MAN selectedMAN)
        {
            mMAN.Clear();

            Serializer dsl = new Serializer(filename, false);
            mMAN = (MAN)dsl.ReadObject();
            dsl.Close();

            listbox0.ItemsSource = mMAN;
            listbox1.ItemsSource = null;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.DefaultExt = ".strh";
            dlg.Filter = "All file (*.*)|*.*|Text documents (strh)|*.strh|Binary documents (binh)|*binh|Xml like document(xmlh)|*xmlh";

            filename = null;
            if (dlg.ShowDialog() == true)
            {
                filename = dlg.FileName;
            }

            if (filename != null)
            {
                string[] fileExtensions = filename.Split('.');

                switch (fileExtensions[fileExtensions.Length - 1])
                {
                    case "strh":
                        String_Save(filename, mMAN);
                        break;
                    case "binh":
                        Bin_Save(filename, mMAN);
                        break;
                    case "xmlh":
                        Serialize(filename, mMAN);
                        break;
                }

                if (isCompression)
                {
                    string compressedFilename = filename + ".bubu";
                    Compression.CompressFile(filename, compressedFilename);
                    File.Delete(filename);
                }
            }
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            bool isCompressed = false;
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.DefaultExt = ".strh";
            dlg.Filter = "All file (*.*)|*.*|Text documents (strh)|*.strh|Binary documents (binh)|*binh|Xml like document(xmlh)|*xmlh|Archive(bubu)|*bubu";

            filename = null;
            if (dlg.ShowDialog() == true)
            {
                filename = dlg.FileName;
            }

            if (filename != null)
            {
                string[] fileExtensions = filename.Split('.');

                if (fileExtensions[fileExtensions.Length - 1] == "bubu")
                {
                    string decompressedFilename = filename.Replace(".bubu", "");
                    Compression.DecompressFile(filename, decompressedFilename);
                    filename = decompressedFilename;
                    isCompressed = true;
                }

                switch (fileExtensions[fileExtensions.Length - 1])
                {
                    case "strh":
                        String_Read(filename, mMAN);
                        break;
                    case "binh":
                        Bin_Read(filename, mMAN);
                        break;
                    case "xmlh":
                        Deserialize(filename, mMAN);
                        break;
                }

                if (isCompressed) File.Delete(filename);
            }
        }

        private void Exit_Application_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }



        private void Select_By_IP_Number_Click(object sender, RoutedEventArgs e)
        {
            byte ipNumber;
            try
            {
                ipNumber = Convert.ToByte(textBox2.Text);
            }
            catch (System.FormatException)
            {
                ipNumber = 0;
            }

            var selectedDesktops = from lan in mMAN
                                   from desktop in lan
                                   where desktop.IP.D == ipNumber
                                   select desktop;

            listbox2.ItemsSource = selectedDesktops;
        }

        private void Sort_All_Desktops_By_Hostname_Click(object sender, RoutedEventArgs e)
        {
            var sortedDesktops = from lan in mMAN
                                 from desktop in lan
                                 orderby desktop.Hostname
                                 select desktop;

            listbox2.ItemsSource = sortedDesktops;
        }

        private void Group_All_Desktops_By_IP_Number_Click(object sender, RoutedEventArgs e)
        {
            var groupedDesktops = from lan in mMAN
                                  from desktop in lan
                                  group desktop by desktop.IP.D;

            var selectedDesktops = from groups in groupedDesktops
                                   from desktops in groups
                                   select desktops;

            listbox2.ItemsSource = selectedDesktops;
        }

        private void Desktops_Hostname_Started_By_Click(object sender, RoutedEventArgs e)
        {
            var allDesktops = from lans in mMAN
                              from desktops in lans
                              select desktops;

            int desktopsCount = allDesktops.Count(s => s.Hostname.StartsWith(textBox3.Text));
            MessageBox.Show("Count of desktops, which hostnames started from \"" + textBox3.Text + "\":" + desktopsCount);
        }
    }
}