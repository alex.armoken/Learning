#include <windows.h>
#include <stdlib.h>
#include <tchar.h>
#include <map>
#include <string.h>
#include "resource.h"
#include "Main.h"

using namespace std;

const int timerID = 228;
const int bitmapCount = 4;

void WMPaintFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMTimerFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMButtonDownFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
typedef void(*WmFunc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

const map<UINT, WmFunc> WndProcTable{
		{WM_PAINT, &WMPaintFunc},
		{WM_TIMER, &WMTimerFunc},
		{WM_CREATE, &WMCreateFunc},
		{WM_DESTROY, &WMDestroyFunc},
		{WM_LBUTTONDOWN, &WMButtonDownFunc},
		{WM_RBUTTONDOWN, &WMButtonDownFunc}
};

POINT imagePos;
POINT cursorPos;
HANDLE hMutex;
HINSTANCE hInst;
int curBitmapNum;
HBITMAP bitmaps[4];
bool isWorking = false;


void DrawBitmap(HDC hDC, int x, int y, int width, int height, HBITMAP hBitmap)
{
	BITMAP bm;
	POINT  ptSize, ptOrg;

	auto hMemDC = CreateCompatibleDC(hDC);
	auto hOldbm = (HBITMAP)SelectObject(hMemDC, hBitmap);
	if (!hOldbm) return;

	SetMapMode(hMemDC, GetMapMode(hDC));
	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);
	ptSize.x = width == NULL ? bm.bmWidth : width;
	ptSize.y = height == NULL ? bm.bmHeight : height;
	DPtoLP(hDC, &ptSize, 1);
	ptOrg.x = 0;
	ptOrg.y = 0;
	DPtoLP(hMemDC, &ptOrg, 1);
	BitBlt(hDC, x, y, ptSize.x, ptSize.y, hMemDC, ptOrg.x, ptOrg.y, SRCCOPY);
	SelectObject(hMemDC, hOldbm);

	DeleteDC(hMemDC);
}

void ChangeState()
{
	POINT p;
	GetPhysicalCursorPos(&p);
	imagePos.x = imagePos.x > p.x ? imagePos.x - 1 : imagePos.x < p.x ? imagePos.x + 1 : imagePos.x;
	imagePos.y = imagePos.y > p.y ? imagePos.y - 1 : imagePos.y < p.y ? imagePos.y + 1 : imagePos.y;
	curBitmapNum = curBitmapNum != bitmapCount - 1 ? curBitmapNum + 1 : 0;
}

DWORD WINAPI DrawThreadFunc(LPVOID tParam)
{
	auto hMutex = (HANDLE)tParam;
	while (true)
	{
		WaitForSingleObject(hMutex, INFINITE);
		if (isWorking) ChangeState();
		ReleaseMutex(hMutex);
		Sleep(60);
	}
	return 0;
}

void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	SetTimer(hWnd, timerID, 60, NULL);
}

void DrawState(HWND hWnd, HDC hdc)
{
	RECT rect;
	GetWindowRect(hWnd, &rect);
	rect.top = 0;
	rect.left = 0;
	FillRect(hdc, &rect, CreateSolidBrush(RGB(255, 255, 255)));

	WaitForSingleObject(hMutex, INFINITE);
	DrawBitmap(hdc, imagePos.x, imagePos.y, NULL, NULL, bitmaps[curBitmapNum]);
	ReleaseMutex(hMutex);
}

void WMPaintFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	auto hdc = BeginPaint(hWnd, &ps);
	DrawState(hWnd, hdc);
	EndPaint(hWnd, &ps);
}

void WMButtonDownFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	WaitForSingleObject(hMutex, INFINITE);
	isWorking = !isWorking;
	ReleaseMutex(hMutex);
}

void WMTimerFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	WaitForSingleObject(hMutex, INFINITE);
	ScreenToClient(hWnd, &cursorPos);
	UpdateWindow(hWnd);
	InvalidateRect(hWnd, NULL, TRUE);
	ReleaseMutex(hMutex);
}

void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PostQuitMessage(0);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (WndProcTable.find(message) == WndProcTable.end())
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	WndProcTable.find(message)->second(hWnd, message, wParam, lParam);
	return 0;
}



WNDCLASSEX WndClassExInit()
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = _T("Number4 Class");
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	return wcex;
}

HWND HWNDInit()
{
	return CreateWindow(_T("Number4 Class"),
		_T("Number4 Title"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		800, 500, NULL,
		NULL, hInst, NULL);
}

void LoadBitmaps()
{
	bitmaps[0] = LoadBitmap(hInst, MAKEINTRESOURCE(IDI_BITMAP0));
	bitmaps[1] = LoadBitmap(hInst, MAKEINTRESOURCE(IDI_BITMAP1));
	bitmaps[2] = LoadBitmap(hInst, MAKEINTRESOURCE(IDI_BITMAP2));
	bitmaps[3] = LoadBitmap(hInst, MAKEINTRESOURCE(IDI_BITMAP3));
}

MSG MainMessageCycle()
{
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg;
}

void MainWindowInit(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance;
	RegisterClassEx(&WndClassExInit());
	auto hWnd = HWNDInit();
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MainWindowInit(hInstance, nCmdShow);
	LoadBitmaps();
	hMutex = CreateMutex(NULL, FALSE, NULL);
	auto thread = CreateThread(NULL, 0, DrawThreadFunc, hMutex, 0, NULL);
	auto msg = MainMessageCycle();
	SuspendThread(thread);
	return (int)msg.wParam;
}
