#include <map>
#include <set>
#include <time.h>
#include <vector>
#include <tchar.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <WinBase.h>
#include <functional>
#include <tlhelp32.h>

#define ID_RECT_BUTTON 228
#define ID_CIRC_BUTTON 229
#define ID_STAR_BUTTON 230
#define ID_DIAM_BUTTON 231

#define ID_RED_BUTTON 233
#define ID_BLUE_BUTTON 232
#define ID_GREEN_BUTTON 234

#define ID_DRAW_BUTTON 335
#define ID_NOT_DRAW_BUTTON 336

using namespace std;

HINSTANCE hInst;
HWND hDrawButton, hNotDrawButton;
HWND hBlueButton, hRedButton, hGreenButton;
HWND hRectButton, hCircButton, hStarButton, hDiamButton;
UINT customMessageFigure, customMessageColor, customMessageState;

void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMCommandFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
typedef void(*WMFunc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
const map<UINT, WMFunc> WndProcTable{
	{WM_CREATE, &WMCreateFunc},
	{WM_COMMAND, &WMCommandFunc},
	{WM_DESTROY, &WMDestroyFunc}
};


const set<UINT> FigureSet {ID_RECT_BUTTON, ID_CIRC_BUTTON, ID_STAR_BUTTON, ID_DIAM_BUTTON};
const set<UINT> ColorSet {ID_BLUE_BUTTON, ID_RED_BUTTON, ID_GREEN_BUTTON};


void CreateFigureButtons(HWND hWnd)
{
	hRectButton = CreateWindow(_T("BUTTON"), _T("Rectangle"),
		WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
		10, 10, 100, 20, hWnd, (HMENU)ID_RECT_BUTTON, hInst, NULL);
	hCircButton = CreateWindow(_T("BUTTON"), _T("Circle"),
		WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
		10, 30, 100, 20, hWnd, (HMENU)ID_CIRC_BUTTON, hInst, NULL);
	hStarButton = CreateWindow(_T("BUTTON"), _T("Star"),
		WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
		10, 50, 100, 20, hWnd, (HMENU)ID_STAR_BUTTON, hInst, NULL);
	hDiamButton = CreateWindow(_T("BUTTON"), _T("Diamond"),
		WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
		10, 70, 100, 20, hWnd, (HMENU)ID_DIAM_BUTTON, hInst, NULL);
}

void CreateColorButtons(HWND hWnd)
{
	hBlueButton = CreateWindow(_T("BUTTON"), _T("Blue"),
		WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
		10, 110, 100, 20, hWnd, (HMENU)ID_BLUE_BUTTON, hInst, NULL);
	hRedButton = CreateWindow(_T("BUTTON"), _T("Red"),
		WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
		10, 130, 100, 20, hWnd, (HMENU)ID_RED_BUTTON, hInst, NULL);
	hGreenButton = CreateWindow(_T("BUTTON"), _T("Green"),
		WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
		10, 150, 100, 20, hWnd, (HMENU)ID_GREEN_BUTTON, hInst, NULL);
}

void CreateDrawButtons(HWND hWnd)
{
	hDrawButton = CreateWindow(_T("BUTTON"), _T("Draw"),
		WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
		10, 190, 100, 20, hWnd, (HMENU)ID_DRAW_BUTTON, hInst, NULL);
	hNotDrawButton = CreateWindow(_T("BUTTON"), _T("Not Draw"),
		WS_CHILD | WS_VISIBLE | BS_RADIOBUTTON,
		10, 210, 100, 20, hWnd, (HMENU)ID_NOT_DRAW_BUTTON, hInst, NULL);
}

void SendMessages()
{
	SendMessage(hRectButton, BM_SETCHECK, true, 0);
	SendMessage(hBlueButton, BM_SETCHECK, true, 0);
	SendMessage(hDrawButton, BM_SETCHECK, true, 0);

	SendMessage(HWND_BROADCAST, customMessageFigure, 0, 1);
	SendMessage(HWND_BROADCAST, customMessageColor, 0, 1);
	SendMessage(HWND_BROADCAST, customMessageState, 0, 1);
}

void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	CreateFigureButtons(hWnd);
	CreateColorButtons(hWnd);
	CreateDrawButtons(hWnd);
	SendMessages();
}



void SendMessageToButton(HWND button, UINT number, UINT customMessage)
{
	SendMessage(button, BM_SETCHECK, true, number);
	SendMessage(HWND_BROADCAST, customMessage, 0, number);
}

void ColorCommandFunc(UINT param)
{
	SendMessage(hRedButton, BM_SETCHECK, false, 0);
	SendMessage(hGreenButton, BM_SETCHECK, false, 0);
	SendMessage(hBlueButton, BM_SETCHECK, false, 0);

	int number = param == ID_RED_BUTTON ? 1 : 
		param == ID_BLUE_BUTTON ? 2 : 3;
	HWND button = param == ID_RED_BUTTON ? hRedButton :
		param == ID_BLUE_BUTTON ? hBlueButton : hGreenButton;
	SendMessageToButton(button, number, customMessageColor);
}

void FigureCommandFunc(UINT param)
{
	SendMessage(hRectButton, BM_SETCHECK, false, 0);
	SendMessage(hCircButton, BM_SETCHECK, false, 0);
	SendMessage(hStarButton, BM_SETCHECK, false, 0);
	SendMessage(hDiamButton, BM_SETCHECK, false, 0);

	int number = param == ID_RECT_BUTTON ? 0 :
		param == ID_CIRC_BUTTON ? 1 :
		param == ID_STAR_BUTTON ? 2 : 3;
	HWND button = param == ID_RECT_BUTTON ? hRectButton :
		param == ID_CIRC_BUTTON ? hCircButton :
		param == ID_STAR_BUTTON ? hStarButton : hDiamButton;
	SendMessageToButton(button, number, customMessageFigure);
}

void DrawStateCommandFunc(UINT param)
{
	SendMessage(hDrawButton, BM_SETCHECK, false, 0);
	SendMessage(hNotDrawButton, BM_SETCHECK, false, 0);

	int number = param == ID_DRAW_BUTTON ? 1 : 2;
	HWND button = param == ID_DRAW_BUTTON ? hDrawButton : hNotDrawButton;
	SendMessageToButton(button, number, customMessageState);
}

void WMCommandFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	UINT param = LOWORD(wParam);
	if (FigureSet.find(param) != FigureSet.end())
		FigureCommandFunc(param);
	else if (ColorSet.find(param) != ColorSet.end())
		ColorCommandFunc(param);
	else if (param == ID_DRAW_BUTTON || param == ID_NOT_DRAW_BUTTON)
		DrawStateCommandFunc(param);
}



void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PostQuitMessage(0);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (WndProcTable.find(message) == WndProcTable.end())
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	WndProcTable.find(message)->second(hWnd, message, wParam, lParam);
	return 0;
}



WNDCLASSEX WndClassExInit()
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = _T("Number5A Class");
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	return wcex;
}

HWND HWNDInit()
{
	return CreateWindow(_T("Number5A Class"),
		_T("Number5A Title"),
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
		150, 300, NULL, NULL, hInst, NULL);
}

void MainWindowInit(HINSTANCE hInstance, int nCmdShow)
{
	RegisterClassEx(&WndClassExInit());
	hInst = hInstance;
	auto hWnd = HWNDInit();
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
}

MSG MainCycle()
{
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg;
}

void RegisterMessages()
{
	customMessageFigure = RegisterWindowMessage(L"MyCustomMessageFigure");
	customMessageColor = RegisterWindowMessage(L"MyCustomMessageColor");
	customMessageState = RegisterWindowMessage(L"MyCustomMessageState");
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MainWindowInit(hInstance, nCmdShow);
	RegisterMessages();
	return (int)MainCycle().wParam;
}
