#include <windows.h>
#include <stdlib.h>
#include <tchar.h>
#include <string.h>

#define ID_ADD_BUTTON 317
#define ID_CLEAR_BUTTON 319
#define ID_TRANS_BUTTON 321
#define ID_DEL_BUTTON 323
#define ID_LISTBOX1 325
#define ID_LISTBOX2 327
#define ID_LABEL 328
#define ID_EDIT 329

HINSTANCE hInst;
HWND hAddButton, hClButton, hTransButton, hDelButton, hList1, hList2, hLabel, hEdit;
TCHAR buf[128];
int index = 0;
bool mark = false;

void WMCreateFunc(HINSTANCE hInst, HWND hWnd)
{
	hAddButton = CreateWindow(_T("BUTTON"), _T("Add Text"), WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
		10, 10, 80, 30, hWnd, (HMENU)ID_ADD_BUTTON, hInst, NULL);
	hClButton = CreateWindow(_T("BUTTON"), _T("Delete All"), WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
		100, 10, 80, 30, hWnd, (HMENU)ID_CLEAR_BUTTON, hInst, NULL);
	hTransButton = CreateWindow(_T("BUTTON"), _T("Swap"), WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
		190, 10, 80, 30, hWnd, (HMENU)ID_TRANS_BUTTON, hInst, NULL);
	hTransButton = CreateWindow(_T("BUTTON"), _T("Delete"), WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
		280, 10, 80, 30, hWnd, (HMENU)ID_DEL_BUTTON, hInst, NULL);

	hLabel = CreateWindow(_T("EDIT"), _T("Text: "), WS_CHILD | WS_VISIBLE | WS_BORDER | ES_READONLY,
		10, 50, 50, 20, hWnd, (HMENU)ID_LABEL, hInst, NULL);
	hEdit = CreateWindow(_T("EDIT"), _T(""), WS_CHILD | WS_VISIBLE | WS_BORDER,
		50, 50, 320, 20, hWnd, (HMENU)ID_EDIT, hInst, NULL);

	hList1 = CreateWindow(_T("LISTBOX"), _T(""), WS_CHILD | WS_VISIBLE | WS_BORDER,
		10, 80, 160, 300, hWnd, (HMENU)ID_LISTBOX1, hInst, NULL);
	hList2 = CreateWindow(_T("LISTBOX"), _T(""), WS_CHILD | WS_VISIBLE | WS_BORDER,
		180, 80, 160, 300, hWnd, (HMENU)ID_LISTBOX2, hInst, NULL);
}

void AddButtonCommand(HWND hWnd)
{
	GetDlgItemText(hWnd, ID_EDIT, buf, 128);
	SetDlgItemText(hWnd, ID_EDIT, (LPWSTR)_T(""));

	mark = false;
	for (int i = 0; buf[i] != _T('\0'); i++)
	{
		if (buf[i] != _T(' '))
		{
			mark = true;
			break;
		}
	}

	if (mark && SendMessage(hList1, LB_FINDSTRINGEXACT, 0, (LPARAM)buf) == LB_ERR)
	{
		SendMessage(hList1, LB_ADDSTRING, 0, (LPARAM)buf);
	}
}

void TransButtonCommand()
{
	index = SendMessage(hList1, LB_GETCURSEL, 0, 0);
	if (index != LB_ERR)
	{
		SendMessage(hList1, LB_GETTEXT, index, (LPARAM)buf);
		SendMessage(hList1, LB_DELETESTRING, index, 0);
		SendMessage(hList2, LB_ADDSTRING, 0, (LPARAM)buf);
	}
}

void DelButtonCommand()
{
	index = SendMessage(hList1, LB_GETCURSEL, 0, 0);
	if (index != LB_ERR) SendMessage(hList1, LB_DELETESTRING, index, 0);
	index = SendMessage(hList2, LB_GETCURSEL, 0, 0);
	if (index != LB_ERR) SendMessage(hList2, LB_DELETESTRING, index, 0);
}

void WMCommandFunc(HINSTANCE hInst, HWND hWnd, WPARAM wParam)
{
	switch (LOWORD(wParam))
	{
	case ID_ADD_BUTTON:
		AddButtonCommand(hWnd);
		break;
	case ID_CLEAR_BUTTON:
		SendMessage(hList1, LB_RESETCONTENT, 0, 0);
		SendMessage(hList2, LB_RESETCONTENT, 0, 0);
		break;
	case ID_TRANS_BUTTON:
		TransButtonCommand();
		break;
	case ID_DEL_BUTTON:
		DelButtonCommand();
		break;
	}
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
		WMCreateFunc(hInst, hWnd);
		break;
	case WM_COMMAND:
		WMCommandFunc(hInst, hWnd, wParam);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}
	return 0;
}

WNDCLASSEX RegisterWndClassEx()
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = _T("Second Lab Class");
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	return wcex;
}

HWND HWNDInit()
{
	return CreateWindow(
		_T("Second Lab Class"),
		_T("Second Lab Title"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT,
		390, 400,
		NULL,
		NULL,
		hInst,
		NULL
	);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInst = hInstance;
	auto wcex = RegisterWndClassEx();
	if (!RegisterClassEx(&wcex))
	{
		MessageBox(NULL, _T("Call to RegisterClassEx failed!"),	_T("Win32 Guided Tour"), NULL);
		return 1;
	}

	auto hWnd = HWNDInit();
	if (!hWnd)
	{
		MessageBox(NULL, _T("Call to CreateWindow failed!"), _T("Win32 Guided Tour"), NULL);
		return 1;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)msg.wParam;
}