#include <windows.h>
#include <stdlib.h>
#include <tchar.h>
#include <string.h>
#include "resource.h"

const int ID_BUTTON1 = 228;
const int ID_BUTTON2 = 1488;

HINSTANCE hInst;
HWND hButton;

static bool isDraw = false;

void DrawBitmap(HDC hDC, int x, int y, HBITMAP hBitmap)
{
	BITMAP bm;
	POINT  ptSize, ptOrg;

	auto hMemDC = CreateCompatibleDC(hDC);
	auto hOldbm = (HBITMAP)SelectObject(hMemDC, hBitmap);
	if (!hOldbm) return;

	SetMapMode(hMemDC, GetMapMode(hDC));
	GetObject(hBitmap, sizeof(BITMAP), (LPSTR)&bm);
	ptSize.x = bm.bmWidth;
	ptSize.y = bm.bmHeight;
	DPtoLP(hDC, &ptSize, 1);
	ptOrg.x = 0;
	ptOrg.y = 0;
	DPtoLP(hMemDC, &ptOrg, 1);
	BitBlt(hDC, x, y, ptSize.x, ptSize.y, hMemDC, ptOrg.x, ptOrg.y, SRCCOPY);
	SelectObject(hMemDC, hOldbm);

	DeleteDC(hMemDC);
}

void DrawButton(LPDRAWITEMSTRUCT lpInfo)
{
	if (lpInfo->CtlType != ODT_BUTTON) return;
	int ResourceID = lpInfo->CtlID == ID_BUTTON1 ? IDB_BUTTON1UP : lpInfo->CtlID == ID_BUTTON2 ? IDB_BUTTON2UP : 0;
	if (ResourceID == 0) return;
	auto hbm = LoadBitmap(hInst, MAKEINTRESOURCE(ResourceID));
	DrawBitmap(lpInfo->hDC, (lpInfo->rcItem).left, (lpInfo->rcItem).top, hbm);
	DeleteObject(hbm);
}

void WMCreateFunc(HWND hWnd)
{
	hButton = CreateWindow(_T("BUTTON"), _T(""), WS_CHILD | WS_VISIBLE | BS_OWNERDRAW,
		20, 20, 48, 48, hWnd, (HMENU)ID_BUTTON1, hInst, NULL);
	hButton = CreateWindow(_T("BUTTON"), _T(""), WS_CHILD | WS_VISIBLE | BS_OWNERDRAW,
		20, 70, 48, 48, hWnd, (HMENU)ID_BUTTON2, hInst, NULL);
}

void DrawMountain(HDC hdc)
{
	POINT PT[5];
	PT[0].x = 300; PT[0].y = 300;
	PT[1].x = 325; PT[1].y = 275;
	PT[2].x = 350; PT[2].y = 150;
	PT[3].x = 375; PT[3].y = 250;
	PT[4].x = 400; PT[4].y = 300;
	Polygon(hdc, PT, 7);
 }

void DrawTrees(HDC hdc)
{
	auto hBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(153, 76, 0)));
	Rectangle(hdc, 270, 210, 350, 360);
	hBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(0, 153, 0)));
	Ellipse(hdc, 350, 120, 755, 240);
	hBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(153, 76, 0)));
	Rectangle(hdc, 800, 270, 820, 420);
	hBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(0, 153, 0)));
	Ellipse(hdc, 765, 180, 855, 300);
	hBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(153, 76, 0)));
	Rectangle(hdc, 900, 200, 920, 350);
	hBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(0, 153, 0)));
	Ellipse(hdc, 865, 110, 955, 230);
}

void DrawBackground(HDC hdc)
{
	// Sky
	auto hBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(51, 255, 255)));
	Rectangle(hdc, 100, 0, 800, 250);

	// Sand
	hBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(255, 255, 0)));
	Rectangle(hdc, 100, 250, 800, 500);
}

void DrawMoon(HDC hdc)
{
	auto hPen = CreatePen(PS_SOLID, 7, RGB(255, 255, 0));
	SelectObject(hdc, hPen);
	auto hBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(255, 255, 0)));
	Ellipse(hdc, 300, 50, 200, 100);
}

void DrawGuy(HDC hdc)
{
	auto hBrush = (HBRUSH)SelectObject(hdc, CreateSolidBrush(RGB(255, 218, 185)));
	Ellipse(hdc, 275, 275, 375, 375);
	Chord(hdc, 275, 375, 300, 400, 300, 375, 275, 400);
}

void DrawPicture(HDC hdc)
{
	DrawBackground(hdc);
	DrawMoon(hdc);
	DrawTrees(hdc);
	DrawMountain(hdc);
	DrawGuy(hdc);
}

void WMPaintFunc(HWND hWnd)
{
	PAINTSTRUCT ps;
	RECT rect = { 0, 0, 800, 500 };
	HBRUSH hWhiteBrush = CreateSolidBrush(RGB(255, 255, 255));
	HBRUSH hBlackBrush = CreateSolidBrush(RGB(0, 0, 0));

	auto hdc = BeginPaint(hWnd, &ps);
	if (isDraw)	DrawPicture(hdc);
	else FillRect(hdc, &rect, hWhiteBrush);
	EndPaint(hWnd, &ps);
}

void WMCommandFunc(HWND hWnd, WPARAM wParam)
{
	switch (LOWORD(wParam))
	{
	case ID_BUTTON1:
		isDraw = true;
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	case ID_BUTTON2:
		isDraw = false;
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	}
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE:
		WMCreateFunc(hWnd);
		break;
	case WM_DRAWITEM:
		DrawButton((LPDRAWITEMSTRUCT)lParam);
		break;
	case WM_PAINT:
		WMPaintFunc(hWnd);
		break;
	case WM_COMMAND:
		WMCommandFunc(hWnd, wParam);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
		break;
	}
	return 0;
}

WNDCLASSEX WndClassExInit()
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = _T("Number3 Class");
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	return wcex;
}

HWND HWNDInit()
{
	return CreateWindow(_T("Number3 Class"),
		_T("Number3 Title"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		800, 500, NULL,
		NULL, hInst, NULL);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInst = hInstance;
	RegisterClassEx(&WndClassExInit());
	auto hWnd = HWNDInit();

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)msg.wParam;
}
