#include <map>
#include <vector>
#include <tchar.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <windows.h>

#define ID_START_BUTTON 228
#define ID_PAUSE_BUTTON 265

#define M_PI 3.14159

using namespace std;

typedef struct stick
{
	HWND hWnd;
	HANDLE hMutex;
	HANDLE hEvent;
	INT sleepTime;

	POINT pCenter;
	POINT *pVertices;
	const INT verticesCount = 4;

	stick() {};
	stick(HWND wnd, HANDLE mutex, HANDLE event, INT time, POINT center, POINT *vertices) :
		hWnd(wnd), hMutex(mutex), hEvent(event), sleepTime(time), pCenter(center), pVertices(vertices) {};
} MyStick;

MyStick* stick1;
MyStick* stick2;

void WMPaintFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMTimerFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMCommandFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
typedef void(*WmFunc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

const map<UINT, WmFunc> WndProcTable{
		{WM_PAINT, &WMPaintFunc},
		{WM_CREATE, &WMCreateFunc},
		{WM_DESTROY, &WMDestroyFunc},
		{WM_COMMAND, &WMCommandFunc}
};

HINSTANCE hInst;
HWND StartButton;
HWND PauseButton;

HANDLE hMutex;
HANDLE workEvent;
HANDLE stickThread1;
HANDLE stickThread2;

void RotatePoint(POINT *rotatePoint, POINT *centerPoint, float angle)
{
	float s = sin(angle);
	float c = cos(angle);

	rotatePoint->x -= centerPoint->x;
	rotatePoint->y -= centerPoint->y;

	float newX = rotatePoint->x * c - rotatePoint->y * s;
	float newY = rotatePoint->x * s + rotatePoint->y * c;

	rotatePoint->x = newX + centerPoint->x;
	rotatePoint->y = newY + centerPoint->y;
}

void ChangeState(MyStick *stick)
{
	for (int i = 0; i < stick->verticesCount; i++)
	{
		RotatePoint(&(stick->pVertices[i]), &(stick->pCenter), 0.1);
	}
}

DWORD WINAPI DrawThreadFunc(LPVOID tParam)
{
	MyStick *stick = (MyStick*)tParam;
	while (WaitForSingleObjectEx(stick->hEvent, 0, TRUE) != WAIT_TIMEOUT)
	{
		WaitForSingleObject(stick->hMutex, INFINITE);
		ChangeState(stick);
		InvalidateRect(stick->hWnd, NULL, TRUE);
		ReleaseMutex(stick->hMutex);
		Sleep(stick->sleepTime);
	}
	ExitThread(0);
}

void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	StartButton = CreateWindow(_T("BUTTON"), _T("Start"),
		WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
		10, 10, 80, 30, hWnd, (HMENU)ID_START_BUTTON, hInst, NULL);
	PauseButton = CreateWindow(_T("BUTTON"), _T("Pause"),
		WS_CHILD | WS_VISIBLE | BS_DEFPUSHBUTTON,
		100, 10, 80, 30, hWnd, (HMENU)ID_PAUSE_BUTTON, hInst, NULL);
}

void FillWindowWhite(HWND hWnd, HDC hdc)
{
	RECT rect;
	GetWindowRect(hWnd, &rect);
	rect.top = 50;
	rect.left = 0;
	FillRect(hdc, &rect, CreateSolidBrush(RGB(255, 255, 255)));
}

void DrawState(HWND hWnd, HDC hdc, MyStick *stick)
{
	WaitForSingleObject(hMutex, INFINITE);
	Polygon(hdc, stick->pVertices, stick->verticesCount);
	ReleaseMutex(hMutex);
}

void WMPaintFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	FillWindowWhite(hWnd, hdc);

	HBRUSH brush = CreateSolidBrush(RGB(255, 0, 0));
	SelectObject(hdc, brush);
	DrawState(hWnd, hdc, stick1);
	DrawState(hWnd, hdc, stick2);
	DeleteObject(brush);

	EndPaint(hWnd, &ps);
}

void StartThreads()
{
	ResumeThread(stickThread1);
	ResumeThread(stickThread2);
}

void PauseThreads()
{
	SuspendThread(stickThread1);
	SuspendThread(stickThread2);
}

void WMCommandFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (LOWORD(wParam) == ID_START_BUTTON)
	{
		StartThreads();
	}
	else if (LOWORD(wParam) == ID_PAUSE_BUTTON)
	{
		PauseThreads();
	}
}

void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PostQuitMessage(0);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (WndProcTable.find(message) == WndProcTable.end())
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	WndProcTable.find(message)->second(hWnd, message, wParam, lParam);
	return 0;
}


WNDCLASSEX WndClassExInit()
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = _T("Number4 Class");
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	return wcex;
}

HWND HWNDInit()
{
	return CreateWindow(_T("Number4 Class"),
		_T("Number4 Title"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		800, 500, NULL,
		NULL, hInst, NULL);
}

MSG MainMessageCycle()
{
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg;
}

HWND WindowInit(HINSTANCE hInstance)
{
	hInst = hInstance;
	RegisterClassEx(&WndClassExInit());
	auto hWnd = HWNDInit();
	return hWnd;
}

POINT* PointsInit(INT x, INT y, INT height, INT width)
{
	POINT *vertices = new POINT[4];
	vertices[0].x = x; vertices[0].y = y;
	vertices[1].x = x + width; vertices[1].y = y;
	vertices[2].x = x + width; vertices[2].y = y + height;
	vertices[3].x = x; vertices[3].y = y + height;
	return vertices;
}

MyStick* StickInit(HWND hWnd, HANDLE hMutex, HANDLE hEvent, INT time, INT x, INT y, INT height, INT width)
{
	POINT *vertices = PointsInit(x, y, height, width);
	POINT center = POINT();
	center.x = int(x + width / 2);
	center.y = int(y + height / 2);
	return new MyStick(hWnd, hMutex, hEvent, time, center, vertices);
}

void SticksAndRotatingThreadsInit(HWND hWnd)
{
	hMutex = CreateMutex(NULL, FALSE, NULL);
	workEvent = CreateEvent(NULL, TRUE, TRUE, NULL);

	stick1 = StickInit(hWnd, hMutex, workEvent, 60, 200, 100, 100, 150);
	stick2 = StickInit(hWnd, hMutex, workEvent, 180, 500, 100, 50, 150);

	stickThread1 = CreateThread(NULL, 0, DrawThreadFunc, stick1, CREATE_SUSPENDED, NULL);
	stickThread2 = CreateThread(NULL, 0, DrawThreadFunc, stick2, CREATE_SUSPENDED, NULL);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HWND hWnd = WindowInit(hInstance);
	SticksAndRotatingThreadsInit(hWnd);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	auto msg = MainMessageCycle();

	ResetEvent(workEvent);
	return (int)msg.wParam;
}
