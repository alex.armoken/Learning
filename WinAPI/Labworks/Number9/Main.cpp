#include <map>
#include <set>
#include <tchar.h>
#include <windows.h>
#include <tlhelp32.h>

#define ID_MY_LISTBOX 113
#define ID_MY_BUTTON_CLEAR 117
#define ID_MY_BUTTON_SEARCH 115
#define	ID_MY_BUTTON_TERMINATE 119

#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383

using namespace std;

void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMCommandFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMContextMenuFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
typedef void(*WmFunc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
const map<UINT, WmFunc> WndProcTable
{
	{WM_CREATE, &WMCreateFunc},
	{WM_DESTROY, &WMDestroyFunc},
	{WM_COMMAND, &WMCommandFunc},
};

void ClearCommand(HWND hWnd);
void SearchCommand(HWND hWnd);
typedef void(*CommandFunc)(HWND hWnd);
const map<UINT, CommandFunc> CommandTable
{
	{ID_MY_BUTTON_CLEAR, &ClearCommand},
	{ID_MY_BUTTON_SEARCH, &SearchCommand},
};

HINSTANCE hInst;

HWND hList;
HWND hButtonClear;
HWND hButtonSearch;

HANDLE hBackgroundThread;
bool isThreadFinished = false;
bool isThreadSuspended = false;

int filesCount = 25;
TCHAR PathTemplate[3] = _T(":\\");


void UpdateMyRegListbox(HWND hWnd, TCHAR *chars, LPCWSTR subKey)
{
	if (_tcslen(chars) == 0) return;
	if (GetFileAttributes(chars) == INVALID_FILE_ATTRIBUTES)
	{
		TCHAR kek[MAX_KEY_LENGTH + 20 + 256];
		_stprintf(kek, _T("%s%s%s"), (TCHAR*)subKey, _T("   ---   "), chars);

		int index = SendDlgItemMessage(hWnd, ID_MY_LISTBOX, LB_ADDSTRING, 0, (LPARAM)kek);
		SendDlgItemMessage(hWnd, ID_MY_LISTBOX, LB_SETITEMDATA, (WPARAM)index, (LPARAM)kek);
	}
}

void GetInfoFromCurRegKey(HWND hWnd, HKEY hSubKey, LPCWSTR subKey)
{
	DWORD type = 0;
	DWORD index = 0;
	BYTE value[MAX_KEY_LENGTH];
	DWORD valLength = MAX_KEY_LENGTH;
	TCHAR valueBuffer[MAX_KEY_LENGTH];
	DWORD valueLength = MAX_KEY_LENGTH;

	while (RegEnumValue(hSubKey, index++, valueBuffer,
		&valueLength, NULL, &type, value, &valLength) == ERROR_SUCCESS)
	{
		TCHAR* chars = (TCHAR*)value;
		if (valueLength > 3 &&
			(type == REG_SZ || type == REG_EXPAND_SZ || type == REG_MULTI_SZ) &&
			chars[1] == PathTemplate[0] && chars[2] == PathTemplate[1])
		{
			UpdateMyRegListbox(hWnd, chars, subKey);
		}

		valueLength = MAX_KEY_LENGTH;
	}
}



void ProcessRegKey(HWND hWnd, HKEY prevKey, LPCWSTR subKey)
{
	HKEY hSubKey;
	if (RegOpenKeyEx(prevKey, subKey, 0, KEY_ALL_ACCESS, &hSubKey) == ERROR_SUCCESS)
	{
		int index = 0;
		DWORD length = MAX_KEY_LENGTH;
		TCHAR nameBuffer[MAX_KEY_LENGTH];

		// Process subkeys
		while (RegEnumKeyEx(hSubKey, index++, nameBuffer, &length,
			NULL, NULL, NULL, NULL) != ERROR_NO_MORE_ITEMS)
		{
			ProcessRegKey(hWnd, hSubKey, nameBuffer);
			length = MAX_KEY_LENGTH;
		}

		GetInfoFromCurRegKey(hWnd, hSubKey, subKey);
		RegCloseKey(hSubKey);
	}
}

DWORD WINAPI RegKeyProcessThread(LPVOID lpParam)
{
	ProcessRegKey((HWND)lpParam, HKEY_LOCAL_MACHINE, NULL);
	isThreadFinished = true;
	ShowScrollBar(hList, SB_HORZ, true);
	return 0;
}



void SearchCommand(HWND hWnd)
{
	hBackgroundThread = CreateThread(NULL, 0, &RegKeyProcessThread, hWnd, 0, NULL);
	isThreadFinished = false;
	isThreadSuspended = false;
	int lError = GetLastError();
	SetTimer(hWnd, 1, 500, NULL);
	if (hBackgroundThread == NULL)
	{
		MessageBox(hWnd, _T("Error opening search thread"), MB_OK, NULL);
	}
}

void ClearCommand(HWND hWnd)
{
	if (hBackgroundThread != nullptr)
	{
		TerminateThread(hBackgroundThread, 0);
	}
	isThreadSuspended = true;
	isThreadFinished = false;

	SendDlgItemMessage(hWnd, ID_MY_LISTBOX, LB_RESETCONTENT, NULL, NULL);
	ResumeThread(hBackgroundThread);
	isThreadSuspended = false;
}

void WMCommandFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (CommandTable.find(LOWORD(wParam)) != CommandTable.end())
	{
		CommandTable.find(LOWORD(wParam))->second(hWnd);
	}
}



void CreateControls(HWND hWnd)
{
	hButtonSearch = CreateWindow(_T("BUTTON"), _T("Search"),
		WS_VISIBLE | WS_CHILD | WS_BORDER, 20, 20, 120, 25,
		hWnd, (HMENU)ID_MY_BUTTON_SEARCH, NULL, NULL);

	hButtonClear = CreateWindow(_T("BUTTON"), _T("Clear results"),
		WS_VISIBLE | WS_CHILD | WS_BORDER, 150, 20, 120, 25,
		hWnd, (HMENU)ID_MY_BUTTON_CLEAR, NULL, NULL);

	hList = CreateWindow(_T("LISTBOX"), NULL,
		WS_VISIBLE | WS_CHILD | WS_BORDER | WS_VSCROLL | WS_HSCROLL,
		20, 50, 1450, 450, hWnd, (HMENU)ID_MY_LISTBOX, NULL, NULL);
}

void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	CreateControls(hWnd);
}



void WMTimerFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (isThreadFinished)
	{
		InvalidateRect(hWnd, 0, true);
	}
}

void WMPaintFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	if (isThreadFinished == 1)
	{
		TextOut(hdc, 50, 50, _T("Search Finished"), 16);
	}
	EndPaint(hWnd, &ps);
}

void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	CloseHandle(hBackgroundThread);
	PostQuitMessage(NULL);
	TerminateThread(hBackgroundThread, 0);
	KillTimer(hWnd, 1);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (WndProcTable.find(message) == WndProcTable.end())
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	WndProcTable.find(message)->second(hWnd, message, wParam, lParam);
	return 0;
}



void WndClassExInit()
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = _T("Number9 Class");
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	RegisterClassEx(&wcex);
}

HWND HWNDInit()
{
	return CreateWindow(_T("Number9 Class"), _T("Number9 Title"),
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
		1500, 555, NULL, NULL, hInst, NULL);
}

HWND MainInit(HINSTANCE hInstance)
{
	hInst = hInstance;
	WndClassExInit();
	HWND hWnd = HWNDInit();
	return hWnd;
}

MSG MainCycle(HWND hWnd, int nCmdShow)
{
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HWND hWnd = MainInit(hInstance);
	MSG msg = MainCycle(hWnd, nCmdShow);
	return (int)msg.wParam;
}
