#include <map>
#include <time.h>
#include <vector>
#include <tchar.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <WinBase.h>
#include <tlhelp32.h>

#define ID_LISTBOX_PROCESSES 103
#define ID_LISTBOX_MODULES 107

#define MENU_ITEM1 4352
#define MENU_ITEM2 4353

using namespace std;

void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMCommandFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMContextMenuFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
typedef void(*WmFunc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

const map<UINT, WmFunc> WndProcTable
{
		{WM_CREATE, &WMCreateFunc},
		{WM_DESTROY, &WMDestroyFunc},
		{WM_COMMAND, &WMCommandFunc},
		{WM_CONTEXTMENU, &WMContextMenuFunc},
};

const map<UINT, const TCHAR*> IdleClassTable
{
	{IDLE_PRIORITY_CLASS, _T("IDLE_PRIORITY_CLASS")},
	{HIGH_PRIORITY_CLASS, _T("HIGH_PRIORITY_CLASS")},
	{NORMAL_PRIORITY_CLASS, _T("NORMAL_PRIORITY_CLASS")},
	{REALTIME_PRIORITY_CLASS, _T("REALTIME_PRIORITY_CLASS")}
};

HMENU hMenu;
HWND hListProcesses;
HWND hListModules;
HINSTANCE hInst;

vector<PROCESSENTRY32> processesList;


const TCHAR* StrForID(DWORD IDparam)
{
	if (IdleClassTable.find(IDparam) == IdleClassTable.end())
	{
		return _T("CAN'T_GET_PRIORITY_CLASS");
	}
	return IdleClassTable.find(IDparam)->second;
}

int AddTextArrowToString(TCHAR *string, int index)
{
	string[index++] = _T(' ');
	string[index++] = _T('-');
	string[index++] = _T('-');
	string[index++] = _T('-');
	string[index++] = _T(' ');
	return index;
}

int CopyString(const TCHAR *input, TCHAR *out, int startOutIndex)
{
	int copyIndex = 0;
	int copyOutIndex = startOutIndex;
	while (input[copyIndex] != _T('\0'))
	{
		out[copyOutIndex] = input[copyIndex];
		copyIndex++;
		copyOutIndex++;
	}
	return copyOutIndex;
}

void AddProcessToListbox(PROCESSENTRY32 processEntry)
{
	TCHAR buffer[1024];
	processesList.push_back(processEntry);
	int curIndex = CopyString(processEntry.szExeFile, buffer, 0);
	curIndex = AddTextArrowToString(buffer, curIndex);

	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processEntry.th32ProcessID);
	const TCHAR *className = StrForID(GetPriorityClass(hProcess));
	curIndex = CopyString(className, buffer, curIndex);
	buffer[curIndex] = _T('\0');

	SendMessage(hListProcesses, LB_ADDSTRING, 0, (LPARAM)buffer);
}

void PrintProcessList()
{
	HANDLE CONST hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (INVALID_HANDLE_VALUE != hSnapshot)
	{
		PROCESSENTRY32 processEntry;

		processesList.clear();
		processEntry.dwSize = sizeof(PROCESSENTRY32);
		Process32First(hSnapshot, &processEntry);
		do
		{
			AddProcessToListbox(processEntry);
		} while (Process32Next(hSnapshot, &processEntry));
	}
	CloseHandle(hSnapshot);
}



void CreateLists(HWND hWnd)
{
	hListProcesses = CreateWindow(_T("LISTBOX"), _T(""),
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | LBS_NOTIFY,
		30, 30, 500, 120, hWnd, (HMENU)ID_LISTBOX_PROCESSES, hInst, NULL);
	hListModules = CreateWindow(_T("LISTBOX"), _T(""),
		WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | LBS_NOTIFY,
		30, 170, 500, 100, hWnd, (HMENU)ID_LISTBOX_MODULES, hInst, NULL);
}

void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	CreateLists(hWnd);
	PrintProcessList();
	SendMessage(hListProcesses, LB_RESETCONTENT, 0, 0);
	PrintProcessList();
}



void CreateContextMenu(HWND hWnd, LPARAM lParam, DWORD value)
{
	hMenu = CreatePopupMenu();
	for (auto pair : IdleClassTable)
	{
		AppendMenu(hMenu, (value == pair.first ? MF_DISABLED : 0), pair.first, pair.second);
	}
	TrackPopupMenu(hMenu, TPM_LEFTALIGN, LOWORD(lParam), HIWORD(lParam), 0, hWnd, NULL);
}

void WMContextMenuFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HANDLE hPr;
	DWORD value = -1;
	int index = SendMessage(hListProcesses, LB_GETCURSEL, 0, 0);
	if (index != LB_ERR)
	{
		hPr = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processesList[index].th32ProcessID);
		value = GetPriorityClass(hPr);
	}
	CreateContextMenu(hWnd, lParam, value);
}



void PrintModulesList(DWORD CONST dwProcessId)
{
	HANDLE CONST hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwProcessId);
	if (INVALID_HANDLE_VALUE != hSnapshot)
	{
		MODULEENTRY32 meModuleEntry;
		SendMessage(hListModules, LB_RESETCONTENT, 0, 0);
		meModuleEntry.dwSize = sizeof(MODULEENTRY32);
		Module32First(hSnapshot, &meModuleEntry);
		do
		{
			auto kek = meModuleEntry.szModule;
			SendMessage(hListModules, LB_ADDSTRING, 0, (LPARAM)kek);
		} while (Module32Next(hSnapshot, &meModuleEntry));
	}
	CloseHandle(hSnapshot);
}

void CommandSetPriority(HANDLE hPr, WPARAM wParam)
{
	if (IdleClassTable.find(LOWORD(wParam)) != IdleClassTable.end())
	{
		SetPriorityClass(hPr, LOWORD(wParam));
		SendMessage(hListProcesses, LB_RESETCONTENT, 0, 0);
		PrintProcessList();
		SendMessage(hListModules, LB_RESETCONTENT, 0, 0);
	}
}

void WMCommandFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int index = SendMessage(hListProcesses, LB_GETCURSEL, 0, 0);
	if (index != LB_ERR)
	{
		HANDLE hPr = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processesList[index].th32ProcessID);
		PrintModulesList(processesList[index].th32ProcessID);
		CommandSetPriority(hPr, wParam);
	}
}



void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PostQuitMessage(0);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (WndProcTable.find(message) == WndProcTable.end())
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	WndProcTable.find(message)->second(hWnd, message, wParam, lParam);
	return 0;
}



void WndClassExInit()
{
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = _T("Number8 Class");
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	RegisterClassEx(&wcex);
}

HWND HwndInit()
{
	return CreateWindow(_T("Number8 Class"),
		_T("Number8 Title"),
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
		560, 400, NULL, NULL, hInst, NULL);
}

HWND MainInit(HINSTANCE hInstance)
{
	hInst = hInstance;
	WndClassExInit();
	return HwndInit();
}

MSG MainCycle(HWND hWnd, int nCmdShow)
{
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HWND hWnd = MainInit(hInstance);
	MSG msg = MainCycle(hWnd, nCmdShow);
	return (int)msg.wParam;
}
