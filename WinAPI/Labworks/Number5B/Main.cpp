#include <map>
#include <time.h>
#include <vector>
#include <tchar.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <WinBase.h>
#include <tlhelp32.h>

using namespace std;

HWND hRButton;
HINSTANCE hInst;
UINT customMessageColor;
UINT customMessageState;
UINT customMessageFigure;


const UINT MY_RECT = 0;
const UINT MY_CIRC = 1;
const UINT MY_STAR = 2;
const UINT MY_DIAM = 3;

int curFigure = MY_RECT;

int cRed = 0;
int cBlue = 1;
int cGreen = 0;

int isDraw = true;

POINT diamCoord[] = { { 0, 0 }, { 20, 20 }, { 0, 20 }, { -20, 0 } };
POINT starCoord[] = {
	{ 0, 0 }, { 5, 20 },
	{ 25, 15 },	{ 10, 30 },	{ 15, 50 }, { 0, 35 },
	{ -15, 50 }, { -10, 30 }, { -25, 15 }, { -5, 20 }
};
POINT curCoord[11];
int x = -1;
int y = -1;

void WMPaintFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMButtonDownFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
typedef void(*WMFunc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
const map<UINT, WMFunc> WndProcTable{
	{WM_PAINT, &WMPaintFunc},
	{WM_DESTROY, &WMDestroyFunc},
	{WM_LBUTTONDOWN, &WMButtonDownFunc},
	{WM_RBUTTONDOWN, &WMButtonDownFunc}
};

void DrawRect(HDC hdc);
void DrawCirc(HDC hdc);
void DrawStar(HDC hdc);
void DrawDiam(HDC hdc);
typedef void(*DrawFunc)(HDC hdc);
const map<UINT, DrawFunc> DrawFigureTable
{
	{MY_RECT, &DrawRect},
	{MY_CIRC, &DrawCirc},
	{MY_STAR, &DrawStar},
	{MY_DIAM, &DrawDiam}
};


void DrawRect(HDC hdc)
{
	Rectangle(hdc, x, y, x + 40, y + 20);
}

void DrawCirc(HDC hdc)
{
	Ellipse(hdc, x - 20, y - 20, x + 20, y + 20);
}

void DrawStar(HDC hdc)
{
	for (int i = 0; i < 10; i++)
		curCoord[i].x = x + starCoord[i].x, curCoord[i].y = y + starCoord[i].y;
	Polygon(hdc, curCoord, 10);
}

void DrawDiam(HDC hdc)
{
	for (int i = 0; i < 10; i++)
		curCoord[i].x = x + diamCoord[i].x, curCoord[i].y = y + diamCoord[i].y;
	Polygon(hdc, curCoord, 4);
}

void TryDrawFigure(HDC hdc)
{
	if (DrawFigureTable.find(curFigure) == DrawFigureTable.end())
	{
		DrawFigureTable.find(MY_RECT)->second(hdc);
		return;
	}
	DrawFigureTable.find(curFigure)->second(hdc);
}

void WMPaintFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	auto hdc = BeginPaint(hWnd, &ps);

	if (!isDraw || (x == -1 && y == -1))
	{
		return;
	}

	auto hBrush = CreateSolidBrush(RGB((int)cRed * 255, (int)cGreen * 255, (int)cBlue * 255));
	SelectObject(hdc, hBrush);
	TryDrawFigure(hdc);

	EndPaint(hWnd, &ps);
}


void WMButtonDownFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	x = LOWORD(lParam);
	y = HIWORD(lParam);
	InvalidateRect(hWnd, NULL, TRUE);
}

void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PostQuitMessage(0);
}


void MaybeMessage(UINT message, LPARAM lParam)
{
	if (message == customMessageColor)
	{
		cRed = lParam == 1 ? 1 : 0;
		cBlue = lParam == 2 ? 1 : 0;
		cGreen = lParam == 3 ? 1 : 0;
	}
	else if (message == customMessageFigure)
	{
		curFigure = lParam;
	}
	else if (message == customMessageState)
	{
		isDraw = lParam == 1 ? true : false;
	}
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	MaybeMessage(message, lParam);
	if (WndProcTable.find(message) == WndProcTable.end())
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	WndProcTable.find(message)->second(hWnd, message, wParam, lParam);
	return 0;
}



WNDCLASSEX WndClassExInit()
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = _T("Number5B Class");
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	return wcex;
}

HWND HWNDInit()
{
	return CreateWindow(_T("Number5B Class"),
		_T("Number5B Title"),
		WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
		400, 400, NULL, NULL, hInst, NULL);
}

void InitChangesTable()
{
	customMessageFigure = RegisterWindowMessage(_T("MyCustomMessageFigure"));
	customMessageColor = RegisterWindowMessage(_T("MyCustomMessageColor"));
	customMessageState = RegisterWindowMessage(_T("MyCustomMessageState"));
}

void MainWindowInit(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance;
	RegisterClassEx(&WndClassExInit());
	auto hWnd = HWNDInit();
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	InitChangesTable();
}

MSG MainCycle()
{
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MainWindowInit(hInstance, nCmdShow);
	return (int)MainCycle().wParam;
}