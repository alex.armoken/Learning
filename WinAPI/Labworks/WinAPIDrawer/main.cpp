#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <cstdlib>
#include <math.h>
#include <vector>

using namespace std;

typedef struct
{
	int r = 0;
	int g = 0;
	int b = 0;
    int x = 0;
	int y = 0;
	int step = 0;
} DRAWPOINT;

const int dragonMaxStepCount = 10000;
const double M_PI = 3.16159265358979323846;
const int d = 4;
const double da = M_PI / 2;
const int canvasMargin = 50;

RECT canvasPosition;
vector<DRAWPOINT> drawPointsArray;

int GetRandColor()
{
	return rand() % 254;
}

void DrawFracLine(HDC hdc, double x, double y, double l, double u)
{
	int x1 = (int)round(x);
	int y1 = (int)round(y);
	if (x1 < canvasPosition.left
		|| y1 < canvasPosition.top
		|| x1 > canvasPosition.right
		|| y1 > canvasPosition.bottom) return;

	int x2 = (int)round(x + l*cos(u));
	int y2 = (int)round(y - l*sin(u));

	if (x2 < canvasPosition.left) x2 = x2 - (x2 - canvasPosition.left);
	if (y2 < canvasPosition.top) y2 = y2 - (y2 - canvasPosition.top);
	if (x2 > canvasPosition.right) x2 = x2 - (x2 - canvasPosition.right);
	if (y2 > canvasPosition.bottom) y2 = y2 - (y2 - canvasPosition.bottom);

	MoveToEx(hdc, x1, y1, NULL);
	LineTo(hdc, x2, y2);
}

int DragonFunc(int i)
{
	int j = i;
	while (1)
	{
	    if ((j - 1) % 4 == 0)
		{
			return -1;
		}
		else if ((j - 3) % 4 == 0)
		{
			return 1;
		}
		else
		{
			j = j / 2;
		}
	}
}

void DrawDragon(HDC hddc, DRAWPOINT point)
{
	HPEN randomPen;
	double x = point.x;
	double y = point.y;
	double a = M_PI / 2;

	randomPen = CreatePen(PS_SOLID, 1, RGB(point.r, point.g, point.b));
	SelectObject(hddc, randomPen);

	for (int i = 1; i < point.step; i++)
	{
		DrawFracLine(hddc, x, y, d, a);
		x = x + d * cos(a);
		y = y - d * sin(a);
		a = a - (M_PI / 2) * DragonFunc(i);
	}
}

void DrawCanvas(HWND hWnd)
{
	GetWindowRect(hWnd, &canvasPosition);
	canvasPosition.left = canvasMargin;
	canvasPosition.top = canvasMargin;
	canvasPosition.bottom = canvasPosition.bottom - canvasMargin;
	canvasPosition.right = canvasPosition.right - canvasMargin;

	HDC hddc = GetDC(hWnd);
	FillRect(hddc, &canvasPosition, WHITE_BRUSH);
}

LRESULT CALLBACK MyMessageHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	DRAWPOINT newPoint;
	PAINTSTRUCT ps;
	HDC hddc;

	switch (message)
	{
	case WM_COMMAND:
		if (LOWORD(wParam) == 228)
		{
			drawPointsArray.clear();
			InvalidateRect(hWnd, NULL, true);
			DrawCanvas(hWnd);
		}
		break;
	case WM_LBUTTONUP:
		newPoint.x = LOWORD(lParam);
		newPoint.y = HIWORD(lParam);

		if (newPoint.x < canvasPosition.left
			|| newPoint.y < canvasPosition.top
			|| newPoint.x > canvasPosition.right
			|| newPoint.y > canvasPosition.bottom) break;

		newPoint.r = GetRandColor();
		newPoint.g = GetRandColor();
		newPoint.b = GetRandColor();
		newPoint.step = rand() % dragonMaxStepCount;

		drawPointsArray.insert(drawPointsArray.end(), newPoint);

		hddc = GetDC(hWnd);
		DrawDragon(hddc, newPoint);
		break;
	case WM_PAINT:
		hddc = BeginPaint(hWnd, &ps);
		DrawCanvas(hWnd);
		for (int i = 0; i < (int)drawPointsArray.size(); i++)
		{
			DrawDragon(hddc, drawPointsArray[i]);
		}
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam); // All other massages will process Windows
	}
	return 0;
}

ATOM RegMyWindowClass(HINSTANCE hInst, LPCTSTR lpzClassName)
{
	WNDCLASS wcWindowClass = { 0 };

	wcWindowClass.lpfnWndProc = (WNDPROC)MyMessageHandler; //Adress of function what will process messages
	wcWindowClass.style = CS_HREDRAW | CS_VREDRAW;
	wcWindowClass.hInstance = hInst;
	wcWindowClass.lpszClassName = lpzClassName;
	wcWindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcWindowClass.hbrBackground = (HBRUSH)COLOR_APPWORKSPACE;
	return RegisterClass(&wcWindowClass);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nCmdShow)
{
	LPCTSTR lpzClass = _T("WinAPIDrawerClass");

	if (!RegMyWindowClass(hInstance, lpzClass))
		return 1;

	int startWindowSizeX = 300;
	int startWindowSizeY = 300;

	// Calculation of the coordinates of window at start
	RECT screen_rect;
	GetWindowRect(GetDesktopWindow(), &screen_rect); //Get screen resolution
	int x = screen_rect.right / 2 - startWindowSizeX / 2;
	int y = screen_rect.bottom / 2 - startWindowSizeY / 2;

	HWND mainWindow = CreateWindow(lpzClass, _T("Дракон Хартера-Хейтуэя"),
								   WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_MAXIMIZE, x, y,
								   300, 300, NULL, NULL, hInstance, NULL);

	HWND ClearButton = CreateWindow(
		_T("BUTTON"),   // predefined class
		_T("Clear"),       // button text
		WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,  // styles
		10,         // starting x position
		10,         // starting y position
		50,        // width
		30,        // height
		mainWindow,       // parent window
		(HMENU)228,       // Name of event
		(HINSTANCE)GetWindowLong(mainWindow, GWL_HINSTANCE),
		NULL);      // pointer not needed

	if (!mainWindow) return 2;

	MSG msg = { 0 };
	int iGetOk = 0;
	while ((iGetOk = GetMessage(&msg, NULL, 0, 0)) != 0)
	{
		if (iGetOk == -1) return 3;
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return msg.wParam;
}
