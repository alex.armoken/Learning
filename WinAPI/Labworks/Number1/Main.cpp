#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>
#include <math.h>
#include "resource.h"

#define M_PI 3.14159
#define FST_TIMER 2001

static int curX = 350;
static int curY = 250;
static int centerX = 250;
static int centerY = 250;
static double curAngle = 0;
static int radius = 100;

const int sWeight = 100;
const int sHeight = 20;

HINSTANCE hInst;
HWND stc;

void ChangeXY()
{
	if (curAngle > 2 * M_PI) curAngle = 0;
	curX = (int)(radius * cos(curAngle)) + centerX;
	curY = (int)(radius * sin(curAngle)) + centerY;
	curAngle += 0.1;
}

void WMCommandFunc(HWND hWnd, WPARAM wParam)
{
	switch (LOWORD(wParam))
	{
	case ID_MOVE_OFF:
		KillTimer(hWnd, FST_TIMER);
		break;
	case ID_MOVE_ON:
		SetTimer(hWnd, FST_TIMER, 50, NULL);
		break;
	}
}

void WMTimerFunc(HWND hWnd)
{
	ChangeXY();
	SetWindowPos(stc, hWnd,
		curX - sWeight / 2,
		curY - sHeight / 2,
		sWeight, sHeight,
		SWP_NOSIZE | SWP_NOZORDER);
}

void WMCreateFunc(HWND hWnd)
{
	stc = CreateWindowW(_T("STATIC"),
		NULL,
		WS_CHILD | WS_VISIBLE | WS_BORDER,
		curX - sWeight / 2,
		curY - sHeight / 2,
		sWeight, sHeight,
		hWnd, NULL, hInst, NULL);
	SetWindowText(stc, _T("Number1 Title"));
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND:
		WMCommandFunc(hWnd, wParam);
		break;
	case WM_CREATE:
		WMCreateFunc(hWnd);
		break;
	case WM_TIMER:
		WMTimerFunc(hWnd);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		KillTimer(hWnd, FST_TIMER);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
}

WNDCLASS InitWND(HINSTANCE hInstance)
{
	WNDCLASS wc;
	memset(&wc, 0, sizeof(WNDCLASS));
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WndProc;
	wc.hInstance = hInstance;
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszClassName = _T("Number1 Class");
	wc.lpszMenuName = MAKEINTRESOURCEW(IDR_MYMENU);
	wc.hIcon = LoadIconW(hInstance, MAKEINTRESOURCEW(IDI_MYICON));
	wc.hCursor = LoadIconA(hInstance, MAKEINTRESOURCEA(IDI_MYCURSOR));

	return wc;
}
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	hInst = hInstance;
	auto wc = InitWND(hInstance);

	RegisterClass(&wc);
	auto hWnd = CreateWindow(_T("Number1 Class"),
		_T("Number1 Title"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		550, 550, NULL,
		NULL, hInstance, NULL);

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)msg.wParam;
}