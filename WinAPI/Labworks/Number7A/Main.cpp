#include <map>
#include <tchar.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

#define FST_TIMER 111

using namespace std;

typedef struct color
{
	BYTE red;
	BYTE green;
	BYTE blue;

	color() {};
	color(BYTE red, BYTE green, BYTE blue) : red(red), green(green), blue(blue) {};
} MyColor;

void WMPaintFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMTimerFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
typedef void(*WmFunc)(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
const map<UINT, WmFunc> WndProcTable{
	{WM_CREATE, &WMCreateFunc},
	{WM_TIMER, &WMTimerFunc},
	{WM_PAINT, &WMPaintFunc},
	{WM_DESTROY, &WMDestroyFunc}
};

HANDLE hEvent;
HINSTANCE hInst;
HANDLE hWaitSemaphore;
HANDLE hChangeSemaphore;

MyColor* color1;
MyColor* color2;
MyColor* color3;

HANDLE colorThread1;
HANDLE colorThread2;
HANDLE colorThread3;

BYTE curRed = 255;
BYTE curGreen = 255;
BYTE curBlue = 255;

void ChangeColor(MyColor *color)
{
	WaitForSingleObject(hChangeSemaphore, INFINITE);
	curRed = color->red;
	curGreen = color->green;
	curBlue = color->blue;
	ReleaseMutex(hChangeSemaphore);
}

void WaitForColorChanging(MyColor *color)
{
	WaitForSingleObject(hWaitSemaphore, INFINITE);
	ChangeColor(color);
	Sleep(1000);
	ReleaseMutex(hWaitSemaphore);
}

DWORD WINAPI DrawThreadFunc(LPVOID tParam)
{
	while (WaitForSingleObjectEx(hEvent, 0, TRUE) == WAIT_OBJECT_0)
	{
		WaitForColorChanging((MyColor*)tParam);
	}
	ExitThread(0);
}



void FillState(HWND hWnd, HDC hdc)
{
	RECT rect;
	GetWindowRect(hWnd, &rect);
	rect.top = 0;
	rect.left = 0;

	WaitForSingleObject(hChangeSemaphore, INFINITE);
	FillRect(hdc, &rect, CreateSolidBrush(RGB(curRed, curGreen, curBlue)));
	ReleaseMutex(hChangeSemaphore);
}

void WMPaintFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	auto hdc = BeginPaint(hWnd, &ps);
	FillState(hWnd, hdc);
	EndPaint(hWnd, &ps);
}

void WMTimerFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	InvalidateRect(hWnd, NULL, TRUE);
}

void WMCreateFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	SetTimer(hWnd, FST_TIMER, 100, NULL);
}

void WMDestroyFunc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	KillTimer(hWnd, FST_TIMER);
	PostQuitMessage(0);
}



LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (WndProcTable.find(message) == WndProcTable.end())
	{
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	WndProcTable.find(message)->second(hWnd, message, wParam, lParam);
	return 0;
}



WNDCLASSEX WndClassExInit()
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInst;
	wcex.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = _T("Number7A Class");
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	return wcex;
}

HWND HWNDInit()
{
	return CreateWindow(_T("Number7A Class"),
		_T("Number7A Title"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		800, 500, NULL,
		NULL, hInst, NULL);
}

HWND MainWindowInit(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance;
	RegisterClassEx(&WndClassExInit());
	return HWNDInit();
}

void ColorsInit(HANDLE waitMutex, HANDLE changeMutex, HANDLE event)
{
	color1 = new MyColor(255, 0, 0);
	color2 = new MyColor(0, 255, 0);
	color3 = new MyColor(0, 0, 255);
}

void ThreadsInit()
{
	colorThread1 = CreateThread(NULL, 0, DrawThreadFunc, color1, CREATE_SUSPENDED, NULL);
	colorThread2 = CreateThread(NULL, 0, DrawThreadFunc, color2, CREATE_SUSPENDED, NULL);
	colorThread3 = CreateThread(NULL, 0, DrawThreadFunc, color3, CREATE_SUSPENDED, NULL);
}

void ColorsAndThreadsInit(HWND hWnd)
{
	hWaitSemaphore = CreateMutex(NULL, FALSE, NULL);
	hChangeSemaphore = CreateMutex(NULL, FALSE, NULL);
	hEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
	ColorsInit(hWaitSemaphore, hChangeSemaphore, hEvent);
	ThreadsInit();
}

void StartThreads()
{
	ResumeThread(colorThread1);
	ResumeThread(colorThread2);
	ResumeThread(colorThread3);
}

void StopThreads()
{
	ResetEvent(hEvent);
	WaitForSingleObject(colorThread1, INFINITE);
	WaitForSingleObject(colorThread2, INFINITE);
	WaitForSingleObject(colorThread3, INFINITE);
}

MSG MainMessageCycle(HWND hWnd, int nCmdShow)
{
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	StartThreads();
	MSG msg;
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	StopThreads();
	return msg;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	HWND hWnd = MainWindowInit(hInstance, nCmdShow);
	ColorsAndThreadsInit(hWnd);
	MSG msg = MainMessageCycle(hWnd, nCmdShow);
	return (int)msg.wParam;
}
