import argparse
import enum
import logging
import pathlib
import re
import sys
from collections import OrderedDict, defaultdict
from typing import Set, Dict, List, Optional, NewType, Tuple

import multidict


logger = logging.Logger(__name__, logging.INFO)

SectionNum = NewType("SectionNum", int)
SectionName = NewType("SectionName", str)


class MovingKind(enum.Enum):
    INSERT_INSTEAD = 0
    INSERT_BEFORE = 1
    INSERT_AFTER = 1
    SWAP_FILES = 2


MOVING_KIND_SIGN_MAP = {
    MovingKind.INSERT_INSTEAD: "=>",
    MovingKind.INSERT_BEFORE: "->",
    MovingKind.INSERT_AFTER: "->>",
    MovingKind.SWAP_FILES: "<->"
}
SIGN_MOVING_KIND_MAP = {v: k for k, v in MOVING_KIND_SIGN_MAP.items()}

MOVING_SIGN_REGEXP = "({})".format(
    "|".join(sign for sign in MOVING_KIND_SIGN_MAP.values())
)
SECTION_NUM_REGEXP = "[0-9]+"
SECTION_NAME_REGEXP = "[_A-Za-z0-9]+"
SECTION_DIR_REGEXP = re.compile(
    f"^(?P<cur_num>){SECTION_NUM_REGEXP}"
    f"(?P<new_num>{MOVING_SIGN_REGEXP}{SECTION_NUM_REGEXP})?"
    f"_(?P<main_name_part>{SECTION_NAME_REGEXP})$"
)


class Section:
    def __init__(self, num: SectionNum, name: SectionName):
        self.num = num
        self.name = name


class MovingOperation:
    def __init__(self, section_name: SectionName, num: SectionNum,
                 dest_num: SectionNum, kind: MovingKind):
        self.section_name = section_name
        self.num = num
        self.dest_num = dest_num
        self.kind = kind

    @property
    def cur_full_name(self):
        return "{}{}{}_{}".format(
            self.num,
            MOVING_KIND_SIGN_MAP[self.kind],
            self.dest_num,
            self.section_name
        )

    @property
    def new_full_name(self):
        return "{}_{}".format(self.num, self.main_name_part)


class DescOfSectionsMovedToTheSameTrg:
    def __init__(self):
        self.inserted_instead = set()
        self.inserted_before = set()
        self.inserted_after = set()
        self.swapped = set()

    def add_inserted_instead(self, num: SectionNum) -> None:
        self.inserted_instead.add(num)

    def add_inserted_before(self, num: SectionNum) -> None:
        self.inserted_before.add(num)

    def add_inserted_after(self, num: SectionNum) -> None:
        self.inserted_after.add(num)

    def add_swapped(self, num: SectionNum) -> None:
        self.swapped.add(num)

    def add(self, num: SectionNum, moving_kind: MovingKind) -> None:
        {
            MovingKind.INSERT_INSTEAD: self.add_inserted_instead,
            MovingKind.INSERT_BEFORE: self.add_inserted_before,
            MovingKind.INSERT_AFTER: self.add_inserted_after,
            MovingKind.SWAP_FILES: self.add_swapped
        }[moving_kind](num)

    def is_multiple_inserted_instead_single(self) -> bool:
        return len(self.inserted_instead) > 1


class ErrorCheckResult:
    def __init__(self, banned_sections: Set[SectionNum]):
        self.banned_sections = banned_sections

    @property
    def is_passed(self):
        return len(self.set_of_banned) == 0


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser("Section directories trimmer")

    parser.add_argument("--path", "-p",
                        action="store", nargs=1,
                        type=str, required=True)
    parser.add_argument("--recursive", "-r", action="store_true")

    return parser.parse_args()


def try_get_section_dirs(path: pathlib.Path) -> Optional[List[pathlib.Path]]:
    was_bad_section_dir_found = False
    sections = []
    for maybe_path_to_section_dir in sorted(path.glob("*")):
        if not maybe_path_to_section_dir.is_dir:
            continue

        if not re.match(SECTION_DIR_REGEXP, maybe_path_to_section_dir.name):
            print(
                "CAN'T continue trimming, because <<{}>> directory "
                "DOESN'T MATCHED as section dir".format(
                    maybe_path_to_section_dir
                )
            )
            was_bad_section_dir_found = True

        sections.add(maybe_path_to_section_dir)

    if was_bad_section_dir_found:
        return None

    return sections


def try_get_moving_operation(
    section_dir: pathlib.Path
) -> Optional[MovingOperation]:
    match_obj = re.search(SECTION_DIR_REGEXP, section_dir.name)
    if match_obj is None:
        print("IGNORING directory <<{}>>, "
              "because it name is in wrong format".format(section_dir))
        return None

    section_name = match_obj.group("main_name_part")
    cur_num = match_obj.group("cur_num")

    new_num_str = match_obj.group("new_num")
    if new_num_str is None:
        return None

    new_num = int(re.sub("[<=->]", "", new_num_str))
    maybe_moving_sign = re.sub("[0-9]", "", new_num_str)
    try:
        moving_kind = MOVING_KIND_SIGN_MAP[maybe_moving_sign]
    except KeyError:
        print("IGNORING directory <<{}>>, "
              "which contains INVALID moving sign!".format(section_dir))
        return None

    return MovingOperation(section_name, cur_num, new_num, moving_kind)


def try_get_moving_operations(
    section_dirs: List[pathlib.Path]
) -> Optional[List[MovingOperation]]:
    moving_operations = []
    for section_dir in section_dirs:
        maybe_moving_operation = try_get_moving_operation(section_dir)
        if maybe_moving_operation is not None:
            moving_operations.append(maybe_moving_operation)

    return moving_operations


def get_numbers_that_will_be_deleted(
    moving_operations: List[MovingOperation]
) -> Set[SectionNum]:
    numbers_to_delete = set()
    for operation in moving_operations:
        if (operation.moving_kind == MovingKind.INSERT_INSTEAD
                # Can't make operation with deleted section
                and operation.cur_num not in numbers_to_delete):
            numbers_to_delete.add(operation.new_num)

    return numbers_to_delete


def create_trg_src_sections_map(
    moving_operations: List[MovingOperation]
) -> Dict[SectionNum, DescOfSectionsMovedToTheSameTrg]:
    result = defaultdict(DescOfSectionsMovedToTheSameTrg)
    for operation in moving_operations:
        result[operation.new_num].add(operation.cur_num, operation.moving_kind)

    return result


def check_moving_multiples_instead_single(
    trg_src_map: Dict[SectionNum, DescOfSectionsMovedToTheSameTrg],
    banned: Set[SectionNum] = set()
) -> ErrorCheckResult:
    new_banned = set()
    for trg_num, src_desc in trg_src_map.items():
        if src_desc.is_multiple_inserted_instead_single():
            new_banned = new_banned.union(src_desc.inserted_instead)

    return ErrorCheckResult(new_banned)


def check_moving_instead_of_section_that_moved_too(
    sections_movings: List[MovingOperation],
    banned: Set[SectionNum] = set()
) -> ErrorCheckResult:
    new_banned = set()
    sections_moved = set()
    for moving_oper in sections_movings:
        if moving_oper.num in banned or moving_oper.dest_num in banned:
            continue
        elif moving_oper.kind in {MovingKind.INSERT_INSTEAD,
                                  MovingKind.INSERT_BEFORE}:
            sections_moved.add(moving_oper.num)
        elif moving_oper.kind == MovingKind.INSERT_AFTER:
            sections_moved.add(moving_oper.dest_num)
        elif moving_oper.kind == MovingKind.SWAP:
            sections_moved.add(moving_oper.num)
            sections_moved.add(moving_oper.dest_num)

    for moving_oper in sections_movings:
        if (moving_oper.kind == MovingKind.INSERT_INSTEAD
                and moving_oper.dest_num in sections_moved):
            new_banned.add(moving_oper.num)
            new_banned.add(moving_oper.dest_num)

    return ErrorCheckResult(new_banned)


def check_swaps_with_common_section(
    sections_movings: List[MovingOperation],
    banned: Set[SectionNum] = set()
) -> ErrorCheckResult:
    new_banned = set()

    return ErrorCheckResult(new_banned)


def check_impossible_movings(
    section_dirs: List[pathlib.Path],
    sections_movings: List[MovingOperation]
) -> bool:
    """
    Ошибки:
      1) Если несколько секций перемещаются с замещением в одно место
      2) Перемещение с замещением в номер, который перемещен любым образом в
         другое место
      3) Несколько свопов с одним общим элементом
      4) Перемещение с замещением в номер или своп с номером, который не
         существует
      5) Перемещение A перед B, а B перед A
      6) Использование в правиле перемещения номера, который учавствует в
         правиле с ошибкой
    """
    trg_src_map = create_trg_src_sections_map(sections_movings)

    banned_sections = set()
    error_check_1 = check_moving_multiples_instead_single(
        trg_src_map,
        banned_sections
    )
    banned_sections = banned_sections.union(error_check_1.banned_sections)

    error_check_2 = check_moving_instead_of_section_that_moved_too(
        sections_movings,
        banned_sections
    )
    banned_sections = banned_sections.union(error_check_2.banned_sections)

    return len(banned_sections) == 0


def move_sections(
    sections_dirs: List[pathlib.Path],
    sections_movings: List[MovingOperation]
) -> List[pathlib.Path]:
    """
    Обработка непростый ситуаций:
      1) Если в одно место перемещается (свопы также считаются) несколько
         секций, то порядок их вставки в нужное место соответствует их
         лексикографическому порядку
      2) Если секция перемещается в номер, в который перемещается с замещением
         другая секция, то вначале перемещаем туда с замещением, а потом
         вставляем туда секцию
      3) Если секция перемещается в несуществующий номер, то она просто меняет
         номер
      4) Выводим список операций, которые нельзя совершить.
         Вначале составляем OrderedDict с результирующими номерами согласно
         правилам, а потом сортируем так, чтобы получился непрерывный ряд
         чисел, и после этого делаем все разрешенные перемещения.
    """
    # numbers = set(section_info_map.keys())
    # numbers_to_delete = get_numbers_that_will_be_deleted(section_info_map)
    # number_impossible_to_move = find_impossible_movings(section_info_map)
    # numbers_to_arrange = numbers \
    #     .difference(numbers_to_delete) \
    #     .difference(number_impossible_to_move)

    result = []
    # for i in range(len(section_info_map)):
    #     pass

    # for section_num, section_info in section_info_map.items():
    #     if section_num in numbers_to_arrange:
    #         pass
    #     else:
    #         result.append(section_num)

    return result


def trim_section_dirs(path_to_dir: pathlib.Path, is_recursive: bool) -> None:
    maybe_section_dirs = try_get_section_dirs(path_to_dir)
    if maybe_section_dirs is None:
        return

    moving_operations = try_get_moving_operations(maybe_section_dirs)
    if moving_operations is None:
        return

    if not check_impossible_movings(maybe_section_dirs, moving_operations):
        return

    new_paths_to_sections = move_sections(
        maybe_section_dirs,
        moving_operations
    )

    if is_recursive:
        for path_to_section in new_paths_to_sections:
            trim_section_dirs(path_to_section, is_recursive)


def main() -> int:
    args = parse_args()

    path_to_root_dir = pathlib.Path(args.path[0])
    if not path_to_root_dir.is_dir():
        logger.error("This is not a directory!")
        return 1

    trim_section_dirs(path_to_root_dir, args.recursive)
    return 0


if __name__ == '__main__':
    sys.exit(main())
